{include file="orderforms/control_standard/common.tpl"}
<!--main content start-->
<section id="main-content" class="cart">
	<!-- Display mobile sidebar alternative if applicable -->
	{if $itCartSidebarMobileDisplay eq "enabled"}
		<div class="row cat-col-row visible-xs visible-sm">
			<div class="col-md-12">
				{include file="orderforms/control_standard/sidebar-categories-collapsed.tpl"}
			</div>
		</div>
	{/if}
	<!-- Display page title -->
	<div class="row">
		<div class="col-md-12">
			<!--breadcrumbs start -->
			{if $itCartSidebarDisplay eq "enabled"}
				<div style="position: absolute; right: 15px; margin-top: 22px;">
				    <button type="button" class="btn btn-default btn-sm hidden-xs hidden-sm" id="btnShowSidebar">
				        <i class="fa fa-arrow-circle-left"></i>
				        {$LANG.showMenu}
				    </button>
				</div>
			{/if}
			{include file="$template/includes/breadcrumb.tpl"}
			<!--breadcrumbs end -->
			<h1 class="h1">{$LANG.cartdomainsconfig}</h1>
			<small class="res-left pull-right" style="margin: 12px 12px 20px 12px;">{$LANG.orderForm.reviewDomainAndAddons}</small>
		</div>
	</div>
	<!-- Display sidebar layout if applicable -->
	{if $itCartSidebarDisplay eq "enabled"}
		<div class="row cart-main-column">
			<div id="internal-content" class="col-md-12 pull-md-left">
				{/if}
				<div class="row">
					<div class="col-md-12">
			            <form id="frmProductDomain" onsubmit="checkdomain();return false">
							
							<div class="panel panel-default">
								<div class="panel-heading">
									<h3 class="panel-title">	
										{$LANG.domaincheckerchoosedomain}
									</h3>
								</div>
								<div class="panel-body">
									<div class="row">
										<div class="col-md-12">
							                <div class="domain-selection-options">
							                    {if $incartdomains}
							                        <div class="option">
							                            <label class="radio-icheck">
							                                <input type="radio" name="domainoption" value="incart" id="selincart" class="icheck cpdoption" /> <span>{$LANG.cartproductdomainuseincart}</span>
							                            </label>
							                            <div class="domain-input-group clearfix" id="domainincart">
															<div class="col-md-6">
						                                        <div class="domains-row">
						                                            <select id="incartsld" name="incartdomain" class="form-control">
						                                                {foreach key=num item=incartdomain from=$incartdomains}
						                                                    <option value="{$incartdomain}">{$incartdomain}</option>
						                                                {/foreach}
						                                            </select>
						                                        </div>
																<br />
						                                        <button type="submit" class="btn btn-primary btn-3d">
						                                            {$LANG.orderForm.use}
						                                        </button>
															</div>
							                            </div>
							                        </div>
							                    {/if}
							                    {if $registerdomainenabled}
							                        <div class="option">
							                            <label class="radio-icheck">
							                                <input type="radio" name="domainoption" value="register" id="selregister" class="icheck cpdoption" /> <span>{$LANG.cartregisterdomainchoice|sprintf2:$companyname}</span>
							                            </label>
							                            <div class="domain-input-group clearfix" id="domainregister">
						                                    <div class="col-md-10">
						                                        <div class="domains-row">
					                                                <div class="input-group">
					                                                    <span class="input-group-addon">{$LANG.orderForm.www}</span>
					                                                    <input type="text" id="registersld" value="{$sld}" class="form-control" autocapitalize="none" />
					                                                    <span class="input-group-btn" id="tlddropdown2">
																			<button type="button" id="tldbutton2" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown"><span id="tldbtn2">{$registertlds.0}</span> <span class="caret"></span></button>
																			<ul class="dropdown-menu dropdown-menu-right">
																				{foreach $registertlds as $listtld}
																					<li class="text-right"><a href="#" onClick="document.getElementById('registertld').value='{$listtld}'">{$listtld}</a></li>
										                                        {/foreach}
																			</ul>
																		</span>			                                                    
					                                                </div>
					                                                <input type="hidden" id="registertld" value="{$registertlds.0}" />
						                                        </div>
																<br />
						                                        <button type="submit" class="btn btn-primary btn-3d">
						                                            {$LANG.orderForm.check}
						                                        </button>
							                                </div>
							                            </div>
							                        </div>
							                    {/if}
							                    {if $transferdomainenabled}
							                        <div class="option">
							                            <label class="radio-icheck">
							                                <input type="radio" name="domainoption" value="transfer" id="seltransfer" class="icheck cpdoption" /> <span>{$LANG.carttransferdomainchoice|sprintf2:$companyname}</span>
							                            </label>
							                            <div class="domain-input-group clearfix" id="domaintransfer">
							                            	<div class="col-md-10">
						                                        <div class="domains-row">			                               
					                                                <div class="input-group">
					                                                    <span class="input-group-addon">www.</span>
					                                                    <input type="text" id="transfersld" value="{$sld}" class="form-control" autocapitalize="none" />
																		<span class="input-group-btn" id="tlddropdown3">
																			<button type="button" id="tldbutton3" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown"><span id="tldbtn3">{$transfertlds.0}</span> <span class="caret"></span></button>
																			<ul class="dropdown-menu dropdown-menu-right">
																				{foreach $transfertlds as $listtld}
																					<li class="text-right"><a href="#" onClick="document.getElementById('transfertld').value='{$listtld}'">{$listtld}</a></li>
										                                        {/foreach}
																			</ul>
																		</span>
					                                                </div>
																	<input type="hidden" id="transfertld" value="{$transfertlds.0}" />
						                                        </div>
																<br />
						                                        <button type="submit" class="btn btn-primary btn-3d">
						                                            {$LANG.orderForm.transfer}
						                                        </button>
					                                        </div>
														</div>
						                            </div>
							                    {/if}
							                    {if $owndomainenabled}
							                        <div class="option">
							                            <label class="radio-icheck">
							                                <input type="radio" name="domainoption" value="owndomain" id="selowndomain" class="icheck cpdoption" /> <span>{$LANG.cartexistingdomainchoice|sprintf2:$companyname}</span>
							                            </label>
							                            <div class="domain-input-group clearfix" id="domainowndomain">
								                            <div class="col-md-10">
						                                        <div class="domains-row">			                               
					                                                <div class="input-group">
					                                                    <span class="input-group-addon">www.</span>
																		<input type="text" id="owndomainsld" value="{$sld}" placeholder="{$LANG.yourdomainplaceholder}" class="form-control" autocapitalize="none" />
																		<span class="input-group-addon dot">.</span>
							                                            <input type="text" id="owndomaintld" size="4" value="{$tld|substr:1}" placeholder="{$LANG.yourtldplaceholder}" class="form-control" autocapitalize="none" />
							                                        </div>
							                                    </div>
							                                    <br />
						                                        <button type="submit" class="btn btn-primary btn-3d" id="useOwnDomain">
						                                            {$LANG.orderForm.use}
						                                        </button>
							                                </div>
							                            </div>
							                        </div>
							                    {/if}
												{if $subdomains}
							                        <div class="option">
							                            <label class="radio-icheck">
							                                <input type="radio" name="domainoption" value="subdomain" id="selsubdomain" class="icheck cpdoption" /> <span>{$LANG.cartsubdomainchoice|sprintf2:$companyname}</span>
							                            </label>
							                            <div class="domain-input-group clearfix" id="domainsubdomain">
								                            <div class="col-md-10">
						                                        <div class="domains-row">			                               
					                                                <div class="input-group">
					                                                    <span class="input-group-addon">http://</span>
							                                            <input type="text" id="subdomainsld" value="{$sld}" placeholder="yourname" class="form-control" autocapitalize="none" />
							                                            <span class="input-group-btn" id="tlddropdown4">
																			<button type="button" id="tldbutton4" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown"><span id="tldbtn4">{$subdomains.0}</span> <span class="caret"></span></button>
																			<ul class="dropdown-menu dropdown-menu-right">
																				{foreach $subdomains as $subid => $subdomain}
																					<li class="text-right"><a href="#" onClick="document.getElementById('subdomaintld').value='{$subid}'">{$subdomain}</a></li>
										                                        {/foreach}
																			</ul>
																		</span>
																	</div>
						                                        </div>
																<br />
						                                        <button type="submit" class="btn btn-primary btn-3d">
						                                            {$LANG.orderForm.check}
						                                        </button>
																<select id="subdomaintld" class="form-control" style="visibility:hidden;">
				                                                    {foreach $subdomains as $subid => $subdomain}
				                                                        <option value="{$subid}">{$subdomain}</option>
				                                                    {/foreach}
				                                                </select>
							                                </div>
							                            </div>
							                        </div>
												{/if}
							                </div>	
											{if $freedomaintlds}
							                    <br /><p>* <em>{$LANG.orderfreedomainregistration} {$LANG.orderfreedomainappliesto}: {$freedomaintlds}</em></p><br />
											{/if}
										</div>
									</div>
								</div>
							</div>
						</form>
						<div class="clearfix"></div>
			            <div class="domain-loading-spinner" id="domainLoadingSpinner">
			                <i class="fa fa-3x fa-spinner fa-spin"></i>
			            </div>
			            <form method="post" action="cart.php?a=add&pid={$pid}&domainselect=1" id="frmProductDomainSelections">
			                <div class="domain-search-results" id="domainSearchResults"></div>
			            </form>						
					</div>
				</div>
				{if $itCartSidebarDisplay eq "enabled"}
			</div>
			<div class="col-md-3 pull-md-right whmcs-sidebar hidden-xs hidden-sm sidebar-secondary cart-sidebar">
				{include file="orderforms/control_standard/sidebar-categories.tpl"}
			</div>
			<div class="clearfix"></div>
		</div>
	{/if}
	<div class="clearfix"></div>
</section>
{include file="orderforms/control_standard/icheck.tpl"}