{include file="orderforms/OnAppOnePage/header.tpl"} 
{literal}
    <script type="text/javascript">
        //Set Custom Template
        MG_Cart.custom_template =   'OnAppOnePage';
        
        $(function(){
            /* --- ONE STEP SLIDER ---- */

            var $slider =  $('#onepage-slider .slider');
            $slider.slider({
                    min: 0, 
                    max: {/literal}{math equation="x - y" x=$products|@count y=1}{literal}, 
                    range: "min", 
                    value: 0, 
                    animate: true, 
                    stop:function(e,ui){
                                    slidCb(true, ui.value);
                    },change: function(e, ui){	
                        $(ui.handle).queue(function(){
                            slidCb(false, ui.value, function(){
                                pid = $("#onepage-slider .slider-packages .current").attr("data-pid");
                                addProduct(pid);
                            });
                        });
                    }/*,slide: function(e, ui){
                                    slidCb(false, ui.value);
                    }*/, create: function(e, ui) {
											$slider.find('.ui-slider-handle').append('<i class="icon icon-caret-left"></i><i class="icon icon-caret-right"></i>');
											$("#onepage-slider .slider-packages > div:first-child").click();
											
											var packagecol = $('#onepage .slider').width() / ( $('.slider-packages > div').length - 1);
											var labelcol = $('#onepage .slider').width() / ($('#onepage .slider-labels > p').length - 1);
											
											$('#onepage .slider-packages > div').each(function(i) 
											{
													$('#onepage .slider-packages > div, #onepage .slider-labels > p').eq(i).css({'left' : packagecol * i});
											});
											
											$('#onepage .slider-labels > p').width(labelcol);
											$('#onepage .slider-labels').css({'margin-left' : -(labelcol/2)}).css({'margin-right' : -(labelcol/2)});
                     viewCart();
                }
            }); 


            var sliderpackages = $('.slider-packages li');
            var sliderlength = sliderpackages.length;

            $('.slider-packages li, .slider-labels li').css({width: 100/sliderlength+'%'});
            $('.slider-packages li:eq(0), .slider-labels li:eq(0), .slider-features li:eq(0)').addClass('current');  
        });
    </script>
{/literal}

<div class="accordion" id="prodgroup">

	<div class="accordion-group">
		<div class="accordion-heading">
			<a class="accordion-toggle" data-toggle="collapse" data-parent="#prodgroup" href="#collapse1">
                            <span>{$lang.current_product_group} <b>{$groupname}</b></span>
                            <span class="accordion-btn">
                                {$LANG.change} <i class="icon icon-plus"></i>
                            </span>
                        </a>
		</div>
		  
		<div id="collapse1" class="accordion-body collapse">
			<div class="accordion-inner">
			
			{foreach from=$productgroups item=group}
				<div class="template-row">
					<label><input type="radio" name="gid" value="{$group.gid}" onclick="window.location='cart.php?gid={$group.gid}'"{if $group.gid eq $gid} checked{/if} /> {$group.name}</label> 
				</div><!--/template-row-->
			{/foreach}
			{*if $loggedin}
				<div class="template-row">
				<label><input type="radio" name="gid" onclick="window.location='cart.php?gid=addons'" /> {$LANG.cartproductaddons}</label>
				<label><input type="radio" name="gid" onclick="window.location='cart.php?gid=renewals'" /> {$LANG.domainrenewals}</label>
				</div><!--/template-row-->
			{/if}
			{if $registerdomainenabled}
				<div class="template-row">
				<label><input type="radio" name="gid" onclick="window.location='cart.php?a=add&domain=register'" /> {$LANG.registerdomain}</label>
				</div><!--/template-row-->
			{/if}
			{if $transferdomainenabled}
				<div class="template-row">
				<label><input type="radio" name="gid" onclick="window.location='cart.php?a=add&domain=transfer'" /> {$LANG.transferdomain}</label>
				</div><!--/template-row-->
			{/if*}			
			
			</div>
                 
		</div>
 </div><!--/accordion group-->
 
{if !$loggedin && $currencies}
<div id="currencychooser" >
{foreach from=$currencies item=curr}
<a href="cart.php?gid={$gid}&currency={$curr.id}"><img src="images/flags/{if $curr.code eq "AUD"}au{elseif $curr.code eq "CAD"}ca{elseif $curr.code eq "EUR"}eu{elseif $curr.code eq "GBP"}gb{elseif $curr.code eq "INR"}in{elseif $curr.code eq "JPY"}jp{elseif $curr.code eq "USD"}us{elseif $curr.code eq "ZAR"}za{else}na{/if}.png" border="0" alt="" /> {$curr.code}</a>
{/foreach}
</div>
{/if}

</div> 

<h2>Select package that <b>fits your needs.</b></h2>
<div id="onepage-slider">
    <div class="slider-content">
		
        <div class="slider-packages">
				
            {foreach from=$products item=product key=num}
                <div onclick="scrollToEl({$num+1}, {$product.pid});" data-pid="{$product.pid}"><span></span>
                  <div class="layerpackage">
                    <div class="layerpackage-back"></div>
                    <div class="layerpackage-bluebox"></div>
                    <div class="layerpackage-front-left"></div>
                    <div class="layerpackage-front-right"></div>
                  </div>
                </div>
            {/foreach}
						
        </div><!--slider-packages-->
				
        <div class="onepage-slider slider"></div>
        <div class="slider-labels">
            {foreach from=$products item=product key=num}
                <p onclick="scrollToEl({$num+1}, {$product.pid});">{$product.name}</li>
            {/foreach}
        </div><!--slider-labels-->
    </div><!-- slider-content -->
    <div class="clearfix"></div>
    <ul class="slider-features">
        {foreach from=$products item=product key=num}
            <li>
                {if $product.custom_description}
                <ul class="slider-features-bar">
                    {foreach from=$product.custom_description item=it}
                        <li>
                            <span>{$it[0]}</span>
                            {$it[1]}
                        </li>  
                    {/foreach}
                    {*<li class="slider-price">
                        <span>{$product.custom_description[5][0]}</span>
                        <b>{$product.custom_description[5][1]}</b>
                    </li>*}
                </ul>
                {/if}
            </li>
        {/foreach}
    </ul>
</div>

<h2><b>Configure</b> your product</h2>

<div class="step-main">
    <div id="add-domain"></div><!-- add domain-->
    <div id="domains-conf"></div>
    <div id="conf-product"></div>
    <div id="cart-summary"></div><!-- cart summary -->
</div>

<div class="step-sidebar"></div>
<div class="clearfix"></div>
        </div><!-- stepcontent -->
<div class="widget step-actions">
    <div class="widget-row">
         <div class="loader order-loader pull-right" style="margin-left:10px; margin-right:-20px;"></div>
        <p>{$LANG.ordersecure} (<strong>{$ipaddress}</strong>) {$LANG.ordersecure2}</p>
        <a id="complete-order" href="#" class="btn btn-large btn-green pull-right btn-complete btn-click">{$LANG.completeorder}  <i class="icon icon-ok"></i></a>
    </div>
</div>
    
    
{include file="orderforms/OnAppOnePage/footer.tpl"} 