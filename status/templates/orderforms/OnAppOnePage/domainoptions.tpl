{if $invalid}
    <div class="alert alert-error">
        <i class="icon icon-remove"></i>
            {$LANG.cartdomaininvalid}
    </div>
{elseif $alreadyindb}
    <div class="alert alert-error">
        <i class="icon icon-remove"></i>
            {$LANG.cartdomainexists}
    </div>
{elseif $status eq "unavailable" && $checktype=="register"}
    <div class="alert alert-error">
        <i class="icon icon-remove"></i>
        {$LANG.cartdomaintaken|sprintf2:$domain}
    </div>
{elseif  $status eq "available" && $checktype=="transfer"}
    <div class="alert alert-error">
        <i class="icon icon-remove"></i>
        {$LANG.carttransfernotregistered|sprintf2:$domain}
    </div>
{elseif $checktype=="register" && $regenabled && $status eq "available"}
    <form action="" method="post" id="domainfrm" style="display: none">
        <input type="hidden" name="domainoption" value="register" />
        <div class="domainavailable">{$LANG.cartcongratsdomainavailable|sprintf2:$domain}</div>
        <input type="hidden" name="domains[]" value="{$domain}" />
        <div class="domainregperiod">{$LANG.cartregisterhowlong} <select name="domainsregperiod[{$domain}]" id="regperiod">{foreach key=period item=regoption from=$regoptions}{if $regoption.register}<option value="{$period}">{$period} {$LANG.orderyears} @ {$regoption.register}</option>{/if}{/foreach}</select></div>
    </form>
    <div class="alert alert-success">
        <i class="icon icon-ok"></i><b>{$sld}{$tld}</b> {$LANG.domainavailable2}
    </div>
{elseif $checktype=="transfer" && $transferenabled && $status eq "unavailable"}
    <form action="" method="post" id="domainfrm" style="display: none">
        <input type="hidden" name="domainoption" value="transfer" />
        <div class="domainunavailable">{$LANG.carttransfernotregistered|sprintf2:$domain}</div>
        <div class="domainavailable">{$LANG.carttransferpossible|sprintf2:$domain:$transferprice}</div>
        <input type="hidden" name="domains[]" value="{$domain}" />
        <input type="hidden" name="domainsregperiod[{$domain}]" value="{$transferterm}" />
    </div>
    <div class="alert alert-success">
        <i class="icon icon-ok"></i><b>{$sld}{$tld}</b> {$LANG.domainavailable2}
    </div>
{elseif $checktype=="owndomain" || $checktype=="subdomain"}
    <form action="" method="" id="domainfrm">
        <input type="hidden" name="domainoption" value="{$checktype}" />
        <input type="hidden" name="sld" value="{$sld}" />
        <input type="hidden" name="tld" value="{$tld}" />
    </form>
    <div class="alert alert-success">
        <i class="icon icon-ok"></i><b>{$sld}{$tld}</b> {$LANG.domainavailable2}
    </div>
{/if}
