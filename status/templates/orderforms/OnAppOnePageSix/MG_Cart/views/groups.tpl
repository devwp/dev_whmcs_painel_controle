

{**
* @author Mariusz Miodowski <mariusz@modulesgarden.com>
*}


<ul>
    {foreach from=$productgroups item=group}
        <li><a href="cart.php?gid={$group.gid}">{$group.name}</a></li>
    {/foreach}
</ul>