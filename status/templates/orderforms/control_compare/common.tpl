{include file="orderforms/$carttpl/cart-options.tpl"}
<link rel="stylesheet" type="text/css" href="templates/orderforms/control_standard/icheck/skins/flat/green.css" />
<script type="text/javascript" src="{$BASE_PATH_JS}/icheck.js"></script>
<script type="text/javascript" src="templates/orderforms/control_standard/base.js"></script>
<script>
jQuery(document).ready(function () {
    jQuery('#btnShowSidebar').click(function () {
        if (jQuery('.cart-sidebar').is(":visible")) {
            jQuery('.cart-main-column').css('right','0');
            jQuery('.cart-sidebar').fadeOut();
            jQuery('#btnShowSidebar').html('<i class="fa fa-arrow-circle-left"></i> {$LANG.showMenu}');
        } else {
            jQuery('.cart-sidebar').fadeIn();
            jQuery('.cart-main-column').css('right','300px');
            jQuery('#btnShowSidebar').html('<i class="fa fa-arrow-circle-right"></i> {$LANG.hideMenu}');
        }
    });
});
</script>