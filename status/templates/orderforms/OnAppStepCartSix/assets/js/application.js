jQuery(document).ready(function ($) {
	
       
	/* --- CART ACCORDION --- */
	
	$("#stepcart .accordion").collapse().height('auto');
	
	$('#stepcart .accordion-toggle').on('click', function () 
	{
			if(!$(this).hasClass('current'))
			{
				$('#stepcart .accordion-toggle').removeClass('current').find('.accordion-btn').html('Show <i class="icon icon-plus"></i>');
				$(this).addClass('current').find('.accordion-btn').html('Hide <i class="icon icon-minus"></i>');
                                $("#stepcart .collapse").css('height', 'auto');
			}
			else 
			{
				$(this).removeClass('current').find('.accordion-btn').html('Show <i class="icon icon-plus"></i>');
                                $("#stepcart .collapse").css('height', '0');
			}
                        return false;

	})
	$('#stepcart .template-row').on('click', function () 
	{
			if($(this).find('input[type=checkbox]').is(':checked')) 
				$(this).addClass('checked-row');
			else 
				$(this).removeClass('checked-row');
	});
	
	$(document).on('click', '.step-tabs li label', function() {
		$(this).tab('show');
	});
       
       $(document).delegate("select[name=country]",'change', function(){
           if(typeof statechange == 'function')
              statechange();
       });
	
	/* --- CART SLIDERS --- */
	
	/*$('.step-main .slider').slider(
	{
		range: "min",
		value:0,
		min: 0,
		max: 10,
		slide: function( event, ui ) {
			$(this).parent().find('.amount input').val( ui.value );
		}	
	});
	
 	$( ".amount input" ).change(function() 
	{
		$(this).closest('.controls').find('.slider').slider( "value", $(this).val() );
	});	
	
 	$( ".amount input" ).each(function() 
	{
			$(this).val($(this).closest('.controls').find('.slider').slider('value'));
	});
	*/
	/* --- ONE STEP SLIDER ---- */
	
	var $slider =  $('#onepage-slider .slider');
	$slider.slider({
		min: 0, 
		max: 6, 
		range: "min", 
		value: 0, 
		animate: true, 
		stop:function(e,ui){
				slidCb(true, ui.value);
		},change: function(e, ui){	
				slidCb(false, ui.value);
		},slide: function(e, ui){	
				slidCb(false, ui.value);
		}, create: function(e, ui) {
				$slider.find('.ui-slider-handle').append('<i class="icon icon-caret-left"></i><i class="icon icon-caret-right"></i>');
		}
	});
	
	
	var sliderpackages = $('.slider-packages li');
	var sliderlength = sliderpackages.length;
	
	$('.slider-packages li, .slider-labels li').css({width: 100/sliderlength+'%'});
	$('.slider-packages li:eq(0), .slider-labels li:eq(0), .slider-features li:eq(0)').addClass('current');
	
	
	/* --- RIGHT SIDEBAR--- */
	//alert($(document).height() - $('#stepcart').height() - $('#stepcart').offset().top + $('.step-actions').height()  + 30)
	/*$('.step-cartsummary').affix({
			offset: {
				top: $('.step-cartsummary').offset().top,
				bottom: 220
			}
	});*/
	
	var btnclick = $('.btn-click');
	btnclick.on('click', function () 
	{
		activate($(this));
	});
	
});

function activate($this) {
	
	var self = $this, 
	activatedClass = 'btn-activated';

	if(!self.hasClass(activatedClass) ) {
		self.addClass(activatedClass);
		setTimeout( function() { self.removeClass( activatedClass ) }, 1000 );
	}
}

/* --- ONE STEP SLIDER FUNCTIONS ---- */

function scrollToEl(ele) {
		$('#onepage-slider .slider').slider("value", ele-1);
}	

function slidCb(magic, ui) {

	$('.slider-labels > li, .slider-features > li, .slider-packages > li').removeClass('current');
	$('.slider-labels > li:eq('+ui+'), .slider-features > li:eq('+ui+'), .slider-packages > li:eq('+ui+')').addClass('current');
		
	$('.slider-packages li').each(function(i) {
		
		if(i <= ui) 
			$(this).addClass('prev');
		else 
			$(this).removeClass('prev');
			
	});

}

// Update Product Configuration
$(function(){
    $(document).delegate("#product-configuration input[type=text]", "change", updateProductConfiguration);
    $(document).delegate("#product-configuration .amount input[type=text]", "change", updateProductConfiguration);
    $(document).delegate("#product-configuration input[type=password]", "change", updateProductConfiguration);
    $(document).delegate("#product-configuration input[type=checkbox]", "click", updateProductConfiguration);
    $(document).delegate("#product-configuration input[type=radio]", "click", updateProductConfiguration);
    $(document).delegate("#product-configuration select", "change", updateProductConfiguration);
});

function updateProductConfiguration()
{
    $.post("cart.php", "ajax=1&a=confproduct&"+$("#product-configuration").serialize(), function(data){
        viewCartSummary();
    });
}


//View Cart
function viewCart()
{

    if($("#stepbar .steps").attr('data-totalSteps')=="1"){
       $.post("cart.php", "ajax=1&a=confproduct&"+$("#product-configuration").serialize(), function(data){
            if(data === '')
            {
                cart = new MG_Cart();
               cart.view(function(data){
                   $("#cart-summary").html(data);
                   $("#cart-summary").show();

                   if(typeof statechange == 'function')
                       statechange();
               });
            }
            else
            {
                $('#errorPopup .modal-body .alert ul').html(data);
                $('#errorPopup').modal('show');
            }
        }); 
    }else{
       cart = new MG_Cart();
       cart.view(function(data){
           $("#cart-summary").html(data);
           $("#cart-summary").show();

           if(typeof statechange == 'function')
               statechange();
       });
        
    }
        
}

//View Cart
function viewCartSummary(clientDetails)
{
    
    cart = new MG_Cart();
    MG_Cart.async = true;  
    cart.viewSummary(function(data){

        $(".step-sidebar").html(data);
    }, clientDetails);
}

//Remove item
function removeItem(type, index)
{
    cart = new MG_Cart();
    if(type === 'p')
    {
        cart.clear(function(){
            viewCartSummary();
        });
        $("#domains-conf").html("");
        $("#conf-product").html("");
    }
    else if(type === 'd')
    {
        cart.deleteDomain(index, function(){
            viewCartSummary();
        }); 
        $("#domains-conf").html("");
    }
}


/*** STEPS ***/

var onapp_current_step = '';
var onapp_current_step_index = 0;

$(function(){
    $(".btn-prevstep").click(function(event){
        event.preventDefault();
        index   =   $("#stepbar ul li.step-current").index();
        current =   $("#stepbar ul li:eq("+index+")").removeClass("step-current");
        prev    =   $("#stepbar ul li:eq("+(index-1)+")").removeClass("step-done").addClass("step-current");
        
        if(index <= 1)
        {
            $(this).hide();
        }
        
        $(".btn-nextstep").show();
        $(".btn-complete").hide();
        $("#cart-summary").hide();
        showStep($("#stepbar ul .step-current a").attr('data-step'));
    });
    
    $(".btn-nextstep").click(function(event){
        event.preventDefault();
        
        index   =   $("#stepbar ul li.step-current").index();
        amount  =   $("#stepbar ul li").length;
        error   =   0;
        
        
        if(index >= amount - 2)
        {        
            $.post("cart.php", "ajax=1&a=confproduct&"+$("#product-configuration").serialize(), function(data){
                if(data === '')
                {
                    confirmationStep();
                }
                else
                {
                    $('#errorPopup .modal-body .alert ul').html(data);
                    $('#errorPopup').modal('show');
                }
            });   
        }
        else
        {
            current =   $("#stepbar ul li:eq("+index+")").removeClass("step-current").addClass("step-done");
            next    =   $("#stepbar ul li:eq("+(index+1)+")").addClass("step-current");
 
            $(".btn-prevstep").show();
            showStep($("#stepbar ul .step-current a").attr('data-step'));
        }
    });
});

function confirmationStep()
{
    $(".btn-nextstep").hide();

    $(".btn-complete").show();
    viewCart();
    if(!$("#stepbar ul").hasClass( "steps1" )){
        index   =   $("#stepbar ul li.step-current").index();
        current =   $("#stepbar ul li:eq("+index+")").removeClass("step-current").addClass("step-done");
        next    =   $("#stepbar ul li:eq("+(index+1)+")").addClass("step-current");
    }

    if($("#stepbar ul .step-current a").attr('data-step') !==undefined && $("#stepbar .steps").attr('data-totalSteps')!="1")
       $(".btn-prevstep").show();
  
    showStep($("#stepbar ul .step-current a").attr('data-step'));
}

//Complete Order
$(function(){
    $("#complete-order").click(function(event){
        event.preventDefault();
        cart = new MG_Cart();
        var loader = $(".order-loader");
        loader.show();
        cart.fullCheckout($("#form-complete").serialize(), function(data2){
            loader.hide();
            if(data2) //Error Handling
            { 
                $('#errorPopup .modal-body .alert ul').html(data2);
                $('#errorPopup').modal('show');
            }
            else
            {
                window.location = 'cart.php?a=fraudcheck';
            }
        });
    });
});


function showStep(step)
{      
    $(".display-step").hide();
    $(step).show();
    onapp_current_step = '';
    
    return false;
}
 
 
 
 function applypromo()
{
    cart = new MG_Cart();
    cart.setPromoCode($("#promocode_val").val(), function(data){
        if(data) //handle error
        {
            $('#errorPopup .modal-body .alert ul').html(data);
            $('#errorPopup').modal('show');
        }
        else
        {
            viewCartSummary();
        }
    });
}

function removepromo()
{
    
    cart = new MG_Cart();
    cart.removePromoCode(function(data){
        viewCartSummary();
    });
}


//Clear Cart
jQuery(function(){
    jQuery(document).delegate(".btn-clearcart", "click", function(event){

        event.preventDefault;
        cart = new MG_Cart();
        cart.clear(function(data){
            viewCartSummary();
        });
        $("#domains-conf").html("");
        $("#conf-product").html("");
        
        $(".btn-nextstep, .btn-complete").hide();
        return false;
    });
});
//CC
function showCCForm()
{
    jQuery("#ccinputform").slideDown();
}
function hideCCForm()
{
    jQuery("#ccinputform").slideUp();
}
function useExistingCC()
{
    jQuery(".newccinfo").hide();
}
function enterNewCC()
{
    jQuery(".newccinfo").show();
}


//Promo Code
function applypromo()
{
    cart = new MG_Cart();
    cart.setPromoCode($("#promocode_val").val(), function(data){
        if(data) //handle error
        {
            $('#errorPopup .modal-body .alert ul').html(data);
            $('#errorPopup').modal('show');
        }
        else
        {
            viewCartSummary();
        }
    });
}

function removepromo()
{
    
    cart = new MG_Cart();
    cart.removePromoCode(function(data){
        viewCartSummary();
    });
}
function showHide(element, status, callback, delay) 
{
	
	if(status == 'on') $(element).addClass('on');
	if(status == 'off') $(element).removeClass('on');
	
	if ((typeof callback === 'function') && delay) 
		setTimeout(callback(),delay);
	else if ((typeof callback === 'function') && !delay) 
		callback();
}

/* State change update tax */
$(function(){
    
      //state
      $(document).delegate("#stateselect, #country", "change", function(event){
            event.preventDefault();
            if($("#stateselect").size()){
                var state = $("#stateselect").val();
            }else{
                var state = $("#stateinput").val();
            }
            var clientDetails ={
                "country": $("#country").val(),
                "state": state
            };
            viewCartSummary(clientDetails);
            
      });
      
      
});