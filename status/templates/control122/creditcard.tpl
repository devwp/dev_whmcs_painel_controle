{if $remotecode}
	<div id="frmRemoteCardProcess" class="text-center" align="center">
		{$remotecode}
		<iframe name="ccframe" class="auth3d-area" width="100%" height="600" scrolling="auto" src="about:blank" frameborder="0"></iframe>
	</div>
	<script language="javascript">
		jQuery("#frmRemoteCardProcess").find("form:first").attr('target', 'ccframe');
		setTimeout("autoSubmitFormByContainer('frmRemoteCardProcess')", 1000);
	</script>
{else}
	<form method="post" action="creditcard.php" class="" role="form">
		<input type="hidden" name="action" value="submit" />
		<input type="hidden" name="invoiceid" value="{$invoiceid}" />
		{if $errormessage}
			{include file="$template/includes/alert.tpl" type="error" errorshtml=$errormessage}
		{/if}
		<div class="row">
			<div class="col-md-8">
				<div id="invoiceIdSummary" class="invoice-summary panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">{$LANG.invoicenumber}{$invoiceid}</h3>
					</div>
					<table class="table table-padded">
						<tr>
							<td><strong>{$LANG.invoicesdescription}</strong></td>
							<td width="150" class="text-right"><strong>{$LANG.invoicesamount}</strong></td>
						</tr>
						{foreach $invoiceitems as $item}
							<tr>
								<td>{$item.description}</td>
								<td class="text-right">{$item.amount}</td>
							</tr>
						{/foreach}
						<tr>
							<td class="total-row text-right">{$LANG.invoicessubtotal}</td>
							<td class="total-row text-right">{$invoice.subtotal}</td>
						</tr>
						{if $invoice.taxrate}
							<tr>
								<td class="total-row text-right">{$invoice.taxrate}% {$invoice.taxname}</td>
								<td class="total-row text-right">{$invoice.tax}</td>
							</tr>
						{/if}
						{if $invoice.taxrate2}
							<tr>
								<td class="total-row text-right">{$invoice.taxrate2}% {$invoice.taxname2}</td>
								<td class="total-row text-right">{$invoice.tax2}</td>
							</tr>
						{/if}
						<tr>
							<td class="total-row text-right">{$LANG.invoicescredit}</td>
							<td class="total-row text-right">{$invoice.credit}</td>
						</tr>
						<tr>
							<td class="total-row text-right"><strong>{$LANG.invoicestotaldue}</strong></td>
							<td class="total-row text-right"><strong>{$invoice.total}</strong></td>
						</tr>
					</table>
					<table class="table table-padded">
						<tr>
							<td class="total-row text-right"><i>{$LANG.paymentstodate}</i></td>
							<td width="150" class="total-row text-right"><i>{$invoice.amountpaid}</i></td>
						</tr>
						<tr>
							<td class="total-row text-right"><strong>{$LANG.balancedue}</strong></td>
							<td class="total-row text-right"><strong>{$balance}</strong></td>
						</tr>
					</table>
				</div>
			</div>
			<div class="col-md-4">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">{$displayTitle}</h3>
					</div>
					<div class="panel-body">
						<div class="form-group">
							<div class="radio {if !$cardOnFile}hidden{/if}">
								<label>
									<input type="radio" name="ccinfo" value="useexisting" onclick="hideNewCardInputFields()" {if $cardOnFile && $ccinfo neq "new"}checked{elseif !$cardOnFile}disabled style="display:none;"{/if} /> {$LANG.creditcarduseexisting}{if $cardOnFile} ({$existingCardType}-{$existingCardLastFour}){/if}
								</label>
							</div>
							<div class="radio">
								<label>
									<input type="radio" name="ccinfo" value="new" onclick="showNewCardInputFields()"{if $ccinfo eq "new" || !$cardOnFile} checked{/if} /> {$LANG.creditcardenternewcard}</label>
								</label>
							</div>
						</div>
						<hr/>
						<div class="form-group{if $userDetailsValidationError} hidden{/if}" id="billingAddressSummary">
							<label for="cctype" class="control-label">Billing Address</label><button type="button" id="btnEditBillingAddress" onclick="editBillingAddress()" class="btn btn-default btn-sm pull-right"{if $cardOnFile} disabled="disabled"{/if}><i class="fa fa-edit"></i> Change</button>
							<br/>
							{if $clientsdetails.companyname}{$clientsdetails.companyname}{else}{$firstname} {$lastname}{/if}<br />
							{$clientsdetails.address1}{if $clientsdetails.address2}, {$clientsdetails.address2}{/if}<br />
							{$clientsdetails.city}, {$clientsdetails.state}, {$clientsdetails.postcode}<br />
							{$clientsdetails.countryname}
						</div>
						<div class="form-group cc-billing-address{if !$userDetailsValidationError} hidden{/if}">
							<label for="inputFirstName" class="control-label">{$LANG.clientareafirstname}</label>
							<input type="text" name="firstname" id="inputFirstName" value="{$firstname}" class="form-control" />
						</div>
						<div class="form-group cc-billing-address{if !$userDetailsValidationError} hidden{/if}">
							<label for="inputLastName" class="control-label">{$LANG.clientarealastname}</label>
							<input type="text" name="lastname" id="inputLastName" value="{$lastname}" class="form-control" />
						</div>
						<div class="form-group cc-billing-address{if !$userDetailsValidationError} hidden{/if}">
							<label for="inputAddress1" class="control-label">{$LANG.clientareaaddress1}</label>
							<input type="text" name="address1" id="inputAddress1" value="{$address1}" class="form-control" />
						</div>
						<div class="form-group cc-billing-address{if !$userDetailsValidationError} hidden{/if}">
							<label for="inputAddress2" class="control-label">{$LANG.clientareaaddress2}</label>
							<input type="text" name="address2" id="inputAddress2" value="{$address2}" class="form-control" />
						</div>
						<div class="form-group cc-billing-address{if !$userDetailsValidationError} hidden{/if}">
							<label for="inputCity" class="control-label">{$LANG.clientareacity}</label>
							<input type="text" name="city" id="inputCity" value="{$city}" class="form-control" />
						</div>
						<div class="form-group cc-billing-address{if !$userDetailsValidationError} hidden{/if}">
							<label for="inputState" class="control-label">{$LANG.clientareastate}</label>
							<input type="text" name="state" id="inputState" value="{$state}" class="form-control" />
						</div>
						<div class="form-group cc-billing-address{if !$userDetailsValidationError} hidden{/if}">
							<label for="inputPostcode" class="control-label">{$LANG.clientareapostcode}</label>
							<input type="text" name="postcode" id="inputPostcode" value="{$postcode}" class="form-control" />
						</div>
						<div class="form-group cc-billing-address{if !$userDetailsValidationError} hidden{/if}">
							<label for="inputCountry" class="control-label">{$LANG.clientareacountry}</label>
							{$countriesdropdown}
						</div>
						<div class="form-group cc-billing-address{if !$userDetailsValidationError} hidden{/if}">
							<label for="inputPhone" class="control-label">{$LANG.clientareaphonenumber}</label>
							<input type="text" name="phonenumber" id="inputPhone" value="{$phonenumber}" class="form-control" />
						</div>
						<div class="form-group cc-details{if !$addingNewCard} hidden{/if}">
							<label for="cctype" class="control-label">{$LANG.creditcardcardtype}</label>
							<select name="cctype" id="cctype" class="form-control newccinfo">
								{foreach from=$acceptedcctypes item=type}
									<option{if $cctype eq $type} selected{/if}>
										{$type}
									</option>
								{/foreach}
							</select>
						</div>
						<div class="form-group cc-details{if !$addingNewCard} hidden{/if}">
							<label for="inputCardNumber" class="control-label">{$LANG.creditcardcardnumber}</label>
							<input type="text" name="ccnumber" id="inputCardNumber" size="30" value="{if $ccnumber}{$ccnumber}{/if}" autocomplete="off" class="form-control newccinfo" />
						</div>
						{if $showccissuestart}
							<div class="form-group cc-details{if !$addingNewCard} hidden{/if}">
								<label for="inputCardStart" class="control-label">{$LANG.creditcardcardstart}</label>
								<div class="row">
									<div class="col-sm-4">
										<select name="ccstartmonth" id="inputCardStart" class="form-control select-inline">
											{foreach from=$months item=month}
												<option{if $ccstartmonth eq $month} selected{/if}>{$month}</option>
											{/foreach}
										</select>
									</div>
									<div class="col-sm-7 col-sm-offset-1">
										<select name="ccstartyear" id="inputCardStartYear" class="form-control select-inline">
											{foreach from=$startyears item=year}
												<option{if $ccstartyear eq $year} selected{/if}>{$year}</option>
											{/foreach}
										</select>
									</div>
								</div>
							</div>
						{/if}
						<div class="form-group cc-details{if !$addingNewCard} hidden{/if}">
							<label for="inputCardExpiry" class="control-label">{$LANG.creditcardcardexpires}</label>
							<div class="row">
								<div class="col-sm-4">
									<select name="ccexpirymonth" id="inputCardExpiry" class="form-control select-inline">
										{foreach from=$months item=month}
											<option{if $ccexpirymonth eq $month} selected{/if}>{$month}</option>
										{/foreach}
									</select>
								</div>
								<div class="col-sm-7 col-sm-offset-1">
									<select name="ccexpiryyear" id="inputCardExpiryYear" class="form-control select-inline">
										{foreach from=$expiryyears item=year}
											<option{if $ccexpiryyear eq $year} selected{/if}>{$year}</option>
										{/foreach}
									</select>
								</div>
							</div>
						</div>
						{if $showccissuestart}
							<div class="form-group cc-details{if !$addingNewCard} hidden{/if}">
								<label for="inputIssueNum" class="control-label">{$LANG.creditcardcardissuenum}</label>
								<input type="text" name="ccissuenum" id="inputIssueNum" value="{$ccissuenum}" class="form-control input-" />
							</div>
						{/if}
						<div class="form-group">
							<label for="cctype" class="control-label">{$LANG.creditcardcvvnumber}</label>
							<a class="btn btn-sm btn-link btn-cvv" data-toggle="popover" data-content="<img src='{$BASE_PATH_IMG}/ccv.gif' width='210' />" data-placement="bottom">
									{$LANG.creditcardcvvwhere}
							</a>
							<input type="text" name="cccvv" id="inputCardCvv" value="{$cccvv}" autocomplete="off" class="form-control" />
						</div>
						{if $shownostore}
							<div class="form-group cc-details{if !$addingNewCard} hidden{/if}">
								<div class="checkbox">
									<label>
										<input type="checkbox" name="nostore" id="inputNoStore"> {$LANG.creditcardnostore}
									</label>
								</div>
							</div>
						{/if}
					</div>
					<div class="panel-footer">
						<input type="submit" class="btn btn-primary btn-3d" value="{$LANG.submitpayment}" onclick="this.value='{$LANG.pleasewait}'" id="btnSubmit" />
					</div>
				</div>
			</div>
		</div>
		<div class="alert alert-warning" role="alert">
			<i class="fa fa-lock"></i> &nbsp; {$LANG.creditcardsecuritynotice}
		</div>
	</form>
{/if}