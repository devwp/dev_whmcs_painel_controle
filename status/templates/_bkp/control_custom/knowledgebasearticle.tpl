{if $kbarticle.voted}
    {include file="$template/includes/alert.tpl" type="success" msg="{lang key="knowledgebaseArticleRatingThanks"}" textcenter=true}
{/if}
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title">
					{$kbarticle.title}
					<a href="#" class="btn btn-default pull-right btn-xs" onclick="window.print();return false"><i class="fa fa-print">&nbsp;</i>{$LANG.knowledgebaseprint}</a>
				</h3>
			</div>
			<div class="panel-body">
				{$kbarticle.text}
			</div>
		</div>
		<div class="hidden-print">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">
						{if $kbarticle.voted}{$LANG.knowledgebaserating}{else}{$LANG.knowledgebasehelpful}{/if}
					</h3>
				</div>
				<div class="panel-body">
					{if $kbarticle.voted}
						{$kbarticle.useful} {$LANG.knowledgebaseratingtext} ({$kbarticle.votes} {$LANG.knowledgebasevotes})
					{else}
						<form action="{if $seofriendlyurls}{$WEB_ROOT}/knowledgebase/{$kbarticle.id}/{$kbarticle.urlfriendlytitle}.html{else}knowledgebase.php?action=displayarticle&amp;id={$kbarticle.id}{/if}" method="post">
							<input type="hidden" name="useful" value="vote">
							<button type="submit" name="vote" value="yes" class="btn btn-success"><i class="fa fa-thumbs-o-up"></i> {$LANG.knowledgebaseyes}</button>
							<button type="submit" name="vote" value="no" class="btn btn-default"><i class="fa fa-thumbs-o-down"></i> {$LANG.knowledgebaseno}</button>
						</form>
					{/if}
				</div>
			</div>
		</div>
	</div>
</div>
{if $kbarticles}
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">{$LANG.knowledgebasealsoread}</h3>
				</div>
				<div class="list-group">
					{foreach key=num item=kbarticle from=$kbarticles}
						<div class="list-group-item">
							<a href="{if $seofriendlyurls}{$WEB_ROOT}/knowledgebase/{$kbarticle.id}/{$kbarticle.urlfriendlytitle}.html{else}knowledgebase.php?action=displayarticle&amp;id={$kbarticle.id}{/if}">
								<i class="fa fa-file-o"></i>
								<!---- editted to ---->
								<strong>
									{$kbarticle.title}
								</strong>
							</a>
							<br />
							{$kbarticle.article|truncate:100:"..."}
						</div>
			        {/foreach}
				</div>
			</div>
		</div>
	</div>
{/if}