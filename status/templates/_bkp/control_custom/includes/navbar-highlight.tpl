<!-- Highlight Active Menu Item -->
{if $templatefile eq "affiliates"}
	<script>
		window.onload = function() {
			document.getElementById("Primary_Navbar-Affiliates").className = "active";
		};
	</script>
{elseif $templatefile eq "knowledgebase"}
	<script>
		window.onload = function() {
			if(document.getElementById("Primary_Navbar-Knowledgebase")){
 				document.getElementById("Primary_Navbar-Knowledgebase").className = "active";
			} else if(document.getElementById("Primary_Navbar-Support-Knowledgebase")){
				document.getElementById("Primary_Navbar-Support-Knowledgebase").className = "active";
				document.getElementById("Primary_Navbar-Support").className = "nav-dropdown open active";
				document.getElementById("Primary_Navbar-Support")querySelector("ul").style = "display: block;";
			};
		};
	</script>
{/if}