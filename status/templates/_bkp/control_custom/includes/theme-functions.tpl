{*		
		Impressive Themes
		http://www.impressivethemes.net
		-------------------------------
		
		Theme Functions for Control Theme by Impressive Themes
		------------------------------------------------------
		
		We would advise customers not to make any changes to the 
		file below. Configurable theme options can be found in the 
		file theme-options.tpl.
		
		Should you require any further help or assistance, please 
		contact customer support via our online support desk at:
		
		http://www.impressivethemes.net/support/contact
		
*}
{* 
	1. 	if itPrimarySidebar = enabled or if itSecondarySidebar = enabled, 
		work out whether or not there is a primary and/or secondary sidebar to display, 
		and whether or not the sidebar layout is needed
	-----------------------------------------------------------------------------------	
*}
		{if $itPrimarySidebar eq "enabled" || $itSecondarySidebar eq "enabled"}
{*		
		1.1 is there a primary sidebar to display?
		------------------------------------------
*}
			{if $itPrimarySidebar eq "enabled"}
				{if $primarySidebar->hasChildren()}
					{assign var=itShowSidebarPrimary value="yes" scope="global"}
				{/if}
			{/if}
{*		
		1.2 is there a secondary sidebar to display?
		--------------------------------------------
*}
			{if $itSecondarySidebar eq "enabled"}
				{if $secondarySidebar->hasChildren()}
					{assign var=itShowSidebarSecondary value="yes" scope="global"}
				{/if}
			{/if}
{*		
		1.3 should the sidebar layout be shown?
		---------------------------------------
*}
			{if !$inShoppingCart and ($itShowSidebarPrimary eq "yes" || $itShowSidebarSecondary eq "yes")}
				{assign var=itShowSidebarLayout value="yes" scope="global"}
			{/if}
		{/if}
{*		
		1.4 disable sidebar display for certain pages
		---------------------------------------------
*}	
		{if $templatefile eq "masspay"}
			{assign var=itShowSidebarLayout value="no" scope="global"}
		{/if}
{* 
	2. For specified pages, show featured-content section above main-body section 
	-----------------------------------------------------------------------------
*}
{* 
		2.1.	clientareahome.tpl  
		--------------------------
*}
		{if $templatefile eq "clientareahome"}
			{assign var=itFeaturedContent value="yes" scope="global"}
			{assign var=itShowSidebarLayout value="yes" scope="global"}
			{assign var=itShowSidebarSecondary value="yes" scope="global"}
		{/if}
{* 
	3. Work out if the left side bar / main nav should be shown or not
	------------------------------------------------------
*}
{* 
		3.1.	should the left side bar / main nav be shown?
		-----------------------------------------------------
*}
		{if $loggedin}
			{assign var=itShowMainNav value="yes" scope="global"}
		{else}
			{if $itGuestNavDisplay eq "disabled"}
				{assign var=itShowMainNav value="no" scope="global"}
			{else}
				{assign var=itShowMainNav value="yes" scope="global"}
			{/if}
		{/if}
{*
	End of Theme Functions File
	---------------------------
*}