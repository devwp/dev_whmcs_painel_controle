{if $invalidTicketId}
    {include file="$template/includes/alert.tpl" type="danger" title=$LANG.thereisaproblem msg=$LANG.supportticketinvalid textcenter=true}
{else}
    {if $closedticket}
        {include file="$template/includes/alert.tpl" type="warning" msg=$LANG.supportticketclosedmsg textcenter=true}
    {/if}
    {if $errormessage}
        {include file="$template/includes/alert.tpl" type="error" errorshtml=$errormessage}
    {/if}
{/if}
{if !$invalidTicketId}
	<form method="post" action="{$smarty.server.PHP_SELF}?tid={$tid}&amp;c={$c}&amp;postreply=true" enctype="multipart/form-data" role="form" id="frmReply" class="form-horizontal">
		<div id="replyform" class="panel panel-info panel-collapsable{if !$postingReply} panel-collapsed{/if} hidden-print">
			<div class="panel-heading" id="ticketReply">
				<div class="collapse-icon pull-right">
					<i class="fa fa-{if !$postingReply}plus{else}minus{/if}"></i>
				</div>
				<h3 class="panel-title">
					<i class="fa fa-pencil"></i> &nbsp; {$LANG.supportticketsreply}
				</h3>
			</div>
			<div id="replybody" class="panel-body{if !$postingReply} panel-body-collapsed{/if}">
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label for="inputName" class="col-sm-3 control-label">{$LANG.supportticketsclientname}</label>
							<div class="col-sm-6">
								{if $loggedin}
									<input class="form-control disabled" type="text" id="inputName" value="{$clientname}" disabled="disabled" />{else}<input class="form-control" type="text" name="replyname" id="inputName" value="{$replyname}" />
								{/if}
							</div>
						</div>
						<div class="form-group">
							<label for="inputEmail" class="col-sm-3 control-label">{$LANG.supportticketsclientemail}</label>
							<div class="col-sm-6">
								{if $loggedin}
									<input class="form-control disabled" type="text" id="inputEmail" value="{$email}" disabled="disabled" />{else}<input class="form-control" type="text" name="replyemail" id="inputEmail" value="{$replyemail}" />
								{/if}
							</div>
						</div>
						<div class="form-group">
							<label for="inputMessage" class="col-sm-3 control-label">{$LANG.contactmessage}</label>
							<div class="col-sm-6">
								<textarea name="replymessage" id="inputMessage" rows="12" class="form-control markdown-editor" data-auto-save-name="client_ticket_reply_{$tid}">{$replymessage}</textarea>
							</div>
						</div>
						<hr />
						<div class="form-group">
							<label for="inputAttachments" class="col-sm-3 control-label">{$LANG.supportticketsticketattachments}</label>
							<div class="col-sm-6">
								<input type="file" name="attachments[]" id="inputAttachments" class="form-control" />
								<div id="fileUploadsContainer"></div>
								<p class="help-block">{$LANG.supportticketsallowedextensions}: {$allowedfiletypes}
							</div>
							<div class="col-sm-3">
								<button type="button" class="btn btn-default btn-sm " onclick="extraTicketAttachment()">
									<i class="fa fa-plus"></i> {$LANG.addmore}
								</button>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div id="replyfooter" class="panel-footer" {if !$postingReply}style="display:none;"{/if}">
					<input class="btn btn-primary btn-3d res-100" type="submit" name="save" value="{$LANG.supportticketsticketsubmit}" />
					<input class="btn btn-default pull-right res-100 res-left" type="reset" value="{$LANG.cancel}" onclick="jQuery('#ticketReply').click()" />
			</div>
		</div>
	</form>
	<div class="panel panel-info visible-print-block">
		<div class="panel-heading">
				<h3 class="panel-title">
						{$LANG.ticketinfo}
				</h3>
		</div>
		<div class="panel-body container-fluid">
				<div class="row">
						<div class="col-md-2 col-xs-6">
								<b>{$LANG.supportticketsticketid}</b><br />{$tid}
						</div>
						<div class="col-md-4 col-xs-6">
								<b>{$LANG.supportticketsticketsubject}</b><br />{$subject}
						</div>
						<div class="col-md-2 col-xs-6">
								<b>{$LANG.supportticketspriority}</b><br />{$urgency}
						</div>
						<div class="col-md-4 col-xs-6">
								<b>{$LANG.supportticketsdepartment}</b><br />{$department}
						</div>
				</div>
		</div>
	</div>
	{foreach from=$descreplies key=num item=reply}
		<div class="panel {if $reply.admin}panel-success{else}panel-default{/if} ticket-reply markdown-content">
			<div class="panel-heading">
				<h3 class="panel-title">
					<i class="fa fa-user"></i> &nbsp; {$reply.name} ({if $reply.admin}{$LANG.supportticketsstaff}{elseif $reply.contactid}{$LANG.supportticketscontact}{elseif $reply.userid}{$LANG.supportticketsclient}{else}{$reply.email}{/if})
					<div class="date pull-right">
						{$reply.date}
					</div>
				</h3>
			</div>
			<div class="panel-body message">
				{$reply.message}
			</div>	
			{if $reply.id && $reply.admin && $ratingenabled}
				<div class="panel-footer">
					<div class="clearfix">
						{if $reply.rating}
							<span class="rating-question">{$LANG.ticketreatinggiven}:</span>
							<div class="rating-done">
								{for $rating=1 to 5}
									<span class="star{if (5 - $reply.rating) < $rating} active{/if}"></span>
								{/for}
							</div>
						{else}
							<span class="rating-question">{$LANG.ticketratingquestion}</span>
							<div class="rating" ticketid="{$tid}" ticketkey="{$c}" ticketreplyid="{$reply.id}">
								<span class="star" rate="5"></span>
								<span class="star" rate="4"></span>
								<span class="star" rate="3"></span>
								<span class="star" rate="2"></span>
								<span class="star" rate="1"></span>
							</div>
						{/if}
					</div>
				</div>
			{/if}
			{if $reply.attachments}
				<div class="panel-footer">
					<strong>{$LANG.supportticketsticketattachments} ({$reply.attachments|count})</strong>
					{foreach from=$reply.attachments key=num item=attachment}
						<br /><i class="fa fa-file-o"></i> <a href="dl.php?type={if $reply.id}ar&id={$reply.id}{else}a&id={$id}{/if}&i={$num}">{$attachment}</a>
					{/foreach}
				</div>
			{/if}
		</div>
	{/foreach}
{/if}