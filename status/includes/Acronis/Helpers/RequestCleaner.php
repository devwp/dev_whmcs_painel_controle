<?php
/**
 *
 * @version 1.0.0.2
 * @Copyright © 2002-2016 Acronis International GmbH. All rights reserved
 */

namespace Acronis\Helpers;

class RequestCleaner
{
    private static $data = array();

    /**
     * Initializes request variables
     * (INPUT_GET/INPUT_POST/INPUT_REQUEST/INPUT_COOKIE/INPUT_SERVER/INPUT_ENV/INPUT_SESSION/INPUT_ARGV - for system argv array)
     * INPUT_REQUEST is default
     * @param string $type
     */
    public static function init($type = 'INPUT_REQUEST')
    {
        switch ($type) {
            case 'INPUT_GET':
                self::$data = $_GET;
                break;
            case 'INPUT_POST':
                self::$data = $_POST;
                break;
            case 'INPUT_REQUEST':
                self::$data = $_REQUEST;
                break;
            case 'INPUT_COOKIE':
                self::$data = $_COOKIE;
                break;
            case 'INPUT_SERVER':
                self::$data = $_SERVER;
                break;
            case 'INPUT_ENV':
                self::$data = $_ENV;
                break;
            case 'INPUT_SESSION':
                self::$data = $_SESSION;
                break;
            case 'INPUT_ARGV':
                $arguments_server = $_SERVER['argv'];
                self::$data = array();
                $index = 0;
                $temparr = array();
                foreach ($arguments_server as $arguments_line) {
                    self::$data [$index] = $arguments_line;
                    $index++;

                    $arguments_tab = explode('&', $arguments_line);
                    foreach ($arguments_tab as $argument) {
                        $name_end = stripos($argument, '=');
                        $value_can = true;
                        if ($name_end === FALSE) {
                            $name_end = strlen($argument);
                            $value_can = false;
                        }
                        $name = substr($argument, 0, $name_end);
                        if ($value_can) {
                            $value = substr($argument, $name_end + 1);
                            $temparr [$name] = $value;
                        }
                    }
                }
                self::$data = array_merge(self::$data, $temparr);
                break;
        }
    }

    /**
     * Gets cleaned value of desired variable after initialization
     * @param string $name - name of the variable to return
     * @param string $default - default value to return if desired variable is not found
     * @param bool $clean_default - true to clean default value (default behavior)
     * @return string
     */
    public static function getVar($name, $default = '', $clean_default = true)
    {
        return self::varIsset($name) ? self::cleanVal(self::$data[$name]) : ($clean_default ? self::cleanVal($default) : $default);
    }

    /**
     * Returns true if desired variable exists after initialization; false otherwise
     * @param $name - name of the variable to check
     * @return boolean
     */
    public static function varIsset($name)
    {
        return isset(self::$data[$name]) ? true : false;
    }

    /**
     * Returns value in cleaned form
     * @param string $val - value to clean
     * @return string
     */
    public static function cleanVal($val)
    {
        return preg_replace('%[^a-zA-Z0-9_-]%', '', $val);
    }

    /**
     * Gets value of desired variable after initialization (without cleaning it)
     * @param $name - name of the variable to return
     * @param string $default - default value to return if desired variable is not found
     * @return string
     */
    public static function getVarRaw($name, $default = '')
    {
        return self::varIsset(self::$data[$name]) ? self::$data[$name] : $default;
    }

    /**
     * Returns array of all initialized and cleaned variables
     * @return array
     */
    public static function getAllVars()
    {
        $vals = array();
        foreach (self::$data as $key => $val) {
            $vals [$key] = self::cleanVal($val);
        }
        return $vals;
    }

    /**
     * Returns array of all initialized  variables (without cleaning them)
     * @return array
     */
    public static function getAllVarsRaw()
    {
        return self::$data;
    }

}