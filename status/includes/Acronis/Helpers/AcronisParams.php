<?php
/**
 *
 * Acronis modules specific params helpers
 * This is just to set proper language for parent class (because it is optional to it)
 * @version 1.0.0
 * @Copyright © 2002-2016 Acronis International GmbH. All rights reserved
 * DEPENDENCIES: \Acronis\WHMCS\Lang
 */
namespace Acronis\Helpers;

use Acronis\Standard\Configs\ModuleConfig;
use Acronis\WHMCS\Lang;
use Acronis\WHMCS\ProvisionParams;

class AcronisParams extends ProvisionParams
{

    public function __construct($params, ModuleConfig $config)
    {
        $lang = new Lang(ACRONIS_INCLUDES_DIR . 'Lang' . DS);
        parent::__construct($params, $config, $lang);
    }

}


