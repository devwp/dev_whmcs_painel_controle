<?php
/**
 *
 * @version 1.0.2
 * @Copyright © 2002-2016 Acronis International GmbH. All rights reserved
 * DEPENDENCIES: Acronis\Database\Database
 */
namespace Acronis\Helpers;

use Illuminate\Database\Capsule\Manager as Capsule;

class ModuleVars
{
    private static $maxValueLengths = array();
    private $moduleName = '';

    public function __construct($module_name)
    {
        $this->moduleName = $module_name;
    }

    /**
     * Sets max value length for variable (stored as varchar in database)
     * If not executed before first get/set attempt, this will be treated as 250 (see install method)
     * This will only work when there is no needed table in database already!
     * This one is static, because it is nice to have possible to save it before object creation (inside configuration initialization for example)
     * @param string $module_name - module name for which max value is set
     * @param int $max_value_length - max value to be set
     */
    public static function setInstallMaxValueLength($module_name, $max_value_length)
    {
        self::$maxValueLengths[$module_name] = $max_value_length;
    }

    /**
     * Gets value attached to desired name from database modulevars table
     * This will create modulevars table if there is no such table in database
     * @param string $name - name of the variable to get
     * @param string $default - default value to be returned when there is no such variable in database
     * @return string
     */
    public function get($name, $default = '')
    {
        $db = Capsule::connection()->getPdo();

        try {
            $sth = $db->prepare('SELECT value FROM `' . $this->moduleName . '_modulevars` WHERE `option` = :name');
            $sth->execute(array(':name' => $name));
            $result = $sth->fetch();
            $val = $result['value'];
        } catch (\Exception $e) {
            $this->install();

            $sth = $db->prepare('SELECT value FROM `' . $this->moduleName . '_modulevars` WHERE `option` = :name');
            $sth->execute(array(':name' => $name));
            $result = $sth->fetch();
            $val = $result['value'];
        }

        if ($val === FALSE) {
            return $default;
        }
        return $val;
    }

    /**
     * Creates modulevars table in database if not already exists
     * value column would be varchar(250). If you want larger columns, use setInstallMaxValueLength static method
     */
    public function install()
    {
        $db = Capsule::connection()->getPdo();

        $max_value_length = isset(self::$maxValueLengths[$this->moduleName]) ? self::$maxValueLengths[$this->moduleName] : 250;

        $db->query("CREATE TABLE IF NOT EXISTS `" . $this->moduleName . "_modulevars` (
                        `id` INT(10) NOT NULL AUTO_INCREMENT,
                        `option` VARCHAR(100) NOT NULL COLLATE utf8_general_ci,
                        `value` VARCHAR(" . $max_value_length . ") NOT NULL COLLATE utf8_general_ci,
                        PRIMARY KEY (`id`),
                        UNIQUE KEY `option` (`option`)
                    ) DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci");
    }

    /**
     * Sets value attached to desired name and stores it to database modulevars table
     * This will create modulevars table if there is no such table in database
     * @param string $name - name of the variable to set
     * @param string $value - value to be set
     * @return int Returns sql affected rows (1 if the value is changed or set, 0 if not)
     */
    public function set($name, $value)
    {
        $db = Capsule::connection()->getPdo();

        try {
            $sth = $db->prepare(
                'REPLACE INTO `' . $this->moduleName . '_modulevars` (`option`,`value`) VALUES  (:name, :value)');
            $sth->execute(array(':name' => $name, ':value' => $value));
            $ret = $sth->rowCount();
        } catch (\Exception $e) {
            self::install();

            $sth = $db->prepare(
                'REPLACE INTO `' . $this->moduleName . '_modulevars` (`option`,`value`) VALUES  (:name, :value)');
            $sth->execute(array(':name' => $name, ':value' => $value));
            $ret = $sth->rowCount();
        }

        return $ret;
    }

    /**
     * Drops modulevars table from database
     */
    public function uninstall()
    {
        $db = Capsule::connection()->getPdo();

        $db->query('DROP TABLE `' . $this->moduleName . '_modulevars`');
    }
}