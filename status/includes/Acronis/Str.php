<?php
namespace Acronis;

/**
 * @Copyright © 2002-2016 Acronis International GmbH. All rights reserved
 */
class Str
{
    /**
     * Format message corresponding passed arguments
     * Usages:
     *      format('hello') -> 'hello'
     *      format('hello %s and %s', 'John', 'James') -> 'hello John and James'
     *      format(
     *          'hello :manager_name and :developer_name',
     *          [':manager_name' => 'John', ':developer_name' => 'James']
     *      ) -> 'hello John and James'
     * @param $message
     * @return string
     */
    public static function format($message)
    {
        if (func_num_args() === 1) {
            return $message;
        }
        $args = func_get_args();
        if (is_array($args[1])) {
            return strtr($message, $args[1]);
        }
        return call_user_func_array('sprintf', $args);
    }

    /**
     * Checks if $haystack string ends with $needle
     * @param string $haystack
     * @param string $needle
     * @return boolean
     */
    public static function endsWith($haystack, $needle)
    {
        return $needle === "" || (($temp = strlen($haystack) - strlen($needle)) >= 0 && strpos($haystack, $needle, $temp) !== false);
    }
}
