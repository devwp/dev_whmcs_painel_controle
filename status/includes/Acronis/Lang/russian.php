<?php
$_LANG['curl_not_installed_error'] = 'cURL для PHP не установлена.';
$_LANG['cant_get_hostname_for_user'] = 'Не удается получить имя хоста для данного пользователя.';
$_LANG['cant_log_into_system'] = 'Не удается войти в систему.';
$_LANG['cant_get_server_admin_id_or_his_group_id'] = 'Не удается получить ИД администратора сервера.';
$_LANG['cant_send_activation_email'] = 'Не удается отправить сообщение активации.';
$_LANG['cant_get_mail_details'] = 'Не удается получить подробную информацию об электронной почте.';
$_LANG['cant_get_access_token'] = 'Не удается получить токен доступа к электронной почте.';
$_LANG['cant_activate_account'] = 'Не удается активировать учетную запись.';
$_LANG['cant_get_admin_details'] = 'Не удается получить подробную информацию об учетной записи администратора.';
$_LANG['cant_find_in_admin_details'] = 'Не удается найти параметр "??" в сведениях об учетной записи администратора.';
$_LANG['cant_send_admin_password_reset'] = 'Не удается отправить сообщение о сбросе пароля.';
$_LANG['cant_determine_access_type'] = 'Не удается определить тип доступа "??".';
$_LANG['admin_credentials_cant_be_empty'] = 'Электронная почта администратора и его имя входа не могут быть пустыми.';
$_LANG['cant_add_group_admin'] = 'Не удается добавить учетную запись администратора группы.';
$_LANG['cant_get_id_of_created_admin'] = 'Не удается создать учетную запись администратора, поскольку невозможно получить его ИД или версию.';
$_LANG['cant_get_subgroups_for_group_id'] = 'Не удается получить подгруппы группы с ИД ??.';
$_LANG['cant_find_storage_id_of_subgroup'] = 'Не удается найти ИД хранилища данных подгруппы.';
$_LANG['cant_get_group_details'] = 'Не удается получить подробную информацию о группе.';
$_LANG['cant_find_group_name_in_parent_group'] = 'Не удается найти подгруппу "??" в данной группе.';
$_LANG['group_with_kind_not_found'] = 'Группа типа "??" не найдена.';
$_LANG['cant_find_in_group'] = 'Не удается найти "??" в данной группе.';
$_LANG['cant_find_final_group_kind'] = 'Не удается найти тип финальной группы. ';
$_LANG['cant_find_reseller_creation_group_kind'] = 'Не удается найти наилучший тип соответствующей группы, в которой разрешено создание данного реселлера.';
$_LANG['cant_find_no_reseller_creation_group_kind'] = 'Не удается найти наилучший тип соответствующей группы, в которой запрещено создание данного реселлера.';
$_LANG['allow_no_allow_reseller_creation_conflict'] = 'Конфликт при разрешении/запрещении создания реселлеров: не удается определить наилучшие типы/оценки группы.';
$_LANG['cant_create_no_reseller_creation_group'] = 'Не удается создать подгруппу реселлера, если реселлер не имеет возможности создать эту группу.';
$_LANG['cant_determine_production_pricing_mode'] = 'Не удается определить наилучший режим ценообразования продукта.';
$_LANG['cant_add_group'] = 'Не удается добавить данную группу в систему.';
$_LANG['cant_get_id_of_created_group'] = 'Не удается создать группу, поскольку невозможно получить ее ИД или версию.';
$_LANG['cant_delete_group'] = 'Не удается удалить группу.';
$_LANG['cant_update_group'] = 'Не удается обновить группу.';
$_LANG['cant_get_version_of_updated_group'] = 'Не удается обновить группу, поскольку невозможно получить ее версию.';
$_LANG['cant_lock_group'] = 'Не удается заблокировать группу.';
$_LANG['cant_unlock_group'] = 'Не удается разблокировать группу.';
$_LANG['cant_get_group_users_details'] = 'Не удается получить информацию о пользователях в группе "??".';
$_LANG['cant_get_group_admins_details'] = 'Не удается получить информацию об администраторах в группе "??".';
$_LANG['cant_find_admin_login_in_group'] = 'Не удается найти администратора с именем входа "??" в группе.';
$_LANG['cant_find_admin_group_id'] = 'Не удается найти ИД группы администратора "??".';
$_LANG['cant_find_group_name_id'] = 'Не удается найти ИД группы "??".';
$_LANG['cant_get_group_bc_link'] = 'Не удается получить ссылку консоли резервного копирования для группы с ИД ??.';
$_LANG['cant_get_logic_constants_db_unknown_host'] = 'Не удается получить логические константы из базы данных, поскольку имя хоста неизвестно.';
$_LANG['cant_save_logic_constants_db_unknown_host'] = 'Не удается сохранить логические константы в базу данных, поскольку имя хоста неизвестно.';
$_LANG['cant_get_logic_constants'] = 'Не удалось получить логические константы.';
$_LANG['group_kind_not_found'] = 'Тип группы "??" не найден.';
$_LANG['cant_get_group_kinds'] = 'Не удается получить типы группы.';
$_LANG['cant_get_pricing_modes'] = 'Не удается получить режимы ценообразования.';
$_LANG['cant_get_access_types'] = 'Не удается получить типы доступа.';
$_LANG['cant_get_statuses_constant_data'] = 'Не удается получить информацию о минимальных статусах.';
$_LANG['cant_get_version_of_updated_group'] = 'Не удается обновить учетную запись пользователя, поскольку невозможно получить ее версию.';
$_LANG['couldnt_connect'] = 'Не удается разрешить имя хоста.';
$_LANG['group_with_such_properties_exist'] = 'Группа с такими свойствами уже существует.';
$_LANG['admin_with_such_email_exist'] = 'Пользователь с таким именем входа уже существует.';
$_LANG['measure']['B'] = 'Б';
$_LANG['measure']['KiB'] = 'КиБ';
$_LANG['measure']['MiB'] = 'МиБ';
$_LANG['measure']['GiB'] = 'ГиБ';
$_LANG['measure']['TiB'] = 'ТиБ	';
$_LANG['measure']['PiB'] = 'ПиБ';
$_LANG['measure']['EiB'] = 'ЭиБ';
$_LANG['measure']['YiB'] = 'ЙиБ';
$_LANG['unlimited'] = 'Без ограничений.';
$_LANG['wrong_data'] = 'Недействительные данные.';
$_LANG['please_check_config'] = 'Проверьте конфигурацию.';
$_LANG['desired_config_option_not_found'] = 'Настраиваемый параметр не найден:';
$_LANG['cant_get_from_custom_fields'] = 'Не удалось получить "??" из настраиваемых полей.';
$_LANG['custom_field_has_wrong_value'] = 'Значение настраиваемого поля "??" неправильное.';
$_LANG['overusage']['server_count'] = 'Квота сервера превышена.';
$_LANG['overusage']['storage_size'] = 'Квота хранения в облачном хранилище данных превышена.';
$_LANG['overusage']['vm_count'] = 'Квота виртуальной машины превышена.';
$_LANG['overusage']['workstation_count'] = 'Квота рабочей станции превышена.';
$_LANG['overusage']['mailbox_count'] = 'Превышена квота для почтовых ящиков Office 365.';
$_LANG['overusage']['mobile_count'] = 'Превышена квота для мобильных устройств.';
$_LANG['nearlylimit']['server_count'] = 'Используется более 90%% квоты сервера.';
$_LANG['nearlylimit']['storage_size'] = 'Используется более 90%% облачного хранилища данных.';
$_LANG['nearlylimit']['vm_count'] = 'Используется более 90%% квоты виртуальной машины.';
$_LANG['nearlylimit']['workstation_count'] = 'Используется более 90%% квоты рабочей станции.';
$_LANG['nearlylimit']['mailbox_count'] = 'Используется более 90%% квоты для почтовых ящиков Office 365.';
$_LANG['nearlylimit']['mobile_count'] = 'Используется более 90%% квоты для мобильных устройств.';
