<?php
/**
 *
 * @version 1.0.0
 * @Copyright © 2002-2016 Acronis International GmbH. All rights reserved
 */
namespace Acronis\WHMCS;

use Acronis\Helpers\Arr;

class Products
{
    /**
     * Fetch product's data by passed id.
     * Return false if entity does not exist or error is occurred.
     *
     * @param int $id
     * @return array|false
     */
    public static function fetchProductDataById($id)
    {
        $localApiRes = localApi('getproducts', array('pid' => $id));
        if (
            $localApiRes['result'] === 'success'
            && ($productData = Arr::getByPath($localApiRes, 'products.product.0'))
        ) {
            return $productData;
        }
        return false;
    }

}
