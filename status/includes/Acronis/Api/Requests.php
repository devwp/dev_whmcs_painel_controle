<?php
/**
 * @Copyright © 2002-2016 Acronis International GmbH. All rights reserved
 */

namespace Acronis\Api;

class Requests extends RequestCaller
{
    public $apiCookie = '';

    /**
     * Login to system - this will generate new cookie file to be used by next requests
     * @param array $login - array of login data ('username' and 'password' keys are needed - see Api documentation)
     * @return array
     */
    public function login($login = array())
    {
        $this->reloadCurl();
        $this->apiCookie = dirname(__FILE__) . '/cookies' . md5($login . time()) . '.txt';
        $this->setCookieFileForRequests($this->apiCookie);
        $this->setCookieJar($this->apiCookie);  //save cookie from this request here
        return $this->runPost("/api/1/login/", $login);
    }

    /**
     * Logout from system (current session - based on cookie file)
     * @warning api call /logout/ seems to not working - returns status 200, but operations are still available, so we just destroy cookie file
     */
    public function logout()
    {
        @unlink($this->apiCookie);
    }

    /**
     * Gets system logic constants
     * @return array
     */
    public function getLogicConstants()
    {
        return $this->runGet("/api/1/logic/constants/");
    }

    /**
     * Gets system logic rules
     * @return array
     */
    public function getLogicRules()
    {
        return $this->runGet("/api/1/logic/rules/");
    }

    /**
     * Gets system logic languages
     * @return array
     */
    public function getLogicLanguages()
    {
        return $this->runGet("/api/1/logic/languages/");
    }

    /**
     * Gets url from which requests shoud be done for desired username
     * @param string $login - login username
     * @return array
     */
    public function accountsRouting($login)
    {
        return $this->runGet("/api/1/accounts/?login={$login}");
    }

    /**
     * Adds accounts to system
     * @param array $data - (See Api documentation)
     * @return array
     */
    public function addAccounts($data = array())
    {
        return $this->runPost("/api/1/accounts/", $data);
    }

    /**
     * Activates account
     * @param array $data - account data in format (array):  {"md5_hashed": 0, "password": "qwerty2"}
     * @param string $token_from_mail_activate - token returned by sendMailActivate method
     * @return array
     */
    public function activateAccount($data, $token_from_mail_activate)
    {
        return $this->runPost("/api/1/actions/activate/?token={$token_from_mail_activate}", $data);
    }

    /**
     * Updates account password
     * @param array $data - new account data in format (array):  {"md5_hashed": 0, "password": "qwerty2"}
     * @param string $token_from_mail_activate - token returned by sendMailActivate method
     * @return array
     */
    public function updateAccount($data, $token_from_mail_activate)
    {
        return $this->runPost("/api/1/actions/reset/?token={$token_from_mail_activate}", $data);
    }

    /**
     * Gets admin data
     * @param int $id_admin - admin id
     * @return array
     */
    public function getAdmin($id_admin)
    {
        return $this->runGet("/api/1/admins/{$id_admin}/");
    }

    /**
     * Deletes admin
     * @param int $id_admin - admin id
     * @param int $version - admin version (gathered from getAdmin request)
     * @return
     */
    public function deleteAdmin($id_admin, $version)
    {
        return $this->runDelete("/api/1/admins/{$id_admin}/?version={$version}");
    }

    /**
     * Updates admin properties
     * @param array $data - new admin properties data (see Api documentation)
     * @param int $id_admin - admin id
     * @param int $version - admin version (gathered from getAdmin request)
     * @return array
     */
    public function updateAdmin($data, $id_admin, $version)
    {
        return $this->runPut("/api/1/admins/{$id_admin}/?version={$version}", $data);
    }

    /**
     * Search system for phrase
     * @param int $limit - how many records to return
     * @param string $prefix - phrase to search prefix
     * @return array
     */
    public function search($limit, $prefix = '')
    {
        return $this->runGet("/api/1/groups/?limit={$limit}&prefix={$prefix}");
    }

    /**
     * Gets information about the group
     * @param int $group_id - group id
     * @return array
     */
    public function getGroup($group_id)
    {
        return $this->runGet("/api/1/groups/{$group_id}/");
    }

    /**
     * Deletes desired group
     * @param $group
     * @param int $version - group version (gathered from getGroup request)
     * @return array
     * @internal param int $group_id
     */
    public function deleteGroup($group, $version)
    {
        return $this->runDelete("/api/1/groups/{$group}/?version={$version}");
    }

    /**
     * Updates group
     * @param array $data - new group data (See Api documentation)
     * @param int $group_id - group id
     * @param int $version - group version (gathered from getGroup request)
     * @return array
     */
    public function updateGroup($data, $group_id, $version)
    {
        return $this->runPut("/api/1/groups/{$group_id}/?version={$version}", $data);
    }

    /**
     * Updates group service data
     * @param array $data - new group service data
     * @param $group
     * @return array
     * @internal param int $group_id - group id
     */
    public function updateServiceGroup($data, $group)
    {
        return $this->runPost("/api/1/groups/{$group_id}/services/", $data);
    }

    /**
     * Locks group
     * @param int $group_id - group id
     * @return array
     */
    public function lockGroup($group_id)
    {
        return $this->runPost("/api/1/groups/{$group_id}/lock/");
    }

    /**
     * Unlocks group
     * @param int $group_id - group id
     * @return array
     */
    public function unlockGroup($group_id)
    {
        return $this->runDelete("/api/1/groups/{$group_id}/lock/");
    }

    /**
     * Adds user to group
     * @param array $data - user data (See Api documentation)
     * @param int $group_id - group id
     * @return array
     */
    public function addGroupUser($data, $group_id)
    {
        return $this->runPost("/api/1/groups/{$group_id}/users/", $data);
    }

    /**
     * Deletes user
     * @param int $user_id - user id
     * @param int $version - user version (gathered from getDetailsUser request)
     * @return array
     */
    public function deleteUser($user_id, $version)
    {
        return $this->runDelete("/api/1/users/{$user_id}/?version={$version}");
    }

    /**
     * Updates user data
     * @warning user must be active before update!
     * @param array $data - new user data (See Api documentation)
     * @param int $user_id - user id
     * @param int $version - user version (gathered from getDetailsUser request)
     * @return array
     */
    public function updateUser($data, $user_id, $version)
    {
        return $this->runPut("/api/1/users/{$user_id}/?version={$version}", $data);
    }

    /**
     * Gets users of desired group
     * @param int $group_id - group id
     * @return array
     */
    public function getGroupUsers($group_id)
    {
        return $this->runGet("/api/1/groups/{$group_id}/users/");
    }

    /**
     * Gets user details
     * @param int $user_id - user id
     * @return array
     */
    public function getDetailsUser($user_id)
    {
        return $this->runGet("/api/1/users/{$user_id}/");
    }

    /**
     * Adds admin to group
     * @param array $data - admin data (See Api documentation)
     * @param int $group_id - group id
     * @return array
     */
    public function addGroupAdmin($data, $group_id)
    {
        return $this->runPost("/api/1/groups/{$group_id}/admins/", $data);
    }

    /**
     * Deletes Group admin
     * @param int $admin_id - admin id
     * @param int $version - admin version (gathered from getDetailsAdmin request)
     * @return array
     */
    public function deleteGroupAdmin($admin_id, $version)
    {
        return $this->runDelete("/api/1/admins/{$admin_id}/?version={$version}");
    }

    /**
     * Gets group admins list
     * @param int $group_id
     * @return array
     */
    public function getGroupAdmins($group_id)
    {
        return $this->runGet("/api/1/groups/{$group_id}/admins/");
    }

    /**
     * Gets admin details
     * @param int $admin_id
     * @return array
     */
    public function getDetailsAdmin($admin_id)
    {
        return $this->runGet("/api/1/admins/{$admin_id}/");
    }

    /**
     * Adds subgroup to group
     * @param array $data - new group data (See Api documentation)
     * @param int $group_id - parent group id
     * @return array
     */
    public function addGroup($data, $group_id)
    {
        return $this->runPost("/api/1/groups/{$group_id}/children/", $data);
    }

    /**
     * Gets e-mail parameters
     * @param string $login - account username
     * @param string $email - e-mail address
     * @return array
     */
    public function getMailDetails($login, $email)
    {
        return $this->runGet("/api/1/actions/mail/activate/?login={$login}&email={$email}");
    }

    /**
     * Return JWT token of specified user
     * used for create link to backup or management
     *
     * @param strin $resource
     * @param int|string $userId
     * @return array
     */
    public function getImpersonate($resource, $userId = 'self')
    {
        return $this->runGet("/api/1/{$resource}/{$userId}/impersonate/");
    }

    /**
     * Sends e-mail with access token and link
     * @param array $data - array with 2 values: 'email' and 'login'
     * @return array
     */
    public function sendMailActivate($data = array())
    {
        return $this->runPost("/api/1/actions/mail/activate/", $data);
    }

    /**
     * Sends e-mail with new access token and link (eg. after password change)
     * @param array $data - array with 2 values: 'email' and 'login'
     * @return array
     */
    public function sendMailReset($data = array())
    {
        return $this->runPost("/api/1/actions/mail/reset/", $data);
    }

    /**
     * Gets group storages list
     * @param int $group_id - id of group
     * @return array
     */
    public function getStoragesGroup($group_id)
    {
        return $this->runGet("/api/1/groups/{$group_id}/storages/");
    }

    /**
     * Returns array with group subgroups
     * @param int $group_id - id of group
     * @return array
     */
    public function getSubgroupsGroup($group_id)
    {
        return $this->runGet("/api/1/groups/{$group_id}/children/");
    }

}
