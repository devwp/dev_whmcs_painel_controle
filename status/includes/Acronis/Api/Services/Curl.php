<?php
/**
 * @Copyright © 2002-2016 Acronis International GmbH. All rights reserved
 */

namespace Acronis\Api\Services;

use Acronis\VersionInfo;

class Curl
{
    const VERSION = '3.5.5';
    const DEFAULT_TIMEOUT = 30;
    public $curl;
    public $id = null;
    public $error = false;
    public $errorCode = 0;
    public $errorMessage = null;
    public $curlError = false;
    public $curlErrorCode = 0;
    public $curlErrorMessage = null;
    public $httpError = false;
    public $httpStatusCode = 0;
    public $httpErrorMessage = null;
    public $baseUrl = null;
    public $url = null;
    public $requestHeaders = null;
    public $responseHeaders = null;
    public $rawResponseHeaders = '';
    public $response = null;
    public $rawResponse = null;
    public $beforeSendFunction = null;
    public $downloadCompleteFunction = null;
    private $successFunction = null;
    private $errorFunction = null;
    private $completeFunction = null;
    private $cookies = array();
    private $headers = array();
    private $options = array();
    private $jsonDecoder = null;
    private $jsonPattern = '/^(?:application|text)\/(?:[a-z]+(?:[\.-][0-9a-z]+){0,}[\+\.]|x-)?json(?:-[a-z]+)?/i';
    private $xmlPattern = '~^(?:text/|application/(?:atom\+|rss\+)?)xml~i';

    /**
     * Construct
     *
     * @access public
     * @param string $base_url - Can be used to save url globally and use put/delete/etc "without" their first parameter (url parameter for them will be treated as data parameter if is array)
     * @throws \ErrorException
     */
    public function __construct($base_url = null)
    {
        if (!extension_loaded('curl')) {
            throw new \ErrorException('cURL library is not loaded');
        }
        $this->curl = curl_init();
        $this->id = 1;
        $this->setDefaultUserAgent();
        $this->setDefaultJsonDecoder();
        $this->setDefaultTimeout();
        $this->setOpt(CURLINFO_HEADER_OUT, true);
        $this->setOpt(CURLOPT_FOLLOWLOCATION, true);
        $this->setOpt(CURLOPT_HEADERFUNCTION, array($this, 'headerCallback'));
        $this->setOpt(CURLOPT_RETURNTRANSFER, true);
        $this->headers = new CaseInsensitiveArray();
        $this->setURL($base_url);
    }

    /**
     * Set Default User Agent
     * Sets User Agent to curl defaults
     * @access public
     */
    public function setDefaultUserAgent()
    {
        $buildInfo = VersionInfo::getInstance();
        $this->setUserAgent(PACKAGE_USER_AGENT . '-' . $buildInfo->getPackageVersion());
    }

    /**
     * Set User Agent
     * Sets User Agent
     * @access public
     * @param  $user_agent
     */
    public function setUserAgent($user_agent)
    {
        $this->setOpt(CURLOPT_USERAGENT, $user_agent);
    }

    /**
     * Set Opt
     * Sets curl option
     * @access public
     * @param  $option
     * @param  $value
     *
     * @return boolean
     */
    public function setOpt($option, $value)
    {
        $required_options = array(
            CURLINFO_HEADER_OUT => 'CURLINFO_HEADER_OUT',
            CURLOPT_RETURNTRANSFER => 'CURLOPT_RETURNTRANSFER',
        );
        if (in_array($option, array_keys($required_options), true) && !($value === true)) {
            trigger_error($required_options[$option] . ' is a required option', E_USER_WARNING);
        }
        $this->options[$option] = $value;
        return curl_setopt($this->curl, $option, $value);
    }

    /**
     * Set Default JSON Decoder
     * Sets JSON Decoder to dataJsonDecode method of this object
     * @access public
     */
    public function setDefaultJsonDecoder()
    {
        $this->jsonDecoder = array($this, 'dataJsonDecode');
    }

    /**
     * Set Default Timeout
     * Sets Timeout to self::DEFAULT_TIMEOUT
     * @access public
     */
    public function setDefaultTimeout()
    {
        $this->setTimeout(self::DEFAULT_TIMEOUT);
    }

    /**
     * Set Timeout
     * Sets curl timeout
     * @access public
     * @param  $seconds
     */
    public function setTimeout($seconds)
    {
        $this->setOpt(CURLOPT_TIMEOUT, $seconds);
    }

    /**
     * Set Url
     * Sets base request url and joins url with data (creates url) (requests methods can be used without url parameter then)
     * @access public
     * @param  $url
     * @param  $data
     */
    public function setURL($url, $data = array())
    {
        $this->baseUrl = $url;
        $this->url = $this->buildURL($url, $data);
        $this->setOpt(CURLOPT_URL, $this->url);
    }

    /**
     * Build Url
     * Builds Url (joins it with data)
     * @access private
     * @param  $url
     * @param  $data
     *
     * @return string
     */
    private function buildURL($url, $data = array())
    {
        return $url . (empty($data) ? '' : (FALSE === stripos($url, '?') ? '?' : '&') . http_build_query($data));
    }

    /**
     * Destruct
     *
     * @access public
     */
    public function __destruct()
    {
        $this->close();
    }

    /**
     * Close
     * Cleans everything up and closes curl handle
     * @access public
     */
    public function close()
    {
        if (is_resource($this->curl)) {
            curl_close($this->curl);
        }
        $this->options = null;
        $this->jsonDecoder = null;
    }

    /**
     * Before Send
     * Sets callback function that will fire before curl_exec (callback parameters are: $this [this object])
     * @access public
     * @param function $callback
     */
    public function beforeSend($callback)
    {
        $this->beforeSendFunction = $callback;
    }

    /**
     * Complete
     * Sets callback function that will fire after curl_exec, after other callbacks (callback parameters are: $this [this object])
     * @access public
     * @param  $callback
     */
    public function complete($callback)
    {
        $this->completeFunction = $callback;
    }

    /**
     * Error
     * Sets callback function that will fire after curl_exec, if curl_errno() function returns non-zero result or there is HTTP error (callback parameters are: $this [this object])
     * @access public
     * @param  $callback
     */
    public function error($callback)
    {
        $this->errorFunction = $callback;
    }

    /**
     * Success
     * Sets callback function that will fire after curl_exec, if curl_errno() function returns zero result and there are no HTTP errors (callback parameters are: $this [this object])
     * @access public
     * @param  $callback
     */
    public function success($callback)
    {
        $this->successFunction = $callback;
    }

    /**
     * Progress
     * Sets callback function that will fire while curl_exec is executing. See CURLOPT_PROGRESSFUNCTION for more info
     * @access public
     * @param  $callback
     */
    public function progress($callback)
    {
        $this->setOpt(CURLOPT_PROGRESSFUNCTION, $callback);
        $this->setOpt(CURLOPT_NOPROGRESS, false);
    }

    /**
     * Data Json Decode
     * Returns $data decoded form json if possible or untauched $data if not possible
     * @access public
     * @param  $json_str
     *
     * @return string
     */
    public function dataJsonDecode($json_str)
    {
        $data = json_decode($json_str);
        if (!($data === null)) {
            $json_str = $data;
        }
        return $json_str;
    }

    /**
     * Delete
     * Executes DELETE request (curl_exec)
     * @access public
     * @param  $url - url for request, will be treated as $query_parameters if $url is array (url that has been set in object constructor or with setURL will be used instead)
     * @param  $query_parameters - request parameters (that are added to url), will be treated as $data if $url is array (url that has been set in object constructor or with setURL will be used instead)
     * @param  $data - post data (will be automatically converted from array if needed)
     *
     * @return string
     */
    public function delete($url, $query_parameters = array(), $data = array())
    {
        if (is_array($url)) {
            $data = $query_parameters;
            $query_parameters = $url;
            $url = $this->baseUrl;
        }
        $this->setURL($url, $query_parameters);
        $this->setOpt(CURLOPT_CUSTOMREQUEST, 'DELETE');
        $this->setOpt(CURLOPT_POSTFIELDS, $this->buildPostData($data));
        return $this->exec();
    }

    /**
     * Build Post Data
     * Prepares post data to be able to send with curl_exec (it will prepare post to be send encoded with json if there is needed Content-Type in headers that have been set)
     * @access public
     * @param  $data
     *
     * @return array|string
     */
    public function buildPostData($data)
    {
        if (is_array($data)) {
            if (self::isArrayMultidim($data)) {
                if (isset($this->headers['Content-Type']) &&
                    preg_match($this->jsonPattern, $this->headers['Content-Type'])
                ) {
                    $data = $this->dataJsonEncode($data);
                } else {
                    $data = self::httpBuildMultiQuery($data);
                }
            } else {
                foreach ($data as $key => $value) {
                    $data[$key] = self::dataConvertToString($value);
                }

                if (isset($this->headers['Content-Type']) &&
                    preg_match($this->jsonPattern, $this->headers['Content-Type'])
                ) {
                    $data = self::dataJsonEncode($data);
                } else {
                    $data = http_build_query($data, '', '&');
                }
            }
        }
        return $data;
    }

    /**
     * Is Array Multidim
     * Checks if array has subarrays
     * @access public
     * @param  $array
     *
     * @return boolean
     */
    public static function isArrayMultidim($array)
    {
        if (!is_array($array)) {
            return false;
        }
        return (bool)count(array_filter($array, 'is_array'));
    }

    /**
     * Data Json Encode
     * Returns $data encoded to json if possible or untauched $data if not possible
     * @access public
     * @param  $data
     *
     * @return string
     */
    public function dataJsonEncode($data)
    {
        $json_str = json_encode($data);
        if (!($json_str === false)) {
            $data = $json_str;
        }
        return $data;
    }

    /**
     * Http Build Multi Query
     * Builds query string from data in array - can build even from nested arrays if needed
     * @access public
     * @param  $data
     * @param  $key - key for subarray if needed
     *
     * @return string
     */
    public static function httpBuildMultiQuery($data, $key = null)
    {
        $query = array();
        if (empty($data)) {
            return $key . '=';
        }
        $is_array_assoc = self::isArrayAssoc($data);
        foreach ($data as $k => $value) {
            if (is_string($value) || is_numeric($value)) {
                $brackets = $is_array_assoc ? '[' . $k . ']' : '[]';
                $query[] = urlencode($key === null ? $k : $key . $brackets) . '=' . rawurlencode($value);
            } elseif (is_array($value)) {
                $nested = $key === null ? $k : $key . '[' . $k . ']';
                $query[] = self::httpBuildMultiQuery($value, $nested);
            }
        }
        return implode('&', $query);
    }

    /**
     * Is Array Assoc
     * Checks if array is associative
     * @access public
     * @param  $array
     *
     * @return boolean
     */
    public static function isArrayAssoc($array)
    {
        return (bool)count(array_filter(array_keys($array), 'is_string'));
    }

    /**
     * Data Convert To String
     * Converts array with post data to string
     * @access public
     * @param  $value
     * @return array
     * @internal param $binary_data
     */
    public static function dataConvertToString($value)
    {
        // Fix "Notice: Array to string conversion" when $value in
        // curl_setopt($ch, CURLOPT_POSTFIELDS, $value) is an array
        // that contains an empty array.
        $data = $value;
        if (is_array($value) && empty($value)) {
            $data = '';
        }

        return $data;
    }

    /**
     * Exec
     * Executes curl_exec - this is called by other methods, but can be used standalone
     * @access public
     *
     * @return string
     */
    public function exec()
    {
        $this->call($this->beforeSendFunction);
        $this->rawResponse = curl_exec($this->curl);
        $this->curlErrorCode = curl_errno($this->curl);
        $this->curlErrorMessage = curl_error($this->curl);
        $this->curlError = !($this->curlErrorCode === 0);
        $this->httpStatusCode = curl_getinfo($this->curl, CURLINFO_HTTP_CODE);
        $this->httpError = in_array(floor($this->httpStatusCode / 100), array(4, 5));
        $this->error = $this->curlError || $this->httpError;
        $this->errorCode = $this->error ? ($this->curlError ? $this->curlErrorCode : $this->httpStatusCode) : 0;
        $this->requestHeaders = $this->parseRequestHeaders(curl_getinfo($this->curl, CURLINFO_HEADER_OUT));
        $this->responseHeaders = $this->parseResponseHeaders($this->rawResponseHeaders);
        list($this->response, $this->rawResponse) = $this->parseResponse($this->responseHeaders, $this->rawResponse);
        $this->httpErrorMessage = '';
        if ($this->error) {
            if (isset($this->responseHeaders['Status-Line'])) {
                $this->httpErrorMessage = $this->responseHeaders['Status-Line'];
            }
        }
        $this->errorMessage = $this->curlError ? $this->curlErrorMessage : $this->httpErrorMessage;
        if (!$this->error) {
            $this->call($this->successFunction);
        } else {
            $this->call($this->errorFunction);
        }

        $this->call($this->completeFunction);
        return $this->response;
    }

    /**
     * Call
     * Calls callback functions - parameters are dynamic (first one is function and the rest are params that will be set to this function, and first parameter is always $this [this object])
     * @access public
     */
    public function call()
    {
        $args = func_get_args();
        $function = array_shift($args);
        if (is_callable($function)) {
            array_unshift($args, $this);
            call_user_func_array($function, $args);
        }
    }

    /**
     * Parse Request Headers
     * Calls parseHeaders for request headers and adds 'Request-Line' header to the result - this is first row from parseHeader method result
     * @access private
     * @param  $raw_headers
     *
     * @return array
     */
    private function parseRequestHeaders($raw_headers)
    {
        $request_headers = new CaseInsensitiveArray();
        list($first_line, $headers) = $this->parseHeaders($raw_headers);
        $request_headers['Request-Line'] = $first_line;
        foreach ($headers as $key => $value) {
            $request_headers[$key] = $value;
        }
        return $request_headers;
    }

    /**
     * Parse Headers
     * Parses request or response headers - from string to array
     * @access private
     * @param  $raw_headers
     *
     * @return array
     */
    private function parseHeaders($raw_headers)
    {
        $raw_headers = preg_split('/\r\n/', $raw_headers, null, PREG_SPLIT_NO_EMPTY);
        $http_headers = new CaseInsensitiveArray();
        $raw_headers_count = count($raw_headers);
        for ($i = 1; $i < $raw_headers_count; $i++) {
            list($key, $value) = explode(':', $raw_headers[$i], 2);
            $key = trim($key);
            $value = trim($value);
            // Use isset() as array_key_exists() and ArrayAccess are not compatible.
            if (isset($http_headers[$key])) {
                $http_headers[$key] .= ',' . $value;
            } else {
                $http_headers[$key] = $value;
            }
        }
        return array(isset($raw_headers['0']) ? $raw_headers['0'] : '', $http_headers);
    }

    /**
     * Parse Response Headers
     * Parses response headers - excludes HTTP/ from curl response and calls parseHeaders method; Adds Status-Line header (first row from parseHeaders result)
     * @access private
     * @param  $raw_response_headers
     *
     * @return array
     */
    private function parseResponseHeaders($raw_response_headers)
    {
        $response_header_array = explode("\r\n\r\n", $raw_response_headers);
        $response_header = '';
        for ($i = count($response_header_array) - 1; $i >= 0; $i--) {
            if (stripos($response_header_array[$i], 'HTTP/') === 0) {
                $response_header = $response_header_array[$i];
                break;
            }
        }
        $response_headers = new CaseInsensitiveArray();
        list($first_line, $headers) = $this->parseHeaders($response_header);
        $response_headers['Status-Line'] = $first_line;
        foreach ($headers as $key => $value) {
            $response_headers[$key] = $value;
        }
        return $response_headers;
    }

    /**
     * Parse Response
     * Parses curl_exec response (analizes Content-Type header and decodes from json or xml if needed)
     * @access private
     * @param  $response_headers
     * @param  $raw_response
     *
     * @return array
     */
    private function parseResponse($response_headers, $raw_response)
    {
        $response = $raw_response;
        if (isset($response_headers['Content-Type'])) {
            if (preg_match($this->jsonPattern, $response_headers['Content-Type'])) {
                $json_decoder = $this->jsonDecoder;
                if (is_callable($json_decoder)) {
                    $response = call_user_func_array($json_decoder, array($response));
                }

            } elseif (preg_match($this->xmlPattern, $response_headers['Content-Type'])) {
                $xml_obj = @simplexml_load_string($response);
                if (!($xml_obj === false)) {
                    $response = $xml_obj;
                }
            }
        }
        return array($response, $raw_response);
    }

    /**
     * Download
     * Executes request (curl_exec) and saves result of it to $mixed_filename
     * @access public
     * @param  $url - url for request, will be treated as $mixed_filename if $url is array (url that has been set in object constructor or with setURL will be used instead)
     * @param  $mixed_filename - path to file where result will be stored, will be treated as callback function that will be executed after download complete if $url is array (url that has been set in object constructor or with setURL will be used instead)
     *
     * @return boolean
     */
    public function download($url, $mixed_filename)
    {
        if (is_callable($mixed_filename)) {
            $this->downloadCompleteFunction = $mixed_filename;
            $fh = tmpfile();
        } else {
            $filename = $mixed_filename;
            $fh = fopen($filename, 'wb');
        }
        $this->setOpt(CURLOPT_FILE, $fh);
        $this->get($url);
        $this->downloadComplete($fh);
        return !$this->error;
    }

    /**
     * Get
     * Executes GET request (curl_exec)
     * @access public
     * @param  $url - url for request, will be treated as $data if $url is array (url that has been set in object constructor or with setURL will be used instead)
     * @param  $data - post data (will be automatically converted from array if needed)
     *
     * @return string
     */
    public function get($url, $data = array())
    {
        if (is_array($url)) {
            $data = $url;
            $url = $this->baseUrl;
        }

        $this->unsetHeader('Content-Length');
        $this->setURL($url, $data);
        $this->setOpt(CURLOPT_CUSTOMREQUEST, 'GET');
        $this->setOpt(CURLOPT_HTTPGET, true);
        return $this->exec();
    }

    /**
     * Unset Header
     * Unsets Header
     * @access public
     * @param  $key
     */
    public function unsetHeader($key)
    {
        $this->setHeader($key, '');
        unset($this->headers[$key]);
    }

    /**
     * Set Header
     * Adds request header
     * @access public
     * @param  $key
     * @param  $value
     *
     * @return string
     */
    public function setHeader($key, $value)
    {
        $this->headers[$key] = $value;
        $headers = array();
        foreach ($this->headers as $key => $value) {
            $headers[$key] = $value;
        }
        $this->setOpt(CURLOPT_HTTPHEADER, array_map(function ($value, $key) {
            return $key . ': ' . $value;
        }, $headers, array_keys($headers)));
    }

    /**
     * Download Complete
     * Executes when download() is complete.
     * @access public
     * @param  $fh - file handler (returned by fopen function) - this is handler to file where curl response has been written
     */
    public function downloadComplete($fh)
    {
        if (!$this->error && $this->downloadCompleteFunction) {
            rewind($fh);
            $this->call($this->downloadCompleteFunction, $fh);
            $this->downloadCompleteFunction = null;
        }
        if (is_resource($fh)) {
            fclose($fh);
        }
        // Fix "PHP Notice: Use of undefined constant STDOUT" when reading the
        // PHP script from stdin. Using null causes "Warning: curl_setopt():
        // supplied argument is not a valid File-Handle resource".
        if (!defined('STDOUT')) {
            define('STDOUT', fopen('php://stdout', 'w'));
        }
        // Reset CURLOPT_FILE with STDOUT to avoid: "curl_exec(): CURLOPT_FILE
        // resource has gone away, resetting to default".
        $this->setOpt(CURLOPT_FILE, STDOUT);
        // Reset CURLOPT_RETURNTRANSFER to tell cURL to return subsequent
        // responses as the return value of curl_exec(). Without this,
        // curl_exec() will revert to returning boolean values.
        $this->setOpt(CURLOPT_RETURNTRANSFER, true);
    }

    /**
     * Options
     * Executes OPTIONS request (curl_exec)
     * @access public
     * @param  $url - url for request, will be treated as $data if $url is array (url that has been set in object constructor or with setURL will be used instead)
     * @param  $data - post data (will be automatically converted from array if needed)
     *
     * @return string
     */
    public function options($url, $data = array())
    {
        if (is_array($url)) {
            $data = $url;
            $url = $this->baseUrl;
        }
        $this->setURL($url, $data);
        $this->unsetHeader('Content-Length');
        $this->setOpt(CURLOPT_CUSTOMREQUEST, 'OPTIONS');
        return $this->exec();
    }

    /**
     * Patch
     * Executes PATCH request (curl_exec)
     * @access public
     * @param  $url - url for request, will be treated as $data if $url is array (url that has been set in object constructor or with setURL will be used instead)
     * @param  $data - post data (will be automatically converted from array if needed)
     *
     * @return string
     */
    public function patch($url, $data = array())
    {
        if (is_array($url)) {
            $data = $url;
            $url = $this->baseUrl;
        }
        $this->setURL($url);
        $this->unsetHeader('Content-Length');
        $this->setOpt(CURLOPT_CUSTOMREQUEST, 'PATCH');
        $this->setOpt(CURLOPT_POSTFIELDS, $data);
        return $this->exec();
    }

    /**
     * Post
     * Executes POST request (curl_exec)
     * @access public
     * @param  $url - url for request, will be treated as $data if $url is array (url that has been set in object constructor or with setURL will be used instead)
     * @param  $data - post data (will be automatically converted from array if needed)
     *
     * @return string
     */
    public function post($url, $data = array())
    {
        if (is_array($url)) {
            $data = $url;
            $url = $this->baseUrl;
        }

        $post_data = $this->buildPostData($data);
        if (empty($post_data)) {
            $this->unsetHeader('Content-Length');
        } else {
            $this->setHeader('Content-Length', strlen($post_data));
        }
        $this->setURL($url);
        $this->setOpt(CURLOPT_CUSTOMREQUEST, 'POST');
        $this->setOpt(CURLOPT_POST, true);
        $this->setOpt(CURLOPT_POSTFIELDS, $post_data);
        return $this->exec();
    }

    /**
     * Put
     * Executes PUT request (curl_exec)
     * @access public
     * @param  $url - url for request, will be treated as $data if $url is array (url that has been set in object constructor or with setURL will be used instead)
     * @param  $data - post data (will be automatically converted from array if needed)
     *
     * @return string
     */
    public function put($url, $data = array())
    {
        if (is_array($url)) {
            $data = $url;
            $url = $this->baseUrl;
        }
        $this->setURL($url);
        $this->setOpt(CURLOPT_CUSTOMREQUEST, 'PUT');
        $put_data = $this->buildPostData($data);
        if (empty($put_data)) {
            $this->unsetHeader('Content-Length');
        } else {
            $this->setHeader('Content-Length', strlen($put_data));
        }
        $this->setOpt(CURLOPT_POSTFIELDS, $put_data);
        return $this->exec();
    }

    /**
     * Head
     * Executes HEAD request (curl_exec)
     * @access public
     * @param  $url - url for request, will be treated as $data if $url is array (url that has been set in object constructor or with setURL will be used instead)
     * @param  $data - post data (will be automatically converted from array if needed)
     *
     * @return string
     */
    public function head($url, $data = array())
    {
        if (is_array($url)) {
            $data = $url;
            $url = $this->baseUrl;
        }
        $this->setURL($url, $data);
        $this->setOpt(CURLOPT_CUSTOMREQUEST, 'HEAD');
        $this->setOpt(CURLOPT_NOBODY, true);
        return $this->exec();
    }

    /**
     * Get Opt
     * Gets actual curl option (set by setOpt method)
     * @access public
     * @param  $option
     *
     * @return mixed
     */
    public function getOpt($option)
    {
        return $this->options[$option];
    }

    /**
     * Set Basic Authentication
     * Sets username and password for Basic Authentication method if is needed
     * @access public
     * @param  $username
     * @param  $password
     */
    public function setBasicAuthentication($username, $password = '')
    {
        $this->setOpt(CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        $this->setOpt(CURLOPT_USERPWD, $username . ':' . $password);
    }

    /**
     * Set Digest Authentication
     * Sets username and password for Digest Authentication method if is needed
     * @access public
     * @param  $username
     * @param  $password
     */
    public function setDigestAuthentication($username, $password = '')
    {
        $this->setOpt(CURLOPT_HTTPAUTH, CURLAUTH_DIGEST);
        $this->setOpt(CURLOPT_USERPWD, $username . ':' . $password);
    }

    /**
     * Set Cookie
     * Sets cookie that will be transfered
     * @access public
     * @param  $key
     * @param  $value
     */
    public function setCookie($key, $value)
    {
        $this->cookies[$key] = $value;
        $this->setOpt(CURLOPT_COOKIE, str_replace(' ', '%20', urldecode(http_build_query($this->cookies, '', '; '))));
    }

    /**
     * Set Cookie File
     * Set file from which cookies will be read.
     * @access public
     * @param  $cookie_file
     */
    public function setCookieFile($cookie_file)
    {
        $this->setOpt(CURLOPT_COOKIEFILE, $cookie_file);
    }

    /**
     * Set Cookie Jar
     * Sets file to which curl will store cookies. Also enables cookies for this curl session so cookie will be transfered for any follow-locations redirects
     * @access public
     * @param  $cookie_jar
     */
    public function setCookieJar($cookie_jar)
    {
        $this->setOpt(CURLOPT_COOKIEJAR, $cookie_jar);
    }

    /**
     * Set JSON Decoder
     * Sets JSON Decoder callback function (will be used instead of dataJsonDecode method while parsing curl response)
     * @access public
     * @param  $function
     */
    public function setJsonDecoder($function)
    {
        if (is_callable($function)) {
            $this->jsonDecoder = $function;
        }
    }

    /**
     * Set Referer
     * Sets referrer url (alias for setReferrer method)
     * @access public
     * @param  $referer
     */
    public function setReferer($referer)
    {
        $this->setReferrer($referer);
    }

    /**
     * Set Referrer
     * Sets referrer url
     * @access public
     * @param  $referrer
     */
    public function setReferrer($referrer)
    {
        $this->setOpt(CURLOPT_REFERER, $referrer);
    }

    /**
     * Verbose
     * Turns verbose mode to on or off
     * @access public
     * @param  $on
     */
    public function verbose($on = true)
    {
        $this->setOpt(CURLOPT_VERBOSE, $on);
    }

    /**
     * Header Callback
     * Function that collects response headers and stores them to $rawResponseHeaders (CURLOPT_HEADERFUNCTION functionality)
     * @access private
     * @param  $ch - unused - it is not needed here, but curl library returns it
     * @param  $header
     *
     * @return integer - size of $header parameter
     */
    private function headerCallback($ch, $header)
    {
        $this->rawResponseHeaders .= $header;
        return strlen($header);
    }

}
