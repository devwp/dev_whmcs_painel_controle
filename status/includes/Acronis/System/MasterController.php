<?php
/**
 *
 *
 * @version 1.0.3
 * @Copyright © 2002-2016 Acronis International GmbH. All rights reserved
 *
 * Controller master class. Other controllers extends it
 * DEPENDENCIES: Acronis\System\PageBuilder, \Acronis\System\ViewsConfig, \Acronis\System\ModuleConfig
 * OPTIONAL DEPENDENCIES: \Acronis\WHMCS\Lang
 */

namespace Acronis\System;

use Acronis\Helpers\AcronisParams;
use Acronis\Standard\Configs\ModuleConfig;

abstract class MasterController
{
    public static $bannedMethods = array(
        'run',
        'boot',
        'loadTpl',
        'getSelfURL',
        'getStartupURL',
        'redirect'
    );

    /**
     *
     * @var ViewsConfig $moduleConfiguration
     */
    protected $viewsConfiguration = null;
    /**
     *
     * @var ModuleConfig $moduleConfiguration
     */
    protected $moduleConfiguration = null;
    protected $params = array();

    protected $ajax = false;
    protected $errors = array();
    protected $infos = array();
    /**
     *
     * @var \Acronis\WHMCS\Lang $lang
     */
    protected $lang = null;


    /**
     *
     * @param AcronisParams $params
     * @param ViewsConfig $views_configuration
     * @param ModuleConfig $module_configuration
     * @throws \Exception
     * @internal param string $views_parent_directory - directory to search views in
     */
    public function __construct($params, ViewsConfig $views_configuration, ModuleConfig $module_configuration)
    {
        $this->params = $params;
        $this->viewsConfiguration = $views_configuration;
        $this->moduleConfiguration = $module_configuration;

        if (!is_string($this->viewsConfiguration->viewsPath) || !file_exists($this->viewsConfiguration->viewsPath)) {
            throw new \Exception('Views directory is not available (' . $this->viewsConfiguration->viewsPath . '). Please contact with service or reinstall module.');
        }

        if (isset($module_configuration->lang) &&
            is_object($module_configuration->lang) &&
            get_class($module_configuration->lang) == 'Acronis\\WHMCS\\Lang'
        ) {
            $this->lang = $module_configuration->lang;
        }
    }

    /**
     * FUNCTION run
     * Runs desired action
     * @param $controller
     * @param $action
     * @return string Returns contents of the rendered page or directly echoes its contents if this is ajax request (this is recognized by the desired action and should be stored in $ajax variable)
     * @throws \Exception
     * @internal param string $name - name of the action to run. There should be method with that name inside destination controller class
     */
    public function run($controller, $action)
    {
        try {
            $data = method_exists($this, 'boot') ? $this->boot($controller, $action) : $this->$action();
        } catch (\Exception $e) {
            if ($this->ajax) {
                ob_clean();
                echo json_encode(array('result' => 0, 'error' => '[ACTION NOT FOUND]', 'message' => $e->getMessage()));
                die();
            } else {
                throw $e; // will be interpreted by error handler if loaded any
            }
        }

        if ($this->ajax) {
            $this->runAjaxPage($data);
        } else {
            return $this->runNormalPage($data);
        }
    }

    /**
     * Directly echoes content (use if this is ajax invoke)
     * @param string $data - content
     */
    private function runAjaxPage($data)
    {
        ob_clean();
        echo $data;
        die();
    }

    /**
     * Returns rendered content
     * @param string $data - content
     * @return string Content
     */
    private function runNormalPage($data)
    {
        return $data;
    }

    /**
     * Redirects to desired url
     * @param string $url
     * @param array $get_params - Parameters to add to link (array('param1'=>1...))
     */
    public function redirect($url, $get_params)
    {
        $add = http_build_query($get_params);
        if (FALSE === stripos($url, '?')) {
            $url .= '?' . $add;
        } else {
            $url .= '&' . $add;
        }
        $url = str_replace('&amp;', '&', $url);

        ob_clean();
        header('location: ' . $url);
        die();
    }

    /**
     * Gets startup url
     * @return string
     */
    public function getStartupURL()
    {
        $url = $this->getSelfURL();
        $url = preg_replace('/\?act=[^\&]*/i', '?', $url);
        $url = preg_replace('/\&act=[^\&]*/i', '', $url);
        $url = preg_replace('/\?c=[^\&]*/i', '?', $url);
        $url = preg_replace('/\&c=[^\&]*/i', '', $url);
        $url = preg_replace('/\&amp;act=[^\&]*/i', '', $url);
        $url = preg_replace('/\&amp;c=[^\&]*/i', '', $url);

        return $url;
    }

    /**
     * Gets self url
     * @return string
     */
    public function getSelfURL()
    {
        return $_SERVER['REQUEST_URI'];
    }

    /**
     * Creates PageBuilder object for desired template file
     * @param string $tplpath - path to template (relative to views parent directory)
     * @return PageBuilder View object
     */
    protected function loadTpl($tplpath = '')
    {
        if (file_exists($this->viewsConfiguration->viewsPath . $tplpath)) {
            return new PageBuilder($this->viewsConfiguration->viewsPath . $tplpath);
        } else {
            return new PageBuilder($this->viewsConfiguration->viewsPath . 'notfound.tpl');
        }
    }

    /**
     * Renders PageBuilder object template (with global CSS's and JS's)
     * @param PageBuilder $template - template object
     * @param array $css_paths - paths to non-global css styles (used only by invoker of this method)
     * @param array $js_paths - paths to non-global js scripts (used only by invoker of this method)
     * @return PageBuilder View object
     */
    protected function renderTpl(PageBuilder $template, $css_paths = array(), $js_paths = array())
    {
        $css_paths_vals = array_keys($css_paths);
        $css_paths2 = array();
        foreach ($this->viewsConfiguration->globalCSS as $css) {
            if (!isset($css_paths_vals[$css])) {
                $css_paths2 [] = $css;
            }
        }
        $css_paths = array_merge($css_paths2, $css_paths);


        $js_paths_vals = array_keys($js_paths);
        $js_paths2 = array();
        foreach ($this->viewsConfiguration->globalJS as $js) {
            if (!isset($js_paths_vals[$js])) {
                $js_paths2 [] = $js;
            }
        }
        $js_paths = array_merge($js_paths2, $js_paths);

        $serviceid = isset($this->params['serviceid']) ? $this->params['serviceid'] : '?';
        $module_links = array
        (
            'self' => $this->getSelfURL(),
            'product_custom_button_action' => 'clientarea.php?action=productdetails&id=' . $serviceid . '&modop=custom',
            'product_clientarea_main' => 'clientarea.php?action=productdetails&id=' . $serviceid,
        );

        $template->setVar('module_links', $module_links);
        $template->setVar('module_path', $this->viewsConfiguration->browserPath);
        $template->setVar('css_paths', $css_paths);
        $template->setVar('js_paths', $js_paths);
        $template->setVar('errors', $this->errors);
        $template->setVar('infos', $this->infos);
        if ($this->lang) {
            $template->setVar('lang', $this->lang->getLangArray());
        }

        return $template->build();
    }

}