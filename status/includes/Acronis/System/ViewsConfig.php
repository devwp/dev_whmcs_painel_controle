<?php
/**
 *
 * @Copyright © 2002-2016 Acronis International GmbH. All rights reserved
 */
namespace Acronis\System;

abstract class ViewsConfig
{

    public $defaultController = '';       //default view controller (ucfirst)
    public $defaultAction = "index";  //default view action
    public $controllersNamespace = '';       //namespace prefix for all controllers

    public $viewsPath = '';       //path to directory with all views (loaded ones will be searched relative to this)
    public $browserPath = '';       //path to site for browser (should start with "modules" in most cases)

    public $globalCSS = array();  //list of CSS files that will be loaded for every page
    public $globalJS = array();  //list of JS diles that will be loaded for every page

    public $controllers = array(); // array with available controllers (keys) and their further configurations (values) if needed


    public function __construct()
    {
    }
}