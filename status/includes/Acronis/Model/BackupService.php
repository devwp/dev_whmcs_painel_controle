<?php
/**
 *
 * @version 1.0.2
 * @Copyright © 2002-2016 Acronis International GmbH. All rights reserved
 * DEPENDENCIES: Acronis\Database\Database
 *
 * @see Server
 */
namespace Acronis\Model;

use Acronis\Helpers\Arr;
use Acronis\Standard\Configs\ModuleConfig;


class BackupService
{
    const STORAGE_GB_VALUE = 1073741824;
    const UNLIMITED_TITLE = 'unlimited';
    const UNLIMITED_VALUE = -1;

    const PRICING_MODE_PRODUCTION = 1;
    const PRICING_MODE_TRIAL = 2;

    /**
     * module variables
     * @var \Acronis\Helpers\ModuleVars
     */
    private $_moduleVars;
    /**
     *
     * @var \Acronis\Standard\Configs\ModuleConfig
     */
    private $_config;

    /**
     *
     * @var int
     */
    private $_workstations = 0;

    /**
     *
     * @var int
     */
    private $_storageSize = 0;

    /**
     *
     * @var int
     */
    private $_vms = 0;

    /**
     *
     * @var int
     */
    private $_servers = 0;

    /**
     *
     * @var int
     */
    private $_o365Mailboxes = 0;

    /**
     *
     * @var int
     */
    private $_mobileDevices = 0;

    /**
     *
     * @var int
     */
    private $_backupLocations = 0;

    /**
     * Construct product
     * @param array $moduleVars
     * @param array $config
     */
    public function __construct($moduleVars, $config)
    {
        $this->_moduleVars = $moduleVars;
        $this->_config = $config;
    }

    /**
     * Get num of Workstations
     * @return int units
     */
    public function getWorkstations()
    {
        return $this->_workstations;
    }

    /**
     * Set num of Workstations
     * @param int|string $value int or 'unlimited'
     */
    public function setWorkstations($value)
    {
        $this->_workstations = $this->inputCorrectValue($value);
    }

    public function inputCorrectValue($value)
    {
        if ($value < 0 || strtolower(trim($value)) == self::UNLIMITED_TITLE) {
            return self::UNLIMITED_VALUE;
        } else {
            return $value;
        }
    }

    /**
     * Get num of storageSize in Bytes
     * @return int bytes
     */
    public function getStorageSize()
    {
        return $this->_storageSize >= 0 ? $this->_storageSize * self::STORAGE_GB_VALUE : self::UNLIMITED_VALUE;
    }

    /**
     * Set storage Size
     * @param int|string $value int or 'unlimited'
     */
    public function setStorageSize($value)
    {
        $this->_storageSize = $this->inputCorrectValue($value);
    }

    /**
     * Get num of VMs
     * @return int units
     */
    public function getVms()
    {
        return $this->_vms;
    }

    /**
     * Set Vms
     * @param int|string $value int or 'unlimited'
     */
    public function setVms($value)
    {
        $this->_vms = $this->inputCorrectValue($value);
    }

    /**
     * Get Mobile Devices
     * @return unlimited|int
     * @internal param int|string $value int or 'unlimited'
     */
    public function getMobileDevices()
    {
        return $this->_mobileDevices;
    }

    /**
     * Set Mobile Devices
     * @param int|string $value int or 'unlimited'
     */
    public function setMobileDevices($value)
    {
        $this->_mobileDevices = $this->inputCorrectValue($value);
    }

    /**
     * Get num of Office 365 Mailboxes
     * @return unlimited|int
     * @internal param int|string $value int or 'unlimited'
     */
    public function getO365Mailboxes()
    {
        return $this->_o365Mailboxes;
    }

    /**
     * Set num of Office 365 Mailboxes
     * @param int|string $value int or 'unlimited'
     */
    public function setO365Mailboxes($value)
    {
        $this->_o365Mailboxes = $this->inputCorrectValue($value);
    }

    /**
     * Get type of Backup Locations
     * @return int
     * @internal param int $value 0 or 1
     */
    public function getBackupLocations()
    {
        return $this->_backupLocations;
    }

    /**
     * Set backup locations type = cloud or cloud&local
     * @param int $value 0 or 1
     */
    public function setBackupLocations($value)
    {
        $this->_backupLocations = $this->inputCorrectValue($value);
    }

    /**
     * Get num of servers
     * @return int units
     */
    public function getServers()
    {
        return $this->_servers;
    }

    /**
     * Set num of Workstations
     * @param int|string $value int or 'unlimited'
     */
    public function setServers($value)
    {
        $this->_servers = $this->inputCorrectValue($value);
    }

    /**
     * Iterates all configurable options and try to search our module
     * config option for matching and update value
     *
     * @param array $configurableOptionsList array of WHMCS's configurable options
     */
    public function loadConfigurableOptions($configurableOptionsList)
    {
        if (is_array($configurableOptionsList)) {
            //iterates and try to match our param by name
            foreach ($configurableOptionsList as $paramName => $paramValue) {
                $this->tryToUpdateValue($paramName, $paramValue);
            }
        }
    }

    /**
     * Set specified value to detected param
     *
     * @param string $paramName name of configruable param to set
     * @param mixed $paramValue value of configurable option
     */
    public function tryToUpdateValue($paramName, $paramValue)
    {
        switch ($paramName) {
            case ModuleConfig::FRIENDLY_NAME_WORKSTATIONS:
                if ($this->_workstations != self::UNLIMITED_VALUE) {
                    $this->_workstations += $paramValue;
                }
                break;
            case ModuleConfig::FRIENDLY_NAME_STORAGE:
                if ($this->_storageSize != self::UNLIMITED_VALUE) {
                    $this->_storageSize += $paramValue;
                }
                break;
            case ModuleConfig::FRIENDLY_NAME_SERVERS:
                if ($this->_servers != self::UNLIMITED_VALUE) {
                    $this->_servers += $paramValue;
                }
                break;
            case ModuleConfig::FRIENDLY_NAME_VMS:
                if ($this->_vms != self::UNLIMITED_VALUE) {
                    $this->_vms += $paramValue;
                }
                break;
            case ModuleConfig::FRIENDLY_NAME_MOBILE_DEVICES:
                if ($this->_mobileDevices != self::UNLIMITED_VALUE) {
                    $this->_mobileDevices += $paramValue;
                }
                break;
            case ModuleConfig::FRIENDLY_NAME_O365_MAILBOXES:
                if ($this->_o365Mailboxes != self::UNLIMITED_VALUE) {
                    $this->_o365Mailboxes += $paramValue;
                }
                break;
        }
    }

    /**
     * Detect group's pricing mode (ABC) using data of product
     *
     * @param array $productData
     * @return int
     */
    public function detectPricingModeFromProductData(array $productData)
    {
        return Arr::getByPath($productData, 'paytype') === 'free'
            ? static::PRICING_MODE_TRIAL
            : static::PRICING_MODE_PRODUCTION;
    }
}
