<?php
/**
 *
 * @version 1.0.2
 * @Copyright © 2002-2016 Acronis International GmbH. All rights reserved
 * DEPENDENCIES: Acronis\Database\Database
 *
 * @see Server
 */
namespace Acronis\Model;

class Storage
{
    const UID_MASK = '/\((.+)\)$/';
    private $_moduleVars;
    private $_config;

    /**
     *
     * @param array $moduleVars
     * @param array $config
     */
    public function __construct($moduleVars, $config)
    {
        $this->_moduleVars = $moduleVars;
        $this->_config = $config;
    }

    /**
     * Returns list of storage as string
     *
     * @return string list of storages as string for options field
     */
    public function getStorageListAsString()
    {
        $storages = $this->getStorageList();
        $storageOptionString = array();
        //transform array to "StorageName (Uid)" list
        foreach ($storages as $storageUid => $storageData) {
            $storageOptionString[] = $storageData['name'] . ' (' . $storageUid . ')';
        }
        //return as list of values for dropdown input control
        //example: "fes-baas (uid..),dc-baas(uid...)"
        return implode(",", $storageOptionString);
    }

    /**
     * Returns list of storages from ABC
     * @return array array(uid => array(serverData) , ...)
     * @internal param array $moduleVars
     * @internal param array $config
     */
    public function getStorageList()
    {
        //get server data from WHMCS db
        $servers = \Acronis\WHMCS\Servers::getServerData();
        $storageList = array();

        foreach ($servers as $serverConf) {
            //load admin user for use it in decrypt function
            $adminUserName = \Acronis\WHMCS\AdminUser::getAdminUserName();
            //decrypt server password by WHMCS localApi call command 'decryptpassword'
            $results = localAPI("decryptpassword", array("password2" => $serverConf['password']), $adminUserName);

            //create API instance with specified server's credentials
            $manager = new \Acronis\ApiManagers\SystemManager($serverConf['hostname'], $this->_config, $this->_moduleVars, false);
            $manager->accountsManager->login($serverConf['username'], $results['password']);
            //load list of storages from ABC
            $storagesFromDb = $manager->accountsManager->getStorages('self');

            //transform storage list to array(uid => array()) list
            foreach ($storagesFromDb['items'] as $storage) {
                $storageList[$storage['uid']] = $storage;
            }
        }

        return $storageList;
    }

    /**
     * returns storage id by option value
     *
     * @param string $optionValue config option value, for example "fes-baas.acronis.com (correct_uid)"
     * @return bool|int
     */
    public function getStorageIdByOptionValue($optionValue)
    {
        return preg_match(self::UID_MASK, $optionValue, $matches) ? $this->getStorageIdByUid($matches[1]) : false;
    }

    /**
     * Returns a storage id by storage's UID
     *
     * @param string $uid correct storage UID
     * @return boolean  return false if there is no storage with same UID
     */
    public function getStorageIdByUid($uid)
    {
        $storageList = $this->getStorageList();
        if (isset($storageList[$uid])) {
            return $storageList[$uid]['id'];
        }
        return false;
    }

}
