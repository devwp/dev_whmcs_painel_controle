<h5>{$lang->_('Synchronization Settings')}</h5>

<form method="post" action="{'settings'|addon_url:'domain'}" id="save_settings_form">
    <input type="hidden" id="cron" value="false"/>
    <input type="hidden" id="serverID" value="{$serverid}"/>
    <input type="hidden" id="domain" value="{$domain}"/>
    <table>
        <tr>
            <td><label class="options-label" for="ResetPasswords">{$lang->_('Node Type')}</label></td>
            <td><select id="nodeType"><option>KVM</option></select></td>
            <td><label class="options-label-description">{$lang->_('Node type setting when creating account on server')}</label></td>
        </tr>
        
    </table>
    <div class="form-actions">
        <button id='save_domain_settings' class="button primary"><i class="icon-ok"> </i>{$lang->_('Update Settings')}</button>
    </div>
</form>