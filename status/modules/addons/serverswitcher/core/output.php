<?php

#ATTACH STYLES
echo '
   
<link href="'.$ASSETS_DIR.'/css/bootstrap.css" rel="stylesheet">
<link href="'.$ASSETS_DIR.'/css/bootstrap-responsive.css" rel="stylesheet">
    
<link href="'.$ASSETS_DIR.'/css/template-styles.css" rel="stylesheet">

<link href="'.$ASSETS_DIR.'/css/modulesgarden.css" rel="stylesheet">
    
<!--FONTS-->
<link href="'.$ASSETS_DIR.'/css/font-awesome.css" rel="stylesheet">

<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
  <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<!--[if IE 7]>
  <link rel="stylesheet" href="assets/css/font-awesome-ie7.css">
<![endif]-->';

echo '<div class="body" data-target=".body" data-spy="scroll" data-twttr-rendered="true" id="mg-wrapper">';


echo '
    <div id="mg-content" class="right">
    
    	<div id="top-bar" '.(WHMCS6 ? 'style="height: 84px;"' : '').'>
        
        	<div id="module-name">
            	<h2>'.$MGC->name.'</h2>';
                if(isset($MGC->menu[$CONTROLLER_NAME]['title']))
                {
                    echo '<h4>'.$MGC->menu[$CONTROLLER_NAME]['title'].'</h4>';
                }
echo '      </div>';

echo '<ul id="top-nav">';
foreach($MGC->menu as $page => $menu)
{
    if(isset($menu['submenu']))
    {
        echo '<li class="dropdown-toggle">';
        echo '<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" id="menu-'.$page.'"><i class="icon-'.$menu['icon'].'"></i>'.$menu['title'].'<i class="icon-caret-down"></i></a>';
        echo '<ul class="dropdown-menu" role="menu" aria-labelledby="menu-'.$page.'">';
        foreach($menu['submenu'] as $subpage => &$submenu)
        {
            if(isset($submenu['type']) && $submenu['type'] == 'custom')
            {
                echo '<li><a href="'.$submenu['url'].'">'.$submenu['title'].'</a></li>';
            }
            else
            {
                echo '<li><a href="'.$MODULE_URL.'&modpage='.$page.'&modsubpage='.$subpage.'">'.$submenu['title'].'</a></li>';
            }
        }
        echo '</ul>';
        echo '</li>';
    }
    else
    {
        if(isset($menu['type']) && $menu['type'] == 'custom')
        {
            echo '<li><a href="'.$menu['url'].'"><i class="icon-'.$menu['icon'].'"></i>'.$menu['title'].'</a></li>';
        }
        else
        {
            echo '<li><a href="'.$MODULE_URL.'&modpage='.$page.'"><i class="icon-'.$menu['icon'].'"></i>'.$menu['title'].'</a></li>';
        }
    }
}
echo '</ul>';

echo '
            
            <!--<div class="clear"></div>-->
        <a class="slogan nblue-box" href="http://www.modulesgarden.com" target="_blank" alt="ModulesGarden Custom Development">
            <span class="mg-logo"></span>
            <small>We are here to help you, just click!</small>
        </a>
        </div><!-- end of TOP BAR -->
        
    	<div class="inner">';


if($MESSAGES && is_array($MESSAGES))
{
    foreach($MESSAGES as $msg)
    {
        echo '<div class="alert alert-'.$msg['type'].'">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
                '.$msg['msg'].'
            </div>';
    }
}

echo $CONTENT;

echo '
        </div><!-- end of INNER -->
        <div class="overlay hide">
        </div>
    </div><!-- end of CONTENT -->

    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    
    <script src="'.$ASSETS_DIR.'/js/jquery.js"></script>
    <script src="'.$ASSETS_DIR.'/js/jquery-ui-1.9.1.custom.min.js"></script> 
    <script src="'.$ASSETS_DIR.'/js/bootstrap.js"></script>
    <script src="'.$ASSETS_DIR.'/js/modulesgarden.js"></script>
    <script src="'.$ASSETS_DIR.'/js/jquery.dataTables.min.js"></script>
        
';
        

echo '</div>';
