<?php
require_once("../../../../init.php");
//require_once __DIR__ . '/../../../init.php';
//require_once __DIR__ . '/../../../includes/gatewayfunctions.php';
require_once ('../../../../includes/invoicefunctions.php');

//LIMPA JUROS ORFAO NA TABELA mod_faturajuros
full_query("DELETE FROM mod_faturajuros WHERE invoiceid IN (SELECT id FROM tblinvoices WHERE duedate < DATE(NOW()) AND status = 'Unpaid') AND invoiceitenid NOT IN (SELECT id FROM tblinvoiceitems WHERE type IN ('faturajuros_LateFee', 'faturajuros_LateInterest'));");

$q1 = full_query("SELECT * FROM tblinvoices WHERE duedate < DATE(NOW()) AND status = 'Unpaid';");
$q2 = full_query("SELECT * FROM tbladdonmodules WHERE module = 'faturajuros_addon';");
$q3 = full_query("SELECT * FROM tblinvoiceitems WHERE id in (SELECT MAX(id) FROM tblinvoiceitems GROUP BY invoiceid, type) AND invoiceid IN (SELECT id FROM tblinvoices WHERE duedate < DATE(NOW()) AND status = 'Unpaid') AND type IN ('faturajuros_LateFee', 'faturajuros_LateInterest');");
$q4 = full_query("SELECT invoiceid, SUM(amount) as total FROM tblinvoiceitems WHERE invoiceid IN (SELECT id FROM tblinvoices WHERE duedate < DATE(NOW()) AND status = 'Unpaid') AND type NOT IN ('faturajuros_LateFee', 'faturajuros_LateInterest') GROUP BY invoiceid;");
$q5 = full_query("SELECT * FROM mod_faturajuros WHERE id in (SELECT MAX(id) FROM mod_faturajuros GROUP BY invoiceid, tipo) AND invoiceid IN (SELECT id FROM tblinvoices WHERE duedate < DATE(NOW()) AND status = 'Unpaid')  ORDER BY id DESC;");

$dconf=array();
while($fconf = mysql_fetch_array($q2)) {
	$dconf[$fconf['setting']] = $fconf['value'];
}

$ii=array();
if($q3){
	while($dii = mysql_fetch_array( $q3)) {
		//print_r($dii);exit();
		$ii[$dii['type']][$dii['invoiceid']] = $dii;
	}
}
// invoice total
$it=array();
if($q4){
	while($dit = mysql_fetch_array($q4)) {
		$it[$dit['invoiceid']] = $dit['total'];
	}
}
// multas
$lm=array();
if($q5){
	while($lmc = mysql_fetch_array($q5)) {
		$lm[$lmc['tipo']][$lmc['invoiceid']] = $lmc;
	}
}

$totalInv = 0;
while($inv =  mysql_fetch_array($q1)) {
	$totalInv++;
	if($dconf["percentFee"] > 0 || $dconf["percentInterestMonth"] > 0) $invd = getInvoceDetails($inv['id']);
	
	if($dconf["percentFee"] > 0) {
		if(array_key_exists('faturajuros_LateFee',$ii) && array_key_exists($inv['id'],$ii['faturajuros_LateFee'])){
			//full_query("UPDATE tblinvoiceitems SET description='Multa por atraso (".$dconf["percentFee"]."% de "." - ".date('d/m/Y').")', amount=".$t1." WHERE id=".$ii['faturajuros_LateFee'][$inv['id']]['id']);
			//echo("UPDATE tblinvoiceitems 'Multa por atraso (".$dconf["percentFee"]."% de "." - ".date(d/m/Y).")', amount=".$t1.", id=".$ii['faturajuros_LateFee'][$inv['id']]['id'])."\n";
		} else {
			
			$li=0;
			$t11=0;
			$inserirfatura=false;
			$valor_devido = $invd['balance2'];
			$data_multa = date('y-m-d', strtotime($invd['duedate'].'+ 1 day'));
			//$t1=calcLateFee($it[$inv['id']],$dconf["percentFee"]);
			$t11=calcLateFee($invd['balance2'],$dconf["percentFee"]);
			if(array_key_exists('multa',$lm) && array_key_exists($inv['id'],$lm['multa'])){ // CASO MULTA JÁ APLICADA ANTERIORMENTE
				$valor_devido = $lm['multa'][$inv['id']]['valor_base'];
				$data_multa = $lm['multa'][$inv['id']]['data_base'];
				$t11=calcLateFee($lm['multa'][$inv['id']]['valor_base'],$dconf["percentFee"]);
			} else {
				$inserirfatura=true;
			}
			full_query("INSERT INTO tblinvoiceitems (invoiceid,userid,type,description,amount) VALUES (".$inv['id'].",".$inv['userid'].",'faturajuros_LateFee','Multa por atraso (".$dconf["percentFee"]."% de R$ ".str_replace(".",",",$valor_devido)." - ".date('d/m/Y',strtotime($data_multa)).")', ".$t11.")");
			$li=mysql_insert_id();
			echo("INSERT INTO tblinvoiceitems (invoiceid,userid,type,description,amount) VALUES (".$inv['id'].",".$inv['userid'].",'faturajuros_LateFee','Multa por atraso (".$dconf["percentFee"]."% de R$ ".str_replace(".",",",$valor_devido)." - ".date('d/m/Y',strtotime($data_multa)).")', ".$t11.")")."\n";
			
			if($inserirfatura) {
				full_query("INSERT INTO mod_faturajuros (invoiceid,invoiceitenid,tipo,data_base,valor_base,valor_taxa,dataehora) VALUES (".$inv['id'].",".$li.",'multa','".$data_multa."',".$valor_devido.",".$t11.",NOW())");
				echo ("INSERT INTO mod_faturajuros (invoiceid,invoiceitenid,tipo,data_base,valor_base,valor_taxa,dataehora) VALUES (".$inv['id'].",".$li.",'multa','".$data_multa."',".$valor_devido.",".$t11.",NOW())")."\n";
			} else {
				full_query("UPDATE mod_faturajuros SET invoiceitenid=".$li."WHERE id=".$lm['multa'][$inv['id']]['id']);
				echo ("UPDATE mod_faturajuros SET invoiceitenid=".$li."WHERE id=".$lm['multa'][$inv['id']]['id'])."\n";
			}
		}
	} else {
		full_query("DELETE FROM tblinvoiceitems WHERE type='faturajuros_LateFee' AND invoiceid=".$inv['id']);
		echo ("DELETE 'Unpaid' tblinvoiceitems 'faturajuros_LateFee' invoiceid=".$inv['id'])."\n";
	}
	if($dconf["percentInterestMonth"] > 0) {
		$li=0;
		$ld=0;
		$t22=0;
		$inserirfatura=false;

		$ld=calcLateDays($inv['duedate']);
		$dias=($ld<=1)?'dia':'dias';
		
		$valor_devido = $invd['balance2'];
		$t22=calcLateInterest($valor_devido,$dconf["percentInterestMonth"],$ld);
		$data_juros = $inv['duedate'];//date('y-m-d');
		$update=true;
		if(array_key_exists('juros',$lm) && array_key_exists($inv['id'],$lm['juros'])){ // CASO JUROS JÁ APLICADA ANTERIORMENTE
			if ($lm['juros'][$inv['id']]['valor_base'] == $valor_devido){
					
				$valor_devido = $lm['juros'][$inv['id']]['valor_base'];
				$data_juros = $lm['juros'][$inv['id']]['data_base'];
				$ld=calcLateDays($data_juros);
				$dias=($ld<=1)?'dia':'dias';
				$t22=calcLateInterest($valor_devido,$dconf["percentInterestMonth"],$ld);
			} else {
				$inserirfatura = true;
				//$valor_devido = $valor_devido;
				//$data_juros = $lm['juros'][$inv['id']]['data_base'];
				$data_juros = $lm['juros'][$inv['id']]['lastupdate'];
				$ld=calcLateDays($data_juros);
				$dias=($ld<=1)?'dia':'dias';
				$t22=calcLateInterest($valor_devido,$dconf["percentInterestMonth"],$ld);
			}

		} else {		
			$inserirfatura = true;
		}
		
		if(array_key_exists('faturajuros_LateInterest',$ii) && array_key_exists($inv['id'],$ii['faturajuros_LateInterest']) && !$inserirfatura ){
			full_query("UPDATE tblinvoiceitems SET description='Juros por ".$ld." ".$dias." de atraso (".$dconf["percentInterestMonth"]."% de R$ ".str_replace(".",",",$valor_devido)."/mes - \"".date('d/m/Y',strtotime($data_juros))." a ".date('d/m/Y')."\")', amount=".$t22." WHERE id=".$ii['faturajuros_LateInterest'][$inv['id']]['id']);
			echo("UPDATE tblinvoiceitems SET description='Juros por ".$ld." ".$dias." de atraso (".$dconf["percentInterestMonth"]."% de R$ ".str_replace(".",",",$valor_devido)."/mes - \"".date('d/m/Y',strtotime($data_juros))." a ".date('d/m/Y')."\")', amount=".$t22." WHERE id=".$ii['faturajuros_LateInterest'][$inv['id']]['id'])."\n";
			full_query("UPDATE mod_faturajuros SET invoiceitenid=".$ii['faturajuros_LateInterest'][$inv['id']]['id']." , valor_taxa= ".$t22.", lastupdate=NOW() WHERE id=".$lm['juros'][$inv['id']]['id']);
			echo ("UPDATE mod_faturajuros SET invoiceitenid=".$ii['faturajuros_LateInterest'][$inv['id']]['id']." , valor_taxa= ".$t22.", lastupdate=NOW() WHERE id=".$lm['juros'][$inv['id']]['id'])."\n";
		} else {
			if($t22>0){
				full_query("INSERT INTO tblinvoiceitems (invoiceid,userid,type,description,amount) VALUES (".$inv['id'].",".$inv['userid'].",'faturajuros_LateInterest','Juros por ".$ld." ".$dias." de atraso (".$dconf["percentInterestMonth"]."% de R$ ".str_replace(".",",",$valor_devido)."/mes  - \"".date('d/m/Y',strtotime($data_juros))." a ".date('d/m/Y')."\")', ".$t22.")");
				$li=mysql_insert_id();
				
				echo ("INSERT INTO tblinvoiceitems (invoiceid,userid,type,description,amount) VALUES (".$inv['id'].",".$inv['userid'].",'faturajuros_LateInterest','Juros por ".$ld." ".$dias." de atraso (".$dconf["percentInterestMonth"]."% de R$ ".str_replace(".",",",$valor_devido)."/mes - \"".date('d/m/Y',strtotime($data_juros))." a ".date('d/m/Y')."\")', ".$t22.")")."\n";

				full_query("INSERT INTO mod_faturajuros (invoiceid,invoiceitenid,tipo,data_base,valor_base,valor_taxa,lastupdate,dataehora) VALUES (".$inv['id'].",".$li.",'juros','".$data_juros."',".$valor_devido.",".$t22.",NOW(),NOW())");
				echo ("INSERT INTO mod_faturajuros (invoiceid,invoiceitenid,tipo,data_base,valor_base,valor_taxa,lastupdate,dataehora) VALUES (".$inv['id'].",".$li.",'juros','".$data_juros."',".$valor_devido.",".$t22.",NOW(),NOW())")."\n";
			} else {
				echo "---- Total <= 0 ---- Não Incluiu Juros por ".$ld." ".$dias." de atraso (".$dconf["percentInterestMonth"]."% de R$ ".str_replace(".",",",$valor_devido)."/mes - \"".date('d/m/Y',strtotime($data_juros))." a ".date('d/m/Y')."\")\n";
			}
		}
		

	} else {
		full_query("DELETE FROM tblinvoiceitems WHERE type='faturajuros_LateInterest' AND invoiceid =".$inv['id']);
		echo("DELETE 'Unpaid' tblinvoiceitems 'faturajuros_LateInterest' invoiceid=".$inv['id'])."\n";
	}
	updateInvoiceTotal($inv['id']);
	echo("update invoices toatal id='".$inv['id']."';")."\n";
}
if($totalInv) {
	echo(''.$totalInv." late invoice(s) processed")."\n";
} else {
	echo("No late invoice found")."\n";
}
// Recalculate invoices

//full_query("UPDATE tblinvoices i SET subtotal=(SELECT sum(amount) FROM tblinvoiceitems WHERE invoiceid = i.id), total=((subtotal+tax+tax2)-credit) WHERE duedate < DATE(NOW()) AND status = 'Unpaid';");
//echo("UPDATE tblinvoices recalc all status = 'Unpaid';")."\n";


//Calcular dias
function calcLateDays($dueDate){
	return (strtotime(date('Y-m-d')) > strtotime(date('Y-m-d',strtotime($dueDate))))?floor((strtotime(date('Y-m-d')) - strtotime(date('Y-m-d', strtotime($dueDate))))/(60*60*24)):0; 
}

//Calcular Multa
function calcLateFee($value, $percentFee){
	return round((($value*$percentFee)/100),2);
}
//Calcular Juros
function calcLateInterest($value, $percentInterestMonth, $lateDays){
	return round((($value*(($percentInterestMonth/30)*$lateDays))/100),2);
}
function getInvoceDetails($invoiceid){

	$result = select_query("tblinvoices", "", array("id" => $invoiceid));
	$data = mysql_fetch_array($result);
	$invoiceid = $data['id'];
	if (!$invoiceid) {
		$apiresults = array("status" => "error", "message" => "Invoice ID Not Found");
		return null;
	}
	$userid = $data['userid'];
	$invoicenum = $data['invoicenum'];
	$date = $data['date'];
	$duedate = $data['duedate'];
	$datepaid = $data['datepaid'];
	$subtotal = $data['subtotal'];
	$credit = $data['credit'];
	$tax = $data['tax'];
	$tax2 = $data['tax2'];
	$total = $data['total'];
	$taxrate = $data['taxrate'];
	$taxrate2 = $data['taxrate2'];
	$status = $data['status'];
	$paymentmethod = $data['paymentmethod'];
	$notes = $data['notes'];
	$result = select_query("tblaccounts", "SUM(amountin)-SUM(amountout)", array("invoiceid" => $invoiceid));
	$data = mysql_fetch_array($result);
	$amountpaid = $data[0];
	$balance = $total - $amountpaid;
	$balance = format_as_currency($balance);
	// TIRA JUROS E MULTA DO addon faturajuros
	$result2 = full_query("SELECT SUM(amount) FROM tblinvoiceitems WHERE type IN ('faturajuros_LateInterest','faturajuros_LateFee','itaushopline_disccount_dalay') AND invoiceid=".$invoiceid);
	$data2 = mysql_fetch_array($result2);
	$amountpaid2 = $data2[0];
	$balance2 = $total - $amountpaid - $amountpaid2;
	$balance2 = format_as_currency($balance2); 
	// FIM TIRA JUROS E MULTA DO addon faturajuros
	$gatewaytype = get_query_val("tblpaymentgateways", "value", array("gateway" => $paymentmethod, "setting" => "type"));
	$ccgateway = (($gatewaytype == "CC" || $gatewaytype == "OfflineCC") ? true : false);
	$apiresults = array("result" => "success", "invoiceid" => $invoiceid, "invoicenum" => $invoicenum, "userid" => $userid, "date" => $date, "duedate" => $duedate, "datepaid" => $datepaid, "subtotal" => $subtotal, "credit" => $credit, "tax" => $tax, "tax2" => $tax2, "total" => $total, "balance" => $balance, "balance2" => $balance2, "taxrate" => $taxrate, "taxrate2" => $taxrate2, "status" => $status, "paymentmethod" => $paymentmethod, "notes" => $notes, "ccgateway" => $ccgateway);
	$result = select_query("tblinvoiceitems", "", array("invoiceid" => $invoiceid));
	while ($data = mysql_fetch_array($result)) {
		$apiresults['items']['item'][] = array("id" => $data['id'], "type" => $data['type'], "relid" => $data['relid'], "description" => $data['description'], "amount" => $data['amount'], "taxed" => $data['taxed']);
	}
	$apiresults['transactions'] = "";
	$result = select_query("tblaccounts", "", array("invoiceid" => $invoiceid));
	while ($data = mysql_fetch_assoc($result)) {
		$apiresults['transactions']['transaction'][] = $data;
	}
	return $apiresults;

}
?>