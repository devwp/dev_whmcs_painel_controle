$.fn.bootstrapSwitch.defaults.handleWidth = 70;    

function postAJAX(url, data, calltype, container, callbackfunc, async)
{
    var page    = url.substr(0, url.indexOf("|") );
    var action  = url.substr(url.indexOf("|")+1, url.length);
    
    callbackfunc = typeof callbackfunc !== 'undefined' ?  callbackfunc : 0;
    calltype = typeof calltype !== 'undefined' ?  calltype : 0;
    async = typeof async !== 'undefined' ?  async : true;
    
    if(calltype)
    {
        calltype = calltype + "=1";
    }
    
    return $.ajax({
        type: "POST",
        url: "addonmodules.php?module=AdvancedBilling&mg-page="+page+"&"+calltype+"&mg-action="+action,
        data: data,
        async: async,
        success: function(result){
            //var data = $.parseJSON(result);
            $("#"+container+"").html(result.alert);    

            if(callbackfunc)
            {
                callbackfunc(result.result);
            }  
        },
        dataType: 'json',
    });    
}