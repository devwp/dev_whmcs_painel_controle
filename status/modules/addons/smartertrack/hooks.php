<?php
/*
*************************************************************

      *** SmarterTrack Help Desk Module Hooks ***

These are the SmarterTrack Module Hooks.

Hooks are used to ovverride certain functionalities of WHMCS.

For more info, please refer to the SmarterTools Forums
 @   http://forums.smartertools.com

*************************************************************
*/
require_once('smartertrack_functions.php');

if (!defined("WHMCS"))
	die("This file cannot be accessed directly");


//Called when the the password is changed within WHMCS for the user account
function smartertrack_changepassword($vars)
{
	$userid = $vars['userid'];
	$password = $vars['password'];

	//	Get Track Settings
	$st_settings = smartertrack_settings();
	$st_ws_url = $st_settings['url']."/services2/svcOrganization.asmx?WSDL";
	$st_admin = $st_settings['admin'];
	$st_adminpassword = $st_settings['password'];

	$client = smartertrack_getClient($userid, $st_settings);
	if($client)
	{
		if($st_settings['md5hashenabled'] == true)
		{
			// Get MD5 Hash Password
			$user = select_query('tblclients', 'password', array('id' => $userid));
			$user = mysql_fetch_array($user,MYSQL_ASSOC);
			$password = $user['password'];
		}
		//	Call Edit User WS
		//SetUsersProperties(string authUserName, string authPassword, int userId, string[] userPropertyValues)
		$params = array( "authUserName" => $st_admin,
            "authPassword" => $st_adminpassword,
			"userId" => $client['st_userid'],
			"userPropertyValues" => array("password=".$password)
				);

		$edituserresult = smartertrack_webServiceCall($st_ws_url, $params, "SetUsersProperties");

		if($edituserresult['SetUsersPropertiesResult']['Result'])
		{
			if($st_settings['md5hashenabled'] == false)
			{
				$password = encrypt($password);
			}

			//  update whmcs db
			$table = "smartertrack_whmcs_users";
			$update = array("st_password" => $password);
			$where = array("whm_userid" => $userid, "st_userid" => $client['st_userid']);
			update_query($table,$update,$where);
		}
	}
}

//called to edit user within WHMCS - updating users through web services.
function smartertrack_edituser($vars)
{
	$newemail = $vars['email'];
	$olddata = $vars['olddata'];
	$whmuserid = $vars['userid'];

	//	Get Track Settings
	$st_settings = smartertrack_settings();
	$client = smartertrack_getClient($whmuserid, $st_settings);
	if($client)
	{
		if($newemail && $olddata['email'] != $newemail)
		{
			$st_ws_url = $st_settings['url']."/services2/svcOrganization.asmx?WSDL";
			$st_admin = $st_settings['admin'];
			$st_adminpassword = $st_settings['password'];

			//	Call Edit User WS
			$params = array( "authUserName" => $st_admin,
				"authPassword" => $st_adminpassword,
				"userId" => $client['st_userid'],
				"userPropertyValues" => array("email=".$newemail, "username=".$newemail)
			);

			$edituserresult = smartertrack_webServiceCall($st_ws_url, $params, "SetUsersProperties");

			if($edituserresult['SetUsersPropertiesResult']['Result'])
			{
				//  update whmcs db
				$table = "smartertrack_whmcs_users";
				$update = array("st_email" => $newemail, "st_username" => $newemail);
				$where = array("whm_userid" => $whmuserid, "st_userid" => $client['st_userid']);
				update_query($table,$update,$where);
			}
           else
            {
                //update WHMCS DB with old data - reverts save
                $table = "tblclients";
                $update = array("email" => $olddata['email']);
                $where = array("id" => $whmuserid);
                update_query($table, $update ,$where);

                if(basename($_SERVER['SCRIPT_NAME']) == 'clientarea.php')
                {
                    global $smarty;
                    global $errormessage;
                    $template = $smarty->get_template_vars('template');

                    $errormessage = smartertrack_errorMessage("Unable to save user changes.", $edituserresult['SetUsersPropertiesResult']['Message'], $st_settings);
                    $smarty->assign_by_ref('errormessage', $errormessage);
                    $smarty->display($template.'/header.tpl');
                    $smarty->display('../modules/addons/smartertrack/templates/st_errorpage.tpl');
                    $smarty->display($template.'/footer.tpl');

                    exit;
                }
                else
                {
                    global $errormessage;
                    $errormessage.=smartertrack_errorMessage("<li>Unable to save user changes.</li>", $edituserresult['SetUsersPropertiesResult']['Message'], $st_settings);

                    exit;
                }

            }

        }
	}
}



//Called to add user to SmarterTrack based off of information added in WHMCS
function smartertrack_adduser($vars)
{
	//	Get Track Settings
	$st_settings = smartertrack_settings();
	$st_ws_url = $st_settings['url']."/services2/svcOrganization.asmx?WSDL";
	$st_admin = $st_settings['admin'];
	$st_adminpassword = $st_settings['password'];

	$firstname = $vars['firstname'];
	$lastname = $vars['lastname'];
	$st_email = $vars['email'];
	$password = $vars['password'];
	$userid = $vars['userid'];

	if($st_settings['md5hashenabled'] == true)
	{
		// Get MD5 Hash Password
		$user = select_query('tblclients', 'password', array('id' => $userid));
		$user = mysql_fetch_array($user,MYSQL_ASSOC);
		$password = $user['password'];
	}

	//	Create Track Username
	$trackusername = $st_email;

	$params = array( "authUserName" => $st_admin,
                "authPassword" => $st_adminpassword,
				"username" => $trackusername,
				"password" => $password,
                "email" => $st_email,
                "isEmailVerified" => true,
				"displayName" => $firstname." ".$lastname
				);

	//	Call Create User Web Service

	$returnArray = smartertrack_webServiceCall($st_ws_url, $params, "CreateUser2");

	if(!$returnArray["CreateUser2Result"]["Result"])
	{
		//global $smarty;
		//$template = $smarty->get_template_vars('template');
		//$st_errormessage = "There was an error while creating a user within SmarterTrack".$returnArray["CreateUserResult"]["Message"];
		//$smarty->assign_by_ref('errormessage', $st_errormessage);
		//$smarty->display($template.'/header.tpl');
		///$smarty->display('../modules/addons/smartertrack/templates/st_errorpage.tpl');
		//$smarty->display($template.'/footer.tpl');
		//exit;
	}
	else
	{
        //Set returned UserId and apply Everyone and Verified User roles for users
        $rtnUserId = $returnArray["CreateUser2Result"]["UserID"];

        $paramsForEveryoneRole = array(
            "authUserName" => $st_admin,
            "authPassword" => $st_adminpassword,
            "userID" => $rtnUserId,
            "roleID" => 6
        );

        $paramsForRegisteredUserRole = array(
            "authUserName" => $st_admin,
            "authPassword" => $st_adminpassword,
            "userID" => $rtnUserId,
            "roleID" => 1
        );

        $st_ws_url = $st_settings['url']."/services2/svcOrganization.asmx?WSDL";
        smartertrack_webServiceCall($st_ws_url,$paramsForEveryoneRole,"AddRoleToUser");
        smartertrack_webServiceCall($st_ws_url,$paramsForRegisteredUserRole,"AddRoleToUser");

		$userid = "userid";
		$username = "username";
		//  Get the ID for the user now  // once we get new install up
		$propparamas = array( "authUserName"=> $st_admin,
				"authPassword"=> $st_adminpassword,
				"username" => $st_email,
				"password" => $password,
				"requestedValues" => array( $userid, $username));

		$returnprops = smartertrack_webServiceCall($st_ws_url, $propparamas, "GetUsersPropertiesbyUsername");

		if($returnprops["GetUsersPropertiesbyUsernameResult"]["Result"])
		{
			$equals = "=";
			foreach($returnprops["GetUsersPropertiesbyUsernameResult"]["RequestResults"]['string'] as $value)
			{
				$thekey = strstr($value, $equals, true);
				if($thekey === "userid")
				{
					$newstid = trim(strstr($value, $equals), $equals);
				}

			}
			if($st_settings['md5hashenabled'] == false)
			{
				$password = encrypt($password);
			}
			$table = "smartertrack_whmcs_users";
			$values = array("st_userid" => $newstid, "st_username" => $st_email,
					"st_password" => $password, "whm_userid" => $vars['userid'],
					"st_email" => $st_email);
			$queryresult = insert_query($table,$values);
		}
	}
}

function smartertrack_deleteuser($vars)
{
	//	Get Track Settings
	$st_settings = smartertrack_settings();
	$st_ws_url = $st_settings['url']."/services2/svcOrganization.asmx?WSDL";
	$st_admin = $st_settings['admin'];
	$st_adminpassword = $st_settings['password'];

	$whmuserid = $vars['userid'];
	$client = smartertrack_getClient($whmuserid, $st_settings);
	if($client)
	{
		$params = array( "authUserName"=> $st_admin,
                "authPassword"=> $st_adminpassword,
				"userId" => $client['st_userid']
				);

		//	Delete User From Track
		$wscallresult = smartertrack_webServiceCall($st_ws_url, $params, "DeleteUser");
	}
}

//Used to redirect traffic to different areas within the client area.  The first option would be to redriect over to submitting support ticket
function smartertrack_redirect()
{
	if (basename($_SERVER['SCRIPT_NAME']) == 'submitticket.php')
	{
		header('Location: supporttickets.php?step=1');
	}
	else if (basename($_SERVER['SCRIPT_NAME']) == 'contact.php')
	{
		$st_settings = smartertrack_settings();
		if($st_settings['redirectcontact'])
		{
			header('Location: supporttickets.php?step=2&deptname='.$st_settings['contactdefaultdepartment']);
		}
	}
	else if (basename($_SERVER['SCRIPT_NAME']) == 'knowledgebase.php')
	{
		$st_settings = smartertrack_settings();
		if($st_settings['overridekb'])
		{
			$st_settings = smartertrack_settings();
			//header('Location: '.$st_settings['url']."/KB/search.aspx");
			header('Location: '.$st_settings['url']."/kb/root.aspx");
		}
	}
}

function smartertrack_choose_department()
{
	if (basename($_SERVER['SCRIPT_NAME']) == 'supporttickets.php' && $_GET['step'] == '1')
	{
		if (!$_SESSION['uid'])
		{
			header('Location: login.php');
			exit;
		}
		$st_settings = smartertrack_settings();
		global $smarty;
		$template = $smarty->get_template_vars('template');
		$client = smartertrack_getClient($_SESSION['uid'], $st_settings);
		if(!$client)
		{
			$errormessage = smartertrack_errorMessage("The User you are currently logged in with is not Authenticated with SmarterTrack.", null, $st_settings);
			$smarty->assign_by_ref('errormessage', $errormessage);
			$smarty->display($template.'/header.tpl');
			$smarty->display('../modules/addons/smartertrack/templates/st_errorpage.tpl');
			$smarty->display($template.'/footer.tpl');
			exit;
		}

		try
		{
						$departments = smartertrack_GetDeptsForUserTicketSubmission($st_settings);
            $deptsToShow = array();
            //	Get Available Departments

            $st_settings = smartertrack_settings();
            $dptString = $st_settings['deptsToHide'];

            $indDepts = array_map('trim',explode(",",$dptString));

            foreach($departments as $key => $value)
            {
                if(!in_array($value['name'], $indDepts))
                {
                    array_push($deptsToShow,$value);
                }
            }
            $departments = $deptsToShow;
			
		if($departments)
		{
			$step = 'step1';
			$smarty->assign_by_ref('departments', $departments);
			$smarty->assign_by_ref('step', $step);
			$smarty->display($template.'/header.tpl');
			$smarty->display('../modules/addons/smartertrack/templates/st_ticketprocess_step1.tpl');
			$smarty->display($template.'/footer.tpl');
			exit;
		}
		else
		{
			$errormessage = smartertrack_errorMessage("There was an error retrieving the Departments", null, $st_settings);
			$smarty->assign_by_ref('errormessage', $errormessage);
			$smarty->display($template.'/header.tpl');
			$smarty->display('../modules/addons/smartertrack/templates/st_errorpage.tpl');
			$smarty->display($template.'/footer.tpl');
			exit;
		}
		}
		catch(Exception $e)
		{
			//	Redirect to error page
			$st_errormessage = "An Exception was thrown with the following error message: ".$e;
			$st_errormessage .= "<br><br>Please report the error details to your service provider at:<br>".$st_settings['helpphone'];
			$smarty->assign_by_ref('errormessage', $st_errormessage);
			$smarty->display($template.'/header.tpl');
			$smarty->display('../modules/addons/smartertrack/templates/st_errorpage.tpl');
			$smarty->display($template.'/footer.tpl');
			exit;
		}
    }
}

function smartertrack_ticket_submitform()
{
	if (basename($_SERVER['SCRIPT_NAME']) == 'supporttickets.php' && $_GET['step'] == '2')
	{
		if (!$_SESSION['uid'] && !$_GET['deptname'] && !$_POST)
		{
			header('Location: login.php');
			exit;
		}
		global $smarty;
		$template = $smarty->get_template_vars('template');
		//	Get Track Settings
		$st_settings = smartertrack_settings();
		$client = smartertrack_getClient($_SESSION['uid'], $st_settings);
		if(!$client && !$_GET['deptname'] && !$_POST)
		{
			$errormessage = smartertrack_errorMessage("The User you are currently logged in with is not Authenticated with SmarterTrack.", null, $st_settings);
			$smarty->assign_by_ref('errormessage', $errormessage);
			$smarty->display($template.'/header.tpl');
			$smarty->display('../modules/addons/smartertrack/templates/st_errorpage.tpl');
			$smarty->display($template.'/footer.tpl');
			exit;
		}
		try
		{

		$st_ws_url = $st_settings['url']."/services2/svcOrganization.asmx?WSDL";
		$st_admin = $st_settings['admin'];
		$st_adminpassword = $st_settings['password'];

		if($_POST['deptname'] || $_GET['deptname'])
		{
			$departments = smartertrack_getDepartments($st_settings);
		}
		else
		{
			$departments = smartertrack_GetDeptsForUserTicketSubmission($st_settings);
		}
		if($_GET['deptname'])
		{
			foreach($departments as $key => $value)
			{
				if($value['name'] == $_GET['deptname'])
				{
					$deptid = $value['deptid'];
					break;
				}
			}
		}
		else if($_GET['deptid'])
		{
			$deptid = $_GET['deptid'];
		}
		else if($_POST['deptid'])
		{
			$deptid = $_POST['deptid'];
		}
		else if($_POST['deptname'])
		{
			foreach($departments as $key => $value)
			{
				if($value['name'] == $_POST['deptname'])
				{
					$deptid = $value['deptid'];
					break;
				}
			}
		}
		if(!$deptid)
		{
			//	Error Page
			$st_errormessage = smartertrack_errorMessage("There was an error retrieving the default department for pre-sales inquiry",null, $st_settings);
			$smarty->assign_by_ref('errormessage', $st_errormessage);
			$smarty->display($template.'/header.tpl');
			$smarty->display('../modules/addons/smartertrack/templates/st_errorpage.tpl');
			$smarty->display($template.'/footer.tpl');
			exit;
		}

		//	Get All Custom Fields  GetAllCustomFields
		$params = array( "authUserName"=> $st_admin,
					"authPassword"=> $st_adminpassword
					);
		$allcfresult = smartertrack_webServiceCall($st_ws_url, $params, "GetAllCustomFields");
		if(!$allcfresult['GetAllCustomFieldsResult']['Result'])
		{
			//	Redirect to error page
			$st_errormessage = smartertrack_errorMessage("There was an error retrieving all custom fields for SmarterTrack", $allcfresult['GetAllCustomFieldsResult']['Message'], $st_settings);
			$smarty->assign_by_ref('errormessage', $st_errormessage);
			$smarty->display($template.'/header.tpl');
			$smarty->display('../modules/addons/smartertrack/templates/st_errorpage.tpl');
			$smarty->display($template.'/footer.tpl');
			exit;
		}
		else
		{
			$allcfarray = array();
			if(is_array($allcfresult["GetAllCustomFieldsResult"]["CustomFields"]["CustomFieldInfo"][0]))
			{
				$allcfarray = $allcfresult["GetAllCustomFieldsResult"]["CustomFields"]["CustomFieldInfo"];
			}
			else
			{
				$allcfarray = $allcfresult["GetAllCustomFieldsResult"]["CustomFields"];
			}
		}

		//	CUSTOM FIELDS GetCustomFieldControls
		$st_ws_url = $st_settings['url']."/services2/svctickets.asmx?WSDL";
		$params = array( "authUserName"=> $st_admin,
					"authPassword"=> $st_adminpassword,
					"departmentId" => $deptid
					);
		$customfieldresult = smartertrack_webServiceCall($st_ws_url, $params, "GetCustomFieldControls");
		if(!$customfieldresult['GetCustomFieldControlsResult']['Result'])
		{
			foreach($departments as $key => $value)
			{
				if($deptid = $value['deptid'])
				{
					$departmentname = $value['name'];
				}
			}
			//	Redirect to error page
			$st_errormessage = smartertrack_errorMessage("There was an error Extracting the Custom Field Values for the following Department ".$departmentname, $customfieldresult['GetCustomFieldControlsResult']['Message'], $st_settings );
			$smarty->assign_by_ref('errormessage', $st_errormessage);
			$smarty->display($template.'/header.tpl');
			$smarty->display('../modules/addons/smartertrack/templates/st_errorpage.tpl');
			$smarty->display($template.'/footer.tpl');
			exit;
		}
		else
		{
			$customfieldarray = array();
			if(is_array($customfieldresult["GetCustomFieldControlsResult"]["CustomFields"]["CustomFieldInfo"][0]))
			{
				$customfieldarray = $customfieldresult["GetCustomFieldControlsResult"]["CustomFields"]["CustomFieldInfo"];
			}
			else
			{
				$customfieldarray = $customfieldresult["GetCustomFieldControlsResult"]["CustomFields"];
			}

			//  Assign new array for all of the CF with the info we need
			$customfields = array();
			foreach($allcfarray as $key => $value)
			{
				foreach($customfieldarray as $key2 => $value2)
				{
					if($value['ID'] == $value2['CustomFieldId'])
					{
						if($value2['DisplayName'] == "Product/Domain" && $_SESSION['uid'] && $value2['IsVisibleToUsers'])
						{
							// Get Domains
							$administrator = select_query('tbladmins');
							$administrator = mysql_fetch_array($administrator, MYSQL_ASSOC);
							$command = "getclientsproducts";
							$values["clientid"] = $_SESSION['uid'];
							$results = localAPI($command,$values,$administrator['id']);
							if($results['products'])
							{
								$options = array("string" => array());
								if($value['DefaultValue'])
								{
									array_push($options['string'], $value['DefaultValue']);
								}
								foreach($results['products']['product'] as $key3 => $value3)
								{
									array_push($options['string'], $value3['name']." / ".$value3['domain']);
								}
								$cfdata = array( "ID" => $value['ID'], "DisplayName" => $value2['DisplayName'],
										"IsVisibleToUsers" => $value2['IsVisibleToUsers'], "IsRequiredByUsers" => $value2['IsRequiredByUsers'], "DataType" => "Choice", "DefaultValue" => $value['DefaultValue'], "Value" => '', "Options" => $options);
								array_push($customfields, $cfdata);
							}
						}
						else if($value2['DisplayName'] == "Authenticated")
						{
							if($_SESSION['uid'])
							{
								$cfdata = array( "ID" => $value['ID'], "DisplayName" => $value2['DisplayName'],
										"IsVisibleToUsers" => false, "IsRequiredByUsers" => false, "DataType" => "Boolean", "DefaultValue" => $value['DefaultValue'], "Value" => 'true', "Options" => $value['ValueOptions']);
								array_push($customfields, $cfdata);
							}
							else
							{
								$cfdata = array( "ID" => $value['ID'], "DisplayName" => $value2['DisplayName'],
										"IsVisibleToUsers" => false, "IsRequiredByUsers" => false, "DataType" => "Boolean", "DefaultValue" => $value['DefaultValue'], "Value" => 'false', "Options" => $value['ValueOptions']);
								array_push($customfields, $cfdata);
							}
						}
						else if($value2['DisplayName'] == "Domain" && $_SESSION['uid'] && $value2['IsVisibleToUsers'])
						{
							// Get Domains
							$administrator = select_query('tbladmins');
							$administrator = mysql_fetch_array($administrator, MYSQL_ASSOC);
							$command = "getclientsdomains";
							$values["clientid"] = $_SESSION['uid'];
							$results = localAPI($command,$values,$administrator['id']);
							if($results['domains'])
							{
								$options = array("string" => array());
								if($value['DefaultValue'])
								{
									array_push($options['string'], $value['DefaultValue']);
								}
								foreach($results['domains']['domain'] as $key4 => $value4)
								{
									array_push($options['string'], $value3['domainname']);
								}
								$cfdata = array( "ID" => $value['ID'], "DisplayName" => $value2['DisplayName'],
										"IsVisibleToUsers" => $value2['IsVisibleToUsers'], "IsRequiredByUsers" => $value2['IsRequiredByUsers'], "DataType" => "Choice", "DefaultValue" => $value['DefaultValue'], "Value" => '', "Options" => $options);
								array_push($customfields, $cfdata);
							}
						}
						else if($value2['DisplayName'] == "Display Name" && $_SESSION['uid'])
						{
							$result = mysql_query("SELECT firstname,lastname FROM tblclients WHERE id=".(int)$_SESSION['uid']);
							if($result)
							{
								$data = mysql_fetch_array($result);
								$name = $data['firstname']." ".$data['lastname'];
							}
							else
							{
								$name = $client['st_username'];
							}
							$cfdata = array( "ID" => $value['ID'], "DisplayName" => $value2['DisplayName'],
										"IsVisibleToUsers" => $value2['IsVisibleToUsers'], "IsRequiredByUsers" => $value2['IsRequiredByUsers'], "DataType" => $value['DataType'], "DefaultValue" => $name, "Value" => $name, "Options" => $value['ValueOptions']);
							array_push($customfields, $cfdata);
						}
						else if($value2['DisplayName'] == "Email" && $_SESSION['uid'])
						{
							$cfdata = array( "ID" => $value['ID'], "DisplayName" => $value2['DisplayName'],
										"IsVisibleToUsers" => $value2['IsVisibleToUsers'], "IsRequiredByUsers" => $value2['IsRequiredByUsers'], "DataType" => $value['DataType'], "DefaultValue" => $client['st_email'], "Value" => $client['st_email'], "Options" => $value['ValueOptions']);
							array_push($customfields, $cfdata);
						}
						//else if($value2['DisplayName'] == "Web Server" && $_SESSION['uid'] && $value2['IsVisibleToUsers'])
						//{
							//	Get WEb Server
						//	$administrator = select_query('tbladmins');
						//	$administrator = mysql_fetch_array($administrator, MYSQL_ASSOC);
					//		$command = "getclientsdomains";
					//		$values["clientid"] = $_SESSION['uid'];
					//		$results = localAPI($command,$values,$administrator['id']);
					//	}
						//else if($value2['DisplayName'] == "Mail Server" && $_SESSION['uid']  && $value2['IsVisibleToUsers'])
						//{
							//	Get Mail Server
						//	$administrator = select_query('tbladmins');
						//	$administrator = mysql_fetch_array($administrator, MYSQL_ASSOC);
						//	$command = "getclientsdomains";
						//	$values["clientid"] = $_SESSION['uid'];
						//	$results = localAPI($command,$values,$administrator['id']);
						//}
                        else if($value2['DisplayName'] == "@Email" && $value2['IsVisibleToUsers']){
                            $cfdata = array( "ID" => $value['ID'], "DisplayName" => "Email",
                                "IsVisibleToUsers" => $value2['IsVisibleToUsers'], "IsRequiredByUsers" => $value2['IsRequiredByUsers'], "DataType" => $value['DataType'], "DefaultValue" => $value['DefaultValue'], "Value" => '', "Options" => $value['ValueOptions']);
                            array_push($customfields, $cfdata);
                        }

                        else if($value2['DisplayName'] == "@UserDisplayName" && $value2['IsVisibleToUsers']){
                            $cfdata = array( "ID" => $value['ID'], "DisplayName" => "User Display Name",
                                "IsVisibleToUsers" => $value2['IsVisibleToUsers'], "IsRequiredByUsers" => $value2['IsRequiredByUsers'], "DataType" => $value['DataType'], "DefaultValue" => $value['DefaultValue'], "Value" => '', "Options" => $value['ValueOptions']);
                            array_push($customfields, $cfdata);
                        }

						else if($value2['IsVisibleToUsers'])
						{
							$cfdata = array( "ID" => $value['ID'], "DisplayName" => $value2['DisplayName'],
										"IsVisibleToUsers" => $value2['IsVisibleToUsers'], "IsRequiredByUsers" => $value2['IsRequiredByUsers'], "DataType" => $value['DataType'], "DefaultValue" => $value['DefaultValue'], "Value" => '', "Options" => $value['ValueOptions']);
							array_push($customfields, $cfdata);
						}
					}
				}
			}
		}

		//	Validate the Custom Fields and Grab their values.
		if($_POST)
		{
			if(!$_POST['subject'] || !$_POST['message'])
			{
				$throwcferror = "Please fill out all required fields and re-try submitting your ticket.  first";
			}
			$customfields2 = array();
			foreach($customfields as $key => $value)
			{
				if($value['IsVisibleToUsers'])
				{
					$value['Value'] = $_POST[$value['ID'].'_value'];
					if($value['IsRequiredByUsers'] == true && !$_POST[$value['ID'].'_value'])
					{
						$throwcferror = "Please fill out all required fields and re-try submitting your ticket.  ".$value["DisplayName"];
					}
				}
				//else
			//	{
				//	$cfvalue = array( 'ID' => $value, 'Value' => $_POST[$value['ID'].'_value']);
				//}
				array_push($customfields2, $value);

			}
		}


		if($_POST && !$throwcferror)
		{
			//	Send Ticket to Track
			if($client || $_POST['deptname'])
			{
				if($client)
				{
					$fromaddress = $client['st_email'];
				}
				else
				{
					$fromaddress = $_POST['email'];
				}
				$params = array( "authUserName"=> $st_admin,
					"authPassword"=> $st_adminpassword,
					"departmentId" => $deptid,
					"fromAddress" => $fromaddress,
					"subject" => $_POST['subject'],
					"body" => $_POST['message'],
					"isHtml" => false,
					"sendAutoResponder" => true
				);

				$createticketreturn = smartertrack_webServiceCall($st_ws_url, $params, "CreateTicketFromCustomer");
				if($createticketreturn["CreateTicketFromCustomerResult"]["Result"])
				{
					if($_POST['deptname'])
					{
						$notemessage = "This Ticket Was Generated Through WHMCS Via Pre-Sales Inquiry.";
					}
					else
					{
						$notemessage = "This Ticket Was Generated Through WHMCS";
					}
					//	Add Ticket Note that says its from WHMCS AddTicketNote
					$params = array( "authUserName"=> $st_admin,
						"authPassword" => $st_adminpassword,
						"ticketNumber" => $createticketreturn["CreateTicketFromCustomerResult"]["RequestResult"],
						"messageType" => "note",
						"notetext" => $notemessage
						);

					$addticketnoteresult = smartertrack_webServiceCall($st_ws_url, $params, "AddTicketNote");

					//	Add Custom Field Values if Necessary
					if($customfields2)
					{
						//	Create Custom Fields String to submit to WS
						$cfstringarray = array();
						foreach($customfields2 as $key => $value)
						{
							array_push($cfstringarray, $value['DisplayName'].'='.$value['Value']);
						}

						//	Add Custom Fields To ticket  SetTicketCustomFields
						$params = array( "authUserName"=> $st_admin,
								"authPassword" => $st_adminpassword,
								"ticketNumber" => $createticketreturn["CreateTicketFromCustomerResult"]["RequestResult"],
								"customFieldValues" => $cfstringarray
								);

						$addcfresult = smartertrack_webServiceCall($st_ws_url, $params, "SetTicketCustomFields");
						if($addcfresult['SetTicketCustomFieldsResult']['Result'])
						{
							//	Redirect to All Tickets with success message ( also below, because we submit ticket before CF, //  can change in a future release).....
							if($_POST['deptname'])
							{
								header('Location: index.php');
							}
							else
							{
								header('Location: supporttickets.php?tsubmitsuccess=true&tid='.$createticketreturn["CreateTicketFromCustomerResult"]["RequestResult"]);
							}
							exit;
						}
						else
						{
							//	Need to think about throwing error here and deleting the ticket that was created..
							//	Review this with team.
							$notemessage = "There was an error while trying to add custom fields to this ticket.  Error: ".$addcfresult['SetTicketCustomFieldsResult']['Message'].
										"  .  The Following is the custom field array: ";
							foreach($cfstringarray as $key => $value)
							{
								$notemessage .= $value['DisplayName']." = ".$value['DataContent'].", ";
							}

							//	Add Ticket Note that says its from WHMCS AddTicketNote
							$params = array( "authUserName"=> $st_admin,
								"authPassword" => $st_adminpassword,
								"ticketNumber" => $createticketreturn["CreateTicketFromCustomerResult"]["RequestResult"],
								"messageType" => "note",
								"notetext" => $notemessage
							);

							$addticketnoteresult = smartertrack_webServiceCall($st_ws_url, $params, "AddTicketNote");
						}

					}

					//	Redirect to All Tickets with success message
					header('Location: supporttickets.php?tsubmitsuccess=true&tid='.$createticketreturn["CreateTicketFromCustomerResult"]["RequestResult"]);
					exit;
				}
				else
				{
					//	Redirect to error page
					$st_errormessage = smartertrack_errorMessage("There was an error Submiting a Ticket", $createticketreturn["CreateTicketFromCustomerResult"]["Message"], $st_settings);
					$smarty->assign_by_ref('errormessage', $st_errormessage);
					$smarty->display($template.'/header.tpl');
					$smarty->display('../modules/addons/smartertrack/templates/st_errorpage.tpl');
					$smarty->display($template.'/footer.tpl');
					exit;
				}


			}
		}

		$result = mysql_query("SELECT firstname,lastname FROM tblclients WHERE id=".(int)$_SESSION['uid']);
		if($result)
		{
			$data = mysql_fetch_array($result);
			$name = $data['firstname']." ".$data['lastname'];
		}
		else
		{
			$name = $client['st_username'];
		}
		$email = $client['st_email'];
		$smarty->assign_by_ref('name', $name);
		$smarty->assign_by_ref('email', $email);


		if($_GET['deptname'])
		{
			$smarty->assign_by_ref('deptname', $_GET['deptname']);
		}
		else if($_POST['deptname'])
		{
			$smarty->assign_by_ref('deptname', $_POST['deptname']);
		}

		if($throwcferror)
		{
			$smarty->assign_by_ref('throwcferror', $throwcferror);
			$smarty->assign_by_ref('subject', $_POST['subject']);
			$smarty->assign_by_ref('message', $_POST['message']);
			$ispost = true;
			$smarty->assign_by_ref('ispost', $ispost);
			$smarty->assign_by_ref('customfields', $customfields2);
		}
		else
		{
			$smarty->assign_by_ref('customfields', $customfields);
		}
		$step = 'step2';
		$smarty->assign_by_ref('step', $step);
		$smarty->assign_by_ref('deptid', $deptid);
		$smarty->assign_by_ref('departments', $departments);

		$smarty->display($template.'/header.tpl');
		$smarty->display('../modules/addons/smartertrack/templates/st_ticketprocess_step2.tpl');
		$smarty->display($template.'/footer.tpl');
		exit;
		}
		catch(Exception $e)
		{
			//	Redirect to error page
			$st_errormessage = "An Exception was thrown with the following error message: ".$e;
			$st_errormessage .= "<br><br>Please report the error details to your service provider at:<br>".$st_settings['helpphone'];
			$smarty->assign_by_ref('errormessage', $st_errormessage);
			$smarty->display($template.'/header.tpl');
			$smarty->display('../modules/addons/smartertrack/templates/st_errorpage.tpl');
			$smarty->display($template.'/footer.tpl');
			exit;
		}
	}
}

//Used to display specific ticket communication as well as the option to update a ticket with a response.  Also this where clients can close out tickets if they'd like too as well.
function smartertrack_viewticket()
{

	if (basename($_SERVER['SCRIPT_NAME']) == 'viewticket.php' && $_GET['tid'] )
	{
		if (!$_SESSION['uid'])
		{
			header('Location: login.php');
			exit;
		}
		global $smarty;
		$template = $smarty->get_template_vars('template');
		$error = false;
		$smarty->assign_by_ref('error',$error);

		//	Get Track Settings
		$st_settings = smartertrack_settings();

		$client = smartertrack_getClient($_SESSION['uid'], $st_settings);
		if(!$client)
		{
			$errormessage = smartertrack_errorMessage("The User you are currently logged in with is not Authenticated with SmarterTrack.", null, $st_settings);
			$smarty->assign_by_ref('errormessage', $errormessage);
			$smarty->display($template.'/header.tpl');
			$smarty->display('../modules/addons/smartertrack/templates/st_errorpage.tpl');
			$smarty->display($template.'/footer.tpl');
			exit;
		}

		try
		{

		$st_ws_url = $st_settings['url']."/services2/svcTickets.asmx?WSDL";
		$st_admin = $st_settings['admin'];
		$st_adminpassword = $st_settings['password'];
		//$ticketNumber = $_GET['tid'];
		$ticketNumber = trim(str_replace(']', '', $_GET['tid']), '[');
		$departments = smartertrack_getDepartments($st_settings);
		//GetTicketInfoByTicketNumber
		$params = array( "authUserName"=> $st_admin,
					"authPassword"=> $st_adminpassword,
					"ticketNumber" => $ticketNumber
					);
		$getticketinforeturn = smartertrack_webServiceCall($st_ws_url, $params, "GetTicketInfoByTicketNumber");
		if(!$getticketinforeturn["GetTicketInfoByTicketNumberResult"]["Result"])
		{
			//	Redirect to error page
			$st_errormessage = smartertrack_errorMessage("There was an error viewing the following Ticket ".$_GET['tid'], $getticketinforeturn["GetTicketInfoByTicketNumberResult"]["Message"], $st_settings);
			$smarty->assign_by_ref('errormessage', $st_errormessage);
			$smarty->display($template.'/header.tpl');
			$smarty->display('../modules/addons/smartertrack/templates/st_errorpage.tpl');
			$smarty->display($template.'/footer.tpl');
			exit;
		}
		$ticket = $getticketinforeturn["GetTicketInfoByTicketNumberResult"]["Ticket"];
		$ticketID = $ticket['ID'];
		$subject = $ticket['Subject'];
		$date = date('d/m/Y h:s',strtotime($ticket['LastReplyDateUtc']));
		foreach($departments as $key => $value)
		{
			if($value['deptid'] == $ticket['IdDepartment'])
			{
				$department = $value['name'];
				break;
			}
		}
		$urgency = smartertrack_getTicketPriority($ticket['Priority']);
		$status = smartertrack_getTicketStatus($ticket['IsOpen'], $ticket['IsActive']);
		$statusColor = smartertrack_getTicketStatusColor($ticket['IsOpen'], $ticket['IsActive']);
		$statusClass = smartertrack_getTicketStatusClass($ticket['IsOpen'], $ticket['IsActive']);

		//	CUSTOM FIELDS GetCustomFieldControls
		$st_ws_url = $st_settings['url']."/services2/svctickets.asmx?WSDL";
		$params = array( "authUserName"=> $st_admin,
					"authPassword"=> $st_adminpassword,
					"departmentId" => $ticket['IdDepartment']
					);
		$customfieldresult = smartertrack_webServiceCall($st_ws_url, $params, "GetCustomFieldControls");
		if(!$customfieldresult['GetCustomFieldControlsResult']['Result'])
		{
			foreach($departments as $key => $value)
			{
				if($deptid = $value['deptid'])
				{
					$departmentname = $value['name'];
				}
			}
			//	Redirect to error page
			$st_errormessage = smartertrack_errorMessage("There was an error Extracting the Custom Field Values for the following Department ".$departmentname, $customfieldresult['GetCustomFieldControlsResult']['Message'], $st_settings );
			$smarty->assign_by_ref('errormessage', $st_errormessage);
			$smarty->display($template.'/header.tpl');
			$smarty->display('../modules/addons/smartertrack/templates/st_errorpage.tpl');
			$smarty->display($template.'/footer.tpl');
			exit;
		}
		else
		{
			$customfieldarray = array();
			if(is_array($customfieldresult["GetCustomFieldControlsResult"]["CustomFields"]["CustomFieldInfo"][0]))
			{
				$customfieldarray = $customfieldresult["GetCustomFieldControlsResult"]["CustomFields"]["CustomFieldInfo"];
			}
			else
			{
				$customfieldarray = $customfieldresult["GetCustomFieldControlsResult"]["CustomFields"];
			}

		}

		//	Get Custom Fields for Ticket GetTicketCustomFieldsList
		$params = array( "authUserName"=> $st_admin,
					"authPassword"=> $st_adminpassword,
					"ticketNumber" => $ticketNumber
					);
		$cfforticketresult = smartertrack_webServiceCall($st_ws_url, $params, "GetTicketCustomFieldsList");
		if($cfforticketresult['GetTicketCustomFieldsListResult']['Result'])
		{
			$ticketcustomfields = array();
			foreach($cfforticketresult['GetTicketCustomFieldsListResult']['RequestResults']['string'] as $key => $value)
			{
				$equals = '=';
				$thekey = strstr($value, $equals, true);
				if($thekey == "@UserDisplayName" || $thekey == "@Email")
				{
					if($thekey == "@UserDisplayName")
						array_push($ticketcustomfields, array( "DisplayName" => "Display Name", "DataContent" => trim(strstr($value, $equals), $equals)));
					else
						array_push($ticketcustomfields, array( "DisplayName" => "Email", "DataContent" => trim(strstr($value, $equals), $equals)));
				}
				else
				{
					foreach($customfieldarray as $key2 => $value2)
					{
						if($thekey == $value2['DisplayName'] && $value2['IsVisibleToUsers'])
						{
							array_push($ticketcustomfields, array( "DisplayName" => $thekey, "DataContent" => trim(strstr($value, $equals), $equals)));
						}
					}
				}
			}
			$smarty->assign_by_ref('ticketcustomfields', $ticketcustomfields);
		}


		//	Check for Reply Send
		if($_POST)
		{
			//  AddMessageToTicket
			$params = array( "authUserName"=> $st_admin,
					"authPassword"=> $st_adminpassword,
					"ticketNumber" => $ticketNumber,
					"fromAddress" => $client['st_email'],
					"toAddress" => $department,
					"ccAddress" => "",
					"subject" => $subject,
					"body" => $_POST['replymessage'],
					"isHtml" => false,
					"toCustomer" => false,
					"sendEmail" => true
					);

			$replyPost = smartertrack_webServiceCall($st_ws_url, $params, "AddMessageToTicket");
			if($replyPost['AddMessageToTicketResult']['Result'])
			{
				$replymessage = "Success!";

                $statusparams = array(
                    "authUserName"=>$st_admin,
                    "authPassword"=>$st_adminpassword,
                    "ticketNumber"=>$ticketNumber,
                    "ticketPropertyValues"=>array("TicketStatusID=1"),
                );
                smartertrack_webServiceCall($st_ws_url, $statusparams, "SetTicketProperties");
			}
			else
			{
				$replymessage = $_POST['Recipient'];
			}

		}



		//	GetTicketConversationPartList(username, password, ticketid);

		$params = array( "authUserName"=> $st_admin,
					"authPassword"=> $st_adminpassword,
					"ticketId" => $ticketID
					);

		$ticketpartlist = smartertrack_webServiceCall($st_ws_url, $params, "GetTicketConversationPartList");
		if(!$ticketpartlist["GetTicketConversationPartListResult"]["Result"])
		{
			//	Redirect to error page
			$st_errormessage = smartertrack_errorMessage("There was an error viewing this following Ticket ".$_GET['tid'], $ticketpartlist["GetTicketConversationPartListResult"]["Message"], $st_settings);
			$smarty->assign_by_ref('errormessage', $st_errormessage);
			$smarty->display($template.'/header.tpl');
			$smarty->display('../modules/addons/smartertrack/templates/st_errorpage.tpl');
			$smarty->display($template.'/footer.tpl');
			exit;
		}

		$messagearray = array();
		if(is_array($ticketpartlist["GetTicketConversationPartListResult"]["Parts"]["TicketPartInfo"][0]))
		{
			$messagearray = $ticketpartlist["GetTicketConversationPartListResult"]["Parts"]["TicketPartInfo"];
		}
		else
		{
			$messagearray = $ticketpartlist["GetTicketConversationPartListResult"]["Parts"];
		}

		//	FOREACH MESSAGE
		//	GetTicketMessageHtml(string authUserName, string authPassword, long ticketMessageId)
		$descreplies = array();
		foreach($messagearray as $key => $value)
		{
			if($value['Type'] == 1)
			{
				$reply = array();
				if($value['Direction'] == 1)
				{
					$reply['admin'] = true;
					$reply['client'] = false;

				}
				else
				{
					$Recipient = $value['Recipient'];
					$reply['admin'] = false;
					$reply['client'] = true;
				}
				$reply['name'] = $value['Sender'];
				$reply['date'] = date('d/m/Y h:s',strtotime($value['CreationDateUtc']));

				//	Get Ticket Message
				$params = array( "authUserName"=> $st_admin,
						"authPassword"=> $st_adminpassword,
						"ticketMessageId" => $value['PartId']
						);
				//GetTicketMessagePlainText
				$ticketmessage = smartertrack_webServiceCall($st_ws_url, $params, "GetTicketMessageHtml");
				if($ticketmessage['GetTicketMessageHtmlResult']['Result'])
				{
					if(!$ticketmessage['GetTicketMessageHtmlResult']['messageBody'])
					{
						//GetTicketMessagePlainText
						$ticketmessageplain = smartertrack_webServiceCall($st_ws_url, $params, "GetTicketMessagePlainText");
						if($ticketmessageplain['GetTicketMessagePlainTextResult']['Result'])
						{
							$reply['message'] = $ticketmessageplain['GetTicketMessagePlainTextResult']['messageBody'];
						}
					}
					else
					{
						$reply['message'] = $ticketmessage['GetTicketMessageHtmlResult']['messageBody'];
					}

				}
				array_push($descreplies, $reply);
			}
		}

		//	Get Users Abillity to Close Tickets
		$userid = "userid";
		$canusercloseticket = "userclosetickets";
		$propparamas = array( "authUserName"=> $st_admin,
				"authPassword"=> $st_adminpassword,
				"username" => $client['st_email'],
				"password" => $client['st_password'],
				"requestedValues" => array( $userid, $canusercloseticket));

		$st_ws_url = $st_settings['url']."/services2/svcOrganization.asmx?WSDL";
		$returnprops = smartertrack_webServiceCall($st_ws_url, $propparamas, "GetUsersPropertiesbyUsername");
		if($returnprops["GetUsersPropertiesbyUsernameResult"]["Result"])
		{
			$equals = "=";
			foreach($returnprops["GetUsersPropertiesbyUsernameResult"]["RequestResults"]["string"] as $value)
			{
				$thekey = strstr($value, $equals, true);
				if($thekey === "canusercloseticket")
				{
					$canusercloseticket = trim(strstr($value, $equals), $equals);
				}

			}
		}
		else
		{
			//	Failed - so just disable their ability anyways
			$canusercloseticket = "false";
		}
		$smarty->assign_by_ref("canusercloseticket", $canusercloseticket);


		$user = select_query('tblclients', 'firstname,lastname,email', array('id' => $_SESSION['uid']));
        $user = mysql_fetch_array($user,MYSQL_ASSOC);

		$email = $client['st_email'];
		$username = $user['firstname']." ".$user['lastname'];
		$smarty->assign_by_ref('username', $username);
		$smarty->assign_by_ref('Recipient', $Recipient);
		$smarty->assign_by_ref('email', $email);
		$smarty->assign_by_ref('subject', $subject);
		$smarty->assign_by_ref('date', $date);
		$smarty->assign_by_ref('department', $department);
		$smarty->assign_by_ref('urgency', $urgency);
		$smarty->assign_by_ref('status', $status);
		$smarty->assign_by_ref('statusColorr', $statusColor);
		$smarty->assign_by_ref('statusClass', $statusClass);
		$smarty->assign_by_ref('replymessage', $replymessage);
		$smarty->assign_by_ref('descreplies', $descreplies);

		$smarty->assign('tid', $ticketNumber);
		$smarty->assign('c',$client['whm_userid']);
		$step = '0';
		$smarty->assign_by_ref('step', $step);
		$smarty->display($template.'/header.tpl');
		$smarty->display('../modules/addons/smartertrack/templates/st_viewticket.tpl');
		$smarty->display($template.'/footer.tpl');
		exit;
		}
		catch(Exception $e)
		{
			//	Redirect to error page
			$st_errormessage = "An Exception was thrown with the following error message: ".$e;
			$st_errormessage .= "<br><br>Please report the error details to your service provider at:<br>".$st_settings['helpphone'];
			$smarty->assign_by_ref('errormessage', $st_errormessage);
			$smarty->display($template.'/header.tpl');
			$smarty->display('../modules/addons/smartertrack/templates/st_errorpage.tpl');
			$smarty->display($template.'/footer.tpl');
			exit;
		}
	}
}

//Used to load the support tickets for the client.  This is will list Active, Waiting, and Closed tickets.  This can be accessed through the 'My Support' option across the top.
function smartertrack_tickets()
{
	if (basename($_SERVER['SCRIPT_NAME']) == 'supporttickets.php')
	{
		if (!$_SESSION['uid'])
		{
			header('Location: login.php');
			exit;
		}
		global $smarty;
		$template = $smarty->get_template_vars('template');
		//	Get Track Settings
		$st_settings = smartertrack_settings();
		$client = smartertrack_getClient($_SESSION['uid'], $st_settings);
		if(!$client)
		{
			$errormessage = smartertrack_errorMessage("The User you are currently logged in with is not Authenticated with SmarterTrack.", null, $st_settings);
			$smarty->assign_by_ref('errormessage', $errormessage);
			$smarty->display($template.'/header.tpl');
			$smarty->display('../modules/addons/smartertrack/templates/st_errorpage.tpl');
			$smarty->display($template.'/footer.tpl');
			exit;
		}
		try
		{

		$st_ws_url = $st_settings['url']."/services2/svctickets.asmx?WSDL";
		$st_admin = $st_settings['admin'];
		$st_adminpassword = $st_settings['password'];
		if($client)
		{
			//	Get Tickets For Client
			$searchcrit1 = "EmailAddress=".$client['st_email'];
			$searchcrit2 = "IsDeleted=false";
			$searchcrit3 = "ignoresearchinguserid=true";
			$searchcrit4 = "sortorder=ascending";
			$params = array( "authUserName"=> $st_admin,
				"authPassword" => $st_adminpassword,
				"searchCriteria" => array( $searchcrit1, $searchcrit2, $searchcrit3, $searchcrit4 )
			);

			$ticketsreturn = smartertrack_webServiceCall($st_ws_url, $params, "GetTicketsBySearch");
			if($ticketsreturn && $ticketsreturn['GetTicketsBySearchResult']['Result'])
			{
				$departments = smartertrack_getDepartments($st_settings);
				$open_tickets = array();
				$ticketarray = array();
				if(is_array($ticketsreturn['GetTicketsBySearchResult']['Tickets']['TicketInfo'][0]))
				{
					$ticketarray = $ticketsreturn['GetTicketsBySearchResult']['Tickets']['TicketInfo'];
				}
				else
				{
					$ticketarray = $ticketsreturn['GetTicketsBySearchResult']['Tickets'];
				}
				foreach($ticketarray as $key => $value)
				{
					$departmentname = "";
					$ticketstatus = smartertrack_getTicketStatus($value['IsOpen'], $value['IsActive']);
					$ticketstatusColor = smartertrack_getTicketStatusColor($value['IsOpen'], $value['IsActive']);
					$ticketstatusClass = smartertrack_getTicketStatusClass($value['IsOpen'], $value['IsActive']);
					$ticketpriority = smartertrack_getTicketPriority($value['Priority']);
					foreach($departments as $key2 => $value2)
					{
						if($value2['deptid'] == $value['IdDepartment'])
						{
							$departmentname = $value2['name'];
						}
					}
					array_push($open_tickets, array('department' => $departmentname,
						'lastreply' => date('d/m/Y h:s',strtotime($value['LastReplyDateUtc'])),
						'normalisedLastReply' => date('Y/m/d h:s',strtotime($value['LastReplyDateUtc'])),
						'subject' => $value['Subject'],
						'status' => $ticketstatus,
						'urgency' => $ticketpriority,
						'statusColorr' => $ticketstatusColor,
						'statusClass' => $ticketstatusClass,
						'tid' => $value['TicketNumber']));
				}
			}
			else
			{
				$st_errormessage = smartertrack_errorMessage("There was an error retrieving tickets", $ticketsreturn['GetTicketsBySearchResult']['Message'], $st_settings);
				$smarty->assign_by_ref('errormessage', $st_errormessage);
				$smarty->display($template.'/header.tpl');
				$smarty->display('../modules/addons/smartertrack/templates/st_errorpage.tpl');
				$smarty->display($template.'/footer.tpl');
				exit;
			}
		}
		if ($client) {
			$smarty->assign('tickets', $open_tickets);
			$smarty->assign('numtickets', count($open_tickets));
		} else {
			//	Get Tickets for ST Client
			$total_tickets = 0;
			$smarty->assign_by_ref('numopentickets',$total_tickets);
			$smarty->assign_by_ref('numtickets',$total_tickets);
		}
		if($_GET['tsubmitsuccess'])
		{
			$st_ticketsubmitsuccess = "Thank you for submitting your ticket. Your ticket number is <a href='viewticket.php?tid=".$_GET['tid']."'>".$_GET['tid']."</a>.  Please
				reference this number in any future correspondence relating to this issue.";
			$smarty->assign_by_ref('st_ticketsubmitsuccess', $st_ticketsubmitsuccess);
		}
		else if($_GET['tclosesuccess'])
		{
			$st_ticketsubmitsuccess = "The Ticket was closed successfully, Thank You";
			$smarty->assign_by_ref('st_ticketsubmitsuccess', $st_ticketsubmitsuccess);
		}
		$step = '0';
		$smarty->assign_by_ref('step', $step);
		$smarty->display($template.'/header.tpl');
		$smarty->display('../modules/addons/smartertrack/templates/st_alltickets.tpl');
		$smarty->display($template.'/footer.tpl');
		exit;
		}
		catch(Exception $e)
		{
			//	Redirect to error page
			$st_errormessage = "An Exception was thrown with the following error message: ".$e;
			$st_errormessage .= "<br><br>Please report the error details to your service provider at:<br>".$st_settings['helpphone'];
			$smarty->assign_by_ref('errormessage', $st_errormessage);
			$smarty->display($template.'/header.tpl');
			$smarty->display('../modules/addons/smartertrack/templates/st_errorpage.tpl');
			$smarty->display($template.'/footer.tpl');
			exit;
		}
	}
}

//Used to load the .clientarea.php portion of WHMCS.  This is the entry page for clients.  Also the 'Home' button takes you to this location.
function smartertrack_clientarea()
{
	if (basename($_SERVER['SCRIPT_NAME']) == 'clientarea.php' && $_SESSION['uid'])
	{
		global $smarty;
		$template = $smarty->get_template_vars('template');
		//	Get Track Settings
		$st_settings = smartertrack_settings();
		$client = smartertrack_getClient($_SESSION['uid'], $st_settings);
		if(!$client)
		{
			$errormessage = smartertrack_errorMessage("The User you are currently logged in with is not Authenticated with SmarterTrack.", null, $st_settings);
			$smarty->assign_by_ref('errormessage', $errormessage);
			$smarty->display($template.'/header.tpl');
			$smarty->display('../modules/addons/smartertrack/templates/st_errorpage.tpl');
			$smarty->display($template.'/footer.tpl');
			exit;
		}
		try
		{

		$st_ws_url = $st_settings['url']."/services2/svctickets.asmx?WSDL";
		$st_admin = $st_settings['admin'];
		$st_adminpassword = $st_settings['password'];
		$open_tickets = array();
		if($client)
		{
			//	Get Tickets For Client
			$password = $client['st_password'];
			$searchcrit1 = "EmailAddress=".$client['st_email'];
			$searchcrit2 = "IsDeleted=false";
			$searchcrit3 = "ignoresearchinguserid=true";
			$searchcrit4 = "isopen=true";
			$searchcrit5 = "sortorder=ascending";
			$params = array( "authUserName"=> $st_admin,
				"authPassword" => $st_adminpassword,
				"searchCriteria" => array( $searchcrit1, $searchcrit2, $searchcrit3, $searchcrit4, $searchcrit5 )
			);

			$ticketsreturn = smartertrack_webServiceCall($st_ws_url, $params, "GetTicketsBySearch");
			if($ticketsreturn['GetTicketsBySearchResult']['Result'])
			{
				$departments = smartertrack_getDepartments($st_settings);
				$ticketarray = array();
				if(is_array($ticketsreturn['GetTicketsBySearchResult']['Tickets']['TicketInfo'][0]))
				{
					$ticketarray = $ticketsreturn['GetTicketsBySearchResult']['Tickets']['TicketInfo'];
				}
				else
				{
					$ticketarray = $ticketsreturn['GetTicketsBySearchResult']['Tickets'];
				}
				foreach($ticketarray as $key => $value)
				{
					$departmentname = "";
					$ticketstatus = smartertrack_getTicketStatus($value['IsOpen'], $value['IsActive']);
					$ticketstatusColor = smartertrack_getTicketStatusColor($value['IsOpen'], $value['IsActive']);
					$ticketstatusClass = smartertrack_getTicketStatusClass($value['IsOpen'], $value['IsActive']);
					$ticketpriority = smartertrack_getTicketPriority($value['Priority']);
					foreach($departments as $key2 => $value2)
					{
						if($value2['deptid'] == $value['IdDepartment'])
						{
							$departmentname = $value2['name'];
						}
					}
					array_push($open_tickets, array('department' => $departmentname,
					'date' => date('d/m/Y h:s',strtotime($value['LastReplyDateUtc'])), 'subject' => $value['Subject'],
					'status' => $ticketstatus, 'urgency' => $ticketpriority,
					'statusColorr' => $ticketstatusColor,
					'statusClass' => $ticketstatusClass,
					'tid' => $value['TicketNumber']));
				}
			}
			else
			{
				$st_errormessage = smartertrack_errorMessage("There was an error retrieving tickets", $ticketsreturn['GetTicketsBySearchResult']['Message'], $st_settings);
				$smarty->assign_by_ref('errormessage', $st_errormessage);
				$smarty->display($template.'/header.tpl');
				$smarty->display('../modules/addons/smartertrack/templates/st_errorpage.tpl');
				$smarty->display($template.'/footer.tpl');
				exit;
			}
		}

		$clientsstats = $smarty->get_template_vars('clientsstats');
		$clientsstats['numactivetickets'] = count($open_tickets);
		$smarty->assign_by_ref('tickets', $open_tickets);
		$smarty->assign_by_ref('clientsstats',$clientsstats);
		}
		catch(Exception $e)
		{
			//	Redirect to error page
			$st_errormessage = "An Exception was thrown with the following error message: ".$e;
			$st_errormessage .= "<br><br>Please report the error details to your service provider at:<br>".$st_settings['helpphone'];
			$smarty->assign_by_ref('errormessage', $st_errormessage);
			$smarty->display($template.'/header.tpl');
			$smarty->display('../modules/addons/smartertrack/templates/st_errorpage.tpl');
			$smarty->display($template.'/footer.tpl');
			exit;
		}
  }
}

//Used to close out ticket from the ticket conversation view.  The button 'If resolved, click here to close the ticket'
function smartertrack_closeticket($vars)
{
	if (basename($_SERVER['SCRIPT_NAME']) == 'viewticket.php' && $_GET['tid'] && $_GET['closeticket'] )
	{
		if (!$_SESSION['uid'])
		{
			header('Location: login.php');
			exit;
		}

		//	Close Ticket
		global $smarty;
		$template = $smarty->get_template_vars('template');
		//	Get Track Settings
		$st_settings = smartertrack_settings();
		$client = smartertrack_getClient($_SESSION['uid'], $st_settings);
		if(!$client)
		{
			$errormessage = smartertrack_errorMessage("The User you are currently logged in with is not Authenticated with SmarterTrack.", null, $st_settings);
			$smarty->assign_by_ref('errormessage', $errormessage);
			$smarty->display($template.'/header.tpl');
			$smarty->display('../modules/addons/smartertrack/templates/st_errorpage.tpl');
			$smarty->display($template.'/footer.tpl');
			exit;
		}
		try
		{

        //Adds comment on ticket that the ticket was closed by the client through WHMCS interface.
		$st_ws_url = $st_settings['url']."/services2/svctickets.asmx?WSDL";
		$st_admin = $st_settings['admin'];
		$st_adminpassword = $st_settings['password'];
		if($client)
		{
			$closingcomment = "Closed by ".$client['st_email']." from the WHMCS Client Interface";

			$params = array( "authUserName"=> $st_admin,
					"authPassword" => $st_adminpassword,
					"ticketNumber" => $_GET['tid'],
					"closingComment" => $closingcomment
					);

			$closeticketreturn = smartertrack_webServiceCall($st_ws_url, $params, "CloseTicket");
			if($closeticketreturn['CloseTicketResult']['Result'])
			{
				//	Successful Close - Show Message.
				header('Location: supporttickets.php?tclosesuccess=true');
				exit;
			}
		}
		}
		catch(Exception $e)
		{
			//	Redirect to error page
			$st_errormessage = "An Exception was thrown with the following error message: ".$e;
			$st_errormessage .= "<br><br>Please report the error details to your service provider at:<br>".$st_settings['helpphone'];
			$smarty->assign_by_ref('errormessage', $st_errormessage);
			$smarty->display($template.'/header.tpl');
			$smarty->display('../modules/addons/smartertrack/templates/st_errorpage.tpl');
			$smarty->display($template.'/footer.tpl');
			exit;
		}
	}
}

//	Client Area Page Hooks



add_hook("ClientAreaPage", 1, "smartertrack_clientarea");
add_hook("ClientAreaPage", 2, "smartertrack_redirect");
add_hook("ClientAreaPage", 3, "smartertrack_choose_department");
add_hook("ClientAreaPage", 4, "smartertrack_ticket_submitform");
add_hook("ClientAreaPage", 5, "smartertrack_tickets");
add_hook("ClientAreaPage", 7, "smartertrack_viewticket");
add_hook("ClientAreaPage", 6, "smartertrack_closeticket");

add_hook("ClientChangePassword",1, "smartertrack_changepassword");

//OLD Client Area Page Hooks - Old order that was used previously - if any isses are noted, you can always comment out this portion and place comments around the hooks above.
/*
add_hook("ClientAreaPage", 12, "smartertrack_clientarea");
add_hook("ClientAreaPage",7,"smartertrack_redirect");
add_hook("ClientAreaPage", 1, "smartertrack_choose_department");
add_hook("ClientAreaPage", 6, "smartertrack_ticket_submitform");
add_hook("ClientAreaPage", 19, "smartertrack_viewticket");
add_hook("ClientAreaPage", 10, "smartertrack_tickets");
add_hook("ClientAreaPage", 12, "smartertrack_closeticket");

add_hook("ClientChangePassword",1, "smartertrack_changepassword");
*/

//	Client Add Hook
add_hook("ClientAdd",1,"smartertrack_adduser");

//	Client Delete Hook
add_hook("ClientDelete", 1, "smartertrack_deleteuser");

// Client Edit Hook
add_hook("ClientEdit", 1, "smartertrack_edituser");

?>
