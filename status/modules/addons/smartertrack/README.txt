/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////	SmarterTrack Help Desk Module   /////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		
NOTE:  THIS MODULE ONLY WORKS WITH SMARTERTRACK VERSION 10.5.5507 OR LATER - DO NOT ATTEMPT TO USE
WITH A PREVIOUS VERSION.  VERSION 1 OF THIS MODULE SUPPORTS SMARTERTRACK VERSION 8.4+, AND CAN BE
DOWNLOADED FROM THE SMARTERTOOLS WEBSITE.

This is a help document that answers basic questions about the module.  
If the answers are not in this document you can always contact SmarterTools
by emailing bugs@smartertools.com, opening a support ticket at portal.smartertools.com,
or starting a live chat from our website http://www.smartertools.com.  Additionally you can visit
our community forums at http://forums.smartertools.com  SmarterTrack also offers hosted support
at http://www.smartertrack.com.

Thank You.

/////////////////////////	Installation   //////////////////////////////////////////////////////////////////////////////////

Please follow these steps when Installing the Module

1.  Place the SmarterTrack Help Desk Module package under ..modules/addons/*SmarterTrack*
2.  In the Management Interface go to Set Up -> Addon Modules
3.  Locate the SmarterTrack Module in the list of modules and click "Activate".  
	This will only show up if you have completed step one.
4.  Click Configure on the SmarterTrack Help Desk Module and fill out all of the settings.
	After The settings have been filled out click "Save Changes".
	Settings Notes:  the domain should not include a / at the end.  
	for example:  www.smartertools.com not www.smartertools.com/ .  The bottom setting,
	"Access Control" is a default setting for all modules and its for who has access to the
	Admin addon page for the SmarterTrack Help Desk Module, which you will need to go to next
	if you want to sync current WHMCS users to SmarterTrack, so enable this for the type of user
	your currently configuring under, mostly likely 'Full Administrator'.
5.  Navigate to Addons -> SmarterTrack Help Desk Module
	Click "Generate" to sync all users.  Note:  It is best to delete all users in SmarterTrack
	before performing this sync.
6.  Test a User within the support system.  test creating a user, test deleting a user. test submitting tickets,
	etc.

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/////////////////////////	Tips    /////////////////////////////////////////////////////////////////////////////////////////

-  You will want to disable users from changing their passwords within SmarterTrack.  This setting can be found
	at 'Settings->Portal->PortalSettings->Options as Enable users to change email address'.
-  You will want to configure your live chat script.  The module will work with both image and plain text.
	There is also an image created just for this module for live chats.  It will take including it in the SmarterTrack
	images folder and configured in SmarterTrack under 'Live Chat Links'.
-  You will want to configure auto responders for each department that you use.  The Ticket link will link to SmarterTrack
	by default, so you will want to create your own link.  your links target will be the following:
	*yourdomain*/viewticket.php?tid=#TICKETNUMBER# .

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
	
/////////////////////////	Hidden / Specialized Custom Fields    ////////////////////////////////////////////////////////////

The Following Custom Fields have been configured to work with SmarterTrack and WHMCS so that they can be modified and 
populated within WHMCS with WHMCS data.

	1.	Email (Single Line Text)  :  The email address of the WHMCS user
	2.	Display Name (Single Line Text) : the First and Last name of the WHMCS user
	3.	Domains (Single Line Text)in track :  in WHMCS a drop down list is populated with user's domains.  The selected Domain 
		will be sent to track as single line text
	4.	Product/Domain (Single Line Text)in track : in WHMCS a drop down list is populated with user's products and domain 
		associated.  Will be sent to track as single line text

Display Names inside of Track must match  these character for character or else they will not populate.

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

