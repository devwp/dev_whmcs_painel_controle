<?php
/*
***************************************************

*** SmarterTrack Help Desk Module Language Strings ***

For more info, please refer to the kb articles
 @   http://portal.smartertools.com

***************************************************
*/

$_ADDONLANG['intro'] = "This module integrates the SmarterTrack help desk into your WHMCS installation.".
    "For more information on installing SmarterTrack, visit <a href=\"http://www.smartertools.com/smartertrack/help-desk-software.aspx\" target=\"_blank\" >the SmarterTools website</a>. ".
    "SmarterTrack is also available as a hosted service from <a href='http://www.smartertrack.com' target='_blank'>SmarterTrack.com</a>.";
$_ADDONLANG['description'] = "SmarterTrack is a powerful help desk that was built for tracking, managing and reporting on customer service and communications across multiple support channels. Using a simple Web browser on any desktop, tablet or mobile device, businesses can easily manage employee and customer communication across email tickets, live chats, phone calls as well as self-help resources like a knowledge base, news items, announcements and customer portal.";
$_ADDONLANG['documentation'] = "This module was developed by SmarterTools Inc and is licensed under the standard GPL open source licensing model.";
$_ADDONLANG['priority'] = "Priority";

?>
