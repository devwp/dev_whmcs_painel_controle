<?php

class OnAppBilling
{
    //Module Name
    public $name = 'OnApp Billing';
    
    //System Name
    public $system_name = 'OnApp Billing';
    
    //Module Description
    public $description = 'This module gives you the possibility to bill your clients for specific resources usage depending from their origin and set up free limits. Additionally, you can set up a workflow of the module for each product separately and even create invoice through one-click!';
    //Module Version
    public $version = '1.6.9';
    
    //Module Author
    public $author = 'OnApp';
    
    //Default Page
    public $default_page = 'configuration';
    
    //Top Menu
    public $top_menu =  array
    (
        'configuration'         =>  array
        (
            'title'             =>  'Configuration',
            'icon'              =>  'magic',
            'submenu'           =>  array
            (
                'edit'          =>  array
                (
                    'title'     =>  'Edit Product',
                    'show'      =>  false
                ),
            )
        ), 
        'items'                 =>  array
        (
            'icon'              =>  'th-list',
            'title'             =>  'Items'
        ),
        'logs'                  =>  array
        (
            'title'             =>  'Logs',
            'icon'              =>  'adjust',
        ),
        'invoices'              =>  array
        (
            'title'             =>  'Awaiting Invoices',
            'icon'              =>  'book',
            'submenu'           =>  array
            (
                'show'          =>  array
                (
                    'title'     =>  'Show Invoice',
                    'show'      =>  false
                )
            )
        )
    );
    
    //Side Menu
    public $side_menu = array
    (
        'configuration'         =>  array
        (
            'title'             =>  'Configuration',
            'icon'              =>  'magic',
        ), 
        'items'                 =>  array
        (
            'icon'              =>  'th-list',
            'title'             =>  'Items'
        ),
        'logs'                  =>  array
        (
            'title'             =>  'Logs',
            'icon'              =>  'adjust',
        ),
        'invoices'              =>  array
        (
            'title'             =>  'Awaiting Invoices',
            'icon'              =>  'book'
        ),
    );
    
    //Enable PHP Debug Info
    public $debug = 0;
    
    //Enable Logger
    public $logger = 1;
    
    /**
     * This function is call when administrator will activate your module
     */
    public function activate()
    {
        mysql_safequery("CREATE TABLE IF NOT EXISTS `OnAppBilling_settings`
            (
                `product_id`            INT(11) NOT NULL,
                `enable`                INT(1) NOT NULL,
                `module`                VARCHAR(255),
                `billing_settings`      TEXT NOT NULL,
                `resource_settings`     TEXT NOT NULL,
                `module_configuration`  TEXT NOT NULL,
                UNIQUE KEY(`product_id`)
            ) ENGINE = MyISAM") or die(mysql_error());
    
        mysql_safequery("CREATE TABLE IF NOT EXISTS `OnAppBilling_resources_settings` 
            (
                `product_id` varchar(255) NOT NULL,
                `resources` blob NOT NULL,
                PRIMARY KEY (`product_id`)
            ) ENGINE=MyISAM") or die(mysql_error());
    
        mysql_safequery("CREATE TABLE IF NOT EXISTS `OnAppBilling_updates`
            (
                `hosting_id`    INT(11) NOT NULL,
                `rel_id`        VARCHAR(128) DEFAULT '0',
                `timestamp`     DATETIME NOT NULL,
                 KEY(`hosting_id`),
                 KEY(`rel_id`)
            ) ENGINE = MyISAM") or die(mysql_error());
    
        mysql_safequery("CREATE TABLE IF NOT EXISTS `OnAppBilling_submodules_data`
            (
                `hosting_id`    INT (11) NOT NULL,
                `data`          BLOB,
                UNIQUE KEY(`hosting_id`)
            ) ENGINE = MyISAM") or die(mysql_error());
                
        //Faktury oczekujace na potwierdzenie
        mysql_safequery("CREATE TABLE IF NOT EXISTS `OnAppBilling_awaiting_invoices`
            (
                `id`            INT(11) NOT NULL AUTO_INCREMENT,
                `userid`        INT(11) NOT NULL,
                `hostingid`     INT(11) NOT NULL,
                `date`          DATE NOT NULL,
                `duedate`       DATE NOT NULL,
                `items`         BLOB NOT NULL,
                PRIMARY KEY (`id`)
            ) ENGINE = MyISAM") or die(mysql_error());
        
        mysql_safequery("CREATE TABLE IF NOT EXISTS `OnAppBilling_hosting_details`
            (
                `hosting_id`    INT(11) NOT NULL,
                `invoice_date`  DATETIME,
                UNIQUE KEY(`hosting_id`),
                KEY(`invoice_date`)
            ) ENGINE = MyISAM") or die(mysql_error());
        
        mysql_safequery("CREATE TABLE IF NOT EXISTS `OnAppBilling_billed_hostings`
        (
            `hosting_id`    INT(11) NOT NULL,
            `date`          DATETIME NOT NULL,
             UNIQUE KEY(`hosting_id`)
        ) ENGINE = MyISAM") or die(mysql_error());

        mysql_safequery("CREATE TABLE IF NOT EXISTS `OnAppBilling_CDN_hostnames`
        (
            `cdn_id`        INT(11) NOT NULL,
            `hosting_id`        INT(11) NOT NULL,
            `hostname`  VARCHAR(255),
            PRIMARY KEY (`cdn_id`)
        ) ENGINE = MyISAM") or die(mysql_error());
        
        //teoretycznie tworzy katalog z logami dla crona 
        $dir = dirname(__FILE__);
        if(!is_dir($dir.DS.'cron'.DS.'logs'))
        {
            //try to create dir
            mkdir($dir.DS.'cron'.DS.'logs', 0644);
        }
    }
    
    /**
     * Functions is called when administrator will deactivate your module
     */
    public function deactivate()
    {
        mysql_safequery("DROP TABLE OnAppBilling_settings");
        mysql_safequery("DROP TABLE OnAppBilling_resources_settings");
        mysql_safequery("DROP TABLE OnAppBilling_updates");
        mysql_safequery("DROP TABLE OnAppBilling_submodules_data");
        mysql_safequery("DROP TABLE OnAppBilling_awaiting_invoices");
        mysql_safequery("DROP TABLE OnAppBilling_hosting_details");
        mysql_safequery("DROP TABLE OnAppBilling_billed_hostings");
        mysql_safequery("DROP TABLE OnAppBilling_CDN_hostnames");
        
        //Uninstall submodules databases
        $this->uninstallSubmodules();
    }
    
    public function upgrade($version)
    {
        if($version < "1.6.5")
        {
            mysql_safequery("CREATE TABLE IF NOT EXISTS `OnAppBilling_CDN_hostnames`
            (
                `cdn_id`        INT(11) NOT NULL,
                `hosting_id`    INT(11) NOT NULL,
                `hostname`      VARCHAR(255),
                PRIMARY KEY (`cdn_id`)
            ) ENGINE = MyISAM") or die(mysql_error());
        }
    }
    
    private function installSubmodules()
    {
        require_once dirname(__FILE__).DIRECTORY_SEPARATOR.'core.php';
        $submodules = OnAppBilling_getModules();   
        foreach($submodules as $name)
        {
            $n = $name.'_resources';
            require_once dirname(__FILE__).DIRECTORY_SEPARATOR.'submodules'.DIRECTORY_SEPARATOR.'class.'.$n.'.php';
            $m = new $n();
            $m->install(); 
        }
    }
    
    private function uninstallSubmodules()
    {
        require_once dirname(__FILE__).DS.'core.php';
        $submodules = OnAppBilling_getModules();   
        foreach($submodules as $name)
        {
            $n = $name.'_resources';
            require_once dirname(__FILE__).DS.'submodules'.DS.'class.'.$n.'.php';
            $m = new $n(0);
            $m->uninstall();
        }
    }
    
    private function upgradeSubmodules()
    {
        require_once dirname(__FILE__).DS.'core.php';
        $submodules = OnAppBilling_getModules();   
        foreach($submodules as $name)
        {
            $n = $name.'_resources';
            require_once dirname(__FILE__).DS.'submodules'.DS.'class.'.$n.'.php';
            $m = new $n(0);
            $m->upgrade();
        }
    }
}