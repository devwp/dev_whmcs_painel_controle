{if $message != ""}
	<div class="alert alert-success text-center" id="alertModuleCustomButtonSuccess">
		{$message}
	</div>
{/if}
{if $errormessage != ""}
	<div class="alert alert-danger text-center" id="alertModuleCustomButtonFailed">
		{$errormessage}
	</div>
{/if}
<br clear="both" /> 

<div style="float:left; width: 123px;">
	<form method="post" action="clientarea.php?action=productdetails">
		<input type="hidden" name="id" value="{$id}" />
		<input type="hidden" name="modop" value="custom" />
		<input type="hidden" name="domain" value="{$DomainName}">
		<input type="hidden" name="a" value="editsmartermaildomainpage" />

		<select style="width: 0px; height: 0; border:0; line-height: 0" size="0" name="selectalias">
		<option value="{$DomainName}">{$DomainName}</option>
		</select>
		<input type="submit" value="{$LANG.smglobalback}" class="btn btn-primary" style="width:123px" />
	</form>
</div><br clear="both" /><br clear="both" />
<script src="/assets/js/bootstrap-tabdrop.js"></script>
	<script type="text/javascript">
	jQuery('.nav-tabs').tabdrop();
	</script>	
	<div class="tabbable">
	<div class="row clearfix">
		<div class="col-xs-12">
			<ul class="nav nav-tabs">
				<li {if ($aliastab == '')}class="active"{/if}>
					<a href="#domain" data-toggle="tab">{$LANG.smeditusers}</a>
				</li>
				<li {$aliastab}>
					<a href="#resourceusage" data-toggle="tab">{$LANG.smmanageAlias}</a>
				</li>	
				
			</ul>
		</div>
	</div>
	<div class="tab-content">
	    <div class="tab-pane {if ($aliastab == '')}active{/if}" id="domain">
		<form method="post" id="deleteForm3" name="deleteForm" action="clientarea.php?action=productdetails" style="float: right;">
			<input type="hidden" name="username" value="{$UserName}">
			<input type="hidden" name="id" value="{$id}" />
			<input type="hidden" name="modop" value="custom" />
			<input type="hidden" name="domain" value="{$DomainName}">
			<input type="hidden" name="a" value="deletesmartermailuser"/>
			
		</form>

    <form method="post" action="clientarea.php?action=productdetails" class="form-horizontal using-password-strength" >
    <h3>{$LANG.smeditusers}</h3>
	<div class="form-group">
        <label for="inputalias" class="col-sm-5 control-label">{$LANG.smusersemail}</label>
		<div class="col-sm-6" style="text-align: left;line-height: 35px;">
		{$UserName}
        </div>
    </div>
	<div class="form-group">
        <label for="inputalias" class="col-sm-5 control-label">{$LANG.smuserspass}</label>
        <div class="col-sm-6">
		<input type="password" name= "password"  value="{$Password}" style="width: 200px; background: #EEE; cursor: not-allowed;" class="form-control" readonly/>
        </div>
    </div> 
	<div class="form-group">
        <label for="inputalias" class="col-sm-5 control-label">{$LANG.smusersconfirmpass}</label>
        <div class="col-sm-6">
		<input type="password" name= "newpassword" style="width: 200px;" class="form-control" placeholder="******"/>
        </div>
    </div> 
	<div class="form-group"> 
        <label for="inputalias" class="col-sm-5 control-label">{$LANG.smusersname}</label>
        <div class="col-sm-6">
		<input type="text" name= "firstname" value="{$FirstName}" style="width: 200px;" class="form-control"/>
        </div>
    </div> 
	
	{if ($IsDedicated == "on")}
		<div class="form-group">
			<label for="inputalias" class="col-sm-5 control-label">{$LANG.smuserssize}</label>
			<div class="col-sm-6">
			<input type="text" name= "mailboxsize" value="{$maxMailboxSize}" style="width: 200px;float:left;line-height: 34px;" class="form-control"/><span style="float:left">MB</span> 
			</div>
		</div> 
				<div class="form-group">
			<label for="inputalias" class="col-sm-5 control-label">Status</label>
			<div class="col-sm-6">
				<select name="mailboxstatus" id="mailboxstatus" class="form-control" style="width:200px;">
					<option	{if ($mailboxStatus== "1")} selected="selected"{/if}  value="1">Ativado</option>
					<option {if ($mailboxStatus == "3")} selected="selected"{/if} value="3">Desativado</option>
				</select>
			</div>
		</div> 
	{else}
		<input type="hidden" name="mailboxsize" value="{$maxMailboxSize}">
	{/if} 
	<br clear="both" />
    <div class="form-group">
	<div class="text-center">
		
			<input type="hidden" name="domaincheckbox" value="{$isdomainadmin}">
			<input type="hidden" name="lastname" value="{$LastName}"> 
			<input type="hidden" name="username" value="{$UserName}">
			<input type="hidden" name="domain" value="{$DomainName}">
			<input type="hidden" name="id" value="{$id}" />
			<input type="hidden" name="modop" value="custom" />
			<input type="hidden" name="a" value="savechangessmartermailuser" /> 
			{if $isdomainadmin == "True"}{else}<input type="submit" value="{$LANG.smglobalreflesh}" class="btn btn-primary" /> <input type="button" id="deleteButtom"value="{$LANG.smglobalDelete}" class="btn btn-default" onClick="document.getElementById('deleteForm3').submit()" /> {/if}  
			</div> 
    </div>
</form>

	<br clear="both" />
</div>

<div class="tab-pane {if ($aliastab == "class='active'")}active{/if}" id="resourceusage"><br clear="both"/>
<link rel="stylesheet" type="text/css" href="{$BASE_PATH_CSS}/dataTables.bootstrap.css">
<link rel="stylesheet" type="text/css" href="{$BASE_PATH_CSS}/dataTables.responsive.css">
<script type="text/javascript" charset="utf8" src="{$BASE_PATH_JS}/jquery.dataTables.min.js"></script>
<script type="text/javascript" charset="utf8" src="{$BASE_PATH_JS}/dataTables.bootstrap.min.js"></script>
<script type="text/javascript" charset="utf8" src="{$BASE_PATH_JS}/dataTables.responsive.min.js"></script>
<div style="float:right; width: 142px;">
    <form method="post" action="clientarea.php?action=productdetails&id={$id}&modop=custom&a=managesmartermailalias"> 
		<input type="hidden" name="username" value="{$UserName}">
        <input type="hidden" name="modop" value="custom" />
        <input type="hidden" name="a" value="addsmartermailalias" />
		<input type="hidden" name="domain" value="{$DomainName}">
        <input type="submit" value="{$LANG.smaddAlias}" class="btn btn-primary" />    
    </form>
</div>

{if $warnings}
    {include file="$template/includes/alert.tpl" type="warning" msg=$warnings textcenter=true}
{/if}
<div class="tab-content">
    <div class="tab-pane fade in active" id="tabOverview">
             <script type="text/javascript">
            jQuery(document).ready(function () {
                var table = jQuery("#tableDomainsList").DataTable({
                    "dom": '<"listtable"fit>pl', "responsive": true,
                         "oLanguage": {
                        "sEmptyTable": "{$LANG.smglobalNothing}",
                        "sInfo": "{$LANG.smmanageAlias} | {$UserName}", 
						"sInfoEmpty": "{$LANG.smmanageAlias} | {$UserName}",
                        "sInfoFiltered": "{$LANG.smmanageAlias} | {$UserName}",
                        "sInfoPostFix": "",
                        "sInfoThousands": ",",
                        "sLengthMenu": "{$LANG.smglobalshow} _MENU_",
                        "sLoadingRecords": "{$LANG.smgloballoading}",
                        "sProcessing": "{$LANG.smglobalProcess}",
                        "sSearch": "",
                        "sZeroRecords": "{$LANG.smglobalNothing}",
                        "oPaginate": {
                            "sFirst": "",
                            "sLast": "",
                            "sNext": "{$LANG.smglobalNext}",
                            "sPrevious": "{$LANG.smglobalPrev}"
                        }
                    },
                    "pageLength": 10,
                    "order": [
                        [0, "asc"]
                    ],
                    "lengthMenu": [
                        [10, 25, 50, -1],
                        [10, 25, 50, "All"]
                    ], "stateSave": true
                });
                jQuery('#tableDomainsList').removeClass('hidden').DataTable();
                table.draw();
				jQuery("#tableDomainsList_filter input").attr("placeholder", "{$LANG.smglobalsearch}");

            });
        </script>
</br>
     
        <div class="table-container clearfix" ></br>
            <table id="tableDomainsList" class="table table-list hidden">
                <thead>
                    <tr>

                        <th>{$LANG.orderdomain}</th>
						<th></th>
                    </tr>
                </thead> 
                <tbody>
                    {foreach $wsGetAliasesResultArray as $domain}	 
                    <tr>
                        <td style="font-size:16px;">
							{$domain}
                        </td>
						<td>
							<form method="post" id="deleteForm" action="clientarea.php?action=productdetails" style="float: right;width:80px;">
							<input type="hidden" name="aliasname" value="{$domain}" />
							<input type="hidden" name="username" value="{$UserName}">
							<input type="hidden" name="id" value="{$id}" />
							<input type="hidden" name="modop" value="custom" />
							<input type="hidden" name="domain" value="{$DomainName}">
							<input type="hidden" name="a" value="deletesmartermailalias" /> 
							<input type="submit" value="Excluir" class="btn btn-primary" style="border:0;font-size: 14px;line-height: 1.42857143;color: #333333 !important; background:transparent; padding:0;">
							</form>
						</td>
                    </tr>
                </tbody>
                {/foreach}
            </table>
            
        </div>
    </div>
</div>

</div>
</div>







