<ul class="nav nav-tabs">
  <li class="active"><a data-toggle="tab" href="#user">{$LANG.smeditusers}</a></li>
  <li><a data-toggle="tab" href="#aliasuser">{$LANG.smmanageAlias}</a></li>
  <li><a data-toggle="tab" href="#config">Configurações</a></li>
</ul>

<div class="tab-content">
   <br clear="both"/>
	<div id="user" class="tab-pane fade in active">
		<form method="post" id="deleteForm3" name="deleteForm" action="clientarea.php?action=productdetails" style="float: right;">
			<input type="hidden" name="username" value="{$UserName}">
			<input type="hidden" name="id" value="{$id}" />
			<input type="hidden" name="modop" value="custom" />
			<input type="hidden" name="domain" value="{$DomainName}">
			<input type="hidden" name="a" value="deletesmartermailuser"/>
			
		</form>

		<form method="post" action="clientarea.php?action=productdetails" class="form-horizontal using-password-strength" >
		<h3>{$LANG.smeditusers}</h3>
		<div class="form-group">
		<label for="inputalias" class="col-sm-5 control-label">{$LANG.smusersemail}</label>
		<div class="col-sm-6" style="text-align: left;line-height: 35px;">
		{$UserName}
		</div>
		</div>
		<div class="form-group">
		<label for="inputalias" class="col-sm-5 control-label">{$LANG.smuserspass}</label>
		<div class="col-sm-6">
		<input type="password" name= "password"  value="{$Password}" style="width: 200px; background: #EEE; cursor: not-allowed;" class="form-control" readonly/>
		</div>
		</div> 
		<div class="form-group">
		<label for="inputalias" class="col-sm-5 control-label">{$LANG.smusersconfirmpass}</label>
		<div class="col-sm-6">
		<input type="password" name= "newpassword" style="width: 200px;" class="form-control" placeholder="******"/>
		</div>
		</div> 
		<div class="form-group"> 
		<label for="inputalias" class="col-sm-5 control-label">{$LANG.smusersname}</label>
		<div class="col-sm-6">
		<input type="text" name= "firstname" value="{$FirstName}" style="width: 200px;" class="form-control"/>
		</div>
		</div> 

		{if ($IsDedicated == "on")}
		<div class="form-group">
			<label for="inputalias" class="col-sm-5 control-label">{$LANG.smuserssize}</label>
			<div class="col-sm-6">
			<input type="text" name= "mailboxsize" value="{$maxMailboxSize}" style="width: 200px;float:left;line-height: 34px;" class="form-control"/><span style="float:left">MB</span> 
			</div>
		</div> 
		{else}
		<input type="hidden" name="mailboxsize" value="{$maxMailboxSize}">
		{/if} 
		<br clear="both" />
		<div class="form-group">
		<div class="text-center">

			<input type="hidden" name="domaincheckbox" value="{$isdomainadmin}">
			<input type="hidden" name="lastname" value="{$LastName}"> 
			<input type="hidden" name="username" value="{$UserName}">
			<input type="hidden" name="domain" value="{$DomainName}">
			<input type="hidden" name="id" value="{$id}" />
			<input type="hidden" name="modop" value="custom" />
			<input type="hidden" name="a" value="savechangessmartermailuser" /> 
			{if $isdomainadmin == "True"}{else}<input type="submit" value="{$LANG.smglobalreflesh}" class="btn btn-primary" /> <input type="button" id="deleteButtom"value="{$LANG.smglobalDelete}" class="btn btn-default" onClick="document.getElementById('deleteForm3').submit()" /> {/if}  
			</div> 
		</div>
		</form>

		<br clear="both" />
	</div>
	<div id="aliasuser" class="tab-pane fade">
		<form method="post" action="/clientarea.php?action=productdetails&id={$id}&modop=custom&a=managesmartermaildomains&d={$DomainName}">
			<div class="form-group">
			<label class="col-sm-3 control-label" for="newalias">{$LANG.smAlias}</label>
			<div class="col-sm-6 input-group">
			<input type="text"  name="aliasname" style="" class="form-control" /><div class="input-group-addon">@{$DomainName}</div>
			<input type="hidden"  name="newaliasemailaddress" id="username"  style="" class="form-control" />
			<input type="hidden"  name="email" id="email" value="{$UserName}"  style="" class="form-control" />
			</div>
			</div> 
			<div class="text-center"> 
			<button type="submit" name="managed" value="addAlias" class="btn btn-primary">
			{$LANG.smaddAlias}</buton> 
			</div> 
		</form>
		<table id="tablealiasuser" class="table table-list">
                <thead>
                    <tr>

                        <th>{$LANG.orderdomain}</th>
						<th></th>
                    </tr>
                </thead> 
                <tbody>
                    {foreach $GetAliases as $domain}	 
                    <tr>
                        <td style="font-size:16px;">
							{$domain}
                        </td>
						<td>
						<form method="post" class="form-horizontal using-password-strength">
						<input type="hidden" name="aliasname" value="{$domain}" /> 
							<button type"submit" class="bnt btn-link" name="delAlias" value="{$domain}"  onclick="if(confirm('Esta ação irá apagar este apelido.\nDeseja continuar?')){ parentNode.submit(); } else { return false; }" ><i class="fa fa-trash-o" aria-hidden="true"></i></button>
						</form>
						</td>
                    </tr>
                    {/foreach}
                </tbody>
         
            </table>
	</div>
	<div id="config" class="tab-pane fade">
	</div>
</div>
