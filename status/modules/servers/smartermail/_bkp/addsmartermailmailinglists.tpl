{if $message != ""}
	<div class="alert alert-success text-center" id="alertModuleCustomButtonSuccess">
		{$message}
	</div>
{/if}
{if $errormessage != ""}
	<div class="alert alert-danger text-center" id="alertModuleCustomButtonFailed">
		{$errormessage}
	</div>
{/if}<br clear="both" />
<div style="float:left; width: 123px;">
	<form method="post" action="clientarea.php?action=productdetails">
		<input type="hidden" name="id" value="{$id}" />
		<input type="hidden" name="modop" value="custom" />
		<input type="hidden" name="domain" value="{$domainName}">
		<input type="hidden" name="a" value="editsmartermaildomainpage" />

		<select style="width: 0px; height: 0; border:0; line-height: 0" size="0" name="selectalias">
		<option value="{$domainName}">{$domainName}</option>
		</select>
		<input type="submit" value="{$LANG.smglobalback}" class="btn btn-primary" style="width:123px" />
	</form>
</div><br clear="both" /><br clear="both" />
    <form method="post" action="clientarea.php?action=productdetails" class="form-horizontal using-password-strength" >
    <h3>{$LANG.smaddmailinglist} | {$domainName}</h3>
    <div class="form-group">
        <label for="inputalias" class="col-sm-5 control-label">{$LANG.smmailinglistsname}</label>
        <div class="col-sm-6">
            <input type="text" name= "newlistname"  style="width: 100%;"  class="form-control" />
        </div>
    </div>
	<div class="form-group">
        <label for="inputalias" class="col-sm-5 control-label">{$LANG.smmailinglistsModerator}</label>
        <div class="col-sm-6">
		  <select  name="newmoderator" class="form-control" >
		       {foreach $wsGetDomainUsersResultArray as $domain}
                 <option value="{$domainName}">{$domain}</option>
               {/foreach} </select> 
          
        </div>
    </div>
	<div class="form-group">
        <label for="inputalias" class="col-sm-5 control-label">{$LANG.smmailinglistssize}</label>
        <div class="col-sm-6">
		<select name="maxmessagesize" class="form-control">
		<option value="2048" selected>2MB</option> 
		<option value="5120" >5MB</option>
		<option value="10240">10MB</option>
		</select>
        </div>
    </div>
	<div class="form-group">
        <label for="inputalias" class="col-sm-5 control-label">{$LANG.smmailinglistsperm}</label>
        <div class="col-sm-6">
		<select name="whocanpost" class="form-control">
			<option value="Moderator">{$LANG.smmailinglistsmod}</option> 
			<option value="Subscribers">{$LANG.smmailinglistsass}</option> 
			<option value="anyone">{$LANG.smmailinglistsother}</option>  
		</select>
        </div>
    </div>
    <div class="form-group">
        <div class="text-center">
           <input type="hidden" name="id" value="{$id}" />
		   			<input type="hidden" name="domain" value="{$domainName}" />
			<input type="hidden" name="modop" value="custom" />
			<input type="hidden" name="a" value="createsmartermailmailinglists" />
			<input type="submit" value="{$LANG.smglobalsave}" class="btn btn-primary" /> 
			<input class="btn btn-default" value="{$LANG.smglobalcancel}" type="reset">
        </div>
    </div>
</form>
