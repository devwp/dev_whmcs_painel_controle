   <script type="text/javascript">
      $("#internal-content").attr("style","width:100% !important");
      $(".sidebar-primary").hide();
    </script> 
<!--
<div style="display:none;">
    <form method="post" action="clientarea.php?action=productdetails">
        <input type="hidden" name="id" value="{$serviceid}" />
        <input type="hidden" name="modop" value="custom" />
        <input type="hidden" name="a" value="managesmartermail" />
        <input type="submit" value="Gerenciar Usuários" />
    </form>

 {if $showdomaindetails}
<div align="left">
<h2>{$domaininfo['GetDomainInfo2Result']['Name']}</h2>
Users: {$domaininfo['GetDomainInfo2Result']['UserCount']} / {$domainsettings['GetDomainSettingsResult']['MaxDomainUsers']}
<br/>Aliases: {$domaininfo['GetDomainInfo2Result']['AliasCount']} / {$domainsettings['GetDomainSettingsResult']['MaxDomainAliases']}
<br/>Size (MB): {$domaininfo['GetDomainInfo2Result']['SizeMb']} / {$domainsettings['GetDomainSettingsResult']['MaxDomainSizeInMB']}
</div
{else}
Only SmarterMail 14 or higher can show domain details here.
{/if}
</div>
-->
{if $IsDedicated == 'on'}

<div class="col-md-6">
  <div class="panel panel-default">
    <div class="panel-heading">
      <h3 class="panel-title">{$LANG.resourceUsage}</h3>
    </div>
    <div class="panel-body text-center">
      <div class="col-sm-10 col-sm-offset-1">
        <div class="col-sm-12">
           <h4>Contas</h4>
           <input type="text" value="{$totalDomainsPercent}" class="dial-usage1" data-width="100" data-height="100" data-min="0" data-readOnly="true" />
           <p>{$totalDomains} / {$maxDomains}</p>

         </div>
       </div>
       <div class="clearfix"></div>
       <!--p class="text-muted">{$LANG.clientarealastupdated}: {$lastupdate}</p-->
       <script src="{$BASE_PATH_JS}/jquery.knob.js"></script>
       <script type="text/javascript">
       jQuery(function() {ldelim}
         jQuery(".dial-usage1").knob({ldelim}
            'change': function (v) {ldelim} console.log(v); {rdelim},
            draw: function () {ldelim}
              $(this.i).val(this.cv + '%');
            {rdelim}
         {rdelim});
       {rdelim});
       </script>
     </div>
   </div>
 </div>
{else}
<div class="col-md-6">
  <div class="panel panel-default">
    <div class="panel-heading">
      <h3 class="panel-title">{$LANG.resourceUsage}</h3>
    </div>
    <div class="panel-body text-center">
      <div class="col-sm-10 col-sm-offset-1">
        <div class="col-sm-12">
           <h4>{$LANG.cPanel.emailAccounts }</h4>
           <input type="text" value="{$totalUsersPercent}" class="dial-usage1" data-width="100" data-height="100" data-min="0" data-readOnly="true" />
           <p>{$totalUsers} / {$maxUsers}</p>
           <p>
             <a href="clientarea.php?action=productdetails&id={$id}&modop=custom&a=managesmartermaildomain" class="btn btn-default btn-sm">{$LANG.smmanageusers}</a>
           </p>
         </div>
       </div>
       <div class="clearfix"></div>
       
       <!--p class="text-muted">{$LANG.clientarealastupdated}: {$lastupdate}</p-->
       <script src="{$BASE_PATH_JS}/jquery.knob.js"></script>
       <script type="text/javascript">
       jQuery(function() {ldelim}
         jQuery(".dial-usage1").knob({ldelim}
            'change': function (v) {ldelim} console.log(v); {rdelim},
            draw: function () {ldelim}
              $(this.i).val(this.cv + '%');
            {rdelim}
         {rdelim});
       {rdelim});
       </script>
     </div>
   </div>
 </div>
 {/if}

