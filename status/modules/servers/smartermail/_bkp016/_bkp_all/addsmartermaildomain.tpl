{if $message != ""}
	<div class="alert alert-success text-center" id="alertModuleCustomButtonSuccess">
		{$message}
	</div>
{/if}
{if $errormessage != ""}
	<div class="alert alert-danger text-center" id="alertModuleCustomButtonFailed">
		{$errormessage}
	</div>
{/if}
<br clear="both" />

<div style="float:left; width: 123px;">
	<a menuitemname="Custom Module Button Gerenciar Domínio" style="background: #337ab7 !important; color:#ffffff !important" href="clientarea.php?action=productdetails&amp;id={$id}&amp;modop=custom&amp;a=managesmartermaildomain" class="list-group-item btn btn-primary" id="Primary_Sidebar-Service_Details_Actions-Custom_Module_Button_Gerenciar_Domínio"> {$LANG.smglobalback}</a>
</div><br clear="both" />
<form method="post" action="clientarea.php?action=productdetails" class="form-horizontal using-password-strength" >
    <h3>{$LANG.smaddDomain}</h3>
    <div class="form-group">
        <label for="inputalias" class="col-sm-5 control-label">{$LANG.smDomain}</label>
        <div class="col-sm-6">
            <input type="text" name= "domain"  style="width: 100%;"  class="form-control" />
        </div>
    </div> 
    <div class="form-group">
        <div class="text-center">
           <input type="hidden" name="id" value="{$id}" />
			<input type="hidden" name="modop" value="custom" />
			<input type="hidden" name="a" value="createsmartermaildomain" />
			<input type="submit" value="{$LANG.smglobalsave}" class="btn btn-primary" /> 
        </div>
    </div>
</form>
