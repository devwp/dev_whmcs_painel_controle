<!DOCTYPE html>

<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
</head>
<body>
    {if $message != ""}
    <div class="alert alert-success text-center" id="alertModuleCustomButtonSuccess">
        {$message}
    </div>
    <br clear="both" />
    {/if}
    {if $errormessage != ""}
    <div class="alert alert-danger text-center" id="alertModuleCustomButtonFailed">
        {$errormessage}
    </div>
    <br clear="both" />
    {/if}

	<div class="row">
        <div class="col-md-12">
            <ul class="nav nav-tabs">
                <li><a href="clientarea.php?action=productdetails&id={$id}">{$LANG.yourdetails} </a></li>
                {if $IsDedicated == 'on'}
	    <li class="active"><a href="">{$LANG.smmanageDomain}</a></li>
                {/if}
            </ul>
        </div>
    </div>

    <link rel="stylesheet" type="text/css" href="{$BASE_PATH_CSS}/dataTables.bootstrap.css">
    <link rel="stylesheet" type="text/css" href="{$BASE_PATH_CSS}/dataTables.responsive.css">
    <script type="text/javascript" charset="utf8" src="{$BASE_PATH_JS}/jquery.dataTables.min.js"></script>
    <script type="text/javascript" charset="utf8" src="{$BASE_PATH_JS}/dataTables.bootstrap.min.js"></script>
    <script type="text/javascript" charset="utf8" src="{$BASE_PATH_JS}/dataTables.responsive.min.js"></script>


    <div style="float: left; width: 123px;">
        <a menuitemname="Custom Module Button Gerenciar Domínio" style="background: #337ab7 !important; color: #ffffff !important" href="clientarea.php?action=productdetails&amp;id={$id}&amp;modop=custom&amp;a=managesmartermaildomain" class="list-group-item btn btn-primary" id="Primary_Sidebar-Service_Details_Actions-Custom_Module_Button_Gerenciar_Domínio">{$LANG.smglobalback}</a>
    </div>
    <br clear="both" />

    <script type="text/javascript">
        jQuery(document).ready(function () {
            var table = jQuery("#tableDomainList").DataTable({
                "dom": '<"listtable"fit>pl', "responsive": true,
                "oLanguage": {
                    "sEmptyTable": "{$LANG.smglobalNothing}",
                    "sInfo": "{$LANG.smmanageDomain}, {$totalDomains} de {$maxDomains}",
                    "sInfoEmpty": "{$LANG.smmanageDomain}, {$totalDomains} de {$maxDomains}",
                    "sInfoFiltered": "({$LANG.smglobalshowing} _MAX_ total)",
                    "sInfoPostFix": "",
                    "sInfoThousands": ",",
                    "sLengthMenu": "{$LANG.smglobalshow} _MENU_",
                    "sLoadingRecords": "{$LANG.smgloballoading}",
                    "sProcessing": "{$LANG.smglobalProcess}",
                    "sSearch": "",
                    "sZeroRecords": "{$LANG.smglobalNothing}",
                    "oPaginate": {
                        "sFirst": "",
                        "sLast": "",
                        "sNext": "{$LANG.smglobalNext}",
                        "sPrevious": "{$LANG.smglobalPrev}"
                    }
                },
                "pageLength": 10,
                "columnDefs": [{
                    "targets": 'no-sort',
                    "orderable": false,
                }],
                "order": [
                    [0, "asc"]
                ],
                "stateSave": true
            });
            jQuery('#tableDomainList').removeClass('hidden').DataTable();
            table.draw();
            jQuery("#tableDomainList_filter input").attr("placeholder", "{$LANG.smglobalsearch}");
        });
    </script>

    <script src="/assets/js/bootstrap-tabdrop.js"></script>
    <script type="text/javascript">
        jQuery('.nav-tabs').tabdrop();
    </script>
    <div class="tabbable">
        <div class="row clearfix">
            <div class="col-xs-12">
                <ul class="nav nav-tabs">
                    <li>
                        <a href="#domain" data-toggle="tab">{$LANG.smmanageusers}</a>
                    </li>



                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12"> 
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">{$LANG.smmanageDomain}</h3>
                    </div>
                    <div class="panel-body">
                        <div style="float: left; width: 180px;">
                            {if $canCreateDomain}
				    <a href="#addsmartermailDomain" class="editmodal btn btn-primary" data-toggle="modal" data-target="#myModalAddDomain"><i class="fa fa-plus-circle" aria-hidden="true"></i>&nbsp;&nbsp;{$LANG.smaddDomain}</a>
                            {/if}
                        </div>
                        <div class="tab-content">
                            <br clear="both" />
                            <div id="domain" class="tab-pane active">
                                <table id="tableDomainList" class="table table-bordered table-hover table-list">
                                    <thead>
                                        <tr>
                                            <th>{$LANG.domainname}</th>
                                            <th class="no-sort"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {if $totalDomains > 0}
							    {foreach $wsGetDomainsResultArray as $key => $val }
								    <tr>
                                        <td><a href="clientarea.php?action=productdetails&id={$id}&modop=custom&a=managesmartermaildomains&d={$val}" title="{$LANG.smglobalshow}">{$val} </a></td>
                                        <td style="width: 45px;" data-order="{$val.PercentUsed}">
                                            <div>
                                                <form method="post">
                                                    <input type="hidden" name="deldomainname" value="{$val}" />
                                                    <input type="hidden" name="managed" value="deldomain" />
                                                    <a href="#" onclick="if(confirm('Esta ação irá apagar todas as contas de email e todos os emails nas caixas de entrada.\nDeseja continuar?')){ parentNode.submit(); } else { return false; }" title="{$LANG.smglobalDelete} {$LANG.smDomain} {$val}"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                                                </form>

                                            </div>
                                        </td>

                                    </tr>
                                        {/foreach}
							    {/if}
                                    </tbody>
                                </table>
                            </div>

                        


                            <br clear="both" />
                            <br clear="both" />

                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
</body>
</html>
