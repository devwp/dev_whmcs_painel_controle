{foreach from=$js_paths item=path}
    <script type="text/javascript" src="{$path}"></script>
{/foreach}

{foreach from=$css_paths item=path}
    <link rel="stylesheet" type="text/css" href="{$path}">
{/foreach}

<div class="rex_center_container">

    {if $success == '1'}
        <div class="mg-successbox">
            <strong>
                <span class="title">{$lang.success}</span>
            </strong>
        </div>
    {/if}

    {if count($errors) > 0 || count($infos) > 0}
        {foreach from=$errors item=error}
            <p class="leftp"><span class="bold">{$lang.error}</span> {$error}</p>
        {/foreach}
        {foreach from=$infos item=info}
            <p class="leftp">{$info}</p>
        {/foreach}
    {/if}

    <hr>

    <div class="form-container">

        {$content}

    </div>

</div>

<div style="clear: both;"></div>