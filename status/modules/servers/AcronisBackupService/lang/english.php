<?php
/**
 * @Copyright © 2002-2016 Acronis International GmbH. All rights reserved
 */
$_LANG['module']['acronis_standard'] = 'Acronis Backup';
$_LANG['management'] = 'Management';
$_LANG['back_to_summary'] = 'Back to summary';
$_LANG['email_accounts_management'] = 'Create Backup Administrator';
$_LANG['login_to_backup_console'] = 'Go to backup console';
$_LANG['login_to_management_console'] = 'Go to management console';

//config options descriptions. ?? will be replaced to <BR> (new line)
$_LANG['config']['maxWorkstations'] = '?? Number of protected workstations.  ?? Type in "unlimited" or negative value to set no limit. ?? Can be dynamic - type in name of configurable option to use it.';
$_LANG['config']['maxStorageGB'] = 'GB. ?? Type in "unlimited" or negative value to set no limit. ?? Can be dynamic - type in name of configurable option to use it.';
$_LANG['config']['maxServers'] = '?? Number of protected servers. ?? Type in "unlimited" or negative value to set no limit.?? Can be dynamic - type in name of configurable option to use it.';
$_LANG['config']['accountStorage'] = '?? Choose storage that belongs to the selected Server Group where the backups will be kept. ?? Changing this option will not impact the existing subscriptions.';
$_LANG['config']['maxVMs'] = '?? Number of protected virtual machines. ?? Type in "unlimited" or negative value to set no limit. ?? Can be dynamic - type in name of configurable option to use it.';
$_LANG['config']['accountType'] = '?? Select account type.';
$_LANG['config']['maxMobileDevices'] = '?? Number of protected mobile devices. ?? Type in "unlimited" or negative value to set no limit.?? Can be dynamic - type in name of configurable option to use it.';
$_LANG['config']['maxO365Mailboxes'] = '?? Number of protected Office 365 mailboxes. ?? Type in "unlimited" or negative value to set no limit.?? Can be dynamic - type in name of configurable option to use it.';
$_LANG['config']['storageLocation'] = '?? Select a backup location.';

//Products
$_LANG['cant_load_storage_list'] = 'Can not load storage list. Please select valid Server Group and make sure that server credentials are correct. Error message: ??';

//Errors:
$_LANG['errors']['form_not_complete'] = 'Invalid form parameters. Please refresh the web page and try again.';
$_LANG['errors']['login_too_long'] = '?? cannot be longer than 64 characters.';
$_LANG['errors']['login_too_short'] = '?? cannot be shorter than 3 characters.';
$_LANG['errors']['username_reserved'] = 'This name is reserved.';
$_LANG['errors']['password_doesnt_match'] = 'The passwords do not match.';
$_LANG['errors']['password_characters_including'] = 'The specified password is weak. Ensure that your password meets the minimum security requirements: use 6 or more characters; use both lowercase and uppercase letters; use one or more numerical digits.';
$_LANG['errors']['invalid_login'] = 'Login is invalid.';
$_LANG['errors']['invalid_email'] = 'Email is invalid.';
$_LANG['errors']['empty_login'] = 'Login cannot be empty.';
$_LANG['errors']['empty_email'] = 'Email cannot be empty.';

$_LANG['errors']['empty_firstname'] = 'First name cannot be empty.';
$_LANG['errors']['empty_lastname'] = 'Last name cannot be empty.';

$_LANG['error'] = 'Error:';
$_LANG['success'] = 'Success';


//Fields:
$_LANG['field']['username'] = 'User name';
$_LANG['field']['name'] = 'Name';
$_LANG['field']['login'] = 'Login';
$_LANG['field']['email'] = 'Email';
$_LANG['field']['firstname'] = 'First name';
$_LANG['field']['lastname'] = 'Last name';
$_LANG['field']['server_count'] = 'Number of servers';
$_LANG['field']['storage_size'] = 'Storage size [GB]';
$_LANG['field']['vm_count'] = 'Number of virtual machines';
$_LANG['field']['workstation_count'] = 'Number of workstations';
$_LANG['field']['user_details'] = 'Create new account';
$_LANG['field']['password'] = 'Password';
$_LANG['field']['confirmpassword'] = 'Confirm password';

$_LANG['order_input_credentials_string'] = 'Please specify credentials for your new backup account.';

//Buttons:
$_LANG['add'] = 'Create new account';
$_LANG['edit'] = 'Edit';
$_LANG['delete'] = 'Delete';
$_LANG['add_account'] = 'Create';
$_LANG['save_account'] = 'Save';
$_LANG['cancel_account'] = 'Cancel';
$_LANG['backtoserviceinfo'] = 'Back to Product Overview';

//Group details:
$_LANG['group_usage_details'] = 'Service usage details';
$_LANG['used_servers_count'] = 'Servers';
$_LANG['used_storage_size'] = 'Cloud storage';
$_LANG['used_vm_count'] = 'Virtual machines';
$_LANG['used_workstations_count'] = 'Workstations';
$_LANG['used_mobile_devices_count'] = 'Mobile devices';
$_LANG['used_mailboxes_count'] = 'Office 365 mailboxes';

//Client Area
$_LANG['clientarea']['used'] = 'Used';
$_LANG['clientarea']['of'] = 'of';
$_LANG['clientarea']['accountnotfound'] = 'The backup user is not found.';
$_LANG['clientarea']['account'] = $_LANG['field']['login'];

