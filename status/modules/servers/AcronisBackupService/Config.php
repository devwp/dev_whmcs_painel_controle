<?php

if (!defined('WHMCS')) die("This file cannot be accessed directly");   //only WHMCS

//service name
define('ACRONIS_SERVICE_NAME', 'AcronisBackupService');
// directory for debug logs. directory must existscd  and be accessible for apache user
define('LOG_DIR_NAME', 'acronis');
//directory separator
defined('DS') ? null : define('DS', DIRECTORY_SEPARATOR);
//full path to whmcs directory index
defined('WHMCS_MAIN_DIR') ? null : define('WHMCS_MAIN_DIR', substr(dirname(__FILE__), 0, strpos(dirname(__FILE__), 'modules' . DS . 'servers')) . DS);
//full path to AcronisBackupService module directory
defined('ACRONISSP_OWN_DIR') ? null : define('ACRONISSP_OWN_DIR', substr(dirname(__FILE__), 0, strpos(dirname(__FILE__), DS . ACRONIS_SERVICE_NAME)) . DS . ACRONIS_SERVICE_NAME . DS);
//full path to AcronisBackupService module directory with client (htms,css) data
defined('ACRONISSP_OWN_BROWSER_CLIENT_DIR') ? null : define('ACRONISSP_OWN_BROWSER_CLIENT_DIR', 'modules/servers/' . ACRONIS_SERVICE_NAME . '/');
//full path to includes dir. basically it is whmcs_root/incldes
defined('ACRONIS_INCLUDES_DIR') ? null : define('ACRONIS_INCLUDES_DIR', substr(dirname(__FILE__), 0, strpos(dirname(__FILE__), DS . 'modules')) . DS . 'includes' . DS . 'Acronis' . DS);
//full path to file contains package version
defined('VERSION_FILE_PATH') ? null : define('VERSION_FILE_PATH', ACRONIS_INCLUDES_DIR . DS . 'Vers.ion');
//mask for HTTPD User-Agent header
defined('PACKAGE_USER_AGENT') ? null : define('PACKAGE_USER_AGENT', 'ci-whmcs-abc-version');
//full path to file with iso languages json list
defined('ISO_LANGUAGE_MAP') ? null : define('ISO_LANGUAGE_MAP', ACRONISSP_OWN_DIR . 'lang/iso-languages.json');
//full path to log file if debug mode is on
defined('LOG_PATH') ? null : define('LOG_PATH', '/var/log/' . LOG_DIR_NAME);
//full path to main smarty class
defined('SMARTY_FILE') ? null : define('SMARTY_FILE', WHMCS_MAIN_DIR . 'includes' . DS . 'smarty' . DS . 'Smarty.class.php');

require_once dirname(__FILE__) . DS . 'handlers.php';

$autoloader = AcronisBackupService_loadAutoloader();
$config = new Acronis\Standard\Configs\ModuleConfig();
$config->loadConfig();
$logger = \Acronis\Log::createLogger($config);
\Acronis\Log::getInstance()->setLogger($logger);
AcronisBackupService_unloadAutoloader($autoloader);