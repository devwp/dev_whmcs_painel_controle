{**********************************************************************
 *  Interspire Email Marketer for WHMCS (31.07.2014)
 * *
 *
 *  CREATED BY MODULESGARDEN       ->        http://modulesgarden.com
 *  CONTACT                        ->       contact@modulesgarden.com
 *
 *
 * This software is furnished under a license and may be used and copied
 * only  in  accordance  with  the  terms  of such  license and with the
 * inclusion of the above copyright notice.  This software  or any other
 * copies thereof may not be provided or otherwise made available to any
 * other person.  No title to and  ownership of the  software is  hereby
 * transferred.
 *
 * @version 1.0.0 (31.07.2014)
 * @author Pawel <pawel@modulesgarden.com>
 *
 **********************************************************************}
{literal}
<script src="includes/jscript/base64.js"></script>
{/literal}

{literal}
<style type="text/css">
    .infotable_int{
        width:100%; 
        border-radius: 14px 14px 0 0;
        border-style: solid;
        border-width: 1px; 
        border-color: #ddd; 
         
    }
    
    .infotable_int th{
        text-align: center;
        padding:10px; 
        font-size:16px;
        color:#058;
        font-weight:bold;
    }
    .infotable_int td{
        padding:3px;
        padding-left: 10px;
        padding-right: 10px;
    }
    .infotable_int td.keyinfo_int{
        width:35%; 
        text-align:right;
        font-weight:bold;
    }
    .infotable_int td.valueinfo_int{
        text-align: center;
    }
    
    .infotable_int tr:nth-child(2n){
        background-color: #efefef;
    }
    
</style>
{/literal}

{if $error}
    <div class="alert alert-danger"><strong>{$error}</strong></div>
{else}
    {if !empty($stats)}
    <h2 style="margin: 0px 0px 20px 10px" class="">{$IEMLANG.userstats}</h2>
    {/if}
    <table class="infotable_int">
        {if $config.newsletters_sent}
        <tr>
            <td class="keyinfo_int">{$IEMLANG.newsletters_sent}</td>
            <td class = "valueinfo_int">{$stats.newsletter.newsletters_sent}</td>
        </tr>
        {/if}
        {if $config.total_emails_sent}
        <tr>
            <td class="keyinfo_int">{$IEMLANG.total_emails_sent}</td>
            <td class = "valueinfo_int">{$stats.newsletter.total_emails_sent}</td>
        </tr>
        {/if}
        {if $config.unique_opens}
        <tr>
            <td class="keyinfo_int">{$IEMLANG.unique_opens}</td>
            <td class = "valueinfo_int">{$stats.newsletter.unique_opens}</td>
        </tr>
        {/if}
        {if $config.total_opens}
        <tr>
            <td class="keyinfo_int">{$IEMLANG.total_opens}</td>
            <td class = "valueinfo_int">{$stats.newsletter.total_opens}</td>
        </tr>
        {/if}
        {if $config.total_bounces}
        <tr>
            <td class="keyinfo_int">{$IEMLANG.total_bounces}</td>
            <td class = "valueinfo_int">{$stats.newsletter.total_bounces}</td>
        </tr>
        {/if}
        {if $config.autoresponders}
        <tr>
            <td class="keyinfo_int">{$IEMLANG.autoresponders}</td>
            <td class = "valueinfo_int">{$stats.autoresponders}</td>
        </tr>
        {/if}
        {if $config.mailinglists}
        <tr>
            <td class="keyinfo_int">{$IEMLANG.mailinglists}</td>
            <td class = "valueinfo_int">{$stats.mailinglists}</td>
        </tr>
        {/if}
        {if $config.lastnewsletter}
        <tr>
            <td class="keyinfo_int">{$IEMLANG.lastnewsletter}</td>
            <td class = "valueinfo_int">{$stats.lastnewsletter}</td>
        </tr>
        {/if}
        {if $config.loginbutton}
        <tr>
            <th colspan="2">
                <button style="margin: 15px 0px 0px 0px" class="btn btn-primary" onclick="{$loginscript}">{$IEMLANG.logintoiem}</button>
            </th>
        </tr>
        {/if}
    </table>
{/if}