<?php


/**
 * @author Maciej Husak <maciej@modulesgarden.com>
 */
ob_clean();
$vm = new NewOnApp_VM($_GET['vserver']);
$vm->setconnection($params);
if ($vm->available($user_id)) {
    if(!$product->getConfig('console')){ //vnc
            ob_clean();
            $console = $vm ->getConsoleKey();
            $url = 'http'.($params['serversecure']=='on' ? 's' :'').'://'.(empty($params['serverhostname']) ? $params['serverip'] : $params['serverhostname']);
            $key =  $console['remote_access_session']['remote_key'];
            header("Location: {$url}/console_remote/{$key}");
            die();
    }else{ // html5
          $details = $vm->getDetails();
          $vars['vm']     = $details['virtual_machine'];
          $vars['params'] = $params;
          $vars['console']= $vm->getConsoleKey();
    }
      
} else $vars['msg_error'] = $lang['mainsite']['perm_error'];

