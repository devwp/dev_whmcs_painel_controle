<?php

/**
 * @author Maciej Husak <maciej@modulesgarden.com>
 */

$lang['module']['error1']                   = 'Invalid "EmailCustomerID" parameter in clients fields';
$lang['module']['error2']                   = 'Invalid domain name';
$lang['module']['error3']                   = 'Unable to load page file'; 
$lang['module']['error4']                   = 'Unable to load css styles';
$lang['module']['error5']                   = 'Wrong page';
$lang['module']['error6']                   = 'We are sorry, an error occurred (Module License Problem). Please contact support in order to solve this problem';
$lang['module']['error7']                   = 'VM not found';

/**************************
 * 
 *       MAIN SITE
 * 
 **************************/
$lang['mainsite']['main_header']            = 'Your Virtual Servers:';
$lang['mainsite']['create_virtual_server']  = 'Create Virtual Server';
$lang['mainsite']['no_virtual_servers']     = 'No virtual servers yet';
$lang['mainsite']['hostname']               = 'Hostname';
$lang['mainsite']['manage']                 = 'Manage';
$lang['mainsite']['delete']                 = 'Delete';
$lang['mainsite']['your_resources']         = 'Your Resources:';
$lang['mainsite']['of']                     = 'of';
$lang['mainsite']['used']                   = 'Used';
$lang['mainsite']['memory']                 = 'Memory';
$lang['mainsite']['free']                   = 'Free';
$lang['mainsite']['ip_addresses']           = 'IP Addresses';
$lang['mainsite']['vm']                     = 'Virtual&nbsp;Servers';
$lang['mainsite']['cpus']                   = 'CPU(s)';
$lang['mainsite']['shares']                 = 'CPU Priority';
$lang['mainsite']['memory_size']            = 'Memory Size';
$lang['mainsite']['disk_size']              = 'Disk Size';
$lang['mainsite']['GB']                     = 'GB';
$lang['mainsite']['MB']                     = 'MB';
$lang['mainsite']['yes']                    = 'Yes';
$lang['mainsite']['no']                     = 'No';
$lang['mainsite']['backups']                = 'Backups';
$lang['mainsite']['please_wait']            = 'Please wait...';
$lang['mainsite']['confirm']                = 'Are you sure you want to delete this virtual server?';
$lang['mainsite']['perm_error']             = 'You don\'t have permission to make this action.';
$lang['mainsite']['vm_deleted']             = 'VM has been scheduled for destruction.';
$lang['mainsite']['vm_deleted_error']       = 'Unexpected error.';
$lang['mainsite']['active']                 = 'Active';
$lang['mainsite']['unlimited']              = 'Unlimited';
$lang['mainsite']['rate_limit']             = 'Port Speed';
$lang['mainsite']['more_usage_records']     = 'Show more usage records';

$lang['mainsite']['apiInfo']           = 'API Info';


/**************************
 * 
 *       FIREWALL
 * 
 **************************/
$lang['firewall']['main_header']            = 'Firewall Rules:';
$lang['firewall']['add_firewall_rule']      = 'Add A Firewall Rule';
$lang['firewall']['interface']              = 'Interface';
$lang['firewall']['command']                = 'Command';
$lang['firewall']['source_address']         = 'Source Address';
$lang['firewall']['destination_port']       = 'Destination Port';
$lang['firewall']['protocol']               = 'Protocol';
$lang['firewall']['actions']                = 'Actions';
$lang['firewall']['add_rule']               = 'Add Rule';
$lang['firewall']['rule_deleted']           = 'Rule deleted.';
$lang['firewall']['nothing_label']          = 'Nothing to display';
$lang['firewall']['edit']                   = 'Edit';
$lang['firewall']['delete']                 = 'Delete';
$lang['firewall']['save']                   = 'Save';
$lang['firewall']['delete_selected']        = 'Delete Selected';
$lang['firewall']['rule_added']             = 'Rule has been added.';
$lang['firewall']['rule_removed']           = 'Rule has been destroyed.';
$lang['firewall']['default_rule_updated']   = 'Default rules has been updated.';
$lang['firewall']['default_firewall_rule']  = 'Default Firewall Rules';
$lang['firewall']['save_default_rule']      = 'Save Default Firewall Rules';
$lang['firewall']['back']                   = 'Back To Service';
$lang['firewall']['success']                = 'Success';
$lang['firewall']['apply']                  = 'Apply Firewall Rules';
$lang['firewall']['rule_updated']           = 'Rules has been updated.';



/**************************
 * 
 *       IP MANAGEMENT
 * 
 **************************/
$lang['manageip']['main_header']            = 'List of Network IP Addresses:';
$lang['manageip']['ip_addresses']           = 'IP Addresses';
$lang['manageip']['ip_address']             = 'IP Address';
$lang['manageip']['netmask']                = 'Netmask';
$lang['manageip']['gateway']                = 'Gateway';
$lang['manageip']['physical_network']       = 'Physical Network';
$lang['manageip']['actions']                = 'Actions';
$lang['manageip']['nothing_label']          = 'Nothing To Display';
$lang['manageip']['delete']                 = 'Delete';
$lang['manageip']['back']                   = 'Back To Service';
$lang['manageip']['rebuild_network']        = 'Rebuild Network';
$lang['manageip']['rebuilded']              = 'Network interface will be rebuilt for this Virtual machine.';
$lang['manageip']['add_ip_header']          = 'Add New IP Address';
$lang['manageip']['submit']                 = 'Submit';
$lang['manageip']['add_ip']                 = 'Add New IP Address';
$lang['manageip']['identification']         = 'Identification';
$lang['manageip']['network_interface']       =      'Network Interface';
$lang['manageip']['ip_addresses']           =      'IP Addresses';
$lang['manageip']['ipadded']                = 'IP Address has been added.';
$lang['manageip']['action']                 = 'Action';
$lang['manageip']['delete']                 = 'Delete';
$lang['manageip']['ip_deleted']             = 'IP Address has been removed.';
$lang['manageip']['interface']             = 'Id';




/**************************
 * 
 *       CPU USAGE
 * 
 **************************/
$lang['stats']['main_header']               = 'CPU Usage Graphs';
$lang['stats']['invalid_data']              = 'Error: Invalid data';
$lang['stats']['back']                      = 'Back To Service';



/**************************
 * 
 *       REBUILD
 * 
 **************************/
$lang['rebuild']['main_header']             = 'Rebuild';
$lang['rebuild']['template']                = 'Template';
$lang['rebuild']['back']                    = 'Back To Service';
$lang['rebuild']['select']                  = 'Select';
$lang['rebuild']['rebuild']                 = 'Rebuild';
$lang['rebuild']['select_template']         = 'Please select a template.';
$lang['rebuild']['vps_rebuilded']           = 'Virtual machine build has been queued, and will take place in a moment.';
$lang['rebuild']['confirm_rebuild']         = 'Are you sure to make this action? All data located on virtual machine will be lost.';
$lang['rebuild']['attention']               = 'If you continue, all data located on virtual machine will be lost!';



/**************************
 * 
 *       BACKUPS
 * 
 **************************/
$lang['backups']['main_header']            = 'Backups For This Virtual Machine';
$lang['backups']['datetime']               = 'Date/Time';
$lang['backups']['disk']                   = 'Disk';
$lang['backups']['status']                 = 'Status';
$lang['backups']['backup_size']            = 'Backup Size';
$lang['backups']['backup_type']            = 'Backup Type';
$lang['backups']['backup_server']          = 'Backup Server';
$lang['backups']['nothing_label']          = 'Nothing To Display';
$lang['backups']['delete']                 = 'Delete';
$lang['backups']['restore']                = 'Restore';
$lang['backups']['back']                   = 'Back To Service';
$lang['backups']['actions']                = 'Actions';
$lang['backups']['MB']                     = 'MB';
$lang['backups']['built']                  = 'Built';
$lang['backups']['running']                = 'Running';
$lang['backups']['create_template']        = 'Convert Backup to Template';
$lang['backups']['backup_deleted']         = 'Backup has been scheduled for removal.';
$lang['backups']['backup_restored']        = 'Backup restore has been scheduled.';
$lang['backups']['not_built']              = 'Not built yet';
$lang['backups']['confirm_restore']        = 'Are you sure you want to restore this backup? All data located on virtual machine will be overwritten.';
$lang['backups']['confirm_delete']         = 'Are you sure you want to delete this backup?';


/**************************
 * 
 *       DISK MANAGE
 * 
 **************************/
$lang['disk']['main_header']                = 'Disks';
$lang['disk']['disk']                       = 'Disk';
$lang['disk']['label']                      = 'Label';
$lang['disk']['size']                       = 'Size';
$lang['disk']['data_store']                 = 'Data Store';
$lang['disk']['type']                       = 'Type';
$lang['disk']['nothing_label']              = 'Nothing To Display';
$lang['disk']['built']                      = 'Built';
$lang['disk']['backups']                    = 'Backups';
$lang['disk']['backup']                     = 'Backup';
$lang['disk']['back']                       = 'Back To Service';
$lang['disk']['actions']                    = 'Actions';
$lang['disk']['MB']                         = 'MB';
$lang['disk']['GB']                         = 'GB';
$lang['disk']['yes']                        = 'Yes';
$lang['disk']['no']                         = 'No';
$lang['disk']['primary']                    = '(primary)';
$lang['disk']['swap']                       = 'Swap';
$lang['disk']['autobackups']                = 'Autobackup';
$lang['disk']['create_backup']              = 'Take Backup';
$lang['disk']['backup_created']             = 'Backup has been created and will be taken shortly.';
$lang['disk']['schedule']                   = 'Schedule for backups';
$lang['disk']['standard']                   = 'Standard';
$lang['disk']['create_disk']                = 'Create Disk';
$lang['disk']['swap_space']                 = 'Swap Space';
$lang['disk']['require_format']             = 'Require Format Disk';
$lang['disk']['add_to_linux']               = 'Add to Linux FSTAB';
$lang['disk']['mount_point']                = 'Mount Point';
$lang['disk']['add_disk']                   = 'Add Disk';
$lang['disk']['header_add_label']           = 'Add New Disk';
$lang['disk']['disk_added']                 = 'Disk has been added successfully.';
$lang['disk']['delete_disk']                = 'Delete Disk';
$lang['disk']['confirm_delete']             = 'Are you sure you want to remove this disk? WARNING: All backups made for this disk will be removed too.';
$lang['disk']['disk_removed']               = 'Disk has been scheduled for destruction.';
$lang['disk']['disk_size_error'] = 'You Are Not Able To Set %s GB Of Disk Space. Space Available: %s GB';


/**************************
 * 
 *       ADD SERVER
 * 
 **************************/
$lang['addserver']['main_header']           = 'New Server Details';
$lang['addserver']['hostname']              = 'Hostname';
$lang['addserver']['template']              = 'Template';
$lang['addserver']['root_password']         = 'Root Password';
$lang['addserver']['memory']                = 'Memory';
$lang['addserver']['cpu_cores']             = 'CPU(s)';
$lang['addserver']['cpu_shares']            = 'CPU Priority';
$lang['addserver']['primary_disk_size']     = 'Primary Disk Size';
$lang['addserver']['swap_disk_size']        = 'Swap Disk Size';
$lang['addserver']['ip_addresses']          = 'IP Addresses';
$lang['addserver']['btn_add']               = 'Add New Virtual Server';
$lang['addserver']['back']                  = 'Back To Service';
$lang['addserver']['label']                 = 'Label';
$lang['addserver']['vps_created']           = 'Virtual machine has been created successfully, and its build has been queued.';
$lang['addserver']['on']                    = 'ON';
$lang['addserver']['off']                   = 'OFF';
$lang['addserver']['rate_limit']            = 'Port Speed';
$lang['addserver']['primary_data_store']    = 'Primary Data Store';
$lang['addserver']['swap_data_store']       = 'Swap Data Store';
$lang['addserver']['network_zone']          = 'Network Zone';
$lang['addserver']['licensing_key']         = 'Licensing Key'; 

/**************************
 * 
 *       EDIT SERVER
 * 
 **************************/
$lang['editserver']['main_header']           = 'Edit Server Details';
$lang['editserver']['hostname']              = 'Hostname';
$lang['editserver']['template']              = 'Template';
$lang['editserver']['new_root_password']     = 'New Root Password';
$lang['editserver']['memory']                = 'Memory';
$lang['editserver']['cpu_cores']             = 'CPU(s)';
$lang['editserver']['primary_disk_size']     = 'Primary Disk Size';
$lang['editserver']['swap_disk_size']        = 'Swap Disk Size';
$lang['editserver']['ip_addresses']          = 'IP Addresses';
$lang['editserver']['btn_add']               = 'Save';
$lang['editserver']['back']                  = 'Back To Service';
$lang['editserver']['label']                 = 'Label';
$lang['editserver']['vps_created']           = 'Virtual machine has been created successfully, and its build has been queued.';
$lang['editserver']['vm_updated']            = 'VM updated successfully.';
$lang['editserver']['leave_blank_pass']      = 'Leave password blank if don\'t want to change.';
$lang['editserver']['on']                    = 'ON';
$lang['editserver']['off']                   = 'OFF';
$lang['editserver']['rate_limit']            = 'Port Speed';
$lang['editserver']['cpu_shares']            = 'CPU Priority';


/**************************
 * 
 *       VM DETAILS
 * 
 **************************/
$lang['vmdetails']['main_header']            = 'Manage Your Server:';
$lang['vmdetails']['server_status']          = 'Refresh Details:';
$lang['vmdetails']['vm_details']             = 'Virtual Server Details:';
$lang['vmdetails']['loading']                = 'Loading...';
$lang['vmdetails']['reboot']                 = 'Reboot';
$lang['vmdetails']['backup']                 = 'Backups Management';
$lang['vmdetails']['reset_pass']             = 'Reset Root Password';
$lang['vmdetails']['reset_pass_confirm']     = 'Are you sure you want to reset the root password?';
$lang['vmdetails']['start']                  = 'Start VM';
$lang['vmdetails']['shutdown']               = 'Shutdown VM';
$lang['vmdetails']['stop']                   = 'Stop VM';
$lang['vmdetails']['recovery']               = 'Startup On Recovery';
$lang['vmdetails']['rebuild']                = 'Rebuild';
$lang['vmdetails']['built']                  = 'Built';
$lang['vmdetails']['booted']                 = 'Booted';
$lang['vmdetails']['cpus']                   = 'CPU(s)';
$lang['vmdetails']['shares']                 = 'CPU Priority';
$lang['vmdetails']['memory_size']            = 'Memory Size';
$lang['vmdetails']['disk_size']              = 'Disk Size';
$lang['vmdetails']['primary_disk_size']      = 'Primary Disk Size';
$lang['vmdetails']['swap_disk_size']         = 'Swap Disk Size';
$lang['vmdetails']['GB']                     = 'GB';
$lang['vmdetails']['MB']                     = 'MB';
$lang['vmdetails']['yes']                    = 'Yes';
$lang['vmdetails']['no']                     = 'No';
$lang['vmdetails']['ip']                     = 'IP Address';
$lang['vmdetails']['network_speed']          = 'Network Speed';
$lang['vmdetails']['created_at']             = 'Created At';
$lang['vmdetails']['template_image']         = 'Template'; 
$lang['vmdetails']['recovery_mode']          = 'Recovery Mode';
$lang['vmdetails']['monthly_bandwidth_used'] = 'Monthly Bandwidth Used';
$lang['vmdetails']['label']                  = 'Label';
$lang['vmdetails']['updated_at']             = 'Last Update';
$lang['vmdetails']['console']                = 'Open Console';
$lang['vmdetails']['vmconsole']              = 'VM Console';
$lang['vmdetails']['success']                = 'Success';
$lang['vmdetails']['additionals']            = 'Additional Tools:';
$lang['vmdetails']['firewall_manage']        = 'Firewall Management';
$lang['vmdetails']['ip_manage']              = 'IP Management';
$lang['vmdetails']['graphs']                 = 'CPU Usage Graphs';
$lang['vmdetails']['back']                   = 'Back To Service';
$lang['vmdetails']['error7']                 = 'VM not found';
$lang['vmdetails']['backups']                = 'Backups';
$lang['vmdetails']['disk_manage']            = 'Disk Management';
$lang['vmdetails']['edit_vm']                = 'Edit VM';
$lang['vmdetails']['network']                = 'Network Management';
$lang['vmdetails']['logs']                   = 'Activity Logs';
$lang['vmdetails']['logs']                   = 'Activity Logs';
$lang['vmdetails']['date']                   = 'Date';
$lang['vmdetails']['action']                 = 'Action';
$lang['vmdetails']['status']                 = 'Status';
$lang['vmdetails']['nothing_label']          = 'Currently there are no logs available. ';
$lang['vmdetails']['unlock']                 = 'Unlock';
$lang['vmdetails']['autoscalling']           = 'Autoscaling';
$lang['vmdetails']['password']='Password';
$lang['vmdetails']['show']='Show';
$lang['vmdetails']['change']='Change';
$lang['vmdetails']['hide']='Hide';
$lang['vmdetails']['change_pass_success']                   = 'Service Password Changed Successfully';


/**************************
 * 
 *       NETWORK
 * 
 **************************/
$lang['network']['main_header']             = 'Network Interfaces for this Virtual Server';
$lang['network']['main_header_graph']       = 'Network Interface Usage';
$lang['network']['main_header_add']         = 'Add New Network Interface';
$lang['network']['main_header_edit']        = 'Edit Network Interface';
$lang['network']['back']                    = 'Back To Service';
$lang['network']['interface']               = 'Interface';
$lang['network']['network_join']            = 'Network Join';
$lang['network']['port_speed']              = 'Port Speed';
$lang['network']['primary_interface']       = 'Primary Interface';
$lang['network']['actions']                 = 'Actions';
$lang['network']['unlimited']               = 'Unlimited';
$lang['network']['yes']                     = 'Yes';
$lang['network']['no']                      = 'No';
$lang['network']['nothing_label']           = 'No network interfaces found.';
$lang['network']['interface_deleted']       = 'Network interface has been removed successfully.';
$lang['network']['add_interface']           = 'Add New Network Interface';
$lang['network']['identification']          = 'Identification';
$lang['network']['label']                   = 'Label';
$lang['network']['connectivity']            = 'Connectivity';
$lang['network']['physical_network']        = 'Physical Network';
$lang['network']['submit']                  = 'Submit';
$lang['network']['interface_added']         = 'Network interface has been added successfully.';
$lang['network']['save']                    = 'Save';
$lang['network']['interface_saved']         = 'Network interface has been updated successfully.';
$lang['network']['mbps']                    = 'Mbps';


/**************************
 * 
 *       LOGS
 * 
 **************************/
$lang['logs']['main_header']                = 'Activity Log';
$lang['logs']['back']                       = 'Back To Service';
$lang['logs']['date']                       = 'Date';
$lang['logs']['action']                     = 'Action';
$lang['logs']['status']                     = 'Status';


/**************************
 * 
 *       SCHEDULE
 * 
 **************************/

$lang['schedule']['main_header']            = 'Schedules';
$lang['schedule']['back']                   = 'Back To Service';
$lang['schedule']['target']                 = 'Target';
$lang['schedule']['action']                 = 'Action';
$lang['schedule']['date']                   = 'Date';
$lang['schedule']['duration']               = 'Duration';
$lang['schedule']['period']                 = 'Period';
$lang['schedule']['nextstart']              = 'Next Start';
$lang['schedule']['status']                 = 'Status';
$lang['schedule']['actions']                = 'Actions';
$lang['schedule']['empty']                  = 'No schedules found';
$lang['schedule']['invalid_disk_id']        = 'Invalid disk id';
$lang['schedule']['new_schedule']           = 'New Schedule';
$lang['schedule']['schedule_added']         = 'The schedule was successfully created.';
$lang['schedule']['back']                   = 'Back To Service';
$lang['schedule']['duration']               = 'Duration';
$lang['schedule']['period']                 = 'Period';
$lang['schedule']['save']                   = 'Save';
$lang['schedule']['header_label']           = 'Add a schedule for this Disk';
$lang['schedule']['header_edit_label']      = 'Edit Schedule';
$lang['schedule']['days']                   = 'Days';
$lang['schedule']['weeks']                  = 'Weeks';
$lang['schedule']['months']                 = 'Months';
$lang['schedule']['years']                  = 'Years';
$lang['schedule']['description_add']        = 'To schedule a backup for this disk, complete the form below and then click the Save button.';
$lang['schedule']['description_edit']       = 'To edit the selected schedule, change its properties in the form below and then click the Save button.';
$lang['schedule']['edit']                   = 'Edit';
$lang['schedule']['delete']                 = 'Delete';
$lang['schedule']['invalid_schedule_id']    = 'Invalid Schedule ID';
$lang['schedule']['schedule_deleted']       = 'The schedule was successfully deleted.';
$lang['schedule']['enabled']                = 'Enabled';
$lang['schedule']['disk']                   = 'Disk';
$lang['schedule']['schedule_edited']        = 'The schedule was successfully updated.';


/**************************
 * 
 *       AUTO SCALLING
 * 
 **************************/
$lang['autoscalling']['main_header']        = 'Autoscaling Configuration For This Virtual Server';
$lang['autoscalling']['back']               = 'Back To Service';
$lang['autoscalling']['up']                 = 'UP';
$lang['autoscalling']['down']               = 'Down';
$lang['autoscalling']['ram']                = 'RAM';
$lang['autoscalling']['time']               = 'Time';
$lang['autoscalling']['is_usage_above']     = 'Is Usage Above';
$lang['autoscalling']['is_usage_below']     = 'Is Usage Below';
$lang['autoscalling']['add']                = 'Add';
$lang['autoscalling']['remove']             = 'Remove';
$lang['autoscalling']['MB']                 = 'MB';
$lang['autoscalling']['GB']                 = 'GB';
$lang['autoscalling']['24hr']               = '24hr limit';
$lang['autoscalling']['cpu']                = 'CPU Usage';
$lang['autoscalling']['disk']               = 'Disk Usage';
$lang['autoscalling']['save_changes']       = 'Save Changes';
$lang['autoscalling']['applied']            = 'Applied';
$lang['autoscalling']['value_too_high']     = 'Value too high.';
$lang['autoscalling']['memory_usage']       = 'Memory Usage';
$lang['autoscalling']['cpu_usage']          = 'CPU Usage';
$lang['autoscalling']['disk_usage']         = 'Disk Usage';



$lang['usagerecords']['free']                   = 'Free';
$lang['usagerecords']['usage_records']          = 'Usage Records';
$lang['usagerecords']['usage_records_pricing']  = 'Pricing For Usage Records';
$lang['usagerecords']['credit_billing']         = 'Credit Billing';
$lang['usagerecords']['current_credit']         = 'Credits Used For This Service';
$lang['usagerecords']['current_paid']           = 'Already Paid For This Service';
$lang['usagerecords']['record']                 = 'Record';
$lang['usagerecords']['usage']                  = 'Usage';
$lang['usagerecords']['total']                  = 'Total';
$lang['usagerecords']['resource_deleted']       = 'Resource Deleted';
$lang['usagerecords']['total_for']              = 'Total For';
$lang['usagerecords']['Disabled']               = 'Disabled';
$lang['usagerecords']['from']                   = 'from';
$lang['usagerecords']['to']                     = 'to';
$lang['usagerecords']['period']                 = 'Period';
$lang['usagerecords']['record_total']           = 'Total';
$lang['usagerecords']['last_update']            = 'Last Update';
$lang['usagerecords']['back']                   = 'Back';

$lang['hyp_error'] ="Hypervisor were not found. Please contact to administrator.";

/**************************
 * 
 *       API Info
 * 
 **************************/
$lang['apiInfo']['main_header']    = 'API info';
$lang['apiInfo']['apiLogin']       = 'API Login';
$lang['apiInfo']['apiKey']         = 'API Key';
$lang['apiInfo']['regenerate']     = 'Regenerate Key';
$lang['apiInfo']['generate']     = 'Generate Key';
$lang['apiInfo']['regenerateMsg']  = 'API Key has been generated successfully ';
$lang['apiInfo']['back']='Back To Service';



/**************************
 * 
 *   CONSOLE
 * 
 **************************/
$lang['console']['confirm']                 = 'Are you sure you want to send Ctrl+Alt+Del?';
$lang['console']['loading']                 = 'Loading';
$lang['console']['not_supported']           = 'Your browser is not supported';
$lang['console']['please_wait']             = 'Please wait while you are redirected to the console.';

