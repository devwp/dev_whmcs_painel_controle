<?php


/**
 * @author Maciej Husak <maciej@modulesgarden.com>
 */
$vm                 = new NewOnApp_VM($_GET['vserver']);
$vm                 ->setconnection($params,true);
if($vm->available($user_id)){         
$transactions = $vm->getTransactions();
    if($vm->isSuccess()){
        $vars['logs']      = $transactions;
    }else {
        $vars['msg_error'] = $vm->error();

    }
} else $vars['msg_error'] = $lang['mainsite']['perm_error'];

