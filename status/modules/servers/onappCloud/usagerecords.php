<?php


/**
 * @author Maciej Husak <maciej@modulesgarden.com>
 */

//Get Hosting ID
$hosting_id = (int)$params['serviceid'];
if(!defined('OnAppBillingDIR'))
    define('OnAppBillingDIR',ROOTDIR.DS.'modules'.DS.'addons'.DS.'OnAppBilling');

include(ROOTDIR.DS.'modules'.DS.'addons'.DS.'OnAppBilling'.DS.'core.php');

include(ROOTDIR.DS.'modules'.DS.'addons'.DS.'OnAppBilling'.DS.'classes'.DS.'class.OnAppBillingAccount.php');
include(ROOTDIR.DS.'modules'.DS.'addons'.DS.'OnAppBilling'.DS.'classes'.DS.'class.OnAppBillingProduct.php');
include(ROOTDIR.DS.'modules'.DS.'addons'.DS.'OnAppBilling'.DS.'classes'.DS.'class.OnAppBillingResource.php');
include(ROOTDIR.DS.'modules'.DS.'addons'.DS.'OnAppBilling'.DS.'class.OAProduct.php');
include(ROOTDIR.DS.'modules'.DS.'addons'.DS.'OnAppBilling'.DS.'class.OAResource.php');
include(ROOTDIR.DS.'modules'.DS.'addons'.DS.'OnAppBilling'.DS.'core'.DS.'class.MG_Pagination.php');



//Load Product Class
//IS ID setup?
if(!$hosting_id)
{
    $vars['msg_error']      = MG_Language::translate('Hosting ID Not Set');
}

//Is exists in hosting id?
$row = mysql_get_row("SELECT hosting_id FROM OnAppBilling_billed_hostings WHERE hosting_id = ?", array($hosting_id));
if(!$row)
{
    $vars['msg_error']      =  MG_Language::translate('Wrong Hosting ID');
    
}

//Have connected product?
$row = mysql_get_row("SELECT packageid FROM OnAppBilling_billed_hostings u
    LEFT JOIN tblhosting h ON u.hosting_id = h.id
    WHERE hosting_id = ?", array($hosting_id));
if(!$row)
{
    $vars['msg_error']      = MG_Language::translate('Cannot Found Product');
   
}
//product ID
$product_id = $row['packageid'];

//Create New Product
$p = new OAProduct($product_id);
if(!$p->isSupported())
{
    $vars['msg_error']      = MG_Language::translate('Product is not supported');
   
}

//Get Type
$type = $p->getServerType();

//Get settings
$settings = $p->getBillingSettings();




$row = mysql_get_row("SELECT count(record_id) as `count` FROM OnAppBilling_".$type."_prices WHERE hosting_id = ? AND product_id = ?", array($hosting_id, $product_id));
$records_count = $row['count'];

if(!$records_count)
{
    mysql_safequery("DELETE FROM OnAppBilling_billed_hostings WHERE hosting_id = ?", array($hosting_id));
    $vars['msg_error']      =  MG_Language::translate('Account have not any usage records');
   
}

//Enable pagination. We don't want all records!
$pagination = new MG_Pagination("mg_items");
$pagination->resetFilter();
if(isset($_GET['p']) && $_GET['p']>-1)
{
   $pagination->setPage($_GET['p']);
}
$pagination->setAmount($records_count);
$pagination->setLimit(20);

//Get Available Resources
$resources = $p->getResources();

$usage_table = OnAppBilling_columnPrefix(array_keys($resources) ,'r');
$price_table = OnAppBilling_columnPrefix(array_keys($resources) ,'p');
$items = mysql_get_array("SELECT p.total, r.id, r.date, ".$usage_table.", ".$price_table." FROM OnAppBilling_".$type."_prices as p
        LEFT JOIN OnAppBilling_".$type."_records r ON p.record_id = r.id 
        WHERE p.hosting_id = ? AND p.product_id = ? ORDER BY `date` DESC ".$pagination->getLimitAndOffset(), array($hosting_id, $product_id));

//Change Unit
foreach($resources as $res_key => &$res)
{
    if(isset($res['AvailableUnits']) && array_key_exists($res['unit'], $res['AvailableUnits']))
    {
        $factor = $res['AvailableUnits'][$res['unit']];
        foreach($items as &$item)
        { 
            if($item['r.'.$res_key] > 0)
            {
                $item['r.'.$res_key] *= $factor;
            }
        }
    }
}

//Get Summary Usage
$account = new OnAppBillingAccount($hosting_id);
$summary_usage = $account->getSummary($product_id);


//Get Curreny
$currency = mysql_get_row("SELECT prefix, suffix FROM tblcurrencies ORDER BY id ASC LIMIT 1" );
//Get Hosting and Client Details
$details = mysql_get_row("SELECT tblhosting.id, domain, firstname, lastname
        FROM tblhosting 
        LEFT JOIN tblclients ON tblhosting.userid = tblclients.id
        WHERE tblhosting.id = ?", array($_REQUEST['id']));

//Set header
$PAGE_SUBMODULE_HEADING = '#'.$details['id'].' ('.($details['domain'] ? $details['domain'] : MG_Language::translate('no hostname')).') '/*.$details['firstname'].' '.$details['lastname']*/;


foreach($summary_usage as $u => $val)
{
    $summary_usage[$u]['usage'] = number_format($val['usage'], 3);
    $summary_usage[$u]['total'] = number_format($val['total'], 2);
}




foreach($items as $key => $val)
{
   foreach($val as $k => $v)
   {
        if($k != 'date') {    
            $items[$key][$k] = number_format($v,3);            
        }
        
        if($k == 'total'){ 
            $items[$key][$k] = number_format($v,2);            
        }
   }
}

$vars['summary_usage']      = $summary_usage;
$vars['currency']           = $currency;
$vars['pagination']         = $pagination->getPagination();
$vars['pagination']         = preg_replace('/\#page=([0-9]+)&parent=mg_items/', 'clientarea.php?action=productdetails&id='.$params['serviceid'].'&modop=custom&a=management&page=usage_records&p=$1', $vars['pagination']);
$vars['resources']          = $resources;
$vars['items']              = $items;
$vars['resource_counter']   = count($resources)+2;