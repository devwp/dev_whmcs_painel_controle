<?php

namespace Lara\Widgets\GoogleAnalytics;

use Lara\Utils\Common as Common;
use Illuminate\Database\Capsule\Manager as Capsule;

/**
 * @package    Google Analytics by Lara - Pro
 * @author     Amr M. Ibrahim <mailamr@gmail.com>
 * @link       https://www.whmcsadmintheme.com
 * @copyright  Copyright (c) WHMCSAdminTheme 2016
 */

require_once("lrga_earnings_sales.php");

if (!defined("WHMCS"))
    die("This file cannot be accessed directly");

class lrga_whmcs_plugin extends lrga_earnings_sales{
	private  $whmcsStartDate;
	private  $whmcsEndDate;
	
	public function __construct($startDate, $endDate){
		Capsule::connection()->setFetchMode(\PDO::FETCH_ASSOC);
		$this->whmcsStartDate = new \DateTime($startDate);
		$this->whmcsEndDate   = new \DateTime($endDate);
		$this->whmcsEndDate   = $this->whmcsEndDate->modify( '+1 day' ); 
		$this->salesLabel     = "New Orders";
		$this->earningsLabel  = "Income";
		parent::__construct($startDate, $endDate);
	}

	protected function getStoreCurrency(){
		try{
			$results = Capsule::table("tblcurrencies")->where('default', 1)
			                                          ->first();

			if ((!empty($results['prefix'])) && (!empty($results['suffix']))){
				$this->storeCurrencyPrefix  = $results['prefix'];
			}elseif (!empty($results['suffix'])){
				$this->storeCurrencySuffix  = $results['suffix'];
			}else{
				$this->storeCurrencyPrefix  = $results['prefix'];
			}
			
		} catch (\Exception $e) {
			Common\ErrorHandler::FatalError("Cannot get default currency. Returned error: ". $e->getMessage());
		}		
	}
	
	protected function getRawData(){
		$sales = $this->generateEmptyPeriodArray();
		$earnings = $sales;
		try{
			$rawOrders = Capsule::table("tblorders")->whereBetween('date', [$this->whmcsStartDate, $this->whmcsEndDate])
												    ->get();

			$rawIncome = Capsule::table("tblaccounts")->whereBetween('date', [$this->whmcsStartDate, $this->whmcsEndDate])
												      ->get();

            foreach ($rawOrders as $order){
					$cDate = new \DateTime($order['date']);
					$cDate = $cDate->format('Y-m-d');
					$sales[$cDate]++;
			}
			
			foreach ($rawIncome as $income){
					$cDate = new \DateTime($income['date']);
					$cDate = $cDate->format('Y-m-d');
					$earnings[$cDate] = $earnings[$cDate] + ($income['amountin']/$income['rate']);
			}
			
		} catch (\Exception $e) {
			Common\ErrorHandler::FatalError("Cannot get invoices data. Returned error: ". $e->getMessage());
		}
		$this->rawData['sales'] = $sales;
		$this->rawData['earnings'] = $earnings;
	}
}
?>