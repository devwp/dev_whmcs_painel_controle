{*		
		Impressive Themes
		http://www.impressivethemes.net
		-------------------------------
		
		"Control_Alternative_Slider" Cart Template Options for Control Theme by Impressive Themes
		-----------------------------------------------------------------------------------------
		
		Please refer to the theme documentation available at the 
		following url before making any changes to this file:
		
		http://www.impressivethemes.net/support/documentation/control-whmcs-theme-documentation
		
		Should you require any further help or assistance, please 
		contact customer support via our online support desk at:
		
		http://www.impressivethemes.net/support/contact	
*}
{* 1. Sidebar Display *}
	{* 1.1.	Enable or Disable Secondary Sidebar Display on Cart Pages - enabled / disabled *}
		{assign var=itCartSidebarDisplay value="enabled" scope="global"}
	{* 1.2. Enable or Disable Mobile Friendly Secondary Sidebar Display on Cart Pages on Small Screen Sizes - enabled / disabled *}
    	{assign var=itCartSidebarMobileDisplay value="enabled" scope="global"}
    {* 1.3. Sidebar Display Mode - permanent / showhide *}
    	{assign var=itCartSidebarDisplayMode value="permanent" scope="global"}    
    {* 1.4. Sidebar Show / Hide Method - slide / squash *}
    	{assign var=itCartSidebarDisplayMethod value="slide" scope="global"}