{include file="orderforms/control_standard/common.tpl"}
<!--main content start-->
<section id="main-content" class="cart">
	<!-- Display mobile sidebar alternative if applicable -->
	{if $itCartSidebarMobileDisplay eq "enabled"}
		<div class="row cat-col-row visible-xs visible-sm">
			<div class="col-md-12">
				{include file="orderforms/control_standard/sidebar-categories-collapsed.tpl"}
			</div>
		</div>
	{/if}
	<!-- Display page title -->
	<div class="row">
		<div class="col-md-12">
			<!--breadcrumbs start -->
			{if $itCartSidebarDisplay eq "enabled"}
				<div style="position: absolute; right: 15px; margin-top: 22px;">
				    <button type="button" class="btn btn-default btn-sm hidden-xs hidden-sm" id="btnShowSidebar">
				        <i class="fa fa-arrow-circle-left"></i>
				        {$LANG.showMenu}
				    </button>
				</div>
			{/if}
			{include file="$template/includes/breadcrumb.tpl"}
			<!--breadcrumbs end -->
			<h1 class="h1">{$LANG.thereisaproblem}</h1>
			{if $domain == 'register'}
				<small class="res-left pull-right" style="margin: 12px 12px 20px 12px;">{$LANG.orderForm.findNewDomain}</small>
			{else}
				<small class="res-left pull-right" style="margin: 12px 12px 20px 12px;">{$LANG.orderForm.transferExistingDomain}</small>
			{/if}
		</div>
	</div>
	<!-- Display sidebar layout if applicable -->
	{if $itCartSidebarDisplay eq "enabled"}
		<div class="row cart-main-column">
			<div id="internal-content" class="col-md-12 pull-md-left">
				{/if}
				<div class="row">
					<div class="col-md-12">
						<div class="panel panel-danger">
							<div class="panel-heading">
								<h3 class="panel-title">	
									{$errortitle}
								</h3>
							</div>
							<div class="panel-body">
								<p>{$errormsg}</p>
							</div>
							<div class="panel-footer">
		                        <a href="javascript:history.go(-1)" class="btn btn-default">
		                            <i class="fa fa-arrow-left"></i>&nbsp;
		                            {$LANG.problemgoback}
		                        </a>
							</div>
						</div>
					</div>
				</div>
				{if $itCartSidebarDisplay eq "enabled"}
			</div>
			<div class="col-md-3 pull-md-right whmcs-sidebar hidden-xs hidden-sm sidebar-secondary cart-sidebar">
				{include file="orderforms/control_standard/sidebar-categories.tpl"}
			</div>
			<div class="clearfix"></div>
		</div>
	{/if}
	<div class="clearfix"></div>
</section>