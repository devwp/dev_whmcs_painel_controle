{include file="orderforms/OnAppOnePageSix/header.tpl"} 

<h2>{$LANG.thereisaproblem}</h2>

<div class="alert alert-error">
    <i class="icon icon-remove"></i>
   {$errormsg}
</div>

<p align="center"><br /><a href="javascript:history.go(-1)">&laquo; {$LANG.problemgoback}</a></p>

{include file="orderforms/OnAppOnePageSix/footer.tpl"} 
