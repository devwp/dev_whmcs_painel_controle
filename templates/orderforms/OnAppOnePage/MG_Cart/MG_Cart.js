

function MG_Cart()
{
    this.async          = false;
    
    this.disableAjax    = false;
    
    this.caller         =   '';
    
    this.abortPrev      = false;
    
    
    this._call = function(data, callback)
    {
        ret = null;
        if(!this.async)
        {
            jQuery.ajaxSetup({async: false});
        }
        
        //Set up custom template
        if(MG_Cart.custom_template != '')
        {
            data    +=  '&custom_template='+MG_Cart.custom_template;
        }
        
        jQuery.post("cart.php", "ajax=1&"+data, function(returned_data){
            if(typeof callback == 'function')
            {
                callback(returned_data);
            }
            ret = returned_data;
        });
        
        return ret;
    };
    
    /**
     * Return details about client
     * Loaded template: viewcart.php
     * @returns {undefined}
     */
    this.view = function(callback)
    {
        this.caller =   'view';
        
        ret = this._call("a=view", callback);
        return ret;
    };
    
    /**
     * Return items added to cart.
     * Loaded template: cartsummary.tpl
     * @returns {undefined}
     */
    this.viewSummary = function(callback, clientDetails)
    {
        this.caller =   'viewSummary';
        var url = "a=view&cartsummary=1";

        if(clientDetails !== undefined && clientDetails.country !=="") 
            url +='&country='+clientDetails.country;
        if(clientDetails !== undefined &&  clientDetails.state !=="")
            url +='&state='+clientDetails.state;
        

        ret = this._call(url, callback);
        return ret;
    };
    
    /**
     * Delete all items from cart
     * @param {type} callback
     * @returns {unresolved}
     */
    this.empty = function(callback)
    {
        this.caller =   'empty';
        
        ret = this._call("a=empty", callback);
        return ret;
    };
     
    /**
     * Add product with out selecting domain
     * @param {type} pid
     * @param {type} callback
     * @returns {unresolved}
     */
    this.addProduct = function(pid, sld, tld, domainoption, domainselect, callback)
    {
        this.caller =   'addProduct';
        
        MG_Cart.product_id = pid;
        if(sld && tld && domainoption)
        {
            if(!domainselect)
            {
                domainselect = 0;
            }
            else
            {
                domainselect = 1;
            }
            ret = this._call("a=add&pid="+pid+"&sld="+sld+"&tld="+tld+"&domainoption="+domainoption+"&domainselect="+domainselect, callback);
        }
        else
        {
            ret = this._call("a=add&pid="+pid, callback);    
            if(ret == "")
            {
                $("#add-domain").hide();
            } else 
            {
                $("#add-domain").show();
            }
        }
        
        return ret;
    };
    
    /**
     * Add new product with your own domain
     * @type Arguments
     */
    this.addProductWithOwnDomain = function(pid, sld, tld, callback)
    {
        this.caller =   'addProductWithOwnDomain';
        
        if(!pid)
        {
            pid = MG_Cart.product_id;
        }
        ret = this._call("a=add&pid="+pid+"&sld="+sld+"&tld="+tld+"&domainoption=owndomain&domainselect=1", callback);
        return ret;
    };
    
    /**
     *  Check domain availability
     * @param {type} checktype
     * @param {type} sld
     * @param {type} tld
     * @param {type} callback
     * @returns {unresolved}
     */
    this.checkDomainOptions = function(checktype, sld, tld, callback)
    {
        this.caller =   'checkDomainOptions';
        
        ret = this._call("a=domainoptions&checktype="+checktype+"&sld="+sld+"&tld="+tld, callback);
        return ret;
    };
    
    
    this.checkout = function(callback)
    {
        this.caller =   'checkout';
        
        ret = this._call("a=checkout", callback);
        return ret;
    };
    
    this.fullCheckout = function(serialized_data, callback)
    {
        this.caller =   'fullCheckout';
        
        ret = this._call("a=checkout&checkout=1&"+serialized_data, callback);
        return ret;
    };
    
    this.complete = function(serialized_data, callback)
    {
        this.caller =   'complete';
        
        ret = this._call("a=view&submit=1&$submit=1&"+serialized_data, callback);
        return ret;
    };
    
    this.getProducts = function(gid)
    {
        this.caller =   'getProducts';
        
        ret = this._call("gid="+gid);
        return ret;
    };
    
    
    this.deleteProduct = function(index, callback)
    {
        this.caller =   'deleteProduct';
        
        ret = this._call("a=remove&i="+index+"&r=p", callback);
        return ret;
    };
    
    this.deleteDomain = function(index, callback)
    {
        this.caller =   'deleteDomain';
        
        ret = this._call("a=remove&i="+index+"&r=d", callback);
        return ret;
    };
    
    this.configureProduct = function(index)
    {
        this.caller =   'configureProduct';
        
        ret = this._call("a=confproduct&i="+index);
        return ret;
    };
    
    this.configureDomains = function(callback)
    {
        this.caller =   'configureDomains';
        
        ret = this._call("a=confdomains", callback);
    };
    
    this.updateDomainsConfiguration = function(serialized_domain_data, callback)
    {
        this.caller =   'updateDomainsConfiguration';
        
        ret = this._call("a=confdomains&update=1&"+serialized_domain_data, callback);
    };
    
    this.clear = function(callback)
    {
        this.caller =   'clear';
        
        ret = this._call("a=empty", callback);
        return ret;
    };
    
    this.setPromoCode = function(code, callback)
    {
        this.caller =   'setPromoCode';
        
        ret = this._call("a=applypromo&promocode="+code, callback);
    };
    
    this.removePromoCode = function(callback)
    {
        this.caller =   'removePromoCode';
        
        ret = this._call("a=removepromo", callback);
    };
}

MG_Cart.product_id = 0;

MG_Cart.custom_template = '';