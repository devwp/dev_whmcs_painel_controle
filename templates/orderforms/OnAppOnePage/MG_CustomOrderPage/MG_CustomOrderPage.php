<?php


/**
 * @author Mariusz Miodowski <mariusz@modulesgarden.com>
 */

if(function_exists('mysql_safequery') == false) {
    function mysql_safequery($query,$params=false) {
        if ($params) {
            foreach ($params as &$v) { $v = mysql_real_escape_string($v); }
            $sql_query = vsprintf( str_replace("?","'%s'",$query), $params );
            $sql_query = mysql_query($sql_query);
        } else {
            $sql_query = mysql_query($query);
        }

        return ($sql_query);
    }
}

if(!function_exists('mysql_get_array'))
{
    function mysql_get_array($query, $params = false)
    {
        $q = mysql_safequery($query, $params);
        $arr = array();
        while($row = mysql_fetch_assoc($q))
        {
            $arr[] = $row;
        }
        
        return $arr;
    }
}

if(!function_exists('mysql_get_row'))
{
    function mysql_get_row($query, $params = false)
    {
        $q = mysql_safequery($query, $params);
        if($q)
        {
            $row = mysql_fetch_assoc($q);
            return $row;
        }
        
        return null;
    }
}

if(isset($_REQUEST['gid']))
{
    //
    $products   =   $this->get_template_vars('products');
    $vars       =   $this->get_template_vars();
    
    if($products[0]['pid'])
    {
        $pid = $products[0]['pid'];
        $row = mysql_get_row("SELECT * FROM tblproducts", array($pid));
        $servertype = $row['servertype'];
    }
}