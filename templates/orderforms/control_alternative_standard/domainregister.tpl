{include file="orderforms/$carttpl/common.tpl"}
<!--main content start-->
<section id="main-content" class="cart">
	<!-- Display mobile sidebar alternative if applicable -->
	{if $itCartSidebarMobileDisplay eq "enabled"}
		<div class="row cat-col-row visible-xs visible-sm">
			<div class="col-md-12">
				{include file="orderforms/$carttpl/sidebar-categories-collapsed.tpl"}
			</div>
		</div>
	{/if}
	<!-- Display page title -->
	<div class="row">
		<div class="col-md-12">
			<!--breadcrumbs start -->
			{if $itCartSidebarDisplay eq "enabled"}
				{if $itCartSidebarDisplayMode eq "showhide"}
					{if $itTextDirectionRTL eq "enabled"}
						<div {if $itBreadcrumbDisplay eq "enabled"}style="position: absolute; left: 15px; margin-top: 5px;"{else}style="position: absolute; left: 15px; margin: -6px 0 20px 0;"{/if}>
						    <button type="button" class="btn btn-default btn-sm hidden-xs hidden-sm" {if $itCartSidebarDisplayMethod eq "squash"}id="btnSquashSidebar"{else}id="btnSlideSidebarRTL"{/if}>
						        <i class="fa fa-arrow-circle-left"></i>
						        {$LANG.showMenu}
						    </button>
						</div>
					{else}
						<div {if $itBreadcrumbDisplay eq "enabled"}style="position: absolute; right: 15px; margin-top: 5px;"{else}style="position: absolute; right: 15px; margin: -6px 0 20px 0;"{/if}>
						    <button type="button" class="btn btn-default btn-sm hidden-xs hidden-sm" {if $itCartSidebarDisplayMethod eq "squash"}id="btnSquashSidebar"{else}id="btnSlideSidebar"{/if}>
						        <i class="fa fa-arrow-circle-left"></i>
						        {$LANG.showMenu}
						    </button>
						</div>
					{/if}
				{/if}
			{/if}			
			{include file="$template/includes/breadcrumb.tpl"}
			<!--breadcrumbs end -->
			<h1 class="h1">{$LANG.registerdomain}</h1>
			<small class="res-left pull-right" style="margin: 12px 12px 20px 12px;">{$LANG.orderForm.findNewDomain}</small>
		</div>
	</div>
	<!-- Display sidebar layout if applicable -->
	{if $itCartSidebarDisplay eq "enabled"}
		<div class="row cart-main-column">
			<div id="internal-content" class="{if $itCartSidebarDisplayMode eq "showhide"}col-md-12{else}col-md-9{/if} pull-md-left">
				{/if}
	            <div class="domain-checker-container">
					<div class="row">
						<div class="col-md-12">
			                <div class="domain-checker-bg clearfix">
			                    <form method="post" action="cart.php" id="frmDomainChecker">
			                        <input type="hidden" name="a" value="checkDomain">
			                        <div class="row">
			                            <div class="col-md-12">
			            	                <div class="input-group input-group-lg input-group-box">
			                                    <input type="text" name="domain" class="form-control" placeholder="{$LANG.findyourdomain}" value="{$lookupTerm}" id="inputDomain" data-toggle="tooltip" data-placement="left" data-trigger="manual" title="{lang key='orderForm.domainOrKeyword'}" />
			                                    <span class="input-group-btn">
			                                        <button type="submit" id="btnCheckAvailability" class="btn btn-primary domain-check-availability">{$LANG.search}</button>
			                                    </span>
			                                </div>
			                            </div>
			                        </div>
			                        {if $captcha}
			                            <div class="captcha-container text-center" id="captchaContainer">
			                                {if $captcha == "recaptcha"}
			                                    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
			                                    <div id="google-recaptcha" class="g-recaptcha center-block" style="width: 304px;" data-sitekey="{$reCaptchaPublicKey}" data-toggle="tooltip" data-placement="left" data-trigger="manual" title="{lang key='orderForm.required'}" ></div>
			                                    <br />
			                                    <br />
			                                {else}
			                                    <div class="default-captcha default-captcha-register-margin well text-center">
			                                        <p>{lang key="cartSimpleCaptcha"}</p>
			                                        <div>
				                                        <br />
			                                            <img id="inputCaptchaImage" src="includes/verifyimage.php" align="middle" />
			                                            <br /><br />
			                                            <input id="inputCaptcha" type="text" name="code" maxlength="5" class="input-sm" data-toggle="tooltip" data-placement="right" data-trigger="manual" title="{lang key='orderForm.required'}" />
			                                        </div>
			                                    </div>
			                                {/if}
			                            </div>
			                        {/if}
			                    </form>
			                </div>
			            </div>
					</div>
				</div>
				<div id="DomainSearchResults" class="hidden">
					<div class="row">
						<div class="col-md-12">
			                <div id="searchDomainInfo" class="domain-checker-result-headline">
			                    <p id="primaryLookupSearching" class="domain-lookup-loader domain-lookup-primary-loader domain-searching">
				                    <i class="fa fa-spinner fa-spin"></i>
				                    <br />
				                    {lang key='orderForm.searching'}...
				                </p>
			                    <div id="primaryLookupResult" class="domain-lookup-result hidden">
			                        <div class="domain-invalid domain-checker-invalid">
				                        <div class="alert alert-warning">
											<i class="fa fa-2x fa-exclamation-triangle"></i> <span class="results-text">{lang key='orderForm.domainLetterOrNumber'}<span class="domain-length-restrictions">{lang key='orderForm.domainLengthRequirements'}</span></span>
											<div class="clearfix"></div>
										</div>
			                        </div>
			                        <div class="domain-unavailable domain-checker-unavailable">
				                        <div class="alert alert-danger">
											<i class="fa fa-2x fa-times"></i> <span class="results-text">{lang key='orderForm.domainIsUnavailable'}</span>
											<div class="clearfix"></div>
										</div>
			                        </div>
			                        <div class="domain-available domain-checker-available">
			                        	<div class="alert alert-success domain-result domain-price">
								            <i class="fa fa-2x fa-check"></i> <span class="results-text"><strong></strong> {$LANG.domainavailable2}</span>
											<button class="btn btn-success btn-sm pull-right res-center btn-add-to-cart" data-whois="0" data-domain="">
												<span class="to-add"><i class="fa fa-cart-plus"></i> {$LANG.addtocart} (<span class="to-add price"></span>)</span>
												<span class="added"><i class="fa fa-shopping-cart"></i> {lang key='checkout'}</span>
												<span class="unavailable"><i class="fa fa-exclamation-triangle"></i> {$LANG.domaincheckertaken}</span>
											</button>
							            </div>
						            </div>
						            <a class="domain-contact-support btn btn-primary">{$LANG.domainContactUs}</a>
			                    </div>
			                </div>
			            </div>
					</div>
	                {if $spotlightTlds}
		                <div class="row">
							<div class="col-md-12">
			                    <div id="spotlightTlds" class="spotlight-tlds clearfix">
			                        <div class="spotlight-tlds-container">
			                            {foreach $spotlightTlds as $key => $data}
			                                <div class="spotlight-tld-container spotlight-tld-container-{$spotlightTlds|count}">
			                                    <div id="spotlight{$data.tldNoDots}" class="spotlight-tld">
			                                        {if $data.group}
			                                            <div class="spotlight-tld-{$data.group}">{$data.groupDisplayName}</div>
			                                        {/if}
			                                        {$data.tld}
			                                        <span class="domain-lookup-loader domain-lookup-spotlight-loader">
			                                            <i class="fa fa-spinner fa-spin"></i>
			                                        </span>
			                                        <div class="domain-lookup-result">
			                                            <button type="button" class="btn btn-sm unavailable hidden" disabled="disabled">
			                                                {lang key='domainunavailable'}
			                                            </button>
			                                            <button type="button" class="btn btn-sm invalid hidden" disabled="disabled">
			                                                {lang key='domainunavailable'}
			                                            </button>
			                                            <span class="available price hidden">{$data.register}</span>
			                                            <button type="button" class="btn hidden btn-sm btn-success btn-add-to-cart" data-whois="0" data-domain="">
			                                                <span class="to-add"><i class="fa fa-cart-plus"></i> {lang key='orderForm.add'}</span>
			                                                <span class="added"><i class="fa fa-shopping-cart"></i> {lang key='checkout'}</span>
			                                                <span class="unavailable"><i class="fa fa-exclamation-triangle"></i> {$LANG.domaincheckertaken}</span>
			                                            </button>
			                                            <button type="button" class="btn btn-sm btn-primary domain-contact-support hidden">{$LANG.domainContactUs}</button>
			                                        </div>
			                                    </div>
			                                </div>
			                            {/foreach}
			                        </div>
			                    </div>
							</div>
		                </div>
	                {/if}
	                <div class="row">
						<div class="col-md-12">	
			                <div class="suggested-domains{if !$showSuggestionsContainer} hidden{/if}">
				                <div class="panel panel-default">
					                <div class="panel-heading">
										<h3 class="panel-title">{lang key='orderForm.suggestedDomains'}</h3>
									</div>
				                    <div id="suggestionsLoader" class="panel-body domain-lookup-loader domain-lookup-suggestions-loader">
				                        <i class="fa fa-spinner fa-spin"></i> {lang key='orderForm.generatingSuggestions'}
				                    </div>
				                    <table class="table table-curved table-hover domain-lookup-result hidden" id="domainSuggestions">
										<tbody>
											<tr class="domain-suggestion hidden">
												<td class="dname">
													<strong><span class="domain"></span><span class="extension"></span></strong>
													<span class="promo hidden">
						                                <span class="sales-group-hot hidden">{lang key='domainCheckerSalesGroup.hot'}</span>
						                                <span class="sales-group-new hidden">{lang key='domainCheckerSalesGroup.new'}</span>
						                                <span class="sales-group-sale hidden">{lang key='domainCheckerSalesGroup.sale'}</span>
						                            </span>
												</td>
												<td class="text-right">
						                            <button type="button" class="btn btn-sm btn-default btn-add-to-cart" data-whois="1" data-domain="">
						                                <span class="to-add"><i class="fa fa-cart-plus"></i> {$LANG.addtocart} (<span class="to-add price"></span>)</span>
														<span class="added"><i class="fa fa-shopping-cart"></i> {lang key='checkout'}</span>
														<span class="unavailable"><i class="fa fa-exclamation-triangle"></i> {$LANG.domaincheckertaken}</span>
						                            </button>
						                            <button type="button" class="btn btn-sm btn-primary domain-contact-support hidden">{$LANG.domainContactUs}</button>
												</td>
											</tr>
					                        <tr class="panel-footer more-suggestions hidden text-center">
						                        <td colspan="2" class="text-muted text-center">
							                        <a id="moreSuggestions" href="#" onclick="loadMoreSuggestions();return false;">{lang key='domainsmoresuggestions'}</a>
							                        <span id="noMoreSuggestions" class="no-more small hidden">{lang key='domaincheckernomoresuggestions'}</span>
						                        </td>
						                    </tr>
										</tbody>
				                    </table>			                    
		                  			<div class="panel-footer">
										<i class="fa fa-exclamation-circle"></i> &nbsp; <small>{lang key='domainssuggestionswarnings'}</small>
									</div>
				                </div>
			                </div>
			            </div>
	                </div>
	            </div>
				<div class="row">
	                <div class="{if $domainTransferEnabled}col-md-6{else}col-md-8 col-md-offset-2{/if}">
		                <div class="panel panel-default">
			                <div class="panel-heading">
				                <h3 class="panel-title"><i class="fa fa-server"></i> &nbsp;{lang key='orderForm.addHosting'}</h3>
			                </div>
			                <div class="panel-body" data-key="sameHeights">	
				                <p><strong>{lang key='orderForm.chooseFromRange'}</strong></p>
		                        <p>{lang key='orderForm.packagesForBudget'}</p>
			                </div>
			                <div class="panel-footer">
				                <a href="cart.php" class="btn btn-warning">
		                            {lang key='orderForm.exploreNow'} &nbsp;<i class="fa fa-chevron-right"></i>
		                        </a>
			                </div>
	                    </div>
	                </div>
	                {if $domainTransferEnabled}
	                    <div class="col-md-6">
	                        <div class="panel panel-default">
				                <div class="panel-heading">
					                <h3 class="panel-title"><i class="fa fa-globe"></i> &nbsp;{lang key='orderForm.transferToUs'}</h3>
				                </div>
				                <div class="panel-body" data-key="sameHeights">	
	                                <p><strong>{lang key='orderForm.transferExtend'}</strong> *</p>
				                </div>
								<div class="panel-footer">
					                <a href="cart.php?a=add&domain=transfer" class="btn btn-primary">
		                                {lang key='orderForm.transferDomain'} &nbsp;<i class="fa fa-chevron-right"></i>
			                        </a>
				                </div>
	                        </div>
	                    </div>
	                {/if}
	            </div>
	            {if $domainTransferEnabled}
	                <div class="row">
	                    <div class="col-md-12">
		                    <br />
		                    <p class="small text-center">* {lang key='orderForm.extendExclusions'}</p>
	                    </div>
                    </div>
				{/if}
				{if $itCartSidebarDisplay eq "enabled"}
			</div>
			<div class="col-md-3 pull-md-right whmcs-sidebar hidden-xs hidden-sm sidebar-secondary {if $itCartSidebarDisplayMode eq "showhide"}{if $itCartSidebarDisplayMethod eq "squash"}cart-sidebar-squash{else}cart-sidebar{/if}{/if}">
				{include file="orderforms/$carttpl/sidebar-categories.tpl"}
			</div>
			<div class="clearfix"></div>
		</div>
	{/if}
	<div class="clearfix"></div>
</section>
{if $lookupTerm && !$captchaError}
    <script>
        jQuery(document).ready(function() {
            jQuery('#btnCheckAvailability').click();
        });
    </script>
{/if}
<script type="text/javascript">
	if ('addEventListener' in window) {
	    window.addEventListener('resize', function(){
	        sameHeights();
	    });
	    window.addEventListener('load', function(){
	        sameHeights();
	    });
	}
</script>