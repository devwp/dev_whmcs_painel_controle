{if $checkout}
	{include file="orderforms/$carttpl/checkout.tpl"}
{else}
	<script>
		// Define state tab index value
		var statesTab = 10;
		var stateNotRequired = true;
	</script>
	<script type="text/javascript" src="{$BASE_PATH_JS}/StatesDropdown.js"></script>
	{include file="orderforms/$carttpl/common.tpl"}
	<!--main content start-->
	<section id="main-content" class="invoice cart">
		<!-- Display mobile sidebar alternative if applicable -->
		{if $itCartSidebarMobileDisplay eq "enabled"}
			<div class="row cat-col-row visible-xs visible-sm">
				<div class="col-md-12">
					{include file="orderforms/$carttpl/sidebar-categories-collapsed.tpl"}
				</div>
			</div>
		{/if}
		<!-- Display page title -->
		<div class="row">
			<div class="col-md-12">
				<!--breadcrumbs start -->
				{if $itCartSidebarDisplay eq "enabled"}
					{if $itCartSidebarDisplayMode eq "showhide"}
						{if $itTextDirectionRTL eq "enabled"}
							<div {if $itBreadcrumbDisplay eq "enabled"}style="position: absolute; left: 15px; margin-top: 5px;"{else}style="position: absolute; left: 15px; margin: -6px 0 20px 0;"{/if}>
							    <button type="button" class="btn btn-default btn-sm hidden-xs hidden-sm" {if $itCartSidebarDisplayMethod eq "squash"}id="btnSquashSidebar"{else}id="btnSlideSidebarRTL"{/if}>
							        <i class="fa fa-arrow-circle-left"></i>
							        {$LANG.showMenu}
							    </button>
							</div>
						{else}
							<div {if $itBreadcrumbDisplay eq "enabled"}style="position: absolute; right: 15px; margin-top: 5px;"{else}style="position: absolute; right: 15px; margin: -6px 0 20px 0;"{/if}>
							    <button type="button" class="btn btn-default btn-sm hidden-xs hidden-sm" {if $itCartSidebarDisplayMethod eq "squash"}id="btnSquashSidebar"{else}id="btnSlideSidebar"{/if}>
							        <i class="fa fa-arrow-circle-left"></i>
							        {$LANG.showMenu}
							    </button>
							</div>
						{/if}
					{/if}
				{/if}			
				{include file="$template/includes/breadcrumb.tpl"}
				<!--breadcrumbs end -->
				<h1 class="h1">{$LANG.cartreviewcheckout}</h1>
			</div>
		</div>
		<!-- Display sidebar layout if applicable -->
		{if $itCartSidebarDisplay eq "enabled"}
			<div class="row cart-main-column">
				<div id="internal-content" class="{if $itCartSidebarDisplayMode eq "showhide"}col-md-12{else}col-md-9{/if} pull-md-left">
					{/if}
					<div class="row">
						<div class="col-md-12">
							{if $cartitems == 0}
								<div class="alert alert-warning text-center" role="alert">
									<p>{$LANG.cartempty}</p>
								</div>
								<div class="text-center">
									<a href="cart.php" class="btn btn-primary btn-3d" id="continueShopping">
										{$LANG.orderForm.continueShopping} 
										<i class="fa fa-arrow-right"></i>
									</a>
									<br />
									<br />
								</div>
							{else}
								{if $promoerrormessage}
									<div class="alert alert-warning text-center" role="alert">
										{$promoerrormessage}
									</div>
								{elseif $errormessage}
									<div class="alert alert-danger" role="alert">
										<p>{$LANG.orderForm.correctErrors}:</p>
										<ul>
											{$errormessage}
										</ul>
									</div>
								{elseif $promotioncode && $rawdiscount eq "0.00"}
									<div class="alert alert-info text-center" role="alert">
										{$LANG.promoappliedbutnodiscount}
									</div>
								{elseif $promoaddedsuccess}
									<div class="alert alert-success text-center" role="alert">
										{$LANG.orderForm.promotionAccepted}
									</div>
								{/if}
								{if $bundlewarnings}
									<div class="alert alert-warning" role="alert">
										<strong>{$LANG.bundlereqsnotmet}</strong><br />
										<ul>
											{foreach from=$bundlewarnings item=warning}
												<li>{$warning}</li>
											{/foreach}
										</ul>
									</div>
								{/if}
								<form method="post" action="{$smarty.server.PHP_SELF}?a=view">
									<div class="panel panel-default">
										<div class="panel-heading">
											<h3 class="panel-title">	
												{$LANG.ordersummary}
											</h3>
										</div>
										<table class="cart">
											<thead>
												<tr>
													<th colspan="2">{$LANG.orderForm.productOptions}</th>
													{if $showqtyoptions}
														<th class="text-right">{$LANG.orderForm.qty}</th>
													{/if}
													<th class="text-right">{$LANG.orderForm.priceCycle}</th>
												</tr>
											</thead>
											<tbody>
												{foreach $products as $num => $product}
													<tr{if $product.addons} class="dashed"{/if}>
														<td>
															<span class="item-title">
																<strong>{$product.productinfo.name}</strong>
															</span>
															<br />
															<span class="item-group">
																{$product.productinfo.groupname}
															</span>
															{if $product.domain}
																<br />
																<span class="item-domain">
																	{$product.domain}
																</span>
															{/if}
															{if $product.configoptions}
																<small>
																	<br />
																	{foreach key=confnum item=configoption from=$product.configoptions}
																		&nbsp;&raquo; {$configoption.name}: {if $configoption.type eq 1 || $configoption.type eq 2}{$configoption.option}{elseif $configoption.type eq 3}{if $configoption.qty}{$configoption.option}{else}{$LANG.no}{/if}{elseif $configoption.type eq 4}{$configoption.qty} x {$configoption.option}{/if}<br />
																	{/foreach}
																</small>
															{/if}
														</td>
														<td>
															<a href="{$smarty.server.PHP_SELF}?a=confproduct&i={$num}" class="btn btn-link btn-xs">
																<i class="fa fa-pencil"></i>
																{$LANG.orderForm.edit}
															</a>
															<br />
															<button type="button" class="btn btn-link btn-xs" onclick="removeItem('p','{$num}')">
																<i class="fa fa-times"></i>
																{$LANG.orderForm.remove}
															</button>
														</td>
														{if $showqtyoptions}
															<td class="text-right">
																{if $product.allowqty}
																		<input type="number" name="qty[{$num}]" value="{$product.qty}" class="form-control text-center" />
																		<button type="submit" class="btn btn-xs">
																			{$LANG.orderForm.update}
																		</button>
																{/if}
															</td>
														{/if}
														<td class="text-right">
															<span>{$product.pricing.totalTodayExcludingTaxSetup}</span><br />
															<span class="cycle">{$product.billingcyclefriendly}</span><br />
															{if $product.pricing.productonlysetup}
																<br />
																<span class="setup-price">&#43; {$product.pricing.productonlysetup->toPrefixed()}</span><br />
																<span class="cycle">{$LANG.ordersetupfee}</span><br />
															{/if}
															{if $product.proratadate}<br />({$LANG.orderprorata} {$product.proratadate})<br />{/if}
														</td>
													</tr>
													{foreach key=addonnum item=addon from=$product.addons name=prodadd}
														<tr{if !$smarty.foreach.prodadd.last} class="dashed"{/if}>
															<td colspan="2">
																<span class="item-title">
																	<strong>{$addon.name}</strong>
																</span>
																<br />
																<span class="item-group">
																		{$LANG.orderaddon}
																</span>
															</td>
															{if $showqtyoptions}
																<td class="text-right">
																</td>
															{/if}
															<td class="text-right">
																<span>{$addon.recurring}</span><br />
																<span class="cycle">{$addon.billingcyclefriendly}</span><br />
																{if $addon.setup}
																	<br />
																	<span class="setup-price">&#43; {$addon.setup}</span><br />
																	<span class="cycle">{$LANG.ordersetupfee}</span><br />
																{/if}
															</td>
														</tr>
													{/foreach}
												{/foreach}
												{foreach $addons as $num => $addon}
													<tr>
														<td>
															<span class="item-title">
																<strong>{$addon.name}</strong>
															</span>
															<br />
															<span class="item-group">
																{$addon.productname}
															</span>
															{if $addon.domainname}
																<br />
																<span class="item-domain">
																	{$addon.domainname}
																</span>
															{/if}
														</td>
														<td>
															<button type="button" class="btn btn-link btn-xs" onclick="removeItem('a','{$num}')">
																<i class="fa fa-times"></i>
																{$LANG.orderForm.remove}
															</button>
														</td>
														{if $showqtyoptions}
															<td class="text-right">
															</td>
														{/if}
														<td class="text-right">
															{if $addon.setup}
																<span>{$addon.pricingtext|substr:0:($addon.pricingtext|strpos:"+"-1)}</span><br />
															{else}
																<span>{$addon.pricingtext}</span><br />
															{/if}
															<span class="cycle">{$addon.billingcyclefriendly}</span><br />
															{if $addon.setup}
																<br />
																<span class="setup-price">&#43; {$addon.setup}</span><br />
																<span class="cycle">{$LANG.ordersetupfee}</span><br />
															{/if}
														</td>
													</tr>
												{/foreach}
												{foreach $domains as $num => $domain}
													<tr>
														<td>
															<span class="item-title">
																<strong>{if $domain.type eq "register"}{$LANG.orderdomainregistration}{else}{$LANG.orderdomaintransfer}{/if}</strong>
															</span>
															{if $domain.domain}
																<br />
																<span class="item-domain">
																	{$domain.domain}
																</span>
															{/if}
															<span class="domain-details">
																{if $domain.dnsmanagement}<br /><i class="fa fa-check"></i> {$LANG.domaindnsmanagement}{/if}
																{if $domain.emailforwarding}<br /><i class="fa fa-check"></i> {$LANG.domainemailforwarding}{/if}
																{if $domain.idprotection}<br /><i class="fa fa-check"></i> {$LANG.domainidprotection}{/if}
															</span>
														</td>
														<td>
															<a href="{$smarty.server.PHP_SELF}?a=confdomains" class="btn btn-link btn-xs">
																<i class="fa fa-pencil"></i>
																{$LANG.orderForm.edit}
															</a>
															<br />
															<button type="button" class="btn btn-link btn-xs btn-remove-from-cart" onclick="removeItem('d','{$num}')">
																<i class="fa fa-times"></i>
																{$LANG.orderForm.remove}
															</button>
														</td>
														{if $showqtyoptions}
															<td class="text-right">
															</td>
														{/if}
														<td class="text-right">
															{if count($domain.pricing) == 1 || $domain.type == 'transfer'}
			                                                    <span name="{$domain.domain}Price">{$domain.price}</span><br />
			                                                    <span class="cycle">{$domain.regperiod} {$domain.yearsLanguage}</span><br />
			                                                    <span class="renewal cycle">
																	{lang key='domainrenewalprice'} <span class="renewal-price cycle">{$domain.renewprice->toPrefixed()}{$domain.shortYearsLanguage}</span>
																</span>
			                                                    <br />
			                                                {else}
			                                                    <span name="{$domain.domain}Price">{$domain.price}</span><br />
			                                                    <div class="dropdown">
			                                                        <button class="btn btn-default btn-xs dropdown-toggle" type="button" id="{$domain.domain}Pricing" name="{$domain.domain}Pricing" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
			                                                            {$domain.regperiod} {$domain.yearsLanguage}
			                                                            <span class="caret"></span>
			                                                        </button>
			                                                        <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="{$domain.domain}Pricing">
			                                                            {foreach $domain.pricing as $years => $price}
			                                                                <li>
			                                                                    <a class="text-right" href="#" onclick="selectDomainPeriodInCart('{$domain.domain}', '{$price.register}', {$years}, '{if $years == 1}{lang key='orderForm.year'}{else}{lang key='orderForm.years'}{/if}');return false;">
			                                                                        {$years} {if $years == 1}{lang key='orderForm.year'}{else}{lang key='orderForm.years'}{/if} @ {$price.register}
			                                                                    </a>
			                                                                </li>
			                                                            {/foreach}
			                                                        </ul>
			                                                    </div>
			                                                    <br />
			                                                    <span class="renewal cycle">
																	{lang key='domainrenewalprice'} <span class="renewal-price cycle">{$domain.renewprice->toPrefixed()}{$domain.shortYearsLanguage}</span>
																</span>
			                                                    <br />
			                                                {/if}
														</td>
													</tr>
												{/foreach}
												{foreach key=num item=domain from=$renewals}
													<tr>
														<td>
															<span class="item-title">
																<strong>{$LANG.domainrenewal}</strong>
															</span>
															<br />
															<span class="item-domain">
																{$domain.domain}
															</span>
															<span class="domain-details">
																{if $domain.dnsmanagement}<br /><i class="fa fa-check"></i> {$LANG.domaindnsmanagement}{/if}
																{if $domain.emailforwarding}<br /><i class="fa fa-check"></i> {$LANG.domainemailforwarding}{/if}
																{if $domain.idprotection}<br /><i class="fa fa-check"></i> {$LANG.domainidprotection}{/if}
															</span>
														</td>
														<td>
															<button type="button" class="btn btn-link btn-xs btn-remove-from-cart" onclick="removeItem('r','{$num}')">
																<i class="fa fa-times"></i>
																{$LANG.orderForm.remove}
															</button>
														</td>
														{if $showqtyoptions}
															<td class="text-right">
															</td>
														{/if}
														<td class="text-right">
															<span>{$domain.price}</span><br />
															<span class="cycle">{$domain.regperiod} {$LANG.orderyears}</span><br />
														</td>
													</tr>
												{/foreach}
												{if $promotioncode || $taxrate || $taxrate2}
												<tr class="total">
													<td colspan="2">
														<span><strong>{$LANG.ordersubtotal}</strong></span>
													</td>
													{if $showqtyoptions}
														<td class="text-right">
														</td>
													{/if}
													<td class="text-right">
														<strong><span id="subtotal">{$subtotal}</span></strong>
													</td>
												</tr>
												{/if}
												{if $promotioncode}
													<tr>
														<td colspan="2">
															<span>{$promotiondescription}</span>
														</td>
														{if $showqtyoptions}
															<td class="text-right">
															</td>
														{/if}
														<td class="text-right">
															- <span id="discount">{$discount}</span>
														</td>
													</tr>
												{/if}
												{if $taxrate}
													<tr>
														<td colspan="2">
															<span>{$taxname} @ {$taxrate}%</span>
														</td>
														{if $showqtyoptions}
															<td class="text-right">
															</td>
														{/if}
														<td class="text-right">
															<span id="taxTotal1">{$taxtotal}</span>
														</td>
													</tr>
												{/if}
												{if $taxrate2}
													<tr>
														<td colspan="2">
															<span>{$taxname2} @ {$taxrate2}%</span>
														</td>
														{if $showqtyoptions}
															<td class="text-right">
															</td>
														{/if}
														<td class="text-right">
															<span id="taxTotal2">{$taxtotal2}</span>
														</td>
													</tr>
												{/if}
												<tr class="total">
													<td colspan="2">
														<span><strong>{$LANG.ordertotalduetoday}</strong></span>
													</td>
													{if $showqtyoptions}
														<td class="text-right">
														</td>
													{/if}
													<td class="text-right">
														<strong><span id="totalDueToday">{$total}</span></strong>
													</td>
												</tr>
												<tr id="recurringMonthly" class="recurring" {if !$totalrecurringmonthly}style="display:none;"{/if}>
													<td colspan="2">
														<span>{$LANG.orderpaymenttermmonthly} {$LANG.cartrecurringcharges}</span>
													</td>
													{if $showqtyoptions}
														<td class="text-right">
														</td>
													{/if}
													<td class="text-right">
														<span class="cost">{$totalrecurringmonthly}</span>
													</td>
												</tr>
												<tr id="recurringQuarterly" class="recurring" {if !$totalrecurringquarterly}style="display:none;"{/if}>
													<td colspan="2">
														<span>{$LANG.orderpaymenttermquarterly} {$LANG.cartrecurringcharges}</span>
													</td>
													{if $showqtyoptions}
														<td class="text-right">
														</td>
													{/if}
													<td class="text-right">
														<span class="cost">{$totalrecurringquarterly}</span>
													</td>
												</tr>
												<tr id="recurringSemiAnnually" class="recurring" {if !$totalrecurringsemiannually}style="display:none;"{/if}>
													<td colspan="2">
														<span>{$LANG.orderpaymenttermsemiannually} {$LANG.cartrecurringcharges}</span>
													</td>
													{if $showqtyoptions}
														<td class="text-right">
														</td>
													{/if}
													<td class="text-right">
														<span class="cost">{$totalrecurringsemiannually}</span>
													</td>
												</tr>
												<tr id="recurringAnnually" class="recurring" {if !$totalrecurringannually}style="display:none;"{/if}>
													<td colspan="2">
														<span>{$LANG.orderpaymenttermannually} {$LANG.cartrecurringcharges}</span>
													</td>
													{if $showqtyoptions}
														<td class="text-right">
														</td>
													{/if}
													<td class="text-right">
														<span class="cost">{$totalrecurringannually}</span>
													</td>
												</tr>
												<tr id="recurringBiennially" class="recurring" {if !$totalrecurringbiennially}style="display:none;"{/if}>
													<td colspan="2">
														<span>{$LANG.orderpaymenttermbiennially} {$LANG.cartrecurringcharges}</span>
													</td>
													{if $showqtyoptions}
														<td class="text-right">
														</td>
													{/if}
													<td class="text-right">
														<span class="cost">{$totalrecurringbiennially}</span>
													</td>
												</tr>
												<tr id="recurringTriennially" class="recurring" {if !$totalrecurringtriennially}style="display:none;"{/if}>
													<td colspan="2">
														<span>{$LANG.orderpaymenttermtriennially} {$LANG.cartrecurringcharges}</span>
													</td>
													{if $showqtyoptions}
														<td class="text-right">
														</td>
													{/if}
													<td class="text-right">
														<span class="cost">{$totalrecurringtriennially}</span>
													</td>
												</tr>
											</tbody>
										</table>
									</div>
								</form>
								<div class="row">
									<div class="col-md-12 text-center">
										<br />
										<br />
										<a href="cart.php?a=checkout" class="btn btn-3d btn-lg btn-primary" id="checkout">
											{$LANG.orderForm.checkout}
											<i class="fa fa-arrow-right"></i>
										</a>
										<br />
										<br />
										<br />
										<br />
									</div>
								</div>
								<div class="row">
									<div class="{if $taxenabled && !$loggedin}col-md-3{else}col-md-4{/if} text-center">
										<a href="cart.php" class="btn btn-link btn-continue-shopping" id="continueShopping">
											<i class="fa fa-cart-plus"></i>
											{$LANG.orderForm.continueShopping}
										</a>
									</div>
									<div class="{if $taxenabled && !$loggedin}col-md-3{else}col-md-4{/if} text-center">
										{if $promotioncode}
											<button type="button" class="btn btn-link" id="btnPromoCodeRemove">
												<i class="fa fa-ticket"></i>
												{$LANG.orderForm.removePromotionCode}
											</button>
										{else}
											<button type="button" class="btn btn-link" id="btnPromoCodeApply">
												<i class="fa fa-ticket"></i>
												{$LANG.orderForm.applyPromoCode}
											</button>
										{/if}
									</div>
									{if $taxenabled && !$loggedin}
										<div class="col-md-3 text-center">
											<button type="button" class="btn btn-link" id="btnEstimateTaxes">
												<i class="fa fa-percent"></i>
												{$LANG.orderForm.estimateTaxes}
											</button>
										</div>
									{/if}
									<div class="{if $taxenabled && !$loggedin}col-md-3{else}col-md-4{/if} text-center">
										<button type="button" class="btn btn-link" id="btnEmptyCart">
											<i class="fa fa-trash"></i>
											{$LANG.emptycart}
										</button>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12 text-center">
										{foreach $gatewaysoutput as $gatewayoutput}
											<div class="view-cart-gateway-checkout">
												{$gatewayoutput}
											</div>
										{/foreach}
									</div>
								</div>
								<br />
								<br />
								<!-- Modals --->
								<!-- Modal 1 - Remove Item -->
								<form method="post" action="cart.php">
										<input type="hidden" name="a" value="remove" />
										<input type="hidden" name="r" value="" id="inputRemoveItemType" />
										<input type="hidden" name="i" value="" id="inputRemoveItemRef" />
										<div class="modal fade" id="modalRemoveItem" tabindex="-1" role="dialog">
												<div class="modal-dialog" role="document">
														<div class="modal-content">
																<div class="modal-header">
																		<button type="button" class="close" data-dismiss="modal" aria-label="{$LANG.orderForm.close}">
																				<span aria-hidden="true">&times;</span>
																		</button>
																		<h4 class="modal-title">
																			<span>{$LANG.orderForm.removeItem}</span>
																		</h4>
																</div>
																<div class="modal-body">
																		{$LANG.cartremoveitemconfirm}
																</div>
																<div class="modal-footer">
																		<button type="submit" class="btn btn-primary btn-3d pull-left">{$LANG.orderForm.removeItem}</button>
																		<button type="button" class="btn btn-default pull-right" data-dismiss="modal">{$LANG.cancel}</button>
																</div>
														</div>
												</div>
										</div>
								</form>
								<!-- Modal 2 - Empty Cart -->
								<form method="post" action="cart.php">
										<input type="hidden" name="a" value="empty" />
										<div class="modal fade modal-remove-item" id="modalEmptyCart" tabindex="-1" role="dialog">
												<div class="modal-dialog" role="document">
														<div class="modal-content">
																<div class="modal-header">
																		<button type="button" class="close" data-dismiss="modal" aria-label="{$LANG.orderForm.close}">
																				<span aria-hidden="true">&times;</span>
																		</button>
																		<h4 class="modal-title">
																				<span>{$LANG.emptycart}</span>
																		</h4>
																</div>
																<div class="modal-body">
																		{$LANG.cartemptyconfirm}
																</div>
																<div class="modal-footer">
																	<button type="submit" class="btn btn-3d btn-primary pull-left ">{$LANG.emptycart}</button>
																	<button type="button" class="btn btn-default pull-right" data-dismiss="modal">{$LANG.cancel}</button>
																</div>
														</div>
												</div>
										</div>
								</form>
								<!-- Modal 3 - Calculate Tax -->
								<form method="post" action="cart.php?a=setstateandcountry">
									<div class="modal fade modal-remove-item" id="modalEstimateTaxes" tabindex="-1" role="dialog">
												<div class="modal-dialog" role="document">
														<div class="modal-content">
																<div class="modal-header">
																		<button type="button" class="close" data-dismiss="modal" aria-label="{$LANG.orderForm.close}">
																				<span aria-hidden="true">&times;</span>
																		</button>
																		<h4 class="modal-title">
																				<span>{$LANG.orderForm.estimateTaxes}</span>
																		</h4>
																</div>
																<div class="modal-body">
																	<div class="form-group"> 
																		<label for="inputState" class="control-label">{$LANG.orderForm.state}</label>
																		<input type="text" name="state" id="inputState" value="{$clientsdetails.state}" class="form-control"{if $loggedin} disabled="disabled"{/if} />
																	</div>
																	<div class="form-group"> 
																		<label for="inputCountry" class="control-label">{$LANG.orderForm.country}</label>
																		<select name="country" id="inputCountry" class="form-control">
																				{foreach $countries as $countrycode => $countrylabel}
																						<option value="{$countrycode}"{if (!$country && $countrycode == $defaultcountry) || $countrycode eq $country} selected{/if}>
																								{$countrylabel}
																						</option>
																				{/foreach}
																		</select>
																	</div>
																</div>
																<div class="modal-footer">
																	<button type="submit" class="btn btn-3d btn-primary pull-left ">{$LANG.orderForm.updateTotals}</button>
																	<button type="button" class="btn btn-default pull-right" data-dismiss="modal">{$LANG.cancel}</button>
																</div>
														</div>
												</div>
										</div>
								</form>
								<!-- Modal 4 - Promo Code --->
								{if $promotioncode}
									<div class="modal fade modal-remove-item" id="modalPromoCodeRemove" tabindex="-1" role="dialog">
										<div class="modal-dialog" role="document">
											<div class="modal-content">
												<div class="modal-header">
													<button type="button" class="close" data-dismiss="modal" aria-label="{$LANG.orderForm.close}">
														<span aria-hidden="true">&times;</span>
													</button>
													<h4 class="modal-title">
														<span>{$LANG.orderForm.removePromotionCode}</span>
													</h4>
												</div>
												<div class="modal-body">
													{$promotioncode} - {$promotiondescription}
												</div>
												<div class="modal-footer">
													<a href="{$smarty.server.PHP_SELF}?a=removepromo" class="btn btn-primary btn-3d pull-left">{$LANG.orderForm.removePromotionCode}</a>
													<button type="button" class="btn btn-default pull-right" data-dismiss="modal">{$LANG.cancel}</button>
												</div>
											</div>
										</div>
									</div>
								{else}
									<form method="post" action="cart.php?a=view">
										<div class="modal fade modal-remove-item" id="modalPromoCodeApply" tabindex="-1" role="dialog">
											<div class="modal-dialog" role="document">
												<div class="modal-content">
													<div class="modal-header">
														<button type="button" class="close" data-dismiss="modal" aria-label="{$LANG.orderForm.close}">
															<span aria-hidden="true">&times;</span>
														</button>
														<h4 class="modal-title">
															<span>{$LANG.orderForm.applyPromoCode}</span>
														</h4>
													</div>
													<div class="modal-body">
														 <div class="input-group"> 
															<div class="input-group-addon"><i class="fa fa-ticket"></i></div>
															<input type="text" class="form-control" name="promocode" id="inputPromotionCode" placeholder="{$LANG.orderPromoCodePlaceholder}" required="required">
														</div>
													</div>
													<div class="modal-footer">
														<button type="submit" name="validatepromo" class="btn btn-primary btn-3d pull-left" value="{$LANG.orderpromovalidatebutton}">{$LANG.orderpromovalidatebutton}</button>
														<button type="button" class="btn btn-default pull-right" data-dismiss="modal">{$LANG.cancel}</button>
													</div>
												</div>
											</div>
										</div>
									</form>
								{/if}
								<!-- End of Modals --->
							{/if}
						</div>
					</div>
					{if $itCartSidebarDisplay eq "enabled"}
				</div>
			<div class="col-md-3 pull-md-right whmcs-sidebar hidden-xs hidden-sm sidebar-secondary {if $itCartSidebarDisplayMode eq "showhide"}{if $itCartSidebarDisplayMethod eq "squash"}cart-sidebar-squash{else}cart-sidebar{/if}{/if}">
				{include file="orderforms/$carttpl/sidebar-categories.tpl"}
				</div>
				<div class="clearfix"></div>
			</div>
		{/if}
		<div class="clearfix"></div>
	</section>
	{include file="orderforms/$carttpl/icheck.tpl"}
{/if}