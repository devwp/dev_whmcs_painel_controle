
	{if $condlinks.domainreg || $condlinks.domaintrans || $condlinks.domainown}
		<form method="post" action="domainchecker.php" class="">
			<div class="row">
				<div class="col-md-12">
					<div class="input-group input-group-lg input-group-box">
						<input type="text" class="form-control" placeholder="{$LANG.findyourdomain}" value="{$domain}" id="inputDomain" name="domain" {if !$loggedin}oninput="showCaptcha()" {/if}/>
						<div class="input-group-btn">

							<button type="button" id="bulkoptions" class="hidden-xs btn btn-default btn-trans dropdown-toggle btn-square" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{$LANG.bulkoptions}<span class="caret"></span></button>
        <ul class="dropdown-menu">
			<li><a href="domainchecker.php?search=bulkregister">{$LANG.domainbulksearch}</a></li>
			<li><a href="domainchecker.php?search=bulktransfer">{$LANG.domainbulktransfersearch}</a>

        </ul>
							<button type="submit" id="btnCheckAvailability" class="btn btn-primary domain-check-availability">{$LANG.search}</button>

						</div>
					</div>
					<div class="form-group text-center">
						<button type="button" class="res-100 visible-xs-block btn btn-default btn-trans dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{$LANG.bulkoptions}<span class="caret"></span></button>
						<ul class="dropdown-menu">
							<li><a href="domainchecker.php?search=bulkregister">{$LANG.domainbulksearch}</a></li>
							<li><a href="domainchecker.php?search=bulktransfer">{$LANG.domainbulktransfersearch}</a>

						</ul>
					</div>
					{include file="$template/includes/captcha.tpl"}
				</div>
			</div>
		</form>
	{/if}
	<div class="row">
		<div class="col-md-6">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">{$LANG.navservicesorder}</h3>
				</div>
				<div class="panel-body">
					<p>{$LANG.clientareahomeorder}</p>
				</div>
				<div class="panel-footer">
					<form method="post" action="cart.php">
						<input type="submit" value="{$LANG.clientareahomeorderbtn}" class="btn res-100 btn-primary" />
					</form>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">{$LANG.manageyouraccount}</h3>
				</div>
				<div class="panel-body">
					<p>{$LANG.clientareahomelogin}</p>
				</div>
				<div class="panel-footer">
					<form method="post" action="clientarea.php">
						<input type="submit" value="{$LANG.clientareahomeloginbtn}" class="btn res-100 btn-primary" />
					</form>
				</div>
			</div>
		</div>
	</div>
	{if $twitterusername}
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">{$LANG.twitterlatesttweets}</h3>
					</div>
					<div class="panel-body">
						<div id="twitterfeed">
							{if $itColorScheme eq "blue" or $itColorScheme eq "blue-solid" or $itColorScheme eq "blue-white"}
								<p><img src="templates/{$template}/img/loading-blue.gif"></p>
							{else}
								<p><img src="templates/{$template}/img/loading.gif"></p>
							{/if}

						</div>
						{literal}
							<script language="javascript">
								jQuery(document).ready(function(){
								  jQuery.post("announcements.php", { action: "twitterfeed", numtweets: 3 },
									function(data){
									  jQuery("#twitterfeed").html(data);
									});
								});
							</script>
						{/literal}
					</div>
				</div>
			</div>
		</div>
	{elseif $announcements}
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">{$LANG.latestannouncements}</h3>
					</div>
					<div class="panel-body">
						{foreach from=$announcements item=announcement}
							<a href="{if $seofriendlyurls}announcements/{$announcement.id}/{$announcement.urlfriendlytitle}.html{else}announcements.php?id={$announcement.id}{/if}"><h4>{$announcement.title} <small class="pull-right">{$announcement.date}</small></h4></a>
							{$announcement.text|strip_tags|truncate:200:"..."}
							{if strlen($announcement.text)>400}<a href="{if $seofriendlyurls}announcements/{$announcement.id}/{$announcement.urlfriendlytitle}.html{else}{$smarty.server.PHP_SELF}?id={$announcement.id}{/if}" class="btn btn-primary res-100 btn-sm">{$LANG.more} &raquo;</a>{/if}
							<hr />
						{/foreach}
					</div>
					<div class="panel-footer">
						<a href="announcements.php" class="btn res-100 btn-sm btn-default">{$LANG.more} {$LANG.announcementstitle}</a>
					<a href="announcementsrss.php" class="btn res-100 res-left btn-sm btn-default pull-right" title="">{$LANG.announcementsrss} <i class="fa fa fa-rss"></i></a>
					</div>
				</div>
			</div>
		</div>
	{/if}	
