<!-- Customized Bootstrap -->
<link href="{$WEB_ROOT}/templates/{$template}/css/bootstrap.min.css" rel="stylesheet">
<link href="{$BASE_PATH_CSS}/font-awesome.min.css" rel="stylesheet">
<!-- CSS Animate -->
<link rel="stylesheet" href="{$WEB_ROOT}/templates/{$template}/css/animate.css">
<!-- Styling -->
{if $itColorScheme eq "blue"}
	<link rel="stylesheet" href="{$WEB_ROOT}/templates/{$template}/css/main-blue.css">
{elseif $itColorScheme eq "blue-solid"}
	<link rel="stylesheet" href="{$WEB_ROOT}/templates/{$template}/css/main-blue-solid.css">
{elseif $itColorScheme eq "blue-white"}
	<link rel="stylesheet" href="{$WEB_ROOT}/templates/{$template}/css/main-blue-white.css">
{elseif $itColorScheme eq "green-solid"}
	<link rel="stylesheet" href="{$WEB_ROOT}/templates/{$template}/css/main-green-solid.css">
{elseif $itColorScheme eq "green-white"}
	<link rel="stylesheet" href="{$WEB_ROOT}/templates/{$template}/css/main-green-white.css">
{else}
	<link rel="stylesheet" href="{$WEB_ROOT}/templates/{$template}/css/main.css">
{/if}
<!-- jQuery -->
<script src="{$BASE_PATH_JS}/jquery.min.js"></script>
<!-- Custom Styling -->
<link rel="stylesheet" href="{$WEB_ROOT}/templates/{$template}/css/custom.css">
<!-- Feature detection -->
<script src="{$WEB_ROOT}/templates/{$template}/js/modernizr-2.6.2.min.js"></script>
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
<script src="{$WEB_ROOT}/templates/{$template}/js/html5shiv.js"></script>
<script src="{$WEB_ROOT}/templates/{$template}/js/respond.min.js"></script>
<![endif]-->
<!-- Additional Font Icons -->
<link rel="stylesheet" href="{$WEB_ROOT}/templates/{$template}/css/simple-line-icons.css">
<!-- Custom Fonts -->
<link href="//fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,900,300italic,400italic,600italic,700italic,900italic" rel="stylesheet" type="text/css">
<link href="//fonts.googleapis.com/css?family=Open+Sans:400,700" rel="stylesheet" type="text/css">
{if !empty($loadMarkdownEditor)}
    <!-- Markdown Editor -->
    <link href="{$BASE_PATH_CSS}/bootstrap-markdown.min.css" rel="stylesheet" />
    <script src="{$BASE_PATH_JS}/bootstrap-markdown.js"></script>
    <script src="{$BASE_PATH_JS}/markdown.min.js"></script>
    <script src="{$BASE_PATH_JS}/to-markdown.js"></script>
    {if !empty($mdeLocale)}
        {$mdeLocale}
    {/if}
{/if}
{if $templatefile == "viewticket" && !$loggedin}
	<meta name="robots" content="noindex" />
{/if}