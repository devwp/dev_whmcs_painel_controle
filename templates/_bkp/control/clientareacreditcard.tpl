{if $remoteupdatecode}
	<div align="center">
		{$remoteupdatecode}
	</div>
{else}
	{if $successful}
		{include file="$template/includes/alert.tpl" type="success" msg=$LANG.changessavedsuccessfully textcenter=true}
	{/if}
	{if $errormessage}
		{include file="$template/includes/alert.tpl" type="error" errorshtml=$errormessage}
	{/if}
	<div class="row">
		<div class="col-md-6">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">{$displayTitle}</h3>
				</div>
				<div class="panel-body">
					{if $cardlastfour}
						<div class="card-icon pull-right">
							<b class="fa fa-3x
							{if $cardtype eq "American Express"}
								fa-cc-amex logo-amex
							{elseif $cardtype eq "Visa"}
								fa-cc-visa logo-visa
							{elseif $cardtype eq "MasterCard"}
								fa-cc-mastercard logo-mastercard
							{elseif $cardtype eq "Discover"}
								fa-cc-discover logo-discover
							{else}
								fa-credit-card
							{/if}"></b>
						</div>
					{/if}
					<p>{$cardtype}</p>
					<p>{if $cardlastfour}xxxx xxxx xxxx {$cardlastfour}{else}{$LANG.creditcardnonestored}{/if}</p>
					{if $allowcustomerdelete && $cardtype}
						<div class="pull-right">
							<form method="post" class="pull-right" action="clientarea.php?action=creditcard">
								<input type="hidden" name="remove" value="1" />
								<button type="submit" class="btn btn-danger btn-sm pull-right">
									{$LANG.creditcarddelete}
								</button>
							</form>
						</div>
					{/if}
					{if $cardstart}<p>Start: {$cardstart}</p>{/if}
					{if $cardexp}<p>Expires: {$cardexp}</p>{/if}
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<form class="" role="form" method="post" action="{$smarty.server.PHP_SELF}?action=creditcard">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">{$LANG.creditcardenternewcard}</h3>
					</div>
					<div class="panel-body">
						<div class="form-group">
							<label for="inputCardType" class="control-label">{$LANG.creditcardcardtype}</label>
							<select name="cctype" id="inputCardType" class="form-control">
								{foreach from=$acceptedcctypes item=fieldcardtype}
									<option {if $fieldcardtype eq $cardtype}selected{/if}>{$fieldcardtype}</option>
								{/foreach}
							</select>
						</div>
						<div class="form-group">
							<label for="inputCardNumber" class="control-label">{$LANG.creditcardcardnumber}</label>
							<input type="text" class="form-control" id="inputCardNumber" name="ccnumber" autocomplete="off" />
						</div>
						{if $showccissuestart}
							<div class="form-group">
								<label for="inputCardStart" class="control-label">{$LANG.creditcardcardstart}</label>
								<div class="row">
									<div class="col-sm-4">
										<select name="ccstartmonth" id="inputCardStart" class="form-control">
											{foreach from=$months item=month}
												<option{if $ccstartmonth eq $month} selected{/if}>{$month}</option>
											{/foreach}
										</select>
									</div>
									<div class="col-sm-7 col-sm-offset-1">
										<select name="ccstartyear" class="form-control">
											{foreach from=$startyears item=year}
												<option{if $ccstartyear eq $year} selected{/if}>{$year}</option>
											{/foreach}
										</select>
									</div>
								</div>
							</div>
						{/if}
						<div class="form-group">
							<label for="inputCardExpiry" class="control-label">{$LANG.creditcardcardexpires}</label>
							<div class="row">
								<div class="col-xs-4">
									<select name="ccexpirymonth" id="inputCardExpiry" class="form-control">
										{foreach from=$months item=month}
											<option{if $ccstartmonth eq $month} selected{/if}>{$month}</option>
										{/foreach}
									</select>
								</div>
								<div class=" col-xs-7 col-xs-offset-1">
									<select name="ccexpiryyear" class="form-control">
										{foreach from=$expiryyears item=year}
											<option{if $ccstartyear eq $year} selected{/if}>{$year}</option>
										{/foreach}
									</select>
								</div>
							</div>
						</div>
						{if $showccissuestart}
							<div class="form-group">
								<label for="inputCardIssue" class="control-label">{$LANG.creditcardcardissuenum}</label>
								<input type="text" class="form-control" id="inputCardIssue" name="ccissuenum" autocomplete="off" />
							</div>
						{/if}
						<div class="form-group">
							<label for="inputCardCVV" class="control-label">{$LANG.creditcardcvvnumber}</label>
							<a class="btn btn-link btn-cvv" data-toggle="popover" data-content="<img src='{$BASE_PATH_IMG}/ccv.gif' width='210' />" data-placement="bottom">
								{$LANG.creditcardcvvwhere}
							</a>
							<input type="text" class="form-control" id="inputCardCVV" name="cardcvv" autocomplete="off" />
						</div>
					</div>
					<div class="panel-footer">
						<input class="btn btn-primary btn-3d" type="submit" name="submit" value="{$LANG.clientareasavechanges}" />
						<input class="btn btn-default pull-right" type="reset" value="{$LANG.cancel}" />
					</div>
				</div>
			</form>
		</div>
	</div>
{/if}