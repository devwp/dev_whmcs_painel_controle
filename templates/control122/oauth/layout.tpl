<!DOCTYPE html>
<!-- Load theme options -->
{include file="$template/includes/theme-options.tpl"}
<!-- Load theme functions -->
{include file="$template/includes/theme-functions.tpl"}
{if $itTextDirectionRTL eq "enabled"}
	<html dir="rtl">
{else}
	<html>
{/if}
  <head>
    <meta charset="{$charset}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{$requestedAction} - {$companyname}</title>
	<!-- Styling -->
	{if $itColorScheme eq "custom"}
		<link rel="stylesheet" href="{$WEB_ROOT}/templates/{$template}/css/{$itColorSchemeCustomName}{if $itCSSMin eq "enabled"}.min{/if}.css?v={$versionHash}">
	{elseif $itColorScheme eq "blue"}
		<link rel="stylesheet" href="{$WEB_ROOT}/templates/{$template}/css/main-blue{if $itCSSMin eq "enabled"}.min{/if}.css?v={$versionHash}">
	{elseif $itColorScheme eq "blue-solid"}
		<link rel="stylesheet" href="{$WEB_ROOT}/templates/{$template}/css/main-blue-solid{if $itCSSMin eq "enabled"}.min{/if}.css?v={$versionHash}">
	{elseif $itColorScheme eq "blue-white"}
		<link rel="stylesheet" href="{$WEB_ROOT}/templates/{$template}/css/main-blue-white{if $itCSSMin eq "enabled"}.min{/if}.css?v={$versionHash}">
	{elseif $itColorScheme eq "green-solid"}
		<link rel="stylesheet" href="{$WEB_ROOT}/templates/{$template}/css/main-green-solid{if $itCSSMin eq "enabled"}.min{/if}.css?v={$versionHash}">
	{elseif $itColorScheme eq "green-white"}
		<link rel="stylesheet" href="{$WEB_ROOT}/templates/{$template}/css/main-green-white{if $itCSSMin eq "enabled"}.min{/if}.css?v={$versionHash}">
	{else}
		<link rel="stylesheet" href="{$WEB_ROOT}/templates/{$template}/css/main{if $itCSSMin eq "enabled"}.min{/if}.css?v={$versionHash}">
	{/if}
	{if $itTextDirectionRTL eq "enabled"}
		<!-- Load Bootstrap RTL theme -->
		<link rel="stylesheet" href="{$WEB_ROOT}/templates/{$template}/css/bootstrap-rtl.min.css">	
	{/if}
	<!-- Custom Styling -->
	<link rel="stylesheet" href="{$WEB_ROOT}/templates/{$template}/css/custom.css">
	<!-- JS -->
	<script src="{$WEB_ROOT}/templates/{$template}/js/scripts{if $itJSMin eq "enabled"}.min{/if}.js?v={$versionHash}"></script>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>

    <section id="header">
        <div class="container">
            <img src="{$logo}" />
            <div class="pull-right text-right">
                {if $loggedin}
                    <form method="post" action="{$issuerurl}oauth/authorize.php" id="frmLogout">
                        <input type="hidden" name="logout" value="1"/>
                        <input type="hidden" name="request_hash" value="{$request_hash}"/>
                        <p>
                            {lang key='oauth.currentlyLoggedInAs' firstName=$loggedinuser.firstname lastName=$loggedinuser.lastname}.
                            <a href="#" onclick="jQuery('#frmLogout').submit()">{lang key='oauth.notYou'}</a>
                        </p>
                    </form>
                {/if}
                <form method="post" action="{$issuerurl}oauth/authorize.php" id="frmCancelLogin">
                    <input type="hidden" name="return_to_app" value="1"/>
                    <input type="hidden" name="request_hash" value="{$request_hash}"/>
                    <button type="submit" class="btn btn-oauth">
                        {lang key='oauth.returnToApp' appName=$appName}
                    </button>
                </form>
            </div>
        </div>
    </section>

    <section id="content">
        {$content}
    </section>

    <section id="footer">
        {lang key='oauth.copyrightFooter' dateYear=$date_year companyName=$companyname}
    </section>
    
  </body>
</html>
