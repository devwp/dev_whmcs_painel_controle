{if $loggedin}
	{include file="$template/includes/alert.tpl" type="error" msg=$LANG.noPasswordResetWhenLoggedIn textcenter=true}
{else}
	{if $success}
		<div class="row">
			<!--div class="col-md-12"-->
			<div class="col-md-6 col-md-offset-3">

				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">{$LANG.pwreset}</h3>
					</div>
					<div class="panel-body">
						<div class="alert alert-success">
							<p>{$LANG.pwresetvalidationsent}</p>
						</div>
						<p>{$LANG.pwresetvalidationcheckemail}</p>
					</div>
				</div>
			</div>
		</div>
	{else}
		{if $errormessage}
			{include file="$template/includes/alert.tpl" type="error" msg=$errormessage textcenter=true}
		{/if}
		<form method="post" action="pwreset.php"  class="form-horizontal">
			<input type="hidden" name="action" value="reset" />
			{if $securityquestion}
				<div class="row">
					<!--div class="col-md-12"-->
					<div class="col-md-6 col-md-offset-3">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h3 class="panel-title">{$LANG.pwreset}</h3>
							</div>
							<div class="panel-body">
								<input type="hidden" name="email" value="{$email}" />
								<p>{$LANG.pwresetsecurityquestionrequired}</p>
								<br />
								<fieldset>
									<div class="form-group">
										<label class="col-sm-3 control-label" for="answer">{$securityquestion}</label>
										<div class="col-sm-6">
											<input class="form-control" name="answer" id="answer" type="text" value="{$answer}" />
										</div>
									</div>
								</fieldset>
							</div>
							<div class="panel-footer">
								<input type="submit" class="btn btn-3d res-100 btn-primary" value="{$LANG.pwresetsubmit}" />
							</div>
						</div>
					</div>
				</div>
			{else}
				<div class="row">
					<div class="col-md-6 col-md-offset-3">
					<!--div class="col-md-12"-->
						<div class="panel panel-default">
							<div class="panel-heading">
								<h3 class="panel-title">{$LANG.pwreset}</h3>
							</div>
							<div class="panel-body">
								<input type="hidden" name="action" value="reset" />
								<p>{$LANG.pwresetemailneeded}</p>
								<br />
								<fieldset>
									<div class="form-group">
										<label class="col-sm-3 control-label" for="email">{$LANG.loginemail}</label>
										<div class="col-sm-6">
											<input class="form-control" name="email" id="email" type="text" />
										</div>
									</div>
								</fieldset>
							</div>
							<div class="panel-footer">
								<input type="submit" class="btn btn-3d res-100 btn-primary" value="{$LANG.pwresetsubmit}" />
							</div>
						</div>
					</div>
				</div>
			{/if}
		</form>
	{/if}
{/if}
