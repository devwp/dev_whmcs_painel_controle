{*		
		Impressive Themes
		http://www.impressivethemes.net
		-------------------------------
		
		Theme Debug Information Output for Control Theme by Impressive Themes
		---------------------------------------------------------------------
		
		We would advise customers not to make any changes to the 
		file below. Configurable theme options can be found in the 
		file theme-options.tpl.
		
		Should you require any further help or assistance, please 
		contact customer support via our online support desk at:
		
		http://www.impressivethemes.net/support/contact
		
*}

{* 	1. 		If theme debug is enabled, display theme options and functions variables for test and debugging purposes *}

			<section id="output-variables">
				<div class="alert alert-warning" style="margin-bottom: 0px; border-radius: 0;">
					<div class="container">
						<div class="row">
							<div class="col-md-6">
								<h4>Theme Options Variables</h4>
								<p><strong>1.1. itBrandDisplay: </strong>{$itBrandDisplay}</p>
								<p><strong>1.2. itBrandType: </strong>{$itBrandType}</p>
								<p><strong>1.3. itBrandLogoUrl: </strong>{$itBrandLogoUrl}</p>
								<p><strong>1.4. itBrandText: </strong>{$itBrandText}</p>
								<p><strong>1.5. itBreadcrumbDisplay: </strong>{$itBreadcrumbDisplay}</p>
								<p><strong>1.6. itCartIconDisplayDesktop: </strong>{$itCartIconDisplayDesktop}</p>
								<p><strong>1.7. itCartIconDisplayMobile: </strong>{$itCartIconDisplayMobile}</p>
								<p><strong>1.8. itNotificationsIconDisplayDesktop: </strong>{$itNotificationsIconDisplayDesktop}</p>
								<p><strong>1.9. itNotificationsIconDisplayMobile: </strong>{$itNotificationsIconDisplayMobile}</p>
								
								<p><strong>1.10. itLanguageIconDisplayDesktop: </strong>{$itLanguageIconDisplayDesktop}</p>
								<p><strong>1.11. itLanguageIconDisplayMobile: </strong>{$itLanguageIconDisplayMobile}</p>
								<p><strong>1.12. itLanguageIconDisplayFooter: </strong>{$itLanguageIconDisplayFooter}</p>
								<p><strong>1.13. itGuestNavDisplay: </strong>{$itGuestNavDisplay}</p>
								<p><strong>2.1. itPrimarySidebar: </strong>{$itPrimarySidebar}</p>
								<p><strong>2.2. itSecondarySidebar: </strong>{$itSecondarySidebar}</p>
								<p><strong>3.1. itThemeDebug: </strong>{$itThemeDebug}</p>
								<br/><br/>
							</div>
							<div class="col-md-6">
								<h4>Theme Functions Variables</h4>
								<p><strong>itShowSidebarPrimary: </strong>{$itShowSidebarPrimary}</p>
								<p><strong>itShowSidebarSecondary: </strong>{$itShowSidebarSecondary}</p>
								<p><strong>itShowSidebarLayout: </strong>{$itShowSidebarLayout}</p>
								<p><strong>itFeaturedContent: </strong>{$itFeaturedContent}</p>
								<p><strong>itShowMainNav: </strong>{$itShowMainNav}</p>
								<br/><br/>
							</div>
						</div>
					</div>
				</div>
			</section>