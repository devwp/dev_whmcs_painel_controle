{if $modulecustombuttonresult}
	{if $modulecustombuttonresult == "success"}
		{include file="$template/includes/alert.tpl" type="success" msg=$LANG.moduleactionsuccess textcenter=true idname="alertModuleCustomButtonSuccess"}
	{else}
		{include file="$template/includes/alert.tpl" type="error" msg=$LANG.moduleactionfailed|cat:' ':$modulecustombuttonresult textcenter=true idname="alertModuleCustomButtonFailed"}
	{/if}
{/if}
{if $pendingcancellation}
	{include file="$template/includes/alert.tpl" type="error" msg=$LANG.cancellationrequestedexplanation textcenter=true idname="alertPendingCancellation"}
{/if}
{if $domain AND $type eq "hostingaccount" AND $serverdata.type eq "plesk"}
<ul class="nav nav-tabs">
  <li class="active"><a  data-toggle="tab" href="#tabOverview">{$LANG.yourdetails} </a></li>
  <!--<li><a data-toggle="tab" href="#tabDownloads">{$LANG.downloadsfiles}</a></li>
  <li><a data-toggle="tab" href="#tabAddons">{$LANG.clientareahostingaddons}</a></li>-->
  <li><a data-toggle="tab" href="#tabChangepw">{$LANG.serverchangepassword}</a></li>
  <li><a href="upgrade.php?type=configoptions&id={$id}">{$LANG.upgradedowngradepackage}</a></li>
</ul>
{/if}

{if $serverdata.type eq "smartermail"}
	    {if $servercustombuttons.vars.IsDedicated eq "on" }
	        <h3 style="padding:10px;">Serviço: E-mail Dedicado</h3>
	    {else} 
	        <h3 style="padding:10px;">Serviço: E-mail</h3>
	    {/if} 

		{if $servercustombuttons.vars.IsDedicated eq "on" }
		<ul class="nav nav-tabs">
		  <li class="active"><a  data-toggle="tab" href="#tabOverview">{$LANG.yourdetails} </a></li>
			<li><a href="clientarea.php?action=productdetails&id={$id}&modop=custom&a=managesmartermaildomains">{$LANG.smmanageDomain}</a></li>
			 <!--<li><a href="/clientarea.php?action=productdetails&id=76&modop=custom&a=managesmartermaildomain#alias">{$LANG.smmanageAliasDom}</a></li>

			<li><a  href="/clientarea.php?action=productdetails&id=76&modop=custom&a=managesmartermaildomain#mailinglist">{$LANG.smmanagemailinglist}</a></li>
		 <li><a data-toggle="tab" href="#tabDownloads">{$LANG.downloadsfiles}</a></li>
		  <li><a data-toggle="tab" href="#tabAddons">{$LANG.clientareahostingaddons}</a></li>
		  <li><a data-toggle="tab" href="#tabChangepw">{$LANG.serverchangepassword}</a></li>-->
		  <!--li><a href="upgrade.php?type=package&id={$id}">{$LANG.upgradedowngradepackage}</a></li-->
		</ul>
		{else}
		<ul class="nav nav-tabs">
		  <li class="active"><a  data-toggle="tab" href="#tabOverview">{$LANG.yourdetails} </a></li>
		  <!--<li><a data-toggle="tab" href="#tabDownloads">{$LANG.downloadsfiles}</a></li>
		  <li><a data-toggle="tab" href="#tabAddons">{$LANG.clientareahostingaddons}</a></li>
		  <li><a data-toggle="tab" href="#tabChangepw">{$LANG.serverchangepassword}</a></li>-->
		  <li><a href="clientarea.php?action=productdetails&id={$id}&modop=custom&a=managesmartermaildomain">{$LANG.smmanageusers}</a></li>
		 <li><a href="/clientarea.php?action=productdetails&id=76&modop=custom&a=managesmartermaildomain#alias">{$LANG.smmanageAliasDom}</a></li>
		</ul>
		{/if}
{/if}
<div class="tab-content margin-bottom">
	<div class="tab-pane fade in active" id="tabOverview">
		{if $tplOverviewTabOutput}
			{$tplOverviewTabOutput}
		{else}
			<div class="product-details clearfix">
				<div class="row">
					<div class="col-md-6">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h3 class="panel-title">{if $domain}{$LANG.cPanel.packageDomain}{else}{$LANG.clientareaproductdetails}{/if}<span class="pull-right label status status-{$rawstatus|strtolower}">{$status}</span></h3>
							</div>
							<div class="panel-body text-center">
								<div class="package-details">
									<em>{$groupname}</em>
									<h4 style="margin:0;">{$product}</h4>
								</div>
								{if $domain}
									{if $type eq "server"}
										<hr/>
										<p>
											<strong>{$LANG.serverhostname}</strong>
											<br/>
											{$domain}
										</p>
										{if $dedicatedip}
											<hr/>
											<p style="display:none">
												<strong>{$LANG.primaryIP}</strong>
												<br/>
												{$dedicatedip}
											</p>
										{/if}
										{if $assignedips}
											<hr/>
										<p style="display:none">
												<strong>{$LANG.assignedIPs}</strong>
												<br/>
												{$assignedips|nl2br}
											</p>
										{/if}
										{if $ns1 || $ns2}
											<hr/>
											<p>
												<strong>{$LANG.domainnameservers}</strong>
												<br/>
												{if $ns1}
													{$ns1}
													<br />
												{/if}
												{if $ns2}
													{$ns2}
													<br/>
												{/if}
											</p>
										{/if}
									{elseif ($type eq "hostingaccount" || $type eq "reselleraccount") && $serverdata}
										{if $domain}
											{if $serverdata.type eq "smartermail"}
											<hr/>
											<p>
												<strong>{$LANG.orderdomain}</strong>
												<br/>
												{$domain}
											</p>
											<p>
												<a href="http://webmail.{$domain}" target="_blank" class="btn btn-default btn-sm"  >{$LANG.cpanelwebmaillogin}</a>
											</p>
											{else}
											<hr/>
											<p>
												<strong>{$LANG.orderdomain}</strong>
												<br/>
												{$domain}
											</p>
											<p>
												<a href="http://{$domain}" target="_blank" class="btn btn-default btn-sm" >{$LANG.visitwebsite}</a>
											</p>
											{/if}
										{/if}
										{if $username AND ($serverdata.type neq "smartermail" AND $serverdata.type neq "plesk")}
											<hr/>
											<p>
												<strong>{$LANG.serverusername}</strong>
												<br/>
												{$username}
											</p>
										{/if}
										{if $serverdata.type neq "smartermail" AND $serverdata.type neq "plesk"}
										<hr/>
										<p>
											<strong>{$LANG.servername}</strong>
											<br/>
											{$serverdata.hostname}
										</p>
										{/if}
										{if $serverdata.type neq "smartermail"}
										<hr/>
									<p style="display:none">
											<strong>{$LANG.domainregisternsip}</strong>
											<br/>
											{$serverdata.ipaddress}
										</p>
										{/if}
										{if $serverdata.nameserver1 || $serverdata.nameserver2 || $serverdata.nameserver3 || $serverdata.nameserver4 || $serverdata.nameserver5}
										<hr/>
										<p>
											<strong>{$LANG.domainnameservers}</strong>
											{if $serverdata.nameserver1}
												<br/>
												{$serverdata.nameserver1} ({$serverdata.nameserver1ip})
											{/if}
											{if $serverdata.nameserver2}
												<br/>
												{$serverdata.nameserver2} ({$serverdata.nameserver2ip})
											{/if}
											{if $serverdata.nameserver3}
												<br/>
												{$serverdata.nameserver3} ({$serverdata.nameserver3ip})
											{/if}
											{if $serverdata.nameserver4}
												<br/>
												{$serverdata.nameserver4} ({$serverdata.nameserver4ip})
											{/if}
											{if $serverdata.nameserver5}
												<br/>
												{$serverdata.nameserver5} ({$serverdata.nameserver5ip})
											{/if}
										</p>
										{/if}
									{else}
										<p>
											<a href="http://www.{$domain}" target="_blank">www.{$domain}</a>
										</p>
										<p>
											<a href="http://{$domain}" class="btn btn-default btn-sm" target="_blank">{$LANG.visitwebsite}</a>
											{if $domainId}
												<a href="clientarea.php?action=domaindetails&id={$domainId}" class="btn btn-success btn-sm" target="_blank">{$LANG.managedomain}</a>
											{/if}
											<input type="button" onclick="popupWindow('whois.php?domain={$domain}','whois',650,420);return false;" value="{$LANG.whoisinfo}" class="btn btn-info btn-sm" />
										</p>
									{/if}
									{if $moduleclientarea AND $serverdata.type neq "smartermail"}
										<hr/>
										{$moduleclientarea}
									{/if}
								{elseif $moduleclientarea AND $serverdata.type neq "smartermail"}
									<hr/>
									{$moduleclientarea}
								{/if}
							</div>
						</div>
					</div>
					{if $serverdata.type eq "smartermail"}
							{if $moduleclientarea}
								{$moduleclientarea}
							{/if}
					{/if}
					{if $lastupdate}
						<div class="col-md-6">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h3 class="panel-title">{$LANG.resourceUsage}</h3>
								</div>
								<div class="panel-body text-center">
									<div class="col-sm-10 col-sm-offset-1">
										<div class="col-sm-6" id="diskSpace">
											<h4>{$LANG.diskSpace}</h4>
											<input type="text" value="{$diskpercent|substr:0:-1}" class="dial-usage" data-width="100" data-height="100" data-min="0" data-readOnly="true" />
											<p>{$diskusage}MB / {$disklimit}MB</p>
										</div>
										<div class="col-sm-6" id="bandwidth">
											<h4>{$LANG.bandwidth}</h4>
											<input type="text" value="{$bwpercent|substr:0:-1}" class="dial-usage" data-width="100" data-height="100" data-min="0" data-readOnly="true" />
											<p>{$bwusage}MB / {$bwlimit}MB</p>
										</div>
									</div>
																		<div class="col-sm-10 col-sm-offset-1">
										<div class="col-sm-6" id="diskSpace">
											<h4>{$LANG.diskSpace}</h4>
											<input type="text" value="{$diskpercent|substr:0:-1}" class="dial-usage" data-width="100" data-height="100" data-min="0" data-readOnly="true" />
											<p>{$diskusage}MB / {$disklimit}MB</p>
										</div>
										<div class="col-sm-6" id="bandwidth">
											<h4>Contas Exchange</h4>
											<input type="text" value="{$bwpercent|substr:0:-1}" class="dial-usage" data-width="100" data-height="100" data-min="0" data-readOnly="true" />
											<p>{$bwusage}MB / {$bwlimit}MB</p>
										</div>
									</div>
									<div class="clearfix">
									</div>
									<p class="text-muted">{$LANG.clientarealastupdated}: {$lastupdate}</p>
									<script src="{$BASE_PATH_JS}/jquery.knob.js"></script>
									<script type="text/javascript">
									jQuery(function() {ldelim}
										jQuery(".dial-usage").knob({ldelim}'format':function (v) {ldelim} alert(v); {rdelim}{rdelim});
									{rdelim});
									</script>
								</div>
							</div>
						</div>
					{/if}
					{if $configurableoptions}
						<div class="col-md-6">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h3 class="panel-title">{$LANG.orderconfigpackage}</h3>
								</div>
								<div class="panel-body">
									{foreach from=$configurableoptions item=configoption}
										<div class="row">
											<div class="col-sm-6 text-right">
												<strong>{$configoption.optionname}</strong>
											</div>
											<div class="col-sm-6 text-left">
												{if $configoption.optiontype eq 3}
													{if $configoption.selectedqty}
														{$LANG.yes}
													{else}
														{$LANG.no}
													{/if}
												{elseif $configoption.optiontype eq 4}
													{$configoption.selectedqty} x {$configoption.selectedoption}
												{else}
													{$configoption.selectedoption}
												{/if}
											</div>
										</div>
									{/foreach}
								</div>
							</div>
						</div>
					{/if}
					{if $customfields}
						<div class="col-md-6">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h3 class="panel-title">{$LANG.additionalInfo}</h3>
								</div>
								<div class="panel-body">
									{foreach from=$customfields item=field}
										<div class="row">
											<div class="col-sm-6 text-right">
												<strong>{$field.name}</strong>
											</div>
											<div class="col-sm-6 text-left">
												{$field.value}
											</div>
										</div>
									{/foreach}
								</div>
							</div>
						</div>
					{/if}
				</div>
				<div class="row">
					<div class="col-md-12">
						{foreach $hookOutput as $output}
							<div>
								{$output}
							</div>
						{/foreach}
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h3 class="panel-title">{$LANG.cPanel.billingOverview}</h3>
							</div>
							<div class="panel-body">
								<div class="row">
									<div class="col-md-5">
										{if $firstpaymentamount neq $recurringamount}
											<div class="row">
												<div class="col-xs-6 text-right">
													{$LANG.firstpaymentamount}
												</div>
												<div class="col-xs-6">
													{$firstpaymentamount}
												</div>
											</div>
										{/if}
										{if $billingcycle != $LANG.orderpaymenttermonetime && $billingcycle != $LANG.orderfree}
											<div class="row">
												<div class="col-xs-6 text-right">
													{$LANG.recurringamount}
												</div>
												<div class="col-xs-6">
													{$recurringamount}
												</div>
											</div>
										{/if}
										<div class="row" id="billingCycle">
											<div class="col-xs-6 text-right">
												{$LANG.orderbillingcycle}
											</div>
											<div class="col-xs-6">
												{$billingcycle}
											</div>
										</div>
										<div class="row" id="paymentMethod">
											<div class="col-xs-6 text-right">
												{$LANG.orderpaymentmethod}
											</div>
											<div class="col-xs-6">
												{$paymentmethod}
											</div>
										</div>
									</div>
									<div class="col-md-6">
										<div class="row" id="registrationDate">
											<div class="col-xs-6 col-md-5 text-right">
												{$LANG.clientareahostingregdate}
											</div>
											<div class="col-xs-6 col-md-7">
												{$regdate}
											</div>
										</div>
										<div class="row" id="nextDueDate">
											<div class="col-xs-6 col-md-5 text-right">
												{$LANG.clientareahostingnextduedate}
											</div>
											<div class="col-xs-6 col-md-7">
												{$nextduedate}
											</div>
										</div>
										{if $suspendreason}
											<div class="row">
												<div class="col-xs-6 col-md-5 text-right">
													{$LANG.suspendreason}
												</div>
												<div class="col-xs-6 col-md-7">
													{$suspendreason}
												</div>
											</div>
										{/if}
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		{/if}
	</div>
	<div class="tab-pane fade in" id="tabDownloads">
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title"><i class="fa fa-file"></i>&nbsp;{$LANG.downloadsfiles}</h3>
					</div>
					<div class="list-group">
						{if $downloads}
							{foreach from=$downloads item=download}
								<div class="list-group-item">
									<a href="{$download.link}">
										<i class="fa fa-file-o"></i>
										<strong>
											{$download.title}
										</strong>
									</a><br />
									{$download.description}
								</div>
							{/foreach}
						{else}
							<div class="list-group-item"><p class="text-center">{$LANG.downloadsnone}</p></div>
						{/if}
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="tab-pane fade in" id="tabAddons">
		{if $addonsavailable}
			{include file="$template/includes/alert.tpl" type="info" msg="{lang key="clientAreaProductAddonsAvailable"}" textcenter=true}
		{/if}
		<div class="row">
			{foreach from=$addons item=addon}
				<div class="col-md-6">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h3 class="panel-title"><i class="fa fa-plus"></i>&nbsp;{$addon.name}<span class="label status-{$addon.status|strtolower}">{$addon.status}</span></h3>
						</div>
						<div class="panel-body">
							<div class="row">
								<div class="col-xs-6 text-right">
									{$LANG.recurringamount}
								</div>
								<div class="col-xs-6">
									{$addon.pricing}
								</div>
							</div>
							<div class="row">
								<div class="col-xs-6 text-right">
									{$LANG.registered}
								</div>
								<div class="col-xs-6">
									{$addon.regdate}
								</div>
							</div>
							<div class="row">
								<div class="col-xs-6 text-right">
									{$LANG.clientareahostingnextduedate}
								</div>
								<div class="col-xs-6">
									{$addon.nextduedate}
								</div>
							</div>
						</div>
					</div>
				</div>
			{/foreach}
		</div>
	</div>
	<div class="tab-pane fade in" id="tabChangepw">
		{if $modulechangepwresult}
			{if $modulechangepwresult == "success"}
				{include file="$template/includes/alert.tpl" type="success" msg=$modulechangepasswordmessage textcenter=true}
			{elseif $modulechangepwresult == "error"}
				{include file="$template/includes/alert.tpl" type="error" msg=$modulechangepasswordmessage|strip_tags textcenter=true}
			{/if}
		{/if}
		<form class="using-password-strength" method="post" action="{$smarty.server.PHP_SELF}?action=productdetails#tabChangepw" role="form">
			<input type="hidden" name="id" value="{$id}" />
			<input type="hidden" name="modulechangepassword" value="true" />
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h3 class="panel-title">{$LANG.serverchangepassword}</h3>
						</div>
						<div class="panel-body">
							<div id="newPassword1" class="form-group has-feedback">
								<label for="inputNewPassword1" class="control-label">{$LANG.newpassword}</label>
								<input type="password" class="form-control" id="inputNewPassword1" name="newpw" autocomplete="off" />
								<span class="form-control-feedback glyphicon"></span>
								{include file="$template/includes/pwstrength.tpl"}
							</div>
							<div id="newPassword2" class="form-group has-feedback">
								<label for="inputNewPassword2" class="control-label">{$LANG.confirmnewpassword}</label>
								<input type="password" class="form-control" id="inputNewPassword2" name="confirmpw" autocomplete="off" />
								<span class="form-control-feedback glyphicon"></span>
								<div id="inputNewPassword2Msg">
								</div>
							</div>
						</div>
						<div class="panel-footer">
							<input class="btn btn-primary btn-3d res-100" type="submit" value="{$LANG.clientareasavechanges}" />
							<input class="btn btn-default pull-right res-left res-100" type="reset" value="{$LANG.cancel}" />
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
