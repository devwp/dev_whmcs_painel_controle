<?php
/**
 *
 * @Copyright © 2002-2016 Acronis International GmbH. All rights reserved
 * DEPENDENCIES: \Acronis\Helpers\RequestCleaner, \Acronis\System\ViewsConfig
 */

namespace Acronis\System;

use Acronis\Helpers\RequestCleaner;
use Acronis\Standard\Configs\ModuleConfig;

class Init
{
    private $params = array();
    private $viewsConfig = null;
    private $moduleConfig = null;

    public function __construct($params, ModuleConfig $module_configuration, ViewsConfig $views_configuration)
    {
        $this->params = $params;
        $this->viewsConfig = $views_configuration;
        $this->moduleConfig = $module_configuration;
    }

    public function run()
    {
        RequestCleaner::init('INPUT_REQUEST');

        $controller_file = RequestCleaner::getVar('c', '');
        $acceptable_pages = array_keys($this->viewsConfig->controllers);

        if (!in_array($controller_file, $acceptable_pages)) {
            $controller_file = $this->viewsConfig->defaultController;
        }

        $controller_name = $this->viewsConfig->controllersNamespace . '\\' . $controller_file;

        $controller = new $controller_name($this->params, $this->viewsConfig, $this->moduleConfig);

        $controller_methods = get_class_methods($controller);

        $action = 'action' . ucfirst($this->viewsConfig->defaultAction);
        $bannedMethods = $controller::$bannedMethods;
        if (RequestCleaner::varIsset('act')) {
            $actionValue = RequestCleaner::getVar('act');
            if (!in_array($actionValue[0], $bannedMethods)) {
                $action = 'action' . ucfirst($actionValue);
            }
        }

        if (!in_array($action, $controller_methods)) {
            $action = 'action' . ucfirst($this->viewsConfig->defaultAction);
        }

        $content = $controller->run($controller_file, $action);

        return $content;
    }


}