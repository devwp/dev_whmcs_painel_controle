<?php
/**
 *
 * @Copyright © 2002-2016 Acronis International GmbH. All rights reserved
 * DEPENDENCIES: \Acronis\Api\Requests, \Acronis\ApiManagers\SystemManager, \Acronis\Helpers\Arr, \Acronis\WHMCS\Lang
 */

namespace Acronis\ApiManagers;

use Acronis\Api\Requests;
use Acronis\Helpers\Arr;
use Acronis\HttpException;
use Acronis\Log;
use Acronis\VersionInfo;
use Acronis\WHMCS\Lang;

class GroupsManager
{
    const GROUP_EXISTS_ERROR_CODE = 409;
    const GROUP_INDEX_DELIMITIER = '/';

    const WARNING_INTERNAL_TAG_WAS_NOT_UPDATED = 'Failed to update the service provider group.';
    const WHMCS_NAME = 'whmcs-version';

    /**
     * @var Requests $api
     */
    private $api = FALSE;
    /**
     * @var SystemManager $system
     */
    private $system = FALSE;
    /**
     * @var Lang $lang
     */
    private $lang = FALSE;

    public function __construct(SystemManager $system_manager)
    {
        $this->system = $system_manager;
        $this->api = $this->system->api;
        $this->lang = $this->system->lang;
    }

    private static function in_multiarray($elem, $array)
    {
        $top = sizeof($array) - 1;
        $bottom = 0;
        while ($bottom <= $top) {
            if ($array[$bottom] == $elem)
                return true;
            else
                if (is_array($array[$bottom]))
                    if (in_multiarray($elem, ($array[$bottom])))
                        return true;

            $bottom++;
        }
        return false;
    }

    /**
     * Gets multiple params of the group by this group id of group details if supported. Throws error when any parameter is not found.
     * @param string $param_paths_and_default_values - array where keys are paths to parameters (character '.' shoud be used to got deeper) and values are default values for those parameters
     * @param mixed $group_details_or_group_id - group id or group details array if we do not know group id
     * @return array Array where keys are paths to parameters and values are their values
     */
    public function getGroupParamsForced($param_paths_and_default_values, $group_details_or_group_id)
    {
        return $this->getGroupParams($param_paths_and_default_values, $group_details_or_group_id, true);
    }

    /**
     * Gets multiple params of the group by this group id of group details if supported
     * @param string $param_paths_and_default_values - array where keys are paths to parameters (character '.' shoud be used to got deeper) and values are default values for those parameters
     * @param mixed $group_details_or_group_id - group id or group details array if we do not know group id
     * @param bool $forced - true to throw error if parameter any is not found
     * @return array Array where keys are paths to parameters and values are their values
     */
    public function getGroupParams($param_paths_and_default_values, $group_details_or_group_id, $forced = true)
    {
        $result = $param_paths_and_default_values;
        foreach ($param_paths_and_default_values as $param_path => $defaultval) {
            list($value, $found) = $this->getGroupParam($param_path, $group_details_or_group_id, $forced);

            $result[$param_path] = $found ? $value : $defaultval;
        }

        return $result;
    }

    /**
     * Gets param of the group by this group id of group details if supported
     * @param string $param_path - path to parameter (character '.' shoud be used to got deeper)
     * @param mixed $group_details_or_group_id - group id or group details array if we do not know group id
     * @param bool $forced - true to throw error if parameter is not found
     * @return array Array of two values: ($value,$found)
     */
    public function getGroupParam($param_path, $group_details_or_group_id, $forced = false)
    {
        $value = -10;
        $found = false;

        $group_details = $group_details_or_group_id;

        if (!is_array($group_details_or_group_id) && is_numeric($group_details_or_group_id)) {
            $group_details = $this->getGroupDetails($group_details_or_group_id);
        }

        $nullobject = new \Exception('null');
        $value = Arr::getByPath($group_details, $param_path, $nullobject);
        if ($value === $nullobject) {
            if (!$forced) {
                $value = null;
            } else {
                $this->api->makeOwnError($this->lang->get('cant_find_in_group', array($param_path)), false, true);
            }
        } else {
            $found = true;
        }

        return array($value, $found);
    }

    /**
     * Gets details for desired group
     * @param string|int $group_id - group id
     * @param bool $renew - if true, data would be gathered again even if is already stored in class memory - this is useful after group updates
     * @return array Array of group details
     */
    public function getGroupDetails($group_id, $renew = false)
    {
        $group_details = array();
        if (!$renew && $this->system->hasBeenQueriedFor($group_id, 'groups')) {
            if (isset($this->system->groups[$group_id])) {
                $group_details = $this->system->groups[$group_id];
            }
        } else {
            $response = $this->api->getGroup($group_id);
            $this->system->testFatalRequestError($this->lang->get('cant_get_group_details') . ' [$ecode]');

            if (empty($response)) {
                $this->api->makeOwnError($this->lang->get('cant_get_group_details'));
            }

            $this->system->groups[$group_id] = $group_details = $response;
            $this->system->markThatHasBeenQueriedFor($group_id, 'groups');
        }

        return $group_details;
    }

    /**
     * Creates nAApi    ew group (storage is evaluated automatically - it is better to do not support it)
     * @pararay $group_data - group data as documented in Api. Without storage - it will be determined automatically
     * @param $group_data
     * @param int $parent_group_id - id of parent group
     * @return array Array with two values: ($new_group_id,$new_group_version)
     */
    public function createGroup($group_data, $parent_group_id)
    {
        //determining parent group storage id if there is no data in $group_data:
        if (!isset($group_data['storage'])) {
            $group_data['storage'] = $this->determineGroupStorageId($parent_group_id);
        }

        do {
            if (isset($response['error_code'])) {
                $group_data['name'] = $this->incrementGroupName($group_data['name']);
            }
            $response = $this->api->addGroup($group_data, $parent_group_id);
        } while ($response['error_code'] == self::GROUP_EXISTS_ERROR_CODE);

        $this->system->testFatalRequestError($this->lang->get('cant_add_group') . ' [$ecode]');

        if (!isset($response['id']) || !isset($response['version'])) {
            $this->api->makeOwnError($this->lang->get('cant_get_id_of_created_group'), true);
        }

        $new_group_id = $response['id'];
        $new_group_version = $response['version'];

        return array($new_group_id, $new_group_version);
    }

    /**
     * Evaluates group storage id for desired parent group
     * @param int $parent_group_id - id of parent group
     * @return int Group storage id
     */
    public function determineGroupStorageId($parent_group_id)
    {
        $parent_group_storage_id = -10;

        //getting admin group details & storage id of admin group if $this->$server_admin_group_id == $parent_group_id:
        $admin_subgroups = array();
        if ($this->system->serverAdminGroupId == $parent_group_id) {
            list($parent_group_storage_id, $found) = $this->getGroupParam('storage.id', $this->system->serverAdminGroupId);

            if (!$found) {
                $parent_group_storage_id = null;
            }
        } else {        //otherwise: checking if admin has subgroup of id=parent reseller and getting storage id from it:
            $admin_subgroups = $this->getSubgroups($this->system->serverAdminGroupId);

            list($parent_group_storage_id, $found_trash) = $this->findGroupStorageIdInSubgroups($parent_group_id, $admin_subgroups);
        }

        return $parent_group_storage_id;
    }

    /**
     * Gets subgroups of the group (there are less details than in group details)
     * @param int $group_id
     * @return array Found subgroups
     */
    public function getSubgroups($group_id)
    {
        $groups = array();

        if ($this->system->hasBeenQueriedFor($group_id, 'subgroups')) {
            if (isset($this->system->groupsSubgroups[$group_id])) {
                $groups = $this->system->groupsSubgroups[$group_id];
            }
        } else {
            $admin_subgroups = $this->api->getSubgroupsGroup($group_id);
            $this->system->testFatalRequestError($this->lang->get('cant_get_subgroups_for_group_id', array($group_id)) . ' [$ecode]');

            if (!isset($admin_subgroups['items'])) {
                $this->api->makeOwnError($this->lang->get('cant_get_subgroups_for_group_id', array($group_id)));
            }

            $this->system->groupsSubgroups[$group_id] = $groups = $admin_subgroups['items'];
            $this->system->markThatHasBeenQueriedFor($group_id, 'subgroups');
        }
        return $groups;
    }

    /**
     * Finds group storage id in subgroups details (there are less details than in group details)
     * @param int $subgroup_id - id of group
     * @param array $subgroups - array of subgroups details to search in
     * @param bool $forced - if true it will throw error when not found
     * @return array Array of two values: ($subgroup_storage_id,$found) - $found is true if storage id is found (if not forced than this is not causing errors)
     */
    public function findGroupStorageIdInSubgroups($subgroup_id, $subgroups, $forced = false)
    {
        $found = false;
        $subgroup_storage_id = -10;
        foreach ($subgroups as $group) {
            if (!$found && isset($group['id']) && $group['id'] == $subgroup_id) {
                $found = true;
                if (isset($group['storage']) && isset($group['storage']['id'])) {
                    $subgroup_storage_id = $group['storage']['id'];
                }
            }
        }

        if (!$found || $subgroup_storage_id == -10) {
            if (!$forced) {
                $subgroup_storage_id = null;
            } else {
                $this->api->makeOwnError($this->lang->get('cant_find_storage_id_of_subgroup'), false, true);
            }
        }
        return array($subgroup_storage_id, $found);
    }

    /**
     * increments group name if group with same name already exists
     *
     * @param string $groupName source name
     * @return string modified name
     */
    public function incrementGroupName($groupName)
    {
        $chunks = explode(self::GROUP_INDEX_DELIMITIER, $groupName);
        $chunksNum = count($chunks);
        //if it is first copy - add "/2" to group name and return
        if ($chunksNum == 1) {
            return $groupName . ' ' . self::GROUP_INDEX_DELIMITIER . '1';
        }
        //if it is notfirst copy - increment /N index and return new group name
        $chunks[$chunksNum - 1] += 1;
        return implode(self::GROUP_INDEX_DELIMITIER, $chunks);
    }

    /**
     * Deletes group (deactivates it first if needed)
     * @param int $group_id - id of group to delete
     * @param mixed $group_version_or_group_details - optional. If null then it will be taken from api or class cache data
     * @param string $username - admin username
     * @param string $password - admin password
     * @return boolean TRUE
     */
    public function deleteGroup($group_id, $group_version_or_group_details, $username, $password)
    {
        $group_version = $group_version_or_group_details;

        //getting group version if not provided:
        if ((is_array($group_version_or_group_details) || $group_version_or_group_details === null)) {
            $param2 = is_array($group_version_or_group_details) ? $group_version_or_group_details : $group_id;
            list($group_version, $found_trash) = $this->getGroupParamForced('version', $param2);
        }

        //deactivating group:
        $group_version = $this->deactivateGroup($group_id, $group_version, $username, $password);

        //deleting group:
        $this->api->deleteGroup($group_id, $group_version);
        $this->system->testFatalRequestError($this->lang->get('cant_delete_group') . ' [$ecode]');

        return true;
    }

    /**
     * Gets param of the group by this group id of group details if supported. Throws error if parameter is not found
     * @param string $param_path - path to parameter (character '.' shoud be used to got deeper)
     * @param mixed $group_details_or_group_id - group id or group details array if we do not know group id
     * @return array Array of two values: ($value,$found)
     */
    public function getGroupParamForced($param_path, $group_details_or_group_id)
    {
        return $this->getGroupParam($param_path, $group_details_or_group_id, true);
    }

    /**
     * Deactivates group
     * @param int $group_id - id of group to delete
     * @param mixed $group_version_or_group_details - optional. If null then it will be taken from api or class cache data
     * @param string $username - admin username
     * @param string $password - admin password
     * @return int Updated group version
     */
    public function deactivateGroup($group_id, $group_version_or_group_details, $username, $password)
    {
        list($enabled_trash, $disabled, $inactive_trash) = $this->system->logicConstantsManager->getStatuses();

        $updated_group_version = $this->updateGroupStatus($group_id, $disabled, $username, $password);

        return $updated_group_version;
    }

    /**
     * Updates group status
     * @param int $group_id - id of group to delete
     * @param $new_status - new group status (gathered by getStatuses method)
     * @param string $username - admin username
     * @param string $password - admin password
     * @return int Updated group version
     */
    public function updateGroupStatus($group_id, $new_status, $username, $password)
    {
        $this->updateGroup($group_id, array('status' => $new_status));

        //relogging to system (needed to complete status change):
        $this->system->accountsManager->login($username, $password);

        //getting new group version (when activating/deactivating the one returned by updateGroup can be inaccurate):
        //it have to be from next api call then (true in get_group_details last parameter):
        $group_details_new = $this->getGroupDetails($group_id, true);
        list($updated_group_version, $found_trash) = $this->getGroupParamForced('version', $group_details_new);

        return $updated_group_version;
    }

    /**
     * Updates group
     * @param int $group_id - id of group to delete
     * @param array $update_data - group data (See Api documentation)
     * @return int Updated group version
     */
    public function updateGroup($group_id, $update_data)
    {
        $groupDetails = $this->getGroupDetails($group_id);
        $update_data = $this->checkNewGroupName($groupDetails, $update_data);

        $response = $this->api->updateGroup($update_data, $group_id, $groupDetails['version']);

        $this->system->testFatalRequestError($this->lang->get('cant_update_group') . ' [$ecode]');

        if (!isset($response['version'])) {
            $this->api->makeOwnError($this->lang->get('cant_get_version_of_updated_group'), true);
        }

        return $response['version'];
    }

    /**
     * @param $groupDetails
     * @param $updateData
     * @return mixed
     */
    private function checkNewGroupName($groupDetails, $updateData)
    {
        if (isset($updateData['name']) || isset($updateData['contact']['firstname']) || isset($updateData['contact']['lastname'])) {
            if (preg_match("/\/[0-9]+/", $groupDetails['name'], $groupName) == false) $groupName = null;
            if ($updateData['name'] == '') {
                $updateData['name'] = $updateData['contact']['firstname'] . ' ' . $updateData['contact']['lastname'];
            }
            if ($groupName != null) $updateData['name'] = $updateData['name'] . ' ' . $groupName[0];
        }
        return $updateData;
    }

    /**
     * Activates group
     * @param int $group_id - id of group to delete
     * @param mixed $group_version_or_group_details - optional. If null then it will be taken from api or class cache data
     * @param string $username - admin username
     * @param string $password - admin password
     * @return int Updated group version
     */
    public function activateGroup($group_id, $group_version_or_group_details, $username, $password)
    {
        list($enabled, $disabled_trash, $inactive_trash) = $this->system->logicConstantsManager->getStatuses();

        $updated_group_version = $this->updateGroupStatus($group_id, $enabled, $username, $password);

        return $updated_group_version;
    }

    /**
     * Gets final system subgroups (EndUserCompany and CompanyDepartment) ids for parent group configuration
     * @param mixed $parent_group_id_or_parent_subgroups - parent group id or subgroups array if we do not know parent group id
     * @return array Array with three values: ($groups_details,$end_user_company_group_kind,$company_department_group_kind) - second and third will be -10 if cannot be evaluated
     */
    public function getGroupFinalSubgroups($parent_group_id_or_parent_subgroups)
    {
        list($groups_details, $group_kinds) = $this->findGroupsDetailsInSubgroupsByKind($parent_group_id_or_parent_subgroups, array('EndUserCompany' => 0, 'CompanyDepartment' => 0));

        $end_user_company_group_kind = isset($group_kinds['EndUserCompany']) ? $group_kinds['EndUserCompany'] : -10;
        $company_department_group_kind = isset($group_kinds['CompanyDepartment']) ? $group_kinds['CompanyDepartment'] : -10;

        return array($groups_details, $end_user_company_group_kind, $company_department_group_kind);
    }

    /**
     * Locks group
     * @param mixed $group_id_or_group_details - group id or group details if id is unknown
     * @return boolean TRUE
     */
    public function lockGroup($group_id_or_group_details)
    {
        $group_id = $group_id_or_group_details;

        if (is_array($group_id_or_group_details)) {
            list($group_id, $found_trash) = $this->getGroupParamForced('id', $group_id_or_group_details);
        }

        $this->api->lockGroup($group_id);
        $this->system->testFatalRequestError($this->lang->get('cant_lock_group') . ' [$ecode]');

        return true;
    }

    /**
     * Unlocks group
     * @param mixed $group_id_or_group_details - group id or group details if id is unknown
     * @return boolean TRUE
     */
    public function unlockGroup($group_id_or_group_details)
    {
        $group_id = $group_id_or_group_details;

        if (is_array($group_id_or_group_details)) {
            list($group_id, $found_trash) = $this->getGroupParamForced('id', $group_id_or_group_details);
        }

        $this->api->unlockGroup($group_id);
        $this->system->testFatalRequestError($this->lang->get('cant_unlock_group') . ' [$ecode]');

        return true;
    }

    /**
     * Gets users details for desired group
     * @param mixed $group_id_or_group_details - group id or group details if id is unknown
     * @return array Array with users details
     */
    public function getGroupUsersDetails($group_id_or_group_details)
    {
        $users_details = array();

        $group_id = $group_id_or_group_details;

        if (is_array($group_id_or_group_details)) {
            list($group_id, $found_trash) = $this->getGroupParamForced('id', $group_id_or_group_details);
        }

        if ($this->system->hasBeenQueriedFor($group_id, 'users')) {
            if (isset($this->system->groupsUsers[$group_id])) {
                $users_details = $this->system->groupsUsers[$group_id];
            }
        } else {
            $response = $this->api->getGroupUsers($group_id);
            $this->system->testFatalRequestError($this->lang->get('cant_get_group_users_details', array($group_id)) . ' [$ecode]');

            if (empty($response) || !isset($response['items'])) {
                return array();
                $this->api->makeOwnError($this->lang->get('cant_get_group_users_details', array($group_id)));
            } else {
                $this->system->groupsUsers[$group_id] = $users_details = $response['items'];
                $this->system->markThatHasBeenQueriedFor($group_id, 'users');
            }
        }

        return $users_details;
    }

    /**
     * Searches for admin login in admin details for desired group
     * @param mixed $admins_details_data_or_group_id - array of admins details to search or group id if is supported
     * @param string $admin_login - admin login
     * @return array Array with two values: ($admin_details,$admins_details)
     */
    public function findAdminDetailsInGroupAdminsDetailsByLogin($admins_details_data_or_group_id, $admin_login)
    {
        $admin_details = array();

        $admins_details = $admins_details_data_or_group_id;

        if (!is_array($admins_details_data_or_group_id) && is_numeric($admins_details_data_or_group_id)) {
            $admins_details = $this->getGroupAdminsDetails($admins_details_data_or_group_id);
        }

        $found = false;
        foreach ($admins_details as $admin) {
            if (!$found && isset($admin['login']) && $admin['login'] == $admin_login) {
                $found = true;
                $admin_details = $admin;
                break;
            }
        }

        if (!$found || empty($admin_details)) {
            $this->api->makeOwnError($this->lang->get('cant_find_admin_login_in_group', array($admin_login)), false, true);
        }

        return array($admin_details, $admins_details);
    }

    /**
     * Gets admins details for desired group
     * @param mixed $group_id_or_group_details - group id or group details if id is unknown
     * @return array Array with admins details
     */
    public function getGroupAdminsDetails($group_id_or_group_details)
    {
        $admins_details = array();

        $group_id = $group_id_or_group_details;

        if (is_array($group_id_or_group_details)) {
            list($group_id, $found_trash) = $this->getGroupParamForced('id', $group_id_or_group_details);
        }

        if ($this->system->hasBeenQueriedFor($group_id, 'admins')) {
            if (isset($this->system->groupsAdmins[$group_id])) {
                $admins_details = $this->system->groupsAdmins[$group_id];
            }
        } else {

            $response = $this->api->getGroupAdmins($group_id);
            $this->system->testFatalRequestError($this->lang->get('cant_get_group_admins_details', array($group_id)) . ' [$ecode]');

            if (empty($response) || !isset($response['items'])) {
                $this->api->makeOwnError($this->lang->get('cant_get_group_admins_details', array($group_id)));
            }

            $this->system->groupsAdmins[$group_id] = $admins_details = $response['items'];
            $this->system->markThatHasBeenQueriedFor($group_id, 'admins');

            //adding this admins also to admins array (we can, because the data is almost the same - everything we needed is provided):
            foreach ($admins_details as $admindet) {
                if (isset($admindet['id'])) {
                    $this->system->admins[$admindet['id']] = $admindet;
                }
            }
        }

        return $admins_details;
    }

    /**
     * Gets group id and admin id by admin name
     * @param string $groupname - name of the group
     * @return int Group id
     */
    public function getGroupIdByGroupName($groupname)
    {
        $needed_values = array(
            'kind' => null
        );

        list($result, $found_trash, $exactly) = $this->system->searchForced($groupname, null, $needed_values);

        if (!$exactly || !isset($result['parent_id'])) {
            $this->api->makeOwnError($this->lang->get('cant_find_group_name_id', array($groupname)), false, true);
        }

        $group_id = $result['parent_id'];

        return $group_id;
    }

    /**
     * Returns link to backup or management console
     *
     * @param int $userId
     * @param string $resource 'admins' or 'users'
     * @return string
     */
    public function getLinkToAcronisPanel($userId, $resource = 'admins')
    {
        $token = $this->api->getImpersonate($resource, $userId);
        return $this->api->baseUrl . '/?jwt=' . $token['jwt'];
    }

    /**
     * Check values of internal tag in root group.
     */
    public function updateInternalTag()
    {
        $groupDetails = $this->getGroupDetails('self');

        $versionInfo = VersionInfo::getInstance();
        $whmcsVersion = $versionInfo->getWhmcsVersion();
        $packageVersion = $versionInfo->getPackageVersion();

        $internalTag = Arr::decode($groupDetails['internal_tag']);

        if (isset($internalTag[PACKAGE_USER_AGENT]) && isset($internalTag[self::WHMCS_NAME])) {
            if ($internalTag[PACKAGE_USER_AGENT] == $packageVersion && $internalTag[self::WHMCS_NAME] == $whmcsVersion) {
                return;
            }
        }

        $internalTag[PACKAGE_USER_AGENT] = $packageVersion;
        $internalTag[self::WHMCS_NAME] = $whmcsVersion;

        try {
            $this->updateGroup($groupDetails['id'], array('internal_tag' => Arr::encode($internalTag)));
        } catch (HttpException $e) {
            if ($e->getCode() != 403) {
                throw $e;
            }
            Log::getInstance()->warning(self::WARNING_INTERNAL_TAG_WAS_NOT_UPDATED);
        }
    }
}