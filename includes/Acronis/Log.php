<?php
namespace Acronis;

use Monolog\Formatter\LineFormatter;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;

/**
 * @Copyright © 2002-2015 Acronis International GmbH. All rights reserved
 */
class Log
{
    const RECORD_FORMAT_SYSLOG = '%context.request_id% %level_name% %message% File: %context.file% Line: %context.line%';
    const RECORD_FORMAT_DEFAULT = '%datetime% %context.request_id% %level_name% %message% File: %context.file% Line: %context.line%';

    // log fields
    const FIELD_REQUEST = '%context.request_id%';
    const FIELD_TIME = '%datetime%';
    const FIELD_MESSAGE = '%message%';
    const FIELD_TYPE = '%level_name%';
    const FIELD_FILE = '%context.file%';
    const FIELD_LINE = '%context.line%';

    const SENSITIVE_DATA_REPLACEMENT = '*****';

    const LOGGER_NAME = 'acronis-backup-cloud';
    const PREFIX_LOG_FILE = 'Events';

    /** @var self */
    private static $_instance;
    /** @var string */
    private static $_requestId;

    /**
     * @var Logger
     */
    private $logger;

    /**
     * Log constructor.
     */
    private function __construct()
    {
        $this->generateId();
    }

    /**
     * Generates unique ID.
     */
    private function generateId()
    {
        self::$_requestId = uniqid();
    }

    /**
     * @return Log
     */
    public static function getInstance()
    {
        if (empty(self::$_instance)) {
            self::$_instance = new Log();
        }
        return self::$_instance;
    }

    /**
     * Creates instance of logger.
     * @param $config
     * @return Logger
     * @throws \Exception
     */
    public static function createLogger($config)
    {
        $logger = new Logger(ACRONIS_SERVICE_NAME);

        if ($config->writeTextLog) {
            if (is_writable(dirname($config->pathToLog))) {
                $handler = new StreamHandler($config->pathToLog);
                $handler->setFormatter(new LineFormatter());
                $logger->pushHandler($handler);
            } else {
                error_log('Missing log directory: ' . $config->pathToLog, 0);
            }
        }

        return $logger;
    }

    public function resetLogger()
    {
        $this->logger = null;
    }

    /**
     * @return Logger
     */
    public function getLogger()
    {
        return $this->logger;
    }

    /**
     * @param \Psr\Log\LoggerInterface $logger
     */
    public function setLogger(\Psr\Log\LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * System is unusable.
     *
     * @param string $message
     * @throws \Exception
     */
    public function emergency($message)
    {
        $caller = debug_backtrace();
        $zeroFrame = $caller[0];
        $this->createRecord(
            call_user_func_array(array('Acronis\Str', 'format'), func_get_args()),
            Logger::EMERGENCY,
            $zeroFrame['file'],
            $zeroFrame['line']
        );
    }

    /**
     * @param string $message
     * @param string $type
     * @param string $file
     * @param int $line
     * @throws \Exception
     */
    private function createRecord($message, $type, $file, $line)
    {
        if (!isset($this->logger)) {
            return;
        }

        $context = array(
            'request_id' => self::$_requestId,
            'file' => $file,
            'line' => $line
        );

        $this->logger->addRecord($type, $message, $context);
    }

    /**
     * Action must be taken immediately.
     *
     * Example: Entire website down, database unavailable, etc. This should
     * trigger the SMS alerts and wake you up.
     *
     * @param string $message
     * @throws \Exception
     */
    public function alert($message)
    {
        $caller = debug_backtrace();
        $zeroFrame = $caller[0];
        $this->createRecord(
            call_user_func_array(array('Acronis\Str', 'format'), func_get_args()),
            Logger::ALERT,
            $zeroFrame['file'],
            $zeroFrame['line']
        );
    }

    /**
     * Critical conditions.
     *
     * Example: Application component unavailable, unexpected exception.
     *
     * @param string $message
     * @throws \Exception
     */
    public function critical($message)
    {
        $caller = debug_backtrace();
        $zeroFrame = $caller[0];
        $this->createRecord(
            call_user_func_array(array('Acronis\Str', 'format'), func_get_args()),
            Logger::CRITICAL,
            $zeroFrame['file'],
            $zeroFrame['line']
        );
    }

    /**
     * Normal but significant events.
     *
     * @param string $message
     * @throws \Exception
     */
    public function notice($message)
    {
        $caller = debug_backtrace();
        $zeroFrame = $caller[0];
        $this->createRecord(
            call_user_func_array(array('Acronis\Str', 'format'), func_get_args()),
            Logger::NOTICE,
            $zeroFrame['file'],
            $zeroFrame['line']
        );
    }

    /**
     * Interesting events.
     *
     * Example: User logs in, SQL logs.
     *
     * @param string $message
     * @throws \Exception
     */
    public function info($message)
    {
        $caller = debug_backtrace();
        $zeroFrame = $caller[0];
        $this->createRecord(
            call_user_func_array(array('Acronis\Str', 'format'), func_get_args()),
            Logger::INFO,
            $zeroFrame['file'],
            $zeroFrame['line']
        );
    }

    /**
     * Detailed debug information.
     *
     * @param string $message
     * @throws \Exception
     */
    public function debug($message)
    {
        $caller = debug_backtrace();
        $zeroFrame = $caller[0];
        $this->createRecord(
            call_user_func_array(array('Acronis\Str', 'format'), func_get_args()),
            Logger::DEBUG,
            $zeroFrame['file'],
            $zeroFrame['line']
        );
    }

    /**
     * @param $message
     * @throws \Exception
     */
    public function event($message)
    {
        $caller = debug_backtrace();
        $zeroFrame = $caller[0];
        $this->createRecord(
            call_user_func_array(array('Acronis\Str', 'format'), func_get_args()),
            Logger::INFO,
            $zeroFrame['file'],
            $zeroFrame['line']
        );
    }

    /**
     * @param string $message
     * @throws \Exception
     */
    public function warning($message)
    {
        $caller = debug_backtrace();
        $zeroFrame = $caller[0];
        $this->createRecord(
            call_user_func_array(array('Acronis\Str', 'format'), func_get_args()),
            Logger::WARNING,
            $zeroFrame['file'],
            $zeroFrame['line']
        );
    }

    /**
     * @param string $message
     * @throws \Exception
     */
    public function error($message)
    {
        $caller = debug_backtrace();
        $zeroFrame = $caller[0];
        $this->createRecord(
            call_user_func_array(array('Acronis\Str', 'format'), func_get_args()),
            Logger::ERROR,
            $zeroFrame['file'],
            $zeroFrame['line']
        );
    }
}