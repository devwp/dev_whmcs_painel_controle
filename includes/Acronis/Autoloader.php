<?php
/**
 *
 * @version 1.0.4
 * @Copyright © 2002-2016 Acronis International GmbH. All rights reserved
 */

namespace Acronis;

class Autoloader
{
    private $directory = '';
    private $includesDirectory = '';
    private $availableClasses = '';
    private $classPrefix = '';

    /**
     *
     * @param string $directory - autoloader base directory
     * @param string $class_namespace_prefix - autoloader vendor namespace
     * @param string $includes_directory - autoloader secondary directory
     */
    public function __construct($directory = '', $class_namespace_prefix = '', $includes_directory = '')
    {

        if (trim($directory) == '') {
            $this->directory = dirname(__FILE__) . DS;
        } else {
            $this->directory = $directory;
        }

        if (trim($includes_directory) != '') {
            $this->includesDirectory = $includes_directory;
        }

        if ($class_namespace_prefix != '') {
            $this->classPrefix = $class_namespace_prefix;
        }

        $this->availableClasses = array();

        if (!file_exists($this->directory) || !file_exists($this->includesDirectory)) {
            die ('<span style="color:white; background-color:red;">Invalid autoloader base or secondary directory! Please contact with service (namespace=' . $class_namespace_prefix . ').</span>');
        }

        $di = new \RecursiveDirectoryIterator($this->directory);

        foreach (new \RecursiveIteratorIterator($di) as $filename => $file) {
            $this->availableClasses[$filename] = $filename;
        }

        spl_autoload_register(array($this, 'autoload'));

        return TRUE;

    }

    /**
     * Unregisters autoloader
     * @return boolean
     */
    public function unregister()
    {

        spl_autoload_unregister(array($this, 'autoload'));

        return TRUE;

    }


    public function autoload($class_name, $debug = false)
    {
        $len = strlen($this->classPrefix);
        if (strncmp($this->classPrefix, $class_name, $len) !== 0) {
            return;
        }

        $class_path = substr($class_name, $len);

        $class_path = ltrim($class_path, '\\');

        if (trim($class_path) == '') {
            return;
        }

        $file_name = $this->directory . str_replace('\\', DS, $class_path) . '.php';

        if ($debug) {
            return '[[' . $file_name . ']]<hr>' . print_r($this->availableClasses, true);
        }

        if (isset($this->availableClasses[$file_name])) {
            require_once($file_name);
        } elseif ($this->includesDirectory != '') {
            $file_name = $this->includesDirectory . str_replace('\\', DS, $class_path) . '.php';
            require_once($file_name);
        }

    }

}

