<?php
/**
 * @Copyright © 2002-2016 Acronis International GmbH. All rights reserved
 */
namespace Acronis;

use Illuminate\Database\Capsule\Manager as Capsule;

class VersionInfo
{
    // separator from key to value
    const PARAM_SEPARATOR = "=";

    // keys of each string in Vers.ion file
    const MAJOR_NUMBER_KEY = "MAJOR_VERSION";
    const MINOR_VERSION_KEY = "MINOR_VERSION";
    const BUILD_NUMBER_KEY = "BUILD_NUMBER";

    // delimiter between parts of version
    const DELIMITER = '.';

    private static $instance;
    private $packageVersion;
    private $whmcsVersion;


    /**
     * VersionInfo constructor.
     */
    protected function __construct()
    {
    }

    /**
     * @return mixed
     */
    public static function getInstance()
    {
        if (null === static::$instance) {
            static::$instance = new static();
        }

        return static::$instance;
    }

    /**
     * Method returns current version of plugin.
     * @return string
     */
    public function getPackageVersion()
    {
        if (!$this->packageVersion) {
            $data = array();
            $versionData = file(VERSION_FILE_PATH);
            foreach ($versionData as $line) {
                $line = trim($line);

                if (empty($line)) {
                    continue;
                }
                list($infoKey, $infoValue) = explode(self::PARAM_SEPARATOR, $line);
                $data[trim($infoKey)] = trim($infoValue);
            }
            $this->packageVersion = implode(self::DELIMITER, $data);
        }

        return $this->packageVersion;
    }

    /**
     * Method returns current version of WHMCS.
     */
    public function getWhmcsVersion()
    {
        if (!$this->whmcsVersion) {
            $db = Capsule::connection()->getPdo();
            $result = $db->query('SELECT `value` FROM `tblconfiguration` WHERE `setting` = "Version"')->fetch();
            $this->whmcsVersion = $result['value'];
        }
        return $this->whmcsVersion;
    }

    /**
     * Empty magic method for singleton pattern realization.
     */
    private function __clone()
    {
    }

    /**
     * Empty magic method for singleton pattern realization.
     */
    private function __wakeup()
    {
    }
}
