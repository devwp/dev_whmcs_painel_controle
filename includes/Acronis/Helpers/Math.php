<?php
/**
 *
 * @version 1.0.0.1
 * @Copyright © 2002-2016 Acronis International GmbH. All rights reserved
 */
namespace Acronis\Helpers;

use Acronis\WHMCS\Lang;

class Math
{

    /**
     * Gets friendly format of value. Approperiate string will be returned if bytes representation is needed or this value is unlimited or unknown
     * @param mixed $value - value to be formated
     * @param int $precision - rounding precision when value is in bytes (number of digits after dot) [optional; default=2]
     * @param bool $unlimited - true if this is unlimited ("unlimited" string would be returned in that case) [optional; default=false]
     * @param bool $unknown - true if the value is unknown ("wrong_data" string would be returned in that case) [optional; default=false]
     * @param bool $bytes - true to treat value as bytes (getBytesFriendlyFormat method would be used) [optional; default=false]
     * @param string $spaces - string that will replace spaces in result. [optional; default= ' ']
     * @return string
     */
    public static function getValueFriendlyFormat($value, $precision = 2, $unlimited = false, $unknown = false, $bytes = false, $spaces = ' ')
    {
        if ($unlimited) {
            return 'unlimited';
        } else {
            if ($bytes) {
                return self::getBytesFriendlyFormat($value, $precision, $spaces . '?');
            } else {
                return $value;
            }
        }
    }

    /**
     * Calculates and returns bytes in friendly formated string
     * @param int $bytes - size in bytes
     * @param int $precision - rounding precision [optional; default=2]
     * @param string $unitformat - format of the added unit. Use '?' in place where unit have to be. (eg. ' - ?' will result in "10 - GiB") [optional; default=' ?']
     * @return string
     */
    public static function getBytesFriendlyFormat($bytes, $precision = 2, $unitformat = ' ?')
    {
        $units = array('B', 'KiB', 'MiB', 'GiB', 'TiB', 'PiB', 'EiB', 'ZiB', 'YiB');

        $lang = new Lang(ACRONIS_INCLUDES_DIR . 'Lang' . DS);

        $bytes_b = max($bytes, 0);
        $pow = floor(($bytes_b != 0 ? log($bytes_b) : 0) / log(1024));
        //get position Unit for $units
        $powx = min($pow, count($units) - 1);

        $bytes_b /= (1 << (10 * $powx));

        //get value from lang file
        $unitValue = $lang->get('measure.' . $units[$powx]);

        $formatted = round($bytes_b, $precision) . str_replace('?', $unitValue, $unitformat);
        return $formatted;
    }


}
