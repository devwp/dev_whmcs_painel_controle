<?php
/**
 *
 * @version 1.0.2
 * @Copyright © 2002-2016 Acronis International GmbH. All rights reserved
 */
namespace Acronis\Helpers;

class Arr
{
    const VALUE_DELIMITER = '=';
    const PAIR_DELIMITER = ';';

    public static function get($array, $name, $default = '')
    {
        return isset($array[$name]) ? $array[$name] : $default;
    }

    public static function aoaSort($array, $key, $desc = false)
    {
        if (!$desc) {
            uasort($array, function ($a, $b) use ($key) {
                return ($a[$key] == $b[$key]) ? 0 : (($a[$key] < $b[$key]) ? -1 : 1);
            });
        } else {
            uasort($array, function ($a, $b) use ($key) {
                return ($a[$key] == $b[$key]) ? 0 : (($a[$key] > $b[$key]) ? -1 : 1);
            });
        }
        return $array;
    }

    public static function getByPath($array, $path, $default = '')
    {
        if (!is_array($array)) {
            return $default;
        }

        $exploded_path = explode(".", $path);

        $temp = $array;
        foreach ($exploded_path as $key) {
            if (isset($temp[$key])) {
                $temp = $temp[$key];
            } else {
                return $default;
            }
        }

        return $temp;
    }

    public static function setByPath($array, $path, $value)
    {
        if (!is_array($array)) {
            return $array;
        }

        $exploded_path = explode(".", $path);

        $temp = &$array;
        foreach ($exploded_path as $key) {
            if (isset($temp[$key])) {
                $temp = &$temp[$key];
            } else {
                $temp[$key] = array();
                $temp = &$temp[$key];
            }
        }
        $temp = $value;

        return $array;
    }

    /**
     * @param array $values
     * @return string
     */
    public static function encode(array $values)
    {
        $pairs = array();
        foreach ($values as $k => $v) {
            $pairs[] = is_null($v) ? $k : $k . self::VALUE_DELIMITER . $v;
        }

        return implode(self::PAIR_DELIMITER, $pairs);
    }

    /**
     * @param string $string
     * @return array
     */
    public static function decode($string)
    {
        if (!is_string($string) || $string === '') {
            return array();
        }

        $pairs = explode(self::PAIR_DELIMITER, $string);

        $values = array();
        foreach ($pairs as $pair) {
            $parts = explode(self::VALUE_DELIMITER, $pair, 2);
            $values[$parts[0]] = count($parts) > 1 ? $parts[1] : null;
        }
        return $values;
    }
}


