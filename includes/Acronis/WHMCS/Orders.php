<?php
/**
 *
 * @version 1.0.0
 * @Copyright © 2002-2016 Acronis International GmbH. All rights reserved
 */
namespace Acronis\WHMCS;

use Acronis\Log;
use Illuminate\Database\Capsule\Manager as Capsule;

class Orders
{
    /**
     * key name for set service name in session product data
     */
    const SERVICE_NAME_KEY = 'serviceName';

    /**
     * Get order by userId and orderId from DB
     *
     * @param int $userId
     * @param int $orderId
     * @param int $productId
     * @return array order info
     */
    public static function getLastOrderByUser($userId, $orderId, $productId)
    {
        $db = Capsule::connection()->getPdo();

        $sth = $db->prepare('SELECT * FROM `tblhosting` WHERE `userid` = ? AND orderid = ? AND `packageid` = ?');
        $sth->execute(array($userId, $orderId, $productId));

        return $sth->fetchAll();
    }

    /**
     * return array with username and password for admin from session
     * @param array $sessionData for example array('username','password')
     * @param $userId
     * @return array|bool
     */
    public static function getCredentialsFromSession($sessionData, $userId)
    {
        //check session data
        if (!is_array($sessionData) || !isset($sessionData['uid'])
            || !isset($sessionData['products'])
            || $sessionData['uid'] != $userId
        ) {
            return false;
        }

        //find our product in cart and get credentials data from them
        $result = array();
        foreach ($sessionData['products'] as $product) {
            //get our product from session data
            if ($product[self::SERVICE_NAME_KEY] == ACRONIS_SERVICE_NAME
                && isset($product['username'])
                && isset($product['password'])
            ) {
                $result['username'] = $product['username'];
                $result['password'] = $product['password'];
                return $result;
            }
        }
        return false;
    }

    /**
     * Update new data for admin's username and password in services table (tblhosting)
     *
     * @param string $username
     * @param string $password
     * @param int $serviceId id of service (primary key in tblhosting)
     */
    public static function updateServiceAdministrator($username, $password, $serviceId)
    {
        $db = Capsule::connection()->getPdo();

        //encrypt password throw localApi before write to db
        $command = 'encryptpassword';
        $admin_user = AdminUser::getAdminUserName();
        $values["password2"] = $password;
        $results = localAPI($command, $values, $admin_user);
        $password = $results['password'];
        if ($db->query("UPDATE `tblhosting` SET `username` = '{$username}', `password` = '{$password}' WHERE `id` = {$serviceId};")->rowCount() > 0) {
            Log::getInstance()->event("Set new admin login {$username} to service");
        } else {
            Log::getInstance()->event("Can't set new admin login {$username} to service");
        }
    }

    /**
     * clean customfieldsvalues during order deleting
     *
     * @param int $orderId id of order
     */
    public static function cleanDataForOrder($orderId)
    {
        $db = Capsule::connection()->getPdo();
        //list orders's service (one or more)
        $sth = $db->prepare('SELECT * FROM `tblhosting` WHERE `orderid` = ?');
        $sth->execute(array($orderId));
        $services = $sth->fetchAll();

        if (is_array($services)) {
            foreach ($services as $service) {
                //remove tblcustomfieldsvalues of this service
                $sthValues = $db->prepare('DELETE FROM `tblcustomfieldsvalues` WHERE `relid` = ?');
                $sthFields = $db->prepare('DELETE FROM `tblcustomfields` WHERE `relid` = ?');

                $sthValues->execute(array($service['id']));
                $sthFields->execute(array($service['id']));
            }
        }
    }
}