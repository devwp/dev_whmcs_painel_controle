<?php
/**
 *
 * @version 1.0.0
 * @Copyright © 2002-2016 Acronis International GmbH. All rights reserved
 *
 * OPTIONAL DEPENDENCIES: Acronis\Lang ['desired_config_option_not_found','please_check_config','cant_get_from_custom_fields','custom_field_has_wrong_value']
 */

namespace Acronis\WHMCS;

use Acronis\Standard\Configs\ModuleConfig;

class ProvisionParams
{
    protected $messages = array
    (
        'desired_config_option_not_found' => 'Desired configurable option not found: ',
        'cant_get_from_custom_fields' => 'Can\'t get ?? from Custom Fields',
        'custom_field_has_wrong_value' => '?? Custom Field has inapproperiate value',
        'please_check_config' => 'Please check your configuration'
    );

    protected $params = array();
    protected $config = null;

    /**
     *
     * @param array $params - $params array given to WHMCS provisioning module functions
     * @param ModuleConfig $config
     * @param \Acronis\WHMCS\Lang $language - [optional] language object
     */
    public function __construct($params, ModuleConfig $config, $language = null)
    {
        $this->params = $params;
        $this->config = $config;
        if (is_object($language) && get_class($language) == 'Acronis\\WHMCS\\Lang') {
            try {
                $messages_lang = $language->getLangArray();
            } catch (\Exception $e) {
                $messages_lang = array();
            }
            foreach ($this->messages as $k => $v) {
                if (isset($messages_lang[$k])) {
                    $this->messages[$k] = $messages_lang[$k];
                }
            }
        }
    }


    /**
     * Gets desired config option
     * @param int $config_option_id - id of option
     * @param mixed $can_be_config_options_name - true to search in configurable options if value is textual; false otherwise; if this is string it will result in searching in configurable options if param value equals this string ($unlimited_flag is ignored in that case)
     * @param string $unlimited_flag - defaults to "unlimited" - if this is value of the option than it will be returned even if there are such config options in params
     * @param bool $force_config_options_name - true to throw an error if option is textual and $can_be_config_options_name and is not found in configurable options
     * @return int|mixed
     */
    public function getConfigOption($config_option_id, $can_be_config_options_name = true, $unlimited_flag = "unlimited", $force_config_options_name = true)
    {
        $value = $this->params['configoption' . $config_option_id];
        if ((!is_string($can_be_config_options_name) && $can_be_config_options_name && $unlimited_flag !== $value && !is_numeric($value))
            || (is_string($can_be_config_options_name) && $can_be_config_options_name == $value)
        ) {
            if (isset($this->params['configoptions'][$value])) {
                $value = $this->params['configoptions'][$value];
            } elseif ($force_config_options_name) {
                $this->config->makeUserError($this->messages['desired_config_option_not_found'] . ' ' . $value . '. ' . $this->messages['please_check_config']);
            } else {
                $value = 0;
            }
        }

        return $value;
    }

    /**
     * Gets full server url based of Provision modules params
     * @return string Returns prepared server url
     * @internal param array $vars - params variable (parameter for WHMCS provision entry point functions)
     */
    public function getServerUrl()
    {
        $serverhostname = $this->params['serverhostname'];
        $serverip = $this->params['serverip'];
        $serversecure = $this->params['serversecure'];

        $server_data = parse_url($serverhostname);
        if (!isset($server_data['host'])) {
            $server_data['host'] = preg_replace('/\/.*$/i', '', $serverhostname);
        }
        $serverprotocol = trim(isset($server_data['scheme']) ? $server_data['scheme'] : '');
        $serverhostname = trim($server_data['host']);

        if ($serversecure && ($serverprotocol == 'http' || $serverprotocol == 'ftp')) {
            $serverprotocol .= 's';
        } elseif (!$serversecure && $serverprotocol == 'https') {
            $serverprotocol = 'http';
        } elseif (!$serversecure && $serverprotocol == 'ftps') {
            $serverprotocol = 'ftp';
        } else {
            $serverprotocol = 'http';
            if ($serversecure) {
                $serverprotocol = 'https';

            }
        }
        $server_url = $serverprotocol . '://' . ($serverhostname != '' ? $serverhostname : $serverip);
        return $server_url;
    }

    /**
     * Gets custom field from module vars
     * @param string $field_name Name of the custom field to get
     * @return int Group id
     */
    public function getCustomFieldFromVars($field_name)
    {
        if (!isset($this->params['customfields']) || !isset($this->params['customfields'][$field_name])) {
            $this->config->makeUserError(str_replace('??', $field_name, $this->messages['cant_get_from_custom_fields']) . '. ' . $this->messages['please_check_config']);
        } else {
            $group_id = trim($this->params['customfields']['GroupID']);

            if (!preg_match('/^\d+$/i', $group_id)) {
                $this->config->makeUserError(str_replace('??', $field_name, $this->messages['custom_field_has_wrong_value']) . '. ' . $this->messages['please_check_config']);
            }
        }

        return $group_id;
    }

}

