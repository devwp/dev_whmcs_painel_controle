<?php
$_LANG['curl_not_installed_error'] = 'PHP cURL ist nicht installiert.';
$_LANG['cant_get_hostname_for_user'] = 'Der Host-Name für den Benutzer konnte nicht abgerufen werden.';
$_LANG['cant_log_into_system'] = 'Anmeldung am System nicht möglich.';
$_LANG['cant_get_server_admin_id_or_his_group_id'] = 'Die ID des Server-Administrators konnte nicht abgerufen werden.';
$_LANG['cant_send_activation_email'] = 'Die Aktivierungsnachricht konnte nicht gesendet werden.';
$_LANG['cant_get_mail_details'] = 'Die E-Mail-Details können nicht abgerufen werden.';
$_LANG['cant_get_access_token'] = 'Das E-Mail-Zugriffstoken konnte nicht abgerufen werden.';
$_LANG['cant_activate_account'] = 'Das Konto konnte nicht aktiviert werden.';
$_LANG['cant_get_admin_details'] = 'Die Administrator-Kontodetails können nicht abgerufen werden.';
$_LANG['cant_find_in_admin_details'] = 'Der Parameter \'??\' kann in den Administrator-Kontodetails nicht gefunden werden.';
$_LANG['cant_send_admin_password_reset'] = 'Die Nachricht zur Kennwortzurücksetzung konnte nicht gesendet werden.';
$_LANG['cant_determine_access_type'] = 'Der Zugriffstyp von \'??\' konnte nicht ermittelt werden.';
$_LANG['admin_credentials_cant_be_empty'] = 'E-Mail-Adresse und Anmeldename für den Administrator dürfen nicht leer sein.';
$_LANG['cant_add_group_admin'] = 'Das Gruppen-Administrator-Konto konnte nicht hinzugefügt werden.';
$_LANG['cant_get_id_of_created_admin'] = 'Das Administrator-Konto konnte nicht erstellt werden, weil dessen ID oder Version nicht abgerufen werden konnte.';
$_LANG['cant_get_subgroups_for_group_id'] = 'Die Untergruppen der Gruppe mit der ID \'??\' können nicht abgerufen werden.';
$_LANG['cant_find_storage_id_of_subgroup'] = 'Die Storage-ID der Untergruppe konnte nicht gefunden werden.';
$_LANG['cant_get_group_details'] = 'Die Gruppendetails können nicht abgerufen werden.';
$_LANG['cant_find_group_name_in_parent_group'] = 'Kann die Untergruppe \'??\' in der Gruppe nicht finden.';
$_LANG['group_with_kind_not_found'] = 'Die Gruppe mit dem Typ \'??\' konnte nicht gefunden werden.';
$_LANG['cant_find_in_group'] = '\'??\' konnte nicht in der Gruppe gefunden werden.';
$_LANG['cant_find_final_group_kind'] = 'Der finale Typ der Gruppe konnte nicht gefunden werden.';
$_LANG['cant_find_reseller_creation_group_kind'] = 'Kann den am besten passenden Gruppentyp nicht finden, bei dem die Reseller-Erstellung für den Reseller erlaubt ist.';
$_LANG['cant_find_no_reseller_creation_group_kind'] = 'Kann den am besten passenden Gruppentyp nicht finden, bei dem die Reseller-Erstellung für den Reseller verboten ist.';
$_LANG['allow_no_allow_reseller_creation_conflict'] = 'Erstellung eines Reseller-Konfliktes erlauben/verbieten – kann die besten Gruppentypen/Gruppeneinstufungen nicht ermitteln.';
$_LANG['cant_create_no_reseller_creation_group'] = 'Sie können keine Reseller-Untergruppe erstellen, ohne die Fähigkeit zur Reseller-Erstellung für diese Gruppe zu haben.';
$_LANG['cant_determine_production_pricing_mode'] = 'Der beste Produktions-Berechnungsmodus konnte nicht ermittelt werden.';
$_LANG['cant_add_group'] = 'Die Gruppe konnte nicht zum System hinzugefügt werden.';
$_LANG['cant_get_id_of_created_group'] = 'Die Gruppe konnte nicht erstellt werden, weil ihre ID oder Version nicht abgerufen werden konnte.';
$_LANG['cant_delete_group'] = 'Die Gruppe konnte nicht gelöscht werden.';
$_LANG['cant_update_group'] = 'Die Gruppe konnte nicht aktualisiert werden.';
$_LANG['cant_get_version_of_updated_group'] = 'Die Gruppe konnte nicht aktualisiert werden, weil ihre ID oder Version nicht abgerufen werden konnte.';
$_LANG['cant_lock_group'] = 'Die Gruppe konnte nicht gesperrt werden.';
$_LANG['cant_unlock_group'] = 'Die Sperrung der Gruppe konnte nicht aufgehoben werden.';
$_LANG['cant_get_group_users_details'] = 'Es konnten keine Informationen über die Benutzer in der Gruppe \'??\' abgerufen werden.';
$_LANG['cant_get_group_admins_details'] = 'Es konnten keine Informationen über die Administratoren in der Gruppe \'??\' abgerufen werden.';
$_LANG['cant_find_admin_login_in_group'] = 'Der Administrator mit dem Anmeldenamen \'??\' konnte nicht in der Gruppe gefunden werden.';
$_LANG['cant_find_admin_group_id'] = 'Die Gruppen-ID des Administrators \'??\' konnte nicht gefunden werden.';
$_LANG['cant_find_group_name_id'] = 'Die ID der Gruppe \'??\' konnte nicht gefunden werden.';
$_LANG['cant_get_group_bc_link'] = 'Der Backup-Konsolen-Link für die Gruppe mit der ID \'??\' konnte nicht abgerufen werden.';
$_LANG['cant_get_logic_constants_db_unknown_host'] = 'Es konnten keine logischen Konstanten aus der Datenbank abgerufen werden, weil der Host-Name unbekannt ist.';
$_LANG['cant_save_logic_constants_db_unknown_host'] = 'Es konnten keine logischen Konstanten in die Datenbank gespeichert werden, weil der Host-Name unbekannt ist.';
$_LANG['cant_get_logic_constants'] = 'Logische Konstanten konnten nicht abgerufen werden.';
$_LANG['group_kind_not_found'] = 'Die Gruppetyp \'??\' konnte nicht gefunden werden.';
$_LANG['cant_get_group_kinds'] = 'Gruppentypen konnten nicht abgerufen werden.';
$_LANG['cant_get_pricing_modes'] = 'Preisberechnungsmodi konnten nicht abgerufen werden.';
$_LANG['cant_get_access_types'] = 'Zugriffstypen konnten nicht abgerufen werden.';
$_LANG['cant_get_statuses_constant_data'] = 'Die Informationen über die minimalen Statuszustände konnten nicht abgerufen werden.';
$_LANG['cant_get_version_of_updated_group'] = 'Das Benutzerkonto konnte nicht aktualisiert werden, weil dessen ID oder Version nicht abgerufen werden konnte.';
$_LANG['couldnt_connect'] = 'Der Host konnte nicht aufgelöst werden.';
$_LANG['group_with_such_properties_exist'] = 'Eine Gruppe mit diesen Eigenschaften ist bereits vorhanden.';
$_LANG['admin_with_such_email_exist'] = 'Ein Benutzer mit diesem Anmeldenamen ist bereits vorhanden.';
$_LANG['measure']['B'] = 'B';
$_LANG['measure']['KiB'] = 'KiB';
$_LANG['measure']['MiB'] = 'MiB';
$_LANG['measure']['GiB'] = 'GiB';
$_LANG['measure']['TiB'] = 'TiB';
$_LANG['measure']['PiB'] = 'PiB';
$_LANG['measure']['EiB'] = 'EiB';
$_LANG['measure']['YiB'] = 'YiB';
$_LANG['unlimited'] = 'Unbegrenzt.';
$_LANG['wrong_data'] = 'Ungültige Daten.';
$_LANG['please_check_config'] = 'Überprüfen Sie Ihre Konfiguration.';
$_LANG['desired_config_option_not_found'] = 'Folgende konfigurierbare Option wurde nicht gefunden:';
$_LANG['cant_get_from_custom_fields'] = '\'??\' konnte nicht aus den benutzerdefinierbaren Feldern abgerufen werden.';
$_LANG['custom_field_has_wrong_value'] = 'Der Wert des benutzerdefinierbaren Feldes \'??\' ist falsch.';
$_LANG['overusage']['server_count'] = 'Die Server-Quota wurde überschritten.';
$_LANG['overusage']['storage_size'] = 'Die Cloud Storage-Quota wurde überschritten.';
$_LANG['overusage']['vm_count'] = 'Die Quota für virtuelle Maschinen wurde überschritten.';
$_LANG['overusage']['workstation_count'] = 'Die Workstation-Quota wurde überschritten.';
$_LANG['overusage']['mailbox_count'] = 'Die Quota für Office 365-Postfächer wurde überschritten.';
$_LANG['overusage']['mobile_count'] = 'Die Quota für Mobilgeräte wurde überschritten.';
$_LANG['nearlylimit']['server_count'] = 'Das Limit für die Server-Quota ist zu über 90%% erreicht.';
$_LANG['nearlylimit']['storage_size'] = 'Der Cloud Storage ist zu über 90%% belegt.';
$_LANG['nearlylimit']['vm_count'] = 'Das Limit der Quota für virtuelle Maschinen ist zu über 90%% erreicht.';
$_LANG['nearlylimit']['workstation_count'] = 'Das Limit für die Workstation-Quota ist zu über 90%% erreicht.';
$_LANG['nearlylimit']['mailbox_count'] = 'Die Quota für Office 365-Postfächer ist zu über 90%% erreicht.';
$_LANG['nearlylimit']['mobile_count'] = 'Die Quota für Mobilgeräte ist zu über 90%% erreicht.';
