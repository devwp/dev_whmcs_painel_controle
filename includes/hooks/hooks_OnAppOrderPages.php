<?php

 
/**
 * @author Mariusz Miodowski <mariusz@modulesgarden.com>
 */

if(function_exists('mysql_safequery') == false) {
    function mysql_safequery($query,$params=false) {
        if ($params) {
            foreach ($params as &$v) { $v = mysql_real_escape_string($v); }
            $sql_query = vsprintf( str_replace("?","'%s'",$query), $params );
            $sql_query = mysql_query($sql_query);
        } else {
            $sql_query = mysql_query($query);
        }
        
        return ($sql_query);
    }
}
/**
 * Tax Calculation, custom order template
 */
if(isset($_REQUEST['a'])  && isset($_REQUEST['ajax']) &&  isset($_REQUEST['custom_template']) &&  ($_REQUEST['a']=='view' || $_REQUEST['a']=='checkout' || $_REQUEST['a']=='confdomains' ) && $_REQUEST['ajax']==1 
     && ( isset($_REQUEST['custom_template']) && in_array($_REQUEST['custom_template'], array(
        'OnAppOnePage',
        'OnAppStepCart',
        'OnAppOnePageSix',
        'OnAppStepCartSix'
    )) )){   
    if(isset($_REQUEST['country']) ||  isset($_REQUEST['state'])){
        $_SESSION['cart']['user']['country']= $_REQUEST['country'];
        $_SESSION['cart']['user']['state']= $_REQUEST['state'];
    }
    $GLOBALS['CONFIG']['OrderFormTemplate'] =$_REQUEST['custom_template'];
}


/**
 * Custom order template WHMCS6
 */
$scriptName = substr($_SERVER['SCRIPT_NAME'], strrpos($_SERVER['SCRIPT_NAME'], DIRECTORY_SEPARATOR) + 1);
if($scriptName=="cart.php" && (isset($_GET['gid']) || isset($_GET['pid']))){
    unset($_SESSION['onAppOrderFormTemplate']);
    if(isset($_GET['gid'])){
        $group = mysql_safequery('SELECT orderfrmtpl FROM   `tblproductgroups`  WHERE id =? ', array($_GET['gid']));
    }else{
        $group = mysql_safequery('SELECT pg.orderfrmtpl FROM   tblproducts p left join `tblproductgroups` pg on (p.gid=pg.id)  WHERE p.id =? ', array($_GET['pid']));
    }
    $group = mysql_fetch_assoc( $group);
    $onapp_templates = array(
        'OnAppOnePage',
        'OnAppStepCart'
    );
    if(in_array($group['orderfrmtpl'], $onapp_templates))
         $_SESSION['onAppOrderFormTemplate'] = $group['orderfrmtpl'];
    
}else if($scriptName=="cart.php" && empty($_GET)){
    unset($_SESSION['onAppOrderFormTemplate']);
}
else if($scriptName=="cart.php" && isset($_SESSION['onAppOrderFormTemplate'])){
    $GLOBALS['CONFIG']['OrderFormTemplate'] = $_SESSION['onAppOrderFormTemplate'];
}else{
    unset($_SESSION['onAppOrderFormTemplate']);
}


function OnAppOrderPages_ClientAreaPage($vars)
{
    
    global $orderform, $orderfrm, $CONFIG;
    
    if($CONFIG['OrderFormTemplate'] == 'OnAppOnePage' && $vars['filename'] == 'cart')
    {
        if(!isset($_REQUEST['ajax']) && !empty($_SERVER['QUERY_STRING']))
        {   
            if(!isset($_GET['gid']) && !in_array($_GET['a'], array('add')))
            {
                ob_clean();
                header("Location: cart.php");
                die();
            }
        }
    }
    
        $onapp_templates = array
    (
        'OnAppOnePage',
        'OnAppStepCart'
    );

    if(!isset($orderform) || !isset($orderfrm) )
    {
        return;
    }

    if(isset($_REQUEST['custom_template']) && in_array($_REQUEST['custom_template'], $onapp_templates) && method_exists($orderfrm, 'setTemplate'))
    {
        $orderfrm->setTemplate($_REQUEST['custom_template']);
    }

}

add_hook('ClientAreaPage', 1, 'OnAppOrderPages_ClientAreaPage');

function OnAppOrderPages_ClientAreaOnAppOnePage($vars){

    if($vars['filename'] != 'cart' )//&& !$_GET['gid']
        return;
    $group = mysql_safequery('SELECT orderfrmtpl FROM   `tblproductgroups`  WHERE id =? ', array($_GET['gid']));
    $group = mysql_fetch_assoc( $group);

    if(!in_array($group['orderfrmtpl'], array("OnAppOnePage",'OnAppStepCart','OnAppStepCartSix' )))//
        return;

    global $smarty;

    //prepare description
    $products = $smarty->get_template_vars('products');
    if(!empty($products)){
        foreach($products as &$product)
        {

            $desc = array_filter(explode("\n", $product['description']));
            foreach($desc as &$d)
            {
                $d = explode("|", $d);
            }
            $product['custom_description'] = $desc;
        }
        $smarty->assign('products', $products);
    }
    
    //Get Domains From Template
    $domains            =   $smarty->get_template_vars('domains');
    if(!empty($domains)){
        //Get Domains From Session
        $session_domains    =   $_SESSION['cart']['domains'];
        //We need domain type
        foreach($domains as &$domain)
        {
            foreach($session_domains as &$session_domain)
            {
                if($domain['domain'] == $session_domain['domain'])
                {
                    $domain['type'] = $session_domain['type'];
                    break;
                }
            }
        }

        foreach($domains as &$domain)
        {
            //Domain TLD
            $tld                           =   substr($domain['domain'], strpos($domain['domain'], '.'));
            //TLD Pricing
            $domain['pricing']             =   getTLDPriceList($tld, true);
        }

        //Assign to template
        $smarty->assign('domains', $domains);
    }
}
add_hook('ClientAreaPage', 1, 'OnAppOrderPages_ClientAreaOnAppOnePage');

//$GLOBALS['CONFIG']['OrderFormTemplate'] = "OnAppStepCartSix";
function OnAppOrderPages_ClientAreaPageOnAppStepCart($vars){
 
    if($vars['filename'] != 'cart' )
        return;
    if($_GET['gid']){
        
        $group = mysql_safequery('SELECT orderfrmtpl FROM   `tblproductgroups`  WHERE id =? ', array($_GET['gid']));
        $group = mysql_fetch_assoc( $group);
        if(!in_array($group['orderfrmtpl'], array("OnAppOnePage",'OnAppStepCart','OnAppStepCartSix' )))//
            return;

    }

    
    
    if(!in_array($vars['carttpl'], array('OnAppStepCart', 'OnAppStepCartSix' )))
        return;
    global $smarty;
    
    if($_GET['a']=='confproduct'){

        //Client can order only one producy
        if(count($_SESSION['cart']['products']) > 1)
        {
            foreach($_SESSION['cart']['products'] as $p_key => $p)
            {
                if($p_key != $_REQUEST['i'])
                {
                    unset($_SESSION['cart']['products'][$p_key]);
                }
            }
        } 

        if($_REQUEST['i'] != 0)
        {
            $product = $_SESSION['cart']['products'][$_REQUEST['i']];
            if($product)
            {
                $_SESSION['cart']['products'] = array();
                $_SESSION['cart']['products'][0] = $product;
            }

            ob_clean();
            header('Location: cart.php?a=confproduct&i=0');
            ob_end_flush();
            die();
            return;
        }

        //find steps
        $options        =   $smarty->get_template_vars('configurableoptions');
        $productinfo    =   $smarty->get_template_vars('productinfo');
        $customfields   =   $smarty->get_template_vars('customfields');
        $steps = array();

        foreach($options as $option)
        {
            $q = mysql_query("SELECT g.name, g.description, g.id 
                FROM tblproductconfigoptions o
                LEFT JOIN tblproductconfiggroups g ON o.gid = g.id
                WHERE o.id = ".$option['id']);
            $row = mysql_fetch_assoc($q);

            if(strpos($name, '|'))
            {
                $name                                                           =   explode('|', $row['name']);
            }
            else
            {
                $name[0] = $row['name'];
                $name[1] = $row['name'];
            }

            $name[0]                                    =   str_replace(array(" "), array(""), $name[0]);
            $name[0]                                    =   'onapp-step-'.strtolower($name[0]); 
            $steps[$name[0]]['name']                    =   $name[1];
            $steps[$name[0]]['description']             =   $row['description'];
            $steps[$name[0]]['configurableoptions'][]   =   $option;
        }

        $number_of_steps = count($steps);


        if($number_of_steps == 0 && $productinfo['type'] == 'server')
        {
            $smarty->assign('steps', array());
            $smarty->assign('first_step', 'onapp-step-server-configuration');
            $smarty->assign('last_step', 'onapp-step-server-configuration');
            $number_of_steps = 1;
        }
        elseif($number_of_steps == 0 && empty($options) && empty($customfields))
        {

            $smarty->assign('steps', array());
            $smarty->assign('first_step', 'onapp-step-confirmation');
            $smarty->assign('last_step', 'onapp-step-confirmation');
            $smarty->assign('totalSteps', '1');
            $number_of_steps = 1;
        }
        else if(!empty($options) || !empty($customfields))
        {
           if(!empty($options) ){
            ksort($steps);

            $keys = array_keys($steps);
            $smarty->assign('steps', $steps);
            $smarty->assign('first_step', $keys[0] ? $keys[0] : 'onapp-step-server-configuration');
            $smarty->assign('last_step', $keys[count($keys)-1] ? $keys[count($keys)-1] : 'onapp-step-server-configuration');
            $number_of_steps =  count($steps) + 1;
               
           } else if(!empty($customfields)){
                $smarty->assign('steps', array());
                $smarty->assign('first_step', 'onapp-step-confirmation-customfield');
                $smarty->assign('last_step', 'onapp-step-confirmation');
                $smarty->assign('totalSteps', '2');
                $number_of_steps =1;
           }     
           
        }
        else {

            $smarty->assign('steps', array());
            $smarty->assign('first_step', 'onapp-step-confirmation');
            $smarty->assign('last_step', 'onapp-step-confirmation');
            $smarty->assign('totalSteps', '1');
            $number_of_steps = 1;
        }

        $smarty->assign('number_of_steps', $number_of_steps);
    }
    
    
}
add_hook('ClientAreaPage', 1, 'OnAppOrderPages_ClientAreaPageOnAppStepCart');