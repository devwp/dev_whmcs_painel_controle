<?php
use WHMCS\View\Menu\Item as MenuItem;

add_hook('ClientAreaPrimaryNavbar', 1, function (MenuItem $primaryNavbar)
{
    $userid = intval($_SESSION['uid']);
    if ($userid=='0'){
			if (!is_null($primaryNavbar->getChild('Home'))) {
				$primaryNavbar->removeChild('Home');
			}
			if (!is_null($primaryNavbar->getChild('Announcements'))) {
					$primaryNavbar->removeChild('Announcements');
			}
			if (!is_null($primaryNavbar->getChild('Knowledgebase'))) {
					$primaryNavbar->removeChild('Knowledgebase');
			}
			if (!is_null($primaryNavbar->getChild('Network Status'))) {
					$primaryNavbar->removeChild('Network Status');
			}
			if (!is_null($primaryNavbar->getChild('Affiliates'))) {
					$primaryNavbar->removeChild('Affiliates');
			}
			if (!is_null($primaryNavbar->getChild('Contact Us'))) {
					$primaryNavbar->removeChild('Contact Us');
			}
    }
});
add_hook('ClientAreaSecondaryNavbar', 1, function (MenuItem $secondaryNavbar)
{
    $userid = intval($_SESSION['uid']);
    if ($userid=='0'){
			if (!is_null($secondaryNavbar->getChild('Register'))) {
				$secondaryNavbar->removeChild('Register');
			}
    }
});
