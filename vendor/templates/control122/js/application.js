var app = function() {

    var init = function() {
        toggleMenuLeft();
        menu();
    };

    var toggleMenuLeft = function() {
        $('#toggle-left').bind('click', function(e) {
           $('body').removeClass('off-canvas-open')    
            var bodyEl = $('#container');
            ($(window).width() > 768) ? $(bodyEl).toggleClass('sidebar-mini'): $(bodyEl).toggleClass('sidebar-opened');
        });
    };

    

    var menu = function() {
        var subMenu = $('.sidebar .nav');
        $(subMenu).navgoco({
            caretHtml: false,
            accordion: true,
            slide: {
                  duration: 400,
                  easing: 'swing'
              }
          });

    };
    //End functions

    //Dashboard functions
    var timer = function() {
        $('.timer').countTo();
    };


  
    //return functions
    return {
        init: init,
        timer: timer,
  
    };
}();

//Load global functions
$(document).ready(function() {
    app.init();
});
