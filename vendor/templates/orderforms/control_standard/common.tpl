{include file="orderforms/$carttpl/cart-options.tpl"}
{if $itCartiCheckColor eq "aero"}
	<link rel="stylesheet" type="text/css" href="templates/orderforms/{$carttpl}/icheck/skins/flat/aero.css" />
{elseif $itCartiCheckColor eq "blue"}
	<link rel="stylesheet" type="text/css" href="templates/orderforms/{$carttpl}/icheck/skins/flat/blue.css" />
{elseif $itCartiCheckColor eq "green"}
	<link rel="stylesheet" type="text/css" href="templates/orderforms/{$carttpl}/icheck/skins/flat/green.css" />
{elseif $itCartiCheckColor eq "grey"}
	<link rel="stylesheet" type="text/css" href="templates/orderforms/{$carttpl}/icheck/skins/flat/grey.css" />
{elseif $itCartiCheckColor eq "orange"}
	<link rel="stylesheet" type="text/css" href="templates/orderforms/{$carttpl}/icheck/skins/flat/orange.css" />
{elseif $itCartiCheckColor eq "pink"}
	<link rel="stylesheet" type="text/css" href="templates/orderforms/{$carttpl}/icheck/skins/flat/pink.css" />
{elseif $itCartiCheckColor eq "purple"}
	<link rel="stylesheet" type="text/css" href="templates/orderforms/{$carttpl}/icheck/skins/flat/purple.css" />
{elseif $itCartiCheckColor eq "red"}
	<link rel="stylesheet" type="text/css" href="templates/orderforms/{$carttpl}/icheck/skins/flat/red.css" />
{elseif $itCartiCheckColor eq "yellow"}
	<link rel="stylesheet" type="text/css" href="templates/orderforms/{$carttpl}/icheck/skins/flat/yellow.css" />
{elseif $itCartiCheckColor eq "black"}
	<link rel="stylesheet" type="text/css" href="templates/orderforms/{$carttpl}/icheck/skins/flat/flat.css" />
{else}
	{if $itColorScheme eq "blue" or $itColorScheme eq "blue-solid" or $itColorScheme eq "blue-white"}
		<link rel="stylesheet" type="text/css" href="templates/orderforms/{$carttpl}/icheck/skins/flat/blue.css" />
	{else}
		<link rel="stylesheet" type="text/css" href="templates/orderforms/{$carttpl}/icheck/skins/flat/green.css" />
	{/if}
{/if}
<script type="text/javascript" src="{$BASE_PATH_JS}/icheck.js"></script>
<script type="text/javascript" src="templates/orderforms/{$carttpl}/base.js"></script>
<script>
jQuery(document).ready(function () {
    jQuery('#btnShowSidebar').click(function () {
        if (jQuery('.cart-sidebar').is(":visible")) {
            jQuery('.cart-main-column').css('right','0');
            jQuery('.cart-sidebar').fadeOut();
            jQuery('#btnShowSidebar').html('<i class="fa fa-arrow-circle-left"></i> {$LANG.showMenu}');
        } else {
            jQuery('.cart-sidebar').fadeIn();
            jQuery('.cart-main-column').css('right','300px');
            jQuery('#btnShowSidebar').html('<i class="fa fa-arrow-circle-right"></i> {$LANG.hideMenu}');
        }
    });
});
</script>