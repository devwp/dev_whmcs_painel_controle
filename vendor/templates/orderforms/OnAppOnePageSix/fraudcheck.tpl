{include file="orderforms/OnAppOnePageSix/header.tpl"} 

<h2>{$LANG.thereisaproblem}</h2>

<div class="alert alert-error">
    <i class="icon icon-remove"></i>
    {$error}
</div>

{include file="orderforms/OnAppOnePageSix/footer.tpl"} 