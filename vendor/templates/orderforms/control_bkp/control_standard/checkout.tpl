<script>
    // Define state tab index value
    var statesTab = 10;
    // Do not enforce state input client side
    var stateNotRequired = true;
</script>
<script type="text/javascript" src="{$BASE_PATH_JS}/StatesDropdown.js"></script>
{function name=getFontAwesomeCCIcon}
    {if $ccType eq "Visa"}
        fa-cc-visa
    {elseif $ccType eq "MasterCard"}
        fa-cc-mastercard
    {elseif $ccType eq "Discover"}
        fa-cc-discover
    {elseif $ccType eq "American Express"}
        fa-cc-amex
    {elseif $ccType eq "JCB"}
        fa-cc-jcb
    {elseif $ccType eq "Diners Club" || $ccType eq "EnRoute"}
        fa-cc-diners-club
    {else}
        fa-credit-card
    {/if}
{/function}
{include file="orderforms/control_standard/common.tpl"}
<!--main content start-->
<section id="main-content" class="cart">
	<!-- Display mobile sidebar alternative if applicable -->
	{if $itCartSidebarMobileDisplay eq "enabled"}
		<div class="row cat-col-row visible-xs visible-sm">
			<div class="col-md-12">
				{include file="orderforms/control_standard/sidebar-categories-collapsed.tpl"}
			</div>
		</div>
	{/if}
	<!-- Display page title -->
	<div class="row">
		<div class="col-md-12">
			<!--breadcrumbs start -->
			{if $itCartSidebarDisplay eq "enabled"}
				<div style="position: absolute; right: 15px; margin-top: 22px;">
				    <button type="button" class="btn btn-default btn-sm hidden-xs hidden-sm" id="btnShowSidebar">
				        <i class="fa fa-arrow-circle-left"></i>
				        {$LANG.showMenu}
				    </button>
				</div>
			{/if}
			{include file="$template/includes/breadcrumb.tpl"}
			<!--breadcrumbs end -->
			<h1 class="h1">{$LANG.ordercheckout}</h1>
		</div>
	</div>
	<!-- Display sidebar layout if applicable -->
	{if $itCartSidebarDisplay eq "enabled"}
		<div class="row cart-main-column">
			<div id="internal-content" class="col-md-12 pull-md-left">
				{/if}
				<form method="post" action="{$smarty.server.PHP_SELF}?a=checkout" class="form-horizontal" name="orderfrm">
            	    <input type="hidden" name="submit" value="true" />
					<input type="hidden" name="custtype" id="inputCustType" value="{$custtype}" />
					<div class="row">
						<div class="col-md-12">
				            {if $errormessage}
				                <div class="alert alert-danger checkout-error-feedback" role="alert">
				                    <p>{$LANG.orderForm.correctErrors}:</p>
				                    <ul>
				                        {$errormessage}
				                    </ul>
				                </div>
				                <div class="clearfix"></div>
				            {/if}
							<div class="panel panel-default">
								<div class="panel-heading">
									<h3 class="panel-title pull-left">	
										{$LANG.billingdetails}
									</h3>
									{if !$loggedin}
										<a href="#" class="pull-right btn btn-default btn-xs" data-toggle="modal" data-target="#loginModal">
											<i class="fa fa-sign-in"></i>
											 {$LANG.orderForm.alreadyRegistered}
										</a>
									{else}
										<a href="clientarea.php?action=details" class="pull-right btn btn-default btn-xs">
											<i class="fa fa-edit"></i>
											 {$LANG.editaccountdetails}
										</a>
									{/if}
									<div class="clearfix"></div>
								</div>
								<div class="panel-body">
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<label for="inputFirstName" class="col-md-4 control-label">{$LANG.clientareafirstname}</label>
												<div class="col-md-6">
													<input class="form-control" type="text" name="firstname" id="inputFirstName" placeholder="{$LANG.orderForm.firstName}" value="{$clientsdetails.firstname}"{if $loggedin} readonly="readonly"{/if} autofocus>
												</div>
											</div>
											<div class="form-group">
												<label for="inputLastName" class="col-md-4 control-label">{$LANG.clientarealastname}</label>
												<div class="col-md-6">
													<input class="form-control" type="text" name="lastname" id="inputLastName" placeholder="{$LANG.orderForm.lastName}" value="{$clientsdetails.lastname}"{if $loggedin} readonly="readonly"{/if}>
												</div>
											</div>

											<div class="form-group">
												<label for="inputCompanyName" class="col-md-4 control-label">{$LANG.clientareacompanyname}</label>
												<div class="col-md-6">
													<input class="form-control" type="text" name="companyname" id="inputCompanyName" placeholder="{$LANG.orderForm.companyName} ({$LANG.orderForm.optional})" value="{$clientsdetails.companyname}"{if $loggedin} readonly="readonly"{/if}>
												</div>
											</div>

											<div class="form-group">
												<label for="inputEmail" class="col-md-4 control-label">{$LANG.clientareaemail}</label>
												<div class="col-md-6">
													<input class="form-control" type="email" name="email" id="inputEmail" placeholder="{$LANG.orderForm.emailAddress}" value="{$clientsdetails.email}"{if $loggedin} readonly="readonly"{/if}>
												</div>
											</div>

											<div class="form-group">
												<label for="inputPhone" class="col-md-4 control-label">{$LANG.clientareaphonenumber}</label>
												<div class="col-md-6">
													<input class="form-control" type="tel" name="phonenumber" id="inputPhone" placeholder="{$LANG.orderForm.phoneNumber}" value="{$clientsdetails.phonenumber}"{if $loggedin} readonly="readonly"{/if}>
												</div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label for="inputAddress1" class="col-md-4 control-label">{$LANG.clientareaaddress1}</label>
												<div class="col-md-6">
													<input class="form-control" type="text" name="address1" id="inputAddress1" placeholder="{$LANG.orderForm.streetAddress}" value="{$clientsdetails.address1}"{if $loggedin} readonly="readonly"{/if}>
												</div>
											</div>
											<div class="form-group">
												<label for="inputAddress2" class="col-md-4 control-label">{$LANG.clientareaaddress2}</label>
												<div class="col-md-6">
													<input class="form-control" type="text" name="address2" id="inputAddress2" placeholder="{$LANG.orderForm.streetAddress2}" value="{$clientsdetails.address2}"{if $loggedin} readonly="readonly"{/if}>
												</div>
											</div>
											<div class="form-group">
												<label for="inputCity" class="col-md-4 control-label">{$LANG.clientareacity}</label>
												<div class="col-md-6">
													<input class="form-control" type="text" name="city" id="inputCity" placeholder="{$LANG.orderForm.city}" value="{$clientsdetails.city}"{if $loggedin} readonly="readonly"{/if}>
												</div>
											</div>
											<div class="form-group">
												<label for="inputState" class="col-md-4 control-label">{$LANG.clientareastate}</label>
												<div class="col-md-6">
													<input class="form-control" type="text" name="state" id="inputState" placeholder="{$LANG.orderForm.state}" value="{$clientsdetails.state}"{if $loggedin} readonly="readonly"{/if}>
												</div>
											</div>
											<div class="form-group">
												<label for="inputPostcode" class="col-md-4 control-label">{$LANG.clientareapostcode}</label>
												<div class="col-md-6">
													<input class="form-control" type="text" name="postcode" id="inputPostcode" placeholder="{$LANG.orderForm.postcode}" value="{$clientsdetails.postcode}"{if $loggedin} readonly="readonly"{/if}>
												</div>
											</div>
											<div class="form-group">
												<label class="col-md-4 control-label" for="country">{$LANG.clientareacountry}</label>
												<div class="col-md-6">
													<select class="form-control" name="country" id="inputCountry"{if $loggedin} disabled="disabled"{/if}>
					                                    {foreach $countries as $countrycode => $countrylabel}
					                                        <option value="{$countrycode}"{if (!$country && $countrycode == $defaultcountry) || $countrycode eq $country} selected{/if}>
					                                            {$countrylabel}
					                                        </option>
					                                    {/foreach}
					                                </select>
												</div>
											</div>
										</div>
				                    </div>
								</div>
							</div>
		                    {if $customfields}
			                    <div class="panel panel-default">
									<div class="panel-heading">
										<h3 class="panel-title">	
											{$LANG.orderadditionalrequiredinfo}
										</h3>
									</div>
									<div class="panel-body">
										<div class="row">
											<div class="col-md-12">	
												{foreach $customfields as $customfield}
													<br/>
													<div class="form-group">
														<label class="col-md-2 control-label" for="customfield{$customfield.id}">{$customfield.name}</label>
														<div class="col-md-9 control">
															{$customfield.input} {$customfield.description}
														</div>
													</div>
												{/foreach}
											</div>
										</div>
									</div>
			                    </div>
							{/if}
			                {if $domainsinorder}
			                    <div class="panel panel-default">
									<div class="panel-heading">
										<h3 class="panel-title">	
											{$LANG.domainregistrantinfo}
										</h3>
									</div>
									<div class="panel-body">
										<p>{$LANG.orderForm.domainAlternativeContact}</p>
										<br />
										<div class="row">
											<div class="col-md-12">
												<div class="form-group">
													<label class="col-md-2 control-label" for="inputDomainContact">{$LANG.clientareachoosecontact}</label>
													<div class="col-md-9">
														<select name="contact" id="inputDomainContact" class="form-control">
							                                <option value="">{$LANG.usedefaultcontact}</option>
							                                {foreach $domaincontacts as $domcontact}
							                                    <option value="{$domcontact.id}"{if $contact == $domcontact.id} selected{/if}>
							                                        {$domcontact.name}
							                                    </option>
							                                {/foreach}
							                                <option value="addingnew"{if $contact == "addingnew"} selected{/if}>
							                                    {$LANG.clientareanavaddcontact}...
							                                </option>
							                            </select>
													</div>
												</div>
											</div>
										</div>
										<div class="row{if $contact neq "addingnew"} hidden{/if}" id="domainRegistrantInputFields">
											<br />
											<div class="col-md-6">
												<div class="form-group">
													<label for="inputDCFirstName" class="col-md-4 control-label">{$LANG.clientareafirstname}</label>
													<div class="col-md-6">
														<input type="text" name="domaincontactfirstname" id="inputDCFirstName" class="form-control" placeholder="{$LANG.orderForm.firstName}" value="{$domaincontact.firstname}">
													</div>
												</div>
												<div class="form-group">
													<label for="inputDCLastName" class="col-md-4 control-label">{$LANG.clientarealastname}</label>
													<div class="col-md-6">
														<input type="text" name="domaincontactlastname" id="inputDCLastName" class="form-control" placeholder="{$LANG.orderForm.lastName}" value="{$domaincontact.lastname}">
													</div>
												</div>
												<div class="form-group">
													<label for="inputDCCompanyName" class="col-md-4 control-label">{$LANG.clientareacompanyname}</label>
													<div class="col-md-6">
														<input type="text" name="domaincontactcompanyname" id="inputDCCompanyName" class="form-control" placeholder="{$LANG.orderForm.companyName} ({$LANG.orderForm.optional})" value="{$domaincontact.companyname}">
													</div>
												</div>
												<div class="form-group">
													<label for="inputDCEmail" class="col-md-4 control-label">{$LANG.clientareaemail}</label>
													<div class="col-md-6">
														<input type="email" name="domaincontactemail" id="inputDCEmail" class="form-control" placeholder="{$LANG.orderForm.emailAddress}" value="{$domaincontact.email}">
													</div>
												</div>
												<div class="form-group">
													<label for="inputDCPhone" class="col-md-4 control-label">{$LANG.clientareaphonenumber}</label>
													<div class="col-md-6">
														<input type="tel" name="domaincontactphonenumber" id="inputDCPhone" class="form-control" placeholder="{$LANG.orderForm.phoneNumber}" value="{$domaincontact.phonenumber}">
													</div>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label for="inputDCAddress1" class="col-md-4 control-label">{$LANG.clientareaaddress1}</label>
													<div class="col-md-6">
														<input type="text" name="domaincontactaddress1" id="inputDCAddress1" class="form-control" placeholder="{$LANG.orderForm.streetAddress}" value="{$domaincontact.address1}">
													</div>
												</div>
												<div class="form-group">
													<label for="inputDCAddress2" class="col-md-4 control-label">{$LANG.clientareaaddress2}</label>
													<div class="col-md-6">
														<input type="text" name="domaincontactaddress2" id="inputDCAddress2" class="form-control" placeholder="{$LANG.orderForm.streetAddress2}" value="{$domaincontact.address2}">
													</div>
												</div>
												<div class="form-group">
													<label for="inputDCCity" class="col-md-4 control-label">{$LANG.clientareacity}</label>
													<div class="col-md-6">
														<input type="text" name="domaincontactcity" id="inputDCCity" class="form-control" placeholder="{$LANG.orderForm.city}" value="{$domaincontact.city}">
													</div>
												</div>
												<div class="form-group">
													<label for="inputDCState" class="col-md-4 control-label">{$LANG.clientareastate}</label>
													<div class="col-md-6">
														<input type="text" name="domaincontactstate" id="inputDCState" class="form-control" placeholder="{$LANG.orderForm.state}" value="{$domaincontact.state}">
													</div>
												</div>
												<div class="form-group">
													<label for="inputDCPostcode" class="col-md-4 control-label">{$LANG.clientareapostcode}</label>
													<div class="col-md-6">
														<input type="text" name="domaincontactpostcode" id="inputDCPostcode" class="form-control" placeholder="{$LANG.orderForm.postcode}" value="{$domaincontact.postcode}">
													</div>
												</div>
												<div class="form-group">
													<label class="col-md-4 control-label" for="inputDCcountry">{$LANG.clientareacountry}</label>
													<div class="col-md-6">
														<select name="domaincontactcountry" id="inputDCCountry" class="form-control">
						                                    {foreach $countries as $countrycode => $countrylabel}
						                                        <option value="{$countrycode}"{if (!$domaincontact.country && $countrycode == $defaultcountry) || $countrycode eq $domaincontact.country} selected{/if}>
						                                            {$countrylabel}
						                                        </option>
						                                    {/foreach}
						                                </select>
													</div>
												</div>
											</div>
										</div>
									</div>
			                    </div>
			                {/if}
			                {if !$loggedin}
			                	<div id="containerNewUserSecurity"{if !$loggedin && $custtype eq "existing"} class="hidden"{/if}>
				                	<div class="panel panel-default">
										<div class="panel-heading">
											<h3 class="panel-title">	
												{$LANG.orderForm.accountSecurity}
											</h3>
										</div>
										<div class="panel-body">
											<div class="row">
												<div class="col-md-12">
													<div id="newPassword1" class="form-group has-feedback">
														<label for="inputNewPassword1" class="col-md-2 control-label">{$LANG.clientareapassword}</label>
														<div class="col-md-9">
															<input type="password" class="form-control" id="inputNewPassword1" name="password" autocomplete="off"/>
															<span class="form-control-feedback glyphicon glyphicon-password"></span>
															{include file="$template/includes/pwstrength.tpl"}
														</div>
													</div>
													<div id="newPassword2" class="form-group has-feedback">
														<label for="inputNewPassword2" class="col-md-2 control-label">{$LANG.clientareaconfirmpassword}</label>
														<div class="col-md-9">
															<input type="password" class="form-control" id="inputNewPassword2" name="password2" autocomplete="off"/>
															<span class="form-control-feedback glyphicon glyphicon-password"></span>
															<div id="inputNewPassword2Msg">
															</div>
														</div>
													</div>
												</div>
											</div>
					                        {if $securityquestions}
					                            <div class="row">
					                                <div class="col-md-12">
														<div class="form-group">
						                                	<label for="inputSecurityQId" class="col-md-2 control-label">{$LANG.clientareasecurityquestion}</label>
															<div class="col-md-9">
							                                    <select name="securityqid" id="inputSecurityQId" class="form-control">
							                                        <option value="">{$LANG.clientareasecurityquestion}</option>
							                                        {foreach $securityquestions as $question}
							                                            <option value="{$question.id}"{if $question.id eq $securityqid} selected{/if}>
							                                                {$question.question}
							                                            </option>
							                                        {/foreach}
							                                    </select>
															</div>
														</div>
														<div class="form-group">
					                                        <label for="inputSecurityQAns" class="col-md-2 control-label">{$LANG.clientareasecurityanswer}</label>
															<div class="col-md-9">
						                                        <input type="password" name="securityqans" id="inputSecurityQAns" class="form-control" placeholder="{$LANG.clientareasecurityanswer}">
															</div>
														</div>
					                                </div>
					                            </div>
					                        {/if}
					                    </div>
				                	</div>
			                	</div>
			                {/if}
		                	<div class="panel panel-default">
								<div class="panel-heading">
									<h3 class="panel-title">	
										{$LANG.paymentmethod}
									</h3>
								</div>
								<div class="panel-body">
				                    <p>{$LANG.orderForm.preferredPaymentMethod}</p>
					                <div class="alert alert-success text-center large-text" role="alert">
					                    {$LANG.ordertotalduetoday}: &nbsp; <strong>{$total}</strong>
					                </div>
				                    <div class="row">
					                    <div class="col-md-12">
											<div class="form-group">
			                                	<label class="col-md-2 control-label">{$LANG.paymentmethod}</label>
												<div class="col-md-9">
													{foreach key=num item=gateway from=$gateways}
							                            <label class="radio-icheck">
							                                <input type="radio" name="paymentmethod" value="{$gateway.sysname}" class="icheck payment-methods{if $gateway.type eq "CC"} is-credit-card{/if}"{if $selectedgateway eq $gateway.sysname} checked{/if} />
							                                <span>{$gateway.name}</span>
							                            </label>
													{/foreach}
												</div>
											</div>
					                    </div>
				                    </div>
								</div>
		                	</div>
			                <div id="creditCardInputFields"{if $selectedgatewaytype neq "CC"} class="hidden"{/if}>
    		                	<div class="panel panel-default">
									<div class="panel-heading">
										<h3 class="panel-title">	
											{$LANG.creditcard}
										</h3>
									</div>
									<div class="panel-body">
					                    {if $clientsdetails.cclastfour}
					                        <div class="row">
					                            <div class="col-md-12">
													<div class="form-group">
														<label class="col-md-2 control-label">{$LANG.creditcarddetails}</label>
														<div class="col-md-9">
						                                    <label class="radio-icheck">
						                                        <input type="radio" class="icheck" name="ccinfo" value="useexisting" id="useexisting" {if $clientsdetails.cclastfour} checked{else} disabled{/if} />
						                                        <span>
							                                        {$LANG.creditcarduseexisting}
							                                        {if $clientsdetails.cclastfour}
							                                            ({$clientsdetails.cclastfour})
							                                        {/if}
						                                        </span>
						                                    </label>
						                                    <label class="radio-icheck">
						                                        <input type="radio" class="icheck" name="ccinfo" value="new" id="new" {if !$clientsdetails.cclastfour || $ccinfo eq "new"} checked{/if} />
						                                        <span>{$LANG.creditcardenternewcard}</span>
						                                    </label>
														</div>
													</div>
					                            </div>
					                        </div>
					                    {else}
					                        <input type="hidden" name="ccinfo" value="new" />
							            {/if}
					                    <div id="newCardInfo" class="row{if $clientsdetails.cclastfour && $ccinfo neq "new"} hidden{/if}">
					                        <div class="col-md-12">
						                        <div class="form-group">
													<label for="cctype" class="col-md-2 control-label">{$LANG.creditcardcardtype}</label>
													<div class="col-md-9">
														<select name="cctype" id="cctype" class="form-control newccinfo">
															{foreach $acceptedcctypes as $cardType}
				                                                <option{if $cctype eq $cardType} selected{/if}>{$cardType}</option>
				                                            {/foreach}
				                                        </select>
				                                    </div>
												</div>
						                        <div class="form-group">
					                                <label for="inputCardNumber" class="control-label col-md-2">{$LANG.orderForm.cardNumber}</label>
													<div class="col-md-9">
						                                <input type="tel" name="ccnumber" id="inputCardNumber" class="form-control" placeholder="{$LANG.orderForm.cardNumber}" autocomplete="cc-number">
													</div>
					                            </div>
											</div>
					                        {if $showccissuestart}
					                            <div class="col-md-6">
					                                <div class="form-group">
						        						<label for="inputCardStart" class="col-md-4 control-label">{$LANG.creditcardcardstart}</label>
						                                <div class="col-md-6">
							                                <input type="tel" name="ccstartdate" id="inputCardStart" class="form-control" placeholder="MM / YY" autocomplete="cc-exp">
						                                </div>
					                                </div>
					                            </div>
					                            <div class="col-md-6">
					                                <div class="form-group">
					                                    <label for="inputCardIssue" class="col-md-4 control-label">{$LANG.creditcardcardissuenum}</label>
						                                <div class="col-md-6">
						                                    <input type="tel" name="ccissuenum" id="inputCardIssue" class="form-control" placeholder="{$LANG.creditcardcardissuenum}">
						                                </div>
					                                </div>
					                            </div>
											{/if}
					                        <div class="col-md-6">
					                            <div class="form-group">
					                                <label for="inputCardExpiry" class="col-md-4 control-label">{$LANG.creditcardcardexpires}</label>
					                                <div class="col-md-6">
						                                <input type="tel" name="ccexpirydate" id="inputCardExpiry" class="form-control" placeholder="MM / YY" autocomplete="cc-exp">
					                                </div>
					                            </div>
					                        </div>
					                        <div class="col-md-6">
					                            <div class="form-group">
					                                <label for="inputCardCVV" class="col-md-4 control-label">{$LANG.orderForm.cvv}</label>
					                                <div class="col-md-6">
						                                <input type="tel" name="cccvv" id="inputCardCVV" class="form-control" placeholder="{$LANG.orderForm.cvv}" autocomplete="cc-cvc">
														<a class="btn btn-sm btn-link btn-cvv" data-toggle="popover" data-content="<img src='{$BASE_PATH_IMG}/ccv.gif' width='210' />" data-placement="bottom">
															{$LANG.creditcardcvvwhere}
														</a>
					                                </div>
					                            </div>
					                        </div>
					                    </div>
					                    <div id="existingCardInfo" class="row{if !$clientsdetails.cclastfour || $ccinfo eq "new"} hidden{/if}">
					                        <div class="col-md-12">
						                        <div class="form-group">
													<label for="cctype" class="col-md-2 control-label">{$LANG.orderForm.cvv}</label>
					                                <div class="col-md-9">
														<input type="tel" name="cccvvexisting" id="inputCardCVV" class="form-control" placeholder="{$LANG.orderForm.cvv}" autocomplete="cc-cvc">
														<a class="btn btn-sm btn-link btn-cvv" data-toggle="popover" data-content="<img src='{$BASE_PATH_IMG}/ccv.gif' width='210' />" data-placement="bottom">
															{$LANG.creditcardcvvwhere}
														</a>
					                                </div>
												</div>							                        
					                        </div>
					                    </div>
					                </div>
				                </div>				
			                </div>
			                {if $shownotesfield}
			                	<div class="panel panel-default">
									<div class="panel-heading">
										<h3 class="panel-title">	
											{$LANG.orderForm.additionalNotes}
										</h3>
									</div>
									<div class="panel-body">
					                    <div class="row">
					                        <div class="col-md-12">
					                            <div class="form-group">
						                            <label class="col-md-2 control-label">{$LANG.invoicesnotes}</label>
													<div class="col-md-9">
						                                <textarea name="notes" class="form-control" rows="6" placeholder="{$LANG.ordernotesdescription}">{$orderNotes}</textarea>
													</div>
					                            </div>
					                        </div>
					                    </div>
									</div>
			                	</div>
			                {/if}
		                    {if $accepttos}
		                    	<div class="panel panel-default">
									<div class="panel-heading">
										<h3 class="panel-title">	
											{$LANG.ordertos}
										</h3>
									</div>
									<div class="panel-body">
				                        <p>
				                            <label class="checkbox-inline">
				                                <input type="checkbox" name="accepttos" id="accepttos" class="icheck" />
				                                &nbsp;
				                                {$LANG.ordertosagreement}
				                                <a href="{$tosurl}" target="_blank">{$LANG.ordertos}</a>
				                            </label>
				                        </p>
									</div>
		                    	</div>
		                    {/if}
			                <div class="text-center">
		                    	<br />
		                    	<br />
								<button type="submit" id="btnCompleteOrder" class="btn btn-primary btn-3d btn-lg"{if $cartitems==0} disabled="disabled"{/if} onclick="this.value='{$LANG.pleasewait}'">
			                        {$LANG.completeorder}
		                        	&nbsp;<i class="fa fa-arrow-circle-right"></i>
								</button>
								<br /><br /><br />
							</div>
							<div class="alert alert-warning checkout-security-msg">
			                	<i class="fa fa-lock"></i>
								{$LANG.ordersecure} (<strong>{$ipaddress}</strong>) {$LANG.ordersecure2}
							</div>
						</div>
					</div>
            	</form>
			    <!-- Start Modal: loginModal  -->
			    <form action="{if $systemsslurl}{$systemsslurl}{else}{$systemurl}{/if}dologin.php" method="post" role="form">
					<div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="loginModalLabel">
						<div class="modal-dialog model-lg" role="document">
					    	<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
									<h4 class="modal-title" id="loginModalLabel">{$LANG.orderForm.existingCustomerLogin}</h4>
					      		</div>
						  		<div id="modal-load" class="modal-body modal-body-touch">
						  			<div class="form-group">
										<div class="input-group">
											<span class="input-group-addon" id="basic-addon1"><i class="glyphicon glyphicon-user"></i></span>
											<input type="email" name="username" class="form-control" id="inputEmail" placeholder="{$LANG.enteremail}" autofocus>
										</div>
									</div>
									<div class="form-group">
										<div class="input-group">
											<span class="input-group-addon" id="basic-addon1"><i class="glyphicon glyphicon-lock"></i></span>
											<input type="password" name="password" class="form-control" id="inputPassword" placeholder="{$LANG.clientareapassword}" autocomplete="off">
										</div>
									</div>
									<div class="form-group">
										<div class="checkbox">
											<label>
												<input type="checkbox" name="rememberme" /> {$LANG.loginrememberme}
											</label>
										</div>
									</div>
					      		</div>
						  		<div class="modal-footer">
					  				<input id="login" type="submit" class="pull-left btn btn-primary btn-3d" value="{$LANG.loginbutton}" />
									<a style="margin-top: 10px;" class="pull-right" href="{$WEB_ROOT}/pwreset.php">{$LANG.forgotpw}</a>
								</div>
					    	</div>
						</div>
					</div>
	            </form>
				<!-- End Modal: loginModal  -->
				{if $itCartSidebarDisplay eq "enabled"}
			</div>
			<div class="col-md-3 pull-md-right whmcs-sidebar hidden-xs hidden-sm sidebar-secondary cart-sidebar">
				{include file="orderforms/control_standard/sidebar-categories.tpl"}
			</div>
			<div class="clearfix"></div>
		</div>
	{/if}
	<div class="clearfix"></div>
</section>	    
<script type="text/javascript" src="{$BASE_PATH_JS}/jquery.payment.js"></script>
{include file="orderforms/control_standard/icheck.tpl"}