<?php

$lang['prev_step']             =   'Prev Step';
$lang['next_step']             =   'Next Step'; 
$lang['step1_heading']         =   'Step 1';
$lang['step1_description']     =   'Complete the wizard by entering details and then clicking next on each page. On the last page click Create Virtual Machine to finish the process.';
$lang['step2_heading']         =   'Step 1';
$lang['step2_description']     =   'Complete the wizard by entering details and then clicking next on each page. On the last page click Create Virtual Machine to finish the process.';
$lang['step3_heading']         =   'Step 1';
$lang['step4_description']     =   'Complete the wizard by entering details and then clicking next on each page. On the last page click Create Virtual Machine to finish the process.';
$lang['step4_heading']         =   'Step 1';
$lang['step4_description']     =   'Complete the wizard by entering details and then clicking next on each page. On the last page click Create Virtual Machine to finish the process.'; 
$lang['already_registered']    =   'Already Registered';
$lang['search']                =   'Search';
$lang['choose_product']        =   'Choose Product:';
$lang['current_product_group'] =   'Current product group:';
$lang['change']                =   'Change';
$lang['default_configuration'] =   'Configure';
$lang['default_description']   =   'Setup product configuration';
$lang['confirmation']          =   'Confirmation';
