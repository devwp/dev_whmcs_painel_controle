<div class="row">
	<div class="col-md-6 col-md-offset-3">
		<div class="panel panel-danger">
			<div class="panel-heading">
				<h3 class="panel-title">{$LANG.subaccountpermissiondenied}</h3>
			</div>
			<div class="panel-body">
				{if !empty($allowedpermissions)}
					<p>{$LANG.subaccountallowedperms}</p>
					<ul>
						{foreach from=$allowedpermissions item=permission}
							<li>{$permission}</li>
						{/foreach}
					</ul>
				{/if}
				<p>{$LANG.subaccountcontactmaster}</p>
			</div>
			<div class="panel-footer">
				<a href="javascript:history.go(-1)" class="btn btn-primary btn-3d res-100">
					<i class="fa fa-arrow-circle-left"></i>
					{$LANG.goback}
				</a>
				<a href="index.php" class="btn btn-default pull-right res-left res-100">
					<i class="fa fa-home"></i>
					{$LANG.returnhome}
				</a>
			</div>
		</div>
	</div>
</div>