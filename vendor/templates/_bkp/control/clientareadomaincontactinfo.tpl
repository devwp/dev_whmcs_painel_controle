{include file="$template/includes/alert.tpl" type="info" msg=$LANG.whoisContactWarning}
{if $successful}
	{include file="$template/includes/alert.tpl" type="success" msg=$LANG.changessavedsuccessfully textcenter=true}
{/if}
{if $error}
	{include file="$template/includes/alert.tpl" type="error" msg=$error textcenter=true}
{/if}
<form method="post" action="{$smarty.server.PHP_SELF}?action=domaincontacts" class="form-horizontal">
	<input type="hidden" name="sub" value="save" />
	<input type="hidden" name="domainid" value="{$domainid}" />
	{foreach from=$contactdetails name=contactdetails key=contactdetail item=values}
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">{$contactdetail} {$LANG.supportticketscontact}</h3>
					</div>
					<div class="panel-body">
						<p>
							<label class="radio-inline">
								<input type="radio" class="icheck" name="wc[{$contactdetail}]" id="{$contactdetail}1" value="contact" onclick="useDefaultWhois(this.id)" /> {$LANG.domaincontactusexisting}
							</label>
						</p>
						<fieldset id="{$contactdetail}defaultwhois">
							<div class="form-group">
								<label class="col-sm-3 control-label" for="{$contactdetail}3">{$LANG.domaincontactchoose}</label>
								<div class="col-sm-6">
									<select class="form-control {$contactdetail}defaultwhois" name="sel[{$contactdetail}]" id="{$contactdetail}3" disabled>
										<option value="u{$clientsdetails.userid}">{$LANG.domaincontactprimary}</option>
										{foreach key=num item=contact from=$contacts}
											<option value="c{$contact.id}">{$contact.name}</option>
										{/foreach}
									</select>
								</div>
							</div>
						</fieldset>
						<p>
							<label class="radio-inline">
								<input type="radio" class="icheck" name="wc[{$contactdetail}]" id="{$contactdetail}2" value="custom" onclick="useCustomWhois(this.id)" checked /> {$LANG.domaincontactusecustom}
							</label>
						</p>
						<fieldset id="{$contactdetail}customwhois">
							{foreach key=name item=value from=$values}
								<div class="form-group">
									<label class="col-sm-3 control-label"	>{$name}</label>
									<div class="col-sm-6">
										<input type="text" name="contactdetails[{$contactdetail}][{$name}]" value="{$value}" class="form-control {$contactdetail}customwhois" />
									</div>
								</div>
							{/foreach}
						</fieldset>
					</div>
				</div>
			</div>
		</div>
	{/foreach}
	<div class="row">
		<div class="col-md-12">
			<input type="submit" value="{$LANG.clientareasavechanges}" class="res-100 btn btn-primary btn-3d" />
			<a href="clientarea.php?action=domaindetails&id={$domainid}" class="res-100 res-left pull-right btn btn-default">{$LANG.clientareabacklink}</a>
		</div>
	</div>
</form>