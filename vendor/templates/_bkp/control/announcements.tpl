{if $announcementsFbRecommend}
	<script>
		(function(d, s, id) {
			var js, fjs = d.getElementsByTagName(s)[0];
			if (d.getElementById(id)) {
				return;
			}
			js = d.createElement(s); js.id = id;
			js.src = "//connect.facebook.net/{$LANG.locale}/all.js#xfbml=1";
			fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));
	</script>
{/if}
{foreach from=$announcements item=announcement}
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title"><a href="{if $seofriendlyurls}announcements/{$announcement.id}/{$announcement.urlfriendlytitle}.html{else}{$smarty.server.PHP_SELF}?id={$announcement.id}{/if}">{$announcement.title}</a></h3>
				</div>
				<div class="panel-body">
					<p><i>{$announcement.timestamp|date_format:"%A, %B %e, %Y"}</i></p>
					<p>{$announcement.text|strip_tags|truncate:400:"..."}</p>
					{if strlen($announcement.text)>400}
						<br />
						<p><a class="btn btn-3d btn-primary" href="{if $seofriendlyurls}announcements/{$announcement.id}/{$announcement.urlfriendlytitle}.html{else}{$smarty.server.PHP_SELF}?id={$announcement.id}{/if}">{$LANG.more} &raquo;</a></p>
					{/if}
					{if $facebookrecommend}
						<div class="fb-like" data-href="{$systemurl}{if $seofriendlyurls}announcements/{$announcement.id}/{$announcement.urlfriendlytitle}.html{else}announcements.php?id={$announcement.id}{/if}" data-send="true" data-width="450" data-show-faces="true" data-action="recommend"></div>
					{/if}
				</div>
			</div>
		</div>
	</div>
{foreachelse}
	{include file="$template/includes/alert.tpl" type="warning" msg=$LANG.announcementsnone textcenter=true}
{/foreach}
<br />
{if $prevpage || $nextpage}
	<div class="row">
		<div class="col-md-12">
			<!---pagination--->
			<div class="btn-group btn-group-justified hidden-xs">
				<div class="btn-group">
					<a class="btn btn-default" href="{if $prevpage}announcements.php?page={$prevpage}{else}javascript:return false;{/if}">&larr; {$LANG.previouspage}</a>
				</div>
				<div class="btn-group">
					<button type="button" class="btn btn-primary">{$LANG.page} {$pagenumber} {$LANG.pageof} {$totalpages}</button>
				</div>
				<div class="btn-group">
					<a class="btn btn-default" href="{if $nextpage}announcements.php?page={$nextpage}{else}javascript:return false;{/if}">{$LANG.nextpage} &rarr;</a>
				</div>
			</div>
			<!---end pagination--->
			<!---mobile pagination--->
			<div class="btn-group btn-group-justified visible-xs">
				<div class="btn-group">
					<a class="btn btn-default" href="{if $prevpage}announcements.php?page={$prevpage}{else}javascript:return false;{/if}">{$LANG.previouspage}</a>
				</div>
				<div class="btn-group">
					<button type="button" class="btn btn-primary">{$pagenumber} {$LANG.pageof} {$totalpages}</button>
				</div>
				<div class="btn-group">
					<a class="btn btn-default" href="{if $nextpage}announcements.php?page={$nextpage}{else}javascript:return false;{/if}">{$LANG.nextpage}</a>
				</div>
			</div>
			<!---end mobile pagination--->
		</div>
	</div>
{/if}