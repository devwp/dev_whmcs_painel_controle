{*		
		Impressive Themes
		http://www.impressivethemes.net
		-------------------------------
		
		Featured Content Output for Control Theme by Impressive Themes
		--------------------------------------------------------------
		
*}
{* 1.	clientareahome.tpl / clientarea.php *}
		{if $templatefile == 'clientareahome'}
			<!--tiles start-->
			<div class="row">
				<div class="col-md-4 col-sm-12">
					<a href="clientarea.php?action=products">
						<div class="dashboard-tile detail tile-blue">
							<div class="content">
								<h1 class="text-left timer" data-from="0" data-to="{$clientsstats.productsnumactive}" data-speed="2500"> </h1>
								<p>{$LANG.navservices}</p>
							</div>
							<div class="icon"><i class="fa fa-cube"></i>
							</div>
						</div>
					</a>
				</div>
				<div class="col-md-4 col-sm-12">
					<a href="supporttickets.php">
						<div class="dashboard-tile detail tile-purple">
							<div class="content">
								<h1 class="text-left timer" data-to="{$clientsstats.numactivetickets}" data-speed="2500"> </h1>
								<p>{$LANG.supportticketspagetitle}</p>
							</div>
							<div class="icon"><i class="fa fa-comments"></i>
							</div>
						</div>
					</a>
				</div>
				<div class="col-md-4 col-sm-12">
					<a href="clientarea.php?action=invoices">
						<div class="dashboard-tile detail tile-turquoise">
							<div class="content">
								<h1 class="text-left ">{$clientsstats.creditbalance} </h1>
								<p>{$LANG.statscreditbalance}</p>
							</div>
							<div class="icon"><i class="fa fa-dollar"></i>
							</div>
						</div>
					</a>
				</div>
			</div>
			<!--tiles end-->
		{/if}