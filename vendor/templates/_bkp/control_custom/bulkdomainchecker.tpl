<div class="row">
	<div class="col-md-12">
		{if $searchingFor == 'register'}
			<p>{$LANG.domainbulksearchintro}</p>
		{else}
			<p>{$LANG.domainbulktransferdescription}</p>
		{/if}
		{if $invalidCaptcha}
			{include file="$template/includes/alert.tpl" type="danger" msg="{lang key=$invalidCaptchaError}" textcenter=true}
		{/if}
		<form method="post" action="domainchecker.php?search=bulk{if $searchingFor == 'transfer'}transfer{/if}">
			<textarea name="bulkdomains" id="inputBulkDomains" rows="8" class="form-control margin-bottom-5">{$bulkdomains}</textarea>
			<br />
			<button type="submit" id="btnCheckAvailability" class="btn btn-3d btn-primary btn-block">{$LANG.checkavailability}</button>
			{include file="$template/includes/captcha.tpl"}
		</form>
		<br />
		{if $invalidDomainsOmitted}
			{include file="$template/includes/alert.tpl" type="warning" msg="{$LANG.domaincheckerbulkinvaliddomain}" textcenter=true}
		{/if}
		{if $bulkCheckResults}
			{include file="$template/domainchecker-results.tpl"}
		{else}
			{if !$loggedin && $currencies && !$performingLookup}
				<div class="currencychooser pull-right clearfix margin-bottom">
					<div class="btn-group" role="group">
						{foreach from=$currencies item=curr}
							<a href="domainchecker.php?currency={$curr.id}" class="btn btn-default{if $currency.id eq $curr.id} active{/if}">
								<img src="{$BASE_PATH_IMG}/flags/{if $curr.code eq "AUD"}au{elseif $curr.code eq "CAD"}ca{elseif $curr.code eq "EUR"}eu{elseif $curr.code eq "GBP"}gb{elseif $curr.code eq "INR"}in{elseif $curr.code eq "JPY"}jp{elseif $curr.code eq "USD"}us{elseif $curr.code eq "ZAR"}za{else}na{/if}.png" border="0" alt="" />
								{$curr.code}
							</a>
						{/foreach}
					</div>
				</div>
				<div class="clearfix"></div>
			{/if}
			<div id="pricingTable"{if $performingLookup} class="hidden"{/if}>
				<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
					{foreach $tldcategories as $tldCategory}
						<div class="panel panel-default">
							<div class="panel-heading" role="tab" id="heading{$tldCategory->id}">
								<h4 class="panel-title">
									<a data-toggle="collapse" data-parent="#accordion" href="#collapse{$tldCategory->id}" aria-expanded="{if $tldCategory@first}true{else}false{/if}" aria-controls="collapse{$tldCategory->id}" class="domain-tld-pricing-category">
										{$tldCategory->category} {$LANG.tldpricing}
									</a>
								</h4>
							</div>
							<div id="collapse{$tldCategory->id}" class="panel-collapse collapse{if $tldCategory@first} in{/if}" role="tabpanel" aria-labelledby="heading{$tldCategory->id}">
								<table class="table table-framed">
									<thead>
										<tr>
											<th class="indent">{$LANG.domaintld}</th>
											<th class="">{$LANG.domainminyears}</th>
											<th class="">{$LANG.domainsregister}</th>
											<th class="">{$LANG.domainstransfer}</th>
											<th class="">{$LANG.domainsrenew}</th>
										</tr>
									</thead>
									<tbody>
										{foreach $tldCategory->topLevelDomains as $tld}
											<tr>
												<td class="indent">{$tld->tld}</td>
												<td class="">{$tldpricing.{$tld->tld}.period}</td>
												<td class="">{if $tldpricing.{$tld->tld}.register}{$tldpricing.{$tld->tld}.register}{else}{$LANG.domainregnotavailable}{/if}</td>
												<td class="">{if $tldpricing.{$tld->tld}.transfer}{$tldpricing.{$tld->tld}.transfer}{else}{$LANG.domainregnotavailable}{/if}</td>
												<td class="">{if $tldpricing.{$tld->tld}.renew}{$tldpricing.{$tld->tld}.renew}{else}{$LANG.domainregnotavailable}{/if}</td>
											</tr>
										{/foreach}
									</tbody>
								</table>
							</div>
						</div>
					{/foreach}
					<div class="panel panel-default">
						<div class="panel-heading" role="tab" id="heading{$tldCategory->id}">
							<h4 class="panel-title">
								<a data-toggle="collapse" data-parent="#accordion" href="#collapseAll" aria-expanded="false" aria-controls="collapseAll" class="domain-tld-pricing-category">
									{$LANG.alltldpricing}
								</a>
							</h4>
						</div>
						<div id="collapseAll" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingAll">
							<table class="table table-framed">
								<thead>
									<tr>
										<th class="indent">{$LANG.domaintld}</th>
										<th class="">{$LANG.domainminyears}</th>
										<th class="">{$LANG.domainsregister}</th>
										<th class="">{$LANG.domainstransfer}</th>
										<th class="">{$LANG.domainsrenew}</th>
									</tr>
								</thead>
								<tbody>
									{foreach $tldpricelist as $tld}
										<tr>
											<td class="indent">{$tld.tld}</td>
											<td class="">{$tld.period}</td>
											<td class="">{if $tld.register}{$tld.register}{else}{$LANG.domainregnotavailable}{/if}</td>
											<td class="">{if $tld.transfer}{$tld.transfer}{else}{$LANG.domainregnotavailable}{/if}</td>
											<td class="">{if $tld.renew}{$tld.renew}{else}{$LANG.domainregnotavailable}{/if}</td>
										</tr>
									{/foreach}
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			{if !$loggedin && $currencies && !$performingLookup}
				<div class="currencychooser pull-right clearfix margin-bottom">
					<div class="btn-group" role="group">
						{foreach from=$currencies item=curr}
							<a href="domainchecker.php?currency={$curr.id}" class="btn btn-default{if $currency.id eq $curr.id} active{/if}">
								<img src="{$BASE_PATH_IMG}/flags/{if $curr.code eq "AUD"}au{elseif $curr.code eq "CAD"}ca{elseif $curr.code eq "EUR"}eu{elseif $curr.code eq "GBP"}gb{elseif $curr.code eq "INR"}in{elseif $curr.code eq "JPY"}jp{elseif $curr.code eq "USD"}us{elseif $curr.code eq "ZAR"}za{else}na{/if}.png" border="0" alt="" />
								{$curr.code}
							</a>
						{/foreach}
					</div>
				</div>
				<div class="clearfix"></div>
			{/if}
		{/if}
	</div>
</div>
<script>
	var langSearch = '{$LANG.search}';
	var langAdding = '{$LANG.domaincheckeradding}';
	var langAdded = '{$LANG.domaincheckeradded}';
	var langUnavailable = '{$LANG.domainunavailable}';
	var langBulkPlaceholder = '{$LANG.domaincheckerbulkplaceholder|escape:'quotes'|replace:"\n":'\n'}';
</script>
<script src="templates/{$template}/js/domainchecker.js"></script>
{include file="$template/includes/modal.tpl" name="Whois" title=$LANG.whoisresults|cat:' <span id="whoisDomainName"></span>'}