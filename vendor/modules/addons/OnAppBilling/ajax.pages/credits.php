<?php



/**
 * @author Mariusz Miodowski <mariusz@modulesgarden.com>
 */

$pagination = new MG_Pagination("OnAppBilling_Credits");

$row = mysql_get_row("SELECT COUNT(OnAppBilling_user_credits.hosting_id) as `count`  FROM OnAppBilling_user_credits");
$pagination->setAmount($row['count']);

$clients = mysql_get_array("SELECT uc.*, c.firstname, c.lastname, h.domain 
    FROM OnAppBilling_user_credits uc
    LEFT JOIN tblclients c ON uc.user_id = c.id
    LEFT JOIN tblhosting h ON uc.hosting_id = h.id
".$pagination->getLimitAndOffset());