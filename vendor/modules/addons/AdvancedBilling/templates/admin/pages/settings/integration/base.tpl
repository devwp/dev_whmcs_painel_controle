<div class="panel panel-primary">
    <div class="panel-body">{$MGLANG->T('In order to enable you customers to monitor resource usage in product details and pricing in product configuration please do following instruction')}.</div>
</div>

<div class="panel panel-primary">
    <div class="panel-heading">
        <h3 class="panel-title">{$MGLANG->T('Display product pricing in product configuration')}</h3>
    </div>
    <div class="panel-body">
        <div class="control-group">
            {$MGLANG->T('In order to enable usage record pricing on order form, open the file')}:        
        </div>
        <pre>your_whmcs/templates/orderforms/modern/configureproduct.tpl</pre>
        <div class="control-group">
           {$MGLANG->T('Find the following line')}:         
        </div>
        <pre>{literal}{if $productinfo.type eq "server"}{/literal}</pre>
        <div class="control-group">
            {$MGLANG->T('Add this code Before the line')}:
        </div>
        <pre>{literal}{$AdvancedBilling_Integration_Code}{/literal}</pre>
    </div>
</div>

    
<div class="panel panel-primary">
    <div class="panel-heading">
        <h3 class="panel-title">{$MGLANG->T('Display usage records')}</h3>
    </div>
    <div class="panel-body">
        <ul id="detailsNav" class="nav nav-tabs" role="tablist">
            <li role="presentation" class="">
                <a href="#detailsV6Tab" aria-controls="detailsV6Tab" role="tab" data-toggle="tab">WHMCS 6</a>
            </li>
            <li role="presentation">
                <a href="#detailsV5Tab" aria-controls="detailsV5Tab" role="tab" data-toggle="tab">WHMCS 5</a>
            </li>
        </ul>
        
        <div id="detailsContent" class="tab-content">
            <div id="detailsV6Tab" role="tabpanel" class="tab-pane active">
                <div class="control-group">
                    {$MGLANG->T('In order to enable your customers to monitor resource usage, open the file')}:        
                </div>
                <pre>your_whmcs/templates/six/clientareaproductdetails.tpl</pre>
                <div class="control-group">
                    {$MGLANG->T('Find the following line')}:         
                </div>

                <pre>&lt;div class="tab-pane fade in" id="tabDownloads"&gt;</pre>
                <div class="control-group">
                    {$MGLANG->T('Add this code Before the line')}:
                </div>
                <pre>{literal}{$AdvancedBilling_Integration_Code}{/literal}</pre>
            </div>

            <div id="detailsV5Tab" role="tabpanel" class="tab-pane">
                <div class="control-group">
                    {$MGLANG->T('In order to enable your customers to monitor resource usage, open the file')}:        
                </div>
                <pre>your_whmcs/templates/default/clientareaproductdetails.tpl</pre>
                <div class="control-group">
                    {$MGLANG->T('Find the following line')}:         
                </div>

                <pre>{literal}{if $moduleclientarea}&lt;div class="moduleoutput"&gt;{$moduleclientarea|replace:'modulebutton':'btn'}&lt;/div&gt;{/if}{/literal}</pre>
                <div class="control-group">
                    {$MGLANG->T('Add this code Before the line')}:
                </div>
                <pre>{literal}{$AdvancedBilling_Integration_Code}{/literal}</pre>
            </div>
        </div>

    </div>
</div>