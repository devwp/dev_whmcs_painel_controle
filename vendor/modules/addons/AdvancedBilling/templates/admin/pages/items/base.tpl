<div class="panel panel-primary">
    <div class="panel-body">{$MGLANG->T('Item list will show you current usage counted by the Advanced Billing module for accounts')}.</div>
</div>

<div id="MGPanel" class="panel panel-primary">
    <div class="panel-heading">
        {if $displayForProduct}
            <h3 class="panel-title"  style="line-height: 28px;">
                {$MGLANG->T('Account List')} {$MGLANG->T('For Product')} <b style="color: #fff">{$productName}</b>
                <div class="pull-right">
                    <a class="btn btn-info" href="addonmodules.php?module=AdvancedBilling&mg-page=items">{$MGLANG->T('Show All')}</a>
                </div>
            </h3>
        {else}
            <h3 class="panel-title">{$MGLANG->T('Account List')}</h3>
        {/if}
    </div>
  <div class="panel-body">      
    <table id="accountTable" class="table table-hover">
        <thead>
            <td>{$MGLANG->T('Hosting')}</td>
            <td>{$MGLANG->T('Product Name')}</td>
            <td>{$MGLANG->T('Client Name')}</td>
            <td>{$MGLANG->T('Total Amount')}</td>
            <td>{$MGLANG->T('Last Update')}</td>
            <td>{$MGLANG->T('Actions')}</td>
        </thead>
        <tbody>
            {if $accounts}
                {foreach from=$accounts item=account}
                    <tr name="{$account->hosting->id}">
                        <td><a href="clientsservices.php?userid={$account->client->id}&id={$account->hosting->id}">#{$account->hosting->id} {$account->hosting->domain}</a></td>
                        <td><a href="configproducts.php?action=edit&id={$account->product->id}">{$account->product->name}</a></td>
                        <td><a href="clientssummary.php?userid={$account->client->id}">{$account->client->firstname} {$account->client->lastname}</td>
                        <td>{$currency->prefix}{$account->totalAmount}{$currency->suffix}</td>
                        <td>{$account->lastUpdate}</td>
                        <td>
                            <a class="openItemsDetailsBtn btn btn-info btn-inverse" data-hostingid="{$account->hosting->id}" href="addonmodules.php?module=AdvancedBilling&mg-page=items&mg-action=details&hostingId={$account->hosting->id}&productId={$account->product->id}">{$MGLANG->T('View Details')}</a>
                            <a class="openConfirmModalBtn btn btn-danger btn-inverse" data-hostingid="{$account->hosting->id}">{$MGLANG->T('Delete Records')}</a>
                        </td>
                    </tr>
                {/foreach}
            {else}
                <td colspan="6">{$MGLANG->T('Nothing to display')}</td>
            {/if}
        </tbody>
    </table>
  </div>
</div>

<div id="confirmModal" class="modal fade" role="dialog">
    <div class="modal-content" style="top: 25%; width: 20%; left: 40%;">
        <div class="modal-header">
            
        </div>
        <div class="modal-body">
            <h3><b>{$MGLANG->T('Are you sure?')}</b></h3>
            <span class="help-block">
                {$MGLANG->T('All currently generated records will be deleted. This action can NOT be undone!')}
            </span>
        </div>
        <div class="modal-footer">
            <button id="deleteItemsDetailsBtn" type="button" data-dismiss="modal" class="btn btn-primary">{$MGLANG->T('Delete')}</button>
            <button type="button" data-dismiss="modal" class="btn btn-danger btn-inverse">{$MGLANG->T('Cancel')}</button>
        </div>
    </div>
</div>
            
{literal}
<script type="text/javascript">
    
    var hostingId;
    $(".openConfirmModalBtn").click(function(){
        hostingId = $(this).data("hostingid");
        
        $("#confirmModal").modal('show');
    });
    
    $("#deleteItemsDetailsBtn").click(function(e)
    {
        postAJAX("items|deleteItems", {'hostingId' : hostingId}, 'json', "resultMessage", function(result){
            if(result == "success")
            {              
                var rows = $("#accountTable tbody tr").length;
                if(rows == 1)
                {
                    $("tr[name='"+hostingId+"']").html({/literal}'<td colspan="6">{$MGLANG->T('Nothing to display')}</td>'{literal});
                }
                else
                {
                    $("tr[name='"+hostingId+"']").html('');
                }
            }
        });
    });
    
</script>
{/literal}