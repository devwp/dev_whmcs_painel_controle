jQuery(document).ready(function() {
    jQuery("a.atooltip").tooltip();

    var attachedrules = [];
    jQuery("#sortable").sortable({
        axis: 'y',
        cancel: 'tr:not(.info)',
        forceHelperSize: true,
        activate: function(event, ui) {

            var w1 = jQuery("#rulestable tr:first th:first").width();
            var w2 = jQuery("#rulestable tr:first th:last").width();

            ui.helper.find("td:first").width(w1);
            ui.helper.find("td:last").width(w2);

            var group = ui.item;
            var rules = (group.next("tr")).nextUntil("tr.info", "tr");
            attachedrules = rules;
            rules.hide();
        },
        sort: function(event, ui) {
            if ((jQuery(ui.placeholder.prev("tr.info")).length && jQuery(ui.placeholder.next("tr:not(.info)")).length) ||
                    (jQuery(ui.placeholder.prev("tr:not(.info)")).length && jQuery(ui.placeholder.next("tr:not(.info)")).length)) {
                ui.placeholder.hide(0);
            } else {
                ui.placeholder.show(0);
            }
        },
        deactivate: function(event, ui) {
            var cancel = false;

            if (ui.item.prev("tr.info").length && ui.item.next("tr:not(.info)").length) {
                jQuery("#sortable").sortable("cancel");
                cancel = true;
            }
            if (ui.item.prev("tr:not(.info)").length && ui.item.next("tr:not(.info)").length) {
                jQuery("#sortable").sortable("cancel");
                cancel = true;
            }

            jQuery.each(attachedrules, function() {
                if (!cancel) {
                    jQuery(this).insertAfter(ui.item);
                }
                jQuery(this).show();
            });

            if (cancel === false) {
                var sorted = jQuery("#sortable").sortable("toArray");
                jQuery.post("addonmodules.php?module=serverswitcher&modpage=ssconfig&modsubpage=setSort", {
                    'ajax': 1,
                    'sorted': sorted
                }, function(data) {
                });
            }

            jQuery("tr.last").appendTo(jQuery("tr.last").parent("tbody"));
            attachedrules = [
            ];
        }
    });
    jQuery("#sortable").disableSelection();
});
jQuery(function() {

    // add group btn
    jQuery('div.inner').on('click', '#addgroup', function(e) {
        e.preventDefault();

        var gform = jQuery("#addgroupform");
        var rform = jQuery("#addruleform");

        if (gform.is(':visible')) {
            gform.hide();
            gform.find("#inputName").val("");
            gform.find("#inputProductg").val("");
            gform.find("#inputServer").val("");

            jQuery("#addgroup.btn.btn-info").show();
        } else {
            jQuery("#serverhint").remove();
            jQuery("#inputServer").show(0);
            jQuery("#addgroupbtn").attr("disabled", false);
            gform.show();

            jQuery(this).hide();
        }
    });


    jQuery('div.inner').on('change', '#inputProductg', function(e) {
        var productGroups = jQuery(this).val();

        jQuery("#serverhint").remove();
        jQuery("#inputServer").show(0);
        jQuery("#addgroupbtn").attr("disabled", false);
        jQuery("#inputServer option").each(function() {
            jQuery(this).remove();
        });

        jQuery.post("addonmodules.php?module=serverswitcher&modpage=ssconfig&modsubpage=getServersAjax", {
            'ajax': 1,
            'productGroups': productGroups
        }, function(data) {
            if (typeof (data) !== 'object') {
                var data = JSON.parse(data);
            }

            if (!data.options.length) {
                jQuery("#inputServer").hide(0);
                jQuery("#addgroupbtn").attr("disabled", "disabled");
                jQuery("<span id=\"serverhint\">Please set server group to at least one product in the product group</span>").insertAfter("#inputServer");
                return;
            }

            jQuery.each(data.options, function(k, v) {
                jQuery("#inputServer").append('<option value="' + v.id + '">' + v.name + '</option>');
            });
        });
    });
    // save new group
    jQuery('div.inner').on('click', '#addgroupbtn', function(e) {
        e.preventDefault();

        var form = jQuery("#addgroupform");

        var name = form.find("#inputName").val();
        var pgroups = form.find("#inputProductg").val();
        var pservers = form.find("#inputServer").val();

        if ((err = validateName(name)) !== true) {
            showAlert(err, 'error', 5000);
            return;
        }

        if (!pgroups) {
            showAlert('Choose at least one product group', 'error', 5000);
            return;
        }

        jQuery.post('addonmodules.php?module=serverswitcher&modpage=ssconfig&modsubpage=savegroup', {
            'name': name,
            'groups': pgroups,
            'servers': pservers
        }, function(data) {
            if (typeof (data) === 'object') {
                if (data.status === 'success') {
                    form.hide();
                    form.find("#inputName").val("");
                    form.find("#inputProductg").val("");
                    form.find("#inputServer").val("");
                    jQuery("#addgroup.btn.btn-info").show();

                    if (jQuery("#errrow").length) {
                        jQuery("#errrow").remove();
                    }

                    var row = '<tr class="info"><input type="hidden" name="rulegruopid" class="rulegruopid" value="' + data.groupdetails.id + '" /><td colspan="1"><div class="row-fluid"><div class="span3">' + name + '</div>';
                    if (data.groupdetails.pgroups && data.groupdetails.pgroups !== "null") {
                        row += '<div class="span5"><ul class="inline"><li><strong>Product Groups:</strong></li>';
                        jQuery.each(data.groupdetails.pgroups, function(k, v) {
                            row += '<li><a href="configproducts.php?action=editgroup&ids=' + v.id + '">' + v.name + '</a></li>';
                        });
                    }
                    row += '</ul></div>';
                    if (data.groupdetails.servers && data.groupdetails.servers !== "null") {
                        row += '<div class="span4"><ul class="inline"><li><strong>Default Server:</strong></li>';
                        jQuery.each(data.groupdetails.servers, function(k, v) {
                            row += '<li><a href="configservers.php?action=manage&id=' + v.id + '">' + v.name + '</a></li>';
                        });
                        row += '</ul></div></div>';
                    }
                    row += '</td><td style="text-align: right;">' +
                            '<a class="addrule btn btn-success  secondbutton" href="#" >Add Rule</a>' +
                            '<a class="delrulegroup btn btn-danger  thirdbutton" href="#" >Remove Group</a>' +
                            '</td></tr>';

                    row += '<td colspan="2"> <p style="text-align: center;">No Rules In This Group.</p> </td>';

                    jQuery("#rulestable").append(row);
                    
                    updateProductsGroups();
                } else {

                }
            }
        });
    });


    // add rule
    jQuery('div.inner').on('click', '.addrule', function(e) {
        e.preventDefault();

        var row = jQuery(this).parents("tr");

        var rulegruopid = row.find(".rulegruopid").val();

        if (rulegruopid === '' || rulegruopid === 'undefined') {
            return;
        }

        document.location = 'addonmodules.php?module=serverswitcher&modpage=ssconfig&modsubpage=addrule&gid=' + rulegruopid;
    });
    // edit rulegroup
    jQuery('#rulestable').on('click', '.editrulegroup', function(e) {
        e.preventDefault();

        var row = jQuery(this).parents("tr");

        var rulegruopid = row.find(".rulegruopid").val();

    });
    // delete rulegroup
    jQuery('#rulestable').on('click', '.delrulegroup', function(e) {
        e.preventDefault();

        var row = jQuery(this).parents("tr");
        var name = jQuery(row.find("div.span3:first")).text();

        var rulegruopid = row.find(".rulegruopid").val();

        if (confirm("Are you sure you want to delete rule group: " + name + "?")) {

            jQuery.post("addonmodules.php?module=serverswitcher&modpage=ssconfig&modsubpage=delrulegroup", {
                'ajax': 1,
                'groupid': rulegruopid
            }, function(data) {
                if (typeof (data) === 'object') {
                    if (data.status === 'success') {
                        jQuery.each(row.nextUntil(".info"), function() {
                            jQuery(this).remove();
                        });
                        row.remove();

                        if (!jQuery("table#rulestable > tbody > tr").length) {
                            jQuery("table#rulestable > tbody").append('<tr id="errrow"><td colspan="2"><p style="text-align: center;">No Rules Found</p></td></tr>');
                        }
                        
                        updateProductsGroups();
                    }
                }
            });
        }
    });

    // edit rule
    jQuery('#rulestable').on('click', '.editrule', function(e) {
        e.preventDefault();

        var row = jQuery(this).parents("tr");

        var ruleid = row.find(".ruleid").val();

        if (ruleid === '' || ruleid === 'undefined') {
            return;
        }

        document.location = 'addonmodules.php?module=serverswitcher&modpage=ssconfig&modsubpage=editrule&id=' + ruleid;
    });
    // delete rule
    jQuery('#rulestable').on('click', '.delrule', function(e) {
        e.preventDefault();

        var row = jQuery(this).parents("tr");
        var grow = jQuery(row.prev("tr.info"));

        var ruleid = row.find(".ruleid").val();

        if (confirm("Are you sure you want to delete rule ?")) {

            jQuery.post("addonmodules.php?module=serverswitcher&modpage=ssconfig&modsubpage=delrule", {
                'ajax': 1,
                'ruleid': ruleid
            }, function(data) {
                if (typeof (data) === 'object') {
                    if (data.status === 'success') {
                        row.remove();

                        if (!(grow.nextUntil(".info")).length) {
                            norules = '<tr><td colspan="2"><p style="text-align: center;">No Rules In This Group.</p></td></tr>';
                            jQuery(norules).insertAfter(grow);
                        }
                    }
                }
            });
        }
    });

//    jQuery('div.inner').on('', '', function(e){
//        
//    });

    function updateProductsGroups() {
        
        var productsSelect = jQuery("#inputProductg");
        
        productsSelect.find("option").each(function () {
            jQuery(this).remove();
        });
        
        jQuery.post("addonmodules.php?module=serverswitcher&modpage=ssconfig", {
            'ajax' : 1,
            'modsubpage' : 'getProductsListAjax'
        }, function(data) {
            if(!jQuery.isEmptyObject(data.products)) {
                jQuery.each(data.products, function (k, v) {
                    productsSelect.append('<option value="' + k + '">' + v + '</option>');
                });
            }
        }, "json");
    }

    function showAlert(msg, type, duration) {

        var divalert = jQuery("div.alert");

        if(divalert.length) {
            divalert.remove();
        }

        var divalert = '<div class="alert alert-' + type + '" style="margin-top: 20px;"><button type="button" class="close" data-dismiss="alert">&times;</button>' + msg + '</div>';

        jQuery(divalert).insertAfter("div.inner p:first").delay(duration).queue(function(next) {
            jQuery(this).remove();
            next();
        });
    }

//    function showBootstrapModal(elemnt) {
//
//        var contentareaWidth = jQuery('.contentarea').width();
//
//        elemnt.css({
//            'width': '' + (contentareaWidth * 0.90),
//            'margin-top': -250,
//            'margin-left': -((contentareaWidth * 0.90) / 2) + 25
//        });
//
//        elemnt.modal('show');
//        elemnt.toggleClass("in").show();
//        jQuery('#mg-wrapper.body').append('<div class="modal-backdrop fade in"></div>');
//    }

    function validateName(name) {

        if (name.length < 3)
            return "Name of group is too short";
        
        if(name.length > 100) 
            return "Name of group is too long";

        if (!name.match(/^[a-zA-Z0-9_\.\s]*$/)) {
            return "Name of group is not valid!";
        }
        return true;
    }

});
