<?php

// Teste de envio NFS-e Prefeitura São Paulo

require('./NFSeSP.class.php');
require('./NFeRPS.class.php');

$cnpjPrestador = '01579377000100';
$ccmPrestador = '1044570';
$certDir = __DIR__.'/certificado/';
$passphrase = '8d04cik';
$pkcs12 = __DIR__.'/certificado/A1_2016_03_15.pfx';
$inscricaomunicipal = '1044570';
$valorservico = '1.00'; 
$valordeducao = '0';
$codigoservico = '01.07.00';
$aliquotaserivco = '0.0350';
$tributacao = 'T';
$discrimiServico = 'Suporte Técnico em TI Manutenção de Dados - 364JH46. Suporte Técnico em TI Manutenção de Dados - 4843K93. Suporte Técnico em TI Manutenção de Dados - 48JAP91. Suporte Técnico em TI Manutenção de Dados - 48NPI92. Suporte Técnico em TI Manutenção de Dados - 48RSD94. Suporte Técnico em TI Manutenção de Dados - 48TCA90. Suporte Técnico em TI Manutenção de Dados - 623WN54. Valor Aproximado dos Tributos Federais: R$ 7,63 (6,76%) Valor Aproximado dos Tributos Municipais: R$ 3,95 (3,50%) Fonte: SEBRAE';
$cnpjdestinatario = '10434078000178';
$tipodocumento = 'C';
$ccmdestinatario =  'Isento';
$nomedestinatario = 'Const Rio Engenharia e Comércio Ltda';
$tipoEndereco = "R";  // Rua
$endereco = 'R MAJOR EMIDIO DE CASTRO';
$enderecoNumero = '54';
$complemento = '';
$bairro =  'Vl. Santo Antonio';;
$cidade = '3550308';
$estado = 'SP';
$cep = '15014420';
$email = 'kelly.ito17@gmail.com';
$datarange['inicio'] =  date("Y-m-d");
$datarange['fim']   = date("Y-m-d");

$nfse = new NFSeSP($cnpjPrestador,$ccmPrestador,$certDir,$passphrase,$pkcs12,0);
//$ret = $nfse->queryCNPJ('');


$rps = new NFeRPS();

$rps->CCM = '1044570';  // inscriçãoo municipal da Empresa
$rps->serie = 'A';       // serie do RPS gerado
$rps->numero = '1';      // numero do RPS gerado

$rps->dataEmissao = date("Y-m-d");
$rps->valorServicos =  '1.00'; 
$rps->valorDeducoes =  '0';
echo '1';
$rps->codigoServico = '01.07.00';   // codigo do serviço executado
$rps->aliquotaServicos = '0.0350';
$rps->tributacao = "T";
$rps->discriminacao = 'Suporte Técnico em TI Manutenção de Dados - 364JH46. Suporte Técnico em TI Manutenção de Dados - 4843K93. Suporte Técnico em TI Manutenção de Dados - 48JAP91. Suporte Técnico em TI Manutenção de Dados - 48NPI92. Suporte Técnico em TI Manutenção de Dados - 48RSD94. Suporte Técnico em TI Manutenção de Dados - 48TCA90. Suporte Técnico em TI Manutenção de Dados - 623WN54. Valor Aproximado dos Tributos Federais: R$ 7,63 (6,76%) Valor Aproximado dos Tributos Municipais: R$ 3,95 (3,50%) Fonte: SEBRAE';
echo '2';
$rps->contractorRPS = new ContractorRPS();

$rps->contractorRPS->cnpjTomador = '10434078000178';
$rps->contractorRPS->ccmTomador = 'Isento';
$rps->contractorRPS->type = 'C';		// C=Pessoa Juridica, F=Pessoa Fisica
$rps->contractorRPS->name = 'MENSEG RIO PRETO CORRETORA DE SEGUROS LTDA EPP';
$rps->contractorRPS->tipoEndereco = "R";  // Rua
echo '3';
$rps->contractorRPS->endereco = 'R MAJOR EMIDIO DE CASTRO';
$rps->contractorRPS->enderecoNumero =  '54';
$rps->contractorRPS->complemento = 'complemento';
$rps->contractorRPS->bairro =  'Vl. Santo Antonio';;
$rps->contractorRPS->cidade = '3550308';
$rps->contractorRPS->estado = 'SP';
$rps->contractorRPS->cep = '15014420';
$rps->contractorRPS->email = 'kelly.ito17@gmail.com';
echo '4';
$rpsArray[] = $rps;

$rangeDate['inicio'] = date("Y-m-d");
$rangeDate['fim']   = date("Y-m-d");
$valorTotal['servicos'] = 1;
$valorTotal['deducoes'] = 0;

$ret = $nfse->sendRPS ($rps);
print_r($ret);
$docxml = $ret->saveXML();


echo "<br>\n";
print_r($docxml);
echo "<br>\n"; 

if ($ret->Cabecalho->Sucesso == "true") {
   if ($ret->Cabecalho->Alerta) {
      $errMsg = "Erro " . $ret->Cabecalho->Alerta->Codigo . " - ";
      $errMsg.=  utf8_decode($ret->Cabecalho->Alerta->Descricao);
   }

   if ($ret->Cabecalho->Erro) {
      $errMsg = "Erro " . $ret->Cabecalho->Erro->Codigo . " - ";
      $errMsg.=  utf8_decode($ret->Cabecalho->Erro->Descricao);
   }
} else {
   if ($ret->Cabecalho->Erro) {
      $errMsg = "Erro " . $ret->Cabecalho->Erro->Codigo . " - ";
      $errMsg.=  utf8_decode($ret->Cabecalho->Erro->Descricao); 
   } else {
      $errMsg = utf8_decode("Erro no processamento da solicitação");
   }
}

if ($errMsg == "") {
   // obtem dados da Nota Fiscal
   $NumeroNFe = trim($ret->ChaveNFeRPS->ChaveNFe->NumeroNFe);
   $CodVer   = trim($ret->ChaveNFeRPS->ChaveNFe->CodigoVerificacao);

   // Como a Prefeitura de São Paulo desconsidera os dados do destinatario que voce envia
   // e mantêm o que esta cadastrado no banco de dados deles...
   // Consulta NFS-e para acertar data / hora / Endereço do destinatario
   $ret = $nfse->queryNFe($NumeroNFe,0,'');
   if ($ret->Cabecalho->Sucesso) {
      $DtEmi = $ret->NFe->DataEmissaoNFe;
      if (strlen($DtEmi) == 19) {
         $HoraEmi = substr($DtEmi,11,2) . substr($DtEmi,14,2) . substr($DtEmi,17,2);
         $DataEmi = substr($DtEmi,0,4) . substr($DtEmi,5,2) . substr($DtEmi,8,2);
         $Tomador   = utf8_decode($ret->NFe->RazaoSocialTomador);
         $FavEmail  = $ret->NFe->EmailTomador;
         if ($FavEmail == "") {
            $FavEmail = "-----";
         }
         $FavRua    = $ret->NFe->EnderecoTomador->TipoLogradouro . " ";
         $FavRua   .= utf8_decode($ret->NFe->EnderecoTomador->Logradouro);
         $FavRua    = replace("'","`",$FavRua);
         $FavRuaNum = $ret->NFe->EnderecoTomador->NumeroEndereco;
         $FavRuaCpl = $ret->NFe->EnderecoTomador->ComplementoEndereco;
         $FavCep    = $ret->NFe->EnderecoTomador->CEP;
         if (strlen($FavCep) < 8) {
            $FavCep = str_repeat("0", 8 - strlen($FavCep)) . $FavCep;
         }
         $FavBairro = utf8_decode($ret->NFe->EnderecoTomador->Bairro);
         $FavBairro = replace("'","`",$FavBairro);
         $FavCidade = $ret->NFe->EnderecoTomador->Cidade;
         $FavEstado = $ret->NFe->EnderecoTomador->UF;
         $VrCredito = $ret->NFe->ValorCredito;
      }
      //
      // insira aqui sua rotina de atualização do banco de dados
      //
   }
}

?> 
