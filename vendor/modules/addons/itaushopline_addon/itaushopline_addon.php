<?php
/**
 * itaushopline
 *
 * Setting module for the itaushopline.
 *
 * @package    WHMCS
 * @author     webplus <suporte@webplus.com.br>
 * @copyright  Copyright (c) webplus 2016
 * @link       http://www.webplus.com.br
 */

if (!defined("WHMCS") && !isset($_GET['cron']))
    die("This file cannot be accessed directly");


if(isset($_GET['cron'])){
  require_once __DIR__ . '/../../../init.php';
  require_once __DIR__ . '/../../../includes/gatewayfunctions.php';
  require_once __DIR__ . '/../../../includes/invoicefunctions.php';
  if(isset($_GET['id']) && $_GET['id'] != '' ){
    $resultretornos = full_query('SELECT * FROM mod_itaushoplineretornos where processed = false AND retornoid = '.$_GET['id']);
    //echo 'SELECT * FROM mod_itaushoplineretornos where processed = false AND retornoid = '.$_GET['id'];
  } else {
    $resultretornos = full_query('SELECT * FROM mod_itaushoplineretornos where processed = false AND TipoRegistro = 1 limit 1');
  }
  $result = array();
  $retornos = array();
  $retornos1 = array();
  while($ret = mysql_fetch_array($resultretornos)){
		$retornos1[] = array(
      'AllLine'=>strlen($ret['AllLine']),
      'FileId'=>$ret['FileId'],
      'Line'=>$ret['Line'],
    );
    $retornos[]=$ret;
	}
  if(count($retornos)) {
    foreach ($retornos as  $ret) {
      full_query('UPDATE mod_itaushoplineretornos SET processed=true, DataehoraProcessed=NOW() where retornoid = '.$ret['retornoid']);
      $result = itaushopline_addon_darBaixa($ret);
      $debug .= $result['debug'];
      //full_query('UPDATE mod_itaushoplineretornos SET processed=true, DataehoraProcessed=NOW() where retornoid = '.$ret['retornoid']);
      //echo 'UPDATE mod_itaushoplineretornos SET processed=true, DataehoraProcessed=NOW() where retornoid = '.$_GET['id'];

    }
  } else $debug = 'nenhum retorno processado';
  if(isset($_GET['id']) && $_GET['id'] != '' ){
    if(count($retornos)==1 && $result['status'] == 'found') {
      echo $result['invoice'];
    }else{
      echo $result['status'];
    }
  } else {
    echo '<pre>';
    echo $debug;
  }
  //'status' => found,notfound,
  //'invoice' => $inv,
  //'desconto' => $desc,
  //'debug' => $debug,
  exit();
}


define("currentItaushoplineVersion", "1.0");
global $attachments_dir;

function itaushopline_addon_config() {
    $configarray = array(
		"name" => "Itau Shopline",
		"description" => "Setting module for the itaushopline.",
		"version" => "1.0",
		"author" => "webplus",
		"fields" => array()
	);
  return $configarray;
}

function itaushopline_addon_activate() {
	global $attachments_dir;
	mkdir ( $attachments_dir.'/itaushopline_retornos');
	$query0 = "CREATE TABLE IF NOT EXISTS `mod_itaushoplineSettings` (`id` int(10) NOT NULL AUTO_INCREMENT, `name` TEXT NOT NULL, `value` TEXT NOT NULL, PRIMARY KEY (`id`))";
    $result0 = full_query($query0);
	full_query("INSERT INTO `mod_itaushoplineSettings` (`name`, `value`) VALUES ('version', '".currentItaushoplineVersion."')");

    $query1 = "
	CREATE TABLE IF NOT EXISTS `mod_itaushoplineboletos` (
	  `pedido`			int(8) unsigned NOT NULL AUTO_INCREMENT,
	  `invoiceid`		int(8) unsigned NOT NULL,
	  `codEmp`			varchar(26)		NOT NULL,
	  `valor`			decimal(10,2)	NOT NULL,
	  `observacao`		varchar(40)		NOT NULL,
	  `chave`			varchar(16)		NOT NULL,
	  `nomeSacado`		varchar(30)		NOT NULL,
	  `codigoInscricao`	varchar(2)		NOT NULL,
	  `numeroInscricao`	varchar(14)		NOT NULL,
	  `enderecoSacado`	varchar(40)		NOT NULL,
	  `bairroSacado`	varchar(15)		NOT NULL,
	  `cepSacado`		varchar(8)		NOT NULL,
	  `cidadeSacado`	varchar(15)		NOT NULL,
	  `estadoSacado`	varchar(2)		NOT NULL,
	  `dataVencimento`	datetime		NOT NULL,
	  `urlRetorna`		varchar(60)		NOT NULL,
	  `obsAdicional1`	varchar(60)		NOT NULL,
	  `obsAdicional2`	varchar(60)		NOT NULL,
	  `obsAdicional3`	varchar(60)		NOT NULL,
	  `created_at`		timestamp		NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	  PRIMARY KEY (`pedido`) USING BTREE
	) ENGINE=InnoDB;
	";
    $result = full_query($query1);


    $query2 = "CREATE TABLE IF NOT EXISTS `mod_itaushoplineretornos` (
		`retornoid`				int(10)	unsigned NOT NULL AUTO_INCREMENT,
	  	`File`					VARCHAR(400) NOT NULL,
	  	`Line`					int(10) unsigned NOT NULL,
		`dataehora`				datetime	 NOT NULL,
		`invoiceid`				varchar(45)  NOT NULL,
	  	`AllLine`				VARCHAR(400) NOT NULL,
		`TipoRegistro`			VARCHAR(1)   NOT NULL,
		`CodigoInscricao`		VARCHAR(2)   NOT NULL,
		`NumeroInscricao`		VARCHAR(14)  NOT NULL,
		`Agencia`				VARCHAR(4)   NOT NULL,
		`Zeros01`				VARCHAR(2)   NOT NULL,
		`Conta`					VARCHAR(5)   NOT NULL,
		`Dac`					VARCHAR(1)   NOT NULL,
		`Brancos01`				VARCHAR(8)   NOT NULL,
		`UsoDaEmpresa`			VARCHAR(25)  NOT NULL,
		`NossoNumero`			VARCHAR(8)   NOT NULL,
		`Brancos02`				VARCHAR(12)  NOT NULL,
		`NumCarteira`			VARCHAR(3)   NOT NULL,
		`NossoNumero2`			VARCHAR(8)   NOT NULL,
		`DacNossoNumero`		VARCHAR(1)   NOT NULL,
		`Brancos03`				VARCHAR(13)  NOT NULL,
		`CodCarteira`			VARCHAR(1)   NOT NULL,
		`CodigoDeOcorrencia`	VARCHAR(2)   NOT NULL,
		`DataDeOcorrencia`		VARCHAR(6)   NOT NULL,
		`NumeroDocumento`		VARCHAR(10)  NOT NULL,
		`NossoNumero3`			VARCHAR(8)   NOT NULL,
		`Brancos04`				VARCHAR(12)  NOT NULL,
		`DataVencimento`		VARCHAR(6)   NOT NULL,
		`ValorDoTitulo`			VARCHAR(13)  NOT NULL,
		`CodigoDoBanco`			VARCHAR(3)   NOT NULL,
		`AgenciaCobradora`		VARCHAR(4)   NOT NULL,
		`DacAgenciaCobradora`	VARCHAR(1)   NOT NULL,
		`Especie`				VARCHAR(2)   NOT NULL,
		`TarifaDeCobranca`		VARCHAR(13)  NOT NULL,
		`Brancos05`				VARCHAR(26)  NOT NULL,
		`ValorDoIOF`			VARCHAR(13)  NOT NULL,
		`ValorAbatimento`		VARCHAR(13)  NOT NULL,
		`Descontos`				VARCHAR(13)  NOT NULL,
		`ValorPrincipal`		VARCHAR(13)  NOT NULL,
		`JurosDeMoraMulta`		VARCHAR(13)  NOT NULL,
		`OutrosCreditos`		VARCHAR(13)  NOT NULL,
		`BoletoDDA`				VARCHAR(1)   NOT NULL,
		`Brancos06`				VARCHAR(2)   NOT NULL,
		`DataCredito`			VARCHAR(6)   NOT NULL,
		`InstrucaoCancelada`	VARCHAR(4)   NOT NULL,
		`Brancos07`				VARCHAR(6)   NOT NULL,
		`Zeros02`				VARCHAR(13)  NOT NULL,
		`NomeDoSacado`			VARCHAR(30)  NOT NULL,
		`Brancos08`				VARCHAR(23)  NOT NULL,
		`Erros`					VARCHAR(8)   NOT NULL,
		`Brancos09`				VARCHAR(7)   NOT NULL,
		`CodigoDeLiquidacao`	VARCHAR(2)   NOT NULL,
		`NumeroSequencial`		VARCHAR(6)   NOT NULL,
		`Processed`		BOOLEAN NOT NULL DEFAULT 0,
    `DataehoraProcessed`		DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
		PRIMARY KEY (`retornoid`)
	) ENGINE=InnoDB
	;";
    $result = full_query($query2);


}

function itaushopline_addon_deactivate() {
	global $attachments_dir;
	rename($attachments_dir.'/itaushopline_retornos',$attachments_dir.'/itaushopline_retornos_bkp'.date('Ymd-His'));

	$query0 = "DROP TABLE `mod_itaushoplineSettings`";
    $result0 = full_query($query0);

    $query1 = "RENAME TABLE  `mod_itaushoplineboletos` TO  `mod_itaushoplineboletos_bkp".date('Ymd-His')."`";
    $result1 = full_query($query1);

    $query2 = "RENAME TABLE  `mod_itaushoplineretornos` TO  `mod_itaushoplineretornos_bkp".date('Ymd-His')."`";
    $result2 = full_query($query2);


}

function itaushopline_addon_upgrade($vars) {

	$version = $vars['version'];
	full_query("UPDATE `mod_itaushoplineSettings` SET `value` = '".currentItaushoplineVersion."' WHERE `name` = 'version' ");

	if ($version < 1.0) {
		//$query = "CREATE TABLE `mod_itaushoplineSettings` (`id` int(10) NOT NULL AUTO_INCREMENT, `name` TEXT NOT NULL, `value` TEXT NOT NULL, PRIMARY KEY (`id`))";
		//$result = full_query($query);
	}

}

function itaushopline_internal_permissions(){
	//$parray = array();
	//$data = get_query_vals("tbladmins", "tbladminroles.widgets,tbladmins.roleid,tbladmins.disabled", array("tbladmins.id" => $_SESSION['adminid']), "", "", "", "tbladminroles ON tbladminroles.id=tbladmins.roleid");
	//if (!empty($data) || $data['disabled'] != "0"){
	//	$adminPermissions = localAPI("getadmindetails");
	//	if ($adminPermissions['result'] === "success"){
	//		$parray["widgets"] = explode(',', $data['widgets']);
	//		$parray["permissions"] = explode(',', $adminPermissions['allowedpermissions']);
	//	}
	//}
	//return $parray;
}



function itaushopline_addon_output($vars) {
require_once __DIR__ . '/../../../init.php';
require_once __DIR__ . '/../../../includes/gatewayfunctions.php';
require_once __DIR__ . '/../../../includes/invoicefunctions.php';

	$form = '
<div class="callout callout-success">
	<h4>Ita&uacute; Shopline</h4>
	Addon instalado com sucesso.
</div>
  <div class="control-group">
    <strong style="color: #cc0000">Nota:</strong><br>
  	<strong>Para ativar a rotina de ativar de ativar serviçoes e dar baixa é necessário incluir nas tarefas agendadas de 1 em 1 min a linha:</strong> <br>
  	<em>php -q '.__DIR__.'/itaushopline_addon.php?cron</em>
  </div><br /><br /><br />
<fieldset>
	<legend>Enviar arquivo de Retorno</legend>

	<form action="" method="post" enctype="multipart/form-data" class="form-horizontal">
		<!--div class="form-group" >
			<label for="teste" class="col-sm-2 control-label">Digite um texto</label>
			<div class="col-sm-10">
			<input type="text" class="form-control" id="teste" name="teste" placeholder="Texto Teste">
			</div>
		</div-->
		<div class="form-group">
			<label for="ret" class="col-sm-2 control-label">Arquivo de Retorno</label>
			<div class="col-sm-10">
				<input type="file" name="ret[]" id="ret" multiple="multiple" accept=".ret" >
			</div>
		</div>
		<div class="form-group">
			<div class="col-sm-offset-2 col-sm-10">
				<button type="submit" class="btn btn-default" name="enviar" value="1"><i class="fa fa-upload"></i> Enviar</button>
			</div>
		</div>
	</form>
</fieldset>
';
	global $attachments_dir;
	echo $form;

/// POSTAR RET
  $imported = 0;
  $debug1 = '';
	if(isset($_POST['enviar']) && $_POST['enviar'] == '1'){
		$files = itaushopline_addon_normalize_files_array($_FILES);
		$debug = '';
		if ($files['ret'][0]['name'] != '') {
			foreach ($files['ret'] as $k=>$v) {
				$debug .= '### LENDO ARQUIVO "'.$v['name']."\" ###\n";
				if ($v['error'] != 0) {
					$debug .= '#### Erro ao enviar arquivo '.$v['name']."\n";
					//$mv
				} else {
          $imported++;
          $_GET['ret'] = '';
					$datetime = date('Ymd-His');
					$newFileLocation=$attachments_dir.'/itaushopline_retornos/'.$datetime.'-'.$v['name'];
					$fileid = md5(uniqid(md5(time()).rand(),1));
					rename($v['tmp_name'],$newFileLocation);
					$retorno = itaushopline_addon_lerRetorno($newFileLocation);

					foreach($retorno['data'] as $rd) {
						$resultLine = select_query('mod_itaushoplineretornos', '', array('AllLine' => $rd['AllLine']));//."\n";
						$existLine = mysql_fetch_array($resultLine);

						if(!$existLine)  {
							$debug .= ' - Importando linha '.$rd['Line']."\n";
							$rd['FileId']=$fileid;
							$lid = itaushopline_addon_gravarRetorno($rd);
							$rd['retornoid'] = $lid;
              //$queue[] = $lid;
							// DAR BAIXA

						} else $debug .= ' --- !!! Linha já importada !!!'."\n";
					}
					$fileid = '';
				}
				$debug1 .= pre($debug);
        $debug = '';
			}
		} else {
			$debug = pre("Nenhum arquivo enviado;");
		}

	}
  //echo ($debug1!='')?$debug1:'';
  //echo pre(var_export($_SERVER,1));
  if ($imported>0) echo pre($imported.' Arquivos Importados');

	echo itaushopline_addon_getLastImports(10);
	if (isset($_GET['ret']) && $_GET['ret'] != '' && $_GET['ret'] != '0'){
		echo itaushopline_addon_getImportsDetails($_GET['ret']);
  }
}

function itaushopline_addon_getLastImports($qto){
	global $attachments_dir;
	$tret=full_query("
	SELECT
		FileId,
		retornoid,
		dataehora,
		substr(replace(m.`File`,'".$attachments_dir.'/itaushopline_retornos/'."',''),17) as file
	FROM
		mod_itaushoplineretornos m
	WHERE
		m.`TipoRegistro` <> ''
	GROUP BY
		m.`FileId`
	");
	$trow= mysql_num_rows($tret);
	$tpag = ceil($trow/$qto);
	$paga=(isset($_GET['pag']) && $_GET['pag'] != '')?$_GET['pag']:0;
	$pagaq=($paga == 0)?0:$_GET['pag']*$qto;
	//$paga=(isset($_GET['ret']) && $_GET['ret'] != '')?$_GET['ret']:false;

	parse_str(html_entity_decode($_SERVER['QUERY_STRING']), $qsr);

	$qsrn=$qsr;
	$qsrp=$qsr;
	$qsrn['pag']=(($paga+1)!=$tpag && $tpag>1)?$paga+1:false;
	$qsrp['pag']=(($paga)>0 && $tpag>1)?$paga-1:false;
	if(array_key_exists('ret',$qsrn)) unset($qsrn['ret']);
	if(array_key_exists('ret',$qsrp)) unset($qsrp['ret']);
	$urln=$_SERVER['SCRIPT_URL'].'?'.http_build_query($qsrn);
	$urlp=$_SERVER['SCRIPT_URL'].'?'.http_build_query($qsrp);

	$listaUltimos = '<fieldset>
		<legend>Arquivos Importados ('.$trow.')</legend>
		<table class="table table-hover table-bordered" style="background-color:#FFF;">
			<tr>
				<th>Arquivo</th>
				<th>Data</th>
				<th>Linhas V&aacute;lidas</th>
        <th>Linhas Processadas</th>
				<th>Baixas</th>
				<th></th>
			</tr>
';
	$ultimos = full_query("
	SELECT
		FileId,
		min(retornoid) as retornoid,
		dataehora,
		substr(replace(m.`File`,'".$attachments_dir.'/itaushopline_retornos/'."',''),17) as file,
		count(FileId) as total,
    sum(Processed) as tProcessed,
		sum(if(invoiceid != '', 1, 0)) as tInvoice
	FROM
		mod_itaushoplineretornos m
	WHERE
		m.`TipoRegistro` <> ''
	GROUP BY
		m.`FileId`
	ORDER BY
		m.`File` desc
	LIMIT ".$pagaq.','.$qto.";
	");
	//echo "LIMIT ".$pagaq.','.$qto."";

	while($ultimo = mysql_fetch_array($ultimos)){
		$qsr['ret']=$ultimo['FileId'];
		$btprocessar=($ultimo['tProcessed']<$ultimo['total'])?'<a href="'.$_SERVER['SCRIPT_URL'].'?'.http_build_query($qsr).'" class="btn btn-default"><i class="fa fa-cogs"></i> Processar</a>':'Processado';
		$listaUltimos .= "		<tr>
				<td><a href='".$_SERVER['SCRIPT_URL'].'?'.http_build_query($qsr)."'><i class='fa fa-file-text sucess'></i> ".$ultimo['file']."</a></td>
				<td>".date('d/m/Y H:i:s',strtotime($ultimo['dataehora']))."</td>
				<td id=\"sValid".$ultimo['FileId']."\">".$ultimo['total']."</td>
        <td id=\"sProcessed".$ultimo['FileId']."\">".$ultimo['tProcessed']."</td>
				<td id=\"sTotalBaixa".$ultimo['FileId']."\">".$ultimo['tInvoice']."</td>
				<td id=\"sStatus".$ultimo['FileId']."\">".$btprocessar."</td>
			</tr>
";
	}
	$listaUltimos .= '			<tr>
				<td colspan="4" align="center">';
	$listaUltimos .= ($qsrp['pag']!==false)?'<a href="'.$urlp.'"><i class="fa fa-angle-left"></i> Anterior</a>&nbsp;&nbsp;':'<i class="fa fa-angle-left"></i> Anterior&nbsp;&nbsp;';
	$listaUltimos .= ' P&aacute;gina '.($paga+1).' de '.$tpag;
	$listaUltimos .= ($qsrn['pag']!==false)?'&nbsp;&nbsp;<a href="'.$urln.'">Pr&oacute;xima <i class="fa fa-angle-right"></i></a>':'&nbsp;&nbsp;Pr&oacute;xima <i class="fa fa-angle-right"></i>';
	$listaUltimos .= '</td>
			</tr>
';
	$listaUltimos .= '	</table>
	</fieldset>';
	return $listaUltimos;
}
function itaushopline_addon_getImportsDetails($id){
	global $attachments_dir;
	parse_str(html_entity_decode($_SERVER['QUERY_STRING']), $qsr);
	if(array_key_exists('ret',$qsr)) unset($qsr['ret']);
	$close = $_SERVER['SCRIPT_URL'].'?'.http_build_query($qsr);
	$listaUltimosl = '';
	$ret = full_query("SELECT *,substr(replace(m.`File`,'".$attachments_dir.'/itaushopline_retornos/'."',''),17) as arq FROM mod_itaushoplineretornos m WHERE m.TipoRegistro<>'' AND m.FileId = '".$id."' ORDER BY line, NumeroSequencial;");
	$total = 0;
	$totalt = 0;
  $queue = array();
	while($retl = mysql_fetch_array($ret)){
    if($retl['Processed']!=1) $queue[] = $retl['retornoid'];
		$arq=$retl['arq'];
		$invoiceLink=($retl['invoiceid']!='')?'<a href="invoices.php?action=edit&id='.$retl['invoiceid'].'">'.$retl['invoiceid'].'</a>':'';
		$comp=($retl['invoiceid']!='')?'<i class="fa fa-check-square-o"></i>':'<i class="fa fa-square-o"></i>';
		$proc=($retl['Processed']==1)?'<i class="fa fa-check-square-o"></i>':'';
		$datavenc=($retl['DataVencimento']!='000000')?date('d/m/Y',strtotime(substr($retl['DataVencimento'], 4, 2).'-'.substr($retl['DataVencimento'], 2, 2).'-'.substr($retl['DataVencimento'], 0, 2).' 00:00:00')):'-';
		$dataoc =($retl['DataDeOcorrencia']!='000000')?date('d/m/Y',strtotime(substr($retl['DataDeOcorrencia'], 4, 2).'-'.substr($retl['DataDeOcorrencia'], 2, 2).'-'.substr($retl['DataDeOcorrencia'], 0, 2).' 00:00:00')):'-';
		$processado=($retl['Processed']==1)?'S':'N';
		$listaUltimosl .= "		<tr id=\"lret\">
				<td>".$retl['NossoNumero']."</td>
				<td id=\"pInv".$retl['retornoid']."\">".$invoiceLink."</td>
				<td>".$datavenc."</td>
				<td>".$dataoc."</td>
				<td>".$retl['NomeDoSacado']."</td>
				<td>R$ " . number_format(floatval(substr($retl['ValorDoTitulo'], 0, -2).'.'.substr($retl['ValorDoTitulo'], -2)), 2, ',', '.')."</td>
				<td>".$retl['Line']."</td>
				<td id=\"bInv".$retl['retornoid']."\">".$comp."</td>
				<td id=\"procid".$retl['retornoid']."\">".$proc."</td>
			</tr>
";
		$total += floatval(substr($retl['ValorDoTitulo'], 0, -2).'.'.substr($retl['ValorDoTitulo'], -2));
		$totalt += floatval(substr($retl['TarifaDeCobranca'], 0, -2).'.'.substr($retl['TarifaDeCobranca'], -2));
	}
	$listaUltimosd = '<fieldset>
		<legend>Retorno <i class="fa fa-file-text"></i> '.$arq.' <a href='.$close.'><i class="fa fa-close"></i></a></legend>
		<table class="table table-hover table-bordered" style="background-color:#FFF;" id="tblRet">
			<tr>
				<th>N&ordm; do doc.</th>
				<th>Invoice</th>
				<th>Data de Vencimento</th>
				<th>Data de Pagamento</th>
				<th>Nome do Sacado</th>
				<th>Valor do t&iacute;tulo</th>
				<th>Linha</th>
				<th>Baixado</th>
				<th>Processado</th>
			</tr>
';
	$listaUltimosd .= $listaUltimosl;
	$listaUltimosd .= '<fieldset>
			<tr>
				<td colspan="9"></td>
			</tr>
			<tr>
				<td colspan="5" align="right">Total Pago: </td>
				<td><strong>R$ '. number_format($total, 2, ',', '.').'</strong></td>
				<td colspan="3"></td>
			</tr>
			<tr>
				<td colspan="5" align="right">Total de Tarifas: </td>
				<td><strong>R$ '. number_format($totalt, 2, ',', '.').'</strong></td>
				<td colspan="3"></td>
			</tr>
			<tr>
				<td colspan="5" align="right">Total a Receber: </td>
				<td><strong>R$ '. number_format(($total-$totalt), 2, ',', '.').'</strong></td>
				<td colspan="3"></td>
			<tr>
';
	$listaUltimosd .= '	</table>
	</fieldset>';
  $execQueue = '';
  if(count($queue)>0) {
    $execQueue = '
    <style>
      .loading_spinner,.table>tbody>tr>td.loading_spinner {padding-left:18px; background:url(/assets/img/spinner.gif) left center no-repeat;}
    </style>
    <script type="text/javascript">
      var queue = ['.implode(", ",$queue).'];
      var lProc=jQuery("#sProcessed'.$id.'").text();
      var tBaix=jQuery("#sTotalBaixa'.$id.'").text();
      var execute_queue = function(index) {
        jQuery("#procid"+queue[index]).text("");
        jQuery("#procid"+queue[index]).addClass(\'loading_spinner\');
        $.ajax( {
          url: "../modules/addons/itaushopline_addon/itaushopline_addon.php?cron&id="+queue[index],
          success: function(data) {
            if(data != \'notfound\'){
              tBaix++;
              jQuery("#procid"+queue[index]).html(\'<i class="fa fa-check-square-o"></i>\');
              jQuery("#bInv"+queue[index]).html(\'<i class="fa fa-check-square-o"></i>\');
              jQuery("#pInv"+queue[index]).html(\'<a href="invoices.php?action=edit&id=\'+data+\'">\'+data+\'</a>\');
              jQuery("#procid"+queue[index]).removeClass(\'loading_spinner\');
              jQuery("#sTotalBaixa'.$id.'").text(tBaix);
            } else if(data == \'notfound\') {
              jQuery("#procid"+queue[index]).html(\'<i class="fa fa-check-square-o"></i>\');
              jQuery("#procid"+queue[index]).removeClass(\'loading_spinner\');
            }
            console.log(data);
            index++;    // going to next queue entry
            lProc++;    // going to next queue entry
            jQuery("#sProcessed'.$id.'").text(lProc);
            if (queue[index] != undefined) {
              execute_queue(index);
            } else {
              jQuery("#sProcessed'.$id.'").removeClass(\'loading_spinner\');
              jQuery("#sStatus'.$id.'").text(\'Processado\');
            }
          }
        }); // end of $.ajax( {...
      }; // end of execute_queue() {...
      jQuery("#sProcessed'.$id.'").addClass(\'loading_spinner\');

      var index = 0;
      execute_queue(index); // go!

    </script>';
  }
  $listaUltimosd .= $execQueue;
	return $listaUltimosd;
}
function itaushopline_addon_gravarRetorno($rd) {

	//$nada = ('INSERT INTO `mod_itaushoplineretornos` (
	full_query('INSERT INTO `mod_itaushoplineretornos` (
	`FileId`
	,`File`
	,`Line`
	,`AllLine`
	,`TipoRegistro`
	,`CodigoInscricao`
	,`NumeroInscricao`
	,`Agencia`
	,`Zeros01`
	,`Conta`
	,`Dac`
	,`Brancos01`
	,`UsoDaEmpresa`
	,`NossoNumero`
	,`Brancos02`
	,`NumCarteira`
	,`NossoNumero2`
	,`DacNossoNumero`
	,`Brancos03`
	,`CodCarteira`
	,`CodigoDeOcorrencia`
	,`DataDeOcorrencia`
	,`NumeroDocumento`
	,`NossoNumero3`
	,`Brancos04`
	,`DataVencimento`
	,`ValorDoTitulo`
	,`CodigoDoBanco`
	,`AgenciaCobradora`
	,`DacAgenciaCobradora`
	,`Especie`
	,`TarifaDeCobranca`
	,`Brancos05`
	,`ValorDoIOF`
	,`ValorAbatimento`
	,`Descontos`
	,`ValorPrincipal`
	,`JurosDeMoraMulta`
	,`OutrosCreditos`
	,`BoletoDDA`
	,`Brancos06`
	,`DataCredito`
	,`InstrucaoCancelada`
	,`Brancos07`
	,`Zeros02`
	,`NomeDoSacado`
	,`Brancos08`
	,`Erros`
	,`Brancos09`
	,`CodigoDeLiquidacao`
	,`NumeroSequencial`
	,`dataehora`
	,`invoiceid`
	)
	VALUES
	(
	\''.$rd['FileId']					.'\',
	\''.$rd['File']						.'\',
	\''.$rd['Line']						.'\',
	\''.$rd['AllLine']					.'\',
	\''.$rd['TipoRegistro']				.'\',
	\''.$rd['CodigoInscricao']			.'\',
	\''.$rd['NumeroInscricao']			.'\',
	\''.$rd['Agencia']					.'\',
	\''.$rd['Zeros01']					.'\',
	\''.$rd['Conta']					.'\',
	\''.$rd['Dac']						.'\',
	\''.$rd['Brancos01']				.'\',
	\''.$rd['UsoDaEmpresa']				.'\',
	\''.$rd['NossoNumero']				.'\',
	\''.$rd['Brancos02']				.'\',
	\''.$rd['NumCarteira']				.'\',
	\''.$rd['NossoNumero2']				.'\',
	\''.$rd['DacNossoNumero']			.'\',
	\''.$rd['Brancos03']				.'\',
	\''.$rd['CodCarteira']				.'\',
	\''.$rd['CodigoDeOcorrencia']		.'\',
	\''.$rd['DataDeOcorrencia']			.'\',
	\''.$rd['NumeroDocumento']			.'\',
	\''.$rd['NossoNumero3']				.'\',
	\''.$rd['Brancos04']				.'\',
	\''.$rd['DataVencimento']			.'\',
	\''.$rd['ValorDoTitulo']			.'\',
	\''.$rd['CodigoDoBanco']			.'\',
	\''.$rd['AgenciaCobradora']			.'\',
	\''.$rd['DacAgenciaCobradora']		.'\',
	\''.$rd['Especie']					.'\',
	\''.$rd['TarifaDeCobranca']			.'\',
	\''.$rd['Brancos05']				.'\',
	\''.$rd['ValorDoIOF']				.'\',
	\''.$rd['ValorAbatimento']			.'\',
	\''.$rd['Descontos']				.'\',
	\''.$rd['ValorPrincipal']			.'\',
	\''.$rd['JurosDeMoraMulta']			.'\',
	\''.$rd['OutrosCreditos']			.'\',
	\''.$rd['BoletoDDA']				.'\',
	\''.$rd['Brancos06']				.'\',
	\''.$rd['DataCredito']				.'\',
	\''.$rd['InstrucaoCancelada']		.'\',
	\''.$rd['Brancos07']				.'\',
	\''.$rd['Zeros02']					.'\',
	\''.$rd['NomeDoSacado']				.'\',
	\''.$rd['Brancos08']				.'\',
	\''.$rd['Erros']					.'\',
	\''.$rd['Brancos09']				.'\',
	\''.$rd['CodigoDeLiquidacao']		.'\',
	\''.$rd['NumeroSequencial']			.'\',
	\''.date('Y-m-d H:i:s')				.'\',
	\''.''								.'\'
	);');
	//$line = select_query('mod_itaushoplineretornos', '', array('AllLine' => $rd['AllLine']));
	//$returnLine = mysql_fetch_array($line);
	//return $returnLine['retornoid'];
	return mysql_insert_id();
}
function pre($v) {
	return '<fieldset><legend>Debug</legend><pre>'.$v.'</pre></fieldset>';
}
function itaushopline_addon_lerRetorno($file) {
	if (!file_exists($file)) exit("Arquivo $file inexistente");
	$lines = array();
	$data = array();
	$myfile = fopen($file, "r") or die("Unable to open file!");
	while (!feof($myfile)) {
		$lines[] = fgets($myfile);
	}
	//$texto = fread($myfile,filesize($arquivo));
	fclose($myfile);
	//echo count($lines);exit();
	$line = 0;
	foreach ($lines as $lk=>$lv) {
		$line++;
		if($lk < sizeof($lines)-1) {
			$data[$lk]['File']				=	$file;
			$data[$lk]['Line']				=	$line;
			$data[$lk]['AllLine']				=	str_replace("\r\n",'',$lv);
		}

		if($lk == 0)					$data[$lk]['first']	=	$lv; // first 	line
		if($lk == sizeof($lines)-2)		$data[$lk]['last']	=	$lv; // last	line

		if($lk != 0 && $lk < sizeof($lines)-2) {
			$data[$lk]['TipoRegistro']			=	substr($lv,  0, 1); //  1
			$data[$lk]['CodigoInscricao']		=	substr($lv,  1, 2); //  2
			$data[$lk]['NumeroInscricao']		=	substr($lv,  3,14); // 14
			$data[$lk]['Agencia']				=	substr($lv, 17, 4); //  4
			$data[$lk]['Zeros01']				=	substr($lv, 21, 2); //  2
			$data[$lk]['Conta']					=	substr($lv, 23, 5); //  5
			$data[$lk]['Dac']					=	substr($lv, 28, 1); //  1
			$data[$lk]['Brancos01']				=	substr($lv, 29, 8); //  8
			$data[$lk]['UsoDaEmpresa']			=	substr($lv, 37,25); // 25
			$data[$lk]['NossoNumero']			=	substr($lv, 62, 8); //  8
			$data[$lk]['Brancos02']				=	substr($lv, 70,12); // 12
			$data[$lk]['NumCarteira']			=	substr($lv, 82, 3); //  3
			$data[$lk]['NossoNumero2']			=	substr($lv, 85, 8); //  8
			$data[$lk]['DacNossoNumero']		=	substr($lv, 93, 1); //  1
			$data[$lk]['Brancos03']				=	substr($lv, 94,13); // 13
			$data[$lk]['CodCarteira']			=	substr($lv,107, 1); //  1
			$data[$lk]['CodigoDeOcorrencia']	=	substr($lv,108, 2); //  2
			$data[$lk]['DataDeOcorrencia']		=	substr($lv,110, 6); //  6
			$data[$lk]['NumeroDocumento']		=	substr($lv,116,10); // 10
			$data[$lk]['NossoNumero3']			=	substr($lv,126, 8); //  8
			$data[$lk]['Brancos04']				=	substr($lv,134,12); // 12
			$data[$lk]['DataVencimento']		=	substr($lv,146, 6); //  6
			$data[$lk]['ValorDoTitulo']			=	substr($lv,152,13); // 13
			$data[$lk]['CodigoDoBanco']			=	substr($lv,165, 3); //  3
			$data[$lk]['AgenciaCobradora']		=	substr($lv,168, 4); //  4
			$data[$lk]['DacAgenciaCobradora']	=	substr($lv,172, 1); //  1
			$data[$lk]['Especie']				=	substr($lv,173, 2); //  2
			$data[$lk]['TarifaDeCobranca']		=	substr($lv,175,13); // 13
			$data[$lk]['Brancos05']				=	substr($lv,188,26); // 26
			$data[$lk]['ValorDoIOF']			=	substr($lv,214,13); // 13
			$data[$lk]['ValorAbatimento']		=	substr($lv,227,13); // 13
			$data[$lk]['Descontos']				=	substr($lv,240,13); // 13
			$data[$lk]['ValorPrincipal']		=	substr($lv,253,13); // 13
			$data[$lk]['JurosDeMoraMulta']		=	substr($lv,266,13); // 13
			$data[$lk]['OutrosCreditos']		=	substr($lv,279,13); // 13
			$data[$lk]['BoletoDDA']				=	substr($lv,292, 1); //  1
			$data[$lk]['Brancos06']				=	substr($lv,293, 2); //  2
			$data[$lk]['DataCredito']			=	substr($lv,295, 6); //  6
			$data[$lk]['InstrucaoCancelada']	=	substr($lv,301, 4); //  4
			$data[$lk]['Brancos07']				=	substr($lv,305, 6); //  6
			$data[$lk]['Zeros02']				=	substr($lv,311,13); // 13
			$data[$lk]['NomeDoSacado']			=	substr($lv,324,30); // 30
			$data[$lk]['Brancos08']				=	substr($lv,354,23); // 23
			$data[$lk]['Erros']					=	substr($lv,377, 8); //  8
			$data[$lk]['Brancos09']				=	substr($lv,385, 7); //  7
			$data[$lk]['CodigoDeLiquidacao']	=	substr($lv,392, 2); //  2
			$data[$lk]['NumeroSequencial']		=	substr($lv,394, 6); //  6
		}
	}
	$retorno = array(
		'lines' => $lines,
		'data' => $data,
	);
	return $retorno;
}
function itaushopline_addon_normalize_files_array($files = []) {
	$normalized_array = [];
	foreach($files as $index => $file) {
		if (!is_array($file['name'])) {
			$normalized_array[$index][] = $file;
			continue;
		}
		foreach($file['name'] as $idx => $name) {
			$normalized_array[$index][$idx] = [
				'name' => $name,
				'type' => $file['type'][$idx],
				'tmp_name' => $file['tmp_name'][$idx],
				'error' => $file['error'][$idx],
				'size' => $file['size'][$idx]
			];
		}
	}
	return $normalized_array;
}
function itaushopline_addon_getInvoceDetails($invoiceid){

	$result = select_query("tblinvoices", "", array("id" => $invoiceid));
	$data = mysql_fetch_array($result);
	$invoiceid = $data['id'];
	if (!$invoiceid) {
		$apiresults = array("status" => "error", "message" => "Invoice ID Not Found");
		return null;
	}
	$userid = $data['userid'];
	$invoicenum = $data['invoicenum'];
	$date = $data['date'];
	$duedate = $data['duedate'];
	$datepaid = $data['datepaid'];
	$subtotal = $data['subtotal'];
	$credit = $data['credit'];
	$tax = $data['tax'];
	$tax2 = $data['tax2'];
	$total = $data['total'];
	$taxrate = $data['taxrate'];
	$taxrate2 = $data['taxrate2'];
	$status = $data['status'];
	$paymentmethod = $data['paymentmethod'];
	$notes = $data['notes'];
	$result = select_query("tblaccounts", "SUM(amountin)-SUM(amountout)", array("invoiceid" => $invoiceid));
	$data = mysql_fetch_array($result);
	$amountpaid = $data[0];
	$balance = $total - $amountpaid;
	$balance = format_as_currency($balance);
	// TIRA JUROS E MULTA DO addon faturajuros
	$result2 = full_query("SELECT SUM(amount) FROM tblinvoiceitems WHERE type IN ('faturajuros_LateInterest','faturajuros_LateFee','itaushopline_disccount_dalay') AND invoiceid=".$invoiceid);
	$data2 = mysql_fetch_array($result2);
	$amountpaid2 = $data2[0];
	$balance2 = $total - $amountpaid - $amountpaid2;
	$balance2 = format_as_currency($balance2);
	// FIM TIRA JUROS E MULTA DO addon faturajuros
	$gatewaytype = get_query_val("tblpaymentgateways", "value", array("gateway" => $paymentmethod, "setting" => "type"));
	$ccgateway = (($gatewaytype == "CC" || $gatewaytype == "OfflineCC") ? true : false);
	$apiresults = array("result" => "success", "invoiceid" => $invoiceid, "invoicenum" => $invoicenum, "userid" => $userid, "date" => $date, "duedate" => $duedate, "datepaid" => $datepaid, "subtotal" => $subtotal, "credit" => $credit, "tax" => $tax, "tax2" => $tax2, "total" => $total, "balance" => $balance, "balance2" => $balance2, "taxrate" => $taxrate, "taxrate2" => $taxrate2, "status" => $status, "paymentmethod" => $paymentmethod, "notes" => $notes, "ccgateway" => $ccgateway);
	$result = select_query("tblinvoiceitems", "", array("invoiceid" => $invoiceid));
	while ($data = mysql_fetch_array($result)) {
		$apiresults['items']['item'][] = array("id" => $data['id'], "type" => $data['type'], "relid" => $data['relid'], "description" => $data['description'], "amount" => $data['amount'], "taxed" => $data['taxed']);
	}
	$apiresults['transactions'] = "";
	$result = select_query("tblaccounts", "", array("invoiceid" => $invoiceid));
	while ($data = mysql_fetch_assoc($result)) {
		$apiresults['transactions']['transaction'][] = $data;
	}
	return $apiresults;

}
function itaushopline_addon_darBaixa($rd){
  // DAR BAIXA
  $lid = $rd['retornoid'];
  $debug='';
  $ValorDoTitulo = floatval(substr($rd['ValorDoTitulo'], 0, -2).'.'.substr($rd['ValorDoTitulo'], -2));
  $ValorPrincipal = floatval(substr($rd['ValorPrincipal'], 0, -2).'.'.substr($rd['ValorPrincipal'], -2));
  $taxaBoleto = floatval(substr($rd['TarifaDeCobranca'], 0, -2).'.'.substr($rd['TarifaDeCobranca'], -2));

  $nossoNumero = intval($rd['NossoNumero']);

  $debug .= ' --- Procurando invoice para NOSSO NUMERO: '.$lid.'>'.$nossoNumero.' Valor: '.$ValorDoTitulo."\n";
  $resultLine2 = select_query('mod_itaushoplineboletos', '', array('pedido' => $nossoNumero));//."\n";
  $existLine2 = mysql_fetch_array($resultLine2);
  $inv='';
  $desc='';
  //$return=array();
  if($existLine2 && $existLine2['invoiceid'] ) {
    //updateInvoiceTotal($existLine2['invoiceid']);
    full_query('UPDATE mod_itaushoplineretornos SET invoiceid='.$existLine2['invoiceid'].' WHERE retornoid='.$rd['retornoid']);
    $invDet = itaushopline_addon_getInvoceDetails($existLine2['invoiceid']);
    $debug .= ' --- Invoice encontrado ID: '.$existLine2['invoiceid'].' Valor: '.$invDet['balance']."\n";
    //// UPDATE INVOICE ID NA TABELA RETORNO
    //$debug .= ($invDet['balance'])."\n";
    //$debug .= ($ValorDoTitulo)."\n";
    // SE valor pago > valor de produtos s/ juros do addon tarifajuros
    // E valor do titulo < valor da fatura
    // CONCEDER DESCONTO
    if($ValorDoTitulo>=$invDet['balance2'] && $ValorDoTitulo<$invDet['balance'] ){
      $desconto = round((($invDet['balance']-$ValorDoTitulo)*(-1)),2);
      full_query("INSERT INTO tblinvoiceitems (invoiceid,userid,type,description,amount,paymentmethod) VALUES (".$existLine2['invoiceid'].",".$invDet['userid'].",'itaushopline_disccount_dalay','Desconto por atraso na compensação do boleto', ".$desconto.", 'itaushopline')");
      updateInvoiceTotal($existLine2['invoiceid']);
      $debug .= ' --- Concedendo desconto por atraso de compensação de :'.$desconto."\n";
      $desc = $desconto;
    }
    //$invDet = itaushopline_addon_getInvoceDetails($existLine2['invoiceid']);
    //$debug .= var_export($invDet,1)."\n";
    logTransaction('Itau ShopLine', $rd, 'Success');
    addInvoicePayment($existLine2['invoiceid'],$lid,$ValorDoTitulo,$taxaBoleto,'itaushopline');
    $debug .= ' --- Dando baixa no invoice '.$existLine2['invoiceid']."\n";
    $inv=$existLine2['invoiceid'];
    $st='found';
  } else {
    $st='notfound';
    $debug .= ' --- !!! Boleto não encontrado !!!'."\n"; 								//echo $nossoNumero;
  }
  //$invoiceId = checkCbInvoiceID($nossoNumero, 'Itau ShopLine');
  //checkCbTransID($transactionId);
  //if($nossoNumero >= 90000000) {
  //	$resultLine2 = select_query('mod_itaushoplineboletos', '', array('idboleto' => $nossoNumero));//."\n";
  //	$existLine2 = mysql_fetch_array($resultLine2);
  //	if($existLine2 && $existLine2['invoiceid'] ) {
  //		logTransaction('Itau ShopLine', $rd, 'Success');
  //		addInvoicePayment($existLine2['invoiceid'],$lid,$valor,0,'Itau ShopLine');
  //	} else $debug .= ' --- !!! Boleto não encontrado !!!'."\n";
  //} else {
  //	logTransaction('Itau ShopLine', $rd, 'Success');
  //	addInvoicePayment($existLine2['invoiceid'],$lid,$valor,0,'Itau ShopLine');
  //}
  return array(
    'status' => $st,
    'invoice' => $inv,
    'desconto' => $desc,
    'debug' => $debug,
  );

}


?>
