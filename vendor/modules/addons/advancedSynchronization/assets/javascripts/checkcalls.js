$(document).ready(function() 
{
    var getURLParameter = function(name) 
    {
        return decodeURI(
            (RegExp(name + '=' + '(.+?)(&|$)').exec(location.search)||[,null])[1]
        );
    };
    
    $('#clienttabs ul').append('<li class="tab" id="callLogOpen"><a href="clientssummary.php?userid='+getURLParameter('userid')+'&showCallLog=1">Call logs</a></li>');
    
    $('input[name="phonenumber"], input[name="customfield['+ CUSTOM_FIELD_ID +']"]').each(function() {
        var that = $(this);

        that.parent().append('<a href="#" class="callOut"> Call Out</a>');
    });
    
    $('#callLogOpen').on('click', function(e)
    {
        var that = $(this);
        var content = $('#tab_content');
        
        $('li').removeClass('tabselected');
        that.addClass('tabselected');
        
        if(typeof history != 'undefined')
            history.pushState(null, null, 'clientssummary.php?userid='+getURLParameter('userid')+'&showCallLog=1');
        
        $.get('addonmodules.php?module=elastixmanager&modpage=getusercalllogs&id='+getURLParameter('userid'), function(result)
        {
            content.html(result);
            
            $(".datepick").datepicker({
                    dateFormat: datepickerformat,
                    showOn: "button",
                    buttonImage: "images/showcalendar.gif",
                    buttonImageOnly: true,
                    showButtonPanel: true,
                    showOtherMonths: true,
                    selectOtherMonths: true
            });
        });
        
        e.preventDefault(); 
    });
    
    $('.callOut').on('click', function(e)
    {
        var that = $(this);
        var destination = that.prev('input').val();
        
        that.text('Orginating the call...');
        
        $.getJSON('addonmodules.php?module=elastixmanager&modpage=callout&ajax=1&extension=129&number=' + destination, function(result) 
        {
            that.text('Call Originated');
            setTimeout(function() {
                that.text('Call Out');
            }, 5000);
            
            console.log(result);
        });
        
        e.preventDefault(); 
    });
    
    if(getURLParameter('showCallLog') == 1)
        $('#callLogOpen').trigger('click');
    
    /*$.jGrowl('<div class="large">Calling: <span class="phoneNumber">697 828 947</span></div><div class="normal">Idenitified as: <strong>Dominik Gacek</strong></div><div class="normal"><a href="#">Send a ticket</a> | <a href="#">Show call log for user</a> | <a href="#">Create a quote</a></div>', { 
        life : 15000, 
        sticky : true,
        theme  : 'default',
        header : 'New Call In!', 
        position : 'bottom-left',
        corners : 0
    });

    var template = '<div class="large">Calling: <span class="phoneNumber">(+48) 697 828 947</span></div><div class="normal">Not identified client:</div><div></div>';

    $.jGrowl(template, {        
        life : 15000, 
        sticky : true,
        header : 'New Call Out!', 
        position : 'bottom-left',
        corners : 0
    });*/

    var checkForNewCalls = function()
    {
        var template = '';
        var number = '';
        $.getJSON('addonmodules.php?module=elastixmanager&modpage=getcalls', function(result) {
            if(result.length > 0)
            {
                for(var i=0;i<result.length;i++)
                {
                    if(result[i].outcall == 0)
                    {
                        if(result[i].clientname == 'Unknown')
                        {
                            template = '<div class="large">Calling: <span class="phoneNumber">'+ result[i].callerid +'</span></div><div class="normal">Not identified client:</div><div><a href="clientsadd.php">Create Client</a> | <a href="addonmodules.php?module=crmaddon&m=lead_info">Create new Lead</a> | <a href="addonmodules.php?module=crmaddon&m=pot_new">Create new Potential</a> | <a href="supporttickets.php?action=open&userid=0">Create new ticket</a></div>';
                        }
                        else
                        {
                            //addonmodules.php?module=elastixmanager&modpage=main&filters[clientname]='+ encodeURIComponent(result[i].clientname) +'
                            template = '<div class="large">Calling: <span class="phoneNumber">'+ result[i].callerid +'</span></div><div class="normal">Idenitified as: <strong>'+ result[i].clientname +'</strong></div><div class="normal"><a href="supporttickets.php?action=open&userid='+ result[i].clientid +'" target="_blank">Send a ticket</a> | <a href="clientssummary.php?userid='+ result[i].clientid +'&showCallLog=1" target="_blank">Show call log for user</a> | <a href="quotes.php?action=manage&userid='+ result[i].clientid +'" target="_blank">Create a quote</a></div>';
                        }
                    }
                    else
                    {
                        number = (result[i].extension.indexOf('/') > 0) ? result[i].extension.split('/')[1] : result[i].extension;
                        
                        template = '<div class="large">Call to: <span class="phoneNumber">'+ number +'</span></div><div class="normal">Idenitified as: <strong>'+ result[i].clientname +'</strong></div><div class="normal"><a href="clientsadd.php">Create new client</a></div>';
                    }
                    
                    $.jGrowl(template, { 
                        life : 15000, 
                        sticky : true,
                        theme  : (result[i].outcall == 1) ? 'callout' : 'default',
                        header : (result[i].outcall == 1) ? 'New Call Out' : 'New Call In!', 
                        position : 'bottom-left',
                    });
                }
            }
        });
        
        setTimeout(checkForNewCalls, 5000);
    };
    
    checkForNewCalls();
});