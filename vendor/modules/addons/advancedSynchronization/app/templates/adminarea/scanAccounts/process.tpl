<h5>{$settings.serverName}</h5>
<form method="post" action="{'scanAccounts'|addon_url:'save'}" id="save_settings_form">
    <input type='hidden' value='{$smarty.get.serverID}' name='serverID'/>
     <table>
              <tr>
                  <td><label class="options-label" for="asNs1">{$lang->_("Nameserver 1")}</label></td>
                  <td><input type="text" id="asNs1" name="nameServers[0]"  value="{$nameServers.0}"> </td>
                  <td><label class="options-label-description"></label></td>
                  
                  <td><label class="options-label" for="asNs2">{$lang->_("Nameserver 2")}</label></td>
                  <td><input type="text" id="asNs2" name="nameServers[1]" value="{$nameServers.1}"></td>
                  <td><label class="options-label-description"></label></td>
                  
                  <td><label class="options-label" for="asNs3">{$lang->_("Nameserver 3")}</label></td>
                  <td><input type="text" id="asNs3" name="nameServers[2]" value="{$nameServers.2}"></td>
                  <td><label class="options-label-description"></label></td>
                  
              </tr>
             <tr>

                  <td><label class="options-label" for="asNs4">{$lang->_("Nameserver 4")}</label></td>
                  <td><input type="text" id="asNs4" name="nameServers[3]" value="{$nameServers.3}"></td>
                  <td><label class="options-label-description"></label></td>
                  
                  <td><label class="options-label" for="asNs5">{$lang->_("Nameserver 5")}</label></td>
                  <td><input type="text" id="asNs5" name="nameServers[4]" value="{$nameServers.4}"></td>
                  <td><label class="options-label-description"></label></td>
                  
                  <td><label class="options-label" for="asNs6">{$lang->_("Nameserver 6")}</label></td>
                  <td><input type="text" id="asNs6" name="nameServers[5]" value="{$nameServers.5}"></td>
                  <td><label class="options-label-description"></label>
                  <a href="#" id="asAddMoreNS" title="{$lang->_("Add more name servers.")}" class="info" data-toggle="tooltip" data-placement="left" ><i class="icon-plus-sign"></i>{$lang->_("More")}</a> 
                  </td>
              </tr>
              <tr class="asAdditionalNameservers"  style="display:none;">
                  <td><label class="options-label" for="asNs7">{$lang->_("Nameserver 7")}</label></td>
                  <td><input type="text" id="asNs7" name="nameServers[6]"  value="{$nameServers.6}"> </td>
                  <td><label class="options-label-description"></label></td>
                  
                  <td><label class="options-label" for="asNs8">{$lang->_("Nameserver 8")}</label></td>
                  <td><input type="text" id="asNs8" name="nameServers[7]" value="{$nameServers.7}"></td>
                  <td><label class="options-label-description"></label></td>
                  
                  <td><label class="options-label" for="asNs9">{$lang->_("Nameserver 9")}</label></td>
                  <td><input type="text" id="asNs9" name="nameServers[8]" value="{$nameServers.8}"></td>
                  <td><label class="options-label-description"></label></td>
              </tr>
             <tr class="asAdditionalNameservers"  style="display:none;">
                  <td><label class="options-label" for="asNs10">{$lang->_("Nameserver 10")}</label></td>
                  <td><input type="text" id="asNs10" name="nameServers[9]" value="{$nameServers.9}"></td>
                  <td><label class="options-label-description"></label></td>
                  
                  <td><label class="options-label" for="asNs11">{$lang->_("Nameserver 11")}</label></td>
                  <td><input type="text" id="asNs11" name="nameServers[10]" value="{$nameServers.10}"></td>
                  <td><label class="options-label-description"></label></td>
                  
                  <td><label class="options-label" for="asNs12">{$lang->_("Nameserver 12")}</label></td>
                  <td><input type="text" id="asNs12" name="nameServers[11]" value="{$nameServers.11}"></td>
                  <td><label class="options-label-description"></label>
                  </td>
              </tr>
              <tr class="asAdditionalNameservers"  style="display:none;">
                  <td><label class="options-label" for="asNs13">{$lang->_("Nameserver 13")}</label></td>
                  <td><input type="text" id="asNs12" name="nameServers[12]"  value="{$nameServers.12}"> </td>
                  <td><label class="options-label-description"></label></td>
                  
                  <td><label class="options-label" for="asNs14">{$lang->_("Nameserver 14")}</label></td>
                  <td><input type="text" id="asNs13" name="nameServers[13]" value="{$nameServers.13}"></td>
                  <td><label class="options-label-description"></label></td>
                  
                  <td><label class="options-label" for="asNs15">{$lang->_("Nameserver 15")}</label></td>
                  <td><input type="text" id="asNs15" name="nameServers[14]" value="{$nameServers.14}"></td>
                  <td><label class="options-label-description"></label></td>
              </tr>
             <tr class="asAdditionalNameservers"  style="display:none;">
                  <td><label class="options-label" for="asNs16">{$lang->_("Nameserver 16")}</label></td>
                  <td><input type="text" id="asNs16" name="nameServers[15]" value="{$nameServers.15}"></td>
                  <td><label class="options-label-description"></label></td>
                  
                  <td><label class="options-label" for="asNs17">{$lang->_("Nameserver 17")}</label></td>
                  <td><input type="text" id="asNs17" name="nameServers[16]" value="{$nameServers.16}"></td>
                  <td><label class="options-label-description"></label></td>
                  
                  <td><label class="options-label" for="asNs18">{$lang->_("Nameserver 18")}</label></td>
                  <td><input type="text" id="asNs18" name="nameServers[17]" value="{$nameServers.17}"></td>
                  <td><label class="options-label-description"></label>
                  </td>
              </tr>
             <tr class="asAdditionalNameservers"  style="display:none;">
                  <td><label class="options-label" for="asNs19">{$lang->_("Nameserver 19")}</label></td>
                  <td><input type="text" id="asNs19" name="nameServers[18]" value="{$nameServers.18}"></td>
                  <td><label class="options-label-description"></label></td>
                  
                  <td><label class="options-label" for="asNs20">{$lang->_("Nameserver 20")}</label></td>
                  <td><input type="text" id="asNs20" name="nameServers[19]" value="{$nameServers.19}"></td>
                  <td><label class="options-label-description"></label></td>
                  
                  <td><label class="options-label" for="asNs21">{$lang->_("Nameserver 21")}</label></td>
                  <td><input type="text" id="asNs6" name="nameServers[20]" value="{$nameServers.20}"></td>
                  <td><label class="options-label-description"></label>
                  </td>
              </tr>
              <tr>
                  <td><label class="options-label" for="as">{$lang->_("Synchronized DNS Per Account")}</label></td>
                  <td colspan="e"><input type="text" id="asNs1" name="synchronizedDNS" value="{$synchronizedDNS}" style="width: 30px;">
                        
                      <a href="#" title="{$lang->_("Minimum number of nameservers mathing nameservers provided above to mark hosting as correct.")}" class="info" data-toggle="tooltip" data-placement="right" ><i class="icon-question-sign"> </i></a> 
                      
                  </td>
              </tr>
              
     </table>

    <div class="form-actions">
        <button id='asDnsFormAction' class="button primary"><i class="icon-ok"> </i>{$lang->_('Update Settings')}</button>
    </div>
</form>
<form method="post">
    <table class="table table-bordered gridTable2" id="acScanTable">
        <thead>
            <tr>
                <th width="20" class="nosort"><input type="checkbox" class="toggleAll"> </th>
                <th  width="auto"><a href="#">{$lang->_('Domain')}</a></th>
                <th width="100"><a href="#">{$lang->_('Username')}</a></th>
                <th width="auto"><a href="#">{$lang->_('Email')}</a></th>
                <th width="100"><a href="#">{$lang->_('IP Address')}</a></th>
                <th width="70"><a href="#">{$lang->_('Status')}</a></th>
                <th width="80"><a href="#">{$lang->_('Created At')}</a></th>
                <th width="80"><a href="#">{$lang->_('Next Due Date')}</a></th>
                <th width="auto" class="nosort" style='color: #fff;'>{$lang->_('DNS')}</th>
                <th width="75" class="nosort"><a href="#">{$lang->_('Actions')}</a></th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>

    <div class="clear"></div>
    <div class="control-group">
        <label>{$lang->_('Action With Selected:')}</label>
        <select name="action">
            <option value="terminate">{$lang->_('Terminate On Server (only if applicable)')}</option>
            <option value="terminateInWhmcs">{$lang->_('Terminate In WHMCS (only if applicable)')}</option>
        </select>
    </div>
   
    <div class="form-actions">
        <button type="submit" class="button primary big" id="batchActionButton"><span class="icon-plus-sign"></span> {$lang->_('Perform Action')}</button>
    </div>

</form>
     
     
{literal}
<script type="text/javascript">
    jQuery(document).ready(function(){
          
          $("body").tooltip({ selector: '[data-toggle=tooltip]' });
          
          
          
          var table;
          table = $('#acScanTable').dataTable( {

              'bStateSave': true,
              'bProcessing': true,
              //'bServerSide': true,
              'sAjaxSource': 'addonmodules.php?module=advancedSynchronization&modpage=scanAccounts&modsubpage=loadData',
              //'pageLength': 10,
              //'aLengthMenu' : [10],
              'aoColumns': [
                  { 'mData' : 0 },
                  { 'mData' : 1 },
                  { 'mData' : 2},
                  { 'mData' : 3},
                  { 'mData' : 4 },
                  { 'mData' : 5},
                  { 'mData' : 6},
                  { 'mData' : 7},
                  { 'mData' : 8},
                  { 'mData' : 9}
              ],
              'aoColumnDefs' : [ {
                  'bSortable' : false,
                  'aTargets' : [ 'nosort' ]
              }]
           });
           ProductsSync.table = table;
           $("#acScanTable_length").after('<div class="dataTables_length" style="margin-top:4px; margin-left:10px;" >  \n\
                  <label><a href="#" id="dataTableRefresh">Refresh<i class="icon-refresh"></i></a> <img id="asRefresh" src="../modules/addons/advancedSynchronization/assets/img/loading2.gif" style="display:none; margin-left: -10px; margin-bottom:3px;"> </label></div>');
           $("#asDnsFormAction").click(function(){
                $.post($("#save_settings_form").attr("action"), $("#save_settings_form").serialize(),
                function(res){
                      alert(res.msg);
                }, "json");
                return false;
           });          
           $("#dataTableRefresh").click(function(e){
                e.preventDefault();
                $("#asRefresh").show();
                ProductsSync.table.fnReloadAjax();
           });
           
            table.on('xhr.dt', function ( e, settings, json ) {
                 $("#asRefresh").hide();
            });
            
            $("#asAddMoreNS").click(function(e){
                  e.preventDefault();
                  $(".asAdditionalNameservers").toggle();
            });
    });
</script>
{/literal}