{**********************************************************************
 * Customization Services by ModulesGarden.com
 * Copyright (c) ModulesGarden, INBS Group Brand, All Rights Reserved 
 * (2015-03-09)
 *
 *  CREATED BY MODULESGARDEN       ->        http://modulesgarden.com
 *  CONTACT                        ->       contact@modulesgarden.com
 *
 *
 *
 *
 * This software is furnished under a license and may be used and copied
 * only  in  accordance  with  the  terms  of such  license and with the
 * inclusion of the above copyright notice.  This software  or any other
 * copies thereof may not be provided or otherwise made available to any
 * other person.  No title to and  ownership of the  software is  hereby
 * transferred.
 *
 *
 **********************************************************************}

{**
 * @author Paweł Kopeć <pawelk@modulesgarden.com>
 *}

{if $service->isButtonTerminate}
 
      <div class="button-group" width="100%">
            
            {if $service->isSubmodule && $service->domainstatus!="Active" && $service->domainstatus!="Suspended"}
                  <a class="button primary ajaxAction" style="min-width:110px;" href="addonmodules.php?module=advancedSynchronization&modpage=scanAccounts&modsubpage=terminateOnWHMCS&serviceID={$service->id}&serverID={$service->server}&domain={$service->domain}">
                        {$lang->_("Terminate In WHMCS")}
                  </a> <br/>
            {/if}
            {if $service->isSubmodule && ( $service->domainstatus=="Active" || $service->domainstatus=="Suspended")}
                  <a class="button primary ajaxAction"  style="min-width:110px;" href="addonmodules.php?module=advancedSynchronization&modpage=scanAccounts&modsubpage=terminate&serviceID={$service->id}&serverID={$service->server}&domain={$service->domain}&username={$service->username}">
                        {$lang->_("Terminate On Server")}
                  </a>
            {/if}   
      </div>
{/if}