<h5>{$lang->_('Server Settings')}</h5>

<form method="post" action="{'settings'|addon_url:'store'}" id="assignDeviceForm">
    <div class="control-group">
        <label>{$lang->_('Server Hostname (or IP)')}</label>
        <div class="controls">
            <input type="text" name="setting[AsteriskHostname]" class="span4" value="{$config.AsteriskHostname}" style="font-weight: bold;" placeholder="{$lang->_('localhost')}">
        </div>
    </div>
    <div class="control-group">
        <label>{$lang->_('Port')}</label>
        <div class="controls">
            <input type="text" name="setting[AsteriskPort]" class="span1" value="{$config.AsteriskPort}" placeholder="{$lang->_('5038')}">
        </div>
    </div>
    <div class="control-group">
        <label>{$lang->_('Username')}</label>
        <div class="controls">
            <input type="text" name="setting[AsteriskUsername]" value="{$config.AsteriskUsername}" class="span3" placeholder="{$lang->_('root')}">
        </div>
    </div>    
    <div class="control-group">
        <label>{$lang->_('Password')}</label>
        <div class="controls">
            <input type="password" name="setting[AsteriskPassword]" value="{$config.AsteriskPassword}" class="span3" placeholder="{$lang->_('password')}">
        </div>
    </div>
    <div class="control-group">
        <label>{$lang->_('Protocol')}</label>
        <div class="controls">
            <select name="setting[AsteriskProtocol]">
                <option value="tcp">{$lang->_('tcp://')}</option>
            </select>
        </div>
    </div>
    <h5>{$lang->_('Base Settings')}</h5>
    <div class="control-group">
        <label>{$lang->_('Checking Frequency')}</label>
        <div class="controls">
            <input type="text" name="setting[AsteriskCheckingFrequency]" value="{$config.AsteriskCheckingFrequency}" class="span1" placeholder="{$lang->_('30')}"> <span>{$lang->_('seconds')}</span>
        </div>
    </div>
    <div class="control-group">
        <label>{$lang->_('Phone Custom Field ID')}</label>
        <div class="controls">
            <input type="text" name="setting[AsteriskCustomFieldID]" value="{$config.AsteriskCustomFieldID}" class="span1" placeholder="{$lang->_('1')}">
        </div>
    </div>
    <div class="control-group">
        <label>{$lang->_('Call Logs per Site')}</label>
        <div class="controls">
            <input type="text" name="setting[AsteriskLogsPerSite]" value="{$config.AsteriskLogsPerSite}" class="span1" placeholder="{$lang->_('25')}">
            <span class="help-block">{$lang->_('By default it is set to 25')}</span>
        </div>
    </div>
    <h5>{$lang->_('ACL')}</h5>
    <div class="control-group">
        <label>{$lang->_('ID of Staff Roles that will be able to see this page, separated by coma.')}</label>
        <div class="controls">
            <input type="text" name="setting[AsteriskModifySettingRoles]" value="{$config.AsteriskModifySettingRoles}" class="span2">
            <span class="help-block">{$lang->_('Please note, that if you will delete your ID from this list and you are not Full Administrator, you wont be able to see this page any more.')}</span>
        </div>
    </div>
    <div class="control-group">
        <label>{$lang->_('ID of Staff Roles that will be able to playback call records, separated by coma.')}</label>
        <div class="controls">
            <input type="text" name="setting[AsteriskPlaybackCallsRoles]" value="{$config.AsteriskPlaybackCallsRoles}" class="span2">
        </div>
    </div>
    <h5>{$lang->_('Templates')}</h5>
    <div class="control-group">
        <label>{$lang->_('Call In Popup Template')}</label>
        <textarea name="setting[AsteriskCallInTemplate]" class="tinymce">{$config.AsteriskCallInTemplate}</textarea>
    </div>
    <div class="control-group">
        <label>{$lang->_('Call Out Popup Template')}</label>
        <textarea name="setting[AsteriskCallOutTemplate]" class="tinymce">{$config.AsteriskCallOutTemplate}</textarea>
    </div>
    <div class="form-actions">
        <button type="submit" class="button primary icon add big">{$lang->_('Update Settings')}</button>
    </div>
</form>