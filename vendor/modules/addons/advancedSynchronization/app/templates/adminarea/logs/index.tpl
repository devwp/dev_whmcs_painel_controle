{if count($logs)}
<table id="logs">
    <thead>
        <tr>
            <th>{$lang->_('Title')}</th>
            <th>{$lang->_('Server')}</th>
            <th>{$lang->_('Date')}</th>
            <th>{$lang->_('Synchronized')}</th>
            <th>{$lang->_('Fail')}</th>
            <th>{$lang->_('Accounts Created')}</th>
            <th style="width: 180px;">{$lang->_('Action')}</th>
        </tr>
    </thead>
    <tbody>
{foreach from=$logs item=log}
    <tr>
        <td style="text-align: center;">{$log.title}</td>
        <td style="text-align: center;">{$log.server}</td>
        <td style="text-align: center;">{$log.date}</td>
        <td style="text-align: center;"><span class='success-status'>{$log.stat.success}</span></td>
        <td style="text-align: center;"><span class='fail-status'>{$log.stat.fail}</span></td>
        <td style="text-align: center;">{$log.stat.users} </td>
        <td>
            <a href="{'logs'|addon_url:'action'}&id={$log.id}" class="button primary"> {$lang->_('Details')}</a>
            <a href="{'logs'|addon_url:'deleteEntry'}&id={$log.id}" class="button danger delete-log-entry"><span class="icon-remove "></span> {$lang->_('Delete Entry')}</a>
        </td>
    <tr>
{/foreach}
    </tbody>
</table>
    {else}
        <div style='text-align:center'>{$lang->_('No Logs Data')}</div>
    {/if}
    <br>
    <br>
<div class="right-buttons">
    <a href="{'logs'|addon_url:'deleteEntries'}" class="button danger delete-log-entries"><span class="icon-remove "></span> {$lang->_('Delete All Entries')}</a>
</div>
