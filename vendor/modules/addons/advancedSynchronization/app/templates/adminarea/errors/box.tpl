<div>
    <span class="label label-terminated" style="background-color: #C43C35;">There was an error during the work of the application :( Please contact ModulesGarden immediately.</span>
</div>

<p style="margin: 10px 0;">You can paste a code from the field below to send and paste it into the Submit Ticket Form at our page. That will help us to faster find a cause of the problem.</p>

{if $debug eq 1}
    <h2>The error message is: <em>{$error->getMessage()}</em></h2>
    <cite>
        {$error->getTraceAsString()|nl2br}
    </cite>
{else}
    <cite style="word-break: break-all">
        {$error|base64_encode}
    </cite>
{/if}