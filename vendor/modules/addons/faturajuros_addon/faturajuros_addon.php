<?php
/**
 * faturajuros
 *
 * Setting module for the faturajuros.
 *
 * @package    WHMCS
 * @author     webplus <suporte@webplus.com.br>
 * @copyright  Copyright (c) webplus 2016
 * @link       http://www.webplus.com.br
 */

if (!defined("WHMCS"))
    die("This file cannot be accessed directly");

define("currentFaturaJurosVersion", "1.0");


function faturajuros_addon_config() {
    $configarray = array(
		"name" => "Fatura Juros",
		"description" => "Módulo addon para incluir juros em faturas vencidas.",
		"version" => "1.0",
		"author" => "webplus",
		"fields" => array(
			"percentFee"			=> array ("FriendlyName" => "% de multa por atraso (ex: 2.00)"	, "Type" => "text", "Size" => "25", "Description" => "", "Default" => "2.00", ),
			"percentInterestMonth"	=> array ("FriendlyName" => "% de juros ao mes (ex: 1.00)"		, "Type" => "text", "Size" => "25", "Description" => "", "Default" => "1.00", ),
		)
	);
    return $configarray;
}

function faturajuros_addon_activate() {
    $query1 = "
	CREATE TABLE  `mod_faturajuros` (
	  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
	  `invoiceid` int(10) unsigned NOT NULL,
	  `invoiceitenid` int(10) unsigned NOT NULL,
	  `tipo` varchar(5) NOT NULL,
	  `data_base` datetime NOT NULL,
	  `valor_base` double(10,2) NOT NULL,
	  `valor_taxa` double(10,2) NOT NULL,
	  `lastupdate` datetime NOT NULL,
	  `dataehora` datetime NOT NULL,
	  PRIMARY KEY (`id`)
	) ENGINE=InnoDB;
	";
    $result1 = full_query($query1);
}

function faturajuros_addon_deactivate() {
	$query1 = "RENAME TABLE  `mod_faturajuros` TO `mod_faturajuros_bkp".date('Ymd-His')."`";
    $result1 = full_query($query1);
}

function faturajuros_addon_upgrade($vars) {
}

function faturajuros_internal_permissions(){
}



function faturajuros_addon_output($vars) {
require_once __DIR__ . '/../../../init.php';
require_once __DIR__ . '/../../../includes/gatewayfunctions.php';
require_once __DIR__ . '/../../../includes/invoicefunctions.php';
	global $customadminpath;
	$debug = '';
	if(
	(isset($_POST['enviar']) && $_POST['enviar'] == '1') &&
	(isset($_POST['percentFee']) && $_POST['percentFee'] != '') &&
	(isset($_POST['percentInterestMonth']) && $_POST['percentInterestMonth'] != '')
	){
		$percentFee				= floatval(str_replace(',','.',$_POST['percentFee']));
		$percentInterestMonth	= floatval(str_replace(',','.',$_POST['percentInterestMonth']));
		if( full_query("UPDATE tbladdonmodules SET value='".$percentFee			."' WHERE module='faturajuros_addon' AND setting='percentFee';") &&
			full_query("UPDATE tbladdonmodules SET value='".$percentInterestMonth	."' WHERE module='faturajuros_addon' AND setting='percentInterestMonth';")){
			
			$vars['percentFee']				= floatval(str_replace(',','.',$_POST['percentFee']));
			$vars['percentInterestMonth']	= floatval(str_replace(',','.',$_POST['percentInterestMonth']));
			$debug .= "Os dados foram alterados com sucesso\n";
		} else {
			$debug .= "Ocorreu um Erro!\n";
		}
	}
	if(
	(isset($_POST['recalcular']) && $_POST['recalcular'] == '1')
	){
		$url = str_replace($customadminpath.DS.'addonmodules.php','',$_SERVER['SCRIPT_URI']).'modules/addons/faturajuros_addon/cron/faturajuros.php';
		//$debug .= var_export($customadminpath,1);
		if( $d = file_get_contents($url)){
			$debug .= $d ;
		} else {
			$debug .= "Ocorreu um Erro ao recalcular!\n";
		}
	}
	$form = '
<div class="callout callout-success">
	<h4>Fatura Juros</h4>
	Addon instalado com sucesso. 
</div>
<div class="border-box">
	<div class="control-group">
	<strong style="color: #cc0000">Nota:</strong><br>
		<strong>Para ativar a rotina de recalcular os juros é necessário incluir nas tarefas agendadas (pelomenos 1 vez ao dia logo após as 0:00):</strong> <br>
		<em>php -q '.dirname(__FILE__).DS.'cron'.DS.'faturajuros.php</em>
	</div>
</div><br /><br />
<fieldset>
	<legend>Alterar Configurações</legend>
	<form action="" method="post" enctype="multipart/form-data" class="form-horizontal">
		<div class="form-group" >
			<label for="percentFee" class="col-sm-2 control-label">% de multa por atraso (ex: 2.00)</label>
			<div class="col-sm-10">
				<input type="text" class="form-control" id="percentFee" name="percentFee" value="'.$vars['percentFee'].'">
			</div>
		</div>
		<div class="form-group" >
			<label for="percentInterestMonth" class="col-sm-2 control-label">% de juros ao mes (ex: 1.00)</label>
			<div class="col-sm-10">
				<input type="text" class="form-control" id="percentInterestMonth" name="percentInterestMonth" value="'.$vars['percentInterestMonth'].'">
			</div>
		</div>
		<!--div class="form-group">
			<label for="ret" class="col-sm-2 control-label">Arquivo de Retorno</label>
			<div class="col-sm-10">
				<input type="file" name="ret[]" id="ret" multiple="multiple" accept=".ret" >
			</div>
		</div-->
		<div class="form-group">
			<div class="col-sm-offset-2 col-sm-10">
				<button type="submit" class="btn btn-success" name="enviar" value="1">Alterar</button>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button type="submit" class="btn btn-default" name="recalcular" value="1">Recalcular Invoices</button>
			</div>
		</div>		
	</form>
</fieldset>
';	
updateInvoiceTotal(50018);
	echo $form;
	echo ($debug!='')? pre2($debug):'';		
	
}

function pre2($v) {
	return '<fieldset><legend>Debug</legend><pre>'.$v.'</pre></fieldset>';
}

?>