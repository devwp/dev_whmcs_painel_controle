<?PHP
/*
*************************************************************

*** SmarterTrack Help Desk Module Commonly Used Functions ***

For more info, please refer to the kb articles
 @   http://portal.smartertools.com

*************************************************************
*/


//	Returns Module Specific Settings in a Dictionary
function smartertrack_settings()
{
  $result = select_query('tbladdonmodules', '', array('module' => 'smartertrack'));
  $settings = array();
  while ($row = mysql_fetch_array($result, MYSQL_ASSOC)) {
   $settings[$row['setting']] = $row['value'];
  }

  $result = select_query('tblconfiguration', '', array('setting' => 'NOMD5'));
  $settings['md5hashenabled'] = false;
  while ($row = mysql_fetch_array($result, MYSQL_ASSOC)) {
	if ($row['value'])
		$settings['md5hashenabled'] = false;
	else
		$settings['md5hashenabled'] = true;
	break;
  }

  return $settings;
}

//	Converts JSON Data Structure from .NET Web Service to Array
function smartertrack_toArray($obj){
    $return=array();
    if (is_array($obj)) {
        $keys=array_keys($obj);
    } elseif (is_object($obj)) {
        $keys=array_keys(get_object_vars($obj));
    } else {return $obj;}
    foreach ($keys as $key) {
        $value=$obj->$key;
        if (is_array($obj)) {
            $return[$key]=smartertrack_toArray($obj[$key]);
        } else {
            $return[$key]=smartertrack_toArray($obj->$key);
        }
    }
    return $return;
}

//	Call .NET Web Service
function smartertrack_webServiceCall($url, $paramaters, $ws_name)
{
	//   array_walk_recursive(
    //                            $paramaters, function (&$value) {
    //                                $value = html_entity_decode($value, ENT_QUOTES, 'UTF-8');
    //                            }
    //                    );
	//echo '<pre>';
	//print_r($paramaters);
	$soap_client = new SoapClient($url);
	$result = $soap_client->__SoapCall($ws_name, array('paramaters' => $paramaters));
	return smartertrack_toArray($result);
}

//	Get Client Info from Custom Module DB Table
function smartertrack_getClient($whmuserid, $st_settings)
{
	global $cc_encryption_hash;
	$client = select_query('smartertrack_whmcs_users', 'st_userid,st_username,st_password, st_email', array('whm_userid' => $whmuserid));
	$client = mysql_fetch_array($client,MYSQL_ASSOC);
	if($client['st_userid'])
	{
		//$st_settings = smartertrack_settings();
		if($st_settings['md5hashenabled'] == false)
		{
			$client['st_password'] = decrypt($client['st_password']);
			$client['st_password'] = ($client['st_password']);
		}
		return $client;
	}
	else
	{
		return null;
	}
}

//	General Error Message - Parsed together with return Error from .NET Web Service
function smartertrack_errorMessage($errormessage, $wsreturn, $st_settings)
{
	//$st_settings = smartertrack_settings();
	$errormessage = "<span style='color: red'>".$errormessage."</span><br>";
	if($wsreturn)
	{
		$errormessage .= "The following error message was returned from SmarterTrack: <br>".$wsreturn;
	}
	if($st_settings)
	{
		$errormessage .= "<br><br>Please report the error details to your service provider at:<br>".$st_settings['helpphone'];
	}
	return $errormessage;
}

//	Get All Departments in SmarterTrack Installation (Excludes Deleted Departments)
function smartertrack_getDepartments($st_settings)
{
	//$st_settings = smartertrack_settings();
	$params = array( "authUserName"=> $st_settings['admin'],
                "authPassword"=> $st_settings['password']
				);

	$departmentsreturn = smartertrack_webServiceCall($st_settings['url']."/services2/svcOrganization.asmx?WSDL", $params, "GetAllDepartments");
	$departments = array();
	if($departmentsreturn["GetAllDepartmentsResult"]["Result"])
	{
		$departmentarray = array();
		if(is_array($departmentsreturn["GetAllDepartmentsResult"]["Departments"]["DepartmentInfo"][0]))
		{
			$departmentarray = $departmentsreturn["GetAllDepartmentsResult"]["Departments"]["DepartmentInfo"];
		}
		else
		{
			$departmentarray = $departmentsreturn["GetAllDepartmentsResult"]["Departments"];
		}
		foreach($departmentarray as $key => $value)
		{
			array_push($departments, array("deptid" => $value["ID"], "name" => $value["Name"]));
		}
		return $departments;
	}
	else
	{
		return null;
	}

}

//	Get All Departments from SmarterTrack Installation that is viewable by an End User
function smartertrack_GetDeptsForUserTicketSubmission($st_settings)
{
	//$st_settings = smartertrack_settings();
	$params = array( "authUserName"=> $st_settings['admin'],
                "authPassword"=> $st_settings['password']
				);

	$departmentsreturn = smartertrack_webServiceCall($st_settings['url']."/services2/svcOrganization.asmx?WSDL", $params, "GetDeptsForUserTicketSubmission");
	$departments = array();
	if($departmentsreturn["GetDeptsForUserTicketSubmissionResult"]["Result"])
	{
		$departmentarray = array();
		if(is_array($departmentsreturn["GetDeptsForUserTicketSubmissionResult"]["Departments"]["DepartmentInfo"][0]))
		{
			$departmentarray = $departmentsreturn["GetDeptsForUserTicketSubmissionResult"]["Departments"]["DepartmentInfo"];
		}
		else
		{
			$departmentarray = $departmentsreturn["GetDeptsForUserTicketSubmissionResult"]["Departments"];
		}
		foreach($departmentarray as $key => $value)
		{
			array_push($departments, array("deptid" => $value["ID"], "name" => $value["Name"]));
		}
		return $departments;
	}
	else
	{
		return null;
	}

}

//	Get Ticket Status Text
function smartertrack_getTicketStatus($openbool, $activebool)
{
  global $_LANG;
  //global $whmcs;


	if($openbool == true && $activebool == true)
	{
    //print_r($LANG);
		return "<span style='color: green' >".$_LANG['supportticketsstatusopen']."</span>";
	}
	else if($openbool == true && $activebool == false)
	{
		return "<span style='color: #db8e00' >".$_LANG['supportticketsstatusanswered']."</span>";
	}
	else
	{
		return "<span style='color: red' >".$_LANG['supportticketsstatusclosed']."</span>";
	}
}
function smartertrack_getTicketStatusColor($openbool, $activebool)
{
	if($openbool == true && $activebool == true)
	{
		return 'green';
	}
	else if($openbool == true && $activebool == false)
	{
		return '#db8e00';
	}
	else
	{
		return 'red';
	}
}
function smartertrack_getTicketStatusClass($openbool, $activebool)
{
	if($openbool == true && $activebool == true)
	{
		return 'active';
	}
	else if($openbool == true && $activebool == false)
	{
		return 'suspended';
	}
	else
	{
		return 'closed';
	}
}

//	Get Ticket Priority Text
function smartertrack_getTicketPriority($priority)
{
	switch($priority)
	{
		case 1:
			return "Low";
			break;
		case 2:
			return "Normal";
			break;
		case 3:
			return "High";
			break;
		case 4:
			return "Urgent";
			break;
	}
}
function smartertrack_translateStatus($string){
  return 'langggggg';
}
?>
