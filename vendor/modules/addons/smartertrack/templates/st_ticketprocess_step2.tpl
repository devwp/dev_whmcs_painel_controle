{**}
{if $errormessage}
    {include file="$template/includes/alert.tpl" type="error" errorshtml=$errormessage}
{/if}
<form name="submitticket" method="post" action="{$smarty.server.PHP_SELF}?step=2" enctype="multipart/form-data" id="submitticket" class="form-horizontal">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">{$LANG.navopenticket}</h3>
				</div>
				<div class="panel-body">
					<fieldset>
						<div class="form-group">
							<label class="col-sm-3 control-label" for="inputName">{$LANG.clientareafullname}</label>
							<div class="col-sm-6">
								{if $loggedin}
									<input class="form-control disabled" type="text" name="name" id="inputName" value="{$name}{$clientname}" disabled="disabled" />
								{else}
									<input class="form-control" type="text" name="name" id="inputName" value="{$name}" />
								{/if}
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label" for="inputEmail">{$LANG.supportticketsclientemail}</label>
							<div class="col-sm-6">
								{if $loggedin}
									<input class="form-control disabled" type="text" name="email" id="inputEmail" value="{$email}" disabled="disabled" />
								{else}
									<input class="form-control" type="text" name="email" id="inputEmail" value="{$email}" />
								{/if}
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label" for="inputSubject">{$LANG.supportticketsticketsubject}</label>
							<div class="col-sm-6">
								<input class="form-control" type="text" name="subject" id="inputSubject" value="{$subject}" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label" for="inputDepartment">{$LANG.supportticketsdepartment}</label>
							<div class="col-sm-6">




                {if $deptname}
                <select name="deptname" class="form-control">
                  {foreach from=$departments item=department}
                  {if $department.name eq $deptname}
                    <option value="{$department.name}" selected="selected">
                      {$department.name}
                    </option>
                  {/if}
                  {/foreach}
                </select>
    						{else}
                <select name="deptid" class="form-control">
                  {foreach from=$departments item=department}
                  {if $department.deptid eq $deptid}
                    <option value="{$department.deptid}" selected="selected">
                      {$department.name}
                    </option>
                  {/if}
                  {/foreach}
                </select>
    						{/if}
							</div>
						</div>
						{if $relatedservices}
							<div class="form-group">
								<label class="col-sm-3 control-label" for="inputRelatedService">{$LANG.relatedservice}</label>
								<div class="col-sm-6">
									<select name="relatedservice" id="inputRelatedService" class="form-control">
										<option value="">{$LANG.none}</option>
										{foreach from=$relatedservices item=relatedservice}
											<option value="{$relatedservice.id}">
												{$relatedservice.name} ({$relatedservice.status})
											</option>
										{/foreach}
									</select>
								</div>
							</div>
						{/if}
            <input type="hidden" name="urgency" value="Medium" />
						<!--div class="form-group">
							<label class="col-sm-3 control-label" for="inputPriority">{$LANG.supportticketspriority}</label>
							<div class="col-sm-6">
								<select name="urgency" id="inputPriority" class="form-control">
									<option value="High"{if $urgency eq "High"} selected="selected"{/if}>
										{$LANG.supportticketsticketurgencyhigh}
									</option>
									<option value="Medium"{if $urgency eq "Medium" || !$urgency} selected="selected"{/if}>
										{$LANG.supportticketsticketurgencymedium}
									</option>
									<option value="Low"{if $urgency eq "Low"} selected="selected"{/if}>
										{$LANG.supportticketsticketurgencylow}
									</option>
								</select>
							</div>
						</div-->
            <div id="customFieldsContainer">
              {foreach key=num item=customfield from=$customfields}
              {if $customfield.IsVisibleToUsers == "true"}
              	<div class="form-group">
              		<!--label class="col-sm-3 control-label" for="customfield{$customfield.id}">{$customfield.name}</label-->
              		<label class="col-sm-3 control-label" for="{$customfield.ID}_value">{$customfield.DisplayName}</label>
              		<div class="col-sm-6">
                    {if $customfield.DataType == 'Choice' or $customfield.DataType == 'ChoiceWithFillIn'}
                      <select class="form-control" name="{$customfield.ID}_value">
                        {foreach from=$customfield.Options.string item=option}
                          <option value="{$option}" {if !$ispost && $customfield.DefaultValue == $option}
                          selected="selected"{elseif $customfield.Value == $option}
                          selected="selected"{/if}>{$option}</option>
                        {/foreach}
                      </select>
                    {elseif $customfield.DataType == 'Boolean'}
                      <select class="form-control" name="{$customfield.ID}_value">
                        <option value="False" {if !$ispost && $customfield.DefaultValue == "false"}
                        selected="selected"{elseif $customfield.Value == "False"}selected="selected"{/if}" >False</option>
                        <option value="True" {if !$ispost && $customfield.DefaultValue == "true"}
                        selected="selected"{elseif $customfield.Value ==
                        "True"}selected="selected"{/if}" >True</option>
                      </select>
                    {elseif $customfield.DataType == 'MultipleLineText'}
                      <textarea  class="form-control" name="{$customfield.ID}_value" rows="5">{if  !$ispost}{$customfield.DefaultValue}{else}{$customfield.Value}{/if}</textarea>
                    {else}
                      <input class="form-control" type="textarea" name="{$customfield.ID}_value" size="40" value="{$customfield.DefaultValue}" />
                    {/if}

                    {if $customfield.description}
                      <p class="help-block">{$customfield.description}</p>
                    {/if}
              		</div>
              	</div>
              {/if}
              {/foreach}
						</div>

						<div class="form-group">
							<label class="col-sm-3 control-label" for="inputMessage">{$LANG.contactmessage}</label>
							<div class="col-sm-6">
								<textarea name="message" id="message" rows="12" class="form-control">{$message}</textarea>
							</div>
						</div>
						<!--hr />
						<div class="form-group">
							<label class="col-sm-3 control-label" for="inputAttachments">{$LANG.supportticketsticketattachments}</label>
							<div class="col-sm-6">
								<input type="file" name="attachments[]" id="inputAttachments" class="form-control" />
								<div id="fileUploadsContainer"></div>
								<p class="help-block">{$LANG.supportticketsallowedextensions}: {$allowedfiletypes}</p>
							</div>
							<div class="col-sm-3">
								<button type="button" class="btn btn-default btn-sm" onclick="extraTicketAttachment()">
									<i class="fa fa-plus"></i> {$LANG.addmore}
								</button>
							</div>
						</div-->
					</fieldset>
					<div id="autoAnswerSuggestions" class="hidden"></div>
					{include file="$template/includes/captcha.tpl"}
				</div>
				<div class="panel-footer">
					<input type="submit"  name="save"  id="openTicketSubmit" value="{$LANG.supportticketsticketsubmit}" class="res-100 btn btn-primary btn-3d" />
			        <a href="supporttickets.php" class="btn btn-default pull-right res-100 res-left">{$LANG.cancel}</a>
				</div>
			</div>
		</div>
	</div>
</form>
{if $kbsuggestions}
    <script>
        jQuery(document).ready(function() {
            getTicketSuggestions();
        });
    </script>
{/if}





{**}{*

{if $errormessage}
<div class="alert-message error">
    <p>{$errormessage}</p>
</div>
{/if}
<div class="page-header">
    <div class="styled_title"><h1>Open Ticket</h1></div>
</div>
{if $throwcferror}
	<h4><span style='color: red' >{$throwcferror}</span></h4>
{/if}

<form name="submitticket" method="post" action="{$smarty.server.PHP_SELF}?step=2" enctype="multipart/form-data" id="submitticket">
		<table width="100%" cellspacing="1" cellpadding="0" class="frame">
			<tr>
				<td><table width="100%" border="0" cellpadding="10" cellspacing="0">
					<tr>
						<td width="120" class="fieldarea">{$LANG.clientareafullname}</td>
						{if $deptname}
							<td><input type="textarea" name="name" size="40" value="{$name}" /></td>
						{else}
						<td>{$name}</td>
						{/if}
					</tr>
					<tr>
						<td class="fieldarea">{$LANG.clientareaemail}</td>
						{if $deptname}
							<td><input type="textarea" name="email" size="40" value="{$email}" /></td>
						{else}
						<td>{$email}</td>
						{/if}
					</tr>
					<tr>
						<td class="fieldarea">{$LANG.supportticketsdepartment}</td>
						{if $deptname}
						<td><select name="deptname">
                        {foreach from=$departments item=department}
                            {if $department.name eq $deptname}<option value="{$department.name}" selected="selected">{$department.name}</option>{/if}
                        {/foreach}
                        </select></td>
						{else}
						<td><select name="deptid">
                        {foreach from=$departments item=department}
                            {if $department.deptid eq $deptid}<option value="{$department.deptid}" selected="selected">{$department.name}</option>{/if}
                        {/foreach}
                        </select></td>
						{/if}
					</tr>
					<tr>
						<td class="fieldarea">{$LANG.supportticketssubject}</td>
						<td><input type="text" name="subject" size="60" value="{$subject}" /></td>
					</tr>
					{foreach from=$customfields item=customfield}
					{if $customfield.IsVisibleToUsers == "true"}
						<tr>
							<td class="fieldarea">{$customfield.DisplayName}</td>
								{if $customfield.DataType == 'Choice' or $customfield.DataType == 'ChoiceWithFillIn'}
									<td><select name="{$customfield.ID}_value">
									{foreach from=$customfield.Options.string item=option}
										<option value="{$option}" {if !$ispost && $customfield.DefaultValue == $option}
										selected="selected"{elseif $customfield.Value == $option}
										selected="selected"{/if}>{$option}</option>
									{/foreach}
									</select></td>
								{elseif $customfield.DataType == 'Boolean'}
									<td><select name="{$customfield.ID}_value">
										<option value="False" {if !$ispost && $customfield.DefaultValue == "false"}
										selected="selected"{elseif $customfield.Value == "False"}selected="selected"{/if}" >False</option>
										<option value="True" {if !$ispost && $customfield.DefaultValue == "true"}
										selected="selected"{elseif $customfield.Value ==
										"True"}selected="selected"{/if}" >True</option>
									</select></td>
								{elseif $customfield.DataType == 'MultipleLineText'}
									<td colspan="2"><textarea name="{$customfield.ID}_value" rows="5" cols="60" style="width:100%">{if  !$ispost}{$customfield.DefaultValue}{else}{$customfield.Value}{/if}</textarea></td>
								{else}
									<td><input type="textarea" name="{$customfield.ID}_value" size="40" value="{$customfield.DefaultValue}" /></td>
								{/if}
						</tr>
					{/if}
					{/foreach}
                    <tr>
						<td colspan="2" class="fieldarea"><textarea name="message" id="message" rows="12" cols="60" style="width:100%">{$message}</textarea></td>
					</tr>
		        </table></td>
			</tr>
		</table>
		<br />


<div class="actions">
    <input class="btn primary" type="submit" name="save" value="{$LANG.supportticketsticketsubmit}" />
    <input class="btn" type="reset" value="{$LANG.cancel}" onclick="window.location='supporttickets.php'" />
</div>

</form>
*}
