<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title">{$LANG.clientareanavsupporttickets}</h3>
			</div>
			<div class="panel-body">
				{include file="$template/includes/tablelist.tpl" tableName="TicketsList" filterColumn="2"}
				<script type="text/javascript">
					jQuery(document).ready( function ()
					{
						var table = $('#tableTicketsList').DataTable();
						{if $orderby == 'did' || $orderby == 'dept'}
							table.order(1, '{$sort}');
						{elseif $orderby == 'subject' || $orderby == 'title'}
							table.order(0, '{$sort}');
						{elseif $orderby == 'status'}
							table.order(2, '{$sort}');
						{elseif $orderby == 'lastreply'}
							table.order(3, '{$sort}');
						{/if}
						table.draw();
					});
				</script>
				<div class="table-container clearfix">
					<table id="tableTicketsList" class="table table-bordered table-hover table-list" width="100%">
						<thead>
							<tr>
								<th>{$LANG.supportticketsticketid}</th>
								<th>{$LANG.supportticketssubject}</th>
								<th>{$LANG.supportticketsdepartment}</th>
								<th>{$LANG.supportticketsstatus}</th>
								<!--th>{$LANG.priority}</th-->
								<th>{$LANG.supportticketsticketlastupdated}</th>
							</tr>
						</thead>
						<tbody>
              {foreach key=num item=ticket from=$tickets}
								<tr>
									<td><a href="viewticket.php?tid={$ticket.tid}&amp;c={$ticket.c}">{if $ticket.unread}<strong>{/if}{$ticket.tid}{if $ticket.unread}</strong>{/if}</a></td>
									<td><a href="viewticket.php?tid={$ticket.tid}&amp;c={$ticket.c}">{if $ticket.unread}<strong>{/if}{$ticket.subject}{if $ticket.unread}</strong>{/if}</a></td>
									<td>{$ticket.department}</td>
									<!--td><span class="label status {if is_null($ticket.statusColor)}status-{$ticket.statusClass}"{else}status-custom" style="border-color: {$ticket.statusColor}; color: {$ticket.statusColor}"{/if}>{$ticket.status|strip_tags}</span></td-->
									<td><span class="label status {if is_null($ticket.statusColor)}status-{$ticket.statusClass}"{else}status-custom" style="border-color: {$ticket.statusColor}; color: {$ticket.statusColor}"{/if}>{$ticket.status|strip_tags}</span></td>
                  <!--td>{$ticket.urgency}</td-->
									<td><span class="hidden">{$ticket.normalisedLastReply}</span>{$ticket.lastreply}</td>
								</tr>
							{/foreach}
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

{*
<p><input type="button" value="{$LANG.opennewticket}" onclick="window.location='submitticket.php'" /></p>
{if $errormessage}
<div class="alert-message error">
    <p>{$errormessage}</p>
</div>
{/if}
{if $st_ticketsubmitsuccess}
<h4><span style='color: green' >{$st_ticketsubmitsuccess}</span></h4>
{/if}
<p>{$numtickets} {$LANG.recordsfound}</p>

<table class="data table table-bordered table-hover table-list dataTable no-footer dtr-inline" width="100%" border="0" align="center" cellpadding="10" cellspacing="0">
    <thead>
        <tr>
            <th>{$LANG.supportticketsdepartment}</th>
            <th>{$LANG.supportticketssubject}</th>
            <th>{$LANG.supportticketsstatus}</th>
			<th>Priority</th>
            <th>{$LANG.supportticketsticketlastupdated}</th>
        </tr>
    </thead>
    <tbody>
{foreach key=num item=ticket from=$tickets}
        <tr>
<td>{$ticket.department}</td>
<td><img src="images/article.gif" hspace="5" align="middle"><a href="viewticket.php?tid={$ticket.tid}&amp;c={$ticket.c}">#{$ticket.tid} - {$ticket.subject}</a></td>
<td>{$ticket.status}</td>
<td>{$ticket.urgency}</td>
<td>{$ticket.lastreply}</td>
        </tr>
{foreachelse}
        <tr>
            <td colspan="7" class="textcenter">{$LANG.norecordsfound}</td>
        </tr>
{/foreach}
    </tbody>
</table>
<br>
<br>
<br>
<br>
*}
