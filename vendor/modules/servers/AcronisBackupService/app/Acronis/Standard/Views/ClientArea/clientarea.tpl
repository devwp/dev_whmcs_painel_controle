{foreach from=$js_paths item=path}
    <script type="text/javascript" src="{$path}"></script>
{/foreach}

{foreach from=$css_paths item=path}
    <link rel="stylesheet" type="text/css" href="{$path}">
{/foreach}


<style type="text/css">
    {literal}
    #mg-wrapper {
        text-align: left;
    }

    #mg-wrapper span.onhover {
        display: none;
    }

    #mg-wrapper p.leftp {
        text-align: left;
    }

    #mg-wrapper .table td.cell {
        width: 40%;
    }

    #mg-wrapper .table td.cell:nth-child(2) {
        width: 60%;
    }

    #mg-wrapper .progress-bar-info span {
        color: black;
    }

    {/literal}
</style>


<div id="mg-wrapper">

    {if count($errors) > 0 || count($infos) > 0}
        {foreach from=$errors item=error}
            <div class="alert alert-error" role="alert">
                <a class="close" data-dismiss="alert" href="#">&amp;times;</a>
                <p class="leftp"><span class="bold">{$lang.error}</span> {$error}</p>
            </div>
        {/foreach}
        {foreach from=$infos item=info}
            <div class="alert alert-info" role="alert">
                <a class="close" data-dismiss="alert" href="#">&amp;times;</a>
                <p class="leftp">{$info}</p>
            </div>
        {/foreach}
    {/if}

    <table width="100%">
        <tr>
            <td width="70%">
                <table width="100%">
                    {if !$admAccount}
                        <tr>
                            <td colspan="2">
                                <span style="color:red">{$lang.clientarea.accountnotfound}</span>
                            </td>
                        </tr>
                    {/if}
                    <tr>
                        {if $admAccount}
                            <td width="40%">{$lang.field.login}:</td>
                            <td>{$admAccount.login}</td>
                        {else}
                            <td width="30%"><a class="btn btn-default"
                                               href="{$module_links.product_custom_button_action}&a=Management&act=AddAccount">{$lang.add}</a>
                            </td>
                            <td></td>
                        {/if}

                    </tr>
                </table>
            </td>
            <td alignt="right">
                {if $admAccount}
                    <input class="btn" type="button" onclick='window.open("{$loginlink}");' value="{$buttonTitle}"/>
                {/if}
            </td>
        </tr>
    </table>

    <hr>


    <h3>{$lang.group_usage_details}</h3>

    <table class="table">

        {if $usagedata.server_count_show}
            <tr class="row">
                <td class="cell">
                    <p class="leftp">{$lang.used_servers_count}</p>
                </td>
                <td class="cell">
                    <div style="width:100%">
                        <div>
                <span>
                    {if $usagedata.server_count_friendly != ""} {$lang.clientarea.used} {$usagedata.server_count_friendly}
                        {if $usagedata.server_count_limit != 0}
                            {$lang.clientarea.of} {$usagedata.server_count_limit_friendly}
                        {/if}
                    {/if}
                </span>
                            {if $usagedata.server_count_warning}
                                <br>
                                <span style="color:red;">{$usagedata.server_count_warning}</span>
                            {/if}
                        </div>
                    </div>
                </td>
            </tr>
        {/if}

        <tr class="row">
            <td class="cell">
                <p class="leftp">{$lang.used_storage_size}</p>
            </td>
            <td class="cell">
                <div style="width:100%">
                    <div>
                <span>
                    {if $usagedata.storage_size_friendly != ""} {$lang.clientarea.used} {$usagedata.storage_size_friendly}
                    {if $usagedata.storage_size_limit != 0}
                        {$lang.clientarea.of} {$usagedata.storage_size_limit_friendly}
                    {/if}
                </span>
                        {/if}
                        {if $usagedata.storage_size_warning}
                            <br>
                            <span style="color:red;">{$usagedata.storage_size_warning}</span>
                        {/if}
                    </div>
                </div>
            </td>
        </tr>

        {if $usagedata.vm_count_show }
            <tr class="row">
                <td class="cell">
                    <p class="leftp">{$lang.used_vm_count}</p>
                </td>
                <td class="cell">
                    <div style="width:100%">
                        <div>
                <span>
                    {if $usagedata.vm_count_friendly != ""} {$lang.clientarea.used} {$usagedata.vm_count_friendly}
                        {if $usagedata.vm_count_limit != 0}
                            {$lang.clientarea.of} {$usagedata.vm_count_limit_friendly}
                        {/if}
                    {/if}
                </span>
                            {if $usagedata.vm_count_warning}
                                <br>
                                <span style="color:red;">{$usagedata.vm_count_warning}</span>
                            {/if}
                        </div>
                    </div>
                </td>
            </tr>
        {/if}

        {if $usagedata.workstation_count_show}
            <tr class="row">
                <td class="cell">
                    <p class="leftp">{$lang.used_workstations_count}</p>
                </td>
                <td class="cell">
                    <div style="width:100%">
                        <div>
                <span>
                    {if $usagedata.workstation_count_friendly != ""} {$lang.clientarea.used} {$usagedata.workstation_count_friendly}
                    {if $usagedata.workstation_count_limit != 0}
                        {$lang.clientarea.of} {$usagedata.workstation_count_limit_friendly}
                    {/if}
                </span>
                            {/if}
                            {if $usagedata.workstation_count_warning}
                                <br>
                                <span style="color:red;">{$usagedata.workstation_count_warning}</span>
                            {/if}
                        </div>
                    </div>
                </td>
            </tr>
        {/if}

        {if $usagedata.mobile_count_show}
            <tr class="row">
                <td class="cell">
                    <p class="leftp">{$lang.used_mobile_devices_count}</p>
                </td>
                <td class="cell">
                    <div style="width:100%">
                        <div>
                <span>
                    {if $usagedata.mobile_count_friendly != ""} {$lang.clientarea.used} {$usagedata.mobile_count_friendly}
                    {if $usagedata.mobile_count_limit != 0}
                        {$lang.clientarea.of} {$usagedata.mobile_count_limit_friendly}
                    {/if}
                </span>
                            {/if}
                            {if $usagedata.mobile_count_warning}
                                <br>
                                <span style="color:red;">{$usagedata.mobile_count_warning}</span>
                            {/if}
                        </div>
                    </div>
                </td>
            </tr>
        {/if}

        {if $usagedata.mailbox_count_show}
            <tr class="row">
                <td class="cell">
                    <p class="leftp">{$lang.used_mailboxes_count}</p>
                </td>
                <td class="cell">
                    <div style="width:100%">
                        <div>
                <span>
                    {if $usagedata.mailbox_count_friendly != ""} {$lang.clientarea.used} {$usagedata.mailbox_count_friendly}
                    {if $usagedata.mailbox_count_limit != 0}
                        {$lang.clientarea.of} {$usagedata.mailbox_count_limit_friendly}
                    {/if}
                </span>
                            {/if}
                            {if $usagedata.mailbox_count_warning}
                                <br>
                                <span style="color:red;">{$usagedata.mailbox_count_warning}</span>
                            {/if}
                        </div>
                    </div>
                </td>
            </tr>
        {/if}


    </table>

</div>
