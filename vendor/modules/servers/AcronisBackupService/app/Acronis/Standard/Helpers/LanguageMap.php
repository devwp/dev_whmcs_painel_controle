<?php
/**
 *
 * Acronis modules specific helpers
 * @version 1.0.0
 * @Copyright © 2002-2016 Acronis International GmbH. All rights reserved
 * DEPENDENCIES: \Acronis\WHMCS\Lang
 */
namespace Acronis\Standard\Helpers;

class LanguageMap
{
    const DEFAULT_LANGUAGE_ISO = 'en';

    /**
     * returns an  correct ISO language code for setting to ABC by whmcs language name
     *
     * @param string $whmcsLangName language name from WHMCS
     * @return string
     */
    public static function getISOLanguageCodeByWHMCSLanguage($whmcsLangName)
    {
        if (!file_exists(ISO_LANGUAGE_MAP) || !is_readable(ISO_LANGUAGE_MAP)) {
            return self::DEFAULT_LANGUAGE_ISO;
        }
        $languageMap = json_decode(file_get_contents(ISO_LANGUAGE_MAP), true);
        return isset($languageMap[$whmcsLangName]) ? $languageMap[$whmcsLangName] : self::DEFAULT_LANGUAGE_ISO;
    }
}
