<?php
/**
 *
 * @Copyright © 2002-2016 Acronis International GmbH. All rights reserved
 */
namespace Acronis\Standard\Controllers;

use Acronis\ApiManagers\SystemManager;
use Acronis\Helpers\AcronisParams;
use Acronis\Helpers\AcronisViews as AcronisViewsHelper;
use Acronis\Helpers\ModuleVars;
use Acronis\Helpers\RequestCleaner;
use Acronis\Standard\Helpers\AcronisForm as AcronisFormHelper;
use Acronis\System\MasterController;
use Acronis\UserException;

class Management extends MasterController
{
    private $success = 0;
    private $acronisParams = null;
    private $moduleVars = null;
    private $acronisViewsHelper = null;
    private $acronisFormHelper = null;
    private $manager = null;

    public function boot($controller, $action)
    {
        //classes initialization:-------------------------------------------------------------------------------------------
        $this->acronisParams = new AcronisParams($this->params, $this->moduleConfiguration);
        $this->moduleVars = new ModuleVars($this->moduleConfiguration->moduleName);
        $this->acronisViewsHelper = new AcronisViewsHelper($this->lang);
        $this->manager = new SystemManager($this->acronisParams->getServerUrl(), $this->moduleConfiguration, $this->moduleVars);


        //login to system:--------------------------------------------------------------------------------------------------
        $this->manager->accountsManager->login($this->params["serverusername"], $this->params["serverpassword"]);

        $content = '';

        //booting action:
        $content = '';
        if (method_exists($this, $action)) {
            $content = $this->$action();
        } else {
            $content = $this->loadTpl('notfound.tpl');
        }

        //rendering template:-----------------------------------------------------------------------------------------------
        $template = $this->loadTpl('Management' . DS . 'mainsite.tpl');

        $template->setVar('content', $content);
        $template->setVar('success', $this->success);

        return $this->renderTpl($template);
    }


    public function actionAccounts()
    {
        if (isset($_GET['success']))
            $this->success = 1;

        $template = $this->loadTpl('Management' . DS . 'accounts.tpl');

        $template->setVar('accounts', $accounts);
        $template->setVar('success', $this->success);

        return $this->renderTpl($template);
    }


    public function actionAddAccount()
    {
        RequestCleaner::init('INPUT_POST');
        $postdata = RequestCleaner::getAllVarsRaw();

        $do = RequestCleaner::getVar('do');
        try {
            if ("add_account" === $do) {
                $filter = new AcronisFormHelper($this->moduleConfiguration, $this->lang, $postdata);

                $filter->validateLogin();
                $filter->validateEmail();
                $filter->validateInput('firstname');
                $filter->validateInput('lastname');
                $filter->validatePassword();

                $filtered_post = $filter->getFilteredPost();
                $group_id = $this->acronisParams->getCustomFieldFromVars('GroupID');

                //create administrator info
                $this->manager->adminsManager->fillAdminData($filtered_post["login"],
                    $filtered_post["firstname"],
                    $filtered_post["lastname"],
                    $filtered_post["email"]);

                $this->manager->adminsManager->createAdmin($group_id, $filtered_post["password"]);

                \Acronis\WHMCS\Orders::updateServiceAdministrator($filtered_post["login"], $filtered_post["password"], $_REQUEST['id']);

                //If we are here everything is ok:
                $this->redirect($this->getStartupURL(), array('success' => 1));

            }
        } catch (UserException $e) {
            $this->errors[] = $e->getMessage();
        }

        $template = $this->loadTpl('Management' . DS . 'add_account.tpl');

        $template->setVar('data', $postdata);

        return $this->renderTpl($template);
    }


}