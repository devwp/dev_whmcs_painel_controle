<?php
/**
 *
 * @Copyright © 2002-2016 Acronis International GmbH. All rights reserved
 */

use Acronis\Log;
use Acronis\WHMCS\Lang;

require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . 'Config.php';


function AcronisBackupService_ConfigOptions()
{
    $al = AcronisBackupService_loadAutoloader();
    $config = new Acronis\Standard\Configs\ModuleConfig(true, false);
    $eh = AcronisBackupService_loadErrorHandler($config);
    $moduleVars = new Acronis\Helpers\ModuleVars($config->moduleName);

    Log::getInstance()->event(__FUNCTION__ . ' was called');

    $storageGateway = new \Acronis\Model\Storage($moduleVars, $config);
    $config_array = $config->configArray;
    try {
        $config_array['accountStorage']['Options'] = $storageGateway->getStorageListAsString();
    } catch (Exception $e) {
        $config_array = $config->showExceptionMessage($e->getMessage());
    }

    AcronisBackupService_unloadErrorHandler($eh);
    AcronisBackupService_unloadAutoloader($al);

    return $config_array;
}

function AcronisBackupService_CreateAccount($vars)
{
    $al = AcronisBackupService_loadAutoloader();
    $config = new \Acronis\Standard\Configs\ModuleConfig(true);
    $eh = AcronisBackupService_loadErrorHandler($config);
    $lang = new Lang(ACRONIS_INCLUDES_DIR . 'Lang' . DS);

    Log::getInstance()->event(__FUNCTION__ . ' was called');

    $result = 'success';

    try {
        //classes initialization:-------------------------------------------------------------------------------------------
        $params = new Acronis\Helpers\AcronisParams($vars, $config);
        $module_vars = new Acronis\Helpers\ModuleVars($config->moduleName);
        $custom_fields = new Acronis\WHMCS\CustomFields($config->moduleName);
        $manager = new Acronis\ApiManagers\SystemManager($params->getServerUrl(), $config, $module_vars);

        //login to system:--------------------------------------------------------------------------------------------------
        $manager->accountsManager->login($vars["serverusername"], $vars["serverpassword"]);

        $groupId = $custom_fields->get($vars['pid'], $vars['serviceid'], 'GroupID');
        if (!$groupId) {

            //creating group:---------------------------------------------------------------------------------------------------
            $backupService = new \Acronis\Model\BackupService($module_vars, $config);

            //determining final group kind :---------------------------------------------------------------------------------
            $selected_group_kind = $config->getGroupKindByType($params->getConfigOption($config->getId('accountType'), false));

            $backupService->setBackupLocations($config->getBackupLocationsValueByOption($params->getConfigOption($config->getId('storageLocation'), false, "unlimited", false)));
            //load quota values
            $backupService->setServers($params->getConfigOption($config->getId('maxServers'), true, 'unlimited', true));
            $backupService->setStorageSize($params->getConfigOption($config->getId('maxStorageGB'), true, 'unlimited', true));
            $backupService->setVms($params->getConfigOption($config->getId('maxVMs'), true, 'unlimited', true));
            $backupService->setWorkstations($params->getConfigOption($config->getId('maxWorkstations'), true, 'unlimited', true));
            $backupService->setMobileDevices($params->getConfigOption($config->getId('maxMobileDevices'), true, 'unlimited', false));
            $backupService->setO365Mailboxes($params->getConfigOption($config->getId('maxO365Mailboxes'), true, 'unlimited', false));

            //modify quota from configurable options of product
            $backupService->loadConfigurableOptions($vars['configoptions']);

            $groupName = empty($vars['clientsdetails']['companyname']) ? $vars['clientsdetails']['firstname'] . ' ' . $vars['clientsdetails']['lastname'] : $vars['clientsdetails']['companyname'];

            $group_data = array
            (
                "kind" => $selected_group_kind,
                "contact" => array
                (
                    "address1" => $vars['clientsdetails']['address1'],
                    "address2" => $vars['clientsdetails']['address2'],
                    "city" => $vars['clientsdetails']['city'],
                    "country" => $vars['clientsdetails']['country'],
                    "email" => $vars['clientsdetails']['email'],
                    "firstname" => $vars['clientsdetails']['firstname'],
                    "lastname" => $vars['clientsdetails']['lastname'],
                    "phone" => $vars['clientsdetails']['phonenumber'],
                    "state" => $vars['clientsdetails']['state'],
                    "zipcode" => $vars['clientsdetails']['postcode']
                ),
                "language" => $lang->getLanguageForABC($vars['userid']),
                "name" => $groupName,
                "privileges" => array
                (
                    "local_backup" => array("permitted" => (int)$backupService->getBackupLocations()),
                    "server_count" => $backupService->getServers(),
                    "quota_servers_hard" => $backupService->getServers(),
                    "storage_size" => $backupService->getStorageSize(),
                    "quota_storage_hard" => $backupService->getStorageSize(),
                    "vm_count" => $backupService->getVms(),
                    "quota_vms_hard" => $backupService->getVms(),
                    "workstation_count" => $backupService->getWorkstations(),
                    "quota_workstations_hard" => $backupService->getWorkstations(),
                    "mailbox_count" => (int)$backupService->getO365Mailboxes(),
                    "quota_mailboxes_hard" => (int)$backupService->getO365Mailboxes(),
                    "mobile_count" => (int)$backupService->getMobileDevices(),
                    "quota_mobiles_hard" => (int)$backupService->getMobileDevices(),
                ),
            );

            //if creating group is not reseller
            if ($selected_group_kind == $config->getGroupKindByType(\Acronis\Standard\Configs\ModuleConfig::GROUP_TYPE_CUSTOMER)) {
                //init pricing mode
                $productData = \Acronis\WHMCS\Products::fetchProductDataById($vars['pid']);
                if ($productData) {
                    $groupPricingMode = $backupService->detectPricingModeFromProductData($productData);
                    $group_data['pricing'] = array('mode' => $groupPricingMode);
                }

                //and storage value is not null
                $storageHelper = new \Acronis\Model\Storage($module_vars, $config);
                if ($storageValue = $storageHelper->getStorageIdByOptionValue($params->getConfigOption($config->getId('accountStorage'), false))) {
                    //set specified storage from product config
                    $group_data['storage'] = (string)$storageValue;
                }
            }

            Log::getInstance()->debug('Trying to create group with data:' . print_r($group_data, true));

            //storage id is determined and added inside create_group function
            list($groupId, $new_group_version_trash) = $manager->groupsManager->createGroup($group_data, $manager->serverAdminGroupId);

            //save group id to customfields:
            $custom_fields->set($vars['pid'], $vars['serviceid'], 'GroupID', $groupId);
        } else {
            Log::getInstance()->event("Trying to resubmit order {$vars['serviceid']} for user {$vars['uid']}. Group ID already exists = {$groupId}.");
        }

        $adminId = $custom_fields->get($vars['pid'], $vars['serviceid'], 'AdminID');

        if (!$adminId) {

            $sessionCredentials = \Acronis\WHMCS\Orders::getCredentialsFromSession($_SESSION, $vars['userid'], $vars['pid']);
            //try to get admin username from session
            if (isset($sessionCredentials['username']) && !is_null($sessionCredentials['username'])) {
                $username = $sessionCredentials['username'];
            } //if there is no username in session - use from vars params (autogenerated values if autoprovisioning is on)
            else {
                $username = $vars['username'];
            }

            //try to get admin password from session
            if (isset($sessionCredentials['password']) && !is_null($sessionCredentials['password'])) {
                $password = $sessionCredentials['password'];
            } //if there is no password in session - use from vars params (autogenerated values if autoprovisioning is on)
            else {
                $password = $vars['password'];
            }

            //create administrator info
            $manager->adminsManager->fillAdminData($username, $vars['clientsdetails']['firstname'], $vars['clientsdetails']['lastname'], $vars['clientsdetails']['email']);

            list($adminId, $new_admin_version_trash) = $manager->adminsManager->createAdmin($groupId, $password);

            $custom_fields->set($vars['pid'], $vars['serviceid'], 'AdminID', $adminId);
        } else {
            Log::getInstance()->event("Trying to resubmit order {$vars['serviceid']} and recreate Admin. Admin ID already exists = {$adminId}.");
            $result = $config->lang->get('cant_resubmit_success_order');
        }
    } catch (Acronis\UserException $e) {
        $result = $config->lang->get('error') . ' ' . $e->getMessage();
    }

    AcronisBackupService_unloadErrorHandler($eh);
    AcronisBackupService_unloadAutoloader($al);

    //Outputting:
    return $result;

}


function AcronisBackupService_TerminateAccount($vars)
{

    $al = AcronisBackupService_loadAutoloader();
    $config = new \Acronis\Standard\Configs\ModuleConfig(true);
    $eh = AcronisBackupService_loadErrorHandler($config);

    Log::getInstance()->event(__FUNCTION__ . ' was called');

    $result = 'success';

    try {

        //classes initialization:-------------------------------------------------------------------------------------------
        $params = new \Acronis\Helpers\AcronisParams($vars, $config);
        $module_vars = new \Acronis\Helpers\ModuleVars($config->moduleName);
        $custom_fields = new \Acronis\WHMCS\CustomFields($config->moduleName);
        $manager = new \Acronis\ApiManagers\SystemManager($params->getServerUrl(), $config, $module_vars);


        //login to system:--------------------------------------------------------------------------------------------------
        $manager->accountsManager->login($vars["serverusername"], $vars["serverpassword"]);


        //getting group id from vars:---------------------------------------------------------------------------------------
        $user_group_id = $params->getCustomFieldFromVars('GroupID');


        //deleting group:---------------------------------------------------------------------------------------------------
        $manager->groupsManager->deleteGroup($user_group_id, null, $vars["serverusername"], $vars["serverpassword"]);

        //save group id to customfields:
        $custom_fields->set($vars['pid'], $vars['serviceid'], 'GroupID', '');
        $custom_fields->set($vars['pid'], $vars['serviceid'], 'AdminID', '');

    } catch (\Acronis\UserException $e) {
        $result = $config->lang->get('error') . ' ' . $e->getMessage();
    }
    AcronisBackupService_unloadErrorHandler($eh);
    AcronisBackupService_unloadAutoloader($al);

    //Outputting:
    return $result;

}


function AcronisBackupService_SuspendAccount($vars)
{

    $al = AcronisBackupService_loadAutoloader();
    $config = new \Acronis\Standard\Configs\ModuleConfig(true);
    $eh = AcronisBackupService_loadErrorHandler($config);

    Log::getInstance()->event(__FUNCTION__ . ' was called');

    $result = 'success';

    try {

        //classes initialization:-------------------------------------------------------------------------------------------
        $params = new \Acronis\Helpers\AcronisParams($vars, $config);
        $module_vars = new \Acronis\Helpers\ModuleVars($config->moduleName);
        $manager = new \Acronis\ApiManagers\SystemManager($params->getServerUrl(), $config, $module_vars);


        //login to system:--------------------------------------------------------------------------------------------------
        $manager->accountsManager->login($vars["serverusername"], $vars["serverpassword"]);


        //getting group id from vars:---------------------------------------------------------------------------------------
        $user_group_id = $params->getCustomFieldFromVars('GroupID');


        //deactivating group:-----------------------------------------------------------------------------------------------
        $manager->groupsManager->deactivateGroup($user_group_id, null, $vars["serverusername"], $vars["serverpassword"]);

    } catch (\Acronis\UserException $e) {
        $result = $config->lang->get('error') . ' ' . $e->getMessage();
    }
    AcronisBackupService_unloadErrorHandler($eh);
    AcronisBackupService_unloadAutoloader($al);

    //Outputting:
    return $result;

}


function AcronisBackupService_UnsuspendAccount($vars)
{

    $al = AcronisBackupService_loadAutoloader();
    $config = new \Acronis\Standard\Configs\ModuleConfig(true);
    $eh = AcronisBackupService_loadErrorHandler($config);

    Log::getInstance()->event(__FUNCTION__ . ' was called');

    $result = 'success';

    try {

        //classes initialization:-------------------------------------------------------------------------------------------
        $params = new \Acronis\Helpers\AcronisParams($vars, $config);
        $module_vars = new \Acronis\Helpers\ModuleVars($config->moduleName);
        $manager = new \Acronis\ApiManagers\SystemManager($params->getServerUrl(), $config, $module_vars);


        //login to system:--------------------------------------------------------------------------------------------------
        $manager->accountsManager->login($vars["serverusername"], $vars["serverpassword"]);


        //getting group id from vars:---------------------------------------------------------------------------------------
        $user_group_id = $params->getCustomFieldFromVars('GroupID');


        //deactivating group:-----------------------------------------------------------------------------------------------
        $manager->groupsManager->activateGroup($user_group_id, null, $vars["serverusername"], $vars["serverpassword"]);

    } catch (\Acronis\UserException $e) {
        $result = $config->lang->get('error') . ' ' . $e->getMessage();
    }
    AcronisBackupService_unloadErrorHandler($eh);
    AcronisBackupService_unloadAutoloader($al);

    //Outputting:
    return $result;

}


function AcronisBackupService_ChangePackage($vars)
{
    $al = AcronisBackupService_loadAutoloader();
    $config = new \Acronis\Standard\Configs\ModuleConfig(true);
    $eh = AcronisBackupService_loadErrorHandler($config);

    Log::getInstance()->event(__FUNCTION__ . ' was called');

    $result = 'success';

    try {

        //classes initialization:-------------------------------------------------------------------------------------------
        $params = new \Acronis\Helpers\AcronisParams($vars, $config);
        $module_vars = new \Acronis\Helpers\ModuleVars($config->moduleName);
        $manager = new \Acronis\ApiManagers\SystemManager($params->getServerUrl(), $config, $module_vars);


        //login to system:--------------------------------------------------------------------------------------------------
        $manager->accountsManager->login($vars["serverusername"], $vars["serverpassword"]);

        //getting group id from vars:---------------------------------------------------------------------------------------
        $user_group_id = $params->getCustomFieldFromVars('GroupID');

        $backupService = new \Acronis\Model\BackupService($module_vars, $config);

        $backupService->setBackupLocations($config->getBackupLocationsValueByOption($params->getConfigOption($config->getId('storageLocation'), false, "unlimited", false)));
        //load quota values
        $backupService->setServers($params->getConfigOption($config->getId('maxServers'), true, 'unlimited', true));
        $backupService->setStorageSize($params->getConfigOption($config->getId('maxStorageGB'), true, 'unlimited', true));
        $backupService->setVms($params->getConfigOption($config->getId('maxVMs'), true, 'unlimited', true));
        $backupService->setWorkstations($params->getConfigOption($config->getId('maxWorkstations'), true, 'unlimited', true));
        $backupService->setMobileDevices($params->getConfigOption($config->getId('maxMobileDevices'), true, 'unlimited', false));
        $backupService->setO365Mailboxes($params->getConfigOption($config->getId('maxO365Mailboxes'), true, 'unlimited', false));
        //modify quota from configurable options of product
        $backupService->loadConfigurableOptions($vars['configoptions']);

        $update_data = array
        (
            "privileges" => array
            (
                "local_backup" => array("permitted" => (int)$backupService->getBackupLocations()),
                "server_count" => $backupService->getServers(),
                "quota_servers_hard" => $backupService->getServers(),
                "storage_size" => $backupService->getStorageSize(),
                "quota_storage_hard" => $backupService->getStorageSize(),
                "vm_count" => $backupService->getVms(),
                "quota_vms_hard" => $backupService->getVms(),
                "workstation_count" => $backupService->getWorkstations(),
                "quota_workstations_hard" => $backupService->getWorkstations(),
                "mailbox_count" => (int)$backupService->getO365Mailboxes(),
                "quota_mailboxes_hard" => (int)$backupService->getO365Mailboxes(),
                "mobile_count" => (int)$backupService->getMobileDevices(),
                "quota_mobiles_hard" => (int)$backupService->getMobileDevices(),
            ),
        );

        //change pricing mode
        $productData = \Acronis\WHMCS\Products::fetchProductDataById($vars['pid']);
        if ($productData) {
            list($groupCurPricingMode, $foundGroupPricingMode) = $manager->groupsManager->getGroupParam(
                'pricing.mode',
                $user_group_id
            );
            if ($foundGroupPricingMode) {
                //we shouldn't change pricing mode from production to trial
                $groupNewPricingMode = $backupService->detectPricingModeFromProductData($productData);
                if (
                    $groupCurPricingMode == \Acronis\Model\BackupService::PRICING_MODE_TRIAL
                    && $groupNewPricingMode == \Acronis\Model\BackupService::PRICING_MODE_PRODUCTION
                ) {
                    $update_data['pricing'] = array('mode' => $groupNewPricingMode);
                }
            } else {
                Log::getInstance()->debug("Unable to get param 'pricing.mode' of group with id: {$user_group_id}");
            }
        }

        Log::getInstance()->debug('Trying to update group with data:' . print_r($update_data, true));

        $manager->groupsManager->updateGroup($user_group_id, $update_data);

    } catch (\Acronis\UserException $e) {
        $result = $config->lang->get('error') . ' ' . $e->getMessage();
    }

    AcronisBackupService_unloadErrorHandler($eh);
    AcronisBackupService_unloadAutoloader($al);

    //Outputting:
    return $result;

}


function AcronisBackupService_ChangePassword($vars)
{

    $al = AcronisBackupService_loadAutoloader();
    $config = new \Acronis\Standard\Configs\ModuleConfig(true);
    $eh = AcronisBackupService_loadErrorHandler($config);

    Log::getInstance()->event(__FUNCTION__ . ' was called');

    $result = 'success';

    try {

        //classes initialization:-------------------------------------------------------------------------------------------
        $params = new \Acronis\Helpers\AcronisParams($vars, $config);
        $module_vars = new \Acronis\Helpers\ModuleVars($config->moduleName);
        $manager = new \Acronis\ApiManagers\SystemManager($params->getServerUrl(), $config, $module_vars);


        //login to system:--------------------------------------------------------------------------------------------------
        $manager->accountsManager->login($vars["serverusername"], $vars["serverpassword"]);


        //updating account:-------------------------------------------------------------------------------------------------
        $manager->accountsManager->updateAccount($vars['username'], $vars['password'], $vars['clientsdetails']['email'], false);

    } catch (\Acronis\UserException $e) {
        $result = $config->lang->get('error') . ' ' . $e->getMessage();
    }
    AcronisBackupService_unloadErrorHandler($eh);
    AcronisBackupService_unloadAutoloader($al);

    //Outputting:
    return $result;

}


function AcronisBackupService_ClientArea($vars)
{

    $al = AcronisBackupService_loadAutoloader();
    $config = new \Acronis\Standard\Configs\ModuleConfig(true);
    $views_config = new \Acronis\Standard\Configs\Views\ClientArea();
    $eh = AcronisBackupService_loadErrorHandler($config);

    Log::getInstance()->event(__FUNCTION__ . ' was called');

    $init = new \Acronis\System\Init($vars, $config, $views_config);
    $content = $init->run();

    $title = $config->moduleName;

    AcronisBackupService_unloadErrorHandler($eh);
    AcronisBackupService_unloadAutoloader($al);


    //Outputting:
    return array(
        'pagetitle' => $title,
        'templatefile' => 'base',
        'requirelogin' => true,
        'vars' => array(
            'content' => $content,
        ),
    );

}

function AcronisBackupService_ClientAreaCustomButtonArray()
{
    $al = AcronisBackupService_loadAutoloader();
    $config = new Acronis\Standard\Configs\ModuleConfig(true);
    $eh = AcronisBackupService_loadErrorHandler($config);

    Log::getInstance()->event(__FUNCTION__ . ' was called');

    $result = array($config->lang->get('email_accounts_management') => 'Management');

    AcronisBackupService_unloadErrorHandler($eh);
    AcronisBackupService_unloadAutoloader($al);
    return $result;
}


function AcronisBackupService_Management($vars)
{

    $al = AcronisBackupService_loadAutoloader();
    $config = new \Acronis\Standard\Configs\ModuleConfig(true);
    $views_config = new \Acronis\Standard\Configs\Views\Management();
    $eh = AcronisBackupService_loadErrorHandler($config);

    Log::getInstance()->event(__FUNCTION__ . ' was called');

    $init = new \Acronis\System\Init($vars, $config, $views_config);
    $content = $init->run();

    $title = $config->moduleName;
    $management_button = $config->lang->get('email_accounts_management');

    AcronisBackupService_unloadErrorHandler($eh);
    AcronisBackupService_unloadAutoloader($al);

    //Outputting:
    return array(
        'pagetitle' => $title,
        'breadcrumb' => ' > <a href="#">' . $management_button . '</a>',
        'templatefile' => 'base',
        'requirelogin' => true,
        'vars' => array(
            'content' => $content,
        ),
    );

}
