<?php
use Acronis\Log;
use Illuminate\Database\Capsule\Manager as Capsule;

require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . 'Config.php';


//Adds customfields if needed:
if ('confproduct' == $_REQUEST['a']) {

    function AcronisBackupService_addCustomField($params)
    {

        $al = AcronisBackupService_loadAutoloader();
        $config = new \Acronis\Standard\Configs\ModuleConfig(true);
        $eh = AcronisBackupService_loadErrorHandler($config);

        $result = array();

        $pid = $_SESSION['cart']['cartsummarypid'];

        $db = Capsule::connection()->getPdo();

        $sth = $db->prepare('SELECT * FROM `tblproducts` WHERE `id`=? LIMIT 1');
        $sth->execute(array($pid));
        $row = $sth->fetch();

        if (!empty($row) && $row['servertype'] == "AcronisBackupService") {

            $return = array(
                array(
                    'id' => '',
                    'name' => $config->lang->get('order_input_credentials_string'),
                    'description' => '',
                    'type' => 'text',
                    'input' => '',
                    'value' => '',
                    'rawvalue' => '',
                    'required' => "*",
                    'adminonly' => '',
                ),

                array(
                    'id' => '',
                    'name' => '<div style="width:130px;">' . $config->lang->get('field.username') . '</div>',
                    'description' => '',
                    'type' => 'text',
                    'input' => '<input id="username" type="text" size="30" value="" name="username">',
                    'value' => '',
                    'rawvalue' => '',
                    'required' => "*",
                    'adminonly' => '',
                ),

                array(
                    'id' => '',
                    'name' => '<div style="width:130px;">' . $config->lang->get('field.password') . '</div>',
                    'description' => '',
                    'type' => 'password',
                    'input' => '<input id="password" type="password" size="30" value="" name="password">',
                    'value' => '',
                    'rawvalue' => '',
                    'required' => "*",
                    'adminonly' => '',
                ),
                array(
                    'id' => '',
                    'name' => '<div style="width:130px;">' . $config->lang->get('field.confirmpassword') . '</div>',
                    'description' => '',
                    'type' => 'password',
                    'input' => '<input id="confirm_password" type="password" size="30" value="" name="confirm_password">',
                    'value' => '',
                    'rawvalue' => '',
                    'required' => "*",
                    'adminonly' => '',
                )
            );

            $result = array('customfields' => array_merge($params['customfields'], $return));
        }

        AcronisBackupService_unloadErrorHandler($eh);
        AcronisBackupService_unloadAutoloader($al);

        //Outputting:
        return $result;

    }

    add_hook("ClientAreaPage", 1, "AcronisBackupService_addCustomField");

}

//Redirects to product configuration if needed:
if (strpos($_SERVER["REQUEST_URI"], "cart.php") && $_REQUEST['a'] == 'view') {

    $acronis_standard_al = AcronisBackupService_loadAutoloader();
    $acronis_standard_config = new \Acronis\Standard\Configs\ModuleConfig(true);
    $acronis_standard_eh = AcronisBackupService_loadErrorHandler($acronis_standard_config);
    $db = Capsule::connection()->getPdo();

    if (isset($_SESSION['cart']) && isset($_SESSION['cart']['products']))
        foreach ($_SESSION['cart']['products'] as $key => $value) {
            if (!isset($_SESSION['cart']['products'][$key]['password']) &&
                !isset($_SESSION['cart']['products'][$key]['username'])
            ) {
                $pid = $_SESSION['cart']['products'][$key]['pid'];

                $sth = $db->prepare('SELECT * FROM `tblproducts` WHERE `id`=? LIMIT 1');
                $sth->execute(array($pid));
                $row = $sth->fetch();

                if (!empty($row) && $row['servertype'] == "AcronisBackupService") {
                    ob_clean();
                    header('location: cart.php?a=confproduct&i=' . $key);
                    die();
                }
            }
        }

    AcronisBackupService_unloadErrorHandler($acronis_standard_eh);
    AcronisBackupService_unloadAutoloader($acronis_standard_al);
}

//Checks username and password in cart
function AcronisBackupService_validateCart($params)
{
    $al = AcronisBackupService_loadAutoloader();
    $config = new \Acronis\Standard\Configs\ModuleConfig(true);
    $eh = AcronisBackupService_loadErrorHandler($config);

    try {

        $item = $params['i'];

        $pid = $_SESSION['cart']['products'][$item]['pid'];

        $db = Capsule::connection()->getPdo();

        $sth = $db->prepare('SELECT * FROM `tblproducts` WHERE `id`=? LIMIT 1');
        $sth->execute(array($pid));
        $row = $sth->fetch();

        if (!empty($row) && $row['servertype'] == "AcronisBackupService") {
            $data = array
            (
                'login' => $params['username'],
                'password' => $params['password'],
                'confirm_password' => $params['confirm_password'],
            );
            $filter = new \Acronis\Standard\Helpers\AcronisForm($config, $config->lang, $data);

            $filter->validateLogin('username');
            $filter->validatePassword();

            $filtered_post = $filter->getFilteredPost();

            $username = $filtered_post['login'];
            $password = $filtered_post['password'];

            $sth = $db->prepare('SELECT * FROM `tblhosting` AS h
                                LEFT JOIN `tblproducts` AS p ON h.`packageid` = p.`id`
                                WHERE `username` = ?
                                AND `servertype` = "AcronisBackupService"');

            $sth->execute(array($username));
            $result = $sth->rowCount();

            if (!$result) {
                $_SESSION['cart']['products'][$item]['username'] = $username;
                $_SESSION['products'][$item]['username'] = $username;
            } else {
                throw new \Acronis\UserException($config->lang->get('errors.username_reserved'));
            }

            //fill data for using in CreateAccount function
            $_SESSION['cart']['products'][$item]['password'] = $password;
            $_SESSION['products'][$item]['password'] = $password;
            $serviceNameKey = \Acronis\WHMCS\Orders::SERVICE_NAME_KEY;
            $_SESSION['cart']['products'][$item][$serviceNameKey] = ACRONIS_SERVICE_NAME;
            $_SESSION['products'][$item][$serviceNameKey] = ACRONIS_SERVICE_NAME;
        }

    } catch (\Acronis\UserException $e) {
        echo $config->lang->get('error') . ' ' . $e->getMessage();
        die();
    }

    AcronisBackupService_unloadErrorHandler($eh);
    AcronisBackupService_unloadAutoloader($al);

}

add_hook("ShoppingCartValidateProductUpdate", 1, "AcronisBackupService_validateCart");


//Encrypts password after order
function AcronisBackupService_updateParams($vars)
{
    $al = AcronisBackupService_loadAutoloader();
    $config = new \Acronis\Standard\Configs\ModuleConfig(true);
    $eh = AcronisBackupService_loadErrorHandler($config);
    $db = Capsule::connection()->getPdo();

    if (isset($_SESSION['products'])) foreach ($_SESSION['products'] as $key => $value) {

        $password = $_SESSION['products'][$key]['password'];
        unset($_SESSION['products'][$key]['password']);

        $username = $_SESSION['products'][$key]['username'];
        unset($_SESSION['products'][$key]['username']);

        $order_id = $vars['OrderID'];
        $services_id = $_SESSION['orderdetails']['ServiceIDs'][$key];

        $command = "encryptpassword";
        $admin_user = \Acronis\WHMCS\AdminUser::getAdminUserName();
        $values["password2"] = $password;
        $results = localAPI($command, $values, $admin_user);
        $password = $results['password'];

        $sth = $db->prepare('UPDATE `tblhosting` SET `username` =?, `password` = ? WHERE `orderid` = ? AND `id` = ?');

        $sth->execute(array($username, $password, $order_id, $services_id));

    }

    AcronisBackupService_unloadErrorHandler($eh);
    AcronisBackupService_unloadAutoloader($al);
}

/**
 * Update information in ABC after edit client profile
 * @param array $vars for more information see WHMCS documentation (http://docs.whmcs.com/Hooks:ClientEdit)
 */
function AcronisBackupService_editClient($vars)
{
    $al = AcronisBackupService_loadAutoloader();
    $config = new \Acronis\Standard\Configs\ModuleConfig(true);
    $eh = AcronisBackupService_loadErrorHandler($config);

    $serverData = \Acronis\WHMCS\Servers::getServerData();
    $servers = $serverData[0];
    $module_vars = new \Acronis\Helpers\ModuleVars($config->moduleName);
    $manager = new \Acronis\ApiManagers\SystemManager($servers['hostname'], $config, $module_vars);
    $manager->accountsManager->login();

    $db = Capsule::connection()->getPdo();
    $sth = $db->prepare('SELECT * FROM `tblhosting` h JOIN `tblcustomfieldsvalues` cfv ON h.id=cfv.relid JOIN `tblcustomfields` cf ON cf.id=cfv.fieldid JOIN `tblclients` cl ON cl.id=h.userid WHERE h.userid =? AND cf.fieldname="GroupID" AND cfv.value NOT LIKE ""');
    $sth->execute(array($vars['userid']));
    $groups = $sth->fetchAll();

    $contactData = array(
        "contact" => array(
            "firstname" => $vars['firstname'],
            "lastname" => $vars['lastname']
        ),
        "name" => $vars['companyname']
    );

    foreach ($groups as $group) {
        $groupId = $group['value'];

        $manager->groupsManager->updateGroup($groupId, $contactData);
    }

    AcronisBackupService_unloadErrorHandler($eh);
    AcronisBackupService_unloadAutoloader($al);
}

add_hook('ClientEdit', 1, 'AcronisBackupService_editClient');

function AcronisBackupService_OrderProductUpgradeOverride($vars)
{
    $al = AcronisBackupService_loadAutoloader();
    $config = new \Acronis\Standard\Configs\ModuleConfig(true);
    $eh = AcronisBackupService_loadErrorHandler($config);
    $db = Capsule::connection()->getPdo();

    Log::getInstance()->event(__FUNCTION__ . ' was called');

    $oldProductId = $vars['oldproductid'];
    $newProductId = $vars['newproductid'];

    $sthOld = $db->prepare('SELECT * FROM `tblcustomfields` WHERE `relid`=? AND `fieldname` IN ("GroupID", "AdminID")');
    $sthNew = $db->prepare('SELECT * FROM `tblcustomfields` WHERE `relid`=? AND `fieldname`=?');
    $sthClone = $db->prepare(
        'INSERT INTO `tblcustomfields`' .
        ' (`type`, `relid`, `fieldname`, `fieldtype`, `description`, `fieldoptions`, `regexpr`, `adminonly`, `required`, `showorder`, `showinvoice`, `sortorder`, `created_at`, `updated_at`) ' .
        'SELECT `type`, ?, `fieldname`, `fieldtype`, `description`, `fieldoptions`, `regexpr`, `adminonly`, `required`, `showorder`, `showinvoice`, `sortorder`, `created_at`, `updated_at` ' .
        'FROM `tblcustomfields` WHERE `relid`=? AND `fieldname`=?'
    );

    $sthOld->execute(array($oldProductId));

    while ($field = $sthOld->fetch()) {
        $sthNew->execute(array($newProductId, $field['fieldname']));
        $count = $sthNew->rowCount();
        $sthNew->closeCursor();

        if ($count) {
            continue;
        }

        $sthClone->execute(array($newProductId, $oldProductId, $field['fieldname']));
    }

    AcronisBackupService_unloadErrorHandler($eh);
    AcronisBackupService_unloadAutoloader($al);
}

add_hook('OrderProductUpgradeOverride', 1, 'AcronisBackupService_OrderProductUpgradeOverride');

/**
 * cleans unused custom fields values from database
 *
 * @param array $vars array with order id
 */
function AcronisBackupService_deleteOrder($vars)
{
    $orderId = $vars['orderid'];

    $al = AcronisBackupService_loadAutoloader();
    $config = new \Acronis\Standard\Configs\ModuleConfig(true);
    $eh = AcronisBackupService_loadErrorHandler($config);

    \Acronis\WHMCS\Orders::cleanDataForOrder($orderId);

    AcronisBackupService_unloadErrorHandler($eh);
    AcronisBackupService_unloadAutoloader($al);
}

add_hook('DeleteOrder', 1, 'AcronisBackupService_deleteOrder');
add_hook('ShoppingCartCheckoutCompletePage', 1, 'AcronisBackupService_updateParams');
add_hook('AfterShoppingCartCheckout', 1, 'AcronisBackupService_updateParams');
