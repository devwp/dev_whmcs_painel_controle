<?php

$LANG['logintoiem'] = 'Log In To Interspire Email Marketer';
$LANG['userstats'] = 'User Statistics';
$LANG['newsletters_sent'] = 'Newsletters Sent';
$LANG['total_emails_sent'] = 'Total Emails Sent';
$LANG['unique_opens'] = 'Unique Opens';
$LANG['total_opens'] = 'Total Opens';
$LANG['total_bounces'] = 'Total Bounces';
$LANG['autoresponders'] = 'Autoresponders';
$LANG['mailinglists'] = 'Mailling Lists';
$LANG['lastnewsletter'] = 'Last Newsletter Sent';