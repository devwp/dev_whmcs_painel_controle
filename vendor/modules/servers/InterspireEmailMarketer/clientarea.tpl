{**********************************************************************
 *  Interspire Email Marketer for WHMCS (31.07.2014)
 * *
 *
 *  CREATED BY MODULESGARDEN       ->        http://modulesgarden.com
 *  CONTACT                        ->       contact@modulesgarden.com
 *
 *
 * This software is furnished under a license and may be used and copied
 * only  in  accordance  with  the  terms  of such  license and with the
 * inclusion of the above copyright notice.  This software  or any other
 * copies thereof may not be provided or otherwise made available to any
 * other person.  No title to and  ownership of the  software is  hereby
 * transferred.
 *
 * @version 1.0.0 (31.07.2014)
 * @author Pawel <pawel@modulesgarden.com>
 *
 **********************************************************************}
{literal}
<script src="includes/jscript/base64.js"></script>
{/literal}
{if $error}
    <div class="alert alert-block alert-error"><strong>{$error}</strong></div>
{else}
    {if !empty($stats)}
    <h2 style="margin: 0px 0px 20px 10px" class="pull-left">{$IEMLANG.userstats}</h2>
    {/if}
    <table class="table table-hover">
        {if $config.newsletters_sent}
        <tr>
            <td>{$IEMLANG.newsletters_sent}</td>
            <td>{$stats.newsletter.newsletters_sent}</td>
        </tr>
        {/if}
        {if $config.total_emails_sent}
        <tr>
            <td>{$IEMLANG.total_emails_sent}</td>
            <td>{$stats.newsletter.total_emails_sent}</td>
        </tr>
        {/if}
        {if $config.unique_opens}
        <tr>
            <td>{$IEMLANG.unique_opens}</td>
            <td>{$stats.newsletter.unique_opens}</td>
        </tr>
        {/if}
        {if $config.total_opens}
        <tr>
            <td>{$IEMLANG.total_opens}</td>
            <td>{$stats.newsletter.total_opens}</td>
        </tr>
        {/if}
        {if $config.total_bounces}
        <tr>
            <td>{$IEMLANG.total_bounces}</td>
            <td>{$stats.newsletter.total_bounces}</td>
        </tr>
        {/if}
        {if $config.autoresponders}
        <tr>
            <td>{$IEMLANG.autoresponders}</td>
            <td>{$stats.autoresponders}</td>
        </tr>
        {/if}
        {if $config.mailinglists}
        <tr>
            <td>{$IEMLANG.mailinglists}</td>
            <td>{$stats.mailinglists}</td>
        </tr>
        {/if}
        {if $config.lastnewsletter}
        <tr>
            <td>{$IEMLANG.lastnewsletter}</td>
            <td>{$stats.lastnewsletter}</td>
        </tr>
        {/if}
        {if $config.loginbutton}
        <tr>
            <th colspan="2">
                <button style="margin: 15px 0px 0px 0px" class="btn btn-primary pull-left" onclick="{$loginscript}">{$IEMLANG.logintoiem}</button>
            </th>
        </tr>
        {/if}
    </table>
{/if}