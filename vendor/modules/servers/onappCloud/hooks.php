<?php


/**
 * @author Maciej Husak <maciej@modulesgarden.com>
 */
if (!defined('DS'))
	define('DS', DIRECTORY_SEPARATOR);

// save product configuration
if (isset($_SESSION['adminid']) && isset($_REQUEST['action'])){
    include_once ROOTDIR . DS . 'includes' . DS . 'onappWrapper' . DS . 'utility.php';
    onapp_loadCLass();
    include_once ROOTDIR . DS . 'modules' . DS . 'servers' . DS . 'onappCloud' . DS . 'class' . DS . 'class.Product.php';
    
    // save config options
    if(isset($_POST['servertype']) && isset($_REQUEST['action']) && $_POST['servertype'] == 'onappCloud' && isset($_POST['customconfigoption'])){
        $product =  new onappCloud_Product($_GET['id']);
	$product -> clearConfig();
	foreach ($_POST['customconfigoption'] as $k => $v){
		$product->saveConfig($k, $v);
	}
    }
    
    
    if(isset($_POST['onAppCloud_action']) && $_POST['onAppCloud_action']==1){
        $product     = new onappCloud_Product($_POST['productid']);
        $params      = $product->getParams();
        switch($_POST['action']){
            case 'getTemplates':
                if(isset($_POST['group']) && count($_POST['group'])>0){
                    $template    = new NewOnApp_Template(null);
                    $template    ->setconnection($params);
                    $list        = array();
                    foreach($_POST['group'] as $val)
                    {
                        if($val == 'all')
                        {
                            $templates = $template->getSystemTemplates();
                            foreach ($templates as $temp){
                               $list[$temp['image_template']['id']] = $temp['image_template']['label'];
                            }
                        } else {
                            $templates   = $template->getTemplatesFromStore($val);
                            foreach ($templates as $temp){
                               $list[$temp['relation_group_template']['image_template']['id']] = $temp['relation_group_template']['image_template']['label'];
                            }
                        }
                    }
                    
                    asort($list);      
                    $selected = $product->getConfig("template_ids");
                    foreach($list as $key => $val)
                    {
                        echo  '<option value="'.$key.'" '.(in_array($key,$selected) ? 'selected' : '').'>'.$val.'</option>';         
                    }
                    
                }
                die();
                break;
            case 'onappCloud_setup_configurable_options':
                if ($product->hasConfigurableOptions()){
                       die(json_encode(array("success" => 0, "result" => 'Product has already configurable options assigned.')));
                }

                if ($product->setupDefaultConfigurableOptions()){
                       die(json_encode(array("success" => 1, "result" =>  'Default Configurable options have been created.')));
                }
                die();
                break;  
            case 'onappCloud_setup_custom_fields':
                if(onapp_customFieldExists($_POST['productid'],'userid')){
                       die(json_encode(array("success" => 0, "result" => 'Product has already custom fields assigned.')));
                }
                if ($product->setupDefaultCustomFields()){
                       die(json_encode(array("success" => 1, "result" => 'Default custom fields have been created.')));
                }
            break;     
           case 'gethypervisors':
                if(isset($_POST['zone']) && $_POST['zone'] > 0)
                {
                    $hypervisor  = new NewOnApp_HypervisorZone($_POST['zone']);
                    $hypervisor -> setconnection($params);
                    $hypervisors = $hypervisor->lisHPV();
                    echo "<option value='0'>Auto</option>";
                    if($hypervisor->isSuccess())
                    {
                        foreach($hypervisors as $key => $val)
                        {
                            if($val['hypervisor']['online'] == 1)
                                echo "<option value='".$val['hypervisor']['id']."'>".$val['hypervisor']['label']."</option>";
                        }
                    }
                }
                die();
            break;
        }
    }    
    
    
}
