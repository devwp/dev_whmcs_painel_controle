<?php


/**
 * @author Pawel Kopec <pawelk@modulesgarden.com>
 */

class NewOnAppCloud_User{
      
      public $id;
      public $user_id;
      public $email;
      public $username;
      public $key;
      
      public function __construct($userID, $data = array()) {
            self::install();
            $this->user_id = (int) $userID;
            if($userID && empty($data)){
                  $data = mysql_get_row('SELECT * FROM  `onappCloud_auth` WHERE `user_id`=? ', array($userID));
                  if($data['key'])
                      $data['key'] = decrypt($data['key']);  
            }
            
            
            foreach($data as $key  => $value){
                  $this->$key  = $value;
            }
      }
      
      public function save(){
            $data = array(
                           $this->user_id,
                           $this->email,
                           $this->username,
                           encrypt($this->key)
            );
            if(!$this->id){
                  $result = mysql_safequery('INSERT INTO `onappCloud_auth` ( `user_id`, `email`, `username`, `key` ) VALUES(?,?,?,?)',  $data);
                  $this->id = mysql_insert_id();
            }else{
                  $data [] = $this->id;
                  $result = mysql_safequery('UPDATE `onappCloud_auth` SET  `user_id`=?, `email`=?, `username`=?, `key`=?  WHERE `id`=?  ',  $data);
            }
            if(!$result)
                  throw new Exception(mysql_error (), mysql_errno ());
            
      }
      public function delete(){
            if($this->id){
                  $result = mysql_safequery('DELETE FROM `onappCloud_auth` WHERE  id =?', array($this->id));
                  if(!$result)
                        throw new Exception(mysql_error (), mysql_errno ());
            }
      }
      public static function install(){
                  mysql_safequery('CREATE TABLE IF NOT EXISTS `onappCloud_auth` (
                                      `id`  int(12)    NOT NULL AUTO_INCREMENT,
                                      `user_id` int(12) unsigned NOT NULL,
                                      `email` varchar(150) NOT NULL,
                                      `username` varchar(150) NOT NULL,
                                      `key` varchar(250) NOT NULL,
                                      PRIMARY KEY (`id`),
                                      UNIQUE KEY `user_id` (`user_id`)
                                    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;'
                          );
      }
}