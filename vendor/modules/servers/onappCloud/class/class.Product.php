<?php


/**
 * @author Grzegorz Draganik <grzegorz@modulesgarden.com>
 * @author Maciej Husak <maciej@modulesgarden.com>
 */

class onappCloud_Product extends MG_WHMCS_Onapp_Product {
    
	public $id;
        
	protected $_tableName = 'onappCloud_prodConfig';
	
        public function __construct($id, $serviceid = null) {
		if ($serviceid){
			$q = mysql_query('SELECT packageid FROM tblhosting WHERE id = ' . (int)$serviceid);
			$row = mysql_fetch_assoc($q);
			$this->id = $row['packageid'];
		} else {
			$this->id = $id;
		}
	}
        
	public $defaultConfig = array( 
               /*'gr0' => 'Pricing',
                'display_pricing'=> array(
                        'title'                 => 'Don\'t display pricing when creating new VM',
                        'type'                  => 'select',
                        'options'		=> array(0=>'No',1=>'Yes'),
                        'useOptionsKeys'        => true
                ),*/
		'gr1' => 'Resources',
                'vm_limit'=> array(
			'title'			=> 'VM Limit <span style="color:red;">*</span>',
			'type'			=> 'text',
			'default'		=> '500',
			'description'           => '',
		),
		'memory' => array(
			'title'			=> 'Default Memory Limit <span style="color:red;">*</span>',
			'type'			=> 'text',
			'default'		=> '512', 
			'description'           => '<select name="customconfigoption[memory_unit]"><option value="MB">MB</option><option value="GB">GB</option></select>',
		),
		'cpus' => array(
			'title'			=> 'Default CPU Limit <span style="color:red;">*</span>',
			'type'			=> 'text',
			'default'		=> '1',
		),
                'cpu_shares' => array(
			'title'			=> 'Default CPU Shares [%] <span style="color:red;">*</span>',
			'type'			=> 'text',
			'default'		=> '20',
			'description'           => '<img src="../modules/servers/onappCloud/img/info.png"  title="For KVM hypervisor the CPU priority value is always 100. For XEN, set a custom value. The default value for XEN is 1" />',
		),
                'primary_disk_size'             => array(
                        'title'                 => 'Default Disk Size <span style="color:red;">*</span>',
                        'type'                  => 'text',
                        'default'		=> '20',
                        'description'           => '<select name="customconfigoption[primary_unit]"><option value="GB">GB</option><option value="MB">MB</option></select>',
                 ),
                'swap_disk_size' => array(
			'title'			=> 'SWAP Space <span style="color:red;">*</span>',
			'type'			=> 'text',
			'default'		=> '10',
			'description'           => '<select name="customconfigoption[swap_unit]"><option value="GB">GB</option><option value="MB">MB</option></select> <img src="../modules/servers/onappCloud/img/info.png" style="vertical-align:middle;"  title="There is no swap disk for Windows-based VMs" />',
		),
                
                'gr2' => 'OS Template',
                'template_group'                =>  array(
			'title'			=> 'Template Store',
			'type'			=> 'multiselect',
			'options'		=> array('all'=>'All Templates'), // loaded later
			'description'           => '<img src="../modules/servers/onappCloud/img/info.png"  title="If no hypervisor is specified, the VM will be built on the hypervisor with the least available RAM (but sufficient RAM for the VM)" />',
			'useOptionsKeys'        => true
		),
                'template_ids' => array(
			'title'			=> 'Available OS Templates <span style="color:red;">*</span>',
			'type'			=> 'multiselect',
			'options'		=> array('-- not specified --'=>'-- not specified --'),
			'description'           => '<img src="../modules/servers/onappCloud/img/info.png"  title="Select available os templates" />',
			'useOptionsKeys'        => true
		),
                'gr3' => 'Storage/Backups',

                'data_store_group_primary_id'   => array(
                        'title'			=> 'Data Store Zone <span style="color:red;">*</span>',
			'type'			=> 'select',
			'options'		=> array('-- not specified --'=>'-- not specified --'),
			'description'           => '<img src="../modules/servers/onappCloud/img/info.png"  title="Select label of the data store zone to which this primary disk is allocated" />',
			'useOptionsKeys'        => true
                ),
                'data_store_group_swap_id'      => array(
                        'title'			=> 'SWAP: Data Store Zone <span style="color:red;">*</span>',
			'type'			=> 'select',
			'options'		=> array('-- not specified --'=>'-- not specified --'),
			'description'           => '<img src="../modules/servers/onappCloud/img/info.png"  title="Select label of the data store zone to which this swap disk is allocated" />',
			'useOptionsKeys'        => true
                ),
                'primary_disk_min_iops'         => array(
			'title'			=> 'Primary Disk Min Iops',
			'type'			=> 'text',
			'default'		=> '',
			'description'           => '<img src="../modules/servers/onappCloud/img/info.png"  title="Minimum number of  IO operations per second for primary disk" />',
		),
                'swap_disk_min_iops'            => array(
			'title'			=> 'Swap Disk Min Iops',
			'type'			=> 'text',
			'default'		=> '',
			'description'           => '<img src="../modules/servers/onappCloud/img/info.png"  title="Minimum number of  IO operations per second for swap disk" />',
		),
                'backup_limits'                => array(
			'title'			=> 'Backups Limit',
			'type'			=> 'text',
			'default'		=> '10',
			'description'           => '<img src="../modules/servers/onappCloud/img/info.png"  title="Minimum number of  backups" />',
		),
                'display_zones'                 => array(
                        'title'                 => 'Display All Zones in ClientArea',
                        'type'                  => 'select',
                        'options'		=> array('No','Yes'),
                        'useOptionsKeys'        => true
                ),
		
		'gr4' => 'Networks',
                'ip_addresses' => array(
			'title'			=> 'IP Addresses count',
			'type'			=> 'text',
			'default'		=> '1',
		),
		
		'rate_limit' => array(
			'title'			=> 'Max Port Speed <span style="color:red;">*</span>',
			'type'			=> 'text',
			'description'           => '<img src="../modules/servers/onappCloud/img/info.png"  title="[Rate] If none set, the system sets port speed to unlimited" />',
                        'default'               => 100
		),
                'rate_limit_vm' => array(
			'title'			=> 'Max Port Speed Per VM',
			'type'			=> 'text',
			'description'           => '<img src="../modules/servers/onappCloud/img/info.png"  title="If blank, module will impose a limit on whole account based on Max Port Speed" />',
                        'default'               => ''
		),
		'primary_network_id' => array(
			'title'			=> 'Network Zone <span style="color:red;">*</span>',
			'type'			=> 'select',
			'options'		=> array('-- not specified --'=>'-- not specified --'),
			'useOptionsKeys'        => true
		),
                'hypervisor_zone' => array(
			'title'			=> 'Hypervisor Zone <span style="color:red;">*</span>',
			'type'			=> 'select',
			'options'		=> array('-- not specified --'=>'-- not specified --'),
			'description'           => '<img src="../modules/servers/onappCloud/img/info.png"  title="If no hypervisor is specified, the VM will be built on the hypervisor with the least available RAM (but sufficient RAM for the VM)" />',
			'useOptionsKeys'        => true,
		),
                'hypervisor_id' => array(
			'title'			=> 'Hypervisor ',
			'type'			=> 'select',
			'options'		=> array('0'=>'Auto'),
			'description'           => '<img src="../modules/servers/onappCloud/img/info.png"  title="If no hypervisor is specified, the VM will be built on the hypervisor with the least available RAM (but sufficient RAM for the VM)" />',
			'useOptionsKeys'        => true
		),
                'gr5'   =>'Additionals',
		'label' => array(
			'title'			=> 'User-friendly VM description',
			'type'			=> 'text',
			'default'		=> 'Virtual Machine created with onappCloud for WHMCS',
		),
                'type_of_format'                => array(
			'title'			=> 'Type Of Filesystem',
			'type'			=> 'text',
			'default'		=> 'ext3',
		),
                'licensing_server_id'           => array(
                        'title'                 => 'Licensing Server Id',
			'description'		=> '<img src="../modules/servers/onappCloud/img/info.png"  title="The ID of a licensing server/template group – this parameter is for Windows XP/7 virtual machines only" />',
			'type'			=> 'text',
		),
                'licensing_type'                => array(
                        'title'                 => 'Licensing Type',
			'description'   	=> '<img src="../modules/servers/onappCloud/img/info.png"  title="The type of a license: mak, kms or user own license. This parameter is for Windows XP/7 virtual machines only" />',
			'type'			=> 'text',
		),
                'licensing_key'                 => array(
                        'title'                 => 'Licensing Key',
			'description'           => '<img src="../modules/servers/onappCloud/img/info.png"  title="The key of a license. This parameter is for Windows XP/7 virtual machines only" />',
			'type'			=> 'text',
		),
                'required_automatic_backup'     => array(
                        'title'                 => 'Automatic Backup',
                        'description'           => '<img src="../modules/servers/onappCloud/img/info.png"  title="Select \'Yes\' if you need automatic backups" />',
                        'type'                  => 'select',
                        'options'		=> array(0=>'No',1=>'Yes'),
                        'useOptionsKeys'        => true
               ),
                'required_virtual_machine_build'=> array(
                        'title'                 => 'Virtual Machine Build',
                        'description'           => '<img src="../modules/servers/onappCloud/img/info.png"  title="Select \'Yes\' if you want build VM automatically" />',
                        'type'                  => 'select',
                        'options'		=> array('Yes','No'),
                        'useOptionsKeys'        => true
               ),
                'console'                       => array(
                        'title'                 => 'Use HTML5 Console',
                        'description'           => '<img src="../modules/servers/onappCloud/img/info.png"  title="Select \'Yes\' if you want use HTML5" />',
                        'type'                  => 'select',
                        'options'		=> array('No','Yes'),
                        'useOptionsKeys'        => true
               ),
                'gr6' => 'User Configuration',
                'user_role'                     => array(
			'title'			=> 'User Role <span style="color:red;">*</span>',
                        'description'           => '<img src="../modules/servers/onappCloud/img/info.png"  title="Assigns a role to a user" />',
			'type'			=> 'select',
			'options'		=> array('-- not specified --'=>'-- not specified --'),
			'useOptionsKeys'        => true
		),
                'user_billing_plan'             => array(
			'title'			=> 'User Billing Plan',
                        'description'           => '<img src="../modules/servers/onappCloud/img/info.png"  title="Set by default, if not selected" />',
			'type'			=> 'select',
			'options'		=> array('-- not specified --'=>'-- not specified --'), // loaded later
			'useOptionsKeys'        => true
		),
                'user_group'                    => array(
			'title'			=> 'User Group',
                        'description'           => '<img src="../modules/servers/onappCloud/img/info.png"  title="ID of the group, to which the user is attached" />',
			'type'			=> 'select',
			'options'		=> array('-- not specified --'=>'-- not specified --'),
			'useOptionsKeys'        => true
		),
                'user_prefix'                   => array(
                        'title'                 => 'Username Prefix',
                        'type'                  => 'text',
                        'default'		=> 'whmcs_',
                ),
                'gr7' => 'UP  AutoScalling',
                'subgroup1' => 'RAM',
                'up_memory_for_minutes'                => array(
			'title'			=> 'Time',
                        'description'           => '<img src="../modules/servers/onappCloud/img/info.png"  title="The time threshold before scaling will be triggered" />',
			'type'			=> 'select',
			'options'		=> array(5 => '5 minutes',10 => '10 minutes', 15 => '15 minutes', 20 => '20 minutes', 25 => '25 minutes', 30 => '30 minutes',),
                        'useOptionsKeys'        => true
		),
                'up_memory_limit_trigger'          => array(
                        'title'                 => 'If usage is above',
                        'description'           => '[%] <img src="../modules/servers/onappCloud/img/info.png"  title="The amount of the resource usage (%). If this value is reached by the VM for the period specified by the for_minutes parameter, the system will add the amount of units set by the add_units parameters" />',
                        'type'                  => 'text',
                ),
                'up_memory_adjust_units'                    => array(
                        'title'                 => 'Add',
                        'description'           => '[MB] <img src="../modules/servers/onappCloud/img/info.png" title="The amount of resource units which the system should add if the rule is met" />',
                        'type'                  => 'text',
                ),
                'up_memory_up_to'                  => array(
                        'title'                 => '24hr limit',
                        'description'           => '[MB] <img src="../modules/servers/onappCloud/img/info.png" title="The amount of resource which cannot be exceeded within 24 hours period" />',
                        'type'                  => 'text',
                ),
                'subgroup2' => 'CPU Usage',
                'up_cpu_for_minutes'                   => array(
			'title'			=> 'Time',
                        'description'           => '<img src="../modules/servers/onappCloud/img/info.png"  title="The time threshold before scaling will be triggered" />',
			'type'			=> 'select',
			'options'		=> array(5 => '5 minutes',10 => '10 minutes', 15 => '15 minutes', 20 => '20 minutes', 25 => '25 minutes', 30 => '30 minutes',),
                        'useOptionsKeys'        => true
		),
                'up_cpu_limit_trigger'                  => array(
                        'title'                 => 'If usage is above',
                        'description'           => '[%] <img src="../modules/servers/onappCloud/img/info.png"  title="The amount of the resource usage (%). If this value is reached by the VM for the period specified by the for_minutes parameter, the system will add the amount of units set by the add_units parameters" />',
                        'type'                  => 'text',
                ),
                'up_cpu_adjust_units'                    => array(
                        'title'                 => 'Add',
                        'description'           => '[%] <img src="../modules/servers/onappCloud/img/info.png" title="The amount of resource units which the system should add if the rule is met" />',
                        'type'                  => 'text',
                ),
                'up_cpu_up_to'                  => array(
                        'title'                 => '24hr limit',
                        'description'           => '[%] <img src="../modules/servers/onappCloud/img/info.png" title="The amount of resource which cannot be exceeded within 24 hours period" />',
                        'type'                  => 'text',
                ),
                'subgroup3' => 'Disk Usage',
                'up_disk_for_minutes'            => array(
			'title'			=> 'Time',
                        'description'           => '<img src="../modules/servers/onappCloud/img/info.png"  title="The time threshold before scaling will be triggered" />',
			'type'			=> 'select',
			'options'		=> array(5 => '5 minutes',10 => '10 minutes', 15 => '15 minutes', 20 => '20 minutes', 25 => '25 minutes', 30 => '30 minutes',),
                        'useOptionsKeys'        => true
		),
                'up_disk_limit_trigger'                 => array(
                        'title'                 => 'If usage is above',
                        'description'           => '[%] <img src="../modules/servers/onappCloud/img/info.png"  title="The amount of the resource usage (%). If this value is reached by the VM for the period specified by the for_minutes parameter, the system will add the amount of units set by the add_units parameters" />',
                        'type'                  => 'text',
                ),
                'up_disk_adjust_units'                   => array(
                        'title'                 => 'Add',
                        'description'           => '[GB] <img src="../modules/servers/onappCloud/img/info.png" title="The amount of resource units which the system should add if the rule is met" />',
                        'type'                  => 'text',
                ),
                'up_disk_up_to'                 => array(
                        'title'                 => '24hr limit',
                        'description'           => '[GB] <img src="../modules/servers/onappCloud/img/info.png" title="The amount of resource which cannot be exceeded within 24 hours period" />',
                        'type'                  => 'text',
                ),
            
                'gr8' => 'Down  AutoScalling',
                'subgroup4' => 'RAM',
                'down_memory_for_minutes'                 => array(
			'title'			=> 'Time',
                        'description'           => '<img src="../modules/servers/onappCloud/img/info.png"  title="The time threshold before scaling will be triggered" />',
			'type'			=> 'select',
			'options'		=> array(5 => '5 minutes',10 => '10 minutes', 15 => '15 minutes', 20 => '20 minutes', 25 => '25 minutes', 30 => '30 minutes',),
                        'useOptionsKeys'        => true
		),
                'down_memory_limit_trigger'                => array(
                        'title'                 => 'If usage is below',
                        'description'           => '[%] <img src="../modules/servers/onappCloud/img/info.png"  title="The amount of the resource usage (%). If this value is reached by the VM for the period specified by the for_minutes parameter, the system will add the amount of units set by the add_units parameters" />',
                        'type'                  => 'text',
                ),
                'down_memory_adjust_units'                  => array(
                        'title'                 => 'Remove',
                        'description'           => '[MB] <img src="../modules/servers/onappCloud/img/info.png" title="The amount of resource units which the system should add if the rule is met" />',
                        'type'                  => 'text',
                ),
                /*'down_memory_up_to'                => array(
                        'title'                 => '24hr limit',
                        'description'           => '[MB] <img src="../modules/servers/onappCloud/img/info.png" title="The amount of resource which cannot be exceeded within 24 hours period" />',
                        'type'                  => 'text',
                ),*/
            
                'subgroup5' => 'CPU Usage',
                'down_cpu_for_minutes'                 => array(
			'title'			=> 'Time',
                        'description'           => '<img src="../modules/servers/onappCloud/img/info.png"  title="The time threshold before scaling will be triggered" />',
			'type'			=> 'select',
			'options'		=> array(5 => '5 minutes',10 => '10 minutes', 15 => '15 minutes', 20 => '20 minutes', 25 => '25 minutes', 30 => '30 minutes',),
                        'useOptionsKeys'        => true
		),
                'down_cpu_limit_trigger'                => array(
                        'title'                 => 'If usage is below',
                        'description'           => '[%] <img src="../modules/servers/onappCloud/img/info.png"  title="The amount of the resource usage (%). If this value is reached by the VM for the period specified by the for_minutes parameter, the system will add the amount of units set by the add_units parameters" />',
                        'type'                  => 'text',
                ),
                'down_cpu_adjust_units'                  => array(
                        'title'                 => 'Remove',
                        'description'           => '[%] <img src="../modules/servers/onappCloud/img/info.png" title="The amount of resource units which the system should add if the rule is met" />',
                        'type'                  => 'text',
                ),
              /*  'down_cpu_up_to'                => array(
                        'title'                 => '24hr limit',
                        'description'           => '[%] <img src="../modules/servers/onappCloud/img/info.png" title="The amount of resource which cannot be exceeded within 24 hours period" />',
                        'type'                  => 'text',
                ),*/
            
                'subgroup6' => 'Disk Usage',
                'down_disk_for_minutes'                => array(
			'title'			=> 'Time',
                        'description'           => '<img src="../modules/servers/onappCloud/img/info.png"  title="The time threshold before scaling will be triggered" />',
			'type'			=> 'select',
			'options'		=> array(5 => '5 minutes',10 => '10 minutes', 15 => '15 minutes', 20 => '20 minutes', 25 => '25 minutes', 30 => '30 minutes',),
                        'useOptionsKeys'        => true
		),
                'down_disk_limit_trigger'               => array(
                        'title'                 => 'If usage is below',
                        'description'           => '[%] <img src="../modules/servers/onappCloud/img/info.png"  title="The amount of the resource usage (%). If this value is reached by the VM for the period specified by the for_minutes parameter, the system will add the amount of units set by the add_units parameters" />',
                        'type'                  => 'text',
                ),
                'down_disk_adjust_units'                   => array(
                        'title'                 => 'Remove',
                        'description'           => '[GB] <img src="../modules/servers/onappCloud/img/info.png" title="The amount of resource units which the system should add if the rule is met" />',
                        'type'                  => 'text',
                ),
                /*'down_disk_up_to'                 => array(
                        'title'                 => '24hr limit',
                        'description'           => '[MB] <img src="../modules/servers/onappCloud/img/info.png" title="The amount of resource which cannot be exceeded within 24 hours period" />',
                        'type'                  => 'text',
                ),*/
                'gr9' => 'Disallow certain actions in clientarea',
                'manage_firewall'                => array(
                        'title'                 => 'Firewall Management',
                        'description'           => '',
                        'type'                  => 'checkbox',
                ),
                'manage_ip'                     => array(
                        'title'                 => 'IP Management',
                        'description'           => '',
                        'type'                  => 'checkbox',
                ),
                'manage_network'                => array(
                        'title'                 => 'Network Management',
                        'description'           => '',
                        'type'                  => 'checkbox',
                ),
                'manage_stats'                  => array(
                        'title'                 => 'CPU Usage Graphs',
                        'description'           => '',
                        'type'                  => 'checkbox',
                ),
                'manage_disk'                   => array(
                        'title'                 => 'Disk Management',
                        'description'           => '',
                        'type'                  => 'checkbox',
                ),
                'manage_backups'                => array(
                        'title'                 => 'Backups',
                        'description'           => '',
                        'type'                  => 'checkbox',
                ),
                'manage_autoscalling'           => array(
                        'title'                 => 'Autoscalling',
                        'description'           => '',
                        'type'                  => 'checkbox',
                ),
                'show_cpu_share'                => array(
                        'title'                 => 'Display CPU Priority',
                        'description'           => '',
                        'type'                  => 'checkbox'
                ),
                'show_api_info'             => array(
                        'title'                 => 'Display API Info',
                        'description'           => '',
                        'type'                  => 'checkbox',
                ),
                
                
            
	);
	
	public function setupDefaultConfigurableOptions(){
		mysql_query('INSERT INTO tblproductconfiggroups(name,description) VALUES("Configurable options for onAppCloud","Auto generated by module")');
		$group_id = mysql_insert_id();
		mysql_query_safe('INSERT INTO tblproductconfiglinks(gid,pid) VALUES(?,?)', array($group_id, $this->id));
		$query = 'INSERT INTO tblproductconfigoptions(gid,optionname,optiontype,qtyminimum,qtymaximum,`order`,hidden) VALUES(?,?,4,?,?,0,0)';
		$options_ids = array();
		
                $mem_min    = $this->getConfig('memory_unit')  == "" || $this->getConfig('memory_unit')  == "MB"  ? 128    : 1;
                $mem_max    = $this->getConfig('memory_unit')  == "" || $this->getConfig('memory_unit')  == "MB"  ? 2048   : 6;
                $disk_min   = $this->getConfig('primary_unit') == "" || $this->getConfig('primary_unit') == "MB"  ? 5120   : 5;
                $disk_max   = $this->getConfig('primary_unit') == "" || $this->getConfig('primary_unit') == "MB"  ? 51200  : 100;
                $swap_min   = $this->getConfig('swap_unit')    == "" || $this->getConfig('swap_unit')    == "MB"  ? 1024   : 1;
                
                mysql_query_safe($query, array( $group_id, 'vm_limit|Virtual Machine Limit',1,100 ));
		$options_ids['vm_limit']	= mysql_insert_id();      
                mysql_query_safe($query, array( $group_id, 'backup_limits|Backups',0,10 ));
		$options_ids['backup_limits']	= mysql_insert_id();
		mysql_query_safe($query, array( $group_id, 'memory|Memory',  $mem_min , $mem_max));
		$options_ids['memory']		= mysql_insert_id();
		mysql_query_safe($query, array( $group_id, 'cpus|CPU(s)',1,100));
		$options_ids['cpus']		= mysql_insert_id();
		mysql_query_safe($query, array( $group_id, 'primary_disk_size|Primary Disk Size',$disk_min, $disk_max));
		$options_ids['disk']		= mysql_insert_id();
		mysql_query_safe($query, array( $group_id, 'swap_disk_size|Swap Disk Size', $swap_min, $disk_max));
		$options_ids['swap']            = mysql_insert_id();
		mysql_query_safe($query, array( $group_id, 'ip_addresses|Extra IP Address',1,100));
		$options_ids['ips']		= mysql_insert_id();
                mysql_query_safe($query, array( $group_id, 'rate_limit|Port Speed',1,100));
		$options_ids['rate_limit']	= mysql_insert_id();
                mysql_query_safe($query, array( $group_id, 'cpu_shares|CPU Priority',1,500));
		$options_ids['cpu_shares']	= mysql_insert_id();
                
		
                
		$query_suboptions = 'INSERT INTO tblproductconfigoptionssub(configid,optionname,sortorder,hidden) VALUES(?,?,0,0)';
		
                mysql_query_safe($query_suboptions, array($options_ids['vm_limit'], 'Unit'));
                mysql_query_safe($query_suboptions, array($options_ids['backup_limits'], 'Unit'));
		mysql_query_safe($query_suboptions, array($options_ids['memory'],   $this->getConfig('memory_unit')  == ""  ? 'MB' : $this->getConfig('memory_unit')));
		mysql_query_safe($query_suboptions, array($options_ids['disk'],     $this->getConfig('primary_unit') == ""  ? 'MB' : $this->getConfig('primary_unit')));                
                mysql_query_safe($query_suboptions, array($options_ids['swap'],     $this->getConfig('swap_unit')    == ""  ? 'MB' : $this->getConfig('swap_unit')));	
		mysql_query_safe($query_suboptions, array($options_ids['cpus'], 'Unit'));
		mysql_query_safe($query_suboptions, array($options_ids['ips'], 'Unit')); 
                mysql_query_safe($query_suboptions, array($options_ids['rate_limit'], 'Mbps'));
                mysql_query_safe($query_suboptions, array($options_ids['cpu_shares'], 'Unit'));
			
		return true;
	}
	
	public function setupDefaultCustomFields(){
                $q = mysql_query('
			SELECT id, relid, fieldname
			FROM tblcustomfields
			WHERE
				relid = '.(int)$this->id.'
				AND type = "product"
				AND fieldname IN("userid|User ID")
		');
		
		$fieldnames = array();
		while ($row = mysql_fetch_assoc($q)){
			$fieldnames[] = $row['fieldname'];
		}
		
		$query_raw = 'INSERT INTO tblcustomfields(type,relid,fieldname,fieldtype,description,fieldoptions,regexpr,adminonly,required,showorder,showinvoice,sortorder)
			VALUES("product", '.$this->id.', ?, "text", "", "", "", "on", "", "", "", 0)';
		
		if (!in_array('userid|User ID', $fieldnames))       mysql_query_safe($query_raw, array('userid|User ID'));
               
                
                return true;
	}
	

        public function hasConfigurableOptions(){
		$q = mysql_query_safe('SELECT * FROM tblproductconfiglinks WHERE pid = ?', array($this->id));
		return (bool)mysql_num_rows($q);
	}
        
        public function customFieldExists($name){
                $q = mysql_query_safe('
                    SELECT id, relid, fieldname
                    FROM tblcustomfields
                    WHERE relid = ? AND type = "product" AND fieldname = ?
                    ',array($this->id,mysql_real_escape_string($name)));
		
		return (bool)mysql_num_rows($q);
	}

	public function getValueWithUnit($val, $new = 'MB', $unit = 'MB')
        {
               if($new == $unit)
               {
                   return $val;
               } else if( $new == 'GB' && $unit == 'MB')
               {
                   return $val*1024;
               } else if( $new == 'MB' && $unit == 'GB')
               {
                   return ceil($val/1024);
               }
            
        }
}


?>