
{**
 * @author Maciej Husak <maciej@modulesgarden.com>
 *}
 
<link rel="stylesheet" type="text/css" href="{$dir}/css/style.css" />
<div>
<a href="clientarea.php?action=productdetails&id={$id}&modop=custom&a=management" class="btn btn-small"><i class="icon-arrow-left"></i> {$lang.back}</a>
<h2 class="set_main_header">{$lang.usage_records}</h2>
{if $msg_error or $msg_success}
    <div class="alert {if $msg_error}alert-danger{else}alert-success{/if}">
        <p></p><li>{if $msg_error}{$msg_error}{else}{$msg_success}{/if}</li><p></p>
    </div>
{/if}

{if $items}
<div style="width: 100%;overflow:auto;">
<table class="table pagiantion" id="mg_items">
    <thead>
        <tr style="white-space:nowrap;">
        {foreach from=$resources item="r"} 
            <th>{$r.FriendlyName}</th>     
        {/foreach}
        <th style="width: 90px">{$lang.record_total}</th>
        <th style="width: 90px">{$lang.last_update}</th>
</tr>
</thead>
<tbody>
{foreach from=$items item="item"}
    <tr>
        {foreach from=$resources item="r" key="r_key"}
        <td>
            <span class="tooltip-box">
                {assign var="key" value="r.`$r_key`"}
                {$item.$key} {$r.unit}
            </span>
        </td>
        {/foreach}
    <td style="white-space: nowrap;">{$currency.prefix}{$item.total} {$currency.suffix}</td>
    <td style="white-space: nowrap;">{$item.date}</td>
    {*<td>
        <form action="addonmodules.php?module=OnAppBilling&modpage=items&modsubpage=show&id={$smarty.get.id}" method="post" style="margin: 0; text-align: center">
            <input type="hidden" name="modaction" value="delete" />
            <input type="hidden" name="itemid" value="{$item.id}" />
            <button class="btn-link btn-delete"><i class="icon-remove"></i></button>
        </form>
      </td>
      *}
    </tr>
{/foreach}
</tbody>
<tfoot class="blue-foot">
        <tr class="level1">
            <td style="text-align: center" colspan="{$resource_counter}">Total for <b>all</b> usage records</td>
        </tr>
        <tr class="level2">
        {foreach from=$resources key="u" item="r"}
            <td>
            {if $r.type == 'average'}
                <span class="tooltip-box">
                    {$summary_usage.$u.usage} {$summary_usage.$u.unit}
                </span>
            {elseif $r.type == 'summary'}
                <span class="tooltip-box">
                  {$summary_usage.$u.usage} {$summary_usage.$u.unit}
                </span>
            {else}       
                Disabled
            {/if}
            </td>
        {/foreach}
        <td></td>
        <td></td>
</tr>
        <tr class="level3">
        {foreach from=$resources key="u" item="r"}
            <td>
            {if $r.type == 'average' || $r['type'] == 'summary'}
               <span class="tooltip-box">
                  {$currency.prefix}{$summary_usage.$u.total}{$currency.suffix}
               </span>
            {else}
                Disabled
            {/if}
            </td>
        {/foreach}
        <td></td>
        <td></td>
      </tfoot>
    </table>
</div>
</div>        
<div class="pagination pagination-right">{$pagination}</div>
{/if}