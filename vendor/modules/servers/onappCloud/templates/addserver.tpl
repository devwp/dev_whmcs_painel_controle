
{**
* @author Maciej Husak <maciej@modulesgarden.com>
*}
<link rel="stylesheet" type="text/css" href="{$dir}/css/style.css" />
<script type=text/javascript src="{$dir}/jquery-ui-1.10.3.custom/js/jquery-ui-1.10.3.custom.min.js"></script>
<link href="{$dir}/jquery-ui-1.10.3.custom/css/ui-lightness/jquery-ui-1.10.3.custom.min.css" rel="stylesheet">
<a href="clientarea.php?action=productdetails&id={$id}&modop=custom&a=management" class="btn btn-small"><i class="icon-arrow-left"></i> {$lang.back}</a>
<h2 class="header_label">{$lang.main_header}</h3>

{if $msg_error or $msg_success}
    <div class="alert {if $msg_error}alert-danger{else}alert-success{/if}">
        <p></p><li>{if $msg_error}{$msg_error}{else}{$msg_success}{/if}</li><p></p>
    </div>
{/if}
<form method="post" id="onapp_add_form">
    <input type="hidden" name="do" value="createVPS" />
    <table class="table table-bordered table-striped solus_stats" style="width:100%;">
        <tr>
            <td>{$lang.label}</td>
            <td style="width:500px;"><input type="text" name="cl_label" value="{$smarty.post.cl_label|escape:'html'}" /></td>
        </tr>
        <tr>
            <td>{$lang.hostname}</td>
            <td style="width:500px;"><input type="text" name="cl_hostname" value="{$smarty.post.cl_hostname|escape:'html'}" /></td>
        </tr>
        <tr>
            <td>{$lang.template}</td>
            <td>
                <select name="cl_template">
                    {foreach from=$templates key="k" item=tem name=ind}
                        <option value="{$k}|{$tem}" {if $smarty.post.cl_template==$k}selected{/if} >{$tem}</option>
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr>
            <td>{$lang.root_password}</td>
            <td><input type="password" name="cl_password" value="{$smarty.post.cl_password|escape:'html'}" /></td>
        </tr>
        <tr>
            <td>{$lang.licensing_key}</td>
            <td><input type="text" name="cl_licensing_key" value="{$smarty.post.licensing_key|escape:'html'}" /></td>
        </tr>
        <tr>
            <td>{$lang.memory}</td>
            <td>
                {if $enable_pricing}
                    <div class="price">
                        <span class='cl_memory_price'>{$maxes.memory.price} /hr</span><br />
                    </div>
                {/if}
                <div class="so_slider">
                    <div id="cl_memory"></div>
                    <span class="so_label"> 
                        <label class="min">128</label>
                        <label class="max">{$maxes.memory.free}</label>
                    </span>
                </div>
                <input type="text" class="cl_numeric" name="cl_memory" value="{if $smarty.post.cl_memory>0}{$smarty.post.cl_memory|escape:'html'}{else}128{/if}" data-min="128" data-max="{$maxes.memory.free}" /> MB 
            </td>
        </tr>
        <tr>
            <td>{$lang.cpu_cores}</td>
            <td>
                {if $enable_pricing}
                    <div class="price">
                        <span class='cl_cpu_price'>{$maxes.cpus.price} /hr</span><br />
                    </div>
                {/if}
                <div class="so_slider">
                    <div id="cl_cpu"></div>
                    <span class="so_label"> 
                        <label class="min">1</label>
                        <label class="max">{$maxes.cpus.free}</label>
                    </span>
                </div>
                <input type="text" class="cl_numeric" name="cl_cpu" value="{if $smarty.post.cl_cpu>0}{$smarty.post.cl_cpu|escape:'html'}{else}1{/if}" data-min="1" data-max="{$maxes.cpus.free}" />
            </td>
        </tr>
        <tr>
            <td>{$lang.cpu_shares}</td>
            <td>
                {if $enable_pricing}
                    <div class="price">
                        <span class='cl_cpu_shares_price'>{$maxes.cpu_shares.price} /hr</span><br />
                    </div>
                {/if}
                <div class="so_slider">
                    <div id="cl_cpu_shares"></div>
                    <span class="so_label"> 
                        <label class="min">1</label>
                        <label class="max">{if $maxes.cpu_shares.free>100}100{else}{$maxes.cpu_shares.free}{/if}</label>
                    </span>
                </div>
                <input type="text" class="cl_numeric" name="cl_cpu_shares" value="{if $smarty.post.cl_cpu_shares>0}{$smarty.post.cl_cpu_shares|escape:'html'}{else}1{/if}" data-min="1" data-max="{if $maxes.cpu_shares.free>100}100{else}{$maxes.cpu_shares.free}{/if}" /> %
            </td>
        </tr>
        <tr>
            <td>{$lang.primary_disk_size}</td>
            <td>
                {if $enable_pricing}
                    <div class="price">
                        <span class='cl_primary_disk_price'>{$maxes.disk_size.price} /hr</span><br />
                    </div>
                {/if}
                <div class="so_slider">
                    <div id="cl_primary_disk"></div>
                    <span class="so_label"> 
                        <label class="min">{if $maxes.disk_size.free >5}5{else}0{/if}</label>
                        <label class="max">{$maxes.disk_size.free}</label>
                    </span>
                </div>
                <input type="text" class="cl_numeric" name="cl_primary_disk" value="{if $smarty.post.cl_primary_disk>0}{$smarty.post.cl_primary_disk|escape:'html'}{else if $maxes.disk_size.free >5}5{else}0{/if}" data-min="5" data-max="{$maxes.disk_size.free}" /> GB
            </td>
        </tr>
        {*{if $datastores|@count > 0}
        <tr>
            <td>{$lang.primary_data_store}</td>
            <td>
                {if $enable_pricing}
                    <div class="price">
                        <span class='cl_primary_data_store_price'>{$maxes.primary_data_store.price} /hr</span><br />
                    </div>
                {/if}
                <select name="cl_primary_data_store">
                    {foreach from=$datastores item="store" key="k"}
                        <option value="{$k}" {if $smarty.post.primary_data_store==$k}selected{/if}>{$store}</option>
                    {/foreach}
                </select>
            </td>
        </tr>
        {/if}*}
        <tr>
            <td>{$lang.swap_disk_size}</td>
            <td>
                {if $enable_pricing}
                    <div class="price">
                        <span class='cl_swap_disk_price'>{$maxes.disk_size.price} /hr</span><br />
                    </div>
                {/if}
                <div class="so_slider">
                    <div id="cl_swap_disk"></div>
                    <span class="so_label"> 
                        <label class="min">0</label>
                        <label class="max">{if $maxes.disk_size.free< $swap_disk_size} {$maxes.disk_size.free}{else}{$swap_disk_size}{/if}</label>
                    </span>
                </div>
                <input type="text" class="cl_numeric" name="cl_swap_disk" value="{if $smarty.post.cl_swap_disk>0}{$smarty.post.cl_swap_disk|escape:'html'}{else}0{/if}" data-min="0" data-max="{if $maxes.disk_size.free} {$maxes.disk_size.free}{else}{/if}" /> GB
            </td>
        </tr>
        {*{if $datastores|@count > 0}
        <tr>

            <td>{$lang.swap_data_store}</td>
            <td>
                {if $enable_pricing}
                    <div class="price">
                        <span class='cl_swap_data_store_price'>{$maxes.swap_data_store.price} /hr</span><br />
                    </div>
                {/if}
                <select name="cl_swap_data_store">
                    {foreach from=$datastores item="store" key="k"}
                        <option value="{$k}" {if $smarty.post.cl_swap_data_store==$k}selected{/if}>{$store}</option>
                    {/foreach}
                </select>
            </td>
        </tr>
        {/if}*}
        <tr>
            <td>{$lang.ip_addresses}</td>
            <td>
                {if $enable_pricing}
                    <div class="price">
                        <span class='cl_ips_price'>{$maxes.ips.price} /hr</span><br />
                    </div>
                {/if}
                <div class="so_slider">
                    <div id="cl_ips"></div>
                    <span class="so_label"> 
                        <label class="min">1</label>
                        <label class="max">{$maxes.ips.free}</label>
                    </span>
                </div>
                <input type="text" class="cl_numeric" name="cl_ips" value="{if $smarty.post.cl_ips>1}{$smarty.post.cl_ips|escape:'html'}{else}1{/if}" data-min="1" data-max="{$maxes.ips.free}" />
            </td>
        </tr>
        <tr>
            <td>{$lang.rate_limit}</td>
            <td>
                {if $enable_pricing}
                    <div class="price">
                        <span class='cl_rate_limit_price'>{$maxes.rate_limit.price} /hr</span><br />
                    </div>
                {/if}
                <div class="so_slider">
                    <div id="cl_rate_limit"></div>
                    <span class="so_label"> 
                        <label class="min">1</label>
                        <label class="max">{$maxes.rate_limit.free}</label>
                    </span>
                </div>
                <input type="text" class="cl_numeric" name="cl_rate_limit" value="{if $smarty.post.cl_rate_limit>0}{$smarty.post.cl_rate_limit|escape:'html'}{else}1{/if}" data-min="1" data-max="{$maxes.rate_limit.free}" /> Mbps
            </td>
        </tr>
        <tr>
            <td>{$lang.network_zone}</td>
            <td>
                {if $enable_pricing && $maxes.network_zone.price}
                    <div class="price">
                        <span class='cl_network_zone_price'>{$maxes.network_zone.price} /hr</span><br />
                    </div>
                {/if}
                <select name="cl_network_zone">
                    {foreach from=$networkzones item="zone" key="k"}
                        <option value="{$k}" {if $smarty.post.cl_network_zone==$k}selected{/if}>{$zone}</option>
                    {/foreach}
                </select>
            </td>
        </tr>
    </table>
    <input type="submit" value="{$lang.btn_add}" class="btn btn-large btn-primary" />
</form>

{literal} 
    <script type="text/javascript">
        var price = {
            "cl_memory_price":      {/literal}{if $maxes.memory.price}{$maxes.memory.price}{else}0{/if}{literal},
            "cl_cpu_price":         {/literal}{if $maxes.cpus.price}{$maxes.cpus.price}{else}0{/if}{literal},
            "cl_primary_disk_price":{/literal}{if $maxes.disk_size.price}{$maxes.disk_size.price}{else}0{/if}{literal},
            "cl_swap_disk_price":   {/literal}{if $maxes.disk_size.price}{$maxes.disk_size.price}{else}0{/if}{literal},
            "cl_ips_price":         {/literal}{if $maxes.ips.price}{$maxes.ips.price}{else}0{/if}{literal},
            "cl_rate_limit_price":  {/literal}{if $maxes.rate_limit.price}{$maxes.rate_limit.price}{else}0{/if}{literal},
            "cl_cpu_shares_price":  {/literal}{if $maxes.cpu_shares.price}{$maxes.cpu_shares.price}{else}0{/if}{literal}
        };
        
        jQuery(document).ready(function() {           
            var swap_disk_size = parseInt("{/literal}{$swap_disk_size}{literal}");
            console.log(swap_disk_size);
            jQuery('#onapp_add_form .so_slider div').each(function(){
                var input = jQuery(this).closest("td").find("input.cl_numeric");
                var min = parseInt(jQuery(input).attr('data-min'));
                var max = parseInt(jQuery(input).attr('data-max'));
                jQuery(this).slider({
                    min: min,
                    max: max,
                    range: "min",
                    value: jQuery(input).val(),
                    slide: function(event, ui) {
                        if(event.target !== undefined)
                        {
                            jQuery("input[name="+event.target.id+"]").val(ui.value);
                            {/literal}
                                {if $enable_pricing}
                                    {literal}
                                        jQuery("span."+event.target.id+"_price").text((ui.value*price[event.target.id+"_price"]).toFixed(5)+' /hr');
                                    {/literal}
                                {/if}
                            {literal}   
                            if(event.target.id == 'cl_primary_disk'){
                                var item_max = jQuery("input[name=cl_primary_disk]").attr("data-max") - jQuery("input[name=cl_primary_disk]").val();
                                jQuery("input[name='cl_swap_disk']").attr("data-max",item_max);
                                if(jQuery("input[name='cl_swap_disk']").val()>=item_max)
                                    jQuery("input[name='cl_swap_disk']").val(item_max);
                                
                                jQuery("div#cl_swap_disk").slider({max: jQuery("input[name='cl_swap_disk']").attr("data-max")});
                            }    
                        }
                    },
                    change: function(event, ui) {
                        if(event.target.id !== undefined)
                        {
                            jQuery("input[name="+event.target.id+"]").val(ui.value);
                            if(event.target.id == 'cl_primary_disk'){
                                var item_max = jQuery("input[name=cl_primary_disk]").attr("data-max") - jQuery("input[name=cl_primary_disk]").val();
                                jQuery("input[name='cl_swap_disk']").attr("data-max",item_max);
                                if(jQuery("input[name='cl_swap_disk']").val()>=item_max)
                                    jQuery("input[name='cl_swap_disk']").val(item_max);
                                var max = swap_disk_size && jQuery("input[name='cl_swap_disk']").attr("data-max") > swap_disk_size ? swap_disk_size : jQuery("input[name='cl_swap_disk']").attr("data-max");
                                 $("#cl_swap_disk").parent().find(".max").text(max);
                                jQuery("div#cl_swap_disk").slider({max: max});
                            }
                        }
                    }
                });
            }); 
            
            jQuery('#onapp_add_form .cl_numeric').change(function(){
                var value = jQuery(this).val();
                if(!value.match(/[0-9 -()+]+$/))
                {
                    value = jQuery(this).attr('data-min');
                }
                else if(parseInt(value) < jQuery(this).attr('data-min'))
                {
                    value = jQuery(this).attr('data-min');
                }
                else
                {
                    if(parseInt(value) > jQuery(this).attr('data-max'))
                    {
                        value = jQuery(this).attr('data-max');
                    }
                }

                jQuery(this).val(value);
                jQuery('#'+jQuery(this).attr('name')).slider('value',value);
            }); 
            
             var template = jQuery('select[name=cl_template]').val().toLowerCase();
                if(template.indexOf("windows")>-1){
                    jQuery("input[name=licensing_key]").closest("tr").show();
                    jQuery("input[name=cl_swap_disk]").closest("tr").hide();
                } else {
                    jQuery("input[name=cl_swap_disk]").closest("tr").show();
                    jQuery("input[name=licensing_key]").closest("tr").hide();
            }
            
            
            jQuery('select[name=cl_template]').change(function(){
                var template = jQuery(this).val().toLowerCase();
                if(template.indexOf("windows")>-1){
                    jQuery("input[name=cl_swap_disk],select[name=cl_swap_data_store]").closest("tr").hide();
                } else {
                    jQuery("input[name=cl_swap_disk],select[name=cl_swap_data_store]").closest("tr").show();
                }
            });

            
        });
    </script>
{/literal}
