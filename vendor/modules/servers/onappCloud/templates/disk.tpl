<link rel="stylesheet" type="text/css" href="{$dir}/css/style.css" />
<div>
    {if $step =='addDisk'}
        {include file="$main_dir/templates/disk_add.tpl"}
    {else}
    <a href="clientarea.php?action=productdetails&id={$id}&modop=custom&a=management&page=vmdetails&vserver={$smarty.get.vserver}" class="btn btn-small"><i class="icon-arrow-left"></i> {$lang.back}</a>
    <h2 class="set_main_header">{$lang.main_header}</h2>
    {if $msg_error or $msg_success}
        <div class="alert {if $msg_error}alert-danger{else}alert-success{/if}">
            <p></p><li>{if $msg_error}{$msg_error}{else}{$msg_success}{/if}</li><p></p>
        </div>
    {/if}
         <table class="table table-striped table-disk">
            <thead>
                <tr>
                    <th>{$lang.disk}</th>
                    <th>{$lang.label}</th>
                    <th>{$lang.size}</th>
                    <th>{$lang.data_store}</th>
                    <th>{$lang.type}</th>
                    <th>{$lang.built}</th>
                    <th>{$lang.backups}</th>
                    <th>{$lang.autobackups}</th>
                    <th>{$lang.actions}</th>
                </tr>
            </thead>
            <tbody>    
                {foreach from=$disks item="entry"}
                    <tr>
                        <td>#{$entry.disk.id}</td>
                        <td>{$entry.disk.label}</td>
                        <td>{$entry.disk.disk_size}{$lang.GB}</td>
                        <td>{$entry.disk.data_store_label}</td>
                        <td>{if $entry.disk.is_swap !=1}{$lang.standard}{else}{$lang.swap}{/if}{if $entry.disk.primary==1}<br />{$lang.primary}{/if}</td>
                        <td>{if $entry.disk.built==1}{$lang.yes}{else}{$lang.no}{/if}</td>
                        <td>{$entry.disk.count_backups}</td>
                        <td>{if $entry.disk.has_autobackups==1}{$lang.yes}{else}{$lang.no}{/if}</td>
                        <td style="text-align:center;">
                           
                            <form action="" method="post" class="action-form" style="display: inline;margin-right: 5px;">   
                                <input type="hidden" name="disk_id" value="{$entry.disk.id}" />
                                <button class="btn" name="do" value="deleteDisk" title="{$lang.delete_disk}" onclick="return confirm('{$lang.confirm_delete}');" />
                                    <img src="{$dir}/img/delete.png" alt="{$lang.delete_disk}" />
                                </button>
                                {if $entry.disk.is_swap !=1}   
                                <button class="btn" name="do" value="createBackup" title="{$lang.create_backup}" />
                                    <img src="{$dir}/img/create_backup.png" alt="{$lang.create_backup}" />
                                </button>
                                {/if}
                            </form>
                            {if $entry.disk.is_swap !=1}    
                            <form action="clientarea.php?action=productdetails&id={$id}&modop=custom&a=management&page=schedule&vserver={$vpsdata.id}" method="post" style="display: inline;" class="action-form">   
                                <input type="hidden" name="disk_id" value="{$entry.disk.id}" />
                                <button class="btn" name="doAction" value="new" id="schedule"  title="{$lang.schedule}" />
                                    <img src="{$dir}/img/calendar.png" alt="{$lang.schedule}" />
                                </button>
                            </form>    
                            {/if}
                        </td>
                    </tr>
                {foreachelse}
                    <tr>
                        <td colspan="9" class="td_center">{$lang.nothing_label}</td>
                    </tr>
                {/foreach}
            </tbody>
          </table>
          <a href="clientarea.php?action=productdetails&id={$id}&&modop=custom&a=management&page=disk&do=addDisk&vserver={$vpsdata.id}" title="{$lang.create_disk}" class="btn btn-success">{$lang.create_disk}</a>
      
       {/if}
</div>            
