
{**
* @author Maciej Husak <maciej@modulesgarden.com>
*}
<link rel="stylesheet" type="text/css" href="{$dir}/css/style.css" />
<script type=text/javascript src="{$dir}/jquery-ui-1.10.3.custom/js/jquery-ui-1.10.3.custom.min.js"></script>
<link href="{$dir}/jquery-ui-1.10.3.custom/css/ui-lightness/jquery-ui-1.10.3.custom.min.css" rel="stylesheet">
<a href="clientarea.php?action=productdetails&id={$id}&modop=custom&a=management&page=vmdetails&vserver={$vpsdata.id}" class="btn btn-small"><i class="icon-arrow-left"></i> {$lang.back}</a>
<h2 class="header_label">{$lang.main_header}</h3>

{if $msg_error or $msg_success}
        <div class="alert {if $msg_error}alert-danger{else}alert-success{/if}">
            <p></p><li>{if $msg_error}{$msg_error}{else}{$msg_success}{/if}</li><p></p>
        </div>
{/if}

<form method="post" id="onapp_add_form">
    <input type="hidden" name="do" value="editVPS" />
    <table class="table table-bordered table-striped" style="width:100%;">
        <tr>
            <td>{$lang.label}</td>
            <td style="width:500px;"><input type="text" name="cl_label" value="{if $smarty.post.cl_label}{$smarty.post.cl_label|escape:'html'}{else}{$vpsdata.label}{/if}" /></td>
        </tr>
        {*<tr>
            <td>{$lang.template}</td>
            <td>
                <select class="cl_numeric" name="cl_template">
                    {foreach from=$templates key="k" item=tem name=ind}
                        <option value="{$k}" {if $smarty.post.cl_template==$k}selected{elseif $k=$vpsdata.template_id}selected{/if}>{$tem}</option>
                    {/foreach}
                </select>
            </td>
        </tr>*}
        <tr>
            <td>{$lang.new_root_password}<br /><span class='small'>{$lang.leave_blank_pass}</span>            </td>
            <td><input type="text" name="cl_password" value="{$smarty.post.cl_password|escape:'html'}" /></td>
        </tr>
        <tr>
            <td>{$lang.memory}</td>
            <td>
                {if $enable_pricing}
                    <div class="price">
                        <span class='memory_price'>{$maxes.memory.price} /hr</span><br />
                    </div>
                {/if}
                <div class="so_slider">
                    <div id="cl_memory"></div>
                    <span class="so_label"> 
                        <label class="min">128</label>
                        <label class="max">{$maxes.memory.free}</label>
                    </span>
                </div>
                <input type="text" class="cl_numeric" name="cl_memory" value="{if $smarty.post.cl_memory>0}{$smarty.post.cl_memory|escape:'html'}{else}{$vpsdata.memory}{/if}" data-min="128" data-max="{$maxes.memory.free}" /> MB
            </td>
        </tr>
        <tr>
            <td>{$lang.cpu_cores}</td>
            <td>
                {if $enable_pricing}
                    <div class="price">
                        <span class='cl_cpu_price'>{$maxes.cpus.price} /hr</span><br />
                    </div>
                {/if}
               <div class="so_slider">
                   <div id="cl_cpu"></div>
                   <span class="so_label"> 
                        <label class="min">1</label>
                        <label class="max">{$maxes.cpus.free}</label>
                   </span>
               </div>
               <input type="text" class="cl_numeric" name="cl_cpu" value="{if $smarty.post.cl_cpu>0}{$smarty.post.cl_cpu|escape:'html'}{else}{$vpsdata.cpus}{/if}" data-min="1" data-max="{$maxes.cpus.free}" />
            </td>
        </tr>
        <tr>
            <td>{$lang.cpu_shares}</td>
            <td>
                {if $enable_pricing}
                    <div class="price">
                        <span class='cl_cpu_shares_price'>{$maxes.cpu_shares.price} /hr</span><br />
                    </div>
                {/if}
                <div class="so_slider"><div id="cl_cpu_shares"></div></div>
                <input type="text" class="cl_numeric" name="cl_cpu_shares" value="{if $smarty.post.cl_cpu_shares>0}{$smarty.post.cl_cpu_shares|escape:'html'}{else}{$vpsdata.cpu_shares}{/if}" data-min="1" data-max="{if $maxes.cpu_shares.free>100}100{else}{$maxes.cpu_shares.free}{/if}" /> %
            </td>
        </tr>
        <tr>
            <td>{$lang.ip_addresses}</td>
            <td>
                {if $enable_pricing}
                    <div class="price">
                        <span class='cl_ips_price'>{$maxes.ips.price} /hr</span><br />
                    </div>
                {/if}
                <div class="so_slider"><div id="cl_ips"></div></div>
                <input type="text" class="cl_numeric" name="cl_ips" value="{if $smarty.post.cl_ips>0}{$smarty.post.cl_ips|escape:'html'}{else}{$vpsdata.ip_addresses|@count}{/if}" data-min="1" data-max="{$maxes.ips.free}" />
            </td>
        </tr>
        <tr>
            <td>{$lang.rate_limit}</td>
            <td>
                {if $enable_pricing}
                    <div class="price">
                        <span class='cl_rate_limit_price'>{$maxes.rate_limit.price} /hr</span><br />
                    </div>
                {/if}
                <div class="so_slider"><div id="cl_rate_limit"></div></div>
                <input type="text" class="cl_numeric" name="cl_rate_limit" value="{if $smarty.post.cl_rate_limit>0}{$smarty.post.cl_rate_limit|escape:'html'}{else}{$rate}{/if}" data-min="0" data-max="{$maxes.rate_limit.free}" /> Mbps
            </td>
        </tr>
    </table>
    <input type="submit" value="{$lang.btn_add}" class="btn btn-large btn-primary" />
</form>

{literal}
    <script type="text/javascript">
        jQuery(document).ready(function() { 
            var price = {
                "cl_memory_price":      {/literal}{if $maxes.memory.price}{$maxes.memory.price}{else}0{/if}{literal},
                "cl_cpu_price":         {/literal}{if $maxes.cpus.price}{$maxes.cpus.price}{else}0{/if}{literal},
                "cl_ips_price":         {/literal}{if $maxes.ips.price}{$maxes.ips.price}{else}0{/if}{literal},
                "cl_rate_limit_price":  {/literal}{if $maxes.rate_limit.price}{$maxes.rate_limit.price}{else}0{/if}{literal},
                "cl_cpu_shares_price":  {/literal}{if $maxes.cpu_shares.price}{$maxes.cpu_shares.price}{else}0{/if}{literal}
            };
           
            jQuery('#onapp_add_form .so_slider div').each(function(){
                var input = jQuery(this).closest("td").find("input.cl_numeric");
                var min = parseInt(jQuery(input).attr('data-min'));
                var max = parseInt(jQuery(input).attr('data-max'));
                jQuery(this).slider({
                    min: min,
                    max: max,
                    range: "min",
                    value: jQuery(input).val(),
                    slide: function(event, ui) {
                        if(event.target !== undefined)
                        {
                            jQuery("input[name="+event.target.id+"]").val(ui.value);
                            {/literal}
                                {if $enable_pricing}
                                    {literal}
                                        jQuery("span."+event.target.id+"_price").text((ui.value*price[event.target.id+"_price"]).toFixed(5)+' /hr');
                                    {/literal}
                                {/if}
                            {literal}
                        }
                    },
                    change: function(event, ui) {
                        if(event.target.id !== undefined)
                        {
                            jQuery("input[name="+event.target.id+"]").val(ui.value);
                        }
                    }
                });
            }); 
            
            jQuery('#onapp_add_form .cl_numeric').change(function(){
                var value = jQuery(this).val();
                if(!value.match(/[0-9 -()+]+$/))
                {
                    value = jQuery(this).attr('data-min');
                }
                else if(parseInt(value) < jQuery(this).attr('data-min'))
                {
                    value = jQuery(this).attr('data-min');
                }
                else
                {
                    if(parseInt(value) > jQuery(this).attr('data-max'))
                    {
                        value = jQuery(this).attr('data-max');
                    }
                }

                jQuery(this).val(value);
                jQuery('#'+jQuery(this).attr('name')).slider('value',value);
            }); 
            
            
        });
    </script>
{/literal}