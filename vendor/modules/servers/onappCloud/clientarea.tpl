
{**
* @author Maciej Husak <maciej@modulesgarden.com>
*}
<link rel="stylesheet" type="text/css" href="{$dir}/css/style.css" />
<div>
    <div class="create_vps">
          {if $disallow_action.api_info !=1}
                    <button class="btn" onclick="window.location='clientarea.php?action=productdetails&id={$id}&modop=custom&a=management&page=apiInfo'; return false;"><span class='icon-info-sign'><i></i></span> {$lang.apiInfo}</button>
          {/if}
                
        <button class="btn-primary btn" onclick="window.location = 'clientarea.php?action=productdetails&id={$id}&modop=custom&a=management&page=addserver';
                return false;"><i class="icon-plus icon-white"></i> {$lang.create_virtual_server}</button>
    </div>    
    <h3 class="set_main_header">{$lang.main_header}</h3> 
    <div id="vm_alerts">
        {if $result}
            <div class="{if $result == 'success'}box-success{else}box-error{/if}">
                {$resultmsg}
            </div>
        {/if}
    </div>
    <table class="table table-bordered vps_list">
        <thead>
            <tr><th>{$lang.hostname}</th><th>{$lang.active}</th><th></th></tr>
        </thead>
        <tbody>
            {foreach from=$vpslist item="entry"}
                <tr>
                    <td>{$entry.virtual_machine.hostname}</td>
                    <td style="text-align:center;vertical-align: middle">{if $entry.virtual_machine.booted==true}<img src="{$dir}/img/statusok.gif" alt="" />{else}<img src="{$dir}/img/off.png" alt="" />{/if}</td>
                    <td>
                        <a href="clientarea.php?action=productdetails&id={$id}&modop=custom&a=management&page=vmdetails&vserver={$entry.virtual_machine.id}" class="btn btn-primary"><i class="icon-wrench icon-white"></i> {$lang.manage}</a>
                        <a href="" class="btn btn-danger vm_delete" rel="{$entry.virtual_machine.id}"><i class="icon-remove icon-white"></i> {$lang.delete}</a> 
                    </td>
                </tr>
            {foreachelse}
                <tr><td colspan="3">{$lang.no_virtual_servers}</td></tr>
                {/foreach}
        </tbody>
    </table>
    <h3 class="set_main_header">{$lang.your_resources}</h3>     
    <table class="table table-bordered">
        <tr>
            <td>{$lang.vm}</td>
            <td>
                <div class="vm_progress-total">{$resources.vm.used} {$lang.of} {if !$resources.vm.max}{$lang.unlimited}{else}<strong>{$resources.vm.max}</strong> {$lang.used} / {math equation="(y -x )" x=$resources.vm.used y=$resources.vm.max} {$lang.free}{/if}</div>
                <div class="vm_progress-wrap vm_progress" data-progress-percent="{math equation="(x / y)*100" x=$resources.vm.used y=$resources.vm.max format="%.2f"}">
                    <div class="vm_progress-bar vm_progress"></div>
                    <div class='vm_progress-percent'>{if !$resources.vm.max}{$resources.vm.used}{else}{math equation="(x / y)*100" x=$resources.vm.used y=$resources.vm.max format="%.2f"}%{/if}</div>
                </div>
                <div style="clear:both;"></div>
            </td>
        </tr>
        <tr>
            <td>{$lang.memory}</td>
            <td>
                <div class="vm_progress-total">{$resources.memory.used_mb} {$lang.of} {if !$resources.memory.max_mb || $resources.memory.max_mb==0}{$lang.unlimited}{else}<strong>{$resources.memory.max_mb}</strong> {$lang.used} / {$resources.memory.free} {$lang.free}{/if}</div>
                <div class="vm_progress-wrap vm_progress" data-progress-percent="{math equation="(x / y)*100" x=$resources.memory.used y=$resources.memory.max format="%.2f"}">
                    <div class="vm_progress-bar vm_progress"></div>
                    <div class="vm_progress-percent">{if !$resources.memory.max_mb || $resources.memory.max_mb==0}{$resources.memory.used_mb}{else}{math equation="(x / y)*100" x=$resources.memory.used y=$resources.memory.max format="%.2f"}%{/if}</div>
                </div>
                <div style="clear:both;"></div>
            </td>
        </tr>
        <tr>
            <td>{$lang.cpus}</td>
            <td>
                <div class="vm_progress-total">{$resources.cpus.used} {$lang.of} {if !$resources.cpus.max}{$lang.unlimited}{else}<strong>{$resources.cpus.max}</strong> {$lang.used} / {math equation="(y -x )" x=$resources.cpus.used y=$resources.cpus.max} {$lang.free}{/if}</div>
                <div class="vm_progress-wrap vm_progress" data-progress-percent="{math equation="(x / y)*100" x=$resources.cpus.used y=$resources.cpus.max format="%.2f"}">
                    <div class="vm_progress-bar vm_progress"></div>
                    <div class="vm_progress-percent">{if !$resources.cpus.max}{$resources.cpus.used}{else}{math equation="(x / y)*100" x=$resources.cpus.used y=$resources.cpus.max format="%.2f"}%{/if}</div>
                </div>
                <div style="clear:both;"></div>
            </td>
        </tr>
        <tr>
            <td>{$lang.shares}</td>
            <td>
                <div class="vm_progress-total">{$resources.cpu_shares.used} {$lang.of} {if !$resources.cpu_shares.max}{$lang.unlimited}{else}<strong>{$resources.cpu_shares.max}</strong> {$lang.used} / {math equation="(y -x )" x=$resources.cpu_shares.used y=$resources.cpu_shares.max} {$lang.free}{/if}</div>
                <div class="vm_progress-wrap vm_progress" data-progress-percent="{math equation="(x / y)*100" x=$resources.cpu_shares.used y=$resources.cpu_shares.max format="%.2f"}">
                    <div class="vm_progress-bar vm_progress"></div>
                    <div class="vm_progress-percent">{if !$resources.cpu_shares.max}{$resources.cpu_shares.used}{else}{math equation="(x / y)*100" x=$resources.cpu_shares.used y=$resources.cpu_shares.max format="%.2f"}%{/if}</div>
                </div>
                <div style="clear:both;"></div>
            </td>
        </tr>
        <tr>
            <td>{$lang.disk_size}</td>
            <td>
                <div class="vm_progress-total">{$resources.disk_size.used_gb} {$lang.of} {if !$resources.disk_size.max_gb || $resources.disk_size.max_gb==0}{$lang.unlimited}{else} <strong>{$resources.disk_size.max_gb}</strong> {$lang.used} / {$resources.disk_size.free} {$lang.free}{/if}</div>
                <div class="vm_progress-wrap vm_progress" data-progress-percent="{math equation="(x / y)*100" x=$resources.disk_size.used y=$resources.disk_size.max format="%.2f"}">
                    <div class="vm_progress-bar vm_progress"></div>
                    <div class="vm_progress-percent">{if !$resources.disk_size.max_gb || $resources.disk_size.max_gb==0}{$resources.disk_size.used_gb}{else}{math equation="(x / y)*100" x=$resources.disk_size.used y=$resources.disk_size.max format="%.2f"}%{/if}</div>
                </div>
                <div style="clear:both;"></div>
            </td>
        </tr>
        <tr>
            <td>{$lang.ip_addresses}</td>
            <td>
                <div class="vm_progress-total">{$resources.ips.used} {$lang.of} {if !$resources.ips.max}{$lang.unlimited}{else}<strong>{$resources.ips.max}</strong> {$lang.used} / {math equation="(y -x )" x=$resources.ips.used y=$resources.ips.max} {$lang.free}{/if}</div>
                <div class="vm_progress-wrap vm_progress" data-progress-percent="{math equation="(x / y)*100" x=$resources.ips.used y=$resources.ips.max format="%.2f"}">
                    <div class="vm_progress-bar vm_progress"></div>
                    <div class='vm_progress-percent'>{if !$resources.ips.max}{$resources.ips.used}{else}{math equation="(x / y)*100" x=$resources.ips.used y=$resources.ips.max format="%.2f"}%{/if}</div>
                </div>
                <div style="clear:both;"></div>
            </td>
        </tr>
        <tr>
            <td>{$lang.rate_limit}</td>
            <td>
                <div class="vm_progress-total">{$resources.rate_limit.used} {$lang.of} {if !$resources.rate_limit.max}{$lang.unlimited}{else}<strong>{$resources.rate_limit.max}</strong> {$lang.used} / {math equation="(y -x )" x=$resources.rate_limit.used y=$resources.rate_limit.max} {$lang.free}{/if}</div>
                <div class="vm_progress-wrap vm_progress" data-progress-percent="{math equation="(x / y)*100" x=$resources.rate_limit.used y=$resources.rate_limit.max format="%.2f"}">
                    <div class="vm_progress-bar vm_progress"></div>
                    <div class='vm_progress-percent'>{if !$resources.rate_limit.max}{$resources.rate_limit.used}{else}{math equation="(x / y)*100" x=$resources.rate_limit.used y=$resources.rate_limit.max format="%.2f"}%{/if}</div>
                </div>
                <div style="clear:both;"></div>
            </td>
        </tr>
        <tr>
            <td>{$lang.backups}</td>
            <td>
                <div class="vm_progress-total">{$resources.backups.used} {$lang.of} {if !$resources.backups.max}{$lang.unlimited}{else} <strong>{$resources.backups.max}</strong> {$lang.used} / {math equation="(y -x )" x=$resources.backups.used y=$resources.backups.max} {$lang.free}{/if}</div>
                <div class="vm_progress-wrap vm_progress" data-progress-percent="{math equation="(x / y)*100" x=$resources.backups.used y=$resources.backups.max format="%.2f"}">
                    <div class="vm_progress-bar vm_progress"></div>
                    <div class='vm_progress-percent'>{if !$resources.backups.max}{$resources.backups.used}{else}{math equation="(x / y)*100" x=$resources.backups.used y=$resources.backups.max format="%.2f"}%{/if}</div>
                </div>
                <div style="clear:both;"></div>
            </td>
        </tr>
    </table>  
    {if $billing_resources}
        <br />
        <h3 class="header_label">{$mg_lang.usage_records} {if $records_range.start_date}<span style="float: right">{$mg_lang.period} {$records_range.start_date|date_format:"%d/%m/%y"} - {$records_range.end_date|date_format:"%d/%m/%y"}{/if}</h3>
        <table class="table table-bordered">
            <thead>
                <tr class="title">
                    <th>{$mg_lang.record}</th>
                    <th>{$mg_lang.usage}</th>
                    <th>{$mg_lang.total}</th>
                </tr>
            </thead>
            <tbody>
                {foreach from=$billing_resources item=r}
                    <tr>
                        <td>{$r.FriendlyName} {if $r.name} - {$r.name} {/if}</td>
                        <td>{$r.usage}{$r.unit}</td>
                        <td>{$r.total}</td>
                    </tr>
                {/foreach}
            </tbody>
            <tfoot>
                <tr>
                    <td colspan="3"><a href="clientarea.php?action=productdetails&id={$id}&modop=custom&a=management&page=usage_records" class="btn btn-success">{$lang.more_usage_records}</a></td>
                </tr>
            </tfoot>
        </table>
    {/if}
</div>

<script type="text/javascript">
    {literal}
        jQuery(document).ready(function() {
            // prograss bars
            function moveProgressBar() {
                jQuery(".vm_progress-wrap").each(function() {
                    var getPercent = (jQuery(this).data("progress-percent") / 100);
                    var getProgressWrapWidth = jQuery(this).width();
                    var progressTotal = getPercent * getProgressWrapWidth;
                    jQuery(this).find(".vm_progress-bar").css('width', progressTotal);
                });
            }
            moveProgressBar();
            jQuery(window).resize(function() {
                moveProgressBar();
            });
            
            jQuery(document).ajaxStart(function() {
                    jQuery("#vm_alerts").html("<div style=\"margin:20px auto;\">{/literal}{$lang.please_wait}{literal}<br/><img src=\"modules/servers/onappCloud/img/loadingsml.gif\"></div>");
                }).ajaxStop(function(){
                    jQuery("#vm_alerts").html("");
                });
            jQuery(".vm_delete").click(function() {
                var elem = jQuery(this);
                if (confirm("{/literal}{$lang.confirm}{literal}")) {
                    var vserverid = jQuery(this).attr("rel");
                    
                    jQuery.post(window.location, {ajax: 1, doAction: "vm_terminate", vserverid: vserverid}, function(res) {
                        if (res.success == 1) {
                            jQuery(elem).closest('tr').replaceWith('<tr id="ajax_message" class="success"><td colspan="3">{/literal}{$lang.vm_deleted}{literal}</td></tr>');
                        } else {
                            if(res.msg!=null)
                                jQuery(elem).closest('tr').after('<tr id="ajax_message" class="error"><td colspan="3">'+res.msg+'</td></tr>');
                            else
                                jQuery(elem).closest('tr').after('<tr id="ajax_message" class="error"><td colspan="3">{/literal}{$lang.vm_deleted_error}{literal}</td></tr>');
                        }
                    }, "json");
                    setTimeout(function() {
                        jQuery("#ajax_message").remove();
                    }, 10000);
                }
                return false;
            });
        });
    {/literal}
</script>