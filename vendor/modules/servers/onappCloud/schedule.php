<?php


/**
 * @author Maciej Husak <maciej@modulesgarden.com>
 */
if($vars['disallow_action']['backups']==1){
    ob_clean();
    header ("Location: clientarea.php?action=productdetails&id=".$params['serviceid']."&modop=custom&a=management&page=vmdetails&vserver=".$_GET['vserver']);
    die();
}

if(isset($_POST['disk_id']) && $_POST['disk_id']>0){
    $disk = new NewOnApp_Disk();
    $disk -> setconnection($params,true);
    $disk -> setID($_POST['disk_id']);          
    
    if(isset($_POST['doAction'])){
        switch($_POST['doAction']){
             case 'save':
                if(isset($_POST['schedule_id'])){
                    $disk->editSchedule($_POST['schedule_id'],array(
                       'schedule'   => array(
                           'duration'   => (int)$_POST['edit']['duration'],
                           'period'     => (string)$_POST['edit']['period'],
                           'status'     => ($_POST['edit']['status']!='enabled' ? 'disabled' : 'enabled')
                       )
                    ));
                    if($disk->isSuccess()){
                        $vars['msg_success']  = $vars['lang']['schedule_edited'];
                        break;
                    } else 
                        $vars['msg_error']    = $disk->error();
                } else $vars['msg_error']     = $vars['lang']['invalid_schedule_id'];
            case 'edit':
                $vars['step'] = 'edit';
                if(isset($_POST['schedule_id'])){
                    $details = $disk->getScheduleDetails($_POST['schedule_id']);
                    if($disk->isSuccess()){
                        $vars['data']      = $details;
                    } else 
                        $vars['msg_error']    = $disk->error();
                } else $vars['msg_error']     = $vars['lang']['invalid_schedule_id'];
                break;
                
            case 'new':
                $vars['step'] = 'add'; 
                break;
            
            case 'delete':
                if(isset($_POST['schedule_id'])){
                    $disk->deleteSchedule($_POST['schedule_id']);
                    if($disk->isSuccess()){
                        $vars['msg_success']  = $vars['lang']['schedule_deleted'];
                    } else 
                        $vars['msg_error']    = $disk->error();
                } else $vars['msg_error']     = $vars['lang']['invalid_schedule_id'];
                break;
                
            case 'add':
                if(isset($_POST['add']['duration'])){
                    $disk->addSchedule(array(
                       'schedule'   => array(
                           'action'     => 'autobackup',
                           'duration'   => (int)$_POST['add']['duration'],
                           'period'     => (string)$_POST['add']['period']
                       )
                    ));
                    if($disk->isSuccess()){
                        $vars['msg_success']  = $vars['lang']['schedule_added'];
                    } else 
                        $vars['msg_error']    = $disk->error();
                }
                break;    
        }
    }
    
    $details = $disk->getScheduleList();
    if($disk->isSuccess()){
        $vars['disk_id']   = (int)$_POST['disk_id'];
        $vars['schedules'] = $details;
    } else $vars['msg_error'] = $disk->error();
    
} else
    $vars['msg_error']   =  $vars['lang']['invalid_disk_id'];

?>