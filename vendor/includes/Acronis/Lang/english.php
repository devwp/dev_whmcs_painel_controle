<?php
//API MANAGERS

$_LANG['curl_not_installed_error'] = 'The PHP cURL is not installed.';

//Accounts:
$_LANG['cant_get_hostname_for_user'] = 'Cannot get the host name for the user.';
$_LANG['cant_log_into_system'] = 'Cannot log in to the system.';
$_LANG['cant_get_server_admin_id_or_his_group_id'] = 'Cannot get ID of the server administrator.';
$_LANG['cant_send_activation_email'] = 'Cannot send the activation message.';
$_LANG['cant_get_mail_details'] = 'Cannot get the email details.';
$_LANG['cant_get_access_token'] = 'Cannot get the email access token.';
$_LANG['cant_activate_account'] = 'Cannot activate the account.';

//Admins:
$_LANG['cant_get_admin_details'] = 'Cannot get the administrator account details.';
$_LANG['cant_find_in_admin_details'] = 'Cannot find parameter "??" in the administrator account details.';
$_LANG['cant_send_admin_password_reset'] = 'Cannot send the password reset message.';
$_LANG['cant_determine_access_type'] = 'Cannot determine access type of "??".';
$_LANG['admin_credentials_cant_be_empty'] = 'The administrator email and login cannot be empty.';
$_LANG['cant_add_group_admin'] = 'Cannot add the group administrator account.';
$_LANG['cant_get_id_of_created_admin'] = 'Cannot create the administrator account because it is impossible to get its ID or version.';

//Groups:
$_LANG['cant_get_subgroups_for_group_id'] = 'Cannot get the subgroups of the group with ID ??.';
$_LANG['cant_find_storage_id_of_subgroup'] = 'Cannot find the storage ID of the subgroup.';
$_LANG['cant_get_group_details'] = 'Cannot get the group details.';
$_LANG['cant_find_group_name_in_parent_group'] = 'Cannot find subgroup "??" in the group.';
$_LANG['group_with_kind_not_found'] = 'The group of type "??" is not found.';
$_LANG['cant_find_in_group'] = 'Cannot find "??" in the group.';
$_LANG['cant_find_final_group_kind'] = 'Cannot find the Final group type.';
$_LANG['cant_find_reseller_creation_group_kind'] = 'Cannot find the best matching group type where reseller creation is allowed for the reseller.';
$_LANG['cant_find_no_reseller_creation_group_kind'] = 'Cannot find the best matching group type where reseller creation is prohibited for the reseller.';
$_LANG['allow_no_allow_reseller_creation_conflict'] = 'Allow/prohibit creation of resellers conflict - Cannot determine the best group kinds/group grades.';
$_LANG['cant_create_no_reseller_creation_group'] = 'You cannot create reseller subgroup without reseller creation ability for this group.';
$_LANG['cant_determine_production_pricing_mode'] = 'Cannot determine the best production pricing mode.';
$_LANG['cant_add_group'] = 'Cannot add the group to the system.';
$_LANG['cant_get_id_of_created_group'] = 'Cannot create the group because it is impossible to get its ID or version.';
$_LANG['cant_delete_group'] = 'Cannot delete the group.';
$_LANG['cant_update_group'] = 'Cannot update the group.';
$_LANG['cant_get_version_of_updated_group'] = 'Cannot update the group because it is impossible to get its version.';
$_LANG['cant_lock_group'] = 'Cannot lock the group.';
$_LANG['cant_unlock_group'] = 'Cannot unlock the group.';
$_LANG['cant_get_group_users_details'] = 'Cannot get the information about the users in group "??".';
$_LANG['cant_get_group_admins_details'] = 'Cannot get the information about the administrators in group "??".';
$_LANG['cant_find_admin_login_in_group'] = 'Cannot find the administrator with login "??" in the group.';
$_LANG['cant_find_admin_group_id'] = 'Cannot find the group ID of the administator "??".';
$_LANG['cant_find_group_name_id'] = 'Cannot find the ID of group "??".';
$_LANG['cant_get_group_bc_link'] = 'Cannot get the backup console link for the group with ID ??.';

//Logic constants:
$_LANG['cant_get_logic_constants_db_unknown_host'] = 'Cannot get logic constants from the database because the host name is unknown.';
$_LANG['cant_save_logic_constants_db_unknown_host'] = 'Cannot save logic constants to the database because the host name is unknown.';
$_LANG['cant_get_logic_constants'] = 'Cannot get logic constants.';
$_LANG['group_kind_not_found'] = 'Group type "??" is not found.';
$_LANG['cant_get_group_kinds'] = 'Cannot get group types.';
$_LANG['cant_get_pricing_modes'] = 'Cannot get pricing modes.';
$_LANG['cant_get_access_types'] = 'Cannot get access types.';
$_LANG['cant_get_statuses_constant_data'] = 'Cannot get the information about minimum statuses.';

$_LANG['cant_get_version_of_updated_group'] = 'Cannot update the user account because it is impossible to get its version.';
//Specific api errors (further explanations):
$_LANG['couldnt_connect'] = 'Cannot resolve the host.';
$_LANG['group_with_such_properties_exist'] = 'The group with such properties already exists.';
$_LANG['admin_with_such_email_exist'] = 'The user with this login already exists.';

//Measures
$_LANG['measure']['B'] = 'B';
$_LANG['measure']['KiB'] = 'KiB';
$_LANG['measure']['MiB'] = 'MiB';
$_LANG['measure']['GiB'] = 'GiB';
$_LANG['measure']['TiB'] = 'TiB';
$_LANG['measure']['PiB'] = 'PiB';
$_LANG['measure']['EiB'] = 'EiB';
$_LANG['measure']['YiB'] = 'YiB';

//OTHERS
//AcronisViews:
$_LANG['unlimited'] = 'Unlimited.';
$_LANG['wrong_data'] = 'Invalid data.';

//Global:
$_LANG['please_check_config'] = 'Please check your configuration.';

//ProvisionParams:
$_LANG['desired_config_option_not_found'] = 'Configurable option is not found:';
$_LANG['cant_get_from_custom_fields'] = 'Cannot get "??" from custom fields.';
$_LANG['custom_field_has_wrong_value'] = 'The value of custom field "??" is incorrect.';

//Warnings:
$_LANG['overusage']['server_count'] = 'The server quota is exceeded.';
$_LANG['overusage']['storage_size'] = 'The cloud storage quota is exceeded.';
$_LANG['overusage']['vm_count'] = 'The virtual machine quota is  exceeded.';
$_LANG['overusage']['workstation_count'] = 'The workstation quota exceeded.';
$_LANG['overusage']['mailbox_count'] = 'The quota for Office 365 mailboxes is exceeded.';
$_LANG['overusage']['mobile_count'] = 'The quota for mobile devices is exceeded.';

$_LANG['nearlylimit']['server_count'] = 'More than 90%% of the server quota is used.';
$_LANG['nearlylimit']['storage_size'] = 'More than 90%% of the cloud storage is used.';
$_LANG['nearlylimit']['vm_count'] = 'More than 90%% of the virtual machine quota is used.';
$_LANG['nearlylimit']['workstation_count'] = 'More than 90%% of the workstation quota is used.';
$_LANG['nearlylimit']['mailbox_count'] = 'More than 90%% of the quota for Office 365 mailboxes is used.';
$_LANG['nearlylimit']['mobile_count'] = 'More than 90%% of the quota for mobile devices is used.';
