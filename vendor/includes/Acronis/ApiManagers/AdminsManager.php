<?php
/**
 *
 * @Copyright © 2002-2016 Acronis International GmbH. All rights reserved
 * DEPENDENCIES: \Acronis\Api\Requests, \Acronis\ApiManagers\SystemManager, \Acronis\Helpers\Arr, \Acronis\WHMCS\Lang
 */

namespace Acronis\ApiManagers;

use Acronis\Api\Requests;
use Acronis\Helpers\Arr;
use Acronis\WHMCS\Lang;

class AdminsManager
{
    const DEFAULT_NOTIFICATIONS_BACKUP = 3;
    const DEFAULT_NOTIFICATIONS_MANAGEMENT = 1;
    const DEFAULT_ACCESS_TYPE = 15;
    /**
     * @var Requests $api
     */
    private $api = FALSE;
    /**
     * @var SystemManager $system
     */
    private $system = FALSE;
    /**
     * @var Lang $lang
     */
    private $lang = FALSE;
    /**
     * container for administrator data
     * @var
     */
    private $_adminData = array();

    public function __construct(SystemManager $system_manager)
    {
        $this->system = $system_manager;
        $this->api = $this->system->api;
        $this->lang = $this->system->lang;
    }

    /**
     * Sends mail reset or activation request (depends on admin status)
     * @param string $admin_email - admin email
     * @param string $admin_login - admin login
     * @param mixed $admin_details_or_group_id - admin details or group id if details are not supported
     * @return array Array with admin details
     */
    public function adminSendMailReset($admin_email, $admin_login, $admin_details_or_group_id)
    {
        $admin_details = $admin_details_or_group_id;

        //get admin details:
        if (!is_array($admin_details_or_group_id) && is_numeric($admin_details_or_group_id)) {
            list ($admin_details, $admins_details_trash)
                = $this->system->groupsManager->findAdminDetailsInGroupAdminsDetailsByLogin($admin_details_or_group_id, $admin_login);
        }

        //get statuses values:
        list($enabled, $disabled_trash, $inactive_trash) = $this->system->logicConstantsManager->getStatuses();

        //get admin status:
        list($admin_status, $admin_details) = $this->getAdminParamForced('status', $admin_details);

        //if admin is not active then send activation e-mail:
        if ($admin_status != $enabled) {
            $this->system->accountsManager->sendMailActivation($admin_email, $admin_login);
        } else { //otherwise send reset e-mail
            $admin_activate_data = array
            (
                'email' => $admin_email,
                'login' => $admin_login
            );

            $this->api->sendMailReset($admin_activate_data);
            $this->system->testFatalRequestError($this->lang->get('cant_send_admin_password_reset') . ' [$ecode]');
        }

        return $admin_details;
    }

    /**
     * Gets parameter from admin details. Throws an error if the parameter is not found
     * @param string $param_path - path to parameter
     * @param mixed $admin_details_or_admin_id - admin details or admin id if details are not supported
     * @return array Array with two values: ($value,$admin_details)
     */
    public function getAdminParamForced($param_path, $admin_details_or_admin_id)
    {
        return $this->getAdminParam($param_path, $admin_details_or_admin_id, true);
    }

    /**
     * Gets parameter from admin details
     * @param string $param_path - path to parameter
     * @param mixed $admin_details_or_admin_id - admin details or admin id if details are not supported
     * @param bool $forced - if true it will throw an error if the parameter is not found
     * @return array Array with two values: ($value,$admin_details)
     */
    public function getAdminParam($param_path, $admin_details_or_admin_id, $forced = false)
    {
        $admin_details = $admin_details_or_admin_id;

        if (!is_array($admin_details_or_admin_id) && is_numeric($admin_details_or_admin_id)) {
            $admin_details = $this->getAdminDetails($admin_details_or_admin_id);
        }

        $nullobject = new \Exception('null');
        $value = Arr::getByPath($admin_details, $param_path, $nullobject);
        if ($value === $nullobject) {
            if (!$forced) {
                $value = null;
            } else {
                $this->api->makeOwnError($this->lang->get('cant_find_in_admin_details', array($param_path)));
            }
        }

        return array($value, $admin_details);
    }

    /**
     * Gets admin details
     * @param int $admin_id - admin id
     * @return array Array with desired admin details
     */
    public function getAdminDetails($admin_id)
    {
        $admin_details = array();

        if (isset($this->system->admins[$admin_id])) {
            $admin_details = $this->system->admins[$admin_id];
        } else {
            $response = $this->api->getAdmin($admin_id);
            $this->system->testFatalRequestError($this->lang->get('cant_get_admin_details') . ' [$ecode]');

            if (empty($response)) {
                $this->api->makeOwnError($this->lang->get('cant_get_admin_details'));
            }

            $this->system->admins[$admin_id] = $admin_details = $response;
        }

        return $admin_details;
    }

    public function fillAdminData($userName, $firstName, $lastName, $email)
    {
        $this->_adminData = array(
            "email" => $email,
            "firstname" => $firstName,
            "lastname" => $lastName,
            "login" => $userName,
            "notifications_backup" => self::DEFAULT_NOTIFICATIONS_BACKUP,
            "notifications_management" => self::DEFAULT_NOTIFICATIONS_MANAGEMENT,
            "access_type" => self::DEFAULT_ACCESS_TYPE
        );
    }

    /**
     * Creates admin and activates its account
     * @param int $parent_group_id - parrent group id
     * @param string $password - admin password
     * @return array Array with two values: ($new_admin_id,$new_admin_version)
     * @internal param array $admin_data - admin data (See Api documentation)
     */
    public function createAdmin($parent_group_id, $password)
    {
        if (!isset($this->_adminData['email']) || !isset($this->_adminData['login'])) {
            $this->api->makeOwnError($this->lang->get('admin_credentials_cant_be_empty'), true, true);
        }

        $response = $this->api->addGroupAdmin($this->_adminData, $parent_group_id);
        $this->system->testFatalRequestError($this->lang->get('cant_add_group_admin') . ' [$ecode]');

        if (!isset($response['id']) || !isset($response['version'])) {
            $this->api->makeOwnError($this->lang->get('cant_get_id_of_created_admin'), true);
        }

        $new_admin_id = $response['id'];
        $new_admin_version = $response['version'];

        $this->system->accountsManager->activateAccount($this->_adminData['login'], $password, $this->_adminData['email'], false);

        return array($new_admin_id, $new_admin_version);
    }
}