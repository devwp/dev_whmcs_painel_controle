<?php
/**
 *
 * @Copyright © 2002-2016 Acronis International GmbH. All rights reserved
 * DEPENDENCIES: \Acronis\Api\Requests, \Acronis\ApiManagers\SystemManager, \Acronis\WHMCS\Lang
 */

namespace Acronis\ApiManagers;

use Acronis\Api\Requests;
use Acronis\WHMCS\Lang;

class AccountsManager
{
    /**
     * @var Requests $api
     */
    private $api = FALSE;
    /**
     * @var SystemManager $system
     */
    private $system = FALSE;
    /**
     * @var Lang $lang
     */
    private $lang = FALSE;


    public function __construct(SystemManager $system_manager)
    {
        $this->system = $system_manager;
        $this->api = $this->system->api;
        $this->lang = $this->system->lang;
    }

    /**
     * Login to system - does all needed requests to properly log into the system
     *
     * @param string $username
     * @param string $password
     * @return array Array of two values: ($admin_id,$admin_group_id)
     */
    public function login($username = null, $password = null)
    {
        if ($username == null || $password == null) {
            $serversData = \Acronis\WHMCS\Servers::getServerData();
            $servers = $serversData[0];
            $decrypted = localAPI("decryptpassword", array("password2" => $servers['password']));
            $password = $decrypted['password'];
            $username = $servers['username'];
        }
        $this->getBaseUrl($username);

        $login_data = array
        (
            'password' => $password,
            'username' => $username,
        );

        $login_result_data = $this->api->login($login_data);
        $this->system->testFatalRequestError($this->lang->get('cant_log_into_system') . ' [$ecode]');

        $this->system->groupsManager->updateInternalTag();

        if (!isset($login_result_data['id']) || !isset($login_result_data['group']) || !isset($login_result_data['group']['id'])) {
            $this->api->makeOwnError($this->lang->get('cant_get_server_admin_id_or_his_group_id'), true);
        }

        $this->system->serverAdminId = $admin_id = $login_result_data['id'];
        $this->system->serverAdminGroupId = $admin_group_id = $login_result_data['group']['id'];

        return array($admin_id, $admin_group_id);
    }

    /**
     * Gets url that should be used by Api while doing requests
     *
     * @param string $username - username that is logging in (this is admin username in most cases)
     * @return string Base url, that will be used to do rest of requests
     */
    public function getBaseUrl($username)
    {
        $base_url = '';

        if (trim($this->system->baseUrl) != '') {
            $base_url = $this->system->baseUrl;
        } else {
            $account_base_url_result = $this->api->accountsRouting($username);
            $this->system->testFatalRequestError($this->lang->get('cant_get_hostname_for_user') . ' [$ecode]');

            if (isset($account_base_url_result['error_code'])) {
                $this->api->makeOwnError($this->lang->get('cant_get_hostname_for_user') . ' [' . $account_base_url_result['error_code'] . ']', true);
            } elseif (!isset($account_base_url_result['server_url'])) {
                $this->api->makeOwnError($this->lang->get('cant_get_hostname_for_user'), true);
            }
            $this->system->baseUrl = $base_url = $account_base_url_result['server_url'];
            $this->api->baseUrl = $base_url;

        }

        return $base_url;
    }

    /**
     * Sends mail activation request
     * @param string $email - email
     * @param string $login - login
     * @return boolean
     */
    public function sendMailActivation($email, $login)
    {
        $admin_activate_data = array
        (
            'email' => $email,
            'login' => $login
        );

        $this->api->sendMailActivate($admin_activate_data);
        $this->system->testFatalRequestError($this->lang->get('cant_send_activation_email') . ' [$ecode]');

        return true;
    }

    /**
     * Activates account (automatically collects token before)
     * @param string $login - account login
     * @param string $password - account password
     * @param mixed $mail_details_or_email - account mail details or e-mail address
     * @param bool $md5_hashed - if true, password will be hashed with md5
     * @return boolean TRUE
     */
    public function activateAccount($login, $password, $mail_details_or_email, $md5_hashed)
    {
        $mail_details = $mail_details_or_email;

        if (is_string($mail_details_or_email)) {
            $mail_details = $this->getMailDetails($mail_details_or_email, $login);
        }

        if (empty($mail_details) || !isset($mail_details['token'])) {
            $this->api->makeOwnError($this->lang->get('cant_get_access_token'), false, true);
        }

        $token = $mail_details['token'];

        $data = array(
            "md5_hashed" => ($md5_hashed ? 1 : 0),
            "password" => ($md5_hashed ? md5($password) : $password)
        );

        $this->api->activateAccount($data, $token);
        $this->system->testFatalRequestError($this->lang->get('cant_activate_account') . ' [$ecode]');

        return true;
    }

    /**
     * Gets mail details
     * @param string $email - email
     * @param string $login - login
     * @return array Array with mail details
     */
    public function getMailDetails($email, $login)
    {
        $mail_details = array();
        if (isset($this->system->mailDetails[$email . '###' . $login])) {
            $mail_details = $this->system->mailDetails[$email . '###' . $login];
        } else {
            $result = $this->api->getMailDetails($login, $email);
            $this->system->testFatalRequestError($this->lang->get('cant_get_mail_details') . ' [$ecode]');

            if (empty($result)) {
                $this->api->makeOwnError($this->lang->get('cant_get_mail_details'));
            }

            $this->system->mailDetails[$email . '###' . $login] = $mail_details = $result;
        }
        return $mail_details;
    }

    public function getStorages($groupId)
    {
        return $this->api->getStoragesGroup($groupId);
    }

    /**
     * Updates account (changes password; automatically collects token before)
     * @param string $login - account login
     * @param string $password - account password
     * @param mixed $mail_details_or_email - account mail details or e-mail address
     * @param bool $md5_hashed - if true, password will be hashed with md5
     * @return boolean TRUE
     */
    public function updateAccount($login, $password, $mail_details_or_email, $md5_hashed)
    {
        $mail_details = $mail_details_or_email;

        if (is_string($mail_details_or_email)) {
            $mail_details = $this->getMailDetails($mail_details_or_email, $login);
        }

        if (empty($mail_details) || !isset($mail_details['token'])) {
            $this->api->makeOwnError($this->lang->get('cant_get_access_token'), false, true);
        }

        $token = $mail_details['token'];

        $data = array(
            "md5_hashed" => ($md5_hashed ? 1 : 0),
            "password" => ($md5_hashed ? md5($password) : $password)
        );

        $this->api->updateAccount($data, $token);
        $this->system->testFatalRequestError($this->lang->get('cant_activate_account') . ' [$ecode]');

        return true;
    }

}