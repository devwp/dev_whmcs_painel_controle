<?php
/**
 *
 * @Copyright © 2002-2016 Acronis International GmbH. All rights reserved
 * DEPENDENCIES: \Acronis\Api\Requests, \Acronis\ApiManagers\SystemManager, \Acronis\Helpers\ModuleVars, \Acronis\WHMCS\Lang
 */

namespace Acronis\ApiManagers;

use Acronis\Api\Requests;
use Acronis\Helpers\ModuleVars;
use Acronis\WHMCS\Lang;

class LogicConstantsManager
{
    /**
     * @var Requests $api
     */
    private $api = FALSE;
    /**
     * @var SystemManager $system
     */
    private $system = FALSE;
    /**
     * @var ModuleVars $moduleVars
     */
    private $moduleVars = FALSE;
    /**
     * @var Lang $lang
     */
    private $lang = FALSE;

    public function __construct(SystemManager $system_manager)
    {
        $this->system = $system_manager;
        $this->moduleVars = $this->system->moduleVars;
        $this->api = $this->system->api;
        $this->lang = $this->system->lang;
    }

    /**
     * Checks if desired group kinds are in system logic constants and throws an error if not and they are forced.
     * Returns found group kinds
     * @param array $needed_kinds - array in format: array(group_kind_1=>$forced); $forced is bool - true to throw an error if not found this kind
     * @return array
     */
    public function getNeededGroupKinds($needed_kinds)
    {
        $group_kinds = array();

        $logic_constants = $this->getLogicConstants();

        if (isset($logic_constants["GroupsKind"])) {
            $kinds = $logic_constants["GroupsKind"];
            foreach ($needed_kinds as $kindname => $forced) {
                if (isset($kinds[$kindname])) {
                    $group_kinds[$kindname] = $kinds[$kindname];
                } elseif ($forced) { //(&&!isset($kinds[$kindname]))
                    $this->api->makeOwnError($this->lang->get('group_kind_not_found', array($kindname)), false, true);
                }
            }
        } else {
            $this->api->makeOwnError($this->lang->get('cant_get_group_kinds'), false, true);
        }
        return $group_kinds;
    }

    /**
     * Gets logic constants from first available resource in order: object cache->database->request to server
     * If constants has been gathered from request, they will be saved to database
     * @return array Array with logic constants
     */
    public function getLogicConstants()
    {
        $logic_constants = array();

        if ($this->system->logicConstants !== FALSE) {
            $logic_constants = $this->system->logicConstants;
        } else {
            list($logic_constants_exists, $result) = $this->getLogicConstantsFromDB();

            if (!$logic_constants_exists) {
                $result = $this->api->getLogicConstants();
                $this->system->testFatalRequestError($this->lang->get('cant_get_logic_constants') . ' [$ecode]');
            }

            if (empty($result)) {
                $this->api->makeOwnError($this->lang->get('cant_get_logic_constants'));
            } else {
                $this->system->logicConstants = $logic_constants = $result;
            }

            if (!$logic_constants_exists) {
                $this->saveLogicConstantsToDB($logic_constants);
            }

        }

        return $logic_constants;
    }

    /**
     * Gets logic constants from database. baseUrl have to be properly set to make it working
     * @return array Array of two values: ($logic_constants_exists,$logic_constants_db)
     */
    public function getLogicConstantsFromDB()
    {
        $logic_constants_exists = false;
        $logic_constants_db = array();

        if (trim($this->system->baseUrl) == '') {
            $this->api->makeOwnError($this->lang->get('cant_get_logic_constants_db_unknown_host'), true, true);
        }

        $db_constants_json = $this->moduleVars->get($this->system->baseUrl . '#constants', '');

        try {
            $logic_constants_db = json_decode($db_constants_json, true);
        } catch (\Exception $e) {
            $logic_constants_db = array();
        }

        if (!empty($logic_constants_db)) {
            $logic_constants_exists = true;
        }

        return array($logic_constants_exists, $logic_constants_db);
    }

    /**
     * Saves logic constants to database. baseUrl have to be properly set to make it working
     * @param array $logic_constants - logic constants array
     * @return boolean TRUE
     */
    public function saveLogicConstantsToDB($logic_constants)
    {
        $db_constants_json = '';

        if (trim($this->system->baseUrl) == '') {
            $this->api->makeOwnError($this->lang->get('cant_save_logic_constants_db_unknown_host'), true, true);
        }

        if (is_array($logic_constants) && !empty($logic_constants)) {
            $db_constants_json = json_encode($logic_constants);
            $this->moduleVars->set($this->system->baseUrl . '#constants', $db_constants_json);
        }

        return true;
    }

    /**
     * Gets pricing modes from system logic constants
     * @return array Array of possible system pricing modes
     */
    public function getPricingModes()
    {
        $modes = array();
        $logic_constants = $this->getLogicConstants();

        if (isset($logic_constants["Pricing"]) && isset($logic_constants["Pricing"]["Status"])) {
            $modes = $logic_constants["Pricing"]["Status"];
        } else {
            $this->api->makeOwnError($this->lang->get('cant_get_pricing_modes'), false, true);
        }
        return $modes;
    }

    /**
     * Gets access types from system logic constants
     * @return array Array of possible system access types
     */
    public function getAccessTypes()
    {
        $types = array();

        $logic_constants = $this->getLogicConstants();

        if (isset($logic_constants["AccessTypes"])) {
            $types = $logic_constants["AccessTypes"];
        } else {
            $this->api->makeOwnError($this->lang->get('cant_get_access_types'), false, true);
        }
        return $types;
    }

    /**
     * Gets statuses ids from system logic constants
     * @return array Array of three values: ($enabled,$disabled,$inactive)
     */
    public function getStatuses()
    {
        $disabled = -10;
        $enabled = -10;
        $inactive = -10;

        $logic_constants = $this->getLogicConstants();

        if (isset($logic_constants["Status"])) {
            $consts = $logic_constants["Status"];

            $disabled = isset($consts["Disabled"]) ? $consts["Disabled"] : -10;
            $enabled = isset($consts["Enabled"]) ? $consts["Enabled"] : -10;
            $inactive = isset($consts["Inactive"]) ? $consts["Inactive"] : -10;
        } else {
            $this->api->makeOwnError($this->lang->get('cant_get_statuses_constant_data'), false, true);
        }

        if ($enabled == -10 || $disabled == -10) {
            $this->api->makeOwnError($this->lang->get('cant_get_statuses_constant_data'), false, true);
        }
        return array($enabled, $disabled, $inactive);
    }


}