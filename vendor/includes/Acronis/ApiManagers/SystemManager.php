<?php
/**
 *
 * @Copyright © 2002-2016 Acronis International GmbH. All rights reserved
 * DEPENDENCIES:
 * \Acronis\Api\Requests as Api;
 * \Acronis\ApiManagers\AccountsManager;
 * \Acronis\ApiManagers\AdminsManager;
 * \Acronis\ApiManagers\GroupsManager;
 * \Acronis\ApiManagers\LogicConstantsManager;
 * \Acronis\Helpers\ModuleVars;
 * \Acronis\Helpers\Arr;
 * \Acronis\System\ModuleConfig;
 * \Acronis\WHMCS\Lang;
 */

namespace Acronis\ApiManagers;

use Acronis\Api\Requests as Api;
use Acronis\Helpers\ModuleVars;
use Acronis\Standard\Configs\ModuleConfig;
use Acronis\UserException;
use Acronis\WHMCS\Lang;

class SystemManager
{

    /**
     * @var Api $api
     */
    public $api = FALSE;    //object of class with Api request
    /**
     * @var ModuleVars $moduleVars
     */
    public $moduleVars = FALSE;
    public $baseUrl = '';  // cache knowledgebase
    public $logicConstants = FALSE;       // base url obtained from getBaseUrl() - this is the url on which api will be working after login - this is not the same as server url
    public $logicRules = FALSE;    // logic constants cache
    public $serverAdminId = FALSE;    // logic rules cache
    public $serverAdminGroupId = FALSE;    // server admin id
    public $groupsAdmins = array();    // server admin group id
    public $groupsSubgroups = array();  // data of admins per group
    public $groupsUsers = array();  // data of subgroups per group
    public $groups = array();  // data of users per group
    public $admins = array();  // complete groups data
    public $users = array();  // data of admins per admin id
    public $mailDetails = array();  // data of users per user id
    /**
     * @var AccountsManager $accountsManager
     */
    public $accountsManager = FALSE;  // e-mail details per key in format: $email.'###'.$login

    /**
     * Api managers (autoloaded at class construction):
     */
    /**
     * @var AdminsManager $adminsManager
     */
    public $adminsManager = FALSE;
    /**
     * @var GroupsManager $groupsManager
     */
    public $groupsManager = FALSE;
    /**
     * @var LogicConstantsManager $logicConstantsManager
     */
    public $logicConstantsManager = FALSE;
    /**
     * @var Lang $lang
     */
    public $lang = FALSE;
    /**
     * Class cache (to avoid duplicated Api requests/Database queries):
     */
    private $groupsData = array();
    /**
     * $specificApiErrorsTranslations is array where keys are api return messages and values are keys for them in language definition
     * Those are specific messages (eg. standard message from createGroup function is "Group can't be created" and it can be replaced by "Group with such properties already exists" if there is translation defined here for that)
     */
    private $specificApiErrorsTranslations = array
    (
        "Couldn't resolve host" => "couldnt_connect",
        "409 The group with a such properties already exists." => "group_with_such_properties_exist",
        "409 The admin with a such email already exists." => "admin_with_such_email_exist",
    );

    /**
     *
     * @param string $base_url - server base url
     * @param ModuleConfig $configuration - module configuration object (which extends ModuleConfig abstract class)
     * @param ModuleVars $module_vars_object - object to access module variables that are stored in database (ModuleVars)
     * @param bool $useClientLanguageParams
     * @throws UserException
     */
    public function __construct($base_url, ModuleConfig $configuration, ModuleVars $module_vars_object, $useClientLanguageParams = true)
    {
        $this->lang = new Lang(ACRONIS_INCLUDES_DIR . 'Lang' . DS, $useClientLanguageParams);

        if (!extension_loaded('curl')) {
            throw new UserException($this->lang->get('curl_not_installed_error'));
        }

        $this->api = new Api(
            $base_url,
            $configuration->verifyCertificates,
            $configuration->logApiRequests,
            $configuration->logErrors,
            $configuration->moduleName
        );

        $this->moduleVars = $module_vars_object;
        $this->accountsManager = new AccountsManager($this);
        $this->adminsManager = new AdminsManager($this);
        $this->groupsManager = new GroupsManager($this);
        $this->logicConstantsManager = new LogicConstantsManager($this);
    }

    public function __destruct()
    {

    }

    /**
     * Checks if something for desired group was already requested from server and is in object cache
     * @param int $group_id -group id
     * @param string $what - one of: admins/subgroups/users/groups (the last one are desired group details)
     * @return boolean - true if this was requested before
     */
    public function hasBeenQueriedFor($group_id, $what)
    {
        if (isset($this->groupsData[$group_id]) &&
            isset($this->groupsData[$group_id][$what]) &&
            $this->groupsData[$group_id][$what]
        ) {
            return true;
        }
        return false;
    }

    /**
     * Marks that something for desired group was already requested from server and is in object cache
     * @param int $group_id - group id
     * @param string $what - one of: admins/subgroups/users/groups (the last one are desired group details)
     */
    public function markThatHasBeenQueriedFor($group_id, $what)
    {
        if (!isset($this->groupsData[$group_id])) {
            $this->groupsData[$group_id] = array();
        }
        $this->groupsData[$group_id][$what] = true;
    }

    /**
     * testFatalRequestError
     * Tests if fatal request error appear, translate fatal api error messages and eventually log error message if approperiate config option is enabled
     * (some specific api errors can have further explanation what happen: eg. "Group can't be created" can be replaced here with "Group with such properties already exist. Check the name")
     * @param string $message - error message to show/log if there is request error ("$ecode" string will be replaced with request error code that occur)
     * @param bool $add_please_check_config - true to add "Please check your configuration string" if there is an error
     */
    public function testFatalRequestError($message, $add_please_check_config = false)
    {
        $api_error_message = strip_tags($this->api->getLastCurlRawResponse());
        if (trim($api_error_message) == '') {
            $api_error_message = $this->api->getLastCurlErrorMessage();
        }

        foreach ($this->specificApiErrorsTranslations as $key => $translation) {
            if (stristr($api_error_message, $key)) {
                $message = $this->lang->get($translation);
            }
        }

        $this->api->testFatalRequestError($message, $add_please_check_config);
    }

}

        
    
