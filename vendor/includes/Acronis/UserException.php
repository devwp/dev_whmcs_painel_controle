<?php
/**
 * @Copyright © 2002-2016 Acronis International GmbH. All rights reserved
 */

namespace Acronis;

/**
 * class just to recognize module error messages such as "Please check your configuration" from real Exceptions
 */
class UserException extends \Exception
{

}