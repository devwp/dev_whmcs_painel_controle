<?php
/**
 *
 * @version 1.0.4
 * @Copyright © 2002-2016 Acronis International GmbH. All rights reserved
 *
 * OPTIONAL DEPENDENCIES: Acronis\System\PageBuilder
 */

namespace Acronis;

use Acronis\Standard\Configs\ModuleConfig;
use Monolog\Formatter\LineFormatter;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;

class ErrorHandler
{
    protected $level;

    /**
     * ErrorHandler constructor.
     * @param ModuleConfig $config
     */
    public function __construct(ModuleConfig $config)
    {
        $this->level = error_reporting(E_ALL);

        $logger = Log::getInstance()->getLogger();

        $errHandler = new \Monolog\ErrorHandler($logger);
        $errHandler->registerErrorHandler();
        $errHandler->registerExceptionHandler();
    }

    public function unregister()
    {
        restore_exception_handler();
        restore_error_handler();

        error_reporting($this->level);
    }
}