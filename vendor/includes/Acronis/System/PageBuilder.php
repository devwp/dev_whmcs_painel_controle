<?php
/**
 *
 * @version 1.0.1
 * @Copyright © 2002-2016 Acronis International GmbH. All rights reserved
 */

namespace Acronis\System;

class PageBuilder
{
    private $smarty = null;
    private $template;
    private $variables = array();

    public function __construct($template)
    {
        $this->template = $template;
    }

    /**
     * Assign vale to smarty variable
     * @param string $name - name of the variable
     * @param mixed $value - value to assign
     */
    public function __set($name, $value)
    {
        $this->assign($name, $value);
    }

    /**
     * Sets view template to be used
     * @param string $template - template path
     */
    public function setTemplate($template)
    {
        $this->template = $template;
    }

    public function build()
    {
        if (!file_exists($this->template)) {
            throw new \Exception('File ' . $this->template . ' does not exists');
        }

        global $templates_compiledir;
        $this->enableSmarty();
        if (method_exists($this->smarty, 'muteExpectedErrors')) {
            $this->smarty->muteExpectedErrors();//whmcs 6 - since we have own error handler
        }

        $this->smarty->template_dir = dirname($this->template);
        $this->smarty->compile_dir = $templates_compiledir;
        $this->smarty->force_compile = 1;


        foreach ($this->variables as $key => $val) {
            $this->smarty->assign($key, $val);
        }

        $output = $this->smarty->fetch(substr($this->template, strlen(dirname($this->template) . DS)), time());

        return $output;
    }

    /**
     * @return bool
     * @throws \Exception
     */
    private function enableSmarty()
    {
        if ($this->smarty) {
            return true;
        }

        if (!class_exists('Smarty')) {
            if (file_exists(SMARTY_FILE)) {
                require_once(SMARTY_FILE);
            } else {
                throw new \Exception('Smarty does not exists.');
            }
        }

        $this->smarty = new \Smarty();
    }

    public function getVars()
    {
        return $this->variables;
    }

    public function setVar($name, $value)
    {
        $this->variables[$name] = $value;
    }

}


