<?php
/**
 *
 * @version 1.0.0
 * @Copyright © 2002-2016 Acronis International GmbH. All rights reserved
 */
namespace Acronis\WHMCS;

use Illuminate\Database\Capsule\Manager as Capsule;

class AdminUser
{
    public static function getAdminUserName()
    {
        $adminId = self::getAdminUserID();

        $db = Capsule::connection()->getPdo();

        $sth = $db->prepare('SELECT `username` FROM `tbladmins` WHERE `id`= :adminId LIMIT 1');
        $sth->execute(array(':adminId' => $adminId));
        $result = $sth->fetch();
        return $result['username'];
    }

    public static function getAdminUserID()
    {
        $db = Capsule::connection()->getPdo();
        $result = $db->query('SELECT a.`id` FROM `tbladmins` a JOIN `tbladminperms` p ON p.`roleid`=a.`roleid` AND p.`permid`=81 AND a.`disabled`=0 LIMIT 1')->fetch();
        return $result['id'];
    }
}