<?php
/**
 *
 * @version 1.0.0
 * @Copyright © 2002-2016 Acronis International GmbH. All rights reserved
 */
namespace Acronis\WHMCS;

use Illuminate\Database\Capsule\Manager as Capsule;

class Servers
{
    /**
     * List of product modules servers
     *
     * @return array list of used servers in WHMCS
     */
    public static function getServerData()
    {
        $db = Capsule::connection()->getPdo();

        $sth = $db->prepare('SELECT * FROM `tblservers` WHERE `type` = ?');
        $sth->execute(array(ACRONIS_SERVICE_NAME));

        return $sth->fetchAll();
    }
}