<?php
/**
 *
 * @version 1.0.0
 * @Copyright © 2002-2016 Acronis International GmbH. All rights reserved
 */
namespace Acronis\WHMCS;

use Acronis\Log;
use Illuminate\Database\Capsule\Manager as Capsule;

class CustomFields
{
    private static $autoCreateProperties = array(); //place for storing properties if field is automatically created (per module name)
    private $moduleName = '';

    public function __construct($module_name)
    {
        $this->moduleName = $module_name;
    }

    /**
     *
     * @param string $modulename - name of the module which is using this function
     * @param string $fieldname
     * @param string $fieldtype - fieldtype (any of: 'text','dropdown','link','password','tickbox','textarea')
     * @param string $validationregexp - regexp to validate value in field
     * @param bool $adminonly - true if this is admin-only field
     * @param bool $required - true if this is required field
     * @param bool $showonorderform - true to show on order form
     * @param bool $showininvoice - true to show in invoice
     * @param mixed $fieldoptions - array of options if fieldtype='dropdown'
     * @param string $description - field description
     * @param string $itemtype - type of item (eg. 'product')
     * @param int $sortorder - default is 0
     * @param string $defaultvalue - default is empty string
     * @return array Created configuration for this custom field
     */
    public static function setAutoCreateProperties($modulename, $fieldname, $fieldtype = 'text', $validationregexp = '', $adminonly = true, $required = false, $showonorderform = false, $showininvoice = false, $fieldoptions = array(), $description = '', $itemtype = 'product', $sortorder = 0, $defaultvalue = '')
    {
        $return = self::$autoCreateProperties [$modulename][$fieldname] = array
        (
            'fieldtype' => $fieldtype,
            'validationregexp' => $validationregexp,
            'adminonly' => $adminonly,
            'required' => $required,
            'showonorderform' => $showonorderform,
            'showininvoice' => $showininvoice,
            'fieldoptions' => $fieldoptions,
            'description' => $description,
            'itemtype' => $itemtype,
            'sortorder' => $sortorder,
            'defaultvalue' => $defaultvalue
        );

        return $return;
    }

    /**
     *
     * @param int $itemrelid - id of item (eq. pid (tblproducts.id) if itemtype='product')
     * @param int $relid - id of instance of item (eq. serviceid (tblhosting.id) if itemtype='product')
     * @param string $fieldname
     * @param string $itemtype - type of item (eg. 'product')
     * @return mixed FALSE if there is no such field for this instance
     */
    public function get($itemrelid, $relid, $fieldname, $itemtype = 'product')
    {
        Log::getInstance()->event("Get {$fieldname} in customfields for product {$itemrelid} and serviceId={$relid}");
        $db = Capsule::connection()->getPdo();
        $sth = $db->prepare(
            'SELECT `value` FROM `tblcustomfieldsvalues` v JOIN `tblcustomfields` f ON v.`fieldid`=f.`id` AND f.`type`=:type AND f.`relid`=:itemRelId WHERE v.`relid`=:relId AND f.`fieldname`= :fieldname ');
        $sth->execute(array(':type' => $itemtype, ':relId' => $relid, ':itemRelId' => $itemrelid, ':fieldname' => $fieldname));

        $result = $sth->fetch();
        $val = $result['value'];

        //auto creating if autocreate config exists:
        if ($val === FALSE && isset(self::$autoCreateProperties[$this->moduleName][$fieldname])) {
            $fields = self::$autoCreateProperties[$this->moduleName][$fieldname];
            $this->create($itemrelid, $relid, $fieldname, $fields['fieldtype'], $fields['validationregexp'], $fields['adminonly'], $fields['required'], $fields['showonorderform'], $fields['showininvoice'], $fields['fieldoptions'], $fields['description'], $fields['itemtype'], $fields['sortorder'], $fields['defaultvalue']);

            $sth = $db->prepare(
                'SELECT `value` FROM `tblcustomfieldsvalues` v JOIN `tblcustomfields` f ON v.`fieldid`=f.`id` AND f.`type`=:type AND f.`relid`=:itemRelId WHERE v.`relid`=:relId AND f.`fieldname`= :fieldname ');
            $sth->execute(array(':type' => $itemtype, ':relId' => $relid, ':itemRelId' => $itemrelid, ':fieldname' => $fieldname));

            $result = $sth->fetch();
            $val = $result['value'];
        }

        return $val;
    }

    /**
     *
     * @param int $itemrelid - id of item (eq. pid (tblproducts.id) if itemtype='product')
     * @param int $relid - id of instance of item (eq. serviceid (tblhosting.id) if itemtype='product')
     * @param string $fieldname
     * @param string $fieldtype - fieldtype (any of: 'text','dropdown','link','password','tickbox','textarea')
     * @param string $validationregexp - regexp to validate value in field
     * @param bool $adminonly - true if this is admin-only field
     * @param bool $required - true if this is required field
     * @param bool $showonorderform - true to show on order form
     * @param bool $showininvoice - true to show in invoice
     * @param mixed $fieldoptions - array of options if fieldtype='dropdown'
     * @param string $description - field description
     * @param string $itemtype - type of item (eg. 'product')
     * @param int $sortorder - default is 0
     * @param string $defaultvalue - default is empty string
     * @return mixed FALSE if field has not been created
     */
    public function create($itemrelid, $relid, $fieldname, $fieldtype = 'text', $validationregexp = '', $adminonly = true, $required = false, $showonorderform = false, $showininvoice = false, $fieldoptions = array(), $description = '', $itemtype = 'product', $sortorder = 0, $defaultvalue = '')
    {
        $fieldoptions_r = is_array($fieldoptions) ? implode(',', $fieldoptions) : $fieldoptions;
        $adminonly_r = $adminonly ? 'on' : '';
        $required_r = $required ? 'on' : '';
        $showonorderform_r = $showonorderform ? 'on' : '';
        $showininvoice_r = $showininvoice ? 'on' : '';

        $db = Capsule::connection()->getPdo(); //boole to on/''
        //try to get custom field id from db
        $sth = $db->prepare('SELECT `id` FROM `tblcustomfields` WHERE `type`=? AND `relid`=? AND `fieldname`= ?');
        $sth->execute(array($itemtype, $itemrelid, $fieldname));
        $result = $sth->fetch();
        $customFieldId = $result['id'];

        //if threre is no custom field in add - try to add it
        if (!$customFieldId) {
            Log::getInstance()->event("Create customField '{$fieldname}' for product {$itemrelid} and serviceId = {$relid}");
            $sth = $db->prepare(
                'INSERT IGNORE INTO `tblcustomfields` (
                    `type`,
                    `relid`,
                    `fieldname`,
                    `fieldtype`,
                    `description`,
                    `fieldoptions`,
                    `regexpr`,
                    `adminonly`,
                    `required`,
                    `showorder`,
                    `showinvoice`,
                    `sortorder`
                ) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)');

            $sth->execute(array(
                $itemtype,
                $itemrelid,
                $fieldname,
                $fieldtype,
                $description,
                $fieldoptions_r,
                $validationregexp,
                $adminonly_r,
                $required_r,
                $showonorderform_r,
                $showininvoice_r,
                $sortorder
            ));

            $customFieldId = $db->lastInsertId();

        }

        //add values to selected custom field id
        $sth = $db->prepare('INSERT IGNORE INTO `tblcustomfieldsvalues` (`fieldid`,`relid`,`value`) VALUES (?,?,?)');
        $sth->execute(array($customFieldId, $relid, $defaultvalue));

        $success = $sth->rowCount();

        return $success > 0;
    }

    /**
     *
     * @param int $itemrelid - id of item (eq. pid (tblproducts.id) if itemtype='product')
     * @param int $relid - id of instance of item (eq. serviceid (tblhosting.id) if itemtype='product')
     * @param string $fieldname
     * @param string $newvalue
     * @param string $itemtype - type of item (eg. 'product')
     * @return mixed FALSE if there is no such field for this instance
     */
    public function set($itemrelid, $relid, $fieldname, $newvalue, $itemtype = 'product')
    {
        Log::getInstance()->event("Set {$fieldname} = {$newvalue} in customfields for product {$itemrelid} and serviceId={$relid}");
        $db = Capsule::connection()->getPdo();

        $sth = $db->prepare('UPDATE `tblcustomfieldsvalues` v JOIN `tblcustomfields` f ON v.`fieldid`=f.`id` AND f.`type`=? AND f.`relid`=? SET v.`value`=? WHERE v.`relid`=? AND f.`fieldname`= ?');
        $sth->execute(array($itemtype, $itemrelid, $newvalue, $relid, $fieldname));

        $val = $sth->rowCount();


        //auto creating if autocreate config exists:
        if ($val <= 0 && isset(self::$autoCreateProperties[$this->moduleName][$fieldname])) {
            $fields = self::$autoCreateProperties[$this->moduleName][$fieldname];
            $this->create($itemrelid, $relid, $fieldname, $fields['fieldtype'], $fields['validationregexp'], $fields['adminonly'], $fields['required'], $fields['showonorderform'], $fields['showininvoice'], $fields['fieldoptions'], $fields['description'], $fields['itemtype'], $fields['sortorder'], $fields['defaultvalue']);

            $sth = $db->prepare('UPDATE `tblcustomfieldsvalues` v JOIN `tblcustomfields` f ON v.`fieldid`=f.`id` AND f.`type`=? AND f.`relid`=? SET v.`value`=? WHERE v.`relid`=? AND f.`fieldname`=?');
            $sth->execute(array($itemtype, $itemrelid, $newvalue, $relid, $fieldname));
            $val = $sth->rowCount();
        }

        return $val > 0;
    }
}

