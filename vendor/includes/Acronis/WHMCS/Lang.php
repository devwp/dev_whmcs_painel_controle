<?php
/**
 *
 * @version 1.0.0
 * @Copyright © 2002-2016 Acronis International GmbH. All rights reserved
 * @warning default lang directory is set to addon_directory/lang/ to change it - simply find and change $default_lang_directory variable
 * DEPENDENCIES: \Acronis\Helpers\RequestCleaner, \Acronis\Helpers\Arr RequestCleaner, \Acronis\Database\Database
 */

namespace Acronis\WHMCS;

use Acronis\Helpers\Arr;
use Acronis\Helpers\RequestCleaner;
use Illuminate\Database\Capsule\Manager as Capsule;

class Lang
{
    private $lang = array();
    private $language = '';
    private $defaultLanguage = 'english'; // little configuration - change it if you want to change default class language
    private $defaultCustomPortugues = 'portuguese-br';

    private $db;

    /**
     *
     * @param string $language_dir - directory with all languages
     * @param bool|string $useClientLanguageParams bool flag, use or not flag from client's ui
     */
    public function __construct($language_dir, $useClientLanguageParams = true)
    {
        $language = '';
        $this->db = Capsule::connection()->getPdo();
        RequestCleaner::init('INPUT_SESSION');
        $request = RequestCleaner::getAllVars();

        if (isset($request['language'])) {
            $language = strtolower($request['language']);
        } elseif (isset($request['Language']) && $useClientLanguageParams) {
            $language = strtolower($request['Language']);
        } elseif (isset($request['uid']) && $useClientLanguageParams) {
            $language = $this->getClientLanguage($request['uid']);
        } elseif (isset($request['adminid'])) {
            $sth = $this->db->prepare('SELECT `language` FROM `tbladmins` WHERE `id` =?');
            $sth->execute(array($request['adminid']));
            $result = $sth->fetch();
            $language = strtolower($result['language']);
        }

        if (!$language) {
            $this->getDefaultLanguage();
        }

        if (!$language) {
            $language = $this->defaultLanguage;
        }

        //set default portugues to custom localised dialect "portuguese-br"
        if ($language == 'portugues') {
            $language = $this->defaultCustomPortugues;
        }

        $this->loadLangFile($language, $language_dir);
    }

    /**
     * Gets client language.
     * @param $userId
     * @return string
     */
    private function getClientLanguage($userId)
    {
        $sth = $this->db->prepare('SELECT `language` FROM `tblclients` WHERE `id` =?');
        $sth->execute(array($userId));
        $result = $sth->fetch();
        return strtolower($result['language']);
    }

    /**
     * Gets default language from global settings.
     * @return mixed
     */
    private function getDefaultLanguage()
    {
        $sth = $this->db->prepare('SELECT `value` FROM `tblconfiguration` WHERE `setting` =?');
        $sth->execute(array('language'));
        $result = $sth->fetch();
        return $result['value'];
    }

    /**
     * Loads language file
     * @param string $language - Language to load
     * @param string $language_dir - Directory with all languages
     * @throws \Exception
     */
    private function loadLangFile($language, $language_dir)
    {
        $path = $language_dir . $language . '.php';

        if (file_exists($path)) {
            require $path;
            $this->lang = $_LANG;
            $this->language = $language;
        } else {
            $language = $this->defaultLanguage;
            $scanpath = $path;
            $path = $language_dir . $language . ".php";
            if (!file_exists($path)) {
                throw new \Exception("Cannot load default language(default='.$path.'; searched='.$scanpath.'). Please check module installation.");
            }
            require $path;
            $this->lang = $_LANG;
            $this->language = $language;
        }
    }

    /**
     * Gets desired string from language file
     * @param string $name - language string path (comma separated)
     * @param array $replaces - array with values that will replace '??' strings
     * @param string $default - string that will be returned if $name is not found in language file
     * @return string
     */
    public function get($name, $replaces = array(), $default = null)
    {
        if ($default === null) {
            $default = '[' . $name . ']';
        }
        $result = Arr::getByPath($this->lang, $name, $default);
        if ($result == $default && !empty($replaces)) {
            $result .= '(PARAMS:' . str_repeat(' ??,', count($replaces));
            $result = substr($result, 0, -1) . ')';
        }
        return vsprintf(str_replace('??', '%s', $result), $replaces);
    }

    /**
     * Gets language array (eg. if is needed for smarty templates)
     * @return array
     */
    public function getLangArray()
    {
        return $this->lang;
    }

    /**
     * Gets loaded language name
     * @return string
     */
    public function getLoadedLanguage()
    {
        return $this->language;
    }

    /**
     * Gets ISO language code for creating group.
     * @param int $userId
     * @return string language ISO code
     */
    public function getLanguageForABC($userId)
    {
        $language = $this->getClientLanguage($userId) ? $this->getClientLanguage($userId) : $this->getDefaultLanguage();
        return \Acronis\Standard\Helpers\LanguageMap::getISOLanguageCodeByWHMCSLanguage($language);
    }

}

