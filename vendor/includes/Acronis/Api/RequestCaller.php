<?php
/**
 * @Copyright © 2002-2016 Acronis International GmbH. All rights reserved
 * DEPENDENCIES: Acronis\Api\Services\Curl, Acronis\UserException
 */

namespace Acronis\Api;

use Acronis\Api\Services\Curl;
use Acronis\CurlException;
use Acronis\HttpException;
use Acronis\Log;
use Acronis\UserException;

class RequestCaller
{
    const LOG_PREFIX_RESPONSE = 'Response: ';
    const LOG_PREFIX_REQUEST = 'Request: ';

    public $baseUrl;
    public $errorMessages = array(
        'please_check_config' => 'Please check your configuration',
    );
    /**
     * @var Curl $curl
     */
    private $curl;
    private $verifyCertificates;
    private $logApiRequests = false;
    private $logRequests = false;
    private $logErrors = false;
    private $moduleName = ACRONIS_SERVICE_NAME;
    private $cookieFile = null;
    private $cookieJar = null;
    private $ownCookieFile = null;
    private $ownCookieJar = null;
    private $fatalRequestError = false;

    /**
     *
     * @param $base_url - base server url
     * @param bool $verify_certificates - true to verify certificates
     * @param bool $log_api_requests - true to log requests to module log
     * @param bool $log_requests - true to log requests to module log
     * @param bool $log_errors - true to log error messages to module log
     * @param string|null $module_name - name of the module that is using this class
     */
    public function __construct($base_url = null, $verify_certificates = true, $log_api_requests = false, $log_requests = false, $log_errors = false, $module_name = null)
    {
        $this->baseUrl = $base_url;
        $this->verifyCertificates = $verify_certificates;
        $this->logApiRequests = $log_api_requests;
        $this->logRequests = $log_requests;
        $this->logErrors = $log_errors;
        if ($module_name != null) {
            $this->moduleName = $module_name;
        }

        $this->reloadCurl();
    }

    /**
     * reloadCurl
     * Restarts curl object before next request if needed
     */
    public function reloadCurl()
    {
        $this->curl = new Curl();
        $this->curl->complete(array($this, 'logRequestIfNecessary'));
        $this->curl->setHeader('Content-Type', 'application/json');

        if (!$this->verifyCertificates) {
            $this->curl->setOpt(CURLOPT_SSL_VERIFYHOST, 0);
            $this->curl->setOpt(CURLOPT_SSL_VERIFYPEER, 0);
        }
    }

    /**
     * Returns last curl raw response
     * @return string Response
     */
    public function getLastCurlRawResponse()
    {
        return $this->curl->rawResponse;
    }

    /**
     * Returns last curl error message
     * @return string Message
     */
    public function getLastCurlErrorMessage()
    {
        return $this->curl->curlErrorMessage;
    }

    /**
     * testFatalRequestError
     * Tests if fatal request error appear and eventually log error message if approperiate config option is enabled
     * @param string $message - error message to show/log if there is request error ($ecode string will be replaced with request error code that occur)
     * @param bool $add_please_check_config - true to add "Please check your configuration string" if there is an error
     * @throws UserException
     */
    public function testFatalRequestError($message, $add_please_check_config = false)
    {
        if (true !== $this->fatalRequestError) {
            if ($add_please_check_config) {
                $message .= '. ' . $this->errorMessages['please_check_config'];
            }

            $message = str_replace('$ecode', $this->fatalRequestError, $message);
            $this->logRequestIfNecessary(null, true);
            $this->logErrorIfNecessary($message);
            Log::getInstance()->error($message);
            throw ($this->curl->httpError) ?
                new HttpException($message, $this->curl->httpStatusCode) :
                new CurlException($message, $this->curl->curlErrorCode);
        }
    }

    /**
     * logRequestIfNecessary
     * This method is used to log request information based on config settings (logApiRequests)
     * This will not log error messages other than HTTP error code. See logErrors method to do that.
     * Do not use that method outside class - its public only because it has to be accessible by Curl class
     * @param Curl $curl - object of class Curl. $this->curl will be always used instead (this is for compatibility with Curl class)
     * @param bool $is_own_error - treat this as errored request
     * @return bool|int|mixed|null|string
     */
    public function logRequestIfNecessary($curl = null, $is_own_error = false)
    {
        $curl = $this->curl;

        $emsg = $curl->httpErrorMessage;  //httpStatusCode dont always have approperiate error code
        $data = $curl->getOpt(CURLOPT_POSTFIELDS);

        $url = $curl->url;
        $ematches = array();
        preg_match('/^\s*[a-z0-9\/\.]+\s+(\d+)\s/i', $emsg, $ematches);
        $ecode = isset($ematches[1]) ? $ematches[1] : ($curl->httpStatusCode);

        if (($ecode >= 200 && $ecode < 300) || $ecode == 304) {
            $ecode = true;
        } elseif ($ecode == 0) {
            $ecode = ($curl->curlErrorMessage);
        } else {
            $emsg = preg_replace('/<[^>]*?>/si', '', $curl->rawResponse);
            $ecode = 'HTTP error code ' . $ecode . ' ("' . $emsg . '")';
        }

        // TODO: to refactor this block of conditions
        if ($this->logRequests) {//if requests have to be logged
            //if $logApiRequests == 2 and $is_own_error is set to true OR
            //if $logApiRequests == 2 and not $is_own_error:
            //OTHER CASES:
            //if $logApiRequests == 2 and $is_own_error is set to false
            //   - we do not log in this case, because this is normal api request
            //    (we will eventually log this request later - only if own error will be found)
            //    (if this is error it will be logged with own error messages [prepared later] - not now)
            //if $logApiRequests != 2 and $is_own_error is set to true
            //   - in this case request was logged after api call - it is unnecessary to log it again
            if (($is_own_error && $this->logApiRequests == 2) ||
                (!$is_own_error && $this->logApiRequests != 2)
            ) {
                logModuleCall($this->moduleName, '',
                    'URL=' . $url . PHP_EOL .
                    print_r($curl->requestHeaders, true) . PHP_EOL .
                    print_r($data, true),
                    '',
                    ($ecode !== true ? $ecode : '') . PHP_EOL .
                    '[' . PHP_EOL .
                    print_r($curl->rawResponseHeaders, true) . PHP_EOL .
                    ']' . PHP_EOL .
                    print_r($curl->rawResponse, true)
                );
            }
        }
        if ($ecode !== true && !$is_own_error) {
            $this->fatalRequestError = $ecode;
        }

        return $ecode;
    }

    /**
     * logErrorIfNecessary
     * Will eventually log errors if any found and approperiate config option is enabled
     * @param string $error - error message to log
     */
    private function logErrorIfNecessary($error)
    {
        if ($this->logErrors && trim($error) != '') {
            logModuleCall($this->moduleName, '', 'ERRORS HAVE BEEN FOUND', '', 'ERROR=' . $error);
        }

    }

    /**
     * makeOwnError
     * Logs request and error message to module log if approperiate configuration settings are on and throws an UserException() exception
     * @param string $error - error message to log and throw ($ecode string will be replaced with request error code that occur)
     * @param bool $add_please_check_config - true to add "Please check your configuration string"
     * @param bool $error_only - true if this error is not request based - the request won't be logged if that cas (unless there is setting to log all requests - then request would be logged before; after curl_exec)
     * @throws UserException
     */
    public function makeOwnError($error, $add_please_check_config = false, $error_only = false)
    {
        //log request if necessary and this is own error:
        if (!$error_only) {
            $this->logRequestIfNecessary(null, true);
        }
        if ($add_please_check_config) {
            $error .= '. ' . $this->errorMessages['please_check_config'];
        }
        $error = str_replace('$ecode', $this->fatalRequestError, $error);
        $this->logErrorIfNecessary($error);

        throw ($this->curl->httpError) ?
            new HttpException($error, $this->curl->httpStatusCode) :
            new CurlException($error, $this->curl->curlErrorCode);
    }

    /**
     * setCookieFileForRequests
     * Sets Cookie file from which cookies will be read for all request if setCookieFile wasn't invoked before that request
     * @param string $cookie_filename - path to cookie file to use
     */
    public function setCookieFileForRequests($cookie_filename)
    {
        $this->cookieFile = $cookie_filename;
        $this->curl->setCookieFile($this->cookieFile);
    }

    /**
     * setCookieFile
     * Sets Cookie file from which cookies will be read for next request (this temporary replaces setCookieFileForRequests setting)
     * @param string $cookie_filename - path to cookie file to use
     */
    public function setCookieFile($cookie_filename)
    {
        $this->ownCookieFile = true;
        $this->curl->setCookieFile($cookie_filename);
    }

    /**
     * setCookieJarForRequests
     * Sets Cookie file to which cookies will be saved for all request if setCookieJar wasn't invoked before that request
     * @param string $cookie_filename - path to cookie file to use
     */
    public function setCookieJarForRequests($cookie_filename)
    {
        $this->cookieJar = $cookie_filename;
        $this->curl->setCookieJar($this->cookieJar);
    }

    /**
     * setCookieJar
     * Sets Cookie file to which cookies will be saved for next request (this temporary replaces setCookieJarForRequests setting)
     * @param string $cookie_filename - path to cookie file to use
     */
    public function setCookieJar($cookie_filename)
    {
        $this->ownCookieJar = true;
        $this->curl->setCookieJar($cookie_filename);
    }

    /**
     * getRawResponseHeaders
     * Gets raw response headers from last curl request
     * @return string
     */
    public function getRawResponseHeaders()
    {
        return $this->curl->rawResponseHeaders;
    }

    /**
     * Access to Curl class requests methods (delete/download/get/options/post/put/patch/head)
     *
     * @method array runDelete($url, $query_parameters, $post_data) Runs DELETE request | $url - url to download (can contains query parameters) | $query_parameters - query parameters ( as in GET;
     * will be added to url) | $post_data - data to be send ( as POST)
     * @method array runDownload($url, $filename) Runs GET request and stores file to disk | $url - url to download (can contains query parameters) | $filename - path to file where downloaded one will be stored
     * @method array runGet($url, $post_data) Runs GET request | $url - url to download (can contains query parameters) | $post_data - data to be send ( as POST)
     * @method array runOptions($url, $post_data) Runs OPTIONS request | $url - url to download (can contains query parameters) | $post_data - data to be send ( as POST)
     * @method array runPost($url, $post_data) Runs POST request | $url - url to download (can contains query parameters) | $post_data - data to be send ( as POST)
     * @method array runPut($url, $post_data) Runs PUT request | $url - url to download (can contains query parameters) | $post_data - data to be send ( as POST)
     * @method array runPatch($url, $post_data) Runs PATCH request | $url - url to download (can contains query parameters) | $post_data - data to be send ( as POST)
     * @method array runHead($url, $post_data) Runs HEAD request | $url - url to download (can contains query parameters) | $post_data - data to be send ( as POST)
     * @param string $name - function name
     * @param array $arguments - arguments array
     * @return array
     * @throws \Exception
     */
    public function __call($name, $arguments)
    {
        if ($this->cookieFile && !$this->ownCookieFile) $this->curl->setCookieFile($this->cookieFile);
        if ($this->cookieJar && !$this->ownCookieJar) $this->curl->setCookieJar($this->cookieJar);

        $this->fatalRequestError = true;

        $name_without_run = preg_replace('/^run/', '', $name);

        $mode = lcfirst($name_without_run);

        if (!in_array($mode, array('get', 'post', 'put', 'delete', 'download', 'options', 'patch', 'head')) OR $name_without_run == $name) {
            throw new \Exception('Desired api method not found: ' . $name);
        }

        $url_path = isset($arguments[0]) ? $arguments[0] : '';
        $data = isset($arguments[1]) ? $arguments[1] : null;

        $encodedData = json_encode($data);
        Log::getInstance()->event(self::LOG_PREFIX_REQUEST . strtoupper($mode) . ' ' . $url_path . ' ' . $encodedData);

        if ($data !== null) {
            $this->curl->$mode($this->baseUrl . $url_path, $encodedData);
        } else {
            $this->curl->$mode($this->baseUrl . $url_path);
        }

        if ($this->ownCookieFile) $this->ownCookieFile = false;
        if ($this->ownCookieJar) $this->ownCookieJar = false;

        $this->curl->setCookieFile(null);
        $this->curl->setCookieJar(null);

        return $this->errorHandler();
    }

    /**
     * errorHandler
     * Returns response as array or array with 5 curl variables from last request: error_message/request_headers/raw_response/response_headers/error_code
     * This can be helpful for some api managers methods
     * The 5 values array is returned only if there is curl error (or http code 400+ or 500+)
     * @return array
     */
    private function errorHandler()
    {
        if ($this->curl->error) {
            return array(
                "error_message" => $this->curl->errorMessage,
                "request_headers" => $this->curl->requestHeaders,
                "raw_response" => $this->curl->rawResponse,
                "response_headers" => $this->curl->responseHeaders,
                "error_code" => $this->curl->errorCode
            );
        } else {
            return $this->parseResponse($this->curl->response);
        }
    }

    /**
     * parseResponse
     * @param mixed $response - object or array or json encoded string
     * @return array|mixed
     */
    public function parseResponse($response)
    {
        if (is_object($response) || is_array($response)) {
            $result = $this->objectToArray($response);
            Log::getInstance()->event(self::LOG_PREFIX_RESPONSE . print_r($result, true));
            return $result;
        } else {
            Log::getInstance()->event(self::LOG_PREFIX_RESPONSE . $response);
            return json_decode($response, true);
        }
    }

    /**
     * objectToArray
     * Recursively converts object to an array (eg. if this is array of objects)
     * @param mixed $obj - object or array
     * @return array
     */
    public function objectToArray($obj)
    {
        if (is_object($obj)) {
            $obj = (array)$obj;
        }
        if (is_array($obj)) {
            $new = array();
            foreach ($obj as $key => $val) {
                $new[$key] = $this->objectToArray($val);
            }
        } else {
            $new = $obj;
        }
        return $new;
    }


}