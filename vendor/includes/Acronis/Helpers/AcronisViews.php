<?php
/**
 *
 * Acronis modules specific helpers
 * @version 1.0.0
 * @Copyright © 2002-2016 Acronis International GmbH. All rights reserved
 * DEPENDENCIES: \Acronis\WHMCS\Lang
 */
namespace Acronis\Helpers;

use Acronis\WHMCS\Lang;

class AcronisViews
{

    const WARNING_DISK_SPACE = 0.9;

    /**
     * @var Lang $lang
     */
    private $lang;

    public function __construct()
    {
        $this->lang = new Lang(ACRONIS_INCLUDES_DIR . 'Lang' . DS);
    }

    /**
     * Prepares group usage details to format acceptable by view which is using it
     * @param array $group_params - array of values obtained by groupsManager->getGroupParamsForced method
     * @return array
     */
    public function prepareGroupUsageDetails($group_params)
    {
        $result = $this->getGroupUsageDetailsDefaultVariables();
        $error = 'success';

        //usage values
        $result['server_count'] = $group_params['usage.server_count'];
        $result['storage_size'] = $group_params['usage.storage_size'];
        $result['vm_count'] = $group_params['usage.vm_count'];
        $result['workstation_count'] = $group_params['usage.workstation_count'];
        $result['mobile_count'] = $group_params['usage.mobile_count'];
        $result['mailbox_count'] = $group_params['usage.mailbox_count'];

        //quota values
        $result['server_count_limit'] = $group_params['privileges.server_count'];
        $result['storage_size_limit'] = $group_params['privileges.storage_size'];
        $result['vm_count_limit'] = $group_params['privileges.vm_count'];
        $result['workstation_count_limit'] = $group_params['privileges.workstation_count'];
        $result['mobile_count_limit'] = $group_params['privileges.mobile_count'];
        $result['mailbox_count_limit'] = $group_params['privileges.mailbox_count'];

        $result['service_users_limit'] = null;

        foreach (array('server_count', 'storage_size', 'vm_count', 'workstation_count', 'mobile_count', 'mailbox_count') as $key) {
            $unlimited = false;
            $unknownlimit = false;

            //prepare warning message for exceeded quotas
            $result[$key . '_warning'] = $this->prepareWarnings($key, $result[$key], $result[$key . '_limit']);
            //prepare flag show or not counter. not show if quota = 0
            $result[$key . '_show'] = $result[$key . '_limit'] !== 0;

            $bytes_format = false;
            if ($key == 'storage_size') {
                $bytes_format = true;
            }

            //unlimited-fix:
            if ($result[$key . '_limit'] === null OR $result[$key . '_limit'] === -1) {
                $result[$key . '_limit'] = $result[$key];
                $unlimited = true;
            }

            //make friendly representation of values and limits:
            $result[$key . '_friendly'] = Math::getValueFriendlyFormat($result[$key], 2, false, false, $bytes_format, '&nbsp;');
            $result[$key . '_limit_friendly'] = Math::getValueFriendlyFormat($result[$key . '_limit'], 2, $unlimited, $unknownlimit, $bytes_format, '&nbsp;');
        }

        //service_users fix:
        $result['service_users_percent_friendly'] = $result['service_users_friendly'];
        $result['service_users_friendly'] = '';
        $result['service_users_limit'] = '';

        foreach ($result as $key => $var) {
            //language support:
            if ($var && in_array($var, array('unlimited', 'wrong_data'))) {
                $result[$key] = $var = str_replace(' ', '&nbsp;', $this->lang->get($var));
            }

            if (FALSE !== stripos($key, '_friendly')) {
                $result[$key] = htmlspecialchars($var);
            }
        }

        return array($error, $result);
    }

    /**
     * Returns array of default group usage details view parameters
     * @return array
     */
    public function getGroupUsageDetailsDefaultVariables()
    {
        return array
        (
            'server_count' => 0,
            'service_users' => 0,
            'storage_size' => 0,
            'vm_count' => 0,
            'workstation_count' => 0,

            'server_count_limit' => 0,
            'service_users_limit' => 0,
            'storage_size_limit' => 0,
            'vm_count_limit' => 0,
            'workstation_count_limit' => 0,

            'server_count_percent' => 0,
            'service_users_percent' => 0,
            'storage_size_percent' => 0,
            'vm_count_percent' => 0,
            'workstation_count_percent' => 0,

            'server_count_percent_friendly' => '0&nbsp;%',
            'service_users_percent_friendly' => '0&nbsp;%',
            'storage_size_percent_friendly' => '0&nbsp;%',
            'vm_count_percent_friendly' => '0&nbsp;%',
            'workstation_count_percent_friendly' => '0&nbsp;%',

            'server_count_friendly' => '0',
            'service_users_friendly' => '0',
            'storage_size_friendly' => '0&nbsp;B',
            'vm_count_friendly' => '0',
            'workstation_count_friendly' => '0',

            'server_count_limit_friendly' => '0',
            'service_users_limit_friendly' => '0',
            'storage_size_limit_friendly' => '0&nbsp;B',
            'vm_count_limit_friendly' => '0',
            'workstation_count_limit_friendly' => '0',
        );
    }

    /**
     * returns warning message for specified paramName
     *
     * @param string $paramName
     * @param int $value
     * @param int $limit
     * @return bool|string
     */
    private function prepareWarnings($paramName, $value, $limit)
    {
        //get warnings messages
        $result = false;
        if ($limit > 0) {
            //get warning for overusage
            if ($value > $limit) {
                $result = $this->lang->get('overusage.' . $paramName);
                //get warning for nearly limit of storage size
            } elseif ($paramName == 'storage_size' && ($value / $limit) > self::WARNING_DISK_SPACE) {
                $result = $this->lang->get('nearlylimit.' . $paramName);
            }
        }
        return $result;
    }

}