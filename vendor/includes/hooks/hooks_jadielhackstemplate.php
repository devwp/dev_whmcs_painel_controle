<?php
use WHMCS\View\Menu\Item;

add_hook('ClientAreaHomepagePanels', 1,
	function (Item $homePagePanels)	{

	//	$homePagePanels->removeChild('Affiliate Program');
	//	$homePagePanels->removeChild('Register a New Domain');
	//	$homePagePanels->removeChild('Recent News');
	//	$homePagePanels->getChild('Recent Support Tickets')->moveToBack();
	//	$homePagePanels->getChild('Active Products/Services')->moveToFront();
	//	$homePagePanels->getChild('Overdue Invoices')->moveToBack();
	//	$homePagePanels->getChild('Recent News')->moveToFront();



	//$homePagePanels->removeChild('Overdue Invoices');
	//$homePagePanels->removeChild('Recent Support Tickets');
	//$homePagePanels->removeChild('Active Products/Services');
	//$homePagePanels->getChild('Active Products/Services');->moveToFront();
	//$homePagePanels->getChild('Overdue Invoices')->getChild('Announcements')->moveToBack();
	//$homePagePanels->getChild('Recent Support Tickets');
	//setOrder(integer  $order)
	}
);

add_hook('ClientAreaPrimaryNavbar', 1, function (Item $primaryNavbar)
{
	$userid = intval($_SESSION['uid']);
    if ($userid!='0'){
		$services = $primaryNavbar->getChild('Services');
		//$services->removeChild('Order New Services');
		
		$Billing = $primaryNavbar->getChild('Billing');
		$Billing->removeChild('My Quotes');
		$Billing->removeChild('Billing Divider');
		$Billing->removeChild('Mass Payment');
		
		$suport = $primaryNavbar->getChild('Support');
		$knowledgebase = $suport->getChild('Knowledgebase');
		$knowledgebase->setAttribute('target', '_blank');
		//$Billing->removeChild('My Quotes');
		//if (isset($_GET['action']) && ($_GET['action'] == 'services' || $_GET['action'] == 'products')) {
		///}
		/*$newMenu = $primaryNavbar->addChild(
			'uniqueMenuItemName',
			array(
				'name' => 'Home',
				'label' => Lang::trans('languageStringVariable'),
				'uri' => 'clientarea.php',
				'order' => 99,
				'icon' => 'fa-calendar-o',
			)
		);*/
		
		$suport->addChild(
			'Open Ticket',
			array(
				'name' => 'Open Ticket',
				'label' => Lang::trans('navopenticket'),
				'uri' => 'submitticket.php',
				'order' => 1,
				'icon' => '',
			)
		);
		$suport->getChild('Tickets')->moveToFront();
		//$primaryNavbar->addChild('Announcements');
		$suport->removeChild('Announcements');
		$suport->removeChild('Downloads');
		
		$primaryNavbar->removeChild('Open Ticket');

		$primaryNavbar->addChild(
			'Announcements',
			array(
				'name' => 'Announcements',
				'label' => Lang::trans('announcementstitle'),
				'uri' => 'announcements.php',
				'order' => 100,
				'icon' => '',
			)
		);
		$services = $primaryNavbar->getChild('Services');
	}
	//$home = $primaryNavbar->getChild('Home');
	//echo '<pre>';
	//var_export($primaryNavbar);
	//exit();

});

add_hook('ClientAreaPrimarySidebar', 1, function (Item $primarySidebar)
{

	$primarySidebar->removeChild('Client Details');
	$primarySidebar->removeChild('Service Details Overview');
	if (isset($_GET['action']) && ($_GET['action'] == 'services' || $_GET['action'] == 'products')) {
		$primarySidebar->removeChild('My Services Status Filter');
	}
	if (isset($_GET['action']) && $_GET['action'] == 'productdetails' ) {
		//$primarySidebar->removeChild('Service Details Actions');
	}
	if ($_SERVER['PHP_SELF'] == '/supporttickets.php' ) {
		$primarySidebar->removeChild('Ticket List Status Filter');//->removeChild('Customer-Reply');
		if(isset($_GET['step']) && ($_GET['step'] == '1' || $_GET['step'] == '2') ) {
			//$primarySidebar->removeChild('Ticket List Status Filter');
		}
	}

	if (isset($_GET['action']) && $_GET['action'] == 'invoices') {
		if (!is_null($primarySidebar->getChild('My Invoices Summary'))) {
			$primarySidebar->removeChild('My Invoices Summary');
		}
		if (!is_null($primarySidebar->getChild('My Invoices Status Filter'))) {
			$primarySidebar->removeChild('My Invoices Status Filter');
		}
	}
	if (isset($_GET['action']) && $_GET['action'] == 'addfunds') {
		if (!is_null($primarySidebar->getChild('Add Funds'))) {
			$primarySidebar->removeChild('Add Funds');
		}

	}
	if (isset($_GET['action']) && ( $_GET['action'] == 'details' || $_GET['action'] == 'contacts' || $_GET['action'] == 'changepw' || $_GET['action'] == 'emails')) {
		if (!is_null($primarySidebar->getChild('My Account'))) {
			$primarySidebar->removeChild('My Account');
		}

	}
	if (isset($_GET['action']) && ( $_GET['action'] == 'details' || $_GET['action'] == 'contacts' || $_GET['action'] == 'changepw' || $_GET['action'] == 'emails')) {
		if (!is_null($primarySidebar->getChild('My Account'))) {
			$primarySidebar->removeChild('My Account');
		}

	}



	//$secondarySidebar->removeChild('Client Contacts');
	//$secondarySidebar->removeChild('Client Shortcuts');
	//$secondarySidebar->AddChild('Recent News');
	//$secondarySidebar->AddChild('Affiliate Program');
	//$secondarySidebar->getChild('Recent News')->moveToFront();
	//$secondarySidebar->getChild('Affiliate Program')->moveToBack();
	//$secondaryNavbar->removeChild('Recent News');
	
});

include_once($_SERVER['DOCUMENT_ROOT']."/modules/addons/nfse_ginfes_addon/nfse_ginfes/nfse/functions.php"); 

add_hook('InvoiceCancelled', 1, 'callcancelarNota_1');
add_hook('InvoicePaid', 1, 'callenviaNota_1');

function callcancelarNota_1($vars){
	$invoiceid =$vars['invoiceid'];
	cancelarNota($invoiceid);
}

function callenviaNota_1($vars){
	$invoiceid =$vars['invoiceid'];
	enviaNota($invoiceid);
}


add_hook('ClientAreaSecondarySidebar', 1, function (Item $secondarySidebar)
{
	$secondarySidebar->removeChild('Client Contacts');
	$secondarySidebar->removeChild('Client Shortcuts');
	if (isset($_GET['action']) && ($_GET['action'] == 'services' || $_GET['action'] == 'products')) {
		$secondarySidebar->removeChild('My Services Actions');
	}
	
	if (isset($_GET['action']) && $_GET['action'] == 'invoices') {
		if (!is_null($secondarySidebar->getChild('Billing'))) {
			$secondarySidebar->removeChild('Billing');
		}
	}
	if (isset($_GET['action']) && $_GET['action'] == 'addfunds') {
		if (!is_null($secondarySidebar->getChild('Billing'))) {
			$secondarySidebar->removeChild('Billing');
		}
	}
	if ($_SERVER['PHP_SELF'] == '/supporttickets.php' || $_SERVER['PHP_SELF'] ==  '/announcements.php' ) {
		$secondarySidebar->removeChild('Support');//->removeChild('Customer-Reply');
		if(isset($_GET['step']) && ($_GET['step'] == '1' || $_GET['step'] == '2') ) {
			//$primarySidebar->removeChild('Ticket List Status Filter');
		}
	}



	//$secondarySidebar->AddChild('Recent News');
	//$secondarySidebar->AddChild('Affiliate Program');
	//$secondarySidebar->getChild('Recent News')->moveToFront();
	//$secondarySidebar->getChild('Affiliate Program')->moveToBack();
	//$secondaryNavbar->removeChild('Recent News');
});
