<?php
/**
 * nfse_ginfes
 *
 * Setting module for the nfse_ginfes.
 *
 * @package    WHMCS
 * @author     webplus <suporte@webplus.com.br>
 * @copyright  Copyright (c) webplus 2016
 * @link       http://www.webplus.com.br
 */

if (!defined("WHMCS"))
    die("This file cannot be accessed directly");

define("currentnfse_ginfesVersion", "1.0");
global $attachments_dir;

function nfse_ginfes_addon_config() {
    $configarray = array(
		"name" => "NFSE Ginfes",
		"description" => "Setting module for the nfse ginfes.",
		"version" => "1.0",
		"author" => "webplus",
		"fields" => array(
		"razaoSocial"			=> array ("FriendlyName" => "Razão Social"	, "Type" => "text", "Size" => "100", "Description" => "", "Default" => "", ),
		"cnpj"	=> array ("FriendlyName" => "CNPJ"		, "Type" => "text", "Size" => "14", "Description" => "Somente números", "Default" => "", ),
		"inscricaoMunicipal"	=> array ("FriendlyName" => "Inscrição Municipal"		, "Type" => "text", "Size" => "25", "Description" => "", "Default" => "", ),
		"caminhoCertificado"	=> array ("FriendlyName" => "Caminho do Certificado"		, "Type" => "text", "Size" => "100", "Description" => "Ex: certificado/cert.pfx", "Default" => "certificado/cert.pfx", ),
		"senhaSertificado"	=> array ("FriendlyName" => "Senha do Certificado"		, "Type" => "text", "Size" => "14", "Description" => "", "Default" => "", ),
		"codigoServico"	=> array ("FriendlyName" => "Código do Serviço"		, "Type" => "text", "Size" => "14", "Description" => "", "Default" => "", ),
		"codigoAtividade"	=> array ("FriendlyName" => "Código da Atividade"		, "Type" => "text", "Size" => "14", "Description" => "", "Default" => "", ),
		"aliquotaMunicipal"	=> array ("FriendlyName" => "Aliquota Municipal"		, "Type" => "text", "Size" => "14", "Description" => "", "Default" => "", ),
		"aliquotaEstadual"	=> array ("FriendlyName" => "aliquota Estadual"		, "Type" => "text", "Size" => "14", "Description" => "", "Default" => "", ),
		"fonte"	=> array ("FriendlyName" => "aliquota Estadual"		, "Type" => "text", "Size" => "14", "Description" => "", "Default" => "", )
	));
    return $configarray;
}

function nfse_ginfes_addon_activate() {
	global $attachments_dir;
	mkdir ( $attachments_dir.'/nfse_ginfes_retornos');
	$query0 = "CREATE TABLE IF NOT EXISTS `mod_nfse_ginfesSettings` (`id` int(10) NOT NULL AUTO_INCREMENT, `name` TEXT NOT NULL, `value` TEXT NOT NULL, PRIMARY KEY (`id`))";
    $result0 = full_query($query0);
	full_query("INSERT INTO `mod_nfse_ginfesSettings` (`name`, `value`) VALUES ('version', '".currentnfse_ginfesVersion."')");
		$query1 = "
			CREATE TABLE IF NOT EXISTS `nfseGinfes` (
			`id` int(10) UNSIGNED NOT NULL,
			`invoiceID` varchar(45) DEFAULT NULL,
			`razaoSocial` varchar(300) NOT NULL,
			`cpfcnpj` varchar(45) NOT NULL,
			`Valor` decimal(10,0) NOT NULL,
			`xml_Envio` varchar(45) NOT NULL, 
			`codVerificacao` varchar(45) NOT NULL,
			`protocolo` varchar(45) NOT NULL,
			`numeroNFSE` varchar(45) NOT NULL,
			`DataReferencia` datetime NOT NULL,
			`DataCadastro` datetime NOT NULL,
			`numeroRps` varchar(45) NOT NULL,
			`dadosTomador` text NOT NULL,
			`cancelado` tinyint(1) UNSIGNED NOT NULL,
			`linkNota` varchar(250) NOT NULL,
			`IP` varchar(250) NOT NULL,
			`relUser` int(10) UNSIGNED NOT NULL,
			`isEnviado` tinyint(1) NOT NULL
			) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
	";
	 $result1 = full_query($query1);
	 
	/*$query2 = "INSERT INTO `tblemailtemplates` (`type`, `name`, `subject`, `message`, `attachments`, `fromname`, `fromemail`, `disabled`, `custom`, `language`, `copyto`, `plaintext`, `created_at`, `updated_at`) VALUES ('invoice', 'Enviar NFSE Ginfes', 'Webplus - Nota Fiscal Eletrônica de Serviços', '&lt;!-- Define Charset --&gt; &lt;!-- Responsive Meta Tag --&gt; Webplus - Hospedagem, E-mail, Cloud &amp;amp; Backup &lt;!-- Responsive Styles and Valid Styles --&gt;\r\n&lt;table class=&quot;main_color&quot; style=&quot;width: 100%; background-color: #5aa3bf;&quot; border=&quot;0&quot; cellspacing=&quot;0&quot; cellpadding=&quot;0&quot; bgcolor=&quot;3b8ced&quot;&gt;\r\n&lt;tbody&gt;\r\n&lt;tr&gt;\r\n&lt;td&gt;\r\n&lt;table class=&quot;container590&quot; style=&quot;width: 600px;&quot; border=&quot;0&quot; cellspacing=&quot;0&quot; cellpadding=&quot;0&quot; align=&quot;center&quot;&gt;\r\n&lt;tbody&gt;\r\n&lt;tr&gt;\r\n&lt;td style=&quot;font-size: 10px; line-height: 10px;&quot; height=&quot;10&quot;&gt; &lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td align=&quot;center&quot;&gt;\r\n&lt;table class=&quot;container590&quot; style=&quot;border-collapse: collapse; width: 600px;&quot; border=&quot;0&quot; cellspacing=&quot;0&quot; cellpadding=&quot;0&quot; align=&quot;left&quot;&gt;\r\n&lt;tbody&gt;\r\n&lt;tr&gt;\r\n&lt;td align=&quot;center&quot;&gt;\r\n&lt;table style=&quot;border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;&quot; border=&quot;0&quot; cellspacing=&quot;0&quot; cellpadding=&quot;0&quot; align=&quot;left&quot;&gt;\r\n&lt;tbody&gt;\r\n&lt;tr&gt;\r\n&lt;td align=&quot;left&quot; valign=&quot;middle&quot; width=&quot;16&quot;&gt;&lt;img style=&quot;width: 8px; height: 10px;&quot; src=&quot;https://cdn-emkt.webplus.com.br/institucional/01/location-icon.png&quot; alt=&quot;&quot; width=&quot;8&quot; height=&quot;10&quot; align=&quot;top&quot; /&gt;&lt;/td&gt;\r\n&lt;td style=&quot;color: #ffffff; font-size: 13px; font-family: &#039;Open Sans&#039;, Calibri, sans-serif; line-height: 20px;&quot; align=&quot;right&quot;&gt;\r\n&lt;div class=&quot;editable_text&quot; style=&quot;line-height: 20px;&quot;&gt;&lt;span class=&quot;text_container&quot;&gt;&lt;span style=&quot;color: #ffffff;&quot;&gt;www.webplus.com.br&lt;/span&gt;&lt;/span&gt;&lt;/div&gt;\r\n&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;/tbody&gt;\r\n&lt;/table&gt;\r\n&lt;table style=&quot;border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;&quot; border=&quot;0&quot; cellspacing=&quot;0&quot; cellpadding=&quot;0&quot; align=&quot;right&quot;&gt;\r\n&lt;tbody&gt;\r\n&lt;tr&gt;\r\n&lt;td align=&quot;left&quot; valign=&quot;middle&quot; width=&quot;20&quot;&gt;&lt;img style=&quot;width: 11px; height: 10px;&quot; src=&quot;https://cdn-emkt.webplus.com.br/institucional/01/tel-icon.png&quot; alt=&quot;&quot; width=&quot;11&quot; height=&quot;11&quot; align=&quot;top&quot; /&gt;&lt;/td&gt;\r\n&lt;td style=&quot;color: #ffffff; font-size: 13px; font-family: &#039;Open Sans&#039;, Calibri, sans-serif; line-height: 20px;&quot; align=&quot;right&quot;&gt;\r\n&lt;div class=&quot;editable_text&quot; style=&quot;line-height: 20px;&quot;&gt;&lt;span class=&quot;text_container&quot;&gt;11 4063-7555 | 17 3234-3500&lt;/span&gt;&lt;/div&gt;\r\n&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;/tbody&gt;\r\n&lt;/table&gt;\r\n&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;/tbody&gt;\r\n&lt;/table&gt;\r\n&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td style=&quot;font-size: 13px; line-height: 13px;&quot; height=&quot;13&quot;&gt; &lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;/tbody&gt;\r\n&lt;/table&gt;\r\n&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;/tbody&gt;\r\n&lt;/table&gt;\r\n&lt;!-- ======= end header ====== --&gt; &lt;!-- ======= header ======= --&gt;\r\n&lt;table class=&quot;bg_color&quot; style=&quot;width: 100%;&quot; border=&quot;0&quot; cellspacing=&quot;0&quot; cellpadding=&quot;0&quot; bgcolor=&quot;02638c&quot;&gt;\r\n&lt;tbody&gt;\r\n&lt;tr&gt;\r\n&lt;td&gt;\r\n&lt;table class=&quot;container590&quot; style=&quot;width: 600px;&quot; border=&quot;0&quot; cellspacing=&quot;0&quot; cellpadding=&quot;0&quot; align=&quot;center&quot;&gt;\r\n&lt;tbody&gt;\r\n&lt;tr&gt;\r\n&lt;td style=&quot;font-size: 20px; line-height: 20px;&quot; height=&quot;20&quot;&gt; &lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td align=&quot;center&quot;&gt;\r\n&lt;table class=&quot;container590&quot; style=&quot;border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;&quot; border=&quot;0&quot; cellspacing=&quot;0&quot; cellpadding=&quot;0&quot; align=&quot;left&quot;&gt;\r\n&lt;tbody&gt;\r\n&lt;tr&gt;\r\n&lt;td align=&quot;center&quot;&gt;\r\n&lt;table border=&quot;0&quot; cellspacing=&quot;0&quot; cellpadding=&quot;0&quot; align=&quot;center&quot;&gt;\r\n&lt;tbody&gt;\r\n&lt;tr&gt;&lt;!-- ======= logo ======= --&gt;\r\n&lt;td align=&quot;center&quot;&gt;&lt;a class=&quot;editable_img&quot; style=&quot;display: block; border-style: none !important; border: 0 !important;&quot; href=&quot;https://www.webplus.com.br&quot;&gt;&lt;img style=&quot;display: block; width: 180px;&quot; src=&quot;https://cdn-emkt.webplus.com.br/institucional/01/logo-webplus-cima.png&quot; alt=&quot;Webplus&quot; width=&quot;180&quot; border=&quot;0&quot; /&gt;&lt;/a&gt;&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;/tbody&gt;\r\n&lt;/table&gt;\r\n&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;/tbody&gt;\r\n&lt;/table&gt;\r\n&lt;table class=&quot;container590&quot; style=&quot;border-collapse: collapse; width: 5px;&quot; border=&quot;0&quot; cellspacing=&quot;0&quot; cellpadding=&quot;0&quot; align=&quot;left&quot;&gt;\r\n&lt;tbody&gt;\r\n&lt;tr&gt;\r\n&lt;td style=&quot;font-size: 20px; line-height: 20px;&quot; width=&quot;5&quot; height=&quot;20&quot;&gt; &lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;/tbody&gt;\r\n&lt;/table&gt;\r\n&lt;table class=&quot;container590&quot; style=&quot;border-collapse: collapse; width: 410px;&quot; border=&quot;0&quot; cellspacing=&quot;0&quot; cellpadding=&quot;0&quot; align=&quot;right&quot;&gt;\r\n&lt;tbody&gt;\r\n&lt;tr&gt;\r\n&lt;td style=&quot;font-size: 15px; line-height: 15px;&quot; height=&quot;15&quot;&gt; &lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td&gt;\r\n&lt;table class=&quot;container590&quot; style=&quot;border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;&quot; border=&quot;0&quot; cellspacing=&quot;0&quot; cellpadding=&quot;0&quot; align=&quot;left&quot;&gt;\r\n&lt;tbody&gt;\r\n&lt;tr&gt;\r\n&lt;td&gt; &lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;/tbody&gt;\r\n&lt;/table&gt;\r\n&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;/tbody&gt;\r\n&lt;/table&gt;\r\n&lt;table class=&quot;container590&quot; style=&quot;border-collapse: collapse; width: 5px;&quot; border=&quot;0&quot; cellspacing=&quot;0&quot; cellpadding=&quot;0&quot; align=&quot;left&quot;&gt;\r\n&lt;tbody&gt;\r\n&lt;tr&gt;\r\n&lt;td style=&quot;font-size: 20px; line-height: 20px;&quot; width=&quot;5&quot; height=&quot;20&quot;&gt; &lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;/tbody&gt;\r\n&lt;/table&gt;\r\n&lt;table class=&quot;container590&quot; style=&quot;border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;&quot; border=&quot;0&quot; cellspacing=&quot;0&quot; cellpadding=&quot;0&quot; align=&quot;right&quot;&gt;\r\n&lt;tbody&gt;\r\n&lt;tr&gt;\r\n&lt;td&gt;\r\n&lt;table style=&quot;border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;&quot; border=&quot;0&quot; cellspacing=&quot;0&quot; cellpadding=&quot;0&quot; align=&quot;center&quot;&gt;\r\n&lt;tbody&gt;\r\n&lt;tr&gt;\r\n&lt;td&gt;\r\n&lt;table border=&quot;0&quot; cellspacing=&quot;0&quot; cellpadding=&quot;0&quot; align=&quot;left&quot;&gt;\r\n&lt;tbody&gt;\r\n&lt;tr&gt;\r\n&lt;td style=&quot;color: #ffffff; font-size: 16px; font-family: &#039;Open Sans&#039;, Calibri, sans-serif; line-height: 20px;&quot; align=&quot;right&quot;&gt;\r\n&lt;div class=&quot;editable_text&quot; style=&quot;line-height: 20px;&quot;&gt;&lt;span class=&quot;text_container&quot;&gt;Nota Fiscal de Serviços&lt;/span&gt;&lt;/div&gt;\r\n&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;/tbody&gt;\r\n&lt;/table&gt;\r\n&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;/tbody&gt;\r\n&lt;/table&gt;\r\n&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;/tbody&gt;\r\n&lt;/table&gt;\r\n&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;/tbody&gt;\r\n&lt;/table&gt;\r\n&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td style=&quot;font-size: 15px; line-height: 15px;&quot; height=&quot;15&quot;&gt; &lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;/tbody&gt;\r\n&lt;/table&gt;\r\n&lt;!--			&lt;/td&gt;\r\n		&lt;/tr&gt;&lt;/tbody&gt;\r\n	&lt;/table&gt;--&gt; &lt;!-- ======= main section ====== --&gt;\r\n&lt;table class=&quot;bg2_color&quot; style=&quot;width: 100%;&quot; border=&quot;0&quot; cellspacing=&quot;0&quot; cellpadding=&quot;0&quot; bgcolor=&quot;f7f7f8&quot;&gt;\r\n&lt;tbody&gt;\r\n&lt;tr&gt;&lt;!--&lt;td style=&quot;background-image: url(https://cdn-emkt.webplus.com.br/cloudbackup/2015/1/main-bg.jpg); background-size: 100% 100%; background-position: center center; background-repeat: repeat;&quot; background=&quot;https://cdn-emkt.webplus.com.br/cloudbackup/2015/1/main-bg.jpg&quot; class=&quot;main-bg&quot;&gt;--&gt;&lt;/tr&gt;\r\n&lt;/tbody&gt;\r\n&lt;tbody&gt;\r\n&lt;tr&gt;\r\n&lt;td style=&quot;font-size: 40px; line-height: 40px;&quot; height=&quot;40&quot;&gt; &lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td class=&quot;section-img&quot; align=&quot;center&quot;&gt;&lt;!--&lt;a href=&quot;https://www.webplus.com.br/&quot; style=&quot;display: block; border-style: none !important; border: 0 !important;&quot; class=&quot;editable_img&quot;&gt;&lt;img src=&quot;https://cdn-emkt.webplus.com.br/emkt/2016/01-webplusabrahosting.png&quot; style=&quot;display: block; width: 600px;&quot; width=&quot;600&quot; border=&quot;0&quot; alt=&quot;section image&quot;&gt;&lt;/a&gt;--&gt;&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td class=&quot;main-header&quot; style=&quot;color: #4ea0be; font-size: 30px; font-family: &#039;Open Sans&#039;, Calibri, sans-serif; font-weight: 300; line-height: 23px;&quot; align=&quot;center&quot;&gt;&lt;!-- ======= section subtitle ====== --&gt;&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td style=&quot;font-size: 23px; line-height: 23px;&quot; height=&quot;23&quot;&gt; &lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td&gt;\r\n&lt;table border=&quot;0&quot; cellspacing=&quot;0&quot; cellpadding=&quot;0&quot; align=&quot;center&quot;&gt;\r\n&lt;tbody&gt;\r\n&lt;tr&gt;\r\n&lt;td style=&quot;color: #777b80; font-size: 14px; font-family: &#039;Open Sans&#039;, Calibri, sans-serif; line-height: 23px;&quot; align=&quot;left&quot; width=&quot;600&quot;&gt;&lt;!-- ======= section subtitle ====== --&gt;\r\n&lt;div style=&quot;line-height: 23px;&quot;&gt;Olá {$client_name},&lt;br /&gt;&lt;br /&gt;&lt;span&gt;Sua nota fiscal eletronica de serviços número &lt;span&gt;{$customvars2},  &lt;/span&gt;referente a fatura  #{$invoice_num}&lt;/span&gt;&lt;span&gt; já esta disponivel no endereço:&lt;/span&gt;&lt;br /&gt; &lt;span&gt;{$customvars}&lt;br /&gt;&lt;br /&gt;&lt;/span&gt;&lt;br /&gt; {$signature}&lt;/div&gt;\r\n&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;/tbody&gt;\r\n&lt;/table&gt;\r\n&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td style=&quot;font-size: 50px; line-height: 50px;&quot; height=&quot;50&quot;&gt; &lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;/tbody&gt;\r\n&lt;!--&lt;/td&gt;--&gt;&lt;/table&gt;\r\n&lt;!-- ======= end main section ====== --&gt; &lt;!-- ======= 3 columns products and services ======= --&gt; &lt;!-- ======= prefooter with contact info ======= --&gt;\r\n&lt;table class=&quot;bg2_color&quot; style=&quot;width: 100%;&quot; border=&quot;0&quot; cellspacing=&quot;0&quot; cellpadding=&quot;0&quot; bgcolor=&quot;f0f0f1&quot;&gt;\r\n&lt;tbody&gt;\r\n&lt;tr&gt;\r\n&lt;td style=&quot;font-size: 20px; line-height: 20px;&quot; height=&quot;20&quot;&gt; &lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td&gt;\r\n&lt;table class=&quot;container590&quot; style=&quot;width: 590px;&quot; border=&quot;0&quot; cellspacing=&quot;0&quot; cellpadding=&quot;0&quot; align=&quot;center&quot;&gt;\r\n&lt;tbody&gt;\r\n&lt;tr&gt;\r\n&lt;td&gt;\r\n&lt;table class=&quot;container590&quot; style=&quot;border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;&quot; border=&quot;0&quot; cellspacing=&quot;0&quot; cellpadding=&quot;0&quot; align=&quot;left&quot;&gt;\r\n&lt;tbody&gt;\r\n&lt;tr&gt;\r\n&lt;td&gt;\r\n&lt;table style=&quot;border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;&quot; border=&quot;0&quot; cellspacing=&quot;0&quot; cellpadding=&quot;0&quot; align=&quot;center&quot;&gt;\r\n&lt;tbody&gt;\r\n&lt;tr&gt;\r\n&lt;td align=&quot;left&quot;&gt;&lt;a class=&quot;editable_img&quot; style=&quot;display: block; border-style: none !important; border: 0 !important;&quot; href=&quot;https://www.webplus.com.br&quot;&gt;&lt;img style=&quot;display: block; width: 100px;&quot; src=&quot;https://cdn-emkt.webplus.com.br/institucional/01/logo-webplus-baixo.png&quot; alt=&quot;Webplus&quot; width=&quot;100&quot; border=&quot;0&quot; /&gt;&lt;/a&gt;&lt;/td&gt;\r\n&lt;td width=&quot;15&quot;&gt; &lt;/td&gt;\r\n&lt;td class=&quot;copy&quot; style=&quot;color: #777b80; font-size: 12px; font-family: Helvetica, Calibri, sans-serif; line-height: 22px;&quot; align=&quot;left&quot;&gt; &lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;/tbody&gt;\r\n&lt;/table&gt;\r\n&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;/tbody&gt;\r\n&lt;/table&gt;\r\n&lt;table class=&quot;container590&quot; style=&quot;border-collapse: collapse; width: 5px;&quot; border=&quot;0&quot; cellspacing=&quot;0&quot; cellpadding=&quot;0&quot; align=&quot;left&quot;&gt;\r\n&lt;tbody&gt;\r\n&lt;tr&gt;\r\n&lt;td style=&quot;font-size: 20px; line-height: 20px;&quot; width=&quot;5&quot; height=&quot;20&quot;&gt; &lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;/tbody&gt;\r\n&lt;/table&gt;\r\n&lt;table class=&quot;container590&quot; style=&quot;border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;&quot; border=&quot;0&quot; cellspacing=&quot;0&quot; cellpadding=&quot;0&quot; align=&quot;right&quot;&gt;\r\n&lt;tbody&gt;\r\n&lt;tr&gt;\r\n&lt;td style=&quot;font-size: 4px; line-height: 4px;&quot; height=&quot;4&quot;&gt; &lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td class=&quot;footer-nav&quot; style=&quot;color: #777b80; font-size: 12px; font-family: Helvetica, Calibri, sans-serif; line-height: 22px;&quot; align=&quot;center&quot;&gt;\r\n&lt;div class=&quot;editable_text&quot; style=&quot;line-height: 22px;&quot;&gt;&lt;span class=&quot;text_container&quot;&gt; © Webplus Datacenter· 2016&lt;/span&gt;&lt;/div&gt;\r\n&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;/tbody&gt;\r\n&lt;/table&gt;\r\n&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;/tbody&gt;\r\n&lt;/table&gt;\r\n&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td style=&quot;font-size: 20px; line-height: 20px;&quot; height=&quot;20&quot;&gt; &lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;/tbody&gt;\r\n&lt;/table&gt;\r\n&lt;!-- ======= end prefooter ======= --&gt; &lt;!-- ======= footer ====== --&gt;', '', '', '', 0, 1, '', '', 0, '2016-08-25 19:08:23', '2016-08-25 20:22:11'),
('invoice', 'Enviar NFSE Ginfes', '', '', '', '', '', 0, 0, 'english', '', 0, '2016-08-25 19:08:23', '2016-08-25 19:08:23'),
('general', 'Enviar Nota Fiscal de Serviços', '', '', '', '', '', 0, 0, 'english', '', 0, '2016-08-25 14:43:47', '2016-08-25 14:43:47'),
('invoice', 'Cancelar NFSE Ginfes', 'Cancelamento - Nota Fiscal Eletrônica de Serviços', '&lt;!-- Define Charset --&gt; &lt;!-- Responsive Meta Tag --&gt; Webplus - Hospedagem, E-mail, Cloud &amp;amp; Backup &lt;!-- Responsive Styles and Valid Styles --&gt;\r\n&lt;table class=&quot;main_color&quot; style=&quot;width: 100%; background-color: #5aa3bf;&quot; border=&quot;0&quot; cellspacing=&quot;0&quot; cellpadding=&quot;0&quot; bgcolor=&quot;3b8ced&quot;&gt;\r\n&lt;tbody&gt;\r\n&lt;tr&gt;\r\n&lt;td&gt;\r\n&lt;table class=&quot;container590&quot; style=&quot;width: 600px;&quot; border=&quot;0&quot; cellspacing=&quot;0&quot; cellpadding=&quot;0&quot; align=&quot;center&quot;&gt;\r\n&lt;tbody&gt;\r\n&lt;tr&gt;\r\n&lt;td style=&quot;font-size: 10px; line-height: 10px;&quot; height=&quot;10&quot;&gt; &lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td align=&quot;center&quot;&gt;\r\n&lt;table class=&quot;container590&quot; style=&quot;border-collapse: collapse; width: 600px;&quot; border=&quot;0&quot; cellspacing=&quot;0&quot; cellpadding=&quot;0&quot; align=&quot;left&quot;&gt;\r\n&lt;tbody&gt;\r\n&lt;tr&gt;\r\n&lt;td align=&quot;center&quot;&gt;\r\n&lt;table style=&quot;border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;&quot; border=&quot;0&quot; cellspacing=&quot;0&quot; cellpadding=&quot;0&quot; align=&quot;left&quot;&gt;\r\n&lt;tbody&gt;\r\n&lt;tr&gt;\r\n&lt;td align=&quot;left&quot; valign=&quot;middle&quot; width=&quot;16&quot;&gt;&lt;img style=&quot;width: 8px; height: 10px;&quot; src=&quot;https://cdn-emkt.webplus.com.br/institucional/01/location-icon.png&quot; alt=&quot;&quot; width=&quot;8&quot; height=&quot;10&quot; align=&quot;top&quot; /&gt;&lt;/td&gt;\r\n&lt;td style=&quot;color: #ffffff; font-size: 13px; font-family: &#039;Open Sans&#039;, Calibri, sans-serif; line-height: 20px;&quot; align=&quot;right&quot;&gt;\r\n&lt;div class=&quot;editable_text&quot; style=&quot;line-height: 20px;&quot;&gt;&lt;span class=&quot;text_container&quot;&gt;&lt;span style=&quot;color: #ffffff;&quot;&gt;www.webplus.com.br&lt;/span&gt;&lt;/span&gt;&lt;/div&gt;\r\n&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;/tbody&gt;\r\n&lt;/table&gt;\r\n&lt;table style=&quot;border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;&quot; border=&quot;0&quot; cellspacing=&quot;0&quot; cellpadding=&quot;0&quot; align=&quot;right&quot;&gt;\r\n&lt;tbody&gt;\r\n&lt;tr&gt;\r\n&lt;td align=&quot;left&quot; valign=&quot;middle&quot; width=&quot;20&quot;&gt;&lt;img style=&quot;width: 11px; height: 10px;&quot; src=&quot;https://cdn-emkt.webplus.com.br/institucional/01/tel-icon.png&quot; alt=&quot;&quot; width=&quot;11&quot; height=&quot;11&quot; align=&quot;top&quot; /&gt;&lt;/td&gt;\r\n&lt;td style=&quot;color: #ffffff; font-size: 13px; font-family: &#039;Open Sans&#039;, Calibri, sans-serif; line-height: 20px;&quot; align=&quot;right&quot;&gt;\r\n&lt;div class=&quot;editable_text&quot; style=&quot;line-height: 20px;&quot;&gt;&lt;span class=&quot;text_container&quot;&gt;11 4063-7555 | 17 3234-3500&lt;/span&gt;&lt;/div&gt;\r\n&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;/tbody&gt;\r\n&lt;/table&gt;\r\n&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;/tbody&gt;\r\n&lt;/table&gt;\r\n&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td style=&quot;font-size: 13px; line-height: 13px;&quot; height=&quot;13&quot;&gt; &lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;/tbody&gt;\r\n&lt;/table&gt;\r\n&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;/tbody&gt;\r\n&lt;/table&gt;\r\n&lt;!-- ======= end header ====== --&gt; &lt;!-- ======= header ======= --&gt;\r\n&lt;table class=&quot;bg_color&quot; style=&quot;width: 100%;&quot; border=&quot;0&quot; cellspacing=&quot;0&quot; cellpadding=&quot;0&quot; bgcolor=&quot;02638c&quot;&gt;\r\n&lt;tbody&gt;\r\n&lt;tr&gt;\r\n&lt;td&gt;\r\n&lt;table class=&quot;container590&quot; style=&quot;width: 600px;&quot; border=&quot;0&quot; cellspacing=&quot;0&quot; cellpadding=&quot;0&quot; align=&quot;center&quot;&gt;\r\n&lt;tbody&gt;\r\n&lt;tr&gt;\r\n&lt;td style=&quot;font-size: 20px; line-height: 20px;&quot; height=&quot;20&quot;&gt; &lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td align=&quot;center&quot;&gt;\r\n&lt;table class=&quot;container590&quot; style=&quot;border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;&quot; border=&quot;0&quot; cellspacing=&quot;0&quot; cellpadding=&quot;0&quot; align=&quot;left&quot;&gt;\r\n&lt;tbody&gt;\r\n&lt;tr&gt;\r\n&lt;td align=&quot;center&quot;&gt;\r\n&lt;table border=&quot;0&quot; cellspacing=&quot;0&quot; cellpadding=&quot;0&quot; align=&quot;center&quot;&gt;\r\n&lt;tbody&gt;\r\n&lt;tr&gt;&lt;!-- ======= logo ======= --&gt;\r\n&lt;td align=&quot;center&quot;&gt;&lt;a class=&quot;editable_img&quot; style=&quot;display: block; border-style: none !important; border: 0 !important;&quot; href=&quot;https://www.webplus.com.br&quot;&gt;&lt;img style=&quot;display: block; width: 180px;&quot; src=&quot;https://cdn-emkt.webplus.com.br/institucional/01/logo-webplus-cima.png&quot; alt=&quot;Webplus&quot; width=&quot;180&quot; border=&quot;0&quot; /&gt;&lt;/a&gt;&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;/tbody&gt;\r\n&lt;/table&gt;\r\n&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;/tbody&gt;\r\n&lt;/table&gt;\r\n&lt;table class=&quot;container590&quot; style=&quot;border-collapse: collapse; width: 5px;&quot; border=&quot;0&quot; cellspacing=&quot;0&quot; cellpadding=&quot;0&quot; align=&quot;left&quot;&gt;\r\n&lt;tbody&gt;\r\n&lt;tr&gt;\r\n&lt;td style=&quot;font-size: 20px; line-height: 20px;&quot; width=&quot;5&quot; height=&quot;20&quot;&gt; &lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;/tbody&gt;\r\n&lt;/table&gt;\r\n&lt;table class=&quot;container590&quot; style=&quot;border-collapse: collapse; width: 410px;&quot; border=&quot;0&quot; cellspacing=&quot;0&quot; cellpadding=&quot;0&quot; align=&quot;right&quot;&gt;\r\n&lt;tbody&gt;\r\n&lt;tr&gt;\r\n&lt;td style=&quot;font-size: 15px; line-height: 15px;&quot; height=&quot;15&quot;&gt; &lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td&gt;\r\n&lt;table class=&quot;container590&quot; style=&quot;border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;&quot; border=&quot;0&quot; cellspacing=&quot;0&quot; cellpadding=&quot;0&quot; align=&quot;left&quot;&gt;\r\n&lt;tbody&gt;\r\n&lt;tr&gt;\r\n&lt;td&gt; &lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;/tbody&gt;\r\n&lt;/table&gt;\r\n&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;/tbody&gt;\r\n&lt;/table&gt;\r\n&lt;table class=&quot;container590&quot; style=&quot;border-collapse: collapse; width: 5px;&quot; border=&quot;0&quot; cellspacing=&quot;0&quot; cellpadding=&quot;0&quot; align=&quot;left&quot;&gt;\r\n&lt;tbody&gt;\r\n&lt;tr&gt;\r\n&lt;td style=&quot;font-size: 20px; line-height: 20px;&quot; width=&quot;5&quot; height=&quot;20&quot;&gt; &lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;/tbody&gt;\r\n&lt;/table&gt;\r\n&lt;table class=&quot;container590&quot; style=&quot;border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;&quot; border=&quot;0&quot; cellspacing=&quot;0&quot; cellpadding=&quot;0&quot; align=&quot;right&quot;&gt;\r\n&lt;tbody&gt;\r\n&lt;tr&gt;\r\n&lt;td&gt;\r\n&lt;table style=&quot;border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;&quot; border=&quot;0&quot; cellspacing=&quot;0&quot; cellpadding=&quot;0&quot; align=&quot;center&quot;&gt;\r\n&lt;tbody&gt;\r\n&lt;tr&gt;\r\n&lt;td&gt;\r\n&lt;table border=&quot;0&quot; cellspacing=&quot;0&quot; cellpadding=&quot;0&quot; align=&quot;left&quot;&gt;\r\n&lt;tbody&gt;\r\n&lt;tr&gt;\r\n&lt;td style=&quot;color: #ffffff; font-size: 16px; font-family: &#039;Open Sans&#039;, Calibri, sans-serif; line-height: 20px;&quot; align=&quot;right&quot;&gt;\r\n&lt;div class=&quot;editable_text&quot; style=&quot;line-height: 20px;&quot;&gt;&lt;span class=&quot;text_container&quot;&gt;Nota Fiscal de Serviços&lt;/span&gt;&lt;/div&gt;\r\n&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;/tbody&gt;\r\n&lt;/table&gt;\r\n&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;/tbody&gt;\r\n&lt;/table&gt;\r\n&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;/tbody&gt;\r\n&lt;/table&gt;\r\n&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;/tbody&gt;\r\n&lt;/table&gt;\r\n&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td style=&quot;font-size: 15px; line-height: 15px;&quot; height=&quot;15&quot;&gt; &lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;/tbody&gt;\r\n&lt;/table&gt;\r\n&lt;!--			&lt;/td&gt;\r\n		&lt;/tr&gt;&lt;/tbody&gt;\r\n	&lt;/table&gt;--&gt; &lt;!-- ======= main section ====== --&gt;\r\n&lt;table class=&quot;bg2_color&quot; style=&quot;width: 100%;&quot; border=&quot;0&quot; cellspacing=&quot;0&quot; cellpadding=&quot;0&quot; bgcolor=&quot;f7f7f8&quot;&gt;\r\n&lt;tbody&gt;\r\n&lt;tr&gt;&lt;!--&lt;td style=&quot;background-image: url(https://cdn-emkt.webplus.com.br/cloudbackup/2015/1/main-bg.jpg); background-size: 100% 100%; background-position: center center; background-repeat: repeat;&quot; background=&quot;https://cdn-emkt.webplus.com.br/cloudbackup/2015/1/main-bg.jpg&quot; class=&quot;main-bg&quot;&gt;--&gt;&lt;/tr&gt;\r\n&lt;/tbody&gt;\r\n&lt;tbody&gt;\r\n&lt;tr&gt;\r\n&lt;td style=&quot;font-size: 40px; line-height: 40px;&quot; height=&quot;40&quot;&gt; &lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td class=&quot;section-img&quot; align=&quot;center&quot;&gt;&lt;!--&lt;a href=&quot;https://www.webplus.com.br/&quot; style=&quot;display: block; border-style: none !important; border: 0 !important;&quot; class=&quot;editable_img&quot;&gt;&lt;img src=&quot;https://cdn-emkt.webplus.com.br/emkt/2016/01-webplusabrahosting.png&quot; style=&quot;display: block; width: 600px;&quot; width=&quot;600&quot; border=&quot;0&quot; alt=&quot;section image&quot;&gt;&lt;/a&gt;--&gt;&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td class=&quot;main-header&quot; style=&quot;color: #4ea0be; font-size: 30px; font-family: &#039;Open Sans&#039;, Calibri, sans-serif; font-weight: 300; line-height: 23px;&quot; align=&quot;center&quot;&gt;&lt;!-- ======= section subtitle ====== --&gt;&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td style=&quot;font-size: 23px; line-height: 23px;&quot; height=&quot;23&quot;&gt; &lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td&gt;\r\n&lt;table border=&quot;0&quot; cellspacing=&quot;0&quot; cellpadding=&quot;0&quot; align=&quot;center&quot;&gt;\r\n&lt;tbody&gt;\r\n&lt;tr&gt;\r\n&lt;td style=&quot;color: #777b80; font-size: 14px; font-family: &#039;Open Sans&#039;, Calibri, sans-serif; line-height: 23px;&quot; align=&quot;left&quot; width=&quot;600&quot;&gt;&lt;!-- ======= section subtitle ====== --&gt;\r\n&lt;div style=&quot;line-height: 23px;&quot;&gt;Olá {$client_name},&lt;br /&gt;&lt;br /&gt;&lt;span&gt;A nota fiscal eletronica de serviços número  &lt;span&gt;{$customvars2},  &lt;/span&gt;referente a fatura  #{$invoice_num}&lt;/span&gt;&lt;span&gt; foi CANCELADA e esta disponivel  para consulta no endereço:&lt;/span&gt;&lt;br /&gt; &lt;span&gt;{$customvars}&lt;br /&gt;&lt;/span&gt;&lt;br /&gt; {$signature}&lt;/div&gt;\r\n&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;/tbody&gt;\r\n&lt;/table&gt;\r\n&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td style=&quot;font-size: 50px; line-height: 50px;&quot; height=&quot;50&quot;&gt; &lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;/tbody&gt;\r\n&lt;!--&lt;/td&gt;--&gt;&lt;/table&gt;\r\n&lt;!-- ======= end main section ====== --&gt; &lt;!-- ======= 3 columns products and services ======= --&gt; &lt;!-- ======= prefooter with contact info ======= --&gt;\r\n&lt;table class=&quot;bg2_color&quot; style=&quot;width: 100%;&quot; border=&quot;0&quot; cellspacing=&quot;0&quot; cellpadding=&quot;0&quot; bgcolor=&quot;f0f0f1&quot;&gt;\r\n&lt;tbody&gt;\r\n&lt;tr&gt;\r\n&lt;td style=&quot;font-size: 20px; line-height: 20px;&quot; height=&quot;20&quot;&gt; &lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td&gt;\r\n&lt;table class=&quot;container590&quot; style=&quot;width: 590px;&quot; border=&quot;0&quot; cellspacing=&quot;0&quot; cellpadding=&quot;0&quot; align=&quot;center&quot;&gt;\r\n&lt;tbody&gt;\r\n&lt;tr&gt;\r\n&lt;td&gt;\r\n&lt;table class=&quot;container590&quot; style=&quot;border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;&quot; border=&quot;0&quot; cellspacing=&quot;0&quot; cellpadding=&quot;0&quot; align=&quot;left&quot;&gt;\r\n&lt;tbody&gt;\r\n&lt;tr&gt;\r\n&lt;td&gt;\r\n&lt;table style=&quot;border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;&quot; border=&quot;0&quot; cellspacing=&quot;0&quot; cellpadding=&quot;0&quot; align=&quot;center&quot;&gt;\r\n&lt;tbody&gt;\r\n&lt;tr&gt;\r\n&lt;td align=&quot;left&quot;&gt;&lt;a class=&quot;editable_img&quot; style=&quot;display: block; border-style: none !important; border: 0 !important;&quot; href=&quot;https://www.webplus.com.br&quot;&gt;&lt;img style=&quot;display: block; width: 100px;&quot; src=&quot;https://cdn-emkt.webplus.com.br/institucional/01/logo-webplus-baixo.png&quot; alt=&quot;Webplus&quot; width=&quot;100&quot; border=&quot;0&quot; /&gt;&lt;/a&gt;&lt;/td&gt;\r\n&lt;td width=&quot;15&quot;&gt; &lt;/td&gt;\r\n&lt;td class=&quot;copy&quot; style=&quot;color: #777b80; font-size: 12px; font-family: Helvetica, Calibri, sans-serif; line-height: 22px;&quot; align=&quot;left&quot;&gt; &lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;/tbody&gt;\r\n&lt;/table&gt;\r\n&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;/tbody&gt;\r\n&lt;/table&gt;\r\n&lt;table class=&quot;container590&quot; style=&quot;border-collapse: collapse; width: 5px;&quot; border=&quot;0&quot; cellspacing=&quot;0&quot; cellpadding=&quot;0&quot; align=&quot;left&quot;&gt;\r\n&lt;tbody&gt;\r\n&lt;tr&gt;\r\n&lt;td style=&quot;font-size: 20px; line-height: 20px;&quot; width=&quot;5&quot; height=&quot;20&quot;&gt; &lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;/tbody&gt;\r\n&lt;/table&gt;\r\n&lt;table class=&quot;container590&quot; style=&quot;border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;&quot; border=&quot;0&quot; cellspacing=&quot;0&quot; cellpadding=&quot;0&quot; align=&quot;right&quot;&gt;\r\n&lt;tbody&gt;\r\n&lt;tr&gt;\r\n&lt;td style=&quot;font-size: 4px; line-height: 4px;&quot; height=&quot;4&quot;&gt; &lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td class=&quot;footer-nav&quot; style=&quot;color: #777b80; font-size: 12px; font-family: Helvetica, Calibri, sans-serif; line-height: 22px;&quot; align=&quot;center&quot;&gt;\r\n&lt;div class=&quot;editable_text&quot; style=&quot;line-height: 22px;&quot;&gt;&lt;span class=&quot;text_container&quot;&gt; © Webplus Datacenter· 2016&lt;/span&gt;&lt;/div&gt;\r\n&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;/tbody&gt;\r\n&lt;/table&gt;\r\n&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;/tbody&gt;\r\n&lt;/table&gt;\r\n&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td style=&quot;font-size: 20px; line-height: 20px;&quot; height=&quot;20&quot;&gt; &lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;/tbody&gt;\r\n&lt;/table&gt;\r\n&lt;!-- ======= end prefooter ======= --&gt; &lt;!-- ======= footer ====== --&gt;', '', '', '', 0, 1, '', '', 0, '2016-08-25 20:11:12', '2016-08-25 20:28:03'),
('invoice', 'Cancelar NFSE Ginfes', '', '', '', '', '', 0, 0, 'english', '', 0, '2016-08-25 20:14:14', '2016-08-25 20:14:14');
";
	 $result2 = full_query($query2);*/
}

function nfse_ginfes_addon_deactivate() {
	global $attachments_dir;
	$query1 = "RENAME TABLE  `nfseGinfes` TO  `nfseGinfes_bkp_".date('Ymd-His')."`";
    $result0 = full_query($query1); 
}

function nfse_ginfes_addon_upgrade($vars) {

	$version = $vars['version'];
	full_query("UPDATE `mod_nfse_ginfesSettings` SET `value` = '".currentnfse_ginfesVersion."' WHERE `name` = 'version' ");

	if ($version < 1.0) {
		//$query = "CREATE TABLE `mod_nfse_ginfesSettings` (`id` int(10) NOT NULL AUTO_INCREMENT, `name` TEXT NOT NULL, `value` TEXT NOT NULL, PRIMARY KEY (`id`))";
		//$result = full_query($query);
	}

}

function nfse_ginfes_internal_permissions(){
	//$parray = array();
	//$data = get_query_vals("tbladmins", "tbladminroles.widgets,tbladmins.roleid,tbladmins.disabled", array("tbladmins.id" => $_SESSION['adminid']), "", "", "", "tbladminroles ON tbladminroles.id=tbladmins.roleid");
	//if (!empty($data) || $data['disabled'] != "0"){
	//	$adminPermissions = localAPI("getadmindetails");
	//	if ($adminPermissions['result'] === "success"){
	//		$parray["widgets"] = explode(',', $data['widgets']);
	//		$parray["permissions"] = explode(',', $adminPermissions['allowedpermissions']);
	//	}
	//}
	//return $parray;
}



function nfse_ginfes_addon_output($vars) {
  require_once __DIR__ . '/../../../init.php';
  require_once __DIR__ . '/../../../includes/gatewayfunctions.php';
  require_once __DIR__ . '/../../../includes/invoicefunctions.php';
	
	$form = "<form id='form_1153525' class='appnitro'  method='post' action='".$attachments_dir."/nfse_ginfes/nfse/form_prestadoor.php'>
		<li id='li_4' >
		<label class='description' for='element_4'>Razão Social</label>
		<div>
			<input id='rs' name='rs' class='element text medium' type='text' maxlength='255' value='".$vars["razaoSocial"]."'/> 
		</div> 
		</li>	
			<li id='li_5' >
			<label class='description' for='element_5'>CNPJ (Somente Números)</label>
			<div>
				<input id='cnpj' name='cnpj' class='element text medium' type='text' maxlength='255' value='".$vars["cnpj"]."'/> 
			</div> 
		</li>		
		<li id='li_5' >
			<label class='description' for='element_5'>Inscrição Estadual</label>
			<div>
				<input id='ie' name='ie' class='element text medium' type='text' maxlength='255' value='".$vars["inscricaoMunicipal"]."'/> 
			</div> 
		</li>		
		<li id='li_5' >
		<label class='description' for='element_5'>Código do serviço (ex. 13.0.1) </label>
		<div>
			<input id='cs' name='cs' class='element text medium' type='text' maxlength='255' value='".$vars["codigoServico"]."'/> 
		</div> 
		</li>		<li id='li_6' >
		<label class='description' for='element_6'>Código da atividade </label>
		<div>
			<input id='ca' name='ca' class='element text medium' type='text' maxlength='255' value='".$vars["codigoAtividade"]."'/> 
		</div> 
		</li>		<li id='li_7' >
		<label class='description' for='element_7'>Aliquota Municipal ( Somente Números ) </label>
		<div>
			<input id='am' name='am' class='element text medium' type='text' maxlength='255' value='".$vars["aliquotaMunicipal"]."'/> 
		</div> 
		</li>		<li id='li_8' >
		<label class='description' for='element_8'>Aliquota Estadual ( Somente Números ) </label>
		<div>
			<input id='ae' name='ae' class='element text medium' type='text' maxlength='255' value='".$vars["aliquotaEstadual"]."'/> 
		</div> 
		</li>		<li id='li_9' >
		<label class='description' for='element_9'>Fonte de pesquisa (Ex. SEBRAE) </label>
		<div>
			<input id='fp' name='fp' class='element text medium' type='text' maxlength='255' value='".$vars["fonte"]."'/> 
		</div> 
		</li>
		</ul>
		</form>";
		
	  echo $form;
	/*$form = '
  <div class="callout callout-success">
  <h4>NFSE Ginfes</h4>
  Addon instalado com sucesso.
  </div>
  <fieldset>
  <legend>Gerar Notas</legend>

  <form action="" method="post" enctype="multipart/form-data" class="form-horizontal">
  <!--div class="form-group" >
  <label for="teste" class="col-sm-2 control-label">Digite um texto</label>
  <div class="col-sm-10">
  <input type="text" class="form-control" id="teste" name="teste" placeholder="Texto Teste">
  </div>
  </div>
  <div class="form-group">
  <label for="ret" class="col-sm-2 control-label">Arquivo de Retorno</label>
  <div class="col-sm-10">
  	<input type="file" name="ret[]" id="ret" multiple="multiple" accept=".ret" >
  </div>
  </div-->
  <div class="form-group">
  <div class="col-sm-offset-2 col-sm-10">
  	<button type="submit" class="btn btn-default" name="gerarNotas" value="1"><i class="fa fa-refresh"></i> Gerar Notas</button>
  </div>
  </div>
  </form>
  </fieldset>
  ';
  global $attachments_dir;
  echo $form;
  /// CLicou em Gerar Notas
  if(isset($_POST['gerarNotas']) && $_POST['gerarNotas'] == '1'){
    echo pre_('Voce clicou em Gerar Notas!!!!');
  }
  /// POSTAR RET
  if(isset($_POST['enviar']) && $_POST['enviar'] == '1'){
  	$files = normalize_files_array($_FILES);
  	$debug = '';
  	if ($files['ret'][0]['name'] != '') {
  		foreach ($files['ret'] as $k=>$v) {
  			$debug .= '### LENDO ARQUIVO "'.$v['name']."\" ###\n";
  			if ($v['error'] != 0) {
  				$debug .= '#### Erro ao enviar arquivo '.$v['name']."\n";
  				//$mv
  			} else {
  				$datetime = date('Ymd-His');
  				$newFileLocation=$attachments_dir.'/nfse_ginfes_retornos/'.$datetime.'-'.$v['name'];
  				$fileid = md5(uniqid(md5(time()).rand(),1));
  				rename($v['tmp_name'],$newFileLocation);
  				$retorno = lerRetorno($newFileLocation);

  				foreach($retorno['data'] as $rd) {
  					$resultLine = select_query('mod_nfse_ginfesretornos', '', array('AllLine' => $rd['AllLine']));//."\n";
  					$existLine = mysql_fetch_array($resultLine);

  					if(!$existLine)  {
  						$debug .= ' - Importando linha '.$rd['Line']."\n";
  						$rd['FileId']=$fileid;
  						$lid = gravarRetorno($rd);
  						$rd['retornoid'] = $lid;

  						// DAR BAIXA

  						if (array_key_exists('first',$rd) || array_key_exists('last',$rd)) ;
  						else {
  							$ValorDoTitulo = floatval(substr($rd['ValorDoTitulo'], 0, -2).'.'.substr($rd['ValorDoTitulo'], -2));
  							$ValorPrincipal = floatval(substr($rd['ValorPrincipal'], 0, -2).'.'.substr($rd['ValorPrincipal'], -2));
  							$taxaBoleto = floatval(substr($rd['TarifaDeCobranca'], 0, -2).'.'.substr($rd['TarifaDeCobranca'], -2));

  							$nossoNumero = intval($rd['NossoNumero']);

  							$debug .= ' --- Procurando invoice para NOSSO NUMERO: '.$lid.'>'.$nossoNumero.' Valor: '.$ValorDoTitulo."\n";
  							$resultLine2 = select_query('mod_nfse_ginfesboletos', '', array('pedido' => $nossoNumero));//."\n";
  							$existLine2 = mysql_fetch_array($resultLine2);
  							if($existLine2 && $existLine2['invoiceid'] ) {
  								//updateInvoiceTotal($existLine2['invoiceid']);
  								full_query('UPDATE mod_nfse_ginfesretornos SET invoiceid='.$existLine2['invoiceid'].' WHERE retornoid='.$rd['retornoid']);
  								$invDet = getInvoceDetails($existLine2['invoiceid']);
  								$debug .= ' --- Invoice encontrado ID: '.$existLine2['invoiceid'].' Valor: '.$invDet['balance']."\n";
  								//// UPDATE INVOICE ID NA TABELA RETORNO
  								//$debug .= ($invDet['balance'])."\n";
  								//$debug .= ($ValorDoTitulo)."\n";
  								// SE valor pago > valor de produtos s/ juros do addon tarifajuros
  								// E valor do titulo < valor da fatura
  								// CONCEDER DESCONTO
  								if($ValorDoTitulo>=$invDet['balance2'] && $ValorDoTitulo<$invDet['balance'] ){
  									$desconto = round((($invDet['balance']-$ValorDoTitulo)*(-1)),2);
  									full_query("INSERT INTO tblinvoiceitems (invoiceid,userid,type,description,amount,paymentmethod) VALUES (".$existLine2['invoiceid'].",".$invDet['userid'].",'nfse_ginfes_disccount_dalay','Desconto por atraso na compensação do boleto', ".$desconto.", 'nfse_ginfes')");
  									updateInvoiceTotal($existLine2['invoiceid']);
  									$debug .= ' --- Concedendo desconto por atraso de compensação de :'.$desconto."\n";
  								}
  								//$invDet = getInvoceDetails($existLine2['invoiceid']);
  								//$debug .= var_export($invDet,1)."\n";
  								logTransaction('Itau ShopLine', $rd, 'Success');
  								addInvoicePayment($existLine2['invoiceid'],$lid,$ValorDoTitulo,$taxaBoleto,'nfse_ginfes');
  								$debug .= ' --- Dando baixa no invoice '.$existLine2['invoiceid']."\n";
  							} else $debug .= ' --- !!! Boleto não encontrado !!!'."\n"; 								//echo $nossoNumero;
  							//$invoiceId = checkCbInvoiceID($nossoNumero, 'Itau ShopLine');
  							//checkCbTransID($transactionId);
  							//if($nossoNumero >= 90000000) {
  							//	$resultLine2 = select_query('mod_nfse_ginfesboletos', '', array('idboleto' => $nossoNumero));//."\n";
  							//	$existLine2 = mysql_fetch_array($resultLine2);
  							//	if($existLine2 && $existLine2['invoiceid'] ) {
  							//		logTransaction('Itau ShopLine', $rd, 'Success');
  							//		addInvoicePayment($existLine2['invoiceid'],$lid,$valor,0,'Itau ShopLine');
  							//	} else $debug .= ' --- !!! Boleto não encontrado !!!'."\n";
  							//} else {
  							//	logTransaction('Itau ShopLine', $rd, 'Success');
  							//	addInvoicePayment($existLine2['invoiceid'],$lid,$valor,0,'Itau ShopLine');
  							//}
  						}
  					} else $debug .= ' --- !!! Linha já importada !!!'."\n";
  				}
  				$fileid = '';
  			}
  			echo pre($debug);$debug = '';
  		}

  	} else {
  		$debug = pre("Nenhum arquivo enviado;");
  	}

  }


*/ 
  //echo pre(var_export(get_invoice(50016),1));
  //echo getLastImports_(4);
 // echo print_r(var_export($vars,1));


}
/*
function getLastImports_($qto){
	global $attachments_dir;
	$tret=full_query("
	SELECT
		FileId,
		retornoid,
		dataehora,
		substr(replace(m.`File`,'".$attachments_dir.'/nfse_ginfes_retornos/'."',''),17) as file
	FROM
		mod_nfse_ginfesretornos m
	WHERE
		m.`TipoRegistro` <> ''
	GROUP BY
		m.`FileId`
	");
	$trow= mysql_num_rows($tret);
	$tpag = ceil($trow/$qto);
	$paga=(isset($_GET['pag']) && $_GET['pag'] != '')?$_GET['pag']:0;
	$pagaq=($paga == 0)?0:$_GET['pag']*$qto;
	//$paga=(isset($_GET['ret']) && $_GET['ret'] != '')?$_GET['ret']:false;

	parse_str(html_entity_decode($_SERVER['QUERY_STRING']), $qsr);

	$qsrn=$qsr;
	$qsrp=$qsr;
	$qsrn['pag']=(($paga+1)!=$tpag && $tpag>1)?$paga+1:false;
	$qsrp['pag']=(($paga)>0 && $tpag>1)?$paga-1:false;
	if(array_key_exists('ret',$qsrn)) unset($qsrn['ret']);
	if(array_key_exists('ret',$qsrp)) unset($qsrp['ret']);
	$urln=$_SERVER['SCRIPT_URL'].'?'.http_build_query($qsrn);
	$urlp=$_SERVER['SCRIPT_URL'].'?'.http_build_query($qsrp);

	$listaUltimos = '<fieldset>
		<legend>Arquivos Importados ('.$trow.')</legend>
		<table class="table table-hover table-bordered" style="background-color:#FFF;">
			<tr>
				<th>Arquivo</th>
				<th>Data</th>
				<th>Linhas V&aacute;lidas</th>
				<th>Baixas</th>
			</tr>
';
	$ultimos = full_query("
	SELECT
		FileId,
		min(retornoid) as retornoid,
		dataehora,
		substr(replace(m.`File`,'".$attachments_dir.'/nfse_ginfes_retornos/'."',''),17) as file,
		count(FileId) as total,
		sum(if(invoiceid != '', 1, 0)) as tInvoice
	FROM
		mod_nfse_ginfesretornos m
	WHERE
		m.`TipoRegistro` <> ''
	GROUP BY
		m.`FileId`
	ORDER BY
		m.`File` desc
	LIMIT ".$pagaq.','.$qto.";
	");
	echo "LIMIT ".$pagaq.','.$qto."";

	while($ultimo = mysql_fetch_array($ultimos)){
		$qsr['ret']=$ultimo['FileId'];
		$listaUltimos .= "		<tr>
				<td><a href='".$_SERVER['SCRIPT_URL'].'?'.http_build_query($qsr)."'><i class='fa fa-file-text'></i> ".$ultimo['file']."</a></td>
				<td>".date('d/m/Y H:i:s',strtotime($ultimo['dataehora']))."</td>
				<td>".$ultimo['total']."</td>
				<td>".$ultimo['tInvoice']."</td>
			</tr>
";
	}
	$listaUltimos .= '			<tr>
				<td colspan="4" align="center">';
	$listaUltimos .= ($qsrp['pag']!==false)?'<a href="'.$urlp.'"><i class="fa fa-angle-left"></i> Anterior</a>&nbsp;&nbsp;':'<i class="fa fa-angle-left"></i> Anterior&nbsp;&nbsp;';
	$listaUltimos .= ' P&aacute;gina '.($paga+1).' de '.$tpag;
	$listaUltimos .= ($qsrn['pag']!==false)?'&nbsp;&nbsp;<a href="'.$urln.'">Pr&oacute;xima <i class="fa fa-angle-right"></i></a>':'&nbsp;&nbsp;Pr&oacute;xima <i class="fa fa-angle-right"></i>';
	$listaUltimos .= '</td>
			</tr>
';
	$listaUltimos .= '	</table>
	</fieldset>';
	return $listaUltimos;
}
function getImportsDetails_($id){
	global $attachments_dir;
	parse_str(html_entity_decode($_SERVER['QUERY_STRING']), $qsr);
	if(array_key_exists('ret',$qsr)) unset($qsr['ret']);
	$close = $_SERVER['SCRIPT_URL'].'?'.http_build_query($qsr);
	$listaUltimosl = '';
	$ret = full_query("SELECT *,substr(replace(m.`File`,'".$attachments_dir.'/nfse_ginfes_retornos/'."',''),17) as arq FROM mod_nfse_ginfesretornos m WHERE m.TipoRegistro<>'' AND m.FileId = '".$id."' ORDER BY line, NumeroSequencial;");
	$total = 0;
	$totalt = 0;
	while($retl = mysql_fetch_array($ret)){
		$arq=$retl['arq'];
		$comp=($retl['invoiceid']!='')?'<i class="fa fa-check-square-o"></i>':'<i class="fa fa-square-o"></i>';
		$datavenc=($retl['DataVencimento']!='000000')?date('d/m/Y',strtotime(substr($retl['DataVencimento'], 4, 2).'-'.substr($retl['DataVencimento'], 2, 2).'-'.substr($retl['DataVencimento'], 0, 2).' 00:00:00')):'-';
		$dataoc =($retl['DataDeOcorrencia']!='000000')?date('d/m/Y',strtotime(substr($retl['DataDeOcorrencia'], 4, 2).'-'.substr($retl['DataDeOcorrencia'], 2, 2).'-'.substr($retl['DataDeOcorrencia'], 0, 2).' 00:00:00')):'-';
		$listaUltimosl .= "		<tr>
				<td>".$retl['NossoNumero']."</td>
				<td><a href='invoices.php?action=edit&id=".$retl['invoiceid']."'>".$retl['invoiceid']."</a></td>
				<td>".$datavenc."</td>
				<td>".$dataoc."</td>
				<td>".$retl['NomeDoSacado']."</td>
				<td>R$ " . number_format(floatval(substr($retl['ValorDoTitulo'], 0, -2).'.'.substr($retl['ValorDoTitulo'], -2)), 2, ',', '.')."</td>
				<td>".$retl['Line']."</td>
				<td>".$comp."</td>
			</tr>
";
		$total += floatval(substr($retl['ValorDoTitulo'], 0, -2).'.'.substr($retl['ValorDoTitulo'], -2));
		$totalt += floatval(substr($retl['TarifaDeCobranca'], 0, -2).'.'.substr($retl['TarifaDeCobranca'], -2));
	}
	$listaUltimosd = '<fieldset>
		<legend>Retorno <i class="fa fa-file-text"></i> '.$arq.' <a href='.$close.'><i class="fa fa-close"></i></a></legend>
		<table class="table table-hover table-bordered" style="background-color:#FFF;">
			<tr>
				<th>N&ordm; do doc.</th>
				<th>Invoice</th>
				<th>Data de Vencimento</th>
				<th>Data de Pagamento</th>
				<th>Nome do Sacado</th>
				<th>Valor do t&iacute;tulo</th>
				<th>Linha</th>
				<th>Baixa Inv.</th>
			</tr>
';
	$listaUltimosd .= $listaUltimosl;
	$listaUltimosd .= '<fieldset>
			<tr>
				<td colspan="8"></td>
			</tr>
			<tr>
				<td colspan="5" align="right">Total Pago: </td>
				<td><strong>R$ '. number_format($total, 2, ',', '.').'</strong></td>
				<td colspan="2"></td>
			</tr>
			<tr>
				<td colspan="5" align="right">Total de Tarifas: </td>
				<td><strong>R$ '. number_format($totalt, 2, ',', '.').'</strong></td>
				<td colspan="2"></td>
			</tr>
			<tr>
				<td colspan="5" align="right">Total a Receber: </td>
				<td><strong>R$ '. number_format(($total-$totalt), 2, ',', '.').'</strong></td>
				<td colspan="2"></td>
			<tr>
';
	$listaUltimosd .= '	</table>
	</fieldset>';
	return $listaUltimosd;
}
function gravarRetorno_($rd) {

	//$nada = ('INSERT INTO `mod_nfse_ginfesretornos` (
	full_query('INSERT INTO `mod_nfse_ginfesretornos` (
	`FileId`
	,`File`
	,`Line`
	,`AllLine`
	,`TipoRegistro`
	,`CodigoInscricao`
	,`NumeroInscricao`
	,`Agencia`
	,`Zeros01`
	,`Conta`
	,`Dac`
	,`Brancos01`
	,`UsoDaEmpresa`
	,`NossoNumero`
	,`Brancos02`
	,`NumCarteira`
	,`NossoNumero2`
	,`DacNossoNumero`
	,`Brancos03`
	,`CodCarteira`
	,`CodigoDeOcorrencia`
	,`DataDeOcorrencia`
	,`NumeroDocumento`
	,`NossoNumero3`
	,`Brancos04`
	,`DataVencimento`
	,`ValorDoTitulo`
	,`CodigoDoBanco`
	,`AgenciaCobradora`
	,`DacAgenciaCobradora`
	,`Especie`
	,`TarifaDeCobranca`
	,`Brancos05`
	,`ValorDoIOF`
	,`ValorAbatimento`
	,`Descontos`
	,`ValorPrincipal`
	,`JurosDeMoraMulta`
	,`OutrosCreditos`
	,`BoletoDDA`
	,`Brancos06`
	,`DataCredito`
	,`InstrucaoCancelada`
	,`Brancos07`
	,`Zeros02`
	,`NomeDoSacado`
	,`Brancos08`
	,`Erros`
	,`Brancos09`
	,`CodigoDeLiquidacao`
	,`NumeroSequencial`
	,`dataehora`
	,`invoiceid`
	)
	VALUES
	(
	\''.$rd['FileId']					.'\',
	\''.$rd['File']						.'\',
	\''.$rd['Line']						.'\',
	\''.$rd['AllLine']					.'\',
	\''.$rd['TipoRegistro']				.'\',
	\''.$rd['CodigoInscricao']			.'\',
	\''.$rd['NumeroInscricao']			.'\',
	\''.$rd['Agencia']					.'\',
	\''.$rd['Zeros01']					.'\',
	\''.$rd['Conta']					.'\',
	\''.$rd['Dac']						.'\',
	\''.$rd['Brancos01']				.'\',
	\''.$rd['UsoDaEmpresa']				.'\',
	\''.$rd['NossoNumero']				.'\',
	\''.$rd['Brancos02']				.'\',
	\''.$rd['NumCarteira']				.'\',
	\''.$rd['NossoNumero2']				.'\',
	\''.$rd['DacNossoNumero']			.'\',
	\''.$rd['Brancos03']				.'\',
	\''.$rd['CodCarteira']				.'\',
	\''.$rd['CodigoDeOcorrencia']		.'\',
	\''.$rd['DataDeOcorrencia']			.'\',
	\''.$rd['NumeroDocumento']			.'\',
	\''.$rd['NossoNumero3']				.'\',
	\''.$rd['Brancos04']				.'\',
	\''.$rd['DataVencimento']			.'\',
	\''.$rd['ValorDoTitulo']			.'\',
	\''.$rd['CodigoDoBanco']			.'\',
	\''.$rd['AgenciaCobradora']			.'\',
	\''.$rd['DacAgenciaCobradora']		.'\',
	\''.$rd['Especie']					.'\',
	\''.$rd['TarifaDeCobranca']			.'\',
	\''.$rd['Brancos05']				.'\',
	\''.$rd['ValorDoIOF']				.'\',
	\''.$rd['ValorAbatimento']			.'\',
	\''.$rd['Descontos']				.'\',
	\''.$rd['ValorPrincipal']			.'\',
	\''.$rd['JurosDeMoraMulta']			.'\',
	\''.$rd['OutrosCreditos']			.'\',
	\''.$rd['BoletoDDA']				.'\',
	\''.$rd['Brancos06']				.'\',
	\''.$rd['DataCredito']				.'\',
	\''.$rd['InstrucaoCancelada']		.'\',
	\''.$rd['Brancos07']				.'\',
	\''.$rd['Zeros02']					.'\',
	\''.$rd['NomeDoSacado']				.'\',
	\''.$rd['Brancos08']				.'\',
	\''.$rd['Erros']					.'\',
	\''.$rd['Brancos09']				.'\',
	\''.$rd['CodigoDeLiquidacao']		.'\',
	\''.$rd['NumeroSequencial']			.'\',
	\''.date('Y-m-d H:i:s')				.'\',
	\''.''								.'\'
	);');
	//$line = select_query('mod_nfse_ginfesretornos', '', array('AllLine' => $rd['AllLine']));
	//$returnLine = mysql_fetch_array($line);
	//return $returnLine['retornoid'];
	return mysql_insert_id();
}
function pre_($v) {
	return '<fieldset><legend>Debug</legend><pre>'.$v.'</pre></fieldset>';
}
function lerRetorno_($file) {
	if (!file_exists($file)) exit("Arquivo $file inexistente");
	$lines = array();
	$data = array();
	$myfile = fopen($file, "r") or die("Unable to open file!");
	while (!feof($myfile)) {
		$lines[] = fgets($myfile);
	}
	//$texto = fread($myfile,filesize($arquivo));
	fclose($myfile);
	//echo count($lines);exit();
	$line = 0;
	foreach ($lines as $lk=>$lv) {
		$line++;
		if($lk < sizeof($lines)-1) {
			$data[$lk]['File']				=	$file;
			$data[$lk]['Line']				=	$line;
			$data[$lk]['AllLine']				=	str_replace("\r\n",'',$lv);
		}

		if($lk == 0)					$data[$lk]['first']	=	$lv; // first 	line
		if($lk == sizeof($lines)-2)		$data[$lk]['last']	=	$lv; // last	line

		if($lk != 0 && $lk < sizeof($lines)-2) {
			$data[$lk]['TipoRegistro']			=	substr($lv,  0, 1); //  1
			$data[$lk]['CodigoInscricao']		=	substr($lv,  1, 2); //  2
			$data[$lk]['NumeroInscricao']		=	substr($lv,  3,14); // 14
			$data[$lk]['Agencia']				=	substr($lv, 17, 4); //  4
			$data[$lk]['Zeros01']				=	substr($lv, 21, 2); //  2
			$data[$lk]['Conta']					=	substr($lv, 23, 5); //  5
			$data[$lk]['Dac']					=	substr($lv, 28, 1); //  1
			$data[$lk]['Brancos01']				=	substr($lv, 29, 8); //  8
			$data[$lk]['UsoDaEmpresa']			=	substr($lv, 37,25); // 25
			$data[$lk]['NossoNumero']			=	substr($lv, 62, 8); //  8
			$data[$lk]['Brancos02']				=	substr($lv, 70,12); // 12
			$data[$lk]['NumCarteira']			=	substr($lv, 82, 3); //  3
			$data[$lk]['NossoNumero2']			=	substr($lv, 85, 8); //  8
			$data[$lk]['DacNossoNumero']		=	substr($lv, 93, 1); //  1
			$data[$lk]['Brancos03']				=	substr($lv, 94,13); // 13
			$data[$lk]['CodCarteira']			=	substr($lv,107, 1); //  1
			$data[$lk]['CodigoDeOcorrencia']	=	substr($lv,108, 2); //  2
			$data[$lk]['DataDeOcorrencia']		=	substr($lv,110, 6); //  6
			$data[$lk]['NumeroDocumento']		=	substr($lv,116,10); // 10
			$data[$lk]['NossoNumero3']			=	substr($lv,126, 8); //  8
			$data[$lk]['Brancos04']				=	substr($lv,134,12); // 12
			$data[$lk]['DataVencimento']		=	substr($lv,146, 6); //  6
			$data[$lk]['ValorDoTitulo']			=	substr($lv,152,13); // 13
			$data[$lk]['CodigoDoBanco']			=	substr($lv,165, 3); //  3
			$data[$lk]['AgenciaCobradora']		=	substr($lv,168, 4); //  4
			$data[$lk]['DacAgenciaCobradora']	=	substr($lv,172, 1); //  1
			$data[$lk]['Especie']				=	substr($lv,173, 2); //  2
			$data[$lk]['TarifaDeCobranca']		=	substr($lv,175,13); // 13
			$data[$lk]['Brancos05']				=	substr($lv,188,26); // 26
			$data[$lk]['ValorDoIOF']			=	substr($lv,214,13); // 13
			$data[$lk]['ValorAbatimento']		=	substr($lv,227,13); // 13
			$data[$lk]['Descontos']				=	substr($lv,240,13); // 13
			$data[$lk]['ValorPrincipal']		=	substr($lv,253,13); // 13
			$data[$lk]['JurosDeMoraMulta']		=	substr($lv,266,13); // 13
			$data[$lk]['OutrosCreditos']		=	substr($lv,279,13); // 13
			$data[$lk]['BoletoDDA']				=	substr($lv,292, 1); //  1
			$data[$lk]['Brancos06']				=	substr($lv,293, 2); //  2
			$data[$lk]['DataCredito']			=	substr($lv,295, 6); //  6
			$data[$lk]['InstrucaoCancelada']	=	substr($lv,301, 4); //  4
			$data[$lk]['Brancos07']				=	substr($lv,305, 6); //  6
			$data[$lk]['Zeros02']				=	substr($lv,311,13); // 13
			$data[$lk]['NomeDoSacado']			=	substr($lv,324,30); // 30
			$data[$lk]['Brancos08']				=	substr($lv,354,23); // 23
			$data[$lk]['Erros']					=	substr($lv,377, 8); //  8
			$data[$lk]['Brancos09']				=	substr($lv,385, 7); //  7
			$data[$lk]['CodigoDeLiquidacao']	=	substr($lv,392, 2); //  2
			$data[$lk]['NumeroSequencial']		=	substr($lv,394, 6); //  6
		}
	}
	$retorno = array(
		'lines' => $lines,
		'data' => $data,
	);
	return $retorno;
}
function normalize_files_array_($files = []) {
	$normalized_array = [];
	foreach($files as $index => $file) {
		if (!is_array($file['name'])) {
			$normalized_array[$index][] = $file;
			continue;
		}
		foreach($file['name'] as $idx => $name) {
			$normalized_array[$index][$idx] = [
				'name' => $name,
				'type' => $file['type'][$idx],
				'tmp_name' => $file['tmp_name'][$idx],
				'error' => $file['error'][$idx],
				'size' => $file['size'][$idx]
			];
		}
	}
	return $normalized_array;
}
function getInvoceDetails_($invoiceid){

	$result = select_query("tblinvoices", "", array("id" => $invoiceid));
	$data = mysql_fetch_array($result);
	$invoiceid = $data['id'];
	if (!$invoiceid) {
		$apiresults = array("status" => "error", "message" => "Invoice ID Not Found");
		return null;
	}
	$userid = $data['userid'];
	$invoicenum = $data['invoicenum'];
	$date = $data['date'];
	$duedate = $data['duedate'];
	$datepaid = $data['datepaid'];
	$subtotal = $data['subtotal'];
	$credit = $data['credit'];
	$tax = $data['tax'];
	$tax2 = $data['tax2'];
	$total = $data['total'];
	$taxrate = $data['taxrate'];
	$taxrate2 = $data['taxrate2'];
	$status = $data['status'];
	$paymentmethod = $data['paymentmethod'];
	$notes = $data['notes'];
	$result = select_query("tblaccounts", "SUM(amountin)-SUM(amountout)", array("invoiceid" => $invoiceid));
	$data = mysql_fetch_array($result);
	$amountpaid = $data[0];
	$balance = $total - $amountpaid;
	$balance = format_as_currency($balance);
	// TIRA JUROS E MULTA DO addon faturajuros
	$result2 = full_query("SELECT SUM(amount) FROM tblinvoiceitems WHERE type IN ('faturajuros_LateInterest','faturajuros_LateFee','nfse_ginfes_disccount_dalay') AND invoiceid=".$invoiceid);
	$data2 = mysql_fetch_array($result2);
	$amountpaid2 = $data2[0];
	$balance2 = $total - $amountpaid - $amountpaid2;
	$balance2 = format_as_currency($balance2);
	// FIM TIRA JUROS E MULTA DO addon faturajuros
	$gatewaytype = get_query_val("tblpaymentgateways", "value", array("gateway" => $paymentmethod, "setting" => "type"));
	$ccgateway = (($gatewaytype == "CC" || $gatewaytype == "OfflineCC") ? true : false);
	$apiresults = array("result" => "success", "invoiceid" => $invoiceid, "invoicenum" => $invoicenum, "userid" => $userid, "date" => $date, "duedate" => $duedate, "datepaid" => $datepaid, "subtotal" => $subtotal, "credit" => $credit, "tax" => $tax, "tax2" => $tax2, "total" => $total, "balance" => $balance, "balance2" => $balance2, "taxrate" => $taxrate, "taxrate2" => $taxrate2, "status" => $status, "paymentmethod" => $paymentmethod, "notes" => $notes, "ccgateway" => $ccgateway);
	$result = select_query("tblinvoiceitems", "", array("invoiceid" => $invoiceid));
	while ($data = mysql_fetch_array($result)) {
		$apiresults['items']['item'][] = array("id" => $data['id'], "type" => $data['type'], "relid" => $data['relid'], "description" => $data['description'], "amount" => $data['amount'], "taxed" => $data['taxed']);
	}
	$apiresults['transactions'] = "";
	$result = select_query("tblaccounts", "", array("invoiceid" => $invoiceid));
	while ($data = mysql_fetch_assoc($result)) {
		$apiresults['transactions']['transaction'][] = $data;
	}
	return $apiresults;

}
*/
?>
 