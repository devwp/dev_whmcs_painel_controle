<?php

$_LANG['token'] = ', Error Token:';
$_LANG['generalError'] = 'Something has gone wrong. Check logs and contact admin';

$_LANG['addonAA']['datatables']['next'] = 'Next';
$_LANG['addonAA']['datatables']['previous'] = 'Next';
$_LANG['addonAA']['datatables']['zeroRecords'] = 'Nothing to display';

$_LANG['addonAA']['pagesLabels']['home']['label']          = 'main';
$_LANG['addonAA']['pagesLabels']['configuration']['label'] = 'Configuration';

$_LANG['addonAA']['pagesLabels']['home']['boxes']          = 'Boxes';
$_LANG['addonAA']['pagesLabels']['home']['buttons']        = 'Buttons';
$_LANG['addonAA']['pagesLabels']['home']['formAdvanced']   = 'Form Advanced';
$_LANG['addonAA']['pagesLabels']['home']['formGeneral']    = 'Form General';
$_LANG['addonAA']['pagesLabels']['home']['icons']          = 'Icons';
$_LANG['addonAA']['pagesLabels']['home']['panels']         = 'Panels';
$_LANG['addonAA']['pagesLabels']['home']['tableAjax']      = 'Table Ajax';
$_LANG['addonAA']['pagesLabels']['home']['tableResponsive']= 'Table Reponsive';
$_LANG['addonAA']['pagesLabels']['home']['tables']         = 'Tables';
$_LANG['addonAA']['pagesLabels']['home']['typography']     = 'Typography';

$_LANG['addonAA']['pagesLabels']['testPage']['label']         = 'Test Page';
$_LANG['addonAA']['pagesLabels']['testPageSimple']['label']   = 'Simple Page';
$_LANG['addonAA']['pagesLabels']['whmcsMagic']['label']       = 'WHMCS Magic';
$_LANG['addonAA']['pagesLabels']['whmcsServices']['label']    = 'WHMCS SErev';
$_LANG['addonAA']['home']['error']['example']  = 'Example Errror,  Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. ';

$_LANG['addonAA']['testPage']['table']['name']                       = 'Test Item List';

$_LANG['addonAA']['testPage']['item']['header']                      = 'Test Form';
$_LANG['addonAA']['testPage']['item']['text_name']['label']          = 'Some Text';
$_LANG['addonAA']['testPage']['item']['text_name']['description']    = 'Some Text Description';
$_LANG['addonAA']['testPage']['item']['text_name2']['label']         = 'Some Text2';
$_LANG['addonAA']['testPage']['item']['text_name2']['description']   = 'Some Desc 2';
$_LANG['addonAA']['testPage']['item']['text_name2']['placeholder']   = 'Example placeholder';

$_LANG['addonAA']['testPage']['item']['checkbox_name']['label']      = 'Test Checkbox';

$_LANG['addonAA']['testPage']['item']['some_option']['label']        = 'Some Option';
$_LANG['addonAA']['testPage']['item']['some_option']['options']['1'] = 'Option 1';
$_LANG['addonAA']['testPage']['item']['some_option']['options']['2'] = 'Option 2';
$_LANG['addonAA']['testPage']['item']['some_option']['options']['3'] = 'Option 3';
$_LANG['addonAA']['testPage']['item']['some_option']['options']['4'] = 'Option 4';
$_LANG['addonAA']['testPage']['item']['some_option']['options']['5'] = 'Option 5';



$_LANG['addonAA']['testPage']['item']['some_option2']['label']        = 'Some Option';

$_LANG['addonAA']['testPage']['item']['some_option2']['options']['1'] = 'Option 1';
$_LANG['addonAA']['testPage']['item']['some_option2']['options']['2'] = 'Option 2';
$_LANG['addonAA']['testPage']['item']['some_option2']['options']['3'] = 'Option 3';
$_LANG['addonAA']['testPage']['item']['some_option2']['options']['4'] = 'Option 4';
$_LANG['addonAA']['testPage']['item']['some_option2']['options']['5'] = 'Option 5';
$_LANG['addonAA']['testPage']['item']['some_option2']['options']['6'] = 'Option 6';
$_LANG['addonAA']['testPage']['item']['some_option2']['options']['7'] = 'Option 7';
$_LANG['addonAA']['testPage']['item']['some_option2']['options']['8'] = 'Option 8';
$_LANG['addonAA']['testPage']['item']['some_option2']['options']['9'] = 'Option 9';
$_LANG['addonAA']['testPage']['item']['some_option2']['options']['10'] = 'Option 10';

$_LANG['addonAA']['testPage']['item']['test']['content']       = 'Test Button';
$_LANG['addonAA']['testPage']['item']['test2']['content']      = 'Test2 Button';
$_LANG['addonAA']['testPage']['item']['test3']['content']      = 'Test3 Button';
$_LANG['addonAA']['testPage']['item']['test']['label']         = 'Action Buttons';


$_LANG['addonAA']['testPage']['item']['example_checkbox']['label']                 = 'Example Checkbox';
$_LANG['addonAA']['testPage']['item']['example_checkbox']['options']['Option1']    = 'Option 1';
$_LANG['addonAA']['testPage']['item']['example_checkbox']['options']['Option2']    = 'Option 2';
$_LANG['addonAA']['testPage']['item']['example_checkbox']['options']['Option3']    = 'Option 3';


$_LANG['addonAA']['testPage']['item']['example_legend']['label'] = 'Example Legend';

$_LANG['addonAA']['testPage']['item']['onoff']['label']      = 'On/Off Field';
$_LANG['addonAA']['testPage']['item']['onoff']['disabled']   = 'Disabled';
$_LANG['addonAA']['testPage']['item']['onoff']['enabled']    = 'Enabled';
$_LANG['addonAA']['testPage']['item']['pass']['label']       = 'Password';
$_LANG['addonAA']['testPage']['item']['radio_field']['label']= 'Radio';

$_LANG['addonAA']['testPage']['item']['saveItem']['label']       = 'Save';

$_LANG['addonAA']['testPage']['item']['id']['label']          = 'ID';
$_LANG['addonAA']['testPage']['item']['actions']['label']     = 'Actions';


$_LANG['addonAA']['testPage']['previous']                 = 'Previous';
$_LANG['addonAA']['testPage']['next']                     = 'Next';
$_LANG['addonAA']['testPage']['actionButtons']['edit']    = 'Edit';
$_LANG['addonAA']['testPage']['actionButtons']['delete']  = 'Delete';

$_LANG['addonAA']['testPage']['modal']['editLabel']       = 'Edit Item:';

$_LANG['addonAA']['testPage']['modal']['deleteLabel']     = 'Delte Item: ';
$_LANG['addonAA']['testPage']['modal']['delete']          = 'Delete';
$_LANG['addonAA']['testPage']['modal']['close']           = 'Close';
$_LANG['addonAA']['testPage']['modal']['deleteDescription'] = 'This action cannot be undone';

$_LANG['addonAA']['testPage']['messages']['saveSuccess']   = 'Item Successfull Saved';
$_LANG['addonAA']['testPage']['messages']['deleteSuccess'] = 'Item %s Successfull deleted';
$_LANG['addonAA']['testPage']['messages']['deleteSuccess2'] = 'Item %s deleted';

$_LANG['addonAA']['testPage']['messages']['validateError']    = 'Form Contains Errors, Please Check Fields Errors';

$_LANG['validationErrors']['emptyField'] = 'Field cannot be empty';

$_LANG['addonAA']['testPage']['modal']['updateItem'] = 'Update';
                                                                               



$_LANG['addonAA']['testPageSimple']['item']['header']                     = 'Simple Item';
$_LANG['addonAA']['testPageSimple']['item']['text_name']['label']         = 'Name';
$_LANG['addonAA']['testPageSimple']['item']['some_option']['label']       = 'Related Item';
$_LANG['addonAA']['testPageSimple']['item']['saveItem']['label']          = 'Save Item';
$_LANG['addonAA']['testPageSimple']['item']['id']['label']                = 'ID';
$_LANG['addonAA']['testPageSimple']['item']['actions']['label']           = 'Actions';

$_LANG['addonAA']['testPageSimple']['table']['name']                      = 'Simple Item List';
$_LANG['addonAA']['testPageSimple']['messages']['saveSuccess']            = 'Item Added Successfull';


$_LANG['addonAA']['testPageSimple']['modal']['editLabel']                 = 'Edit Item';
$_LANG['addonAA']['testPageSimple']['modal']['close']                     = 'Close';
$_LANG['addonAA']['testPageSimple']['modal']['updateItem']                = 'Update';

$_LANG['addonAA']['testPageSimple']['modal']['deleteLabel']               = 'Delete Item';
$_LANG['addonAA']['testPageSimple']['modal']['deleteDescription']         = 'This Action Cannot be undone';
$_LANG['addonAA']['testPageSimple']['modal']['delete']                    = 'Delete';

$_LANG['addonAA']['testPageSimple']['actionButtons']['edit']              = 'Edit';
$_LANG['addonAA']['testPageSimple']['actionButtons']['delete']            = 'Delete';

$_LANG['addonAA']['testPage']['item']['simpleNum']['label']               = 'Simple Number';



$_LANG['serverAA']['configuration']['configuration']['header']          = 'Configuration';
$_LANG['serverAA']['configuration']['configuration']['a']['label']      = 'A';
$_LANG['serverAA']['configuration']['configuration']['b']['label']      = 'B';
$_LANG['serverAA']['configuration']['configuration']['save']['content'] = 'Save';


$_LANG['addonAA']['whmcsServices']['table']['name'] = 'whmcsServices table name';
$_LANG['addonAA']['whmcsServices']['item']['id']['label'] = 'item id label';
$_LANG['addonAA']['whmcsServices']['item']['type']['label'] = 'item type label';
$_LANG['addonAA']['whmcsServices']['item']['group']['label'] = 'item group label';
$_LANG['addonAA']['whmcsServices']['item']['name']['label'] = 'item name label';
$_LANG['addonAA']['whmcsServices']['item']['server_type']['label'] = 'item server_type label';
$_LANG['addonAA']['whmcsServices']['item']['server_group_id']['label'] = 'item server_group_id label';
$_LANG['addonAA']['whmcsServices']['item']['actions']['label'] = 'item actions label';
$_LANG['addonAA']['whmcsServices']['modal']['editLabel'] = 'whmcsServices modal editLabel';
$_LANG['addonAA']['whmcsServices']['modal']['close'] = 'whmcsServices modal close';
$_LANG['addonAA']['whmcsServices']['modal']['updateItem'] = 'whmcsServices modal updateItem';
$_LANG['addonAA']['whmcsServices']['modal']['deleteLabel'] = 'whmcsServices modal deleteLabel';
$_LANG['addonAA']['whmcsServices']['modal']['deleteDescription'] = 'whmcsServices modal deleteDescription';
$_LANG['addonAA']['whmcsServices']['modal']['delete'] = 'whmcsServices modal delete';


$_LANG['addonAA']['whmcsServices']['actionButtons']['customfields'] = 'customfields';
$_LANG['addonAA']['whmcsServices']['actionButtons']['configoptions'] = 'configoptions';
$_LANG['addonAA']['whmcsServices']['actionButtons']['edit'] = 'edit';
$_LANG['addonAA']['whmcsServices']['actionButtons']['delete'] = 'delete';

$_LANG['addonCA']['home']['credentials'] = 'Credentials';
$_LANG['addonCA']['home']['username'] = 'username';
$_LANG['addonCA']['home']['password'] = 'password';
$_LANG['addonCA']['home']['showPassword'] = 'showPassword';
$_LANG['addonCA']['home']['hidePassword'] = 'hidePassword';
$_LANG['addonCA']['home']['openPanel'] = 'openPanel';


################################################################################
# Advanced Billing 3.0 Lang

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *  Main Menu
 */
$_LANG['addonAA']['pagesLabels']['items']['label']          = "Items";
$_LANG['addonAA']['pagesLabels']['invoices']['label']       = "Invoices";
$_LANG['addonAA']['pagesLabels']['settings']['label']       = "Settings";
$_LANG['addonAA']['pagesLabels']['settings']['integration'] = 'Integration';
$_LANG['addonAA']['pagesLabels']['settings']['extensions']  = 'Extensions';
$_LANG['addonAA']['pagesLabels']['settings']['logs']        = 'Logs';

$_LANG['addonAA']['pagesLabels']['label']['configuration'] = 'Configuration';
$_LANG['addonAA']['pagesLabels']['label']['items'] = 'Items';
$_LANG['addonAA']['pagesLabels']['label']['invoices'] = 'Invoices';
$_LANG['addonAA']['pagesLabels']['label']['settings'] = 'Settings';
$_LANG['addonAA']['pagesLabels']['label']['credits'] = 'Credits';

$_LANG['addonAA']['configuration']['Please Note'] = 'Please Note';
$_LANG['addonAA']['configuration']['In order to enable automatic synchronization, please setup a following command line in the cron (5 minutes suggested)'] = 'In order to enable automatic synchronization, please set up a following cron command line';
$_LANG['addonAA']['configuration']['and set cron frequency for each product'] = 'and set cron frequency for each product (every 5 minutes suggested)';
$_LANG['addonAA']['configuration']['Add Product'] = 'Add Product';
$_LANG['addonAA']['configuration']['Enable Advanced Billing For'] = 'Enable Advanced Billing For';
$_LANG['addonAA']['configuration']['To enable product in Advanced Billing module please select it in field below'] = 'To enable a product in Advanced Billing module, please select it below';
$_LANG['addonAA']['configuration']['Product List'] = 'Products List';
$_LANG['addonAA']['configuration']['Product Name'] = 'Product Name';
$_LANG['addonAA']['configuration']['Submodule'] = 'Submodule';
$_LANG['addonAA']['configuration']['Enabled Extensions'] = 'Enabled Extensions';
$_LANG['addonAA']['configuration']['Cron Frequency'] = 'Cron Frequency';
$_LANG['addonAA']['configuration']['This field accepts numbers only'] = 'This field accepts numbers only';
$_LANG['addonAA']['configuration']['Value can\'t be below 180'] = 'Value can\'t be below 180';
$_LANG['addonAA']['configuration']['seconds'] = 'seconds';
$_LANG['addonAA']['configuration']['Status'] = 'Status';
$_LANG['addonAA']['configuration']['Actions'] = 'Actions';
$_LANG['addonAA']['configuration']['Disabled'] = 'Disabled';
$_LANG['addonAA']['configuration']['Enabled'] = 'Enabled';
$_LANG['addonAA']['configuration']['Enable'] = 'Enable';
$_LANG['addonAA']['configuration']['Disable'] = 'Disable';
$_LANG['addonAA']['configuration']['Pricing'] = 'Pricing';
$_LANG['addonAA']['configuration']['Settings'] = 'Settings';
$_LANG['addonAA']['configuration']['View Items'] = 'View Items';
$_LANG['addonAA']['configuration']['View Invoices'] = 'View Invoices';

$_LANG['addonAA']['configuration']['Set Pricing For '] = 'Set Pricing For ';
$_LANG['addonAA']['configuration']['Usage Record'] = 'Usage Record';
$_LANG['addonAA']['configuration']['Free Limit'] = 'Free Limit';
$_LANG['addonAA']['configuration']['Price'] = 'Price';
$_LANG['addonAA']['configuration']['Display Unit'] = 'Display Unit';
$_LANG['addonAA']['configuration']['Type'] = 'Type';
$_LANG['addonAA']['configuration']['Configure'] = 'Configure';
$_LANG['addonAA']['configuration']['Save'] = 'Save';
$_LANG['addonAA']['configuration']['Cancel'] = 'Cancel';

$_LANG['addonAA']['configuration']['Module Settings'] = 'Module Settings';
$_LANG['addonAA']['configuration']['Recurring Billing and Credit Billing cannot be enabled simultaneously'] = 'Recurring Billing and Credit Billing extensions cannot be enabled simultaneously';
$_LANG['addonAA']['configuration']['Settings for this module are not available'] = 'Settings for this module are not available';
$_LANG['addonAA']['configuration']['Settings for this extension are not available'] = 'Settings for this extension are not available';
        
        
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *  Items
 */
$_LANG['addonAA']['items']['Item list will show you current usage counted by the Advanced Billing module for accounts'] = 'Items list will show you current usage counted by the Advanced Billing module for single accounts';
$_LANG['addonAA']['pagesLabels']['items']['details'] = 'Details';
$_LANG['addonAA']['items']['Account List'] = 'Accounts List';
$_LANG['addonAA']['items']['Product Name'] = 'Product Name';
$_LANG['addonAA']['items']['Domain'] = 'Domain';
$_LANG['addonAA']['items']['Actions'] = 'Actions';
$_LANG['addonAA']['items']['View Details'] = 'View Details';
$_LANG['addonAA']['items']['Delete Records'] = 'Delete Records';
$_LANG['addonAA']['items']['Hosting'] = 'Hosting';
$_LANG['addonAA']['items']['Client Name'] = 'Client Name';
$_LANG['addonAA']['items']['Total Amount'] = 'Total Amount';
$_LANG['addonAA']['items']['Last Update'] = 'Last Update';
$_LANG['addonAA']['items']['Are you sure?'] = 'Are you sure?';
$_LANG['addonAA']['items']['Nothing to display'] = 'Nothing to display';
$_LANG['addonAA']['items']['Delete'] = 'Delete';
$_LANG['addonAA']['items']['Cancel'] = 'Cancel';
$_LANG['addonAA']['items']['Generate Invoice'] = 'Generate Invoice';
$_LANG['addonAA']['items']['Total'] = 'Total';
$_LANG['addonAA']['items']['Date'] = 'Date';
$_LANG['addonAA']['items']['All currently generated records will be deleted. This action can NOT be undone!'] = 'All currently generated records will be deleted. This action can NOT be undone!';
$_LANG['addonAA']['items']['For Product'] = 'For Product';
$_LANG['addonAA']['items']['Show All'] = 'Show All';

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *  Invoices
 */
$_LANG['addonAA']['invoices']['Awaiting Invoices means, that it will not be generated until you confirm that manually'] = 'Awaiting Invoice means that it will not be generated until you confirm that action manually';
$_LANG['addonAA']['invoices']['This is the best way to avoid any unwanted invoices for your clients and test the possibilities of Advanced Billing module.
            To enable this feature, please edit your Products in Configuration page and disable "Autogenerate Invoice" option.'] = 'This is the best way to avoid any unwanted invoices for your clients and test the possibilities of Advanced Billing module.
            To enable this feature, please edit your Products in Configuration page and disable "Autogenerate Invoice" option.';
$_LANG['addonAA']['invoices']['Awaiting Invoices'] = 'Awaiting Invoices';
$_LANG['addonAA']['invoices']['Client Name'] = 'Client Name';
$_LANG['addonAA']['invoices']['Hosting'] = 'Hosting';
$_LANG['addonAA']['invoices']['Product Name'] = 'Product Name';
$_LANG['addonAA']['invoices']['Total'] = 'Total';
$_LANG['addonAA']['invoices']['Invoice Date'] = 'Invoice Date';
$_LANG['addonAA']['invoices']['Invoice Due Date'] = 'Invoice Due Date';
$_LANG['addonAA']['invoices']['Actions'] = 'Actions';
$_LANG['addonAA']['invoices']['Show Details'] = 'Show Details';
$_LANG['addonAA']['invoices']['Delete'] = 'Delete';
$_LANG['addonAA']['invoices']['Generate'] = 'Generate';
$_LANG['addonAA']['invoices']['Invoices History'] = 'Invoices History';
$_LANG['addonAA']['invoices']['Nothing to display'] = 'Nothing to display';

$_LANG['addonAA']['invoices']['Are you sure?'] = 'Are you sure?';
$_LANG['addonAA']['invoices']['Selected Invoice will be deleted. This action can NOT be undone!'] = 'Selected invoice will be deleted. This action can NOT be undone!';
$_LANG['addonAA']['invoices']['Cancel'] = 'Cancel';

$_LANG['addonAA']['invoices']['Invoice Details'] = 'Invoice Details';
$_LANG['addonAA']['invoices']['Description'] = 'Description';
$_LANG['addonAA']['invoices']['Amount'] = 'Amount';
$_LANG['addonAA']['invoices']['Taxed'] = 'Taxed';
$_LANG['addonAA']['invoices']['Save'] = 'Save';
$_LANG['addonAA']['invoices']['Date'] = 'Date';
$_LANG['addonAA']['invoices']['Due Date'] = 'Due Date';
$_LANG['addonAA']['invoices']['Client'] = 'Client';

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *  Settings → Integration Code
 */
$_LANG['addonAA']['settings']['In order to enable you customers to monitor resource usage in product details and pricing in product configuration please do following instruction'] = 'In order to enable you customers to monitor resource usage in product details and pricing in product configuration please do following instruction';
$_LANG['addonAA']['settings']['Display product pricing in product configuration'] = 'Display Product Pricing During Order';
$_LANG['addonAA']['settings']['Display usage records'] = 'Display Usage Records';
$_LANG['addonAA']['settings']['In order to enable your customers to monitor resource usage, open the file'] = 'In order to enable your customers to monitor resource usage, open the file';
$_LANG['addonAA']['settings']['Find the following line'] = 'Find the following line';
$_LANG['addonAA']['settings']['Add this code Before the line'] = 'Add this code above that line';
$_LANG['addonAA']['settings']['In order to enable usage record pricing on order form, open the file'] = 'In order to enable usage record pricing on order form, open the file';

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *  Settings → Extensions
 */
$_LANG['addonAA']['settings']['Each extension is configured per product, therefore you can use different sets of extensions according to your needs'] = 'Each extension is configured per product, therefore you can use different sets of extensions according to your needs';
$_LANG['addonAA']['settings']['Available Extensions'] = 'Available Extensions';
$_LANG['addonAA']['settings']['Extension'] = 'Extension';
$_LANG['addonAA']['settings']['Type'] = 'Type';
$_LANG['addonAA']['settings']['Version'] = 'Version';
$_LANG['addonAA']['settings']['Actions'] = 'Actions';
$_LANG['addonAA']['settings']['Enabled'] = 'Enabled';
$_LANG['addonAA']['settings']['Disabled'] = 'Disabled';
$_LANG['addonAA']['settings']['Configure'] = 'Configure';
$_LANG['addonAA']['settings']['Save'] = 'Save';
$_LANG['addonAA']['settings']['Cancel'] = 'Cancel';
$_LANG['addonAA']['settings']['Set Configuration For '] = 'Set Configuration For ';
$_LANG['addonAA']['settings']['License Key'] = 'License Key';

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *  Settings → Logs
 */
$_LANG['addonAA']['settings']['Logs Settings'] = 'Logs Settings';
$_LANG['addonAA']['settings']['Logs'] = 'Logs';
$_LANG['addonAA']['settings']['Logger Type'] = 'Logger Type';
$_LANG['addonAA']['settings']['Database'] = 'Database';
$_LANG['addonAA']['settings']['File'] = 'File';
$_LANG['addonAA']['settings']['Date'] = 'Date';
$_LANG['addonAA']['settings']['Message'] = 'Message';
$_LANG['addonAA']['settings']['File Content'] = 'File Content';
$_LANG['addonAA']['settings']['Back'] = 'Back';
$_LANG['addonAA']['pagesLabels']['settings']['viewLogFile'] = 'View File';

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *  Extension
 */
$_LANG['addonAA']['configuration']['Set Settings For'] = 'Configure Settings For';
$_LANG['addonAA']['configuration']['Check this option if you want to enable usage records extension for this product'] = 'Check this option if you want to enable Usage Records extension for this product';
$_LANG['addonAA']['configuration']['Display Summary From'] = 'Display Summary From';
$_LANG['addonAA']['configuration']['Usage Records Precision'] = 'Usage Records Precision';
$_LANG['addonAA']['configuration']['Usage Records Per Page'] = 'Usage Records Per Page';
$_LANG['addonAA']['configuration']['Enable Records History'] = 'Records History';

$_LANG['addonAA']['configuration']['Bill on terminate'] = 'Bill On Termination';
$_LANG['addonAA']['configuration']['Bill your client after account is terminated'] = 'Bill your client after account is terminated';
$_LANG['addonAA']['configuration']['Check this option if you want to bill hosting on terminate'] = 'Check this option if you want to bill hosting on termination';
$_LANG['addonAA']['configuration']['Billing Type'] =  'Billing Type';
$_LANG['addonAA']['configuration']['Billing Type Value'] = 'Billing Type Value';
$_LANG['addonAA']['configuration']['Due Date'] = 'Due Date';
$_LANG['addonAA']['configuration']['Autogenerate Invoice'] = 'Autogenerate Invoice';
$_LANG['addonAA']['configuration']['Check this option if you want module to generate invoice automatically'] = 'Check this option if you want the module to generate invoices automatically';
$_LANG['addonAA']['configuration']['Auto Apply Credits'] = 'Auto Apply Credits';
$_LANG['addonAA']['configuration']['When this is enabled invoices will be generated automatically'] = 'When this is enabled invoices will be generated automatically';
$_LANG['addonAA']['configuration']['Auto apply any available credit from clients credit balance'] = 'Auto apply any available credit from clients credit balance';

$_LANG['addonAA']['configuration']['Enable Credit Billing'] = 'Enable Credit Billing';
$_LANG['addonAA']['configuration']['Check this option if you want to enable Credit Billing for this product'] = 'Check this option if you want to enable Credit Billing for this product';
$_LANG['addonAA']['configuration']['Create Invoice Each'] = 'Create Invoice Each';
$_LANG['addonAA']['configuration']['Days'] = 'Days';
$_LANG['addonAA']['configuration']['Minimum Credit'] = 'Minimum Credit';
$_LANG['addonAA']['configuration']["Minimum amount that will be charged from client's account"] = 'Minimum amount that will be charged from client\'s account';
$_LANG['addonAA']['configuration']['Low Credit Notification'] = 'Low Credit Notification';
$_LANG['addonAA']['configuration']['Send email Notification about low credit amount on account']  = 'Send email Notification about low credit amount on account';
$_LANG['addonAA']['configuration']['Email Inverval'] = 'Email Interval';
$_LANG['addonAA']['configuration']['Set email interval for email notification'] = 'Set time interval for email notification';
$_LANG['addonAA']['configuration']['Autosuspend'] = 'Autosuspend';
$_LANG['addonAA']['configuration']['Autosuspend account when user does not have sufficient funds'] = 'Autosuspend account if user does not have sufficient funds';
$_LANG['addonAA']['configuration']['Days'] = 'Days';

$_LANG['addonAA']['configuration']['Autorefill'] = 'Autorefill';
$_LANG['addonAA']['configuration']['Enable to charge from client\'s credit card when client\'s balance reach 0'] = 'Enable to charge from client\'s credit card when client\'s balance reaches 0';
$_LANG['addonAA']['configuration']['Gateway'] = 'Gateway';
$_LANG['addonAA']['configuration']['Minimum Amount'] = 'Minimum Amount';
$_LANG['addonAA']['configuration']['Minimum amount to charge from client\'s credit card'] = 'Minimum amount to charge from client\'s credit card';
$_LANG['addonAA']['configuration']['Maximum Amount'] = 'Maximum Amount';
$_LANG['addonAA']['configuration']['Maximum amount to charge from client\'s credit card'] = 'Maximum amount to charge from client\'s credit card';

$_LANG['addonAA']['configuration']['Check this option if you want to enable Graph Extension for this product'] = 'Check this option if you want to enable Graph Extension for this product';

$_LANG['addonAA']['pagesLabels']['settings']['archive'] = 'Items Archive';
$_LANG['addonAA']['settings']['On this page you can find information regarding archived usage records'] = "On this page you can find information regarding archived usage records";
$_LANG['addonAA']['settings']['Archive Information'] = "Archive Information";
$_LANG['addonAA']['settings']['Archived Items'] = "Archived Items";
$_LANG['addonAA']['settings']['Space On Disk'] = "Space On Disk";
$_LANG['addonAA']['settings']['First Item Date'] = "First Item Date";
$_LANG['addonAA']['settings']['Flush'] = "Flush";

