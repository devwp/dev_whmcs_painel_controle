<div class="body" data-target=".body" data-spy="scroll" data-twttr-rendered="true" id="mg-wrapper">
    <div id="mg-content" class="">
        <div id="top-bar">
            <div id="module-name">
                <h2>{$addon->name}</h2>
                <!--<h4>{$addon->getSiteTitle()}</h4>-->
            </div>
            <ul id="top-nav">
                {foreach from=$addon->menu() item=element key=page}
                    {if not $element.hidden} 
                        {if $element.submenu}
                            <li class="dropdown">

                                <a href="{$addon->url($page)}" class="dropdown-toggle" data-toggle="dropdown" role="button" id="menu-{$page}"><i class="icon-{$element.icon}"></i>{$element.title}<i class="icon-caret-down"></i></a>
                                <ul class="dropdown-menu" role="menu" aria-labelledby="menu-{$page}">
                                    {foreach from=$element.submenu item=subelement key=subpage}
                                        {if $subelement.url} 
                                              <li><a href="{$subelement.url}" target="_blank"><i class="icon-{$subelement.icon}"></i>{$subelement.title}</a> </li>
                                        {else}
                                              <li><a href="{$addon->url($page, $subpage)}"><i class="icon-{$subelement.icon}"></i>{$subelement.title}</a> </li>
                                        {/if}
                                    {/foreach}
                                </ul>
                            </li>
                        {else}
                              <li><a href="{$addon->url($page)}"><i class="icon-{$element.icon}"></i>{$element.title}</a></li>
                        {/if}
                    {/if}
                {/foreach}
            </ul>

            <a class="slogan nblue-box visible-desktop hidden-tablet" href="http://www.modulesgarden.com" target="_blank" alt="ModulesGarden Custom Development">
                <span class="mg-logo"></span>
                <small>We are here to help you, just click!</small>
            </a>

        </div><!-- end of TOP BAR -->

        <div class="inner">
            {if ! isset($isError) || $isError neq 1}
            <h2 class="section-heading">
                <i class="icon-{$addon->getSiteIcon()}"></i> <a href="{$smarty.get.modpage|addon_url}">{$addon->getSiteTitle()}</a>
            </h2>
            <!--
            <ol class="breadcrumb">
                <li><a href="{$addon->url($addon->defaultPage[0])}"><i class="icon icon-home"></i></a></li>
                {if $addon->isSubpage()}
                <li><a href="{$addon->url($smarty.get.modpage)}">{$addon->getSiteTitle()}</a></li>
                <li class="active">{$addon->getSubpageTitle()}</li>
                {else}
                <li class="active">{$addon->getSiteTitle()}</li>
                {/if}
            </ol>-->
            
            {if isset($messages) and ! empty($messages)}
            {foreach from=$messages item=message}
                <div class="alert alert-block alert-{$message.status}">
                     <h5 style="margin: 0 0 5px 0;">{if $message.status eq 'error'}Problem occured!{elseif $message.status eq 'success'}Success!{/if}</h5> 
                    {$message.message} <button type="button" class="close" data-dismiss="alert">&times;</button></div>
            {/foreach}
            {/if}
            {else}
                <h2 class="section-heading">
                    <i class="icon-warning-sign"></i> <a href="{$smarty.get.modpage|addon_url}">Ooops! Something bad probably just happend.</a>
                </h2>
            {/if}
            
            {$content}
        </div><!-- end of INNER -->
        <div class="overlay hide">
        </div>
    </div><!-- end of CONTENT -->
</div>