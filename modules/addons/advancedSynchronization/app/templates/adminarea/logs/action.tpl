<table>
    <tbody><tr>
            <td>{$lang->_('Server: ')}</td>
            <td><b>{$server}</b></td>
        </tr>

    </tbody>
</table>
<table>
    <tbody>
        <tr>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td><b>{$lang->_('Synchronization Status')}</b> </td>
            <td></td>
        </tr>
        <tr>
            <td> </td>
            <td></td>
        </tr>
        <tr>
            <td>{$lang->_('Products Successfully Synchronized: ')}</td>
            <td><span class="success-status"><b>{$statistics.success}</b></span></td>
        </tr>
        <tr>
            <td>{$lang->_('Products Not Synchronized Successfully: ')}</td>
            <td><span class="fail-status"><b>{$statistics.fail}</b></span></td>
        </tr>
        <tr>
            <td>{$lang->_("New Clients' Accounts Created: ")}</td>
            <td><span class="success-status"><b>{$statistics.users}</b></span></td>
        </tr>


    </tbody>
</table>
<br> 
{if count($logs)}


    <table id="logs">
        <thead>
            <tr>
                <th>{$lang->_('Title')}</th>
                <th>{$lang->_('Date')}</th>
                <th>{$lang->_('Status')}</th>
                <th>{$lang->_('Description')}</th>
            </tr>
        </thead>
        <tbody>
            {foreach from=$logs item=log}
                <tr>
                    <td>{$log.title}</td>
                    <td>{$log.date}</td>
                    <td>{$log.status}</td>
                    <td style='text-align: left;'>{$log.description}</td>
                <tr>
                {/foreach}
        </tbody>
    </table>
{else}
    <div style="text-align: center">{$lang->_('No Logs Data')}</div>
{/if}
<div class="right-buttons">
    <a href="{'logs'|addon_url:'index'}" class="button">{$lang->_('Back To List')}</a>
</div>