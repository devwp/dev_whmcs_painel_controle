<?php

defined('DS') ? null : define('DS', DIRECTORY_SEPARATOR);

/**
 * Call functions when modules is terminated. This function should genererate invoice for termianted account.
 * It is working only if credit billing is disabled!
 * @param type $params 
 */

function OnAppBilling_AfterModuleTerminate($params)
{
    //include required files
    require_once dirname(__FILE__).DIRECTORY_SEPARATOR.'core.php';
    //get account details
    $hosting_id =   $params['params']['accountid'];
    $product_id =   $params['params']['pid'];
    $user_id    =   $params['params']['clientsdetails']['id'];
    
    $row = mysql_get_row("SELECT billing_settings, module FROM OnAppBilling_settings WHERE product_id = ? AND enable = 1", array($product_id));
    if(!$row)
    {
        return false;
    }
        
    $billing_settings = unserialize($row['billing_settings']);
    
    if($billing_settings['bill_on_terminate'] != 1)
    {
        return false;
    }
    
    if($billing_settings['credit_billing']['enable'] == 1)
    {
        return true;
    }
    
    //Create Product Class
    $p = new OAProduct($product_id);
    //Get Server Type
    $type = $p->getServerType();
    //Get First Record
    $first_record = mysql_get_row("SELECT DATE(`date`) as `date` FROM OnAppBilling_".$type."_prices 
                WHERE product_id = ? AND hosting_id = ? ORDER BY record_id ASC LIMIT 1", array($product_id, $hosting_id));
    if(!$first_record)
    {
        return false;
    }
    
    $start_date =   $first_record['date'];
    $end_date   =   date('Y-m-d');
    
    if($billing_settings['autogenerate_invoice'] == 1)
    {
        $invoice_id = OnAppBillingGenerateInvoice($hosting_id, $product_id, $start_date, $end_date, array(
            'autoapplycredit'   =>  $billing_settings['autoapplycredit'],
            'duedate'           =>  $billing_settings['billing_duedate']
        ));
        
        if($invoice_id !== false)
        {
            OnAppBillingDeleteUsageRecord($hosting_id, $product_id, $start_date, $end_date);
        } 
    }
    else
    {
        $invoice_id = OnAppBillingGenerateAwaitingInvoice($hosting_id, $product_id, $start_date, $end_date, array(
            'autoapplycredit'   =>  $billing_settings['autoapplycredit'],
            'duedate'           =>  $billing_settings['billing_duedate']
        ));
        
        if($invoice_id !== false)
        {
            OnAppBillingDeleteUsageRecord($hosting_id, $product_id, $start_date, $end_date);
        }
    }
}
add_hook('AfterModuleTerminate', 100, 'OnAppBilling_AfterModuleTerminate');
 
function OnAppBilling_InvoiceCreationPreEmail($params)
{
    global $CONFIG;
    if($CONFIG['NoInvoiceEmailOnOrder'])
    {
        return;
    }
    
    //get invoice id 
    $invoice_id = $params['invoiceid'];
    
    //include required files
    require_once dirname(__FILE__).DIRECTORY_SEPARATOR.'core.php';
    
    $invoice = mysql_get_row("SELECT status, userid FROM tblinvoices WHERE id = ?", array($invoice_id));
    $items = mysql_get_array("SELECT i.relid, h.packageid
        FROM tblinvoiceitems i
        LEFT JOIN tblhosting h ON i.relid = h.id
        WHERE i.invoiceid = ? AND i.type = 'Hosting'", array($invoice_id));
      
    foreach($items as &$item)
    {
        $hosting_id =   $item['relid'];
        $product_id =   $item['packageid'];
        
        $row = mysql_get_row("SELECT enable, billing_settings, module FROM OnAppBilling_settings WHERE product_id = ? AND enable = 1", array($product_id));
        if(!$row)
        {
            continue;
        }
        
        $billing_settings = unserialize($row['billing_settings']);

        //Should we bill this account?
        if(!$billing_settings['bill_on_invoice_generate'])
        {
            continue;
        }
        
        if($billing_settings['credit_billing']['enable'] == 1)
        {
            continue;
        }

        //Create Product Class
        $p = new OAProduct($product_id);
        //Get Server Type
        $type = $p->getServerType();
        //Get First Record
        $first_record = mysql_get_row("SELECT DATE(`date`) as `date` FROM OnAppBilling_".$type."_prices 
                    WHERE product_id = ? AND hosting_id = ? ORDER BY record_id ASC LIMIT 1", array($product_id, $hosting_id));
        if(!$first_record)
        {
            continue;
        }

        $start_date =   $first_record['date'];
        $end_date   =   date('Y-m-d');

        if(strtolower($invoice['status']) == 'unpaid')
        {
            $invoice_id = OnAppBillingUpdateInvoice($invoice_id, $hosting_id, $product_id, $start_date, $end_date, array(
                'autoapplycredit'   =>  $billing_settings['autoapplycredit'],
                'duedate'           =>  $billing_settings['billing_duedate']
            ));

            if($invoice_id !== false)
            {
                OnAppBillingDeleteUsageRecord($hosting_id, $product_id, $start_date, $end_date);
            } 
        }
        else
        {
            $invoice_id = OnAppBillingGenerateInvoice($hosting_id, $product_id, $start_date, $end_date, array(
                'autoapplycredit'   =>  $billing_settings['autoapplycredit'],
                'duedate'           =>  $billing_settings['billing_duedate']
            ));

            if($invoice_id !== false)
            {
                OnAppBillingDeleteUsageRecord($hosting_id, $product_id, $start_date, $end_date);
            } 
        }
    }
}
add_hook('InvoiceCreationPreEmail', 100, 'OnAppBilling_InvoiceCreationPreEmail');


/**
 * 
 * @global type $CONFIG
 * @param type $params
 * @return type
 */
function OnAppBilling_InvoiceCreated($params)
{
    global $CONFIG;
    if(!$CONFIG['NoInvoiceEmailOnOrder'])
    {
        return;
    }

    //get invoice id 
    $invoice_id = $params['invoiceid'];
    
    //include required files
    require_once dirname(__FILE__).DIRECTORY_SEPARATOR.'core.php';
    
    $invoice = mysql_get_row("SELECT status, userid FROM tblinvoices WHERE id = ?", array($invoice_id));
    $items = mysql_get_array("SELECT i.relid, h.packageid
        FROM tblinvoiceitems i
        LEFT JOIN tblhosting h ON i.relid = h.id
        WHERE i.invoiceid = ? AND i.type = 'Hosting'", array($invoice_id));
    
    foreach($items as &$item)
    {
        $hosting_id =   $item['relid'];
        $product_id =   $item['packageid'];
        
        $row = mysql_get_row("SELECT enable, billing_settings, module FROM OnAppBilling_settings WHERE product_id = ? AND enable = 1", array($product_id));
        if(!$row)
        {
            continue;
        }
        
        $billing_settings = unserialize($row['billing_settings']);
        
        //Should we bill this account?
        if(!$billing_settings['bill_on_invoice_generate'])
        {
            continue;
        }
        
        if($billing_settings['credit_billing']['enable'] == 1)
        {
            continue;
        }

        //Create Product Class
        $p = new OAProduct($product_id);
        //Get Server Type
        $type = $p->getServerType();
        //Get First Record
        $first_record = mysql_get_row("SELECT DATE(`date`) as `date` FROM OnAppBilling_".$type."_prices 
                    WHERE product_id = ? AND hosting_id = ? ORDER BY record_id ASC LIMIT 1", array($product_id, $hosting_id));
        if(!$first_record)
        {
            continue;
        }

        $start_date =   $first_record['date'];
        $end_date   =   date('Y-m-d');

        if(strtolower($invoice['status']) == 'unpaid')
        {
            $invoice_id = OnAppBillingUpdateInvoice($invoice_id, $hosting_id, $product_id, $start_date, $end_date, array(
                'autoapplycredit'   =>  $billing_settings['autoapplycredit'],
                'duedate'           =>  $billing_settings['billing_duedate']
            ));

            if($invoice_id !== false)
            {
                OnAppBillingDeleteUsageRecord($hosting_id, $product_id, $start_date, $end_date);
            } 
        }
        else
        {
            $invoice_id = OnAppBillingGenerateInvoice($hosting_id, $product_id, $start_date, $end_date, array(
                'autoapplycredit'   =>  $billing_settings['autoapplycredit'],
                'duedate'           =>  $billing_settings['billing_duedate']
            ));

            if($invoice_id !== false)
            {
                OnAppBillingDeleteUsageRecord($hosting_id, $product_id, $start_date, $end_date);
            } 
        }
    }
}
add_hook('InvoiceCreated', 100, 'OnAppBilling_InvoiceCreated');

/**
 * Create invoice when product package is changed
 * @param type $params
 * @return type
 */
function OnAppBilling_AfterProductUpgrade($params)
{
    //include required files
    require_once dirname(__FILE__).DIRECTORY_SEPARATOR.'core.php';
    
    $upgrade_id     =   $params['upgradeid'];
    $upgrade        =   mysql_get_row("SELECT u.*, c.id as client_id
                                        FROM tblupgrades u
                                        LEFT JOIN tblhosting h ON u.relid = h.id
                                        LEFT JOIN tblclients c ON h.userid = c.id
                                        WHERE u.id = ? AND u.type = 'package'", array($upgrade_id));
    $hosting_id     =   $upgrade['relid'];
    $product_id     =   $upgrade['originalvalue'];
    $user_id        =   $upgrade['client_id'];

    $row = mysql_get_row("SELECT billing_settings, module FROM OnAppBilling_settings WHERE product_id = ? AND enable = 1", array($product_id));
    if(!$row)
    {
        return false;
    }
        
    $billing_settings = unserialize($row['billing_settings']);
    
    //Create Product Class
    $p = new OAProduct($product_id);
    //Get Server Type
    $type = $p->getServerType();
    //Get First Record
    $first_record = mysql_get_row("SELECT DATE(`date`) as `date` FROM OnAppBilling_".$type."_prices 
                WHERE product_id = ? AND hosting_id = ? ORDER BY record_id ASC LIMIT 1", array($product_id, $hosting_id));
    if(!$first_record)
    {
        return false;
    }
    
    $start_date =   $first_record['date'];
    $end_date   =   date('Y-m-d');

    if($billing_settings['autogenerate_invoice'] == 1)
    {
        $invoice_id = OnAppBillingGenerateInvoice($hosting_id, $product_id, $start_date, $end_date, array(
            'autoapplycredit'   =>  $billing_settings['autoapplycredit'],
            'duedate'           =>  $billing_settings['billing_duedate']
        ));

        if($invoice_id !== false)
        {
            OnAppBillingDeleteUsageRecord($hosting_id, $product_id, $start_date, $end_date);
        } 
    }
    else
    {
        $invoice_id = OnAppBillingGenerateAwaitingInvoice($hosting_id, $product_id, $start_date, $end_date, array(
            'autoapplycredit'   =>  $billing_settings['autoapplycredit'],
            'duedate'           =>  $billing_settings['billing_duedate']
        ));

        if($invoice_id !== false)
        {
            OnAppBillingDeleteUsageRecord($hosting_id, $product_id, $start_date, $end_date);
        }
    }
}
add_hook('AfterProductUpgrade', 100,  'OnAppBilling_AfterProductUpgrade'); 


function OnAppBilling_AfterModuleChangePackage($params)
{
    if(!$_SESSION['adminid'])
    {
        return;
    }
        //include required files
    require_once dirname(__FILE__).DIRECTORY_SEPARATOR.'core.php';

    
    $current_pid    =   $params['params']['pid'];
    $hosting_id     =   $params['params']['serviceid'];
    $user_id        =   $params['clientsdetails']['id'];
 
    $products = mysql_get_array("SELECT product_id FROM OnAppBilling_settings WHERE enable = 1");
     
    foreach($products as $product)
    {
        $product_id = $product['product_id'];
        
        if($current_pid == $product_id)
        {
            continue;
        }
        //Create Product Class
        $p = new OAProduct($product_id);
        //Get Server Type
        $type = $p->getServerType();
        //Get First Record
        $first_record = mysql_get_row("SELECT DATE(`date`) as `date` FROM OnAppBilling_".$type."_prices 
                    WHERE product_id = ? AND hosting_id = ? ORDER BY record_id ASC LIMIT 1", array($product_id, $hosting_id));
        if(!$first_record)
        {
            continue;
        }
        
        //Get Billing Settings
        $row = mysql_get_row("SELECT billing_settings, module FROM OnAppBilling_settings WHERE product_id = ? AND enable = 1", array($product_id));
        $billing_settings = unserialize($row['billing_settings']);
        

        $start_date =   $first_record['date'];
        $end_date   =   date('Y-m-d');

        if($billing_settings['autogenerate_invoice'] == 1)
        {
            $invoice_id = OnAppBillingGenerateInvoice($hosting_id, $product_id, $start_date, $end_date, array(
                'autoapplycredit'   =>  $billing_settings['autoapplycredit'],
                'duedate'           =>  $billing_settings['billing_duedate']
            ));

            if($invoice_id !== false)
            {
                OnAppBillingDeleteUsageRecord($hosting_id, $product_id, $start_date, $end_date);
            } 
        }
        else
        {
            $invoice_id = OnAppBillingGenerateAwaitingInvoice($hosting_id, $product_id, $start_date, $end_date, array(
                'autoapplycredit'   =>  $billing_settings['autoapplycredit'],
                'duedate'           =>  $billing_settings['billing_duedate']
            ));

            if($invoice_id !== false)
            {
                OnAppBillingDeleteUsageRecord($hosting_id, $product_id, $start_date, $end_date);
            }
        }
    }
}
add_hook('AfterModuleChangePackage', 100, 'OnAppBilling_AfterModuleChangePackage');


/**
 * Delete Account Data When Hosting Was Deleted
 * @param type $params
 */
function OnAppBilling_ServiceDelete($params)
{
    //include required files
    require_once dirname(__FILE__).DIRECTORY_SEPARATOR.'core.php';
    
    $products = mysql_get_array("SELECT product_id FROM OnAppBilling_settings");
    
    foreach($products as $product)
    {
        $product_id = $product['product_id'];
        //Delete Usage Records 
        OnAppBillingDeleteUsageRecord($params['serviceid'], $product_id);
    }
}
add_hook('ServiceDelete', 100, 'OnAppBilling_ServiceDelete');