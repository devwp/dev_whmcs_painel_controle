<?php
if(!class_exists('OnAppBillingEventManager'))
{
    class OnAppBillingEventManager
    {
        static $events = array();

        static public function register($eventName)
        {
            if(!isset(self::$events[$eventName]))
            {
                self::$events[$eventName] = array();
            }
        }

        static public function attach($eventName, $callback)
        {
            if(isset(self::$events[$eventName]))
            {
                if(!in_array($callback, self::$events[$eventName]))
                {
                    self::$events[$eventName][] = $callback;
                    return true;
                }
            }

            return false;
        }

        static public function detach($eventName, $callback)
        {
            if(isset(self::$events[$eventName]))
            {
                foreach(self::$events[$eventName] as $eventKey => $eventCallback)
                {
                    if($eventCallback === $callback)
                    {
                        unset(self::$events[$eventName][$eventKey]);
                    }
                }
            }

            return false;
        }

        static function call($eventName /*$param, $param, $param */)
        {
            $params = func_get_args();
            unset($params[0]);
            
            if(self::$events[$eventName])
            {
                foreach(self::$events[$eventName] as $event)
                {
                    call_user_func_array($event, $params);
                }
            }
        }
    }
}