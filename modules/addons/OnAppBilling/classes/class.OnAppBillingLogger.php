<?php
class OnAppBillingLogger
{
    static function error($error)
    {
        if(!trim($error))
        {
            return;
        }
        
        $file = dirname(__FILE__).DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'cron'.DIRECTORY_SEPARATOR.'logs'.DIRECTORY_SEPARATOR.'errorlog-'.gmdate('Y-m').'.log';
        @file_put_contents($file, date("Y-m-d G:i:s").': '.$error."\n", FILE_APPEND);
    }
     
    static function info($info)
    {
        if(!trim($info))
        {
            return;
        }
        
        $file = dirname(__FILE__).DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'cron'.DIRECTORY_SEPARATOR.'logs'.DIRECTORY_SEPARATOR.'infolog-'.gmdate('Y-m').'.log';
        @file_put_contents($file, date("Y-m-d G:i:s").': '.$info."\n", FILE_APPEND);
    }
    
    static function crtitical($crtitical)
    {
        if(!trim($crtitical))
        {
            return;
        }
        
        $file = dirname(__FILE__).DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'cron'.DIRECTORY_SEPARATOR.'logs'.DIRECTORY_SEPARATOR.'criticallog-'.gmdate('Y-m').'.log';
        @file_put_contents($file, date("Y-m-d G:i:s").': '.$crtitical."\n", FILE_APPEND);
    }
}