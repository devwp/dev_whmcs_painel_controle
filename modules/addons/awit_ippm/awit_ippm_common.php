<?php
/*
 * AWIT IPPM - Common functions
 * Copyright (c) 2013-2014, AllWorldIT
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Admin user of biling module
$AWIT_IPPM_ADMIN_USER = 'awit_ippm';

// Internal version
$AWIT_IPPM_IVER = '201308200943';

// Our global supported field list & default values
$AWIT_IPPM_SUPPORTED_FIELDS = array(
		"invoice_include_days" => "5",

		"sales_person" => "Sales person",
		"sales_commission" => "Sales commission",
		"sales_commission_s" => "A=50,B=30,C=20,D=15,E=10,F=7.5,G=5,H=2.5",

		"billing_type" => "Billing type",
		"billing_start" => "Billing start",
		"billing_end" => "Billing end",
		"billing_period_start" => "Billing period start",

		"variable_quantity" => "Variable quantity",
		"variable_amount" => "Variable amount",
		"variable_identifier" => "Variable identifier",
		"variable_attributes" => "Variable attributes",

		"contract_start" => "Contract start",
		"contract_term" => "Contract term",

		"commitment_value" => "Commitment value",
		"commitment_calculation" => "Commitment calculation",

		"bulk_discount_calculation" => "Bulk discount calculation",

		// Only set if we're live
		"version" => null,
);

// Set in our hook
$AWIT_IPPM_CONFIG_CUSTOM_FIELDS = array();
$AWIT_IPPM_CONFIG_CUSTOM_FIELDS_REVERSE = array();


/**
 * Log debugging information
 * Set a level from 1 to 5, 5 is most verbose
 */
// prefix, level, format, argument_list...
function log_debug2()
{
	// Grab function arguments
	$args = func_get_args();
	$prefix = array_shift($args);
	$level = array_shift($args);
	$format = array_shift($args);

	// Check if we have a log level defined
	if (!defined('AWIT_IPPM_LOG_LEVEL')) {
		return false;
	}

	// Setup a timestamp
	$timestamp = strftime('%Y-%m-%d %H:%M:%S', time());
	// Format message
	$message = vsprintf($format,$args);

	// If we have a file, log to file, or log to stderr
	if (defined('AWIT_IPPM_LOG_FILE') && AWIT_IPPM_LOG_FILE) {
		if ($level <= AWIT_IPPM_LOG_LEVEL) {
			$fp = fopen(AWIT_IPPM_LOG_FILE,'a');
			fwrite($fp,"$timestamp [$prefix] $message\n");
			fflush($fp);
		}
	} else {
		$fp = fopen('/dev/stderr','a');
		fwrite($fp,"$timestamp [$prefix] $message\n");
		fflush($fp);
		fclose($fp);
	}
}



/*
 * Function to display date in a pretty way
 */
function log_debug2_pretty_date($date)
{
	return isset($date) ? (gettype($date) == "object" ? $date->format('Y-m-d') : $date) : '-';
}



/*
 * Function to display a variable in a pretty way
 */
function log_debug2_pretty_variable($var)
{
	return isset($var) ? $var : '-';
}


//
// DEFINITIONS:
//
// Service Days / Service Date - service as in how many days in a month one *could* benefit from a service/product
// Billing Days / Billing Date - billing as in the actual amount of service/product rendered, which client pays for
//
//


/**
 * Applies the commitment calculation to per G/byte
 * products where Commitment Calculation, Commitment Value and Variable Quantity
 * custom fields are specified.
 */
function awit_ippm_doCommitmentBilling($invoice_id, $live)
{
	global $AWIT_IPPM_ADMIN_USER;
	global $AWIT_IPPM_CONFIG_CUSTOM_FIELDS;


	log_debug2('CBILLING',1,'Processing invoice [%s] for [CommitmentBilling]',$invoice_id);

	// Grab the invoice
	$invoice = localAPI('getinvoice',array('invoiceid' => $invoice_id),$AWIT_IPPM_ADMIN_USER);

	// We cache our invoice items between the two loops
	$invoiceData = array();

	// Check if we doing commitment billing
	$hasCommitmentCalculationField = 0;
	foreach ($invoice['items']['item'] as $invoiceItemKey => $invoiceItem) {
		// Grab product info
		$invoiceData[$invoiceItemKey] = list($cproduct,$customFields,$a_invoiceItem) = awit_ippm_getProductInfo($invoiceItem);
		// Flip the flag if we're doing commitment calculation billing
		if (isset($customFields['commitment_calculation'])) {
			$hasCommitmentCalculationField = 1;
		}
	}
	// If we not there is no use continuing
	if (!$hasCommitmentCalculationField) {
		log_debug2('CBILLING',3,'Skipping invoice, no commitment calculation billing on this invoice');
		return false;
	}

	// Updates to process for this invoice
	$invoiceUpdates = array();

	// Loop with invoice items
	foreach ($invoiceData as $invoiceItemData) {
		// Grab items we need
		list($cproduct,$customFields,$invoiceItem) = $invoiceItemData;

		log_debug2('CBILLING',1,'Product: [%s], Domain: [%s]',$cproduct['name'],$cproduct['domain']);

		// Continue if this is not a commitment line
		if (empty($customFields['commitment_calculation'])) {
			continue;
		}

		// Skip if somethings wrong
		if (isset($customFields['commitment_calculation']) && !isset($customFields['commitment_value'])) {
			log_debug2('CBILLING',1,'ERROR: Validation on field combination [%s][%s], [%s][%s], [%s][%s] failed',
				log_debug2_pretty_variable($AWIT_IPPM_CONFIG_CUSTOM_FIELDS['commitment_calculation']),
				log_debug2_pretty_variable($customFields['commitment_calculation']),
				log_debug2_pretty_variable($AWIT_IPPM_CONFIG_CUSTOM_FIELDS['commitment_value']),
				log_debug2_pretty_variable($customFields['commitment_value'])
			);
			continue;
		}

		// Check Variable Quantity
		$variableQuantity = NULL;
		if ($value = $customFields['variable_quantity']) {
			// Check for an integer
			if (preg_match('/^[0-9.]+$/',$value)) {
				// Grab variable quantity value
				$variableQuantity = $value;
			} else {
				log_debug2('CBILLING',1,'ERROR: Validation on field [%s] with value [%s] failed',
					log_debug2_pretty_variable($AWIT_IPPM_CONFIG_CUSTOM_FIELDS['variable_quantity']),
					log_debug2_pretty_variable($value)
				);
				continue;
			}
		}

		// Check Commitment Value
		$commitmentValue = NULL;
		if ($value = $customFields['commitment_value']) {
			// Check for a floating point number
			if (preg_match('/^[+-]?[0-9]*\.?[0-9]*$/',$value)) {
				// Grab commitment value
				$commitmentValue = $value;
			} else {
				log_debug2('CBILLING',1,'ERROR: Validation on field [%s] with value [%s] failed',
					log_debug2_pretty_variable($AWIT_IPPM_CONFIG_CUSTOM_FIELDS['commitment_value']),
					log_debug2_pretty_variable($value)
				);
				continue;
			}
		}

		// Check commitment caclculation
		$commitmentCalculation = NULL;
		if ($value = $customFields['commitment_calculation']) {
			// Check for a valid commitments
			// 65=0.25,66=0.33,69=1,70=1,71=1.5,72=1.5
			if (preg_match('/^\[((?:[0-9]+=[0-9.]+,?)+)\]$/',$value,$matches)) {
				// Pull in commitment calculation minus the []
				$commitmentCalculation = $matches[1];
			} else {
				log_debug2('CBILLING',2,'ERROR: Validation on field [%s] value [%s] failed',
					$AWIT_IPPM_CONFIG_CUSTOM_FIELDS['commitment_calculation'],
					log_debug2_pretty_variable($value)
				);
				continue;
			}
		}

		log_debug2('CBILLING',3,'Service info - VariableQuantity [%s], CommitmentValue [%s], CommitmentCalculation [%s]',
			log_debug2_pretty_variable($variableQuantity),
			log_debug2_pretty_variable($commitmentValue),
			log_debug2_pretty_variable($commitmentCalculation)
		);

		// Mapping of pid's to multipliers
		$commitmentCalculationMapping = array();
		foreach (explode(',', $commitmentCalculation) as $item) {
			list($pid,$multiplier) = explode('=', $item);
			// Check attribute is valid
			if (!isset($pid) || !isset($multiplier)) {
				log_debug2('CBILLING',1,'ERROR: Commitment calculation item [%s] is invalid',
					$item
				);
				continue;
			}
			// Check for duplicates
			if (in_array($pid,array_keys($commitmentCalculationMapping))) {
				log_debug2('CBILLING',2,'WARNING: Commitment calculation item [%s] is duplicated',
					$item
				);
				continue;
			}

			$commitmentCalculationMapping[$pid] = $multiplier;
		}

		// Loop with invoice items
		$variableQuantityTotal = 0;
		foreach ($invoiceData as $v_invoiceItemData) {
			// Grab product info
			list($a_cproduct,$a_customFields,$a_cproduct) = $v_invoiceItemData;

			// If this is part of our calculation, then lets add it up
			if ($multiplier = $commitmentCalculationMapping[$a_cproduct['pid']]) {
# FIXME: rounding as per variable attributes?
				// Calculate our new total
				$variableQuantityNewTotal = $variableQuantityTotal + ($a_customFields['variable_quantity'] * $multiplier);

				log_debug2('CBILLING',4,'Using [%s] domain [%s], VariableQuantityTotal updated [%s] = CommitmentQuantity [%s] + (VariableQuantity [%s] * Multiplier [%s])',
					$a_cproduct['name'], $a_cproduct['domain'],
					$variableQuantityNewTotal,
					$variableQuantityTotal,
					$a_customFields['variable_quantity'],
					$multiplier
				);

				// Save our new quantity
				$variableQuantityTotal = $variableQuantityNewTotal;
			}
		}

		// Only populate shortfall if its > 0
		$shortFall = $customFields['commitment_value'] - $variableQuantityTotal;
		$shortFall = ($shortFall > 0) ? $shortFall : 0;

		log_debug2('CBILLING',3,'Calculated VariableQuantityTotal [%s], ShortFall [%s]',
			$variableQuantityTotal,
			$shortFall
		);

		// If there is no shortfall, skip adding it to the invoice
		if (!$shortFall) {
			continue;
		}

		$shortFallRounded = ceil($shortFall);

		// If we do calculate the item data
		$itemDescription = sprintf("%s\nCommitment ShortFall = %.0f (Minimum Commitment %.0f - Total Quantity %.2f = %.2f)",
			$invoiceItem['description'],
			$shortFallRounded,
			$customFields['commitment_value'],
			$variableQuantityTotal,
			$shortFall
		);
		// Calculate the amount
		$itemAmount = sprintf('%.2f',$shortFallRounded * $cproduct['recurringamount']);

		log_debug2('CBILLING',3,'+ Adding commitment shortfall, ItemDescription [%s] and ItemAmount [%.2f]',
			str_replace("\n", '\n', $itemDescription),
			$itemAmount
		);
		// Setup new item we're adding to the invoice
		$invoiceUpdates['newitemdescription'][$invoiceItem['id']] = $itemDescription;
		$invoiceUpdates['newitemamount'][$invoiceItem['id']] = $itemAmount;
		$invoiceUpdates['newitemtaxed'][$invoiceItem['id']] = $invoiceItem['taxed'];
	}

	awit_ippm_updateInvoice($invoice_id,$invoiceUpdates,$live);

	log_debug2('CBILLING',1,'Processing [CommitmentBilling] completed on invoice [%s]',$invoice_id);
}



/**
 * Performs updates to variable custom fields
 * based on billing variable reports.
 */
function awit_ippm_doVariableBilling($invoice_id, $live)
{
	global $AWIT_IPPM_ADMIN_USER;
	global $AWIT_IPPM_CONFIG_CUSTOM_FIELDS;


	log_debug2('VBILLING',1,'Processing invoice [%s] for [VariableBilling]',$invoice_id);

	// Grab the invoice
	$invoice = localAPI('getinvoice',array('invoiceid' => $invoice_id),$AWIT_IPPM_ADMIN_USER);

	// Updates to process for this invoice
	$invoiceUpdates = array();

	// Loop with invoice items
	foreach ($invoice['items']['item'] as $invoiceItem) {
		// Grab product info
		list($cproduct,$customFields) = awit_ippm_getProductInfo($invoiceItem);

		// if we have a supported variable field, update the line item accordingly
		// if not check the next item
		if (
				!isset($customFields['variable_quantity']) &&
				!isset($customFields['variable_amount']) &&
				!isset($customFields['variable_identifier']) &&
				!isset($customFields['variable_attributes'])
		) {
			log_debug2('VBILLING',3,'Skipping for line item, no variable billing fields set');
			continue;
		}

		// Check Variable Quantity
		$variableQuantity = NULL;
		if ($value = $customFields['variable_quantity']) {
			// check for an integer
			if (preg_match('/^[0-9\.]+$/',$value)) {
				// Pull in variable quantity
				$variableQuantity = $value;
			} else {
				log_debug2('VBILLING',1,'ERROR: Validation on field [%s] value [%s] failed',
					$AWIT_IPPM_CONFIG_CUSTOM_FIELDS['variable_quantity'],
					log_debug2_pretty_variable($value)
				);
				continue;
			}
		}

		// Check Variable Amount
		$variableAmount = NULL;
		if ($value = $customFields['variable_amount']) {
			//check for a floating point number
			if (preg_match('/^[0-9\.]+$/',$value)) {
				// Pull in variable amount
				$variableAmount = $value;
			} else {
				log_debug2('VBILLING',1,'ERROR: Validation on field [%s] value [%s] failed',
					$AWIT_IPPM_CONFIG_CUSTOM_FIELDS['variable_amount'],
					log_debug2_pretty_variable($value)
				);
				continue;
			}
		}

		// Check Variable Identifier
		$variableIdentifier = NULL;
		if ($value = $customFields['variable_identifier']) {
			// check for a valid identifier e.g. [system=radius,type=usage,index=realm,realm=rsadsl,class=shaped]
			if (preg_match('/^\[(.*)=(.*)[,]?(.*)\]$/',$value)) {
				// Pull in variable identifier
				$variableIdentifier = $value;
			} else {
				log_debug2('VBILLING',1,'ERROR: Validation on field [%s] value [%s] failed',
					$AWIT_IPPM_CONFIG_CUSTOM_FIELDS['variable_identifier'],
					log_debug2_pretty_variable($value)
				);
				continue;
			}
		}

		// Check Variable Attributes
		$variableAttributesStr = NULL;
		if ($value = $customFields['variable_attributes']) {
			// check for a valid Attributes e.g. [round=alwaysup]
			if (preg_match('/^\[(.*=.*,?.*)\]$/',$value,$matches)) {
				// Pull in variable attributes
				$variableAttributeStr = $matches[1];
			} else {
				log_debug2('VBILLING',2,'ERROR: Validation on field [%s] value [%s] failed',
					$AWIT_IPPM_CONFIG_CUSTOM_FIELDS['variable_attributes'],
					log_debug2_pretty_variable($value)
				);
				continue;
			}
		}

		// We cannot work if there are unset items
		if (($variableAmount != NULL && $variableQuantity != NULL)) {
			log_debug2('VBILLING',1,'ERROR: Validation on field combination [%s][%s], [%s][%s], [%s][%s] failed',
				log_debug2_pretty_variable($AWIT_IPPM_CONFIG_CUSTOM_FIELDS['variable_quantity']),
				log_debug2_pretty_variable($variableQuantity),
				log_debug2_pretty_variable($AWIT_IPPM_CONFIG_CUSTOM_FIELDS['variable_amount']),
				log_debug2_pretty_variable($variableAmount),
				log_debug2_pretty_variable($AWIT_IPPM_CONFIG_CUSTOM_FIELDS['variable_identifier']),
				log_debug2_pretty_variable($variableIdentifier)
			);
			continue;
		}

		// Parsing variable attributes
		$variableAttributeList = explode(',', $variableAttributeStr);
		foreach ($variableAttributeList as $variableAttribute) {
			list($attr,$value) = explode('=', $variableAttribute);
			// Check attribute is valid
			if (!isset($attr) || !isset($value)) {
				log_debug2('VBILLING',1,'ERROR: Variable attribute [%s] is invalid',
					$variableAttribute
				);
				continue;
			}
			// Check for duplicates
			if (in_array($attr,array_keys($variableAttributes))) {
				log_debug2('VBILLING',2,'Variable attribute [%s] is duplicated',
					$variableAttribute
				);
				continue;
			}

			$variableAttributes[strtolower(trim($attr))] = strtolower(trim($value));
		}

		// Check if we processing a variable amount
		if ($variableAmount) {
			// Updating the invoice item
			// NK: we use %.2f here so we don't get decimals beyond 2 places, the data we get from servers could have many
			$invoiceUpdates['itemamount'][$invoiceItem['id']] = sprintf("%.2f",$variableAmount);

			// CM: for when processing variable attribute calculations
			$invoiceItem['amount'] = $invoiceUpdates['itemamount'][$invoiceItem['id']];

			$extraDesc = sprintf("\nVariable Amount: %.2f",$variableAmount);

			$invoiceUpdates['itemdescription'][$invoiceItem['id']] = $invoiceItem['description'] . $extraDesc;
			$invoiceItem['description'] = $invoiceUpdates['itemdescription'][$invoiceItem['id']];
			$invoiceUpdates['itemtaxed'][$invoiceItem['id']] = $invoiceItem['taxed'];

			log_debug2('VBILLING',3,'~ Adding variable amount to description [%s] with VariableAmount [%s]',
				str_replace("\n", '\n', $extraDesc),
				$variableAmount
			);

		} elseif ($variableQuantity) {
			// no api call to update product quantity
			//$query = "UPDATE tblproducts SET qty = '$variableQuantity' WHERE id = '".$cproduct['pid']."'";

			// Do rounding
			$variableQuantityOrig = $variableQuantity;
			if (isset($variableAttributes['round'])) {
				# Check and do rounding
				if ($variableAttributes['round'] == "up") {
					$variableQuantity = round($variableQuantity, 0, PHP_ROUND_HALF_UP);
				} elseif ($variableAttributes['round'] == "down") {
					$variableQuantity = round($variableQuantity, 0, PHP_ROUND_HALF_Down);
				} elseif ($variableAttributes['round'] == "alwaysup") {
					$variableQuantity = ceil($variableQuantity);
				} elseif ($variableAttributes['round'] == "alwaysdown") {
					$variableQuantity = floor($variableQuantity);
				} else {
					log_debug2('VBILLING',1,'ERROR: Variable quantity cannot be rounded, rounding type is invalid [%s]',
						$variableAttributes['round']
					);
				}
				# If we rounded, log or ERR
				if ($variableQuantityOrig) {
					log_debug2('VBILLING',3,'Variable quantity rounded [%s] from [%s] to [%s]',
						$variableAttributes['round'],
						$variableQuantityOrig,
						$variableQuantity
					);
				}
			}

			// NK: we use %.2f here so we don't get decimals beyond 2 places during multiplication
			$newAmount = sprintf("%.2f",$invoiceItem['amount'] * $variableQuantity);

			$itemDescription = sprintf("%s\nVariable Calculation: %.2f @%.2f",
					$invoiceItem['description'],
					$variableQuantity,
					$invoiceItem['amount']
			);

			log_debug2('CBILLING',2,'~ Adding variable quanitty to item description [%s] with Amount [%s], VariableQuantityOrig [%s], VariableQuantity [%s], NewAmount [%s]',
				str_replace("\n", '\n', $itemDescription),
				$invoiceItem['amount'],
				$variableQuantityOrig,
				$variableQuantity,
				$newAmount
			);

			// Push the changes to update
			$invoiceUpdates['itemdescription'][$invoiceItem['id']] = $itemDescription;
			$invoiceUpdates['itemamount'][$invoiceItem['id']] = $newAmount;
			$invoiceUpdates['itemtaxed'][$invoiceItem['id']] = $invoiceItem['taxed'];

			// Override for when processing variable attribute calculations
# NK: not sure about this
#			$invoiceItem['description'] = $invoiceUpdates['itemdescription'][$invoiceItem['id']];
#			$invoiceItem['amount'] = $invoiceUpdates['itemamount'][$invoiceItem['id']];

		}

	}

	awit_ippm_updateInvoice($invoice_id,$invoiceUpdates,$live);

	log_debug2('VBILLING',1,'Processing [VariableBilling] completed on invoice [%s]',$invoice_id);
}



/**
 * Performs prepaid, current and postpaid billing.
 * Adds back billing line items where needed.
 */
function awit_ippm_doCustomBilling($invoice_id, $live)
{
	global $AWIT_IPPM_ADMIN_USER;
	global $AWIT_IPPM_CONFIG_CUSTOM_FIELDS;


	log_debug2('BILLING',1,'Processing invoice [%s] for [CustomBilling]',$invoice_id);

	// Grab the invoice
	$invoice = localAPI('getinvoice',array('invoiceid' => $invoice_id),$AWIT_IPPM_ADMIN_USER);

	// Arrays of our product updates so we process them in 1 go right at the end
	$cproductUpdates = array();
	$cproductCustomFieldUpdates = array();

	// Loop with invoice items
	foreach ($invoice['items']['item'] as $invoiceItemKey => $invoiceItem) {
		// Grab product info
		list($cproduct,$customFields) = awit_ippm_getProductInfo($invoiceItem);

		// Check if we have a supported field
		if (!isset($customFields['billing_type'])) {
			log_debug2('BILLING',3,'Skipping for line item, no billing fields set');
			continue;
		}

		// Updates to process for this invoice
		$invoiceUpdates = array();
		// Extra text to add to description
		$extraDesc = "";

		// Handling the products addons similarly to the actual product
		// This allows us to include the addons in our billing calculations
		if (strtolower($invoiceItem['type']) == 'addon') {
			// grabbing the connecting products hosting id
			$query = 'SELECT hostingid FROM tblhostingaddons WHERE id = ' . $invoiceItem['relid'] . '';
			$res = mysql_query($query);
			$row = mysql_fetch_assoc($res);
			// faking the relid here so the addon products will be treated like the tblhosting products
			// with access to all the custom fields and billing features for custom billing.
			$invoiceItem['relid'] = $row['hostingid'];
			log_debug2('BILLING',5,'Found addon, faking relid [%s]',$row['hostingid']);
		}

		// Save original invoice item
		$origInvoiceItem = $invoiceItem;

		// Client product details
		$cproduct_regdate = new DateTime($cproduct['regdate']);
		// Check billing start, check for override by custom field
		$cproduct_nextduedate = new DateTime($cproduct['nextduedate']);
		$cproduct_nextduedate_overridden = 0;
		if ($value = $customFields['billing_period_start']) {
			// XXX: Throw exception, n/a is also valid!!!!!
			if (preg_match('/^\d{4}-\d{2}-\d{2}$/',$value)) {
				// Pull in billing period start date
				$cproduct_nextduedate = new DateTime($value);
				$cproduct_nextduedate_overridden = 1;
				log_debug2('BILLING',2,'Overriding NextDueDate to [%s]',
					log_debug2_pretty_date($cproduct_nextduedate)
				);
			}
		}

		log_debug2('BILLING',3,'Billing line before => %s, FirstPaymentAmount [%s], RecurringAmount [%s]',
				str_replace("\n", '\n', $invoiceItem['description']),
				$cproduct['firstpaymentamount'],$cproduct['recurringamount']
		);

		// Shove in our token, so we can replace parts of the description later
		$cproduct_name = preg_replace('/^(.*)\(\d{4}-\d{2}-\d{2} - \d{4}-\d{2}-\d{2}\)(.*)$/m','$1@@@$2',$invoiceItem['description']);

		// Billing cycle
		if ($cproduct['billingcycle'] == "Monthly") {
			$billingCycle = 1;
		} elseif ($cproduct['billingcycle'] == "Quarterly") {
			$billingCycle = 3;
		} elseif ($cproduct['billingcycle'] == "Semi-Annually") {
			$billingCycle = 6;
		} elseif ($cproduct['billingcycle'] == "Annually") {
			$billingCycle = 12;
		} elseif ($cproduct['billingcycle'] == "Biennially") {
			$billingCycle = 24;
		} elseif ($cproduct['billingcycle'] == "Triennially") {
			$billingCycle = 36;
		} else {
			log_debug2('BILLING',1,'ERROR: Unknown billing cycle "%s"', $cproduct['billingcycle']);
			continue;
		}

		// Look for the stuff we support
		$billingType = strtolower($customFields['billing_type']);

		// If unsupported, err and skip
		if (!in_array($billingType, array('prepaid', 'postpaid', 'current'))) {
			log_debug2('BILLING',1,'ERROR: Unknown billing type "%s"', $billingType);
			continue;
		}

		// Set the invoice include days
		$invoiceIncludeDays = $AWIT_IPPM_CONFIG_CUSTOM_FIELDS['invoice_include_days'];
		if (!isset($invoiceIncludeDays) || empty($invoiceIncludeDays) || $invoiceIncludeDays < 1) {
			$invoiceIncludeDays = 0;
		}

		// Get how signup is aligned to our billing run
		$signupAlignDays = $cproduct_regdate->diff($cproduct_nextduedate)->format('%R%a');

		log_debug2('BILLING',3,'Service info - RegDate [%s], NextDueDate [%s], BillingCycleMonths [%s], SignupAlignDays [%s], InvoiceIncludeDays [%s]',
			log_debug2_pretty_date($cproduct_regdate),
			log_debug2_pretty_date($cproduct_nextduedate),
			$billingCycle,
			$signupAlignDays,
			$invoiceIncludeDays
		);

		// Check billing start
		$billingStartDate = NULL;
		if ($value = $customFields['billing_start']) {
			// XXX: Throw exception, n/a is also valid!!!!!
			if (preg_match('/^\d{4}-\d{2}-\d{2}$/',$value)) {
				// Pull in billing start date
				$billingStartDate = new DateTime($value);
			}
		}

		// Check billing end
		$billingEndDate = NULL;
		if ($value = $customFields['billing_end']) {
			// XXX: Throw exception, n/a is also valid!!!!!
			if (preg_match('/^\d{4}-\d{2}-\d{2}$/',$value)) {
				// Pull in billing end date
				$billingEndDate = new DateTime($value);
			}
		}

		log_debug2('BILLING',3,'Billing info - BillingType [%s], BillingStart [%s], BillingEndDate [%s]',
			$billingType,
			log_debug2_pretty_date($billingStartDate),
			log_debug2_pretty_date($billingEndDate)
		);

		// It is not possible for the billing next due date to lay before the billing start date
		if ($cproduct_nextduedate_overridden && $billingStartDate && $billingStartDate->diff($cproduct_nextduedate)->format('%R%a') < 0) {
			log_debug2('BILLING',1,'ERROR: It is not possible for the PeriodStart date to be BEFORE the BillingStartDate');
			continue;
		}

		// Setup the start & end dates based on billing cycle
		$billingCycleStartDate = NULL; $billingCycleEndDate = NULL;
		if ($billingType == "prepaid") {
			// If we have a billing start date use it, instead of using next due date, which would result in the start_date being in the past
			if ($billingStartDate) {
				// We bill for a FULL period here, so next due date, which is next month the 1st minus 1 month is 1st of this month
				$billingCycleStartDate = clone $billingStartDate;
			} else {
				$billingCycleStartDate = clone $cproduct_nextduedate;
			}
			// If we have a specific end date use it
			if ($billingEndDate) {
				$billingCycleEndDate = clone $billingEndDate;
			} else {
				$billingCycleEndDate = clone $cproduct_nextduedate;
				$billingCycleEndDate->modify("+$billingCycle month");
			}

		} elseif ($billingType == "current") {
			// If we have a billing start date use it, instead of using next due date, which would result in the start_date being in the past
			if ($billingStartDate) {
				// We bill for a FULL period here, so next due date, which is next month the 1st minus 1 month is 1st of this month
				$billingCycleStartDate = clone $billingStartDate;
			// Normal billing, we are already X days before next due date when the invoice is scheduled to be generated, so we want to set the
			// start period in the past for the billingCycle period
			} else {
				$billingCycleStartDate = clone $cproduct_nextduedate;
			}

			// If we have a specific end date use it
			if ($billingEndDate) {
				$billingCycleEndDate = clone $billingEndDate;
			} else {
				// Start off by setting the end period to the next due date
				$billingCycleEndDate = clone $cproduct_nextduedate;

				// In the case we have a >1 billing cycle, we bump up the month by that many months
				// Client signs up on the 30th, NDD - InvoiceDays = 25th
				if ($signupAlignDays <= $invoiceIncludeDays) {
					$billingCycleEndDate->modify("+$billingCycle month");
				}
			}

		} elseif ($billingType == "postpaid") {
			// If we have a specific start date use it
			if ($billingStartDate) {
				$billingCycleStartDate = clone $billingStartDate;
			// Or assume we had one, we invoiced for it, now we doing a full month run
			} else {
				$billingCycleStartDate = clone $cproduct_nextduedate;
				// Next due date is 1st August, we running on the 2nd of August
				$billingCycleStartDate->modify("-$billingCycle month");
			}

			// If we have a specific end date use it
			if ($billingEndDate) {
				$billingCycleEndDate = clone $billingEndDate;
			} else {
				// Set postpaid end date as next due date
				$billingCycleEndDate = clone $cproduct_nextduedate;
			}
		}

		// Day of the month our billing is on
		$cproduct_billingday = $cproduct_nextduedate->format('d');
		// Date that the service period starts, without pro-rata
		$serviceStartDate = clone $cproduct_nextduedate;
		// If we are starting billing, we are in the current billing period, not the next one
		if ($billingStartDate) {
			$serviceStartDate->modify("-$billingCycle month");
		}
		// Total service days, without pro-rata
		$totalServiceDays = $cproduct_nextduedate->diff($serviceStartDate)->days;
		// Total days we going to be billing for
		$totalBillingDays = $billingCycleEndDate->diff($billingCycleStartDate)->days;

		// Set display_start_date to start_date as a default value
		$display_start_date = clone $billingCycleStartDate;

		log_debug2('BILLING',3,'Billing params - CycleStartDate [%s], CycleEndDate [%s], BillingDay [%s], ServiceStartDate [%s], TotalServiceDays [%s], TotalBillingDays [%s]',
			log_debug2_pretty_date($billingCycleStartDate),
			log_debug2_pretty_date($billingCycleEndDate),
			$cproduct_billingday,
			log_debug2_pretty_date($serviceStartDate),
			$totalServiceDays,
			$totalBillingDays
		);

		// If item passed until here do some validation
		if ($billingCycleEndDate->diff($billingCycleStartDate)->format('%R%a') >= 0) {
			# Failing that, this is a odd circumstance
			log_debug2('BILLING',1,'ERROR: start_date [%s] greater than or equal to end_date [%s]',
				log_debug2_pretty_date($billingCycleStartDate), log_debug2_pretty_date($billingCycleEndDate)
			);

			continue;
		}
		// This can occur if a postpaid invoice has a due date before the billing start date, just skip it on this invoice
		if ($billingType == "postpaid" && $billingStartDate && $billingStartDate->diff($cproduct_nextduedate)->format('%R%a') > $invoiceIncludeDays) {
			log_debug2('BILLING',2,'- Postpaid service does not fall in this billing run - InvoiceIncludeDays [%s], NextDueDate [%s], StartDate [%s]. Removing from invoice.',
				log_debug2_pretty_variable($invoiceIncludeDays),
				log_debug2_pretty_date($cproduct_nextduedate),
				log_debug2_pretty_date($billingCycleStartDate)
			);

			// Check if we must remove the line or not
			$invoiceUpdates['deletelineids'][] = $invoiceItem['id'];
			goto SALES_END;
		}

		// If we don't have a billing start date, there is no reason to continue through the back billing
		if (!$billingStartDate) {
			goto BILLING_END;
		}

		/*
		 * START OF BACK BILLING
		 */

		// If we are busy billing for a paritial billing period WITHIN the SAME month, we need to modify the line item rather than backbill
		if ($totalServiceDays - $totalBillingDays > 0) {
			$invoiceItem['amount'] = sprintf('%.2f',($cproduct['recurringamount'] / $billingCycle) * ($totalBillingDays / $totalServiceDays) * $billingCycle);
			log_debug2('BILLING',4,'New partial amount [%s] (amount / billingCycle) * (totalBillingDays / totalServiceDays) * billingCycle',$invoiceItem['amount']);
		}

		// This is the day the client started the last service period, if he signed up on the 13th, its the 13th
		$lastServiceDate = clone $billingStartDate;

		// Set backbilling date
		$endBackBillingDate = clone $cproduct_nextduedate;
		// Bump postpaid back a billing cycle
		if ($billingType == 'postpaid') {
			$endBackBillingDate->modify("-$billingCycle month");
		}

		// While there are more than 0 days from last service date to next billing start period, run the below
		while (1) {
			// Check if we should be doing back billing
			$serviceAlignDays = $lastServiceDate->diff($endBackBillingDate)->format('%R%a');

			// If the service days is not matching the end of the last back-billing date:
			// 1. We check if its positive, which means in the past, this means possible backbilling applies
			// 2. Backbilling only applies if we have exceeded our NextDueDate - InvoiceIncludedDays
			//
			// 3. Or if we signed up past our invoice date
			$shouldBackBill = ($serviceAlignDays > 0 && ($serviceAlignDays <= $invoiceIncludedDays || $signupAlignDays <= $invoiceIncludeDays));

			log_debug2('BILLING',3,'BackBilling info - LastServiceDate [%s], BackBillingEndDate [%s], ServiceAlignDays = [%s], SignupAlignDays = [%s], '.
					'InvoiceIncludeDays = [%s], ProcessBackBilling [%s]',
				log_debug2_pretty_date($lastServiceDate), log_debug2_pretty_date($endBackBillingDate),
				$serviceAlignDays, $signupAlignDays, $invoiceIncludeDays, $shouldBackBill ? 'yes' : 'no'
			);

			// Break out of the while() if we shouldn't
			if (!$shouldBackBill) {
				break;
			}

			// Grab what would of been the last billing date
			$lastBillingDate = new DateTime($lastServiceDate->format('Y-m')."-".$cproduct_billingday);
			// Check if we've just been dropped in the current month AFTER the last service date
			// CASE: LastServiceDate = 20, Billing Day = 25 ... this would drop us to 25th of this month, which is wrong
			if ($lastServiceDate->diff($lastBillingDate)->format('%R%a') > 0) {
				log_debug2('BILLING',4,'Adjusting lastBillingDate to fall within the right month');
				# If we have, blow away a month to get the right billing month
				$lastBillingDate->modify("-1 month");
			}

			// Grab what would of been or would be the next billing date
			$nextBillingDate = clone $lastBillingDate;
			$nextBillingDate->modify("+1 month");

			// We need to track the service next billing day independantly, and before we may change it below
			$serviceNextBillingDate = clone $nextBillingDate;

			// Stop back billing up to the billingEndDate if the billingEndDate is before the next lastBillingDate
			// Set the description
			if ($billingEndDate) {
				if ($billingEndDate->diff($serviceNextBillingDate)->format('%R%a') > 0) {
					$nextBillingDate = clone $billingEndDate;
				}
			}

			// Calculate how many days the client had service & how many billing days there were
			$billingDays = $nextBillingDate->diff($lastServiceDate)->days;
			$serviceDays = $serviceNextBillingDate->diff($lastBillingDate)->days;

			// From the above we can then determine how much he is liable for paying
			$billingAmount = sprintf('%.2f',$cproduct['recurringamount'] / $billingCycle / $serviceDays * $billingDays);

			// Add invoice line item
			$lastServiceDay = clone $nextBillingDate;

			log_debug2('BILLING',3,'BackBilling info - LastBillingDate [%s], NextBillingDate [%s], ServiceNextBillingDate [%s], LastServiceDay [%s], Amount [%s]',
				log_debug2_pretty_date($lastBillingDate), log_debug2_pretty_date($nextBillingDate),
				log_debug2_pretty_date($serviceNextBillingDate), log_debug2_pretty_date($lastServiceDay),
				$billingAmount
			);

			// Only displaying previous months last day when billing ends on the 1st day of the month
			if ($lastServiceDay->format('d') == '01') {
				$lastServiceDay->modify("-1 day");
			}

			// Setup invoice updates
			$invoiceUpdates['newitemdescription'][] = preg_replace(
					'/@@@/',
					"(BackBill: ".$lastServiceDate->format('Y-m-d')." to ".$lastServiceDay->format('Y-m-d').")",
					$cproduct_name
			);
			$invoiceUpdates['newitemamount'][] = $billingAmount;
			$invoiceUpdates['newitemtaxed'][] = $invoiceItem['taxed'];

			// Bump last service date
			$lastServiceDate = $nextBillingDate;

			// Catch last billing date - Date to display as start date on the line item we got from whmcs
			$display_start_date = clone $nextBillingDate;

			log_debug2('BILLING',2,'+ BackBilling - BillingDays [%s], ServiceDays [%s], BillingAmount [%s], Description [%s]',
				$billingDays, $serviceDays, $billingAmount,
				str_replace("\n", '\n', $invoiceUpdates['newitemdescription'][count($invoiceUpdates['newitemdescription']) - 1])
			);

			// Stop back billing up to the billingEndDate if the billingEndDate is before the next lastBillingDate
			// Skip to the next invoice item
			if ($billingEndDate && $billingEndDate->diff($serviceNextBillingDate)->format('%R%a') > 0) {
				break;
			}
		}
		/*
		 * END OF BACK BILLING
		 */

BILLING_END:
		// We use end_date - 1 day so it appears to be inclusive of the last day 2010-01-01 to 2010-01-31 instead of to 2010-02-01
		$display_end_date = clone $billingCycleEndDate;

		// only displaying previous months last day when billing ends on the 1st day of the month
		if ($billingCycleEndDate->format('d') == '01') {
			$display_end_date->modify("-1 day");
		}

		$extraDesc = sprintf("(%s: %s to %s)",ucfirst($billingType),$display_start_date->format('Y-m-d'),$display_end_date->format('Y-m-d'));

		// If we overriding the next due date, set it up nicely
		if ($cproduct_nextduedate_overridden) {
			$nextduedate_new = clone $billingCycleEndDate;

# XXX: DO NOT DO THIS FOR CURRENT
			if ($billingType == 'postpaid') {
				$nextduedate_new->modify("-$billingCycle month");
			}

			$cproductUpdates[$invoiceItemKey]['nextduedate'] = $nextduedate_new->format('Y-m-d');
			log_debug2('BILLING',2,'Setting NextDueDate to [%s]',
				log_debug2_pretty_date($nextduedate_new)
			);
		}

		// Reset our custom fields
		$cproductCustomFieldUpdates[$invoiceItemKey]['_billing_start_id'] = "n/a";
		$cproductCustomFieldUpdates[$invoiceItemKey]['_billing_period_start_id'] = "n/a";


		/*
		 * Sales Features
		 */

		// Some vars we need
		$salesPerson = $customFields['sales_person'];
		$salesCommission = NULL;
		// Work out salesman comission
		if ($value = $customFields['sales_commission']) {
			if ($value > 0) {
				$salesCommission = sprintf("%.2f",$invoiceItem['amount'] * ($value / 100));
			}
		}

		// Do we have sales comission?
		if ($salesPerson && $salesCommission) {

			// Record these
			$table = "mod_awit_ippm";
			$values = array(
					"timestamp" => date("Y-m-d H:i:s"),
					"sales_person" => $salesPerson,
					"sales_commission" => $salesCommission,
					"invoice_id" => $invoiceItem['id']
			);
			$newid = insert_query($table,$values);

			// Mark as commission added
			$extraDesc .= "^";
		}

SALES_END:

		/*
		 * Make final changes
		 */

		// Check if we must adjust the invoice item description
		if (strlen($extraDesc) > 0) {

			$invoiceUpdates['itemdescription'][$invoiceItem['id']] = preg_replace('/@@@/',$extraDesc,$cproduct_name);
			$invoiceUpdates['itemamount'][$invoiceItem['id']] = $invoiceItem['amount'];
			$invoiceUpdates['itemtaxed'][$invoiceItem['id']] = $invoiceItem['taxed'];

			log_debug2('BILLING',2,'~ Change line item - Description [%s] => [%s], Amount [%s] => [%s]',
				$origInvoiceItem['description'], str_replace("\n", '\n', $invoiceUpdates['itemdescription'][$invoiceItem['id']]),
				$origInvoiceItem['amount'], $invoiceUpdates['itemamount'][$invoiceItem['id']]
			);
		}

		awit_ippm_updateInvoice($invoice_id,$invoiceUpdates,$live);
	}

	/*
	 * Update any fields for products that need to like billing start after the addons have been processed.
	 * We MUST do this AFTER we've processed the entire invoice above as addon ordering is not guaranteed
	 */
	foreach ($invoice['items']['item'] as $invoiceItemKey => $invoiceItem) {
		// Grab product info
		list($cproduct,$customFields) = awit_ippm_getProductInfo($invoiceItem);


		$billingType = strtolower($customFields['billing_type']);

		if (in_array($billingType, array('prepaid', 'postpaid', 'current'))) {

			foreach ($cproductCustomFieldUpdates[$invoiceItemKey] as $name => $value) {
				log_debug2('BILLING',4,'Update client product custom field - FieldName [%s], ServiceID [%s], FieldID [%s], NewValue [%s]',
					$name,$invoiceItem['relid'], $customFields[$name], $value
				);
			}

			// Change fields that are once use to n/a
			$values = array_merge(
					array(
						'serviceid' => $invoiceItem['relid'],
						'customfields' => base64_encode(serialize(
							$cproductCustomFieldUpdates[$invoiceItemKey]
						))
					), $cproductUpdates[$invoiceItemKey]
			);
			if ($live) {
				$res = localAPI('updateclientproduct',$values,$AWIT_IPPM_ADMIN_USER);
			}
		}

	}

	log_debug2('BILLING',1,'Processing [CustomBilling] completed on invoice [%s]',$invoice_id);
}



/**
 * Applies the bulk discount calculation to the specified products
 * where Bulk Discount Calculation is specified
 */
function awit_ippm_doBulkDiscountBilling($invoice_id, $live)
{
	global $AWIT_IPPM_ADMIN_USER;
	global $AWIT_IPPM_CONFIG_CUSTOM_FIELDS;


	log_debug2('BDBILLING',1,'Processing invoice [%s] for [BulkDiscountBilling]',$invoice_id);

	// Grab the invoice
	$invoice = localAPI('getinvoice',array('invoiceid' => $invoice_id),$AWIT_IPPM_ADMIN_USER);

	// We cache our invoice items between the two loops
	$invoiceData = array();

	// Check if we doing bulk discount billing
	$hasBulkDiscountField = 0;
	foreach ($invoice['items']['item'] as $invoiceItemKey => $invoiceItem) {
		// Grab product info
		$invoiceData[$invoiceItemKey] = list($cproduct,$customFields,$a_invoiceItem) = awit_ippm_getProductInfo($invoiceItem);
		// Return if we have the field we need
		if (isset($customFields['bulk_discount_calculation'])) {
			$hasBulkDiscountField = 1;
		}
	}
	// If we not there is no use continuing
	if (!$hasBulkDiscountField) {
		log_debug2('BDBILLING',3,'Skipping invoice, no bulk discount billing on this invoice');
		return false;
	}

	// Updates to process for this invoice
	$invoiceUpdates = array();

	// Loop with invoice items
	foreach ($invoiceData as $invoiceItemData) {
		// Grab product info
		list($cproduct,$customFields,$invoiceItem) = $invoiceItemData;

		// Check if we have a supported field
		if (empty($customFields['bulk_discount_calculation'])) {
			continue;
		}

		log_debug2('BDBILLING',1,'Product: [%s], Domain: [%s]',$cproduct['name'],$cproduct['domain']);

		// Check for bulk discount calculation field
		$bulkDiscountPids = NULL;
		$bulkDiscountCalculation = NULL;
		if ($value = $customFields['bulk_discount_calculation']) {
			// check for a valid bulk discount calculation attribute pair string
			if (preg_match('/^\[((?:[0-9],?)+)\/((?:[0-9]+=[0-9\.]+,?)+)\]$/',$value,$matches)) {
				// Pull in bulk discount pids & calculation
				$bulkDiscountPids = $matches[1];
				$bulkDiscountCalculation = $matches[2];
			} else {
				log_debug2('BDBILLING',1,'ERROR: Validation on field [%s] value [%s] failed',
					log_debug2_pretty_variable($AWIT_IPPM_CONFIG_CUSTOM_FIELDS['bulk_discount_calculation']),
					log_debug2_pretty_variable($value)
				);
				continue;
			}
		}

		log_debug2('BDBILLING',3,'Service info - BulkDiscountCalculation [%s]',
			log_debug2_pretty_variable($bulkDiscountCalculation)
		);

		// Get an array with our pids in
		$bulkDiscountPidList = explode(',',$bulkDiscountPids);
		// Mapping of pid's to discount bands
		$bulkDiscountCalculationMapping = array();
		foreach (explode(',', $bulkDiscountCalculation) as $item) {
			list($qty,$multiplier) = explode('=', $item);
			// Check attribute is valid
			if (!isset($qty) || !isset($multiplier)) {
				log_debug2('BDBILLING',1,'ERROR: Bulk discount calculation item [%s] is invalid',
					$item
				);
				continue;
			}
			// Check for duplicates
			if (in_array($qty,array_keys($bulkDiscountCalculationMapping))) {
				log_debug2('BDBILLING',2,'WARNING: Bulk discount calculation item [%s] is duplicated',
					$item
				);
				continue;
			}

			$bulkDiscountCalculationMapping[$qty] = $multiplier;
		}
		// Sort array so we have the highest value is first
		krsort($bulkDiscountCalculationMapping);

		// Loop with invoice items and total up
		$variableQuantityTotal = 0;
		$variableAmountTotal = 0;
		foreach ($invoiceData as $v_invoiceItemData) {
			// Grab product info
			list($a_cproduct,$a_customFields,$a_invoiceItem) = $v_invoiceItemData;

			// If we have a specific quantity use it instead of the default 1
			$variableQuantity = 1;
			if (isset($a_customFields['variable_quantity'])) {
				$variableQuantity += $a_customFields['variable_quantity'];
			}
# FIXME: rounding
			// Calculate our new total
			$variableQuantityNewTotal = $variableQuantityTotal + $variableQuantity;
			$variableAmountNewTotal = $variableAmountTotal + $a_invoiceItem['amount'];

			log_debug2('BDBILLING',4,'Using [%s] domain [%s], VariableQuantityNewTotal updated [%s] = VariableQuantityTotal [%s] + VariableQuantity [%s], '.
					'VariableAmountNewTotal updated [%s] = VariableAmountTotal [%s] + VariableAmount [%s]',
				$a_cproduct['name'], $a_cproduct['domain'],
				$variableQuantityNewTotal,
				$variableQuantityTotal,
				$variableQuantity,
				$variableAmountNewTotal,
				$variableAmountTotal,
				$a_invoiceItem['amount']
			);
			// Save our values
			$variableQuantityTotal = $variableQuantityNewTotal;
			$variableAmountTotal = $variableAmountNewTotal;
		}

		log_debug2('BDBILLING',3,'Calculated VariableQuantityTotal [%s], VariableAmountTotal [%s]',
			$variableQuantityTotal.
			$variableAmountTotal
		);

		// Look which band this item falls in, start at the top
		foreach ($bulkDiscountCalculationMapping as $qty => $multiplier) {
			// We found IT!!!
			if ($variableQuantityTotal >= $qty) {
				// If we do calculate the item data
				$itemDescription = sprintf("%s\nBulk Discount of %.2f%% applied to %.2f",
					$invoiceItem['description'],
					$multiplier * 100,
					$variableAmountTotal
				);

				// Calculate the amount
				$itemAmount = sprintf('%.2f',$variableAmountTotal * $multiplier * -1);

				log_debug2('BDBILLING',3,'+ Adding commitment bulk discount with ItemDescription [%s] and ItemAmount [%.2f], DiscountBand [%s]',
					str_replace("\n", '\n', $itemDescription),
					$itemAmount,
					$qty
				);

				break;
			}
		}

		// Setup new invoice item update
		$invoiceUpdates['newitemdescription'][$invoiceItem['id']] = $itemDescription;
		$invoiceUpdates['newitemamount'][$invoiceItem['id']] = $itemAmount;
		$invoiceUpdates['newitemtaxed'][$invoiceItem['id']] = $invoiceItem['taxed'];
	}

	awit_ippm_updateInvoice($invoice_id,$invoiceUpdates,$live);

	log_debug2('BDBILLING',1,'Processing [BulkDiscountBilling] completed on invoice [%s]',$invoice_id);
}



/*
 * Function to return product details
 * Returns: $cproduct, $configCustomFields, $product
 */
function awit_ippm_getProductInfo($invoiceItem)
{
	global $AWIT_IPPM_ADMIN_USER;
	global $AWIT_IPPM_CONFIG_CUSTOM_FIELDS_REVERSE;


	// Pull in the client product
	if (!($invoiceItem['relid'] > 0)) {
		log_debug2('getProductInfo',1,'ERROR: Bad RELID "%s"',$invoiceItem['relid']);
		return;
	}

	$cproduct = localAPI('getclientsproducts',array('serviceid' => $invoiceItem['relid']),$AWIT_IPPM_ADMIN_USER);
	// Make things a little easier
	if (isset($cproduct['products']['product'][0])) {
		$cproduct = $cproduct['products']['product'][0];
	} else {
		log_debug2('getProductInfo',1,'ERROR: Bad product return from localAPI/getclientproducts [%s]',var_export($cproduct, true));
		return;
	}

	log_debug2('getProductInfo',1,'Product: [%s], Domain: [%s]',$cproduct['name'],$cproduct['domain']);

	// Make a list of custom fields
	$customFields = array();
	foreach ($cproduct['customfields']['customfield'] as $customField) {
		$customFieldName = trim($customField['name']);
		// Check if this is a system custom field, if so rename it
		if ($newCustomFieldName = $AWIT_IPPM_CONFIG_CUSTOM_FIELDS_REVERSE[$customFieldName]) {
			$customFieldName = $newCustomFieldName;
		}
		$customFields[$customFieldName] = trim($customField['value']);
		$customFields["_".$customFieldName."_id"] = $customField['id'];
	}

	// Pull in the system product
	$product = localAPI('getproducts',array('pid' => $cproduct['pid']),$AWIT_IPPM_ADMIN_USER);

	return array($cproduct, $customFields, $invoiceItem);
}



// Function to conditionally process invoice updates
function awit_ippm_updateInvoice($invoice_id,$invoiceUpdates,$live)
{
	global $AWIT_IPPM_ADMIN_USER;


	// Are there invoice updates to be made?
	if (sizeof($invoiceUpdates)) {
		// Change invoice
		if ($live) {
			// Parameters
			$params = $invoiceUpdates;
			$params['invoiceid'] = $invoice_id;
			// Update
			$res = localAPI('updateinvoice',$params,$AWIT_IPPM_ADMIN_USER);
		} else {
			log_debug2('awit_ippm_updateInvoice',5,'Non-Live Update: '.print_r($invoiceUpdates,1));
		}
	}
}


// Hook into the invoice creation so we can work some magic
function awit_ippm_hook_invoice_creation_pre_email($vars, $live = 1)
{
	global $AWIT_IPPM_IVER;
	global $AWIT_IPPM_CONFIG_CUSTOM_FIELDS;
	global $AWIT_IPPM_CONFIG_CUSTOM_FIELDS_REVERSE;
	global $AWIT_IPPM_SUPPORTED_FIELDS;


	// Query modules table
	$table = "tbladdonmodules"; $fields = "setting,value"; $where = array( 'module' => 'awit_ippm' );
	$result = select_query($table,$fields,$where);

	// Filter out the settings we need
	$AWIT_IPPM_CONFIG_CUSTOM_FIELDS = $AWIT_IPPM_SUPPORTED_FIELDS;
	while ($row = mysql_fetch_assoc($result)) {
		// Check in our global list
		if (in_array($row['setting'],array_keys($AWIT_IPPM_SUPPORTED_FIELDS))) {
			$value = trim($row['value']);
			// Setup globals
			$AWIT_IPPM_CONFIG_CUSTOM_FIELDS[$row['setting']] = $value;
		}	
	}
	foreach ($AWIT_IPPM_CONFIG_CUSTOM_FIELDS as $key => $value) {
		$AWIT_IPPM_CONFIG_CUSTOM_FIELDS_REVERSE[$value] = $key;
	}

	// Check the addon is enabled?
	if ($live && !isset($AWIT_IPPM_CONFIG_CUSTOM_FIELDS['version'])) {
		log_debug("HOOK: Addon not enabled, live = $live");
		exit;
	}

	// Invoice ID
	$invoice_id = array_shift($vars);

	log_debug2('AWIT_IPPM',1,'Version [%s]',$AWIT_IPPM_IVER);

	awit_ippm_doVariableBilling($invoice_id, $live);

	awit_ippm_doCustomBilling($invoice_id, $live);

	awit_ippm_doCommitmentBilling($invoice_id, $live);

	awit_ippm_doBulkDiscountBilling($invoice_id, $live);
}


// vim: ts=4
