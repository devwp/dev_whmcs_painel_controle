<div id="add_mailbox_bookmarks_view">
    <form action="" method="post" id="add_mailbox_form">
        <input type="hidden" value="add_account" name="do"/>
        <div id="add_mailbox_general_area">
            <table cellspacing="0" cellpadding="0">
                <tr><h4>{$lang.field.user_details}</h4></tr>

                <tr>
                    <td>{$lang.field.firstname}*</td>
                    <td><input type="text" name="firstname" value="{$data.firstname}"/></td>
                </tr>
                <tr>
                    <td>{$lang.field.lastname}*</td>
                    <td><input type="text" name="lastname" value="{$data.lastname}"/></td>
                </tr>


                <tr>
                    <td><p class="login">{$lang.field.login}*</p></td>
                    <td>
                        <input type="text" name="login" value="{$data.login}"/>
                    </td>
                </tr>
                <tr>
                    <td>{$lang.field.email}*</td>
                    <td>
                        <input type="text" name="email" value="{$data.email}"/>
                    </td>
                </tr>

                <tr>
                    <td>{$lang.field.password}*</td>
                    <td>
                        <input type="password" name="password" value="{$data.password}"/>
                    </td>
                </tr>

                <tr>
                    <td>{$lang.field.confirmpassword}*</td>
                    <td>
                        <input type="password" name="confirm_password" value="{$data.password}"/>
                    </td>
                </tr>

            </table>
        </div>
        <input type="submit" name="submit" id="add_mailbox_submit" class="btn" value="{$lang.add_account}"/>
        <a class="btn" href="{$module_links.product_custom_button_action}">{$lang.cancel_account}</a>
    </form>
</div>