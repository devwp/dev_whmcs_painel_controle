<?php
/**
 *
 * @Copyright © 2002-2016 Acronis International GmbH. All rights reserved
 */
namespace Acronis\Standard\Controllers;

use Acronis\ApiManagers\SystemManager;
use Acronis\Helpers\AcronisParams;
use Acronis\Helpers\AcronisViews as AcronisViewsHelper;
use Acronis\Helpers\ModuleVars;
use Acronis\Helpers\RequestCleaner;
use Acronis\Log;
use Acronis\Standard\Configs\ModuleConfig;
use Acronis\System\MasterController;
use Acronis\UserException;
use Acronis\WHMCS\CustomFields;
use Exception;

class ClientArea extends MasterController
{
    public function actionIndex()
    {
        //classes initialization:-------------------------------------------------------------------------------------------
        $params = new AcronisParams($this->params, $this->moduleConfiguration);
        $module_vars = new ModuleVars($this->moduleConfiguration->moduleName);
        $custom_fields = new CustomFields($this->moduleConfiguration->moduleName);
        $config = new ModuleConfig(true);
        $acronis_views_helper = new AcronisViewsHelper();
        $manager = new SystemManager($params->getServerUrl(), $this->moduleConfiguration, $module_vars);
        $vars = $this->params;

        $usagedata = $acronis_views_helper->getGroupUsageDetailsDefaultVariables();
        $loginlink = '';

        try {
            //login to system:--------------------------------------------------------------------------------------------------
            //getting new password from $_POST if has been changed:
            $login_password = $vars["password"];
            $password_change = false;

            RequestCleaner::init('INPUT_POST');
            $postdata = RequestCleaner::getAllVars();

            if (isset($postdata['modulechangepassword']) && $postdata['modulechangepassword'] == true &&
                isset($postdata['newpw']) && isset($postdata['confirmpw']) && $postdata['newpw'] == $postdata['confirmpw']
            ) {
                $login_password = $postdata['newpw'];
                $password_change = true;
            }

            try {
                list($admin_id, $admin_group_id) = $manager->accountsManager->login($vars["username"], $login_password);
            } catch (UserException $e) {
                if ($password_change) {
                    //if cannot login and password has been changed: attempt to login with unchaged password
                    list($admin_id, $admin_group_id) = $manager->accountsManager->login($vars["username"], $vars["password"]);
                }
            }

            $userId = $custom_fields->get($vars['pid'], $vars['serviceid'], 'AdminID');
            $groupId = $custom_fields->get($vars['pid'], $vars['serviceid'], 'GroupID');

            //getting group usage data:-----------------------------------------------------------------------------------------
            //needed group parameters and their default values:
            $params_to_gather = array
            (
                'usage.server_count' => 0,
                'usage.service_users' => 0,
                'usage.storage_size' => 0,
                'usage.vm_count' => 0,
                'usage.workstation_count' => 0,
                'usage.mobile_count' => 0,
                'usage.mailbox_count' => 0,
                'privileges.server_count' => 0,
                'privileges.storage_size' => 0,
                'privileges.vm_count' => 0,
                'privileges.workstation_count' => 0,
                'privileges.mobile_count' => 0,
                'privileges.mailbox_count' => 0,
                'kind' => 0,
            );
            $group_usage_data = $manager->groupsManager->getGroupParamsForced($params_to_gather, $groupId);

            list($result, $usagedata) = $acronis_views_helper->prepareGroupUsageDetails($group_usage_data);
            if ($result != 'success') {
                $this->errors [] = $result;
            }

            //getting admin account
            try {
                if ($group_usage_data['kind'] == $config->getGroupKindByType(ModuleConfig::GROUP_TYPE_RESELLER)) {
                    //if reseller - get list of admins
                    $result = $manager->groupsManager->getGroupAdminsDetails($groupId);
                    $buttonTitle = $this->lang->get('login_to_management_console');
                } elseif ($group_usage_data['kind'] == $config->getGroupKindByType(ModuleConfig::GROUP_TYPE_CUSTOMER)) {
                    //if customer - get list of users
                    $result = $manager->groupsManager->getGroupUsersDetails($groupId);
                    $buttonTitle = $this->lang->get('login_to_backup_console');
                } else {
                    $result = array();
                    Log::getInstance()->error("Trying to get administrator list for group {$groupId} with unsupported kind '{$group_usage_data['kind']}'");
                }
            } catch (Exception $e) {
                $result = null;
            }

            $loginlink = $manager->groupsManager->getLinkToAcronisPanel($userId);
            if (!$loginlink) {
                $this->infos [] = $this->lang->get('errors.selected_group_has_no_bc_access');
            }

            $backupAccount = null;
            if (count($result) > 0) {
                $backupAccount = array(
                    'id' => $result[0]['id'],
                    'status' => $result[0]['status'],
                    'login' => $result[0]['login'],
                    'email' => $result[0]['email'],
                );
            }

        } catch (UserException $e) {
            Log::getInstance()->error('Exceptions with message: ' . $e->getMessage());
        }

        //rendering template:-----------------------------------------------------------------------------------------------
        $template = $this->loadTpl('ClientArea' . DS . 'clientarea.tpl');

        $template->setVar('admAccount', $backupAccount);
        $template->setVar('usagedata', $usagedata);
        $template->setVar('loginlink', $loginlink);
        $template->setVar('buttonTitle', $buttonTitle);

        return $this->renderTpl($template);
    }
}

