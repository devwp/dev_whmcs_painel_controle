<?php
/**
 *
 * Acronis modules specific helpers
 * @version 1.0.0
 * @Copyright © 2002-2016 Acronis International GmbH. All rights reserved
 * DEPENDENCIES: \Acronis\WHMCS\Lang
 */
namespace Acronis\Standard\Helpers;

use Acronis\Standard\Configs\ModuleConfig;
use Acronis\WHMCS\Lang;

class AcronisForm
{
    /**
     * @var ModuleConfig $config
     */
    private $config;

    /**
     * @var Lang $lang
     */
    private $lang;

    private $data;

    public function __construct(ModuleConfig $config, Lang $lang, $data)
    {
        $this->lang = $lang;
        $this->config = $config;
        $this->data = $data;
    }

    public function validateUserID()
    {
        $value = isset($this->data['userid']) ? $this->data['userid'] : '';
        $value = filter_var($value, FILTER_VALIDATE_INT);
        if (NULL === $value || false === $value || '' === $value) {
            $this->config->makeUserError($this->lang->get('errors.form_not_complete'));
        }
        $this->data['userid'] = $value;
    }

    public function validateLogin($langfieldname = null)
    {
        if (!$langfieldname) {
            $langfieldname = $this->lang->get('field.login');
        } else {
            $langfieldname = $this->lang->get('field.' . $langfieldname);
        }

        $login = isset($this->data['login']) ? $this->data['login'] : '';
        if ('' !== $login) {
            if (preg_match("/^[a-z0-9._\-\+@]+$/i", $login)) {
                if (strlen($login) <= 3) {
                    $this->config->makeUserError($this->lang->get('errors.login_too_short', array($langfieldname)));
                } elseif (strlen($login) >= 64) {
                    $this->config->makeUserError($this->lang->get('errors.login_too_long', array($langfieldname)));
                }
            } else {
                $this->config->makeUserError($this->lang->get('errors.invalid_login'));
            }
        } else {
            $this->config->makeUserError($this->lang->get('errors.empty_login'));
        }
        $this->data['login'] = $login;
    }

    public function validateEmail()
    {
        $langfieldname = $this->lang->get('field.email');
        $email = isset($this->data['email']) ? $this->data['email'] : '';
        $email = filter_var($email, FILTER_VALIDATE_EMAIL);
        if (NULL === $email) {
            $this->config->makeUserError($this->lang->get('errors.invalid_email'));
        } elseif (false === $email || '' === $email) {
            $this->config->makeUserError($this->lang->get('errors.empty_email'));
        }
        $this->data['email'] = $email;
    }

    public function validateInput($fieldname)
    {
        $value = isset($this->data[$fieldname]) ? $this->data[$fieldname] : '';
        $value = filter_var($value);
        if (NULL == $value || $value == '') {
            $this->config->makeUserError($this->lang->get('errors.empty_' . $fieldname));
        }
        $this->data[$fieldname] = $value;
    }

    public function validatePassword()
    {
        $value = isset($this->data['password']) ? $this->data['password'] : '';
        $confirm_value = isset($this->data['confirm_password']) ? $this->data['confirm_password'] : '';

        if ($value != $confirm_value) {
            $this->config->makeUserError($this->lang->get('errors.password_doesnt_match'));
        } elseif (preg_match_all('/[A-Z]/', $value, $mtc) < 1//uppercase
            OR preg_match_all('/[a-z]/', $value, $mtc) < 1//lowercase
            OR preg_match_all('/[0-9]/', $value, $mtc) < 1//numbers
            OR preg_match_all('/[\s]/', $value, $mtc) != 0
            OR strlen($value) < 6
        ) {
            $this->config->makeUserError($this->lang->get('errors.password_characters_including'));
        }
        $this->data['password'] = $value;
        $this->data['confirm_password'] = $confirm_value;
    }


    public function getFilteredPost()
    {
        return $this->data;
    }


}
