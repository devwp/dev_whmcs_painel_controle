<?php

if (!function_exists('AcronisBackupService_loadAutoloader')) {
    function AcronisBackupService_loadAutoloader()
    {
        require_once ACRONIS_INCLUDES_DIR . 'Autoloader.php';
        $autoloader = new Acronis\Autoloader(ACRONISSP_OWN_DIR . 'app' . DS . 'Acronis' . DS, 'Acronis', ACRONIS_INCLUDES_DIR);

        return $autoloader;
    }
}


if (!function_exists('AcronisBackupService_unloadAutoloader')) {
    function AcronisBackupService_unloadAutoloader($autoloader)
    {
        $autoloader->unregister();
    }
}


if (!function_exists('AcronisBackupService_loadErrorHandler')) {
    function AcronisBackupService_loadErrorHandler(\Acronis\Standard\Configs\ModuleConfig $config)
    {
        $error_handler = new Acronis\ErrorHandler($config);

        return $error_handler;
    }
}


if (!function_exists('AcronisBackupService_unloadErrorHandler')) {
    function AcronisBackupService_unloadErrorHandler($error_handler)
    {
        $error_handler->unregister();
    }
}