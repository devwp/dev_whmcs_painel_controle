<?php
return array(
    'logApiRequests' => 0, // log requests to module log (0 - off, 1 - on, 2 - only bad requests)
    'verifyCertificates' => true, // curl should verify certificates. Use false for testing purposes if there are certificates verification problems
    'writeTextLog' => true, // write debug message to log file,
    'pathToLog' => LOG_PATH . '/acronis-backup-service.log'
);