{if $message != ""}
	<div class="alert alert-success text-center" id="alertModuleCustomButtonSuccess">
		{$message}
	</div>
{/if}
{if $errormessage != ""}
	<div class="alert alert-danger text-center" id="alertModuleCustomButtonFailed">
		{$errormessage}
	</div>
{/if}

<ul class="nav nav-tabs">

  <li><a href="clientarea.php?action=productdetails&id={$id}">{$LANG.yourdetails} </a></li>
  <!--<li><a data-toggle="tab" href="#tabDownloads">{$LANG.downloadsfiles}</a></li>
	<li><a data-toggle="tab" href="#tabChangepw">{$LANG.serverchangepassword}</a></li>-->
{if $IsDedicated == 'on'}
  <li><a href="">{$LANG.smmanageDomain}</a></li>
{/if}
  <li class="active"><a href="clientarea.php?action=productdetails&id={$id}&modop=custom&a=managesmartermailuserdomain">{$LANG.smmanageusers }</a></li>
  <li><a href="upgrade.php?type=package&id={$id}">{$LANG.upgradedowngradepackage}</a></li>
</ul>





<!--div style="float:left; width: 123px;">
	<form method="post" action="clientarea.php?action=productdetails">
		<input type="hidden" name="id" value="{$id}" />
		<input type="hidden" name="modop" value="custom" />
		<input type="hidden" name="domain" value="{$DomainName}">
		<input type="hidden" name="a" value="editsmartermaildomainpage" />

		<select style="width: 0px; height: 0; border:0; line-height: 0" size="0" name="selectalias">
		<option value="{$DomainName}">{$DomainName}</option>
		</select>
		<input type="submit" value="{$LANG.smglobalback}" class="btn btn-primary" style="width:123px" />
	</form>
</div><br clear="both" />
<br clear="both" /-->









<div class="row">
<div class="col-md-12">
<div class="panel panel-default">
<div class="panel-heading" >
	<h3 class="panel-title" >{$LANG.smmanageusers}</h3>
</div>
<div class="panel-body">
	<div style="float:left; width: 180px;">
		{if $canCreateUser}
		<a href="clientarea.php?action=productdetails&id={$id}&modop=custom&a=addsmartermailuser"  class="btn btn-primary"><i class="fa fa-plus-circle" aria-hidden="true"></i> {$LANG.smaddusers}</a>
		<!--form method="post" action="clientarea.php?action=productdetails&id={$id}">
			<input type="hidden" name="modop" value="custom" />
			<input type="hidden" name="a" value="addsmartermailuser" />
			<input type="hidden" name="domain" value="{$DomainName}" />
			<button type="submit" class="btn btn-primary"><i class="fa fa-plus-circle" aria-hidden="true"></i> {$LANG.smaddusers}</button>
		</form-->
		{/if}
	</div>
	<div class="tab-content">
	    <div><br clear="both"/>

<form method="post" action="clientarea.php?action=productdetails&id={$id}" class="form-horizontal using-password-strength" >
    <h3>{$LANG.smaddusers}</h3>
    <div class="form-group">
        <label for="inputalias" class="col-sm-5 control-label">{$LANG.smusersemail}</label>
        <div class="col-sm-6">
		<input type="text" name="newusername" style="width: 200px;" class="form-control"/>@{$DomainName}
        </div>
    </div>
	<div class="form-group">
        <label for="inputalias" class="col-sm-5 control-label">{$LANG.smuserspass}</label>
        <div class="col-sm-6">
		<input type="text" name= "newpassword" style="width: 200px;" class="form-control"/>
        </div>
    </div>
	<div class="form-group">
        <label for="inputalias" class="col-sm-5 control-label">{$LANG.smusersname}</label>
        <div class="col-sm-6">
		<input type="text" name= "newfirstname" style="width: 200px;" class="form-control"/>
        </div>
    </div>

    <div class="form-group">
        <div class="text-center">
			<input type="hidden" name="id" value="{$id}" />
			<input type="hidden" name="modop" value="custom" />
			<input type="hidden" name="domain" value="{$DomainName}" />
			<input type="hidden" name="a" value="createsmartermailuser" />
			<input type="submit" value="{$LANG.smglobalsave}" class="btn btn-primary" />
        </div>
    </div>
</form>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
