   <script type="text/javascript">
      $("#internal-content").attr("style","width:100% !important");
      $(".sidebar-primary").hide();
    </script> 

{if $IsDedicated == 'on'}
<div class="col-md-6">
  <div class="panel panel-default">
    <div class="panel-heading">
      <h3 class="panel-title">{$LANG.resourceUsage}</h3>
    </div>
    <div class="panel-body text-center">
      <div class="col-sm-10 col-sm-offset-1">
        <div class="col-sm-12">
           <h4>Contas</h4>
           <input type="text" value="{$totalDomainsPercent}" class="dial-usage1" data-width="100" data-height="100" data-min="0" data-readOnly="true" />
           <p>{$totalDomains} / {$maxDomains}</p>

         </div>
       </div>
       <div class="clearfix"></div>
       <!--p class="text-muted">{$LANG.clientarealastupdated}: {$lastupdate}</p-->
       <script src="{$BASE_PATH_JS}/jquery.knob.js"></script>
       <script type="text/javascript">
       jQuery(function() {ldelim}
         jQuery(".dial-usage1").knob({ldelim}
            'change': function (v) {ldelim} console.log(v); {rdelim},
            draw: function () {ldelim}
              $(this.i).val(this.cv + '%');
            {rdelim}
         {rdelim});
       {rdelim});
       </script>
     </div>
   </div>
 </div>
{else}
<div class="col-md-6">
  <div class="panel panel-default">
    <div class="panel-heading">
      <h3 class="panel-title">{$LANG.resourceUsage}</h3>
    </div>
    <div class="panel-body text-center">
      <div style="float:left; width:50%">
        <div >
           <h4>{$LANG.cPanel.emailAccounts }</h4>
           <input type="text" value="{$totalUsersPercent}" class="dial-usage1" data-width="100" data-height="100" data-min="0" data-readOnly="true" />
           <p>{$totalUsers} / {$maxUsers}</p>
           <p>
             <a href="clientarea.php?action=productdetails&id={$id}&modop=custom&a=managesmartermaildomain" class="btn btn-default btn-sm">{$LANG.smmanageusers}</a>
           </p>
         </div>
       </div>
          <div style="float:left; width:50%">
        <div>
       <h4>Contas Exchange:</h4>
           <input type="text" value="{$totalUsersPercent}" class="dial-usage1" data-width="100" data-height="100" data-min="0" data-readOnly="true" />
           <p>{$totalUsers} / {$maxUsers}</p>
           <p>
             <a href="clientarea.php?action=productdetails&id={$id}&modop=custom&a=managesmartermaildomain" class="btn btn-default btn-sm">{$LANG.smmanageusers}</a>
           </p>
         </div> 
       </div>
       <div class="clearfix"></div>
       <!--p class="text-muted">{$LANG.clientarealastupdated}: {$lastupdate}</p-->
       <script src="{$BASE_PATH_JS}/jquery.knob.js"></script>
       <script type="text/javascript">
       jQuery(function() {ldelim}
         jQuery(".dial-usage1").knob({ldelim}
            'change': function (v) {ldelim} console.log(v); {rdelim},
            draw: function () {ldelim}
              $(this.i).val(this.cv + '%');
            {rdelim}
         {rdelim});
       {rdelim});
       </script>
     </div>
   </div>
 </div>
<div class="col-md-6">
  <div class="panel panel-default">
    <div class="panel-heading">
      <h3 class="panel-title">Contrate ou amplie seus recursos</h3>
    </div>
    <div class="panel-body text-center">
      <div class="col-sm-10 col-sm-offset-1">
 <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script>
  $( function() {
    $( "#slider-range-max" ).slider({
      range: "max",
      min: 5,
      max: 100,
      value: 2,
      slide: function( event, ui ) {
        $( "#amount" ).val( ui.value );
      }
    });
    $( "#amount" ).val( $( "#slider-range-max" ).slider( "value" ) );
  } );
   $( function() {
    $( "#slider-range-max2" ).slider({
      range: "max",
      min: 5,
      max: 100,
      value: 2,
      slide: function( event, ui ) {
        $( "#amount2" ).val( ui.value );
      }
    });
    $( "#amount2" ).val( $( "#slider-range-max2" ).slider( "value" ) );
  } );
  </script>
  <h3>Contas de e-mail:</h3>
  <div id="slider-range-max"></div>
  <p for="amount" style="border:0; color: #5D5F63; font-family: 'Open Sans', sans-serif; float:left; font-size:16px; line-height:36px;"> Mudar meu plano para </p> <input type="text" id="amount" readonly style="border:0; color:#666; font-weight:bold; width:46px; font-size:25px; float:left;line-height:16px;text-align:center"/><p for="amount" style="border:0;       color: #5D5F63; font-family: 'Open Sans', sans-serif;float:left;font-size:16px;line-height:36px;">  contas de e-mail</p>
     
       <div class="clearfix"></div>
<h3>Contas Exchange:</h3>
  <div id="slider-range-max2"></div>
  <p for="amount2" style="border:0; color: #5D5F63; font-family: 'Open Sans', sans-serif; float:left; font-size:16px; line-height:36px;"> Mudar meu plano para </p> <input type="text" id="amount2" readonly style="border:0; color:#666; font-weight:bold; width:46px; font-size:25px; float:left;line-height:16px;text-align:center"/><p for="amount2" style="border:0; color: #5D5F63; font-family: 'Open Sans', sans-serif;float:left;font-size:16px;line-height:36px;">  contas exchange</p> 
       </div>
       <div class="clearfix"></div>
         </div>
     </div> 
   </div>

 {/if}

