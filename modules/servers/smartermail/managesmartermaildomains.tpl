<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title></head>
<body>
    <link rel="stylesheet" type="text/css" href="https://controle.webplus.com.br/assets/css/dataTables.bootstrap.css">
    <link rel="stylesheet" type="text/css" href="{$BASE_PATH_CSS}/dataTables.responsive.css">
    <script type="text/javascript" charset="utf8" src="{$BASE_PATH_JS}/jquery.dataTables.min.js"></script>
    <script type="text/javascript" charset="utf8" src="{$BASE_PATH_JS}/dataTables.bootstrap.min.js"></script>
    <script type="text/javascript" charset="utf8" src="{$BASE_PATH_JS}/dataTables.responsive.min.js"></script>
    <script type="text/javascript">
    jQuery(document).ready(function () { 
        var table = jQuery("#tableDomainList").DataTable({ "dom": '<"listtable"fit>pl', "responsive": true, "oLanguage": { "sEmptyTable": "{$LANG.smglobalNothing}", "sInfo":"", "sInfoEmpty": "", "sInfoFiltered": "({$LANG.smglobalshowing} _MAX_ total)", "sInfoPostFix": "", "sInfoThousands": ",", "sLengthMenu": "{$LANG.smglobalshow} _MENU_", "sLoadingRecords": "{$LANG.smgloballoading}", "sProcessing": "{$LANG.smglobalProcess}", "sSearch": "", "sZeroRecords": "{$LANG.smglobalNothing}", "oPaginate": { "sFirst": "", "sLast": "", "sNext": "{$LANG.smglobalNext}", "sPrevious": "{$LANG.smglobalPrev}" } }, "pageLength": 10, "columnDefs": [{ "targets": 'no-sort', "orderable": false, }], "order": [[0, "asc"]], "stateSave": true }); jQuery('#tableDomainList').removeClass('hidden').DataTable(); table.draw(); jQuery("#tableDomainList_filter input").attr("placeholder", "{$LANG.smglobalsearch}"); });
    </script>
    <script src="/assets/js/bootstrap-tabdrop.js"></script>
    <script type="text/javascript">
    jQuery('.nav-tabs').tabdrop(); 
        $("#internal-content").attr("style","width:100% !important");
        $(".sidebar-primary").hide();
    </script>
    <script type="text/javascript">
    $(function() {
      // Javascript to enable link to tab
      var hash = document.location.hash;
      if (hash) {
        console.log(hash);
        $('.nav-tabs a[href='+hash+']').tab('show');
      }
      // Change hash for page-reload
      $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {
        window.location.hash = e.target.hash;
      });
    });
</script>
{debug}
    {if ($IsDedicated == "on")}
        <h3 style="padding:10px;">Servidor: {$serverdata.hostname}</h3>
    {else}
        <h3 style="padding:10px;">Serviço: E-mail</h3>
    {/if}
    {if $message != ""}
    <div class="alert alert-success text-center" id="alertModuleCustomButtonSuccess">{$message} </div>
    <br clear="both" />
    {/if} {if $errormessage != ""}
    <div class="alert alert-danger text-center" id="alertModuleCustomButtonFailed">{$errormessage} </div>
    <br clear="both" />
    {/if}<div class="row">
        <div class="col-md-12">
            <ul class="nav nav-tabs">
                <li><a href="clientarea.php?action=productdetails&id={$id}">{$LANG.yourdetails}</a></li>
   
                <li class="active"><a data-toggle="tab" href="#overview">{$LANG.smmanageDomain}</a></li>

            </ul>

        </div>
    </div>
    <div class="tabbable">
        <div class="row">
            <div class="col-md-12">
         <div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">{$LANG.smmanageDomain}</h3>
    </div>
    <div class="panel-body">
                        <div style="float: left; width: 180px;">{if $canCreateDomain}<a href="#addsmartermailDomain" class="editmodal btn btn-primary" data-toggle="modal" data-target="#myModalAddDomain"><i class="fa fa-plus-circle" aria-hidden="true"></i>&nbsp;&nbsp;{$LANG.smaddDomain}</a>{/if} </div>
                        <div class="tab-content">
                            <br clear="both" />
                            <div id="domain" class="tab-pane active">
                                <table id="tableDomainList" class="table table-bordered table-hover table-list dataTable no-footer dtr-inline">
                                    <thead>
                                        <tr>
                                            <th>{$LANG.domainname}</th>
                                            <th class="no-sort"></th>
           
                                        </tr>
                                    </thead>
                                    <tbody>{if $totalDomains > 0}{foreach $wsGetDomainsResultArray as $key => $val }<tr>
                                        <td><a href="clientarea.php?action=productdetails&id={$id}&modop=custom&a=managesmartermaildomains&d={$val}" title="{$LANG.smglobalshow}">{$val} </a></td>
                                        <td style="width: 45px;" data-order="{$val.PercentUsed}">
                                            <div><a href="clientarea.php?action=productdetails&id={$id}&modop=custom&a=managesmartermaildomains&d={$val}" class="editmodal" style="float:left"><i class="fa fa-pencil" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;</a>
                                             <form method="post" style="float:left">
                                                    <input type="hidden" name="deldomainname" value="{$val}" /><input type="hidden" name="managed" value="deldomain" /><a href="#" onclick="if(confirm('Esta ação irá apagar todas as contas de email e todos os emails nas caixas de entrada.\nDeseja continuar?')){ parentNode.submit(); } else { return false; }" title="{$LANG.smglobalDelete} {$LANG.smDomain} {$val}"><i class="fa fa-trash-o" aria-hidden="true"></i></a></form>
                                            </div>
                                        </td>
                                    </tr>
                                        {/foreach}{/if}</tbody>
                                </table>
                            </div>
                            <br clear="both" />
                            <br clear="both" />
                        </div>
                    </div> 
                </div>
            </div>
        </div>
    </div>
    <div id="myModalAddDomain" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button><h4 class="modal-title">{$LANG.smaddDomain}</h4>
                </div>
                <div class="modal-body">
                    <div class="panel-body">
                        <fieldset>
                            <form method="post" class="form-horizontal using-password-strength">
                                <!--h3>{$LANG.smaddusers}</h3-->
                                <div class="form-group">
                                    <label for="newdomainname" class="col-sm-3 control-label">{$LANG.domainname}</label><div class="col-sm-6input-group">
                                        <input type="text" name="newdomainname" id="newdomainname" style="" class="form-control" /><!--divclass="input-group-addon">@{$DomainName}</div--></div>
                                </div>
                                <div class="form-group">
                                    <div class="text-center">
                                        <input type="hidden" name="managed" value="adddomain" />
                                        <button type="submit" class="btn btn-primary">
                                            <i class="fa fa-floppy-o" aria-hidden="true"></i>
                                        &nbsp;&nbsp;{$LANG.smaddDomain}</buton></div>
                                </div>
                            </form>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="text-right">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button></div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
