<?php


/**
 * @author Pawel Kopec <pawelk@modulesgarden.com>
 */
if($vars['disallow_action']['api_info']==1){
    ob_clean();
    header ("Location: clientarea.php?action=productdetails&id=".$params['serviceid']);
    die();
}
  try {
         $onapCloudUser = new NewOnAppCloud_User($params['clientsdetails']['userid']);
         
         $user    = new NewOnApp_User(null);
         $user->setUserID($params['customfields']['userid']);
         $user->setconnection($params);
         if($_POST['regenerate']){
               $res =$user->generateAPIKey();
               $onapCloudUser->email =$res['user']['email'];
               $onapCloudUser->username = $res['user']['login'];
               $onapCloudUser->key = $res['user']['api_key'];
               $onapCloudUser->save();
               $vars['msg_success'] = $vars['lang']['regenerateMsg'];
         }
         $res = $user->getDetails();
         if(empty($onapCloudUser->key)){
              $vars['isGenerate'] = true;
         }
         $vars['userDetails'] = array(
                                'username' => $res['user']['email'],
                                'password' => $onapCloudUser->key
         );

         if($user->error())
               throw new Exception($user->error());
  } catch (Exception $ex) {
        $vars['msg_error'] = $ex->getMessage();
  }